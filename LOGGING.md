# Logging

## Technologies

Logging for Event Dynamic requires a few technologies:

1. [SL4J w/ Logback](https://www.slf4j.org/manual.html)
2. [Filebeats by Elastic](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-overview.html)
3. [AWS ElasticSearch Service w/ Kibana](https://aws.amazon.com/elasticsearch-service/)

Of these technologies, only the SL4J & Logback libraries need to be configured per job/service.  More on this later.

## Logging Lifecycle

Since we're dealing with services & jobs in DC/OS running across multiple agents (EC2 instances on AWS), we take the following steps for logging information:

1. First we need to stream to log files on disk.  We do this with SLF4J & Logback.
    - [Logback Configuration Documentation](https://logback.qos.ch/manual/configuration.html)
2. Next we use Elastic's Filebeats to watch updates to log files and send the latest logging information to AWS ElasticSearch Service
    - **Note:** Filebeats is configured on the private agents in the [Event Dynamic Infrastructure Repository](https://github.com/dialexa/ed-infrastructure).  You shouldn't have to do anything here.
3. Finally we leverage ElasticSearch's Kibana to view & query all the logs in the system.
    - *TBD: Add links to Kibana instances here*

## Convention

To configure a service/job to integrate with Event Dynamic's logging architecture, you'll need to do the following:

1. In the Dockerfile for your service/job, add the following line:

    ```sh
    # Add root level logs directory
    RUN mkdir /logs
    ```

2. In this repository's `configs` directory, locate your service/job's configuration file(s) (for each environment!!), and define the following value for `volumes`. This tells DC/OS how to mount the Docker container's `/logs` directory to the EC2 instance.  This means the log files written for a service/job will be persisted to an external disk on AWS.

    ```js
    {
        ...
        "container": {
                "volumes": [
                    {
                        "containerPath": "/logs",
                        "hostPath": "/mnt/event-dynamic/logs",
                        "mode": "RW"
                    }
                ],
                ...
        }
        ...
    }
    ```

    **Note:** Unforunately, DC/OS doesn't play nicely with mounting nested directories in a Docker container, which is why we created the root level logs directory in step 1.  [Visit here](https://docs.mesosphere.com/1.12/storage/persistent-volume/) for more information.

3. Configure your service/job with the appropriate logback configuration. This will live in the following file: `<SERVICE_OR_JOB_DIRECTORY>/src/main/resources/logback.xml`.  An example configuration used for the `api` service is provided below:

    ```xml
        <configuration>
            <conversionRule conversionWord="coloredLevel" converterClass="play.api.libs.logback.ColoredLevel" />

            <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
                <encoder>
                    <pattern>%coloredLevel %logger{15} - %message%n%xException{10}</pattern>
                </encoder>
            </appender>

            <appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
                <file>logs/ed-<SERVICE_OR_TASK_NAME_HERE>-${HOSTNAME}.log</file>
                <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
                    <!-- Daily rollover with compression -->
                    <fileNamePattern>logs/ed-<SERVICE_OR_TASK_NAME_HERE>-${HOSTNAME}-log-%d{yyyy-MM-dd}.gz</fileNamePattern>
                    <!-- keep 30 days worth of history -->
                    <maxHistory>30</maxHistory>
                </rollingPolicy>
                <encoder>
                    <pattern>%date{yyyy-MM-dd HH:mm:ss ZZZZ} [%level] from %logger in %thread - %message%n%xException</pattern>
                </encoder>
            </appender>

            <appender name="ASYNCFILE" class="ch.qos.logback.classic.AsyncAppender">
                <appender-ref ref="FILE" />
            </appender>

            <root level="INFO">
                <appender-ref ref="STDOUT" />
                <appender-ref ref="FILE" />
            </root>
        </configuration>
    ```

4. Finally, you'll need to include the logging dependencies in your project.  In `build.sbt` make sure to add `loggingDependencies` to your service/job's dependencies

And that's it! You now can view logs in local development, and when your service/job is deployed to DC/OS it'll automatically be populated into AWS ElasticSearch Service to be viewed in Kibana :tada:
