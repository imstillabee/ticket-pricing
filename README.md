# Event Dynamic Ticket Pricing Platform

This repository contains all code for running and maintaining the Event Dynamic Ticket Pricing Platform.

## Folder Layout

- `configs` holds the DC/OS package config files
- `scripts` holds scripts used to deploy the application
- `ed-backend` is the Scala project that has the API and batch scripts
- `ed-test` holds any integration and e2e tests
- `ed-web` is the React web application
- `flyway` holds the Flyway database migrations
- `jenkins` holds the Jenkinsfiles for building
- `jupyterhub` holds the Docker and Marathon files for running JupyterHub on DC/OS
