# configs

This folder contains all of the DC/OS package configuration files.

## Dependencies
In order to interact with the DC/OS infrastructure or Marathon services, make sure you have the [`dcos`](https://dcos.io/docs/latest/cli/) CLI utility installed.

## Usage

```sh
dcos package install <packagename> --options=<optionsjson>
```

## Service Configs and Environment Variables
The service config has been split into 2 separate config files – one with the core config and another with the environment variables. This allows us to update the core configuration without affecting the environment variables and vice versa.

#### Updating Existing Marathon Services
To update an existing Marathon service configuration, run the following command:
```sh
dcos marathon app update MARATHON_SERVICE_ID < CONFIG_FILE_PATH
```
_Example_
```sh
# example to update the API env variables
dcos marathon app update /qa/ed-api < configs/qa/ed-api/env.json

# example to update the Web core configuration
dcos marathon app update /qa/ed-web < configs/qa/ed-web/service.json
```

#### View Service's Current Environment Variables
You can view a service's currently configured environment variables by running the following command.
```sh
dcos marathon app show MARATHON_SERVICE_ID | jq '{env}'
```
_Note: this uses a CLI tool called [`jq`](https://stedolan.github.io/jq/)_
