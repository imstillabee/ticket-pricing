# DCOS Jobs

This directory contains client jobs that will run on DCOS.

## Event Dynamic Job Types

| Job Type  | Description  |  Run Interval |
|---|---| --- |
| Ticket Sync  | aka `Primary Sync` - this job syncs Primary transactions and prices to Event Dynamic  | 5 minutes|  
| Inventory Sync  | aka `Secondary Sync` - this job syncs Event Dynamic inventory onto a clients Secondary Integrations | 24 hr | 
| Ticket Pricing  | this job gets predicted prices and dispatched the prices to primary and secondary integrations   | 15 minutes |

## Suggested Directory Structure

Because we will have co-located jobs, each job type will have a directory with jobs for each client.

 ```
jobs
  │  sample.ticket-sync.json
  │  sample.ticket-pricing.json 
  │
  └───ticket-pricing
  │     sos.json
  │     marlins.json
  └───primary-sync
        sos.json
        marlins.json
```
## Client Specific Job Configuration Values

| key  | type  | format  |  example |
|---|---|---|---|
| id  | string  | `{environment}`.`{jobType}`.`{clientName}`  | prod.mets.ticketSync  | 
| description  | string  | Sync `{clientName}` ticket prices and transactions from `{Primary Integration}`  | Sync Mets ticket prices and transactions from Tickets.com   |

## Schedules

Schedules are defined with the following json structure:

```json
"schedules": [{
  "id": "default",
  "enabled": false,
  "cron": "*/15 * * * *",
  "timezone": "America/Chicago",
  "concurrencyPolicy": "FORBID",
  "startingDeadlineSeconds": 15
}]
```

Cron schedules are defined each provided schedule `cron` key using cron format. 

You can use this utility to help generate the correct expression: https://crontab.guru/

## Environment

Each job should have its own set of environment variables defined. These will differ depending on client integrations.

### Ticket Sync
* __ED_TICKET_SYNC_CLIENT_ID__ 
  
  Required for the ticket sync job. It should be set to the value of the client id in the Event Dynamic database. 
