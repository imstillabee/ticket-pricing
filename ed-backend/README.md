# Event Dynamic Backend

> ED Backend is the core Scala project for the Event Dynamic platform.

## Quick Start

### Setup

1. Make sure you have [Homebrew](https://brew.sh/) and [Docker for Mac](https://store.docker.com/editions/community/docker-ce-desktop-mac) installed

1. Install [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) for OS X

1. Install [sbt](https://www.scala-sbt.org/)
	
	`brew install sbt`
	
1. Install [flyway](https://flywaydb.org/)

	`brew install flyway`
 
1. Install [IntelliJ IDE Community Edition 2017](https://www.jetbrains.com/idea/download/#section=mac)
	1. Install the __Scala__, __Scalafmt__ and __BashSupport__ IntelliJ plugins

1. Import the __ed-backend__ project into IntelliJ
	1. Ensure the Project JDK is set to (_/Library/Java/JavaVirtualMachines/jdk1.8.0_172.jdk/Contents/Home_)
	1. From the "Import Project" wizard, click on "Import project from external model" and select "sbt"
		
		_Leave all other options as default_

1. Checkout the __.idea__ folder that was overwritten during the the importing process 
	
	`git checkout ed-backend/.idea`
		
1. Restart IntelliJ to load the shared __.idea__ config for the project

1. Copy __.env.template__ to __.env__ and update variables as needed

### Running

1. Start docker to run postgres in the background

	`docker-compose up -d`

1. Run migrations

	`cd ../flyway && flyway migrate`

1. Onboard a client

1. Start the API using the IntelliJ run configuration

## Project Structure

| Module | Type | Purpose |
| :--- | :--- | :--- |
| [__api__](https://github.com/dialexa/ed-ticket-pricing/tree/develop/ed-backend/api) | Service | REST API for the platform |
| [__external-services__](https://github.com/dialexa/ed-ticket-pricing/tree/develop/ed-backend/external-services) | Library | Library providing access to external APIs |
| [__inventory-sync__](https://github.com/dialexa/ed-ticket-pricing/tree/develop/ed-backend/inventory-sync) | Job | AKA "secondary-sync", job to synchronize secondary integrations with Event Dynamic |
| [__onboard__](https://github.com/dialexa/ed-ticket-pricing/tree/develop/ed-backend/onboard) | Job | Script to perform the initial data load for a new Event Dynamic client, also used for "seeding" the DB during during development |
| [__services__](https://github.com/dialexa/ed-ticket-pricing/tree/develop/ed-backend/services) | Library | Core library for accessing the Event Dynamic database |
| [__tdc-proxy__](https://github.com/dialexa/ed-ticket-pricing/tree/develop/ed-backend/tdc-proxy) | Service | Proxy API for accessing the Mets replicated database inside of the Mets VPN |
| [__ticket-fulfillment__](https://github.com/dialexa/ed-ticket-pricing/tree/develop/ed-backend/ticket-fulfillment) | Service | Service for managing ticket fulfilment |
| [__ticket-pricing-batch__](https://github.com/dialexa/ed-ticket-pricing/tree/develop/ed-backend/ticket-pricing-batch) | Job | Job to generate ticket prices and push those prices to integrations |
| [__ticket-sync__](https://github.com/dialexa/ed-ticket-pricing/tree/develop/ed-backend/ticket-pricing-batch) | Job | AKA "primary-sync", job to synchronize Event Dynamic with the primary integration |

### Play Framework

The `api` is built using the [playframework](https://www.playframework.com/)

### Swagger Docs

The swagger docs are hosted on `http://localhost:9000/docs`

## Testing

### Running Tests

The suggested way to run tests is through the ScalaTests run configurations in IntelliJ.

You can edit the default run configuration or make a new one to test a specific class for example:

1. Edit Configurations > Scala Tests
2. Change "Test kind:" to "Class"
3. Make sure you have the right module selected by changing "Use classpath and SDK of modules:"
4. Change "Test Class:" (those three dots let you search for a class!)

## FAQs

**How do I resolve the "No Scala SDK in module" message in IntelliJ IDE?**

When this message appears, click on the "Setup Scala SDK" button on the right side of the message container and select `scala-sdk-2.**.*`. If this list appears empty ensure that you have installed sbt, added the Scala plugin to Intellij, _and_ that the project was imported using the "Import project from external model" > "sbt" option.
