package com.eventdynamic.api.controllers

import com.eventdynamic.api.models.{AuthApiModel, Message, UserClientApiModel}
import com.eventdynamic.services.{ClientService, UserClientService, UserService}
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.util.{Credentials, PasswordHasherRegistry}
import com.mohiva.play.silhouette.api.{Environment, _}
import com.mohiva.play.silhouette.impl.providers._
import io.swagger.annotations._
import play.api._
import play.api.i18n.I18nSupport
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._
import utils.silhouette.{DefaultEnv, EDIdentityService, UserClientIdentity, WithClient}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/auth")
class AuthController @Inject()(
  controllerComponents: ControllerComponents,
  silhouette: Silhouette[DefaultEnv],
  edIdentityService: EDIdentityService,
  clientService: ClientService,
  userService: UserService,
  userClientService: UserClientService,
  authInfoRepository: AuthInfoRepository,
  credentialsProvider: CredentialsProvider,
  passwordHasherRegistry: PasswordHasherRegistry,
  conf: Configuration
)(implicit ec: ExecutionContext)
    extends AbstractController(controllerComponents)
    with I18nSupport
    with AuthApiModel
    with UserClientApiModel
    with Message {

  // API view of the User, implicitly used by Json.toJson
  def env: Environment[DefaultEnv] = silhouette.env

  /**
    * Sign In - Authenticates User with Email Password
    *
    * @return User Object
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ User info }"),
      new ApiResponse(code = 400, message = "Bad Request"),
      new ApiResponse(code = 404, message = "Could not find user")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "Email and Password",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.SignInInfo",
        paramType = "body"
      )
    )
  )
  def signIn = silhouette.UnsecuredAction.async(parse.tolerantJson) { implicit request =>
    request.body.validate[SignInApiModel] match {
      case s: JsSuccess[SignInApiModel] => {
        credentialsProvider
          .authenticate(Credentials(s.value.email, s.value.password))
          .flatMap { loginInfo =>
            edIdentityService
              .retrieve(loginInfo)
              .flatMap {
                case Some(userClient) => {
                  for {
                    authenticator <- env.authenticatorService.create(loginInfo)
                    cookie <- env.authenticatorService.init(authenticator)
                    defaultClientId <- userClientService.getDefaultClientIdForUserById(
                      userClient.user.id
                    )
                    updatedUser <- userService.setActiveClient(
                      userClient.user.id.get,
                      defaultClientId.get
                    )
                    result <- env.authenticatorService.embed(
                      cookie,
                      Ok(Json.toJson(UserClientIdentity(updatedUser.get, userClient.clientIds)))
                    )
                  } yield {
                    result
                  }
                }
                case None => Future.successful(NotFound(message("Could Not Find User")))
              }
          }
          .recoverWith {
            case _ => Future.successful(BadRequest(message("Incorrect Email or Password")))
          }
      }
      case e: JsError => {
        Future.successful(BadRequest(message("Invalid request body")))
      }
    }
  }

  /**
    * Is Authenticated
    * Checks if user is authenticated
    *
    * @return Ok or Unauthorized
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ User info }"),
      new ApiResponse(code = 401, message = "Unauthenticated")
    )
  )
  def isAuthenticated = silhouette.UserAwareAction { implicit request =>
    request.identity match {
      case Some(user) => Ok(Json.toJson(user))
      case None       => Unauthorized
    }
  }

  /**
    * Logout - Logs User Out
    *
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "Logged Out"),
      new ApiResponse(code = 401, message = "Unauthenticated")
    )
  )
  def logout = silhouette.SecuredAction.async { implicit request =>
    env.authenticatorService.discard(request.authenticator, Ok(message("Logged Out")))
  }
}
