package com.eventdynamic.api.controllers

import com.eventdynamic.{IntegrationServiceBuilder, IpService, SttpHelper}
import com.eventdynamic.api.models.{
  BuyerTypeBulkModification,
  BuyerTypeModel,
  BuyerTypeModelFormat,
  Message
}
import com.eventdynamic.models.TDCClientIntegrationConfig
import com.eventdynamic.services.{
  ClientIntegrationService,
  EventService,
  ProVenuePricingRuleService
}
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations.{Api, ApiImplicitParam, ApiImplicitParams, ApiParam}
import play.api.libs.json.Json
import play.api.mvc._
import utils.silhouette.DefaultEnv

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/buyerTypes")
class BuyerTypesController @Inject()(
  cc: ControllerComponents,
  eventService: EventService,
  clientIntegrationService: ClientIntegrationService,
  integrationServiceBuilder: IntegrationServiceBuilder,
  proVenuePricingRuleService: ProVenuePricingRuleService,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with BuyerTypeModelFormat
    with Message
    with TDCClientIntegrationConfig.format {

  private def getForwardedFromRequest(headers: Headers): Future[String] = {
    IpService.getMyInternalIP.map(ipResult => {
      Seq(headers.get("X-Forwarded-For"), ipResult)
        .collect { case Some(part) => part }
        .mkString(", ")
    })
  }

  def list(
    @ApiParam(
      value = "ProVenue Buyer Type Status",
      required = false,
      example = "ACTIVE",
      defaultValue = "BOTH"
    ) status: Option[String]
  ): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    val clientId = request.identity.user.activeClientId
    val name = "Tickets.com"

    for {
      clientIntegration <- clientIntegrationService
        .getByName(clientId, name, false)
        .map {
          case Some(clientIntegration) => clientIntegration
          case None                    => throw new Exception(s"No Tickets.com integration for client: $clientId")
        }
      events <- eventService.getAll(Some(clientId), isAdmin = false)
      buyerTypes <- integrationServiceBuilder
        .buildTdcReplicatedProxyService(clientIntegration.config[TDCClientIntegrationConfig])
        .listBuyerTypes(events.map(_.primaryEventId.get))
        .map(SttpHelper.handleResponse)
    } yield {
      val response =
        Json
          .toJson(
            buyerTypes.map(
              bt =>
                BuyerTypeModel(
                  bt.id.toString,
                  bt.publicDescription,
                  bt.code,
                  bt.active,
                  bt.isInPriceStructure
              )
            )
          )
      Ok(response)
    }
  }
}
