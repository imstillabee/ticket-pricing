package com.eventdynamic.api.controllers

import com.eventdynamic.api.models.{
  ClientIntegrationApiModel,
  Message,
  UpdateClientIntegrationApiModel
}
import com.eventdynamic.services.{ClientIntegrationService, SecondaryPricingRulesService}
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations.{Api, _}
import play.api.libs.json._
import play.api.mvc.{AbstractController, Action, ControllerComponents}
import utils.silhouette.DefaultEnv
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/clientIntegrations")
class ClientIntegrationsController @Inject()(
  cc: ControllerComponents,
  clientIntegrationService: ClientIntegrationService,
  secondaryPricingRulesService: SecondaryPricingRulesService,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with ClientIntegrationApiModel
    with Message {

  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Get an Integration by id
    *
    * @param id
    *
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ ClientIntegration }"),
      new ApiResponse(code = 404, message = "ClientIntegration not found")
    )
  )
  def getById(
    @ApiParam(value = "ClientIntegration ID", required = true, defaultValue = "1") id: Int
  ) =
    silhouette.SecuredAction.async { implicit request =>
      clientIntegrationService.getById(id).map {
        case Some(clientIntegrationRulesComposite) =>
          Ok(Json.toJson(clientIntegrationRulesComposite))
        case None => NotFound(message("ClientIntegration not found"))
      }
    }

  /**
    * Gets ClientIntegrations for a client
    *
    * @return List of ClientIntegrations
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ [ClientIntegrationComposite] }"),
      new ApiResponse(code = 403, message = "Access Denied")
    )
  )
  def get = silhouette.SecuredAction.async { implicit request =>
    val clientId = request.identity.user.activeClientId
    clientIntegrationService
      .getByClientId(clientId, false, false)
      .map(data => Ok(Json.toJson(data)))
  }

  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "ClientIntegration Updated"),
      new ApiResponse(code = 400, message = "Request Error"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "ClientIntegration not found"),
      new ApiResponse(
        code = 422,
        message = "Cannot add Secondary Pricing rule to a Primary Integration"
      ),
      new ApiResponse(code = 500, message = "Error processing request")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "Updated fields for ClientIntegration",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.UpdateClientIntegrationInfo",
        paramType = "body"
      )
    )
  )
  def update(
    @ApiParam(value = "ClientIntegration ID", required = true, defaultValue = "1") id: Int
  ): Action[JsValue] =
    silhouette.SecuredAction.async(parse.tolerantJson) { implicit request =>
      request.body.validate[UpdateClientIntegrationApiModel] match {
        case err: JsError => Future.successful(BadRequest(JsError.toJson(err)))
        case body: JsSuccess[UpdateClientIntegrationApiModel] => {

          val clientId = request.identity.user.activeClientId
          clientIntegrationService.getById(id).flatMap {
            case None => Future.successful(NotFound(message("ClientIntegration not found")))
            case Some(clientIntegration) if clientIntegration.clientId != clientId => {
              Future.successful(NotFound(message("ClientIntegration not found")))
            }
            case Some(clientIntegration) if clientIntegration.isPrimary => {
              Future.successful(
                UnprocessableEntity(
                  message("Cannot add Secondary Pricing rule to a Primary Integration")
                )
              )
            }
            case Some(clientIntegration) => {
              secondaryPricingRulesService
                .saveSettings(clientIntegration.id.get, body.value.percent, body.value.constant)
                .map(_ => Ok(message("ClientIntegration Updated")))
                .recover {
                  case error => {
                    logger.error(error.getMessage)
                    InternalServerError(message("Error processing request"))
                  }
                }
            }
          }
        }
      }
    }
}
