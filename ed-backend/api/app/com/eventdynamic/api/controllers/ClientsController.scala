package com.eventdynamic.api.controllers
import com.eventdynamic.api.models.{ClientApiModel, Message, UserClientApiModel}
import com.eventdynamic.models.Client
import com.eventdynamic.services.{ClientService, UserService}
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations._
import play.api.libs.json._
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import utils.silhouette.{Admin, DefaultEnv, UserClientIdentity, WithClient}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/clients")
class ClientsController @Inject()(
  cc: ControllerComponents,
  clientService: ClientService,
  userService: UserService,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with ClientApiModel
    with UserClientApiModel
    with Message {

  /**
    * Get Client - Gets a Client by id
    * @param id unique, auto-incremented & indexed identifier
    * @return client object
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ Client }"),
      new ApiResponse(code = 400, message = "Request error"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "Client not found")
    )
  )
  def getById(
    @ApiParam(value = "Id used to look up client", required = true, defaultValue = "2") id: Int
  ) =
    silhouette.SecuredAction(WithClient(id)).async { implicit request =>
      clientService.getById(id).map {
        case Some(client) => Ok(Json.toJson(client))
        case None         => NotFound(message("Client does not exist or could not be found"))
      }
    }

  /**
    * Get - Returns array of all clients for user
    */
  def get: Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    clientService.getAll(request.identity.user.id.get).map(clients => Ok(Json.toJson(clients)))
  }

  /**
    * set client to active
    * Checks if user is authenticated
    *
    * @param id
    * @return Ok or Unauthorized
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ User info }"),
      new ApiResponse(code = 400, message = "Bad Request"),
      new ApiResponse(code = 401, message = "Unauthenticated")
    )
  )
  def setActive(@ApiParam(value = "client id to activate", required = true) id: Int) =
    silhouette.SecuredAction(WithClient(id)).async { implicit request =>
      {
        val userClient = request.identity
        userService.setActiveClient(userClient.user.id.get, id).map {
          case Some(updatedUser) =>
            Ok(Json.toJson(UserClientIdentity(updatedUser, userClient.clientIds)))
          case _ => BadRequest(message(s"Unable to update user to active client ${id}"))
        }
      }
    }
}
