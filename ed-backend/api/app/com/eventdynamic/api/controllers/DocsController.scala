package com.eventdynamic.api.controllers

import com.eventdynamic.services.UserService
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.util.PasswordHasherRegistry
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import io.swagger.annotations._
import play.api.Configuration
import play.api.i18n.I18nSupport
import play.api.mvc._
import utils.silhouette.DefaultEnv

import scala.concurrent.Future

@Singleton
@Api(value = "/docs")
class DocsController @Inject()(
  controllerComponents: ControllerComponents,
  silhouette: Silhouette[DefaultEnv],
  userService: UserService,
  authInfoRepository: AuthInfoRepository,
  credentialsProvider: CredentialsProvider,
  passwordHasherRegistry: PasswordHasherRegistry,
  config: Configuration
) extends AbstractController(controllerComponents)
    with I18nSupport {

  @ApiResponses(Array(new ApiResponse(code = 200, message = "Redirects to documentation.")))
  def redirectDocs() = silhouette.UserAwareAction.async { implicit request =>
    val baseUrl = config.get[String]("application.baseUrl")
    Future.successful(
      Redirect(
        url = "/assets/lib/swagger-ui/index.html",
        queryString = Map("url" -> Seq(baseUrl + "swagger.json"))
      )
    )
  }
}
