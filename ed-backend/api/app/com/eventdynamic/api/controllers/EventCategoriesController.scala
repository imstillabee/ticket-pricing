package com.eventdynamic.api.controllers

import com.google.inject.{Inject, Singleton}
import com.eventdynamic.api.models.Message
import com.eventdynamic.services.EventCategoryService
import com.eventdynamic.api.models.EventCategoryModelFormat
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations.Api
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc._
import utils.silhouette.DefaultEnv

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/eventCategories")
class EventCategoriesController @Inject()(
  cc: ControllerComponents,
  eventCategoryService: EventCategoryService,
  silhouette: Silhouette[DefaultEnv],
  config: Configuration
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with EventCategoryModelFormat
    with Message {

  def list(): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    eventCategoryService
      .getAllForClient(request.identity.user.activeClientId)
      .map(eventCategories => Ok(Json.toJson(eventCategories)))
  }
}
