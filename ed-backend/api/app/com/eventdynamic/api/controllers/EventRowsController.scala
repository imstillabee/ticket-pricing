package com.eventdynamic.api.controllers

import com.eventdynamic.api.models.{EventRowApiModel, Message}
import com.eventdynamic.services.{EventSeatService, EventService}
import com.eventdynamic.transactions.EventRowsTransaction
import com.mohiva.play.silhouette.api.Silhouette
import com.google.inject.{Inject, Singleton}
import io.swagger.annotations._
import play.api.Configuration
import play.api.i18n.I18nSupport
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import utils.silhouette.{Admin, DefaultEnv, WithEvent}

@Singleton
@Api(value = "/eventRows")
class EventRowsController @Inject()(
  controllerComponents: ControllerComponents,
  eventRowsTransaction: EventRowsTransaction,
  eventSeatService: EventSeatService,
  eventService: EventService,
  silhouette: Silhouette[DefaultEnv],
  config: Configuration
)(implicit ec: ExecutionContext)
    extends AbstractController(controllerComponents)
    with I18nSupport
    with EventRowApiModel
    with Message {

  /** GET EventRows - Gets all EventRows with available (unsold) seats for a given EventId
    *
    * @param eventId
    * @return collection of EventRows
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "[{ EventRow }]"),
      new ApiResponse(code = 404, message = "EventRows not found")
    )
  )
  def getAll(
    @ApiParam(
      value = "eventId used to look up unsold inventory",
      required = true,
      defaultValue = "4"
    ) eventId: Int
  ) = silhouette.SecuredAction(WithEvent(eventId, eventService)).async { implicit request =>
    eventRowsTransaction
      .getAllByEvent(eventId)
      .map {
        case Seq()     => NotFound(message("No EventRows were found for this event"))
        case eventRows => Ok(Json.toJson(eventRows))
      }
  }

  /** Update EventRows - Updates EventSeats records based on ids passed from client
    *
    * - NOTE: BulkEventSeatUpdateApiModel passed from client contains three attributes:
    * -- eventSeatIds: Seq[Int]
    * -- isListed: Option[Boolean] true|false if isListed should be updated,
    * ------ null here would indicate this is to be an overridePrice change
    * -- overridePrice: Option[BigDecimal] null here would be saved as NULL in db
    *
    * @return number of updatedEventSeat records
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "EventSeats updated"),
      new ApiResponse(code = 400, message = "Request error"),
      new ApiResponse(code = 404, message = "EventRow(s) not found")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "EventSeats Bulk Update Info",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.EventRowUpdateInfo",
        paramType = "body"
      )
    )
  )
  def update: Action[JsValue] = silhouette.SecuredAction.async(parse.tolerantJson) {
    implicit request =>
      request.body.validate[BulkEventSeatUpdateApiModel] match {
        case uo: JsSuccess[BulkEventSeatUpdateApiModel] =>
          eventSeatService
            .bulkUpdate(uo.get.eventSeatIds, uo.get.overridePrice, uo.get.isListed)
            .map {
              case 0 => NotAcceptable(message("There was a problem - No EventSeats were updated"))
              case n => Ok(message(s"$n EventSeats were updated"))
            }
        case e: JsError => Future.successful(BadRequest(message(s"Invalid request body - $e")))
      }
  }

}
