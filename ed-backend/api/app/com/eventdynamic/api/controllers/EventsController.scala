package com.eventdynamic.api.controllers

import java.sql.Timestamp
import java.time.{Instant, LocalDateTime}
import java.time.format.DateTimeFormatter

import com.eventdynamic.api.models._
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models.{
  ClientIntegrationComposite,
  Event,
  JobPriorityType,
  JobTriggerType,
  JobType,
  ScheduledJob
}
import com.eventdynamic.modelserver.ModelServerService
import com.eventdynamic.queues.ScheduledJobQueue
import com.eventdynamic.services.{
  ClientIntegrationService,
  ClientService,
  EventService,
  JobConfigService,
  PricingPreviewSectionService,
  VenueService
}
import com.eventdynamic.transactions.{InventoryStats, InventoryStatsTransaction}
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations._
import org.slf4j.LoggerFactory
import play.api.libs.json._
import play.api.mvc._
import utils.silhouette.{Admin, DefaultEnv, WithEvent}

import scala.concurrent.{ExecutionContext, Future}
import scala.math.BigDecimal.RoundingMode
import scala.util.{Failure, Success, Try}

case class Row(name: String, timestamp: Option[Timestamp] = None)
case class InventoryStatsResponse(
  totalInventory: Int,
  soldIndividualInventory: Int,
  unsoldInventory: Int,
  revenue: BigDecimal
)

@Singleton
@Api(value = "/events")
class EventsController @Inject()(
  cc: ControllerComponents,
  eventService: EventService,
  clientService: ClientService,
  pricingPreviewSectionService: PricingPreviewSectionService,
  clientIntegrationService: ClientIntegrationService,
  jobConfigService: JobConfigService,
  scheduledJobQueue: ScheduledJobQueue,
  inventoryStatsTransaction: InventoryStatsTransaction,
  venueService: VenueService,
  modelServerService: ModelServerService,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with EventModelFormat
    with Message
    with EventPricesModelFormat
    with PricingPreviewFormat {

  private val logger = LoggerFactory.getLogger(this.getClass)
  private val EVENT_SCORE_SCALE = 2
  private val SPRING_SCALE = 4

  private def validateModifiers(
    clientId: Int,
    event: Event,
    eventScoreModifier: Double,
    springModifier: Double
  ): Future[(Boolean, String)] = {
    for {
      clientOption <- clientService.getById(clientId)
      predictedSpring <- fetchPredictedSpring(
        clientId,
        event.copy(eventScoreModifier = eventScoreModifier)
      )
    } yield {
      clientOption match {
        case None => throw new Exception("Client not found")
        case Some(client) =>
          var errorMessage = ""

          // It should be safe to use .get for event score here because a pricing job should
          // have ran at least once before a user is able to see the events overview page
          val finalEventScore = event.eventScore.get + eventScoreModifier
          val finalSpring = predictedSpring.get + springModifier

          var validEventScore = finalEventScore >= client.eventScoreFloor
          var validSpring = finalSpring >= client.springFloor

          if (client.eventScoreCeiling.isDefined) {
            validEventScore = finalEventScore >= client.eventScoreFloor && finalEventScore <= client.eventScoreCeiling.get
          }

          if (client.springCeiling.isDefined) {
            validSpring = finalSpring >= client.springFloor && finalSpring <= client.springCeiling.get
          }

          if (!validEventScore) {
            val eventScoreErrorMessage =
              getModifierErrorMessage(
                "Event Score",
                client.eventScoreFloor,
                client.eventScoreCeiling,
                finalEventScore,
                EVENT_SCORE_SCALE
              )
            logger.info(eventScoreErrorMessage)
            errorMessage += eventScoreErrorMessage
          }

          if (!validSpring) {
            val springErrorMessage =
              getModifierErrorMessage(
                "Spring",
                client.springFloor,
                client.springCeiling,
                finalSpring,
                SPRING_SCALE
              )
            logger.info(springErrorMessage)
            errorMessage += springErrorMessage
          }
          (validEventScore && validSpring, errorMessage)
      }
    }
  }

  private def getInventoryStatResponse(
    maybeInventoryStat: Option[InventoryStats],
    clientIntegration: Option[ClientIntegrationComposite]
  ): InventoryStatsResponse = {
    val integrationsWithHoldCodes = Set("Tickets.com")
    maybeInventoryStat match {
      case Some(stats) =>
        val unsold =
          if (clientIntegration.isDefined && integrationsWithHoldCodes.contains(
                clientIntegration.get.name
              ))
            stats.unsold
          else stats.remaining
        InventoryStatsResponse(stats.total, stats.soldIndividual, unsold, stats.revenue)
      case None => InventoryStatsResponse(0, 0, 0, BigDecimal(0))
    }
  }

  private def getModifierErrorMessage(
    modifier: String,
    floor: Double,
    ceiling: Option[Double],
    finalValue: Double,
    scale: Int
  ): String = {
    if (finalValue < floor)
      s"Final $modifier cannot be below ${BigDecimal(floor)
        .setScale(scale, RoundingMode.HALF_UP)} "
    else
      s"Final $modifier cannot exceed ${BigDecimal(ceiling.get)
        .setScale(scale, RoundingMode.HALF_UP)} "
  }

  private def scheduleJob(clientId: Int, eventId: Int, jobType: JobType): Future[ScheduledJob] = {
    for {
      jobConfig <- jobConfigService.getRawConfig(clientId, jobType).map {
        case Some(config) => config
        case None         => throw new Exception(s"No job config for client: $clientId, jobType: $jobType")
      }
      updateRes <- scheduledJobQueue
        .add(
          eventId,
          jobConfig.id.get,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )
    } yield updateRes
  }

  private def fetchPredictedSpring(clientId: Int, event: Event): Future[Option[Double]] = {
    event.eventScore match {
      case Some(eventScore) =>
        modelServerService
          .getAutomatedSpringValue(
            clientId.toString,
            eventScore + event.eventScoreModifier,
            event.timestamp
          )
          .map(res => Some(res.springValue * 100))
          .recover {
            case e: Throwable =>
              logger.error("Error fetching predicted spring value from the Model Server", e)
              None
          }
      case None => Future.successful(None)
    }
  }

  private def eventSpring(clientId: Int, event: Event): Future[Option[Double]] = {
    if (event.timestamp.get.toInstant.isBefore(Instant.now()))
      Future.successful(event.spring)
    else
      fetchPredictedSpring(clientId, event)
  }

  /**
    * Get Event - Gets an Event by id
    *
    * @param id event id
    * @return event object
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ Event }"),
      new ApiResponse(code = 400, message = "Request error"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "Event not found")
    )
  )
  def getById(
    @ApiParam(value = "Id used to look up event", required = true, defaultValue = "100") id: Int
  ): Action[AnyContent] =
    silhouette
      .SecuredAction(Admin() || WithEvent(id, eventService))
      .async { implicit request =>
        val clientId = request.identity.user.activeClientId
        val jobType = JobType.Pricing
        eventService.getById(id).flatMap {
          case Some(event) =>
            for {
              iStats <- inventoryStatsTransaction
                .getInventory(clientId, Some(id), None)
              scheduledJobComposite <- scheduledJobQueue.getLatestForEvent(id, jobType, None)
              clientIntegration <- clientIntegrationService
                .getByClientId(clientId, onlyActive = true, onlyPrimary = true)
                .map(_.headOption)
              venue <- venueService.getById(event.venueId)
              spring <- eventSpring(clientId, event)
            } yield {
              val inventoryStatResponse = getInventoryStatResponse(iStats, clientIntegration)
              val timeZone = venue.map(_.timeZone)

              val eventWithInventory = EventModel(
                id = event.id,
                primaryEventId = event.primaryEventId,
                modifiedAt = event.modifiedAt,
                createdAt = event.createdAt,
                name = event.name,
                timestamp = event.timestamp,
                clientId = event.clientId,
                venueId = event.venueId,
                eventCategoryId = event.eventCategoryId,
                seasonId = event.seasonId,
                isBroadcast = event.isBroadcast,
                percentPriceModifier = event.percentPriceModifier,
                totalInventory = inventoryStatResponse.totalInventory,
                soldInventory = inventoryStatResponse.soldIndividualInventory,
                unsoldInventory = inventoryStatResponse.unsoldInventory,
                eventScore = event.eventScore,
                eventScoreModifier = event.eventScoreModifier,
                spring = spring,
                springModifier = event.springModifier,
                scheduledJob = scheduledJobComposite.map(_.scheduledJob),
                revenue = inventoryStatResponse.revenue,
                timeZone = timeZone
              )

              Ok(Json.toJson(eventWithInventory))
            }
          case None =>
            Future.successful(NotFound(message("Event does not exist or could not be found")))
        }
      }

  /**
    * Update Event - Updates an Event
    *
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "Event Updated"),
      new ApiResponse(code = 400, message = "Request error"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "Event not found"),
      new ApiResponse(code = 409, message = "Event Already Exists")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "Updated Event Object",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.EventCreateInfo",
        paramType = "body"
      )
    )
  )
  def update(
    @ApiParam(value = "Id used to look up event", required = true, defaultValue = "100") id: Int
  ): Action[JsValue] = silhouette.SecuredAction(Admin()).async(parse.tolerantJson) {
    implicit request =>
      request.body.validate[CreateEventModel] match {
        case s: JsSuccess[CreateEventModel] =>
          val formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME

          Try(LocalDateTime.parse(s.value.timestamp, formatter)) match {
            case Success(value) =>
              eventService
                .updateUnique(
                  id,
                  None,
                  s.value.name,
                  Some(Timestamp.valueOf(value)),
                  s.value.clientId,
                  s.value.venueId,
                  s.value.eventCategoryId,
                  s.value.seasonId,
                  s.value.percentPriceModifier,
                  s.value.eventScoreModifier.getOrElse(0),
                  s.value.springModifier.getOrElse(0)
                )
                .map {
                  case NotFoundResponse              => NotFound(message("Event does not exist"))
                  case DuplicateResponse             => Conflict(message("Event Already Exists"))
                  case SuccessResponse(event: Event) => Ok(Json.toJson(event))
                }
                .recover {
                  case _ => NotFound(message("Invalid clientId or venueId"))
                }
            case Failure(_) => Future.successful(BadRequest(message("Invalid Date or Time")))
          }
        case _: JsError =>
          Future.successful(BadRequest(message("Invalid request body")))
      }
  }

  /** Get - Gets all events by season id with some constraints on non admin users
    *
    * @param seasonId season id
    * @return List of Events
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "[{ Event }]"),
      new ApiResponse(code = 400, message = "Request error"),
      new ApiResponse(code = 404, message = "No events for season")
    )
  )
  def get(
    @ApiParam(value = "Season Id to get events for", required = false, defaultValue = "1") seasonId: Option[
      Int
    ]
  ): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    val clientId = request.identity.user.activeClientId
    val isAdmin = request.identity.user.isAdmin
    val jobType = JobType.Pricing
    seasonId match {
      case Some(s) =>
        // TODO: move inventory calculation out of the controller
        for {
          events <- eventService
            .getAllBySeason(s, clientId, isAdmin)
          iStatsSeq <- inventoryStatsTransaction
            .getInventoryForEvents(clientId, events.map(e => e.id.get), None)
          clientIntegration <- clientIntegrationService
            .getByClientId(clientId, onlyActive = true, onlyPrimary = true)
            .map(_.headOption)
          eventsWithInventory <- Future.sequence(events.map(event => {
            for {
              scheduledJobComposite <- scheduledJobQueue
                .getLatestForEvent(event.id.get, jobType, None)
              venue <- venueService.getById(event.venueId)
              spring <- eventSpring(clientId, event)
            } yield {
              val maybeInventoryStats = iStatsSeq.find(stats => stats.eventId == event.id)
              val inventoryStat =
                getInventoryStatResponse(maybeInventoryStats, clientIntegration)

              val timeZone = venue.map(_.timeZone)
              EventModel(
                id = event.id,
                primaryEventId = event.primaryEventId,
                modifiedAt = event.modifiedAt,
                createdAt = event.createdAt,
                name = event.name,
                timestamp = event.timestamp,
                clientId = event.clientId,
                venueId = event.venueId,
                eventCategoryId = event.eventCategoryId,
                seasonId = event.seasonId,
                isBroadcast = event.isBroadcast,
                percentPriceModifier = event.percentPriceModifier,
                totalInventory = inventoryStat.totalInventory,
                soldInventory = inventoryStat.soldIndividualInventory,
                unsoldInventory = inventoryStat.unsoldInventory,
                eventScore = event.eventScore,
                eventScoreModifier = event.eventScoreModifier,
                spring = spring,
                springModifier = event.springModifier,
                scheduledJob = scheduledJobComposite.map(_.scheduledJob),
                revenue = inventoryStat.revenue,
                timeZone = timeZone
              )
            }
          }))
        } yield Ok(Json.toJson(eventsWithInventory))
      case _ =>
        eventService.getAll(Some(clientId), isAdmin).map(events => Ok(Json.toJson(events)))
    }
  }

  /**
    * Update Event - Updates an Event Percent Price Modifier
    *
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ Event }"),
      new ApiResponse(code = 400, message = "Invalid request body"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "Event not found")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "Percent Price Modifier",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.UpdatePercentPriceModifier",
        paramType = "body"
      )
    )
  )
  def updatePercentPriceModifier(
    @ApiParam(value = "Id used to look up event", required = true, defaultValue = "100") id: Int
  ): Action[JsValue] =
    silhouette.SecuredAction(WithEvent(id, eventService)).async(parse.tolerantJson) {
      implicit request =>
        request.body.validate[UpdatePriceModifierModel] match {
          case s: JsSuccess[UpdatePriceModifierModel] =>
            eventService.getById(id).flatMap {
              case None => Future.successful(NotFound(message("Event not found")))
              case Some(event) =>
                eventService
                  .update(event.copy(percentPriceModifier = s.value.percentPriceModifier))
                  .map {
                    case NotFoundResponse =>
                      NotFound(message("Event not found"))
                    case SuccessResponse(updatedEvent: Event) =>
                      Ok(Json.toJson(updatedEvent))
                  }
            }
          case _: JsError =>
            Future.successful(BadRequest(message("Invalid request body")))

        }
    }

  /**
    * Update Event - Updates an Event Percent Price Modifier
    *
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ Event }"),
      new ApiResponse(code = 400, message = "Invalid request body"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "Event not found")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "Event Score Override",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.UpdateAdminModifier",
        paramType = "body"
      )
    )
  )
  def updateAdminModifier(
    @ApiParam(value = "Id used to look up event", required = true, defaultValue = "100") id: Int
  ): Action[JsValue] =
    silhouette.SecuredAction(Admin()).async(parse.tolerantJson) { implicit request =>
      request.body.validate[UpdateAdminModifierModel] match {
        case s: JsSuccess[UpdateAdminModifierModel] =>
          eventService.getById(id).flatMap {
            case None => Future.successful(NotFound(message("Event not found")))
            case Some(event) =>
              val clientId = request.identity.user.activeClientId
              validateModifiers(clientId, event, s.value.eventScoreModifier, s.value.springModifier)
                .flatMap(res => {
                  val (isValid, errorMessage) = res
                  if (isValid)
                    eventService
                      .update(
                        event.copy(
                          eventScoreModifier = s.value.eventScoreModifier,
                          springModifier = s.value.springModifier
                        )
                      )
                      .flatMap {
                        case NotFoundResponse =>
                          Future.successful(NotFound(message("Event not found")))
                        case SuccessResponse(updatedEvent: Event) =>
                          logger.info(
                            s"Changing modifiers for event $id ES ${event.eventScoreModifier} -> ${s.value.eventScoreModifier} and spring ${event.springModifier} -> ${s.value.springModifier}"
                          )
                          // Schedule job, but don't wait for the response
                          scheduleJob(event.clientId, event.id.get, JobType.Pricing).andThen {
                            case Failure(error) => logger.error(s"Error scheduling job", error)
                          }
                          for {
                            predictedSpring <- fetchPredictedSpring(clientId, updatedEvent)
                          } yield Ok(Json.toJson(updatedEvent.copy(spring = predictedSpring)))
                      } else Future.successful(BadRequest(message(errorMessage)))
                })
          }
        case _: JsError =>
          Future.successful(BadRequest(message("Invalid request body")))
      }
    }

  /**
    * Toggle isBroadcast - Toggles the Boolean isBroadcast for a specific event
    * @param id event id
    * @return updated isBroadcast and Timestamp for event
    */

  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "Event Updated"),
      new ApiResponse(code = 400, message = "Invalid request body"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "Event does not exist or could not be found")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "Updated isBroadcast field",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.ToggleIsBroadcast",
        paramType = "body"
      )
    )
  )
  def toggleIsBroadcast(
    @ApiParam(value = "Event Id", required = true, defaultValue = "1") id: Int
  ): Action[JsValue] =
    silhouette.SecuredAction(Admin() || WithEvent(id, eventService)).async(parse.tolerantJson) {
      implicit request =>
        request.body.validate[ToggleIsBroadcastModel] match {
          case s: JsSuccess[ToggleIsBroadcastModel] =>
            eventService.toggleIsBroadcast(id, s.value.isBroadcast).map {
              case Some(result) =>
                Ok(Json.obj("isBroadcast" -> result._1, "modifiedAt" -> result._2))
              case None => NotFound(message("Event not found"))
            }
          case _ => Future.successful(BadRequest(message("Invalid request body")))
        }
    }

  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "Prices for Event"),
      new ApiResponse(code = 400, message = "Invalid request body"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "Event does not exist or could not be found")
    )
  )
  def getPricesForEvent(
    @ApiParam(value = "Id used to look up event", required = true) id: Int,
    @ApiParam(value = "Event score to use or blank for what's on event", required = false) eventScore: Option[
      Double
    ],
    @ApiParam(value = "Spring to use or blank for what's on event", required = false) spring: Option[
      Double
    ]
  ): Action[AnyContent] =
    silhouette.SecuredAction(WithEvent(id, eventService)).async { implicit request =>
      {
        eventService.getById(id).flatMap {
          case None => Future.successful(NotFound(message("Unable to find event")))
          case Some(event) =>
            if (eventScore.isEmpty && event.eventScore.isEmpty)
              Future.successful(BadRequest(message("Event score not defined.")))
            else if (spring.isEmpty && event.spring.isEmpty)
              Future.successful(BadRequest(message("Spring not defined.")))
            else {
              val finalEventScore =
                eventScore.getOrElse(event.eventScore.get + event.eventScoreModifier)
              val finalSpring = spring.getOrElse(event.spring.get + event.springModifier)

              modelServerService
                .getAllPrices(event.clientId.toString, finalEventScore, finalSpring)
                .map(
                  resp =>
                    Ok(
                      Json.toJson(
                        EventPricesModel(
                          resp.prices.map(row => EventPrice(row.section, row.row, row.price))
                        )
                      )
                  )
                )
                .recover {
                  case e =>
                    logger.error("Failed to query all prices for event", e)
                    InternalServerError(message("Error querying for prices."))
                }
            }
        }
      }
    }

  def getPricingPreviewForEvent(
    @ApiParam(value = "Id used to look up event", required = true) id: Int,
    @ApiParam(value = "Event score to use or blank for what's on event", required = true) eventScore: Double,
    @ApiParam(value = "Spring to use or blank for what's on event", required = true) spring: Double
  ): Action[AnyContent] =
    silhouette.SecuredAction(WithEvent(id, eventService)).async { implicit request =>
      {
        val clientId = request.identity.user.activeClientId

        val future = for {
          prices <- modelServerService
            .getAllPrices(clientId.toString, eventScore, spring)
            .map(_.prices)
          previewSections <- pricingPreviewSectionService.getForClient(clientId)
        } yield {
          val pricesForSection = prices.filter(previewSections.map(_.section) contains _.section)
          val sectionStats = pricesForSection.groupBy(_.section).mapValues(_.map(_.price).sorted)

          val preview = PricingPreview(
            event = PricingStat.of(prices.map(_.price)),
            sections = sectionStats.mapValues(PricingStat.of)
          )

          Ok(Json.toJson(preview))
        }

        future.recover {
          case e =>
            logger.error("Unknown error getting prices", e)
            InternalServerError(message("Unknown error getting prices"))
        }
      }
    }

  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ springValue }"),
      new ApiResponse(code = 400, message = "Invalid request body"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "Event not found")
    )
  )
  def getAutomatedSpringValue(
    @ApiParam(value = "Id used to look up event", required = true) id: Int,
    @ApiParam(value = "Event score on event", required = true) eventScore: Double
  ): Action[AnyContent] =
    silhouette.SecuredAction(WithEvent(id, eventService)).async { implicit request =>
      {
        val clientId = request.identity.user.activeClientId
        eventService.getById(id).flatMap {
          case Some(event) =>
            modelServerService
              .getAutomatedSpringValue(clientId.toString, eventScore, event.timestamp)
              .map(response => Ok(Json.toJson(response.springValue * 100)))
              .recover {
                case error: Throwable =>
                  logger.error(s"Error connecting to Model Server", error)
                  InternalServerError(Json.toJson(error.getMessage))
              }
          case None => Future.successful(NotFound(message("Event not found")))
        }
      }
    }
}
