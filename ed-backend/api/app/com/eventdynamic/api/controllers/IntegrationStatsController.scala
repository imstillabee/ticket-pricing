package com.eventdynamic.api.controllers

import com.eventdynamic.api.models.{IntegrationApiModel, Message}
import com.eventdynamic.services._
import com.eventdynamic.transactions.IntegrationStatsTransaction
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations._
import play.api.libs.json._
import play.api.mvc._
import utils.silhouette.DefaultEnv

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/integrationStats")
class IntegrationStatsController @Inject()(
  cc: ControllerComponents,
  seasonService: SeasonService,
  eventService: EventService,
  integrationStatsTransaction: IntegrationStatsTransaction,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with IntegrationApiModel
    with Message {

  /**
    * Get Integrations For Season - Gets Integrations with stats for Season by Id
    * @param seasonId - Season Id
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "[{ Integration }]"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 404, message = "Season does not exist"),
      new ApiResponse(code = 404, message = "Event does not exist")
    )
  )
  def getIntegrationsStats(
    @ApiParam(value = "Season Id to return", required = false, defaultValue = "1") seasonId: Option[
      Int
    ],
    @ApiParam(value = "Event Id to return", required = false, defaultValue = "1") eventId: Option[
      Int
    ]
  ): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    (seasonId, eventId) match {
      case (_, Some(id)) =>
        eventService.getById(id).flatMap {
          case Some(event) if event.clientId != request.identity.user.activeClientId =>
            Future.successful(NotFound("Event Does Not Exist"))
          case Some(event) if event.seasonId.contains(seasonId.getOrElse(event.seasonId.get)) =>
            getStats(request.identity.user.activeClientId, event.seasonId, Some(id))
          case Some(_) =>
            Future.successful(NotFound(message("Event Does Not Exist in Season")))
          case _ => Future.successful(NotFound(message("Event Does Not Exist")))
        }
      case (Some(id), _) =>
        seasonService
          .getById(seasonId.get, Some(request.identity.user.activeClientId), false)
          .flatMap {
            case Some(_) => getStats(request.identity.user.activeClientId, Some(id), None)
            case _       => Future.successful(NotFound(message("Season Does Not Exist")))
          }
      case _ => Future.successful(BadRequest(message("Invalid query parameters")))
    }
  }

  /**
    * Gets integration stats for season and event
    *
    * @param clientId
    * @param seasonId
    * @param eventId
    * @return
    */
  private def getStats(
    clientId: Int,
    seasonId: Option[Int],
    eventId: Option[Int]
  ): Future[Result] = {
    integrationStatsTransaction
      .getIntegrationTotals(clientId, seasonId, eventId)
      .map(stats => Ok(Json.toJson(stats)))
  }
}
