package com.eventdynamic.api.controllers

import io.swagger.annotations.Api
import com.eventdynamic.api.models.{IntegrationApiModel, Message}
import com.eventdynamic.services.{IntegrationService}
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations._
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents}
import utils.silhouette.{DefaultEnv}
import scala.concurrent.{ExecutionContext}

@Singleton
@Api(value = "/integrations")
class IntegrationsController @Inject()(
  cc: ControllerComponents,
  integrationService: IntegrationService,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with IntegrationApiModel
    with Message {

  /**
    * Get an Integration by id
    *
    * @param id
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ Integration }"),
      new ApiResponse(code = 404, message = "Integration not found")
    )
  )
  def getById(@ApiParam(value = "Integration ID", required = true, defaultValue = "1") id: Int) =
    silhouette.SecuredAction.async { implicit request =>
      integrationService.getById(id).map {
        case Some(integration) => Ok(Json.toJson(integration))
        case None              => NotFound(message("Integration not found"))
      }
    }
}
