package com.eventdynamic.api.controllers

import com.mohiva.play.silhouette.api.Silhouette
import com.google.inject.{Inject, Singleton}
import io.swagger.annotations._
import org.slf4j.LoggerFactory
import play.api.Configuration
import play.api.i18n.I18nSupport
import play.api.mvc._
import utils.silhouette.DefaultEnv

import scala.concurrent.Future

@Singleton
@Api(value = "/ping")
class PingController @Inject()(
  controllerComponents: ControllerComponents,
  silhouette: Silhouette[DefaultEnv],
  config: Configuration
) extends AbstractController(controllerComponents)
    with I18nSupport {

  private val logger = LoggerFactory.getLogger(this.getClass)

  @ApiResponses(Array(new ApiResponse(code = 200, message = "Health check.")))
  def ping() = silhouette.UserAwareAction.async { implicit request =>
    {
      logger.trace("> INSIDE Ping Service")
      Future.successful(Ok("pong"))
    }
  }
}
