package com.eventdynamic.api.controllers

import com.eventdynamic.api.models.{Message, PriceScaleApiModel}
import com.eventdynamic.services.PriceScaleService
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations.Api
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import utils.silhouette.DefaultEnv

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/priceScales")
class PriceScalesController @Inject()(
  cc: ControllerComponents,
  priceScaleService: PriceScaleService,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with PriceScaleApiModel
    with Message {

  def list(): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    priceScaleService
      .getAllForClient(request.identity.user.activeClientId)
      .map(priceScales => Ok(Json.toJson(priceScales)))
  }
}
