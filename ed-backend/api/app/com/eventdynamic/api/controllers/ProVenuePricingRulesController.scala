package com.eventdynamic.api.controllers

import com.eventdynamic.api.models.{
  Message,
  ProVenuePricingRuleModel,
  ProVenuePricingRuleModification
}
import com.eventdynamic.services.ProVenuePricingRuleService
import com.eventdynamic.utils.{NotFoundResponse, SuccessResponse}
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations._
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json._
import play.api.mvc._
import utils.silhouette.DefaultEnv

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/proVenuePricingRules")
class ProVenuePricingRulesController @Inject()(
  cc: ControllerComponents,
  proVenuePricingRuleService: ProVenuePricingRuleService,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with ProVenuePricingRuleModel
    with Message {

  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  private case class ConflictResponse(proVenuePricingRules: Seq[Int]) {
    def nonEmpty: Boolean = proVenuePricingRules.nonEmpty
  }
  implicit private lazy val conflictResponseFormat: Format[ConflictResponse] =
    Json.format[ConflictResponse]

  def list: Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    val clientId = request.identity.user.activeClientId
    proVenuePricingRuleService
      .getAll(clientId, false)
      .map(rules => Ok(Json.toJson(rules)))
  }

  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "New Pricing Rule Object",
        required = true,
        dataType = "com.eventdynamic.api.models.ProVenuePricingRuleModification",
        paramType = "body"
      )
    )
  )
  def create: Action[JsValue] =
    silhouette.SecuredAction.async(parse.tolerantJson) { implicit request =>
      val clientId = request.identity.user.activeClientId
      request.body.validate[ProVenuePricingRuleModification] match {
        case err: JsError => Future.successful(BadRequest(JsError.toJson(err)))
        case body: JsSuccess[ProVenuePricingRuleModification] =>
          checkForConflicts(None, clientId, body.value)
            .flatMap {
              case rules if rules.nonEmpty =>
                Future.successful(Conflict(Json.toJson(rules)))
              case _ =>
                proVenuePricingRuleService
                  .create(
                    clientId,
                    body.value.eventIds,
                    body.value.priceScaleIds,
                    body.value.externalBuyerTypeIds,
                    body.value.isActive,
                    body.value.mirrorPriceScaleId,
                    body.value.percent,
                    body.value.constant,
                    body.value.name,
                    body.value.round
                  )
                  .map {
                    case SuccessResponse(id: Int) => Created(Json.toJson(id))
                    case _                        => InternalServerError(message("Unhandled database error"))
                  }
                  .recover {
                    case err =>
                      logger.error("Error creating pricing rule", err)
                      InternalServerError(message("Unknown error"))
                  }

            }
      }
    }

  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "New Pricing Rule Object",
        required = true,
        dataType = "com.eventdynamic.api.models.ProVenuePricingRuleModification",
        paramType = "body"
      )
    )
  )
  def update(
    @ApiParam(value = "ProVenuePricingRule ID", required = true) id: Int
  ): Action[JsValue] = silhouette.SecuredAction.async(parse.tolerantJson) { implicit request =>
    val clientId = request.identity.user.activeClientId
    request.body.validate[ProVenuePricingRuleModification] match {
      case err: JsError => Future.successful(BadRequest(JsError.toJson(err)))
      case body: JsSuccess[ProVenuePricingRuleModification] =>
        checkForConflicts(Some(id), clientId, body.value)
          .flatMap {
            case rules if rules.nonEmpty =>
              Future.successful(Conflict(Json.toJson(rules)))
            case _ =>
              proVenuePricingRuleService
                .update(
                  id,
                  clientId,
                  body.value.eventIds,
                  body.value.priceScaleIds,
                  body.value.externalBuyerTypeIds,
                  body.value.isActive,
                  body.value.mirrorPriceScaleId,
                  body.value.percent,
                  body.value.constant,
                  body.value.name,
                  body.value.round
                )
                .map {
                  case SuccessResponse(id: Int) => Ok(Json.toJson(id))
                  case NotFoundResponse         => NotFound(message("Matching record(s) not found"))
                  case _                        => InternalServerError(message("Unhandled database error"))
                }
                .recover {
                  case err =>
                    logger.error("Error updating pricing rule", err)
                    InternalServerError(message("Unknown error"))
                }
          }
    }
  }

  def getOne(
    @ApiParam(value = "ProVenuePricingRule ID", required = true) id: Int
  ): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    val clientId = request.identity.user.activeClientId
    proVenuePricingRuleService
      .getOne(id, clientId)
      .map {
        case None         => NotFound(message("Not found"))
        case Some(record) => Ok(Json.toJson(record))
      }
  }

  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "Pricing rule successfully deleted"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 404, message = "Pricing rule does not exist or could not found")
    )
  )
  def delete(
    @ApiParam(value = "ProVenuePricingRule ID", required = true) id: Int
  ): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    val clientId = request.identity.user.activeClientId
    proVenuePricingRuleService
      .delete(id, clientId)
      .map {
        case false => NotFound(message("Pricing rule does not exist or could not found"))
        case true  => Ok(message("Pricing rule successfully deleted"))
      }
  }

  private def checkForConflicts(
    id: Option[Int],
    clientId: Int,
    proVenuePricingRuleModification: ProVenuePricingRuleModification
  ): Future[ConflictResponse] = {
    if (!proVenuePricingRuleModification.isActive) {
      return Future.successful(ConflictResponse(Seq()))
    }
    val adjustmentRuleFuture = proVenuePricingRuleService
      .getConflicting(
        id,
        clientId,
        proVenuePricingRuleModification.eventIds,
        proVenuePricingRuleModification.priceScaleIds,
        proVenuePricingRuleModification.externalBuyerTypeIds
      )

    for {
      adjustmentRuleIds <- adjustmentRuleFuture
    } yield {
      ConflictResponse(adjustmentRuleIds)
    }
  }
}
