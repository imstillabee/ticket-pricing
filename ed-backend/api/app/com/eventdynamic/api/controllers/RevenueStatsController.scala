package com.eventdynamic.api.controllers

import com.eventdynamic.api.models.{Message, RevenueCategoryRevenueApiModel}
import com.eventdynamic.models.{Event, Season}
import com.eventdynamic.services.{EventService, SeasonService}
import com.eventdynamic.transactions.RevenueStatsTransaction
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations._
import play.api.libs.json.Json
import play.api.mvc._
import utils.silhouette.DefaultEnv

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/revenueStats")
class RevenueStatsController @Inject()(
  cc: ControllerComponents,
  silhouette: Silhouette[DefaultEnv],
  revenueStatsTransaction: RevenueStatsTransaction,
  eventService: EventService,
  seasonService: SeasonService
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with RevenueCategoryRevenueApiModel
    with Message {

  def validateEventExistsForClient(eventId: Option[Int], clientId: Int)(
    continue: Option[Event] => Future[Result]
  ): Future[Result] = {
    if (eventId.isEmpty) {
      continue(None)
    } else {
      val notFoundResponse = Future.successful(NotFound(message("Event does not exist")))
      eventService.getById(eventId.get).flatMap {
        case None                              => notFoundResponse
        case Some(e) if e.clientId != clientId => notFoundResponse
        case event                             => continue(event)
      }
    }
  }

  def validateSeasonExists(seasonId: Option[Int], clientId: Option[Int], isAdmin: Option[Boolean])(
    continue: Option[Season] => Future[Result]
  ): Future[Result] = {
    if (seasonId.isEmpty) {
      continue(None)
    } else {
      seasonService.getById(seasonId.get, clientId, isAdmin.get).flatMap {
        case None   => Future.successful(NotFound(message("Season does not exist")))
        case season => continue(season)
      }
    }
  }

  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "[{ RevenueCategoryRevenue }]"),
      new ApiResponse(code = 404, message = "Event or season does not exist")
    )
  )
  def get(
    @ApiParam(value = "Event ID to filter by", required = false, example = "1") eventId: Option[
      Int
    ],
    @ApiParam(value = "Season ID to filter by", required = false, example = "1") seasonId: Option[
      Int
    ]
  ): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    val clientId = request.identity.user.activeClientId
    val isAdmin = request.identity.user.isAdmin
    validateEventExistsForClient(eventId, clientId) { event =>
      validateSeasonExists(seasonId, Some(clientId), Some(isAdmin)) { season =>
        revenueStatsTransaction
          .getRevenueByCategory(clientId, event.flatMap(_.id), season.flatMap(_.id))
          .map(stats => Ok(Json.toJson(stats)))
      }
    }
  }
}
