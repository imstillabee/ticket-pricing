package com.eventdynamic.api.controllers

import com.eventdynamic.api.models.{EventModelFormat, Message, SeasonApiModel}
import com.eventdynamic.models._
import com.eventdynamic.services._
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations._
import play.api.libs.json._
import play.api.mvc._
import utils.silhouette.{Admin, DefaultEnv}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/seasons")
class SeasonsController @Inject()(
  cc: ControllerComponents,
  seasonService: SeasonService,
  eventService: EventService,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with SeasonApiModel
    with EventModelFormat
    with Message {

  /**
    * Get Season - Gets a Season by id
    * @param id
    * @return season object
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "[{ Season }]"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 404, message = "Season not found")
    )
  )
  def getById(
    @ApiParam(value = "Season Id to return", required = true, defaultValue = "1") id: Int
  ): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    seasonService
      .getById(id, Some(request.identity.user.activeClientId), request.identity.user.isAdmin)
      .map {
        case Some(season) => Ok(Json.toJson(season))
        case None         => NotFound(message("Season does not exist or could not be found"))
      }
  }

  /**
    * Get All Seasons - Gets all Seasons
    * @return sequence of season objects
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "[{ Season }]"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Unauthorized"),
      new ApiResponse(code = 404, message = "No seasons found")
    )
  )
  def getAll(): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    seasonService
      .getAll(Some(request.identity.user.activeClientId), request.identity.user.isAdmin)
      .map(s => Ok(Json.toJson(SeasonModel.fromSeasons(s))))
      .recover {
        case e => InternalServerError(message(s"Error getting seasons: $e"))
      }
  }

  /**
    * Create Season - Creates a Season
    * @return id of created season or existing season
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 201, message = "Season created"),
      new ApiResponse(code = 400, message = "Request error"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Unauthorized"),
      new ApiResponse(code = 404, message = "Season not found"),
      new ApiResponse(code = 409, message = "Season already exists")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "New Season Object",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.SeasonCreateInfo",
        paramType = "body"
      )
    )
  )
  def create: Action[JsValue] = silhouette.SecuredAction.async(parse.tolerantJson) {
    implicit request =>
      request.body.validate[CreateSeasonApiModel] match {
        case s: JsSuccess[CreateSeasonApiModel] =>
          seasonService
            .create(s.value.name, s.value.clientId, s.value.startTimestamp, s.value.endTimestamp)
            .map(_ => Created(message("Season created")))
            .recover {
              case _ => InternalServerError(message("Error creating season"))
            }
        case _: JsError =>
          Future.successful(BadRequest(message(s"Invalid request body ${request.body}")))
      }
  }

  /**
    * Update - Updates Season by Id
    * @param id
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "Season Updated"),
      new ApiResponse(code = 400, message = "Request error"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Unauthorized"),
      new ApiResponse(code = 404, message = "Season not found")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "Updated Season Object",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.SeasonCreateInfo",
        paramType = "body"
      )
    )
  )
  def update(
    @ApiParam(value = "Id used to look up Season", required = true, defaultValue = "1") id: Int
  ): Action[JsValue] = silhouette.SecuredAction.async(parse.tolerantJson) { implicit request =>
    request.body.validate[CreateSeasonApiModel] match {
      case s: JsSuccess[CreateSeasonApiModel] =>
        seasonService
          .update(
            Season(
              Some(id),
              s.value.name,
              s.value.clientId,
              s.value.startTimestamp,
              s.value.startTimestamp
            )
          )
          .map {
            case NotFoundResponse                => NotFound(message("Season does not exist"))
            case SuccessResponse(season: Season) => Ok(Json.toJson(season))
          }
          .recover {
            case _ => InternalServerError(message("Error updating season"))
          }
      case _: JsError =>
        Future.successful(BadRequest(message("Invalid request body")))
    }
  }

  /**
    * Delete - Delete a Season by Id
    * @param id
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "Deleted Season [id]"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Unauthorized"),
      new ApiResponse(code = 404, message = "Season does not exist or could not be found")
    )
  )
  def delete(
    @ApiParam(value = "Season Id to be deleted", required = true, defaultValue = "1") id: Int
  ): Action[AnyContent] = silhouette.SecuredAction(Admin()).async { implicit request =>
    seasonService
      .delete(id)
      .map {
        case true => Ok(message("Season successfully deleted"))
        case _ =>
          NotFound(message("Season does not exist or could not be found"))
      }
      .recover {
        case _ => BadRequest(message("Invalid request body"))
      }
  }

  /**
    * Resets the start and end dates on a season object.
    *
    * @param id
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "[{ Season }]"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Unauthorized"),
      new ApiResponse(code = 404, message = "Season does not exist or could not be found")
    )
  )
  def resetDates(
    @ApiParam(value = "Season Id to have dates reset", required = true, defaultValue = "1") id: Int
  ): Action[AnyContent] = silhouette.SecuredAction(Admin()).async { implicit request =>
    seasonService
      .getById(id, Some(request.identity.user.activeClientId), request.identity.user.isAdmin)
      .flatMap {
        case Some(s) =>
          seasonService.resetDates(s).map {
            case SuccessResponse(s: Season) => Ok(Json.toJson(s))
            case _                          => InternalServerError("Failed to update season")
          }
        case None =>
          Future.successful(NotFound(message("Season does not exist or could not be found")))
      }
  }
}
