package com.eventdynamic.api.controllers

import com.eventdynamic.api.models.{Message, TeamStatApiModel}
import com.eventdynamic.models.ClientPerformanceType._
import com.eventdynamic.services.{
  ClientService,
  MLSSeasonStatService,
  NCAABSeasonStatService,
  NCAAFSeasonStatService,
  NFLSeasonStatService,
  TeamStatService
}
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations._
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents}
import utils.silhouette.DefaultEnv

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/teamStatistics")
class TeamStatsController @Inject()(
  cc: ControllerComponents,
  teamStatService: TeamStatService,
  clientService: ClientService,
  nflSeasonStatService: NFLSeasonStatService,
  mlsSeasonStatService: MLSSeasonStatService,
  ncaafSeasonStatService: NCAAFSeasonStatService,
  ncaabSeasonStatService: NCAABSeasonStatService,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with Message
    with TeamStatApiModel {

  /**
    * Get TeamStats - Gets Client Team Stats by user client id and client performance type
    *
    * @return TeamStat object
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "[{ Team Statistics }]"),
      new ApiResponse(code = 400, message = "Request error"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "Team Statistics not found")
    )
  )
  def get = silhouette.SecuredAction.async { implicit request =>
    Some(request.identity.user.activeClientId) match {
      case Some(id) =>
        val results = for {
          client <- clientService.getById(id)
        } yield {
          client.get.performanceType match {
            case MLB =>
              teamStatService.getByClientId(id).map {
                case Seq()     => NotFound(message("Team Statistics not found"))
                case teamStats => Ok(Json.toJson(teamStats))
              }
            case NFL =>
              nflSeasonStatService.getByClientId(id).map {
                case Seq()          => NotFound(message("NFL Season Statistics not found"))
                case nflSeasonStats => Ok(Json.toJson(nflSeasonStats))
              }
            case MLS =>
              mlsSeasonStatService.getByClientId(id).map {
                case Seq()          => NotFound(message("MLS Season Statistics not found"))
                case mlsSeasonStats => Ok(Json.toJson(mlsSeasonStats))
              }
            case NCAAF =>
              ncaafSeasonStatService.getByClientId(id).map {
                case Seq()            => NotFound(message("NCAAF Season Statistics not found"))
                case ncaafSeasonStats => Ok(Json.toJson(ncaafSeasonStats))
              }
            case NCAAB =>
              ncaabSeasonStatService.getByClientId(id).map {
                case Seq()            => NotFound(message("NCAAB Season Statistics not found"))
                case ncaabSeasonStats => Ok(Json.toJson(ncaabSeasonStats))
              }
          }
        }
        results.flatten
      case _ => Future.successful(Forbidden(message("Access Denied")))
    }
  }
}
