package com.eventdynamic.api.controllers

import java.time.{Instant, LocalDate}

import com.eventdynamic.api.models.{Message, TimeStatApiModel}
import com.eventdynamic.models.{Event, Season}
import com.eventdynamic.services.{EventService, SeasonService}
import com.eventdynamic.transactions.TimeStatsTransaction
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations.{Api, ApiParam}
import play.api.libs.json.Json
import play.api.mvc._
import utils.silhouette.DefaultEnv

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/timeStats")
class TimeStatsController @Inject()(
  cc: ControllerComponents,
  eventService: EventService,
  seasonService: SeasonService,
  timeStatsTransaction: TimeStatsTransaction,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with TimeStatApiModel
    with Message {

  def validateEventExistsForClient(eventId: Option[Int], clientId: Int)(
    continue: Option[Event] => Future[Result]
  ): Future[Result] = {
    if (eventId.isEmpty) {
      continue(None)
    } else {
      val notFoundResponse = Future.successful(NotFound(message("Event does not exist")))
      eventService.getById(eventId.get).flatMap {
        case None                              => notFoundResponse
        case Some(e) if e.clientId != clientId => notFoundResponse
        case event                             => continue(event)
      }
    }
  }

  def validateSeasonExists(seasonId: Option[Int], clientId: Int)(
    continue: Option[Season] => Future[Result]
  ): Future[Result] = {
    if (seasonId.isEmpty) {
      continue(None)
    } else {
      seasonService.getById(seasonId.get, Some(clientId), false).flatMap {
        case None   => Future.successful(NotFound(message("Season does not exist")))
        case season => continue(season)
      }
    }
  }

  /**
    * Get time stats
    *
    * @param eventId Event ID filter
    * @param seasonId Season ID filter
    * @param startDate Start date filter expect an ISO date
    * @param endDate End time filter expects an ISO date
    * @return
    */
  def get(
    @ApiParam(value = "Event ID", required = false, example = "1") eventId: Option[Int],
    @ApiParam(value = "Season ID", required = false, example = "1") seasonId: Option[Int],
    @ApiParam(value = "ISO start date", required = false, example = "2018-05-01") startDate: String,
    @ApiParam(value = "ISO end date", required = false, example = "2018-05-10") endDate: String
  ): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    val start = LocalDate.parse(startDate)
    val end = LocalDate.parse(endDate)

    if (start.isAfter(end)) {
      Future.successful(BadRequest(message("Start date must be before end date")))
    } else {
      val clientId = request.identity.user.activeClientId
      val isAdmin = request.identity.user.isAdmin
      validateEventExistsForClient(eventId, clientId) { event =>
        validateSeasonExists(seasonId, clientId) { season =>
          timeStatsTransaction
            .get(
              start,
              end,
              clientId,
              event.flatMap(_.id),
              season.flatMap(_.id),
              now = Instant.now()
            )
            .map(stats => Ok(Json.toJson(stats)))
        }
      }
    }
  }
}
