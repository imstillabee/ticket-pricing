package com.eventdynamic.api.controllers

import java.sql.Timestamp
import java.time.Instant

import com.eventdynamic.api.models.Message
import com.eventdynamic.services.{EventService, SeasonService, TransactionReportService}
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations._
import play.api.mvc._
import utils.silhouette.{DefaultEnv, WithEvent, WithSeason}
import org.slf4j.LoggerFactory
import play.api.http.HttpEntity
import play.api.mvc

import scala.concurrent.ExecutionContext

@Singleton
@Api(value = "/transactionReport")
class TransactionReportController @Inject()(
  cc: ControllerComponents,
  silhouette: Silhouette[DefaultEnv],
  transactionReportService: TransactionReportService,
  seasonService: SeasonService,
  eventService: EventService
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with Message {

  private val logger = LoggerFactory.getLogger(this.getClass)

  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "successful operation"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 500, message = "Error processing request")
    )
  )
  def getBySeasonId(
    @ApiParam(value = "Season ID to filter by", required = true, example = "1") id: Int,
    @ApiParam(
      value = "Starting ISO date in date range",
      required = false,
      example = "2018-10-01T00:00:00.00Z"
    ) start: Option[String],
    @ApiParam(
      value = "End ISO date in date range",
      required = false,
      example = "2019-01-01T00:00:00.00Z"
    ) end: Option[String]
  ) = silhouette.SecuredAction(WithSeason(id, seasonService)) { implicit request =>
    val clientId = request.identity.user.activeClientId
    try {
      val csvSource = transactionReportService
        .generateReport(
          seasonId = Some(id),
          start = start.map(x => Timestamp.from(Instant.parse(x))),
          end = end.map(x => Timestamp.from(Instant.parse(x))),
          clientId = clientId
        )

      val headerMap = Map("Content-disposition" -> "attachment; filename=TransactionReport.csv")
      Result(
        header = mvc.ResponseHeader(200, headerMap),
        body = HttpEntity.Streamed(csvSource, None, Some("text/csv"))
      )
    } catch {
      case ex: Throwable => {
        logger.error(ex.getMessage)
        InternalServerError(message("Error processing request"))
      }
    }
  }

  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "successful operation"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 500, message = "Error processing request")
    )
  )
  def getByEventId(
    @ApiParam(value = "Event ID to filter by", required = true, example = "1") id: Int,
    @ApiParam(
      value = "Starting ISO date in date range",
      required = false,
      example = "2018-10-01T00:00:00.00Z"
    ) start: Option[String],
    @ApiParam(
      value = "End ISO date in date range",
      required = false,
      example = "2019-01-01T00:00:00.00Z"
    ) end: Option[String]
  ) = silhouette.SecuredAction(WithEvent(id, eventService)) { implicit request =>
    val clientId = request.identity.user.activeClientId

    try {
      val csvSource = transactionReportService
        .generateReport(
          eventId = Some(id),
          start = start.map(x => Timestamp.from(Instant.parse(x))),
          end = end.map(x => Timestamp.from(Instant.parse(x))),
          clientId = clientId
        )

      val headerMap = Map("Content-disposition" -> "attachment; filename=TransactionReport.csv")
      Result(
        header = mvc.ResponseHeader(200, headerMap),
        body = HttpEntity.Streamed(csvSource, None, Some("text/csv"))
      )
    } catch {
      case ex: Throwable => {
        logger.error(ex.getMessage)
        InternalServerError(message("Error processing request"))
      }
    }
  }
}
