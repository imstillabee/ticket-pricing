package com.eventdynamic.api.controllers

import com.eventdynamic.api.models.{AuthApiModel, Message, UserApiModel}
import com.eventdynamic.models.User
import com.eventdynamic.services.{FailedToSendEmailException, MailerService, TCode, UserService}
import com.eventdynamic.utils.{NotFoundResponse, SuccessResponse}
import com.mohiva.play.silhouette.api.{Environment, LoginInfo, Silhouette}
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.util.{Credentials, PasswordHasherRegistry, PasswordInfo}
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import io.swagger.annotations._
import com.google.inject.{Inject, Singleton}
import org.slf4j.LoggerFactory
import play.api.Configuration
import play.api.i18n.I18nSupport
import play.api.libs.json._
import play.api.mvc._
import utils.silhouette.{Admin, DefaultEnv}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
@Api(value = "/users")
class UsersController @Inject()(
  controllerComponents: ControllerComponents,
  silhouette: Silhouette[DefaultEnv],
  userService: UserService,
  mailerService: MailerService,
  authInfoRepository: AuthInfoRepository,
  credentialsProvider: CredentialsProvider,
  passwordHasherRegistry: PasswordHasherRegistry,
  conf: Configuration
)(implicit ec: ExecutionContext)
    extends AbstractController(controllerComponents)
    with I18nSupport
    with UserApiModel
    with AuthApiModel
    with Message {

  private val logger = LoggerFactory.getLogger(this.getClass)

  // API view of the User, implicitly used by Json.toJson
  def env: Environment[DefaultEnv] = silhouette.env

  private def getUserById(id: Int) = {
    userService.getById(id).map {
      case Some(user) => Ok(Json.toJson(user))
      case None       => NotFound(message("User does not exist or could not be found"))
    }
  }

  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "Gets the user by email."),
      new ApiResponse(code = 400, message = "Could not parse, id is not int"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Unauthorized"),
      new ApiResponse(code = 404, message = "User does not exist or could not be found")
    )
  )
  def getById(
    @ApiParam(value = "id used to look up user", required = true, defaultValue = "2") id: Int
  ) = silhouette.SecuredAction.async { implicit request =>
    request.identity.user.id match {
      case Some(`id`) => getUserById(id)
      case _ => {
        if (request.identity.user.isAdmin) {
          getUserById(id)
        } else {
          Future.successful(Forbidden(message("Access Denied")))
        }
      }
    }
  }

  /**
    * get - Gets all users with the same clientId as the current user
    *
    * @return - Seq[User]
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ [User] }"),
      new ApiResponse(code = 401, message = "Unauthenticated")
    )
  )
  def get(): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    val clientId =
      request.identity.user.activeClientId
    val isAdmin = request.identity.user.isAdmin
    userService.getAll(clientId, isAdmin).map(users => Ok(Json.toJson(users)))
  }

  /**
    * Delete - Deletes a User by Email
    *
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "Deleted User [email]"),
      new ApiResponse(code = 400, message = "Invalid request body"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Unauthorized"),
      new ApiResponse(code = 404, message = "User does not exist or could not be found")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        value = "Email to be deleted",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.DeleteUserInfo",
        paramType = "body"
      )
    )
  )
  def delete = silhouette.SecuredAction(Admin()).async(parse.tolerantJson) { implicit request =>
    val bodyMustBe: Reads[String] = (JsPath \ "email").read[String]
    request.body.validate[String](bodyMustBe) match {
      case email: JsSuccess[String] =>
        userService
          .deleteByEmail(email.value)
          .map {
            case true => Ok(message("User successfully deleted"))
            case _ =>
              NotFound(message("User does not exist or could not be found"))
          }
          .recover {
            case _ => BadRequest(message("Invalid request body"))
          }
      case _: JsError => Future.successful(BadRequest(message("Invalid request body")))
    }
  }

  /**
    * Create User - Creates a User
    *
    * @return User Object
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 201, message = "Created"),
      new ApiResponse(code = 400, message = "Invalid request body"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Unauthorized"),
      new ApiResponse(code = 404, message = "Not Created")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "New User Object",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.CreateUserInfo",
        paramType = "body"
      )
    )
  )
  def createUser = silhouette.SecuredAction(Admin()).async(parse.tolerantJson) { implicit request =>
    request.body.validate[CreateUserApiModel] match {
      case s: JsSuccess[CreateUserApiModel] => {
        userService
          .create(
            s.value.email,
            s.value.firstName,
            s.value.lastName,
            None,
            s.value.clientId.get,
            s.value.phoneNumber,
            s.value.isAdmin,
            conf.underlying.getInt("user.tempPasswordExpiration")
          )
          .map(userTuple => {
            val (newUser, tPass) = userTuple

            mailerService.send(
              TCode.new_user,
              Map(
                "userId" -> newUser.id.toString,
                "toEmail" -> newUser.email,
                "toName" -> newUser.firstName,
                "tempPass" -> tPass
              )
            )
            Created(message("User successfully created"))
          })
          .recover {
            case ex: FailedToSendEmailException => {
              logger.error(s"in UserController.createUser - email to ${s.value.email} failed", ex)
              Created(message(s"Failed to send email to ${s.value.email}"))
            }
            case _ => {
              NotFound(message("Could not create user"))
            }
          }
      }
      case e: JsError => {
        Future.successful(BadRequest(message("Invalid request body")))
      }
    }
  }

  /**
    * Update - Updates a users personal information
    *
    * @return - updated user information
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ user }"),
      new ApiResponse(code = 400, message = "Invalid request body"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 404, message = "User does not exist or could not be found")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "User Name and Phone Number",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.CreateUserInfo",
        paramType = "body"
      )
    )
  )
  def update = silhouette.SecuredAction.async(parse.tolerantJson) { implicit request =>
    request.body.validate[UpdateContactInfoApiModel] match {
      case s: JsSuccess[UpdateContactInfoApiModel] => {
        userService
          .updateUserInfo(
            request.identity.user.id.get,
            s.value.firstName,
            s.value.lastName,
            s.value.phoneNumber
          )
          .map {
            case NotFoundResponse =>
              NotFound(message("User does not exist or could not be found"))
            case SuccessResponse(updatedUser: User) =>
              Ok(Json.toJson(updatedUser))
          }
      }
      case _: JsError => {
        Future.successful(BadRequest(message("Invalid request body")))
      }
    }
  }

  /**
    * UpdateEmail - Updates a users email
    *
    * @return - updated user email and re-authenticates user
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ user }"),
      new ApiResponse(code = 400, message = "Invalid request body"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 404, message = "User does not exist or could not be found")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "Email and Password",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.CreateUserInfo",
        paramType = "body"
      )
    )
  )
  def updateEmail = silhouette.SecuredAction.async(parse.tolerantJson) { implicit request =>
    request.body.validate[UpdateEmailInfoApiModel] match {
      case s: JsSuccess[UpdateEmailInfoApiModel] => {
        credentialsProvider
          .authenticate(Credentials(request.identity.user.email, s.value.password))
          .flatMap { loginInfo =>
            userService
              .updateEmail(request.identity.user.id.get, s.value.email)
              .flatMap {
                case NotFoundResponse =>
                  Future.successful(NotFound(message("User does not exist or could not be found")))
                case SuccessResponse(updatedUser: User) =>
                  val passwordInfo = passwordHasherRegistry.current.hash(s.value.password)
                  val newAuth = LoginInfo(loginInfo.providerID, s.value.email)
                  for {
                    _ <- authInfoRepository.update[PasswordInfo](newAuth, passwordInfo)
                    authenticator <- env.authenticatorService.create(newAuth)
                    cookie <- env.authenticatorService.renew(authenticator)
                    result <- env.authenticatorService.embed(cookie, Ok(Json.toJson(updatedUser)))
                  } yield result
              }
          }
          .recoverWith {
            case _ => Future.successful(BadRequest(message("Incorrect Email or Password")))
          }
      }
      case _: JsError => {
        Future.successful(BadRequest(message("Invalid request body")))
      }
    }
  }

  /**
    * changePassword - changes a users password
    *
    * @return - updated user password and re-authenticates user
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ user }"),
      new ApiResponse(code = 400, message = "Invalid request body"),
      new ApiResponse(code = 401, message = "Unauthenticated"),
      new ApiResponse(code = 403, message = "Unauthorized"),
      new ApiResponse(code = 404, message = "User does not exist or could not be found")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "body",
        value = "Old Password and New Password",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.CreateUserInfo",
        paramType = "body"
      )
    )
  )
  def changePassword = silhouette.SecuredAction.async(parse.tolerantJson) { implicit request =>
    request.body.validate[ChangePasswordApiModel] match {
      case s: JsSuccess[ChangePasswordApiModel] => {
        credentialsProvider
          .authenticate(Credentials(request.identity.user.email, s.value.password))
          .flatMap { loginInfo =>
            userService.changePassword(request.identity.user.id.get, s.value.newPassword).flatMap {
              case NotFoundResponse =>
                Future.successful(NotFound(message("User does not exist or could not be found")))
              case SuccessResponse(updatedUser: User) =>
                val passwordInfo = passwordHasherRegistry.current.hash(s.value.newPassword)
                for {
                  _ <- authInfoRepository.update[PasswordInfo](loginInfo, passwordInfo)
                  authenticator <- env.authenticatorService.create(loginInfo)
                  cookie <- env.authenticatorService.renew(authenticator)
                  result <- env.authenticatorService.embed(cookie, Ok(Json.toJson(updatedUser)))
                } yield result
            }
          }
          .recoverWith {
            case _ => Future.successful(BadRequest(message("Incorrect Email or Password")))
          }
      }
      case _: JsError => {
        Future.successful(BadRequest(message("Invalid request body")))
      }
    }
  }

  /**
    * ForgotPassword - Creates a tempPass with an expiration and emails it to the user
    *
    * @return
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "Sent temporary password [email]"),
      new ApiResponse(code = 400, message = "Invalid request body"),
      new ApiResponse(code = 404, message = "User does not exist or could not be found")
    )
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        value = "Email of user that forgot password",
        required = true,
        dataType = "com.eventdynamic.api.models.swagger.ForgotPasswordUserInfo",
        paramType = "body"
      )
    )
  )
  def forgotPassword = silhouette.UnsecuredAction.async(parse.tolerantJson) { implicit request =>
    val bodyMustBe: Reads[String] = (JsPath \ "email").read[String]
    request.body.validate[String](bodyMustBe) match {
      case email: JsSuccess[String] =>
        userService
          .forgotPassword(email.value, conf.underlying.getInt("user.resetPasswordExpiration"))
          .map(userTuple => {
            val (user, tPass) = userTuple.get
            mailerService.send(
              TCode.reset_pass,
              Map(
                "userId" -> user.id.get.toString,
                "toEmail" -> user.email,
                "toName" -> user.firstName,
                "tempPass" -> tPass
              )
            )
            Ok(message("Password recovery email sent"))
          })
          .recover {
            case ex: FailedToSendEmailException => {
              logger.error(s"in UserController.forgotPassword - email to ${email.value} failed", ex)
              Created(message(s"Failed to send forgot password email to ${email.value}"))
            }
            case _ => {
              BadRequest(message("Invalid request body"))
            }
          }
      case _: JsError => Future.successful(BadRequest(message("Invalid request body")))
    }
  }
}
