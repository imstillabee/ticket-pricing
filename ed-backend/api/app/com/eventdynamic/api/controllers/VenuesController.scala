package com.eventdynamic.api.controllers

import com.eventdynamic.api.models.{Message, PriceScaleApiModel, VenueApiModel}
import com.eventdynamic.services.{EventService, PriceScaleService, VenueService}
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import io.swagger.annotations._
import play.api.libs.json._
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import utils.silhouette.{DefaultEnv, WithVenue}

import scala.concurrent.ExecutionContext

@Singleton
@Api(value = "/venues")
class VenuesController @Inject()(
  cc: ControllerComponents,
  eventService: EventService,
  venueService: VenueService,
  priceScaleService: PriceScaleService,
  silhouette: Silhouette[DefaultEnv]
)(implicit ec: ExecutionContext)
    extends AbstractController(cc)
    with VenueApiModel
    with PriceScaleApiModel
    with Message {

  /**
    * Get Venue - Gets a Venue by id
    *
    * @param id unique, auto-incremented & indexed identifier
    * @return venue object
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "{ Venue }"),
      new ApiResponse(code = 400, message = "Request error"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "Venue not found")
    )
  )
  def getById(
    @ApiParam(value = "Id used to look up venue", required = true, defaultValue = "2") id: Int
  ) =
    silhouette.SecuredAction(WithVenue(id, eventService)).async { implicit request =>
      venueService.getById(id).map {
        case Some(venue) => Ok(Json.toJson(venue))
        case None        => NotFound(message("Venue does not exist or could not be found"))
      }
    }

  /**
    * Get PriceScales - Gets PriceScales for a Venue id
    *
    * @param id unique, auto-incremented & indexed identifier
    * @return seq of PriceScale objects
    */
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "[{ PriceScale }]"),
      new ApiResponse(code = 400, message = "Request error"),
      new ApiResponse(code = 403, message = "Access Denied"),
      new ApiResponse(code = 404, message = "PriceScales not found")
    )
  )
  def getPriceScales(
    @ApiParam(value = "VenueId used to look up PriceScales", required = true, defaultValue = "1") id: Int
  ): Action[AnyContent] =
    silhouette.SecuredAction(WithVenue(id, eventService)).async { implicit request =>
      priceScaleService.getAllForVenue(id).map {
        case priceScales if priceScales.nonEmpty => Ok(Json.toJson(priceScales))
        case _                                   => NotFound(message("No PriceScales were found for the specified Venue"))
      }
    }
}
