package com.eventdynamic.api.models

import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json.{JsPath, Reads}

trait AuthApiModel {
  case class SignInApiModel(email: String, password: String)

  implicit val signInReads: Reads[SignInApiModel] = (
    (JsPath \ "email").read[String](email) and
      (JsPath \ "password").read[String](minLength[String](6))
  )(SignInApiModel.apply _)
}
