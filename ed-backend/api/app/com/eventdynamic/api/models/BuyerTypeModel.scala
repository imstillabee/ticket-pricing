package com.eventdynamic.api.models

import play.api.libs.json.{Format, Json, OFormat}

/**
  *
  * @param id
  * @param publicDescription
  * @param code
  * @param active - Active from Tickets.com
  * @param isInPriceStructure - True if BuyerType is in a price structure for corresponding events, false otherwise
  */
case class BuyerTypeModel(
  id: String,
  publicDescription: String,
  code: String,
  active: Boolean,
  isInPriceStructure: Boolean
)

case class BuyerTypeBulkModification(externalBuyerTypeId: String, disabled: Boolean)
abstract class BuyerTypeBulkModificationList extends java.util.List[BuyerTypeBulkModification]

trait BuyerTypeModelFormat {
  implicit lazy val buyerTypeFormat: Format[BuyerTypeModel] = Json.format[BuyerTypeModel]

  implicit lazy val buyerTypeBulkModificationFormat: OFormat[BuyerTypeBulkModification] =
    Json.format[BuyerTypeBulkModification]
}
