package com.eventdynamic.api.models

import com.eventdynamic.models.Client
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, Reads, Writes}

trait ClientApiModel {
  implicit val clientWrites = new Writes[Client] {

    def writes(client: Client) = Json.obj(
      "id" -> client.id,
      "createdAt" -> client.createdAt,
      "modifiedAt" -> client.modifiedAt,
      "name" -> client.name,
      "pricingInterval" -> client.pricingInterval,
      "logoUrl" -> client.logoUrl,
      "performanceType" -> client.performanceType
    )
  }

  case class CreateClientApiModel(name: String, pricingInterval: Int)

  implicit val createClientReads: Reads[CreateClientApiModel] = (
    (JsPath \ "name").read[String] and
      (JsPath \ "pricingInterval").read[Int]
  )(CreateClientApiModel.apply _)
}
