package com.eventdynamic.api.models

import com.eventdynamic.models.{ClientIntegration, ClientIntegrationComposite}
import io.swagger.annotations.ApiModelProperty
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._

trait ClientIntegrationApiModel extends TimestampFormat {
  implicit lazy val clientIntegrationCompositeFormat: OFormat[ClientIntegrationComposite] =
    Json.format[ClientIntegrationComposite]

  implicit lazy val clientIntegrationFormat: Format[ClientIntegration] =
    Json.format[ClientIntegration]

  implicit val updateClientIntegrationReads: Reads[UpdateClientIntegrationApiModel] = (
    (JsPath \ "isPrimary").readNullable[Boolean] and
      (JsPath \ "percent").readNullable[Int](min(-100) keepAnd max(100)) and
      (JsPath \ "constant").readNullable[BigDecimal]
  )(UpdateClientIntegrationApiModel.apply _)
}

case class UpdateClientIntegrationApiModel(
  @ApiModelProperty(value = "isPrimary", example = "false") isPrimary: Option[Boolean],
  @ApiModelProperty(value = "percent", example = "5") percent: Option[Int],
  @ApiModelProperty(value = "constant", example = "5.50") constant: Option[BigDecimal]
)
