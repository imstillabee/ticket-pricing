package com.eventdynamic.api.models

import com.eventdynamic.models.EventCategory
import play.api.libs.json.{Format, Json}

trait EventCategoryModelFormat extends TimestampFormat {
  implicit lazy val eventCategoryFormat: Format[EventCategory] = Json.format[EventCategory]
}
