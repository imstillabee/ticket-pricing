package com.eventdynamic.api.models

import java.sql.Timestamp

import com.eventdynamic.models.ScheduledJobStatus.ScheduledJobStatus
import com.eventdynamic.models.{Event, ScheduledJob, ScheduledJobStatus}
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

case class EventModel(
  id: Option[Int],
  primaryEventId: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  name: String,
  timestamp: Option[Timestamp],
  clientId: Int,
  venueId: Int,
  eventCategoryId: Int,
  seasonId: Option[Int],
  isBroadcast: Boolean,
  percentPriceModifier: Int,
  totalInventory: Int,
  soldInventory: Int,
  unsoldInventory: Int,
  eventScore: Option[Double],
  eventScoreModifier: Double,
  spring: Option[Double],
  springModifier: Double,
  scheduledJob: Option[ScheduledJob],
  revenue: BigDecimal,
  timeZone: Option[String]
)

case class CreateEventModel(
  integrationId: Option[Int],
  name: String,
  timestamp: String,
  clientId: Int,
  venueId: Int,
  eventCategoryId: Int,
  seasonId: Option[Int],
  isBroadcast: Boolean,
  percentPriceModifier: Int,
  eventScoreModifier: Option[Double],
  springModifier: Option[Double]
)

case class ToggleIsBroadcastModel(isBroadcast: Boolean)

case class UpdatePriceModifierModel(percentPriceModifier: Int)

case class UpdateAdminModifierModel(eventScoreModifier: Double, springModifier: Double)

trait EventModelFormat extends TimestampFormat {
  implicit lazy val scheduledJobStatusReads: Reads[ScheduledJobStatus] =
    Reads.enumNameReads(ScheduledJobStatus)
  implicit lazy val scheduledJobStatusWrites: Writes[ScheduledJobStatus] = Writes.enumNameWrites

  implicit lazy val scheduledJobFormat: Format[ScheduledJob] = Json.format[ScheduledJob]

  implicit lazy val eventModelFormat: Format[EventModel] = Json.format[EventModel]

  implicit lazy val eventFormat: Format[Event] = Json.format[Event]

  implicit lazy val createEventReads: Reads[CreateEventModel] = (
    (JsPath \ "integrationId").readNullable[Int] and
      (JsPath \ "name").read[String] and
      (JsPath \ "timestamp").read[String] and
      (JsPath \ "clientId").read[Int] and
      (JsPath \ "venueId").read[Int] and
      (JsPath \ "eventCategoryId").read[Int] and
      (JsPath \ "seasonId").readNullable[Int] and
      (JsPath \ "isBroadcast").read[Boolean] and
      (JsPath \ "percentPriceModifier").read[Int](min(-50) keepAnd max(50)) and
      (JsPath \ "eventScoreModifier")
        .readNullable[Double](min(-100.00) keepAnd max(100.00)) and
      (JsPath \ "springModifier")
        .readNullable[Double](min(-2.00) keepAnd max(2.00))
  )(CreateEventModel.apply _)

  implicit lazy val UpdatePriceModifierReads: Reads[UpdatePriceModifierModel] =
    (JsPath \ "percentPriceModifier")
      .read[Int](min(-50) keepAnd max(50))
      .map(UpdatePriceModifierModel.apply _)

  implicit lazy val UpdateAdminModifierReads: Reads[UpdateAdminModifierModel] = (
    (JsPath \ "eventScoreModifier")
      .read[Double](min(-100.00) keepAnd max(100.00)) and
      (JsPath \ "springModifier")
        .read[Double](min(-2.00) keepAnd max(2.00))
  )(UpdateAdminModifierModel.apply _)

  implicit lazy val ToggleIsBroadcastReads: Reads[ToggleIsBroadcastModel] =
    (JsPath \ "isBroadcast")
      .read[Boolean]
      .map(ToggleIsBroadcastModel.apply)
}
