package com.eventdynamic.api.models

import play.api.libs.json.{Format, Json}

case class EventPricesModel(prices: Seq[EventPrice])
case class EventPrice(section: String, row: String, price: BigDecimal)

trait EventPricesModelFormat {
  implicit val eventPriceFormat: Format[EventPrice] = Json.format[EventPrice]
  implicit val eventPricesModelFormat: Format[EventPricesModel] = Json.format[EventPricesModel]
}
