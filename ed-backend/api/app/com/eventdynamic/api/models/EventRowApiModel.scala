package com.eventdynamic.api.models

import com.eventdynamic.models.EventRow
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, Reads, Writes}

trait EventRowApiModel {

  def idsToSeq(ids: String): Seq[Int] =
    ids // postgres json_agg returns "[Int, Int, Int..]"?
      .stripPrefix("[")
      .stripSuffix("]")
      .replaceAll("\\s", "")
      .split(",")
      .map(_.toInt)

  implicit val eventRowWrites = new Writes[EventRow] {

    def writes(eventRow: EventRow) = Json.obj(
      "eventId" -> eventRow.eventId,
      "section" -> eventRow.section,
      "row" -> eventRow.row,
      "priceScaleId" -> eventRow.priceScaleId,
      "seats" -> idsToSeq(eventRow.seats),
      "listedPrice" -> eventRow.listedPrice,
      "overridePrice" -> eventRow.overridePrice,
      "minimumPrice" -> eventRow.minimumPrice,
      "maximumPrice" -> eventRow.maximumPrice,
      "isListed" -> eventRow.isListed
    )
  }

  case class BulkEventSeatUpdateApiModel(
    eventSeatIds: Seq[Int],
    overridePrice: Option[BigDecimal],
    isListed: Option[Boolean]
  )

  implicit val updateBulkEventSeatUpdateReads: Reads[BulkEventSeatUpdateApiModel] = (
    (JsPath \ "eventSeatIds").read[Seq[Int]] and
      (JsPath \ "overridePrice").readNullable[BigDecimal] and
      (JsPath \ "isListed").readNullable[Boolean]
  )(BulkEventSeatUpdateApiModel.apply _)

}
