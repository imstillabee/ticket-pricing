package com.eventdynamic.api.models

import com.eventdynamic.models.Integration
import com.eventdynamic.transactions.TransactionIntegration
import play.api.libs.json.{Json, Writes}

trait IntegrationApiModel {
  implicit val integrationWrites = new Writes[Integration] {

    def writes(integration: Integration) = Json.obj(
      "id" -> integration.id,
      "createdAt" -> integration.createdAt,
      "modifiedAt" -> integration.modifiedAt,
      "name" -> integration.name,
      "logoUrl" -> integration.logoUrl
    )
  }

  implicit val seasonAndIntegrationWrites = new Writes[TransactionIntegration] {

    def writes(transactionIntegration: TransactionIntegration) = Json.obj(
      "id" -> transactionIntegration.id,
      "name" -> transactionIntegration.name,
      "logoUrl" -> transactionIntegration.logoUrl,
      "isActive" -> transactionIntegration.isActive,
      "sold" -> transactionIntegration.sold,
      "total" -> transactionIntegration.total
    )
  }
}
