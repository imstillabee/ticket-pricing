package com.eventdynamic.api.models

import play.api.libs.json.Json

trait Message {

  def message(message: String) = {
    Json.obj("message" -> message)
  }

}
