package com.eventdynamic.api.models

import com.eventdynamic.models.PriceScale
import play.api.libs.json._

trait PriceScaleApiModel {
  implicit val priceScaleWrites = new Writes[PriceScale] {

    def writes(priceScale: PriceScale) =
      Json.obj(
        "id" -> priceScale.id,
        "name" -> priceScale.name,
        "venueId" -> priceScale.venueId,
        "integrationId" -> priceScale.integrationId
      )
  }
}
