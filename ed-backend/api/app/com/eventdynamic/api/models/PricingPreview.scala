package com.eventdynamic.api.models

import play.api.libs.json.{Json, OFormat}

case class PricingStat(min: BigDecimal, max: BigDecimal, mean: BigDecimal)
case class PricingPreview(event: PricingStat, sections: Map[String, PricingStat])

object PricingStat {

  def of(prices: Seq[BigDecimal]): PricingStat = {
    PricingStat(prices.min, prices.max, prices.sum / prices.size)
  }
}

trait PricingPreviewFormat {
  implicit lazy val pricingStatFormat: OFormat[PricingStat] = Json.format[PricingStat]
  implicit lazy val pricingPreviewFormat: OFormat[PricingPreview] = Json.format[PricingPreview]
}
