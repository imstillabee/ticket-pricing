package com.eventdynamic.api.models

import com.eventdynamic.models.RoundingType.RoundingType
import com.eventdynamic.models.{ProVenuePricingRule, RoundingType}
import com.eventdynamic.services.ProVenuePricingRuleService
import io.swagger.annotations.ApiModelProperty
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

case class ProVenuePricingRuleModification(
  @ApiModelProperty(value = "Name", example = "My Pricing Rule") name: Option[String],
  @ApiModelProperty(value = "Is Active", example = "true") isActive: Boolean,
  @ApiModelProperty(value = "Mirror Price ScaleId", example = "1", dataType = "Int") mirrorPriceScaleId: Option[
    Int
  ],
  @ApiModelProperty(value = "Percent", example = "-10", dataType = "Int") percent: Option[Int],
  @ApiModelProperty(value = "Constant", example = "-10.99", dataType = "Double") constant: Option[
    BigDecimal
  ],
  @ApiModelProperty(
    value = "Percent",
    example = "Ceil",
    dataType = "String",
    allowableValues = "Ceil, Floor, Round, None"
  ) round: RoundingType,
  @ApiModelProperty(value = "Event Ids") eventIds: Seq[Int],
  @ApiModelProperty(value = "Price Scale Ids") priceScaleIds: Seq[Int],
  @ApiModelProperty(value = "External Buyer Type Ids") externalBuyerTypeIds: Seq[String]
)

trait ProVenuePricingRuleModel extends TimestampFormat {
  implicit val roundingTypeReads: Reads[RoundingType] = Reads.enumNameReads(RoundingType)
  implicit val roundingTypeWrites: Writes[RoundingType] = Writes.enumNameWrites
  implicit val proVenuePricingRuleFormat: Format[ProVenuePricingRule] = Json.format
  implicit val compoundProVenuePricingRuleFormat: Format[
    ProVenuePricingRuleService.CompoundProVenuePricingRule
  ] = Json.format

  implicit val proVenuePricingRuleModification: Reads[ProVenuePricingRuleModification] = (
    (JsPath \ "name").readNullable[String] and
      (JsPath \ "isActive").readNullable[Boolean].map(_.getOrElse(false)) and
      (JsPath \ "mirrorPriceScaleId").readNullable[Int] and
      (JsPath \ "percent").readNullable[Int](min(-100) keepAnd max(100)) and
      (JsPath \ "constant").readNullable[BigDecimal] and
      (JsPath \ "round").readNullable[RoundingType].map(_.getOrElse(RoundingType.Ceil)) and
      (JsPath \ "eventIds").read[Seq[Int]](minLength[Seq[Int]](1)) and
      (JsPath \ "priceScaleIds").read[Seq[Int]](minLength[Seq[Int]](1)) and
      (JsPath \ "externalBuyerTypeIds").read[Seq[String]](minLength[Seq[String]](1))
  )(ProVenuePricingRuleModification.apply _)
}
