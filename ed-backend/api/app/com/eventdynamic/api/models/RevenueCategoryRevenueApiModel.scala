package com.eventdynamic.api.models

import com.eventdynamic.models.RevenueCategoryRevenue
import play.api.libs.json.{Json, Writes}

trait RevenueCategoryRevenueApiModel {
  implicit val revenueCategoryRevenueWrites: Writes[RevenueCategoryRevenue] =
    (revenueCategoryRevenue: RevenueCategoryRevenue) =>
      Json.obj(
        "id" -> revenueCategoryRevenue.id,
        "name" -> revenueCategoryRevenue.name,
        "revenue" -> revenueCategoryRevenue.revenue,
        "ticketsSold" -> revenueCategoryRevenue.ticketsSold
    )
}
