package com.eventdynamic.api.models

import java.sql.Timestamp

import com.eventdynamic.models.{Event, Season}
import com.eventdynamic.utils.DateHelper
import play.api.libs.functional.syntax._
import play.api.libs.json._

trait SeasonApiModel extends TimestampFormat {

  implicit val seasonWrites = new Writes[Season] {
    val now = DateHelper.now()

    def writes(season: Season) =
      Json.obj(
        "id" -> season.id,
        "name" -> season.name,
        "clientId" -> season.clientId,
        "startTimestamp" -> season.startDate,
        "endTimestamp" -> season.endDate
      )
  }

  case class SeasonModel(
    id: Option[Int],
    name: Option[String],
    clientId: Int,
    startTimestamp: Option[Timestamp],
    endTimestamp: Option[Timestamp],
    isCurrentSeason: Boolean
  )

  object SeasonModel {

    def fromSeasons(seasons: Seq[Season], now: Timestamp = DateHelper.now()) = {
      // Finds the most recent season that has started
      val currentSeason =
        seasons.find(s => s.startDate.isDefined && s.startDate.get.before(now))

      // Maps the seasons to season api models with current season
      seasons.map(s => SeasonModel.fromSeason(s, currentSeason.contains(s)))
    }

    def fromSeason(season: Season, isCurrentSeason: Boolean) = SeasonModel(
      season.id,
      season.name,
      season.clientId,
      season.startDate,
      season.endDate,
      isCurrentSeason
    )
  }

  implicit val seasonModelWrites = new Writes[SeasonModel] {
    val now = DateHelper.now()

    def writes(season: SeasonModel) =
      Json.obj(
        "id" -> season.id,
        "name" -> season.name,
        "clientId" -> season.clientId,
        "startTimestamp" -> season.startTimestamp,
        "endTimestamp" -> season.endTimestamp,
        "isCurrentSeason" -> season.isCurrentSeason
      )
  }

  case class CreateSeasonApiModel(
    name: Option[String],
    clientId: Int,
    startTimestamp: Option[Timestamp],
    endTimestamp: Option[Timestamp]
  )

  implicit val createSeasonReads: Reads[CreateSeasonApiModel] = (
    (JsPath \ "name").readNullable[String] and
      (JsPath \ "clientId").read[Int] and
      (JsPath \ "startTimestamp").readNullableWithDefault[Timestamp](None) and
      (JsPath \ "endTimestamp").readNullableWithDefault[Timestamp](None)
  )(CreateSeasonApiModel.apply _)
}
