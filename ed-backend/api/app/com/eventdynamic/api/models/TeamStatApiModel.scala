package com.eventdynamic.api.models

import com.eventdynamic.models.{
  MLSSeasonStat,
  NCAABSeasonStat,
  NCAAFSeasonStat,
  NFLSeasonStat,
  TeamStat
}
import play.api.libs.json.Json

trait TeamStatApiModel {
  implicit val teamStatsWrites = Json.writes[TeamStat]
  implicit val nflSeasonStatWrites = Json.writes[NFLSeasonStat]
  implicit val mlsSeasonStatWrites = Json.writes[MLSSeasonStat]
  implicit val ncaafSeasonStatWrites = Json.writes[NCAAFSeasonStat]
  implicit val ncaabSeasonStatWrites = Json.writes[NCAABSeasonStat]
}
