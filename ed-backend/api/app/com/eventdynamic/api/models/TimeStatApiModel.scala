package com.eventdynamic.api.models

import com.eventdynamic.models.{TimeStat, TimeStatsMeta, TimeStatsResponse}
import play.api.libs.json.{Json, OFormat, Reads, Writes}

trait TimeStatApiModel extends TimestampFormat {
  implicit val timeStatWrites: Writes[TimeStat] = (timeStat: TimeStat) =>
    Json.obj(
      "timestamp" -> timeStat.intervalStart,
      "inventory" -> timeStat.cumulativeInventory,
      "revenue" -> timeStat.cumulativeRevenue,
      "periodicInventory" -> timeStat.inventory,
      "periodicRevenue" -> timeStat.revenue,
      "isProjected" -> timeStat.isProjected
  )

  implicit val timeStatReads: Reads[TimeStat] = Json.reads[TimeStat]

  implicit val timeStatsMetaFormat: OFormat[TimeStatsMeta] = Json.format[TimeStatsMeta]

  implicit val timeStatResponseFormat: OFormat[TimeStatsResponse] = Json.format[TimeStatsResponse]
}
