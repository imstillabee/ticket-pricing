package com.eventdynamic.api.models

import java.sql.Timestamp
import java.time.Instant

import play.api.libs.json.Json._
import play.api.libs.json._

/**
  * Trait to help parse timestamps from json.
  */
trait TimestampFormat {
  implicit val timestampFormat: Format[Timestamp] = new Format[Timestamp] {
    def writes(t: Timestamp): JsValue = toJson(t.toInstant.toString)

    def reads(json: JsValue): JsResult[Timestamp] = JsSuccess(
      Timestamp.from(Instant.parse(json.as[String]))
    )
  }
}
