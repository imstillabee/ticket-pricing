package com.eventdynamic.api.models

import com.eventdynamic.models.User
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads.{email, minLength}
import play.api.libs.json.{JsPath, Json, Reads, Writes}

trait UserApiModel {
  // API view of the User, implicitly used by Json.toJson
  implicit val userWrites = new Writes[User] {

    def writes(user: User) = Json.obj(
      "id" -> user.id,
      "firstName" -> user.firstName,
      "lastName" -> user.lastName,
      "email" -> user.email,
      "createdAt" -> user.createdAt,
      "modifiedAt" -> user.modifiedAt,
      "phoneNumber" -> user.phoneNumber,
      "isAdmin" -> user.isAdmin,
      "activeClientId" -> user.activeClientId
    )
  }

  case class CreateUserApiModel(
    email: String,
    firstName: String,
    lastName: String,
    clientId: Option[Int],
    phoneNumber: Option[String],
    isAdmin: Boolean
  )

  implicit val createUserReads: Reads[CreateUserApiModel] = (
    (JsPath \ "email").read[String](email) and
      (JsPath \ "firstName").read[String] and
      (JsPath \ "lastName").read[String] and
      (JsPath \ "clientId").readNullable[Int] and
      (JsPath \ "phoneNumber").readNullable[String] and
      (JsPath \ "isAdmin").read[Boolean]
  )(CreateUserApiModel.apply _)

  case class UpdateContactInfoApiModel(
    firstName: String,
    lastName: String,
    phoneNumber: Option[String]
  )

  implicit val updateContactInfoReads: Reads[UpdateContactInfoApiModel] = (
    (JsPath \ "firstName").read[String] and
      (JsPath \ "lastName").read[String] and
      (JsPath \ "phoneNumber").readNullable[String]
  )(UpdateContactInfoApiModel.apply _)

  case class ChangePasswordApiModel(password: String, newPassword: String)

  implicit val changePasswordInfoReads: Reads[ChangePasswordApiModel] = (
    (JsPath \ "password").read[String](minLength[String](6)) and
      (JsPath \ "newPassword").read[String](minLength[String](6))
  )(ChangePasswordApiModel.apply _)

  case class UpdateEmailInfoApiModel(email: String, password: String)

  implicit val updateEmailInfoReads: Reads[UpdateEmailInfoApiModel] = (
    (JsPath \ "email").read[String](email) and
      (JsPath \ "password").read[String](minLength[String](6))
  )(UpdateEmailInfoApiModel.apply _)

}
