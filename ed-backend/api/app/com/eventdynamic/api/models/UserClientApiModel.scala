package com.eventdynamic.api.models

import play.api.libs.json.{Json, Writes}
import utils.silhouette.UserClientIdentity

trait UserClientApiModel {
  // API view of the User with client id, implicitly used by Json.toJson
  implicit val userClientWrites = new Writes[UserClientIdentity] {

    def writes(userClientIdentity: UserClientIdentity) = {
      val user = userClientIdentity.user

      Json.obj(
        "id" -> user.id,
        "firstName" -> user.firstName,
        "lastName" -> user.lastName,
        "email" -> user.email,
        "createdAt" -> user.createdAt,
        "modifiedAt" -> user.modifiedAt,
        "phoneNumber" -> user.phoneNumber,
        "clientId" -> user.activeClientId,
        "clientIds" -> userClientIdentity.clientIds,
        "isAdmin" -> user.isAdmin
      )
    }
  }
}
