package com.eventdynamic.api.models

import com.eventdynamic.models.Venue
import play.api.libs.json.{Json, Writes}

trait VenueApiModel {
  implicit val venueWrites = new Writes[Venue] {

    def writes(venue: Venue) =
      Json.obj("id" -> venue.id, "name" -> venue.name, "svgUrl" -> venue.svgUrl)
  }
}
