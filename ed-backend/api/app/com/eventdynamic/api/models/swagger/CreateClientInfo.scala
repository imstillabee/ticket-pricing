package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class CreateClientInfo(
  @ApiModelProperty(value = "name", example = "K Street Ballers") name: String,
  @ApiModelProperty(value = "pricingInterval", example = "15") pricingInterval: Int
)
