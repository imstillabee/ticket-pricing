package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class CreateUserInfo(
  @ApiModelProperty(value = "Email", example = "sample@dialexa.com") email: String,
  @ApiModelProperty(value = "Password", example = "123456") password: String,
  @ApiModelProperty(value = "First Name", example = "Test") firstName: String,
  @ApiModelProperty(value = "Last Name", example = "User") lastName: String,
  @ApiModelProperty(value = "Client Id", example = "1") clientId: Int,
  @ApiModelProperty(value = "Phone Number", example = "1234567890") phoneNumber: String,
  @ApiModelProperty(value = "Is Admin", example = "false") isAdmin: Boolean
)
