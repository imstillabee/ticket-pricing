package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class DeleteUserInfo(
  @ApiModelProperty(value = "Email", example = "sample@dialexa.com") email: String
)
