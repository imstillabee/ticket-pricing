package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class EventCreateInfo(
  @ApiModelProperty(value = "Name", example = "Mets @ Astros") name: String,
  @ApiModelProperty(value = "Timestamp", example = "2011-12-03T10:15:30") timestamp: String,
  @ApiModelProperty(value = "ClientId", example = "1") clientId: Int,
  @ApiModelProperty(value = "VenueId", example = "1") venueId: Int,
  @ApiModelProperty(value = "SeasonId", example = "1") seasonId: Int,
  @ApiModelProperty(value = "eventCategoryId", example = "1") eventCategoryId: Int,
  @ApiModelProperty(value = "percentPriceModifier", example = "0") percentPriceModifier: Int,
  @ApiModelProperty(value = "IsBroadcast", example = "true") isBroadcast: Boolean,
)
