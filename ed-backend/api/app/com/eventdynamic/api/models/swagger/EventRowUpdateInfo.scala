package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class EventRowUpdateInfo(
  @ApiModelProperty(value = "eventSeatIds", example = "[1,7,11,19,23,27]") eventSeatIds: Array[Int],
  @ApiModelProperty(value = "overridePrice", example = "45.00") overridePrice: Option[Float],
  @ApiModelProperty(value = "isListed", example = "true") isListed: Option[Boolean]
)
