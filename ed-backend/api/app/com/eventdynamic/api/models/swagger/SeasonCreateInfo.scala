package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class SeasonCreateInfo(
  @ApiModelProperty(value = "Name", example = "MLB Mets 2018") name: String,
  @ApiModelProperty(value = "ClientId", example = "1") clientId: String
)
