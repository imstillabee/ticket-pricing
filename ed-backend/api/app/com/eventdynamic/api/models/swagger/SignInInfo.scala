package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class SignInInfo(
  @ApiModelProperty(value = "Email", example = "root@dialexa.com") email: String,
  @ApiModelProperty(value = "Password", example = "") password: String
)
