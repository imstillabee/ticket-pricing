package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class ToggleIsBroadcast(
  @ApiModelProperty(value = "isBroadcast", example = "true") isBroadcast: Boolean
)
