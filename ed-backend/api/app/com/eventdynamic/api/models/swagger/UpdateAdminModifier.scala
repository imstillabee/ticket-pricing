package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class UpdateAdminModifier(
  @ApiModelProperty(value = "eventScoreModifier", example = "1.50") eventScoreModifier: Double,
  @ApiModelProperty(value = "springModifier", example = "1.50") springModifier: Double
)
