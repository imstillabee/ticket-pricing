package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class UpdateClientInfo(
  @ApiModelProperty(value = "name", example = "New Name") name: String,
  @ApiModelProperty(value = "pricingInterval", example = "15") pricingInterval: Int
)
