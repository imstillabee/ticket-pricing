package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class UpdateClientIntegrationInfo(
  @ApiModelProperty(value = "percent", example = "5") percent: Int,
  @ApiModelProperty(value = "constant", example = "5") constant: BigDecimal
)
