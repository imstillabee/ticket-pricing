package com.eventdynamic.api.models.swagger

import io.swagger.annotations.ApiModelProperty

case class UpdatePercentPriceModifier(
  @ApiModelProperty(value = "percentPriceModifier", example = "0") percentPriceModifier: Int
)
