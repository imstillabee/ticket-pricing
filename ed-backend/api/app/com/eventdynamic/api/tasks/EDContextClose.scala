package com.eventdynamic.api.tasks

import com.eventdynamic.db.EDContext
import javax.inject.{Inject, Singleton}
import org.slf4j.LoggerFactory
import play.api.inject.ApplicationLifecycle

@Singleton
class EDContextClose @Inject()(lifecycle: ApplicationLifecycle, ctx: EDContext) {
  private val logger = LoggerFactory.getLogger(this.getClass)

  logger.info("Adding EDContext close stop hook")
  ctx.registerShutdownHook()

  // This is how play wants to add shutdown hooks, but it's not working
  // Keeping here in case the new method above doesn't work either
//  lifecycle.addStopHook { () =>
//    {
//      println("Closing EDContext database connections")
//      Future.successful(ctx.db.close())
//    }
//  }
}
