package com.eventdynamic.api.tasks

import akka.actor.{Actor, ActorRef, ActorSystem}
import com.eventdynamic.services.MaterializedViewService
import com.google.inject.Inject
import com.google.inject.name.Named
import org.slf4j.LoggerFactory
import play.api.Configuration

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
  * Task to set up play's actor system to periodically refresh materialized views
  *
  * @param actorSystem
  * @param actor
  * @param conf
  * @param ec
  */
class MaterializedViewRefreshTask @Inject()(
  actorSystem: ActorSystem,
  @Named("refresh-actor") actor: ActorRef,
  conf: Configuration
)(implicit ec: ExecutionContext) {
  private val logger = LoggerFactory.getLogger(this.getClass)

  // Get interval from config, default to an hour
  val interval: Int = conf.getOptional[Int]("application.refreshInterval").getOrElse(60)

  if (interval <= 0)
    logger.info("Refreshing materialized views disabled.")
  else {
    logger.info(s"Refreshing materialized views every $interval minutes")
    actorSystem.scheduler.schedule(
      initialDelay = 0.seconds,
      interval = interval.minutes,
      receiver = actor,
      message = Refresh()
    )
  }
}

/**
  * Case class for the RefreshActor
  */
case class Refresh()

/**
  * Actor that refreshes the materialized views periodically
  *
  * @param materializedViewService
  * @param ec
  */
class RefreshActor @Inject()(materializedViewService: MaterializedViewService)(
  implicit ec: ExecutionContext
) extends Actor {

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case Refresh() =>
      logger.debug("Starting to refresh materialized views")
      materializedViewService.refreshAll().onComplete {
        case Success(res) =>
          logger.debug(s"Materialized views refreshed ($res)")
        case Failure(res) =>
          logger.error(s"Materialized view failed to refresh ($res)")
      }
    case msg =>
      logger.error(s"Received unknown message: $msg")
  }
}
