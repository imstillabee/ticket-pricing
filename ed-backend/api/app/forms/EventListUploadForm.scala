package forms

import play.api.data._
import play.api.data.Forms._

object EventListUploadForm {
  val form = Form(mapping("venueId" -> number, "clientId" -> number)(Data.apply)(Data.unapply))

  case class Data(venueId: Int, clientId: Int)
}
