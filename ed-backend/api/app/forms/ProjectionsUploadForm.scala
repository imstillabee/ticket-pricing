package forms

import play.api.data.Forms._
import play.api.data._

object ProjectionsUploadForm {
  val form = Form(mapping("eventId" -> number)(Data.apply)(Data.unapply))

  case class Data(eventId: Int)

}
