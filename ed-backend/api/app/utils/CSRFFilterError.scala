package utils

import play.api.mvc._
import play.api.mvc.Results.Forbidden
import play.api.libs.json.Json
import scala.concurrent.Future

class CSRFFilterError extends play.filters.csrf.CSRF.ErrorHandler {

  def handle(req: RequestHeader, msg: String): Future[Result] = {
    val result = Forbidden(Json.toJson("Forbidden"))

    Future.successful(result)
  }
}
