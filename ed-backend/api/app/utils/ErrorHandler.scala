package utils

import com.eventdynamic.api.models.Message
import com.mohiva.play.silhouette.api.actions.{SecuredErrorHandler, UnsecuredErrorHandler}
import javax.inject.{Inject, Provider, Singleton}
import play.api._
import play.api.http.DefaultHttpErrorHandler
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc.Results._
import play.api.mvc._
import play.api.routing.Router

import scala.concurrent.Future

@Singleton
class ErrorHandler @Inject()(
  env: Environment,
  config: Configuration,
  sourceMapper: OptionalSourceMapper,
  router: Provider[Router],
  val messagesApi: MessagesApi
) extends DefaultHttpErrorHandler(env, config, sourceMapper, router)
    with SecuredErrorHandler
    with UnsecuredErrorHandler
    with I18nSupport
    with Message {

  // 401 - Unauthorized
  override def onNotAuthenticated(implicit request: RequestHeader): Future[Result] =
    Future.successful {
      Unauthorized(message("Unauthenticated"))
    }

  // 403 - Forbidden
  override def onNotAuthorized(implicit request: RequestHeader): Future[Result] =
    Future.successful {
      Forbidden(message("Access Denied"))
    }

  // 404 - page not found error
  override def onNotFound(request: RequestHeader, message: String): Future[Result] =
    Future.successful {
      NotFound(env.mode match {
        case Mode.Prod => Json.obj("message" -> "Page Not Found")
        case _         => Json.obj("message" -> "Page Not Found")
      })
    }

  // 500 - internal server error
  override def onProdServerError(request: RequestHeader, exception: UsefulException) =
    Future.successful {
      InternalServerError("Internal Server Error")
    }
}
