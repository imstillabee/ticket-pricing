package utils.silhouette

import com.eventdynamic.services.{EventService, SeasonService}
import com.mohiva.play.silhouette.api.Authorization
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import play.api.mvc.Request

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class Admin() extends Authorization[UserClientIdentity, CookieAuthenticator] {

  def isAuthorized[A](userClientIdentity: UserClientIdentity, authenticator: CookieAuthenticator)(
    implicit r: Request[A]
  ): Future[Boolean] =
    Future.successful(userClientIdentity.user.isAdmin)
}

case class WithEvent(id: Int, eventService: EventService)
    extends Authorization[UserClientIdentity, CookieAuthenticator] {

  def isAuthorized[A](userClientIdentity: UserClientIdentity, authenticator: CookieAuthenticator)(
    implicit r: Request[A]
  ): Future[Boolean] = {
    eventService.getById(id).map {
      case Some(event) => userClientIdentity.clientIds.contains(event.clientId)
      case None        => false
    }
  }
}

case class WithSeason(id: Int, seasonService: SeasonService)
    extends Authorization[UserClientIdentity, CookieAuthenticator] {

  def isAuthorized[A](userClientIdentity: UserClientIdentity, authenticator: CookieAuthenticator)(
    implicit r: Request[A]
  ): Future[Boolean] = {
    seasonService
      .getById(id, Some(userClientIdentity.user.activeClientId), userClientIdentity.user.isAdmin)
      .map {
        case Some(season) => userClientIdentity.clientIds.contains(season.clientId)
        case None         => false
      }
  }
}

case class WithClient(id: Int) extends Authorization[UserClientIdentity, CookieAuthenticator] {

  def isAuthorized[A](userClientIdentity: UserClientIdentity, authenticator: CookieAuthenticator)(
    implicit r: Request[A]
  ): Future[Boolean] =
    Future.successful(userClientIdentity.user.isAdmin || userClientIdentity.clientIds.contains(id))
}

case class WithVenue(id: Int, eventService: EventService)
    extends Authorization[UserClientIdentity, CookieAuthenticator] {

  override def isAuthorized[A](
    userClientIdentity: UserClientIdentity,
    authenticator: CookieAuthenticator
  )(implicit r: Request[A]): Future[Boolean] = {
    if (userClientIdentity.user.isAdmin) return Future.successful(true)

    for {
      events <- eventService.getByClientAndVenue(userClientIdentity.user.activeClientId, id)
    } yield events.nonEmpty
  }
}
