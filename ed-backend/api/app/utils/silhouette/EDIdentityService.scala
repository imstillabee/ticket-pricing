package utils.silhouette

import com.eventdynamic.services.{UserClientService, UserService}
import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.services.IdentityService
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class EDIdentityService @Inject()(userService: UserService, userClientService: UserClientService)(
  implicit ec: ExecutionContext
) extends IdentityService[UserClientIdentity] {
  override def retrieve(loginInfo: LoginInfo): Future[Option[UserClientIdentity]] = {
    for {
      user <- userService.getByEmail(loginInfo.providerKey)
      userId = user.flatMap(u => u.id).headOption
      clientIds <- userClientService.getAllClientIdsForUser(userId)
    } yield {
      if (clientIds.isEmpty)
        None
      else
        Some(UserClientIdentity(user.get, clientIds))
    }
  }
}
