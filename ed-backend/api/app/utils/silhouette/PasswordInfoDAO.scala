package utils.silhouette

import java.sql.Timestamp
import javax.inject.Inject
import com.eventdynamic.services.UserService
import com.eventdynamic.utils.DateHelper
import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.password.BCryptPasswordHasher
import com.mohiva.play.silhouette.persistence.daos.DelegableAuthInfoDAO

import scala.concurrent.{ExecutionContext, Future}

case class PasswordInfoDAO @Inject()(protected val userService: UserService)(
  implicit ec: ExecutionContext
) extends DelegableAuthInfoDAO[PasswordInfo] {

  def add(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] =
    update(loginInfo, authInfo)

  def find(loginInfo: LoginInfo): Future[Option[PasswordInfo]] =
    userService.getByEmail(loginInfo.providerKey).map {
      case Some(user) => {
        val expires: Long = user.tempExpire.getOrElse[Timestamp](DateHelper.now()).getTime()
        val now: Long = DateHelper.now().getTime()
        var output = Some(PasswordInfo(BCryptPasswordHasher.ID, user.password.getOrElse(""), None))
        // if tempExpire is in the future, check the temporary password instead of the normal one
        if (expires > now) {
          output =
            Some(PasswordInfo(BCryptPasswordHasher.ID, user.tempPass.getOrElse[String](""), None))
        }
        output
      }
      case _ => None
    }

  def remove(loginInfo: LoginInfo): Future[Unit] =
    userService.deleteByEmail(loginInfo.providerKey).map(_ => Unit)

  def save(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] =
    find(loginInfo).flatMap {
      case Some(_) => update(loginInfo, authInfo)
      case None    => add(loginInfo, authInfo)
    }

  def update(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] =
    userService.getByEmail(loginInfo.providerKey).map {
      case Some(user) => {
        userService.update(user.copy(password = Some(authInfo.password)))
        authInfo
      }
      case _ =>
        throw new Exception("PasswordInfoDAO - update : the user must exist to update password")
    }

}
