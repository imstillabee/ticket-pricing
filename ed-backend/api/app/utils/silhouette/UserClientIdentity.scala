package utils.silhouette

import com.eventdynamic.models.User
import com.mohiva.play.silhouette.api.Identity

case class UserClientIdentity(user: User, clientIds: Seq[Int]) extends Identity
