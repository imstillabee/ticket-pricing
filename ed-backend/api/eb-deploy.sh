#!/bin/bash

set -x
set -e

STACK=$1

AWS_CONFIG_FILE=$HOME/.aws/config

mkdir $HOME/.aws
touch $AWS_CONFIG_FILE
chmod 600 $AWS_CONFIG_FILE

echo "[profile eb-cli]"                              > $AWS_CONFIG_FILE
echo "aws_access_key_id=$AWS_ACCESS_KEY_ID"         >> $AWS_CONFIG_FILE
echo "aws_secret_access_key=$AWS_SECRET_ACCESS_KEY" >> $AWS_CONFIG_FILE

if [[ "$STACK" == "qa" ]]; then
eb deploy ed-api-$STACK | tee $CIRCLE_ARTIFACTS/eb_deploy_output.txt
else if [["$STACK" == "prod" ]]; then
eb deploy ed-api | tee $CIRCLE_ARTIFACTS/eb_deploy_output.txt
fi

grep -v -c -q -i error $CIRCLE_ARTIFACTS/eb_deploy_output.txt