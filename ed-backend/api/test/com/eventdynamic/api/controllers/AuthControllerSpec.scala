package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import com.eventdynamic.api.models.Message
import com.mohiva.play.silhouette.api.util.Credentials
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.test.{FakeRequest, Injecting, WithApplication}
import com.mohiva.play.silhouette.test._
import org.specs2.mock.Mockito
import play.api.libs.json.Json
import play.api.test.Helpers._

import scala.concurrent.Future

/**
  * Add your spec here.
  * You can mock out a whole application including requests, plugins etc.
  *
  * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
  */
class AuthControllerSpec extends TestContext with GuiceOneAppPerTest with Injecting with Mockito {

  "The isAuthenticated method" should {
    "return status 401 if not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[AuthController]
      val result = controller.isAuthenticated(request)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 200 if authenticated" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[AuthController]
      val result = controller.isAuthenticated(request)

      status(result) mustBe OK
    }
  }

  "The signIn method" should {
    "return status 400 if body data is wrong" in new WithApplication(application) {
      val request = FakeRequest().withBody(
        Json.parse("""{ "wrong": "root@dialexa.com", "credentials": "123456" }""")
      )
      val controller = inject[AuthController]
      val result = controller.signIn(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid request body\"}"
    }

    "return status 400 with error if email password incorrect" in new WithApplication(application) {
      val request = FakeRequest().withBody(
        Json.parse("""{ "email": "test@dialexa.com", "password": "123456" }""")
      )
      val controller = inject[AuthController]
      val result = controller.signIn(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Incorrect Email or Password\"}"
    }

    "return status 400 with errors if credentials could not be validated" in new WithApplication(
      application
    ) {
      val request = FakeRequest().withBody(
        Json.parse("""{ "email": "test@dialexa.com", "password": "123456" }""")
      )
      val controller = inject[AuthController]
      val result = controller.signIn(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Incorrect Email or Password\"}"
    }

    "return status 200 if user successfully logs in" in new WithApplication(application) {
      credentialsProvider.authenticate(any[Credentials]) returns Future
        .successful(loginInfo)
      userService.getByEmail(any[String]) returns Future.successful(Some(identity))
      userClientService.getAllClientIdsForUser(any[Option[Int]]) returns Future.successful(Seq(1))
      userClientService.getDefaultClientIdForUserById(any[Option[Int]]) returns Future.successful(
        Some(1)
      )
      userService.setActiveClient(any[Int], any[Int]) returns Future.successful(Some(identity))

      val request = FakeRequest().withBody(
        Json.parse("""{ "email": "root@dialexa.com", "password": "3v3ntDynamicRoot!" }""")
      )
      val controller = inject[AuthController]
      val result = controller.signIn(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }

    "return status 200 if user successfully logs in with temporary password" in new WithApplication(
      application
    ) {
      credentialsProvider.authenticate(any[Credentials]) returns Future
        .successful(loginInfo)
      userService.getByEmail(any[String]) returns Future.successful(Some(identity))
      userClientService.getAllClientIdsForUser(any[Option[Int]]) returns Future.successful(Seq(1))
      userClientService.getDefaultClientIdForUserById(any[Option[Int]]) returns Future.successful(
        Some(1)
      )
      userService.setActiveClient(any[Int], any[Int]) returns Future.successful(Some(identity))

      val request = FakeRequest().withBody(
        Json.parse("""{ "email": "new@dialexa.com", "password": "temporary" }""")
      )
      val controller = inject[AuthController]
      val result = controller.signIn(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }

  }

  "The logout user method" should {
    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[AuthController]
      val result = controller.logout(request)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 200 if user successfully logged out" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[AuthController]
      val result = controller.logout(request)

      status(result) mustBe OK
    }
  }
}
