package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import com.eventdynamic.api.models.BuyerTypeBulkModification
import com.eventdynamic.models.{ClientIntegrationComposite, TDCClientIntegrationConfig}
import com.eventdynamic.tdcreplicatedproxy.models.TdcBuyerType
import com.softwaremill.sttp.Response
import play.api.test.{FakeRequest, Injecting}
import com.mohiva.play.silhouette.test._
import play.api.libs.json.Json
import play.api.test.Helpers._

import scala.concurrent.Future

class BuyerTypesControllerSpec extends TestContext with Injecting {
  private def successResponse[T](body: T): Response[T] = {
    Response(Right(body), 200, "success", scala.collection.immutable.Seq(), List())
  }

  "BuyerTypesController#list" should {
    val buyerTypes =
      Seq(
        TdcBuyerType(123, "Adult", true, "Adult buyer type", true),
        TdcBuyerType(345, "Child", true, "Child buyer type", true)
      )

    val tdcClientIntegrationComposite = ClientIntegrationComposite(
      Some(1),
      now,
      now,
      1,
      1,
      "New York Mets",
      Some("v1"),
      Some("""{
             |    "agent": "agent",
             |    "apiKey": "apiKey",
             |    "appId": "appId",
             |    "baseUrl": "https://baseUrl",
             |    "password": "password",
             |    "username": "username",
             |    "supplierId": 1,
             |    "tdcProxyBaseUrl": "https://tdcProxyBaseUrl",
             |    "mlbamGridConfigs": [{
             |        "priceGroupId": 1,
             |        "externalBuyerTypeIds": ["test"]
             |    }]
             |}""".stripMargin),
      true,
      true,
      Some("http://eventdynamic.com/assets/logo.png"),
      Some(0),
      None
    )

    clientIntegrationService
      .getByName(any[Int], any[String], any[Boolean]) returns Future.successful(
      Some(tdcClientIntegrationComposite)
    )

    eventService.getAll(any[Option[Int]], any[Boolean]) returns Future.successful(Seq(event.get))

    integrationServiceBuilder.buildTdcReplicatedProxyService(any[TDCClientIntegrationConfig]) returns tdcReplicatedProxyService

    tdcReplicatedProxyService.listBuyerTypes(any[Seq[Int]]) returns Future
      .successful(successResponse(buyerTypes))

    "return status 200 and default to disabled is true" in {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[BuyerTypesController]
      val result = controller.list(None)(request)
      val body = contentAsJson(result)

      status(result) mustBe OK
      body mustBe Json.parse("""
          |[
          |  {
          |    "id": "123",
          |    "publicDescription": "Adult buyer type",
          |    "code": "Adult",
          |    "active": true,
          |    "isInPriceStructure": true
          |  },
          |  {
          |    "id": "345",
          |    "publicDescription": "Child buyer type",
          |    "code": "Child",
          |    "active": true,
          |    "isInPriceStructure": true
          |  }
          |]
        """.stripMargin)
    }

    "return status 401 if the user is not authenticated" in {
      val request = FakeRequest()
      val controller = inject[BuyerTypesController]
      val result = controller.list(None)(request)

      status(result) mustBe UNAUTHORIZED
    }
  }
}
