package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import com.eventdynamic.models.ClientIntegrationComposite
import com.eventdynamic.utils.DateHelper
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.test.{FakeRequest, Injecting, WithApplication}
import play.api.test.Helpers._
import com.mohiva.play.silhouette.test._
import play.api.libs.json.JsResult.Exception
import play.api.libs.json.{JsError, Json}

import scala.concurrent.Future

class ClientIntegrationsControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito {
  "ClientIntegrationsController#getById" should {
    "return status 200 if clientIntegration is returned" in new WithApplication(application) {
      clientIntegrationService.getById(any[Int]) returns Future.successful(
        Some(clientIntegrationComposite)
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[ClientIntegrationsController]
      val result = controller.getById(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }

    "return status 401 if user is not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[ClientIntegrationsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 404 if clientIntegration could not be found" in new WithApplication(application) {
      clientIntegrationService.getById(any[Int]) returns Future.successful(None)

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[ClientIntegrationsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"ClientIntegration not found\"}"
    }
  }

  "ClientIntegrationsController#get" should {
    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[ClientIntegrationsController]
      val result = controller.get()(request)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 200 with all integrations for user's client" in new WithApplication(application) {
      clientIntegrationService.getByClientId(any[Int], any[Boolean], any[Boolean]) returns Future
        .successful(Seq(clientIntegrationComposite))
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[ClientIntegrationsController]
      val result = controller.get()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
    }
  }

  "ClientIntegrationsController#saveSettings" should {
    "return status 401 if user is not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
        .withBody(Json.parse("""{"percent": 5}"""))
      val controller = inject[ClientIntegrationsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 404 if clientId doesn't match clientId on client integration" in new WithApplication(
      application
    ) {
      clientIntegrationService.getById(any[Int]) returns Future
        .successful(
          Option(
            ClientIntegrationComposite(
              Some(1),
              DateHelper.now(),
              DateHelper.now(),
              2,
              1,
              "TDC",
              None,
              None,
              true,
              false,
              Some("string"),
              Some(5),
              None
            )
          )
        )
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{"percent": 5}"""))
      val controller = inject[ClientIntegrationsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
    }

    "return status 404 if clientIntegration doesn't not exist" in new WithApplication(application) {
      clientIntegrationService.getById(any[Int]) returns Future
        .successful(None)

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{"percent": 5}"""))
      val controller = inject[ClientIntegrationsController]
      val result = controller.update(-1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"ClientIntegration not found\"}"
    }

    "return status 422 if clientIntegration is a primary" in new WithApplication(application) {
      clientIntegrationService.getById(any[Int]) returns Future
        .successful(
          Option(
            ClientIntegrationComposite(
              Some(1),
              DateHelper.now(),
              DateHelper.now(),
              1,
              1,
              "TDC",
              None,
              None,
              true,
              true,
              Some("string"),
              Some(5),
              None
            )
          )
        )

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{"percent": 5}"""))
      val controller = inject[ClientIntegrationsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNPROCESSABLE_ENTITY
      bodyText mustBe "{\"message\":\"Cannot add Secondary Pricing rule to a Primary Integration\"}"
    }

    "return status 200 if a client integration could be updated" in new WithApplication(application) {
      clientIntegrationService.getById(any[Int]) returns Future
        .successful(
          Option(
            ClientIntegrationComposite(
              Some(1),
              DateHelper.now(),
              DateHelper.now(),
              1,
              1,
              "TDC",
              None,
              None,
              true,
              false,
              Some("string"),
              None,
              None
            )
          )
        )

      secondaryPricingRuleService.saveSettings(1, Some(5), None) returns Future.successful(1)

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{"percent": 5}"""))
      val controller = inject[ClientIntegrationsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText mustBe "{\"message\":\"ClientIntegration Updated\"}"
    }

    "return status 400 if a percent parameter is invalid" in new WithApplication(application) {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{"percent": -200}"""))
      val controller = inject[ClientIntegrationsController]
      val result = controller.update(1)(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 500 if a failure occurred while trying to update" in new WithApplication(
      application
    ) {
      secondaryPricingRuleService
        .saveSettings(any[Int], any[Option[Int]], any[Option[BigDecimal]]) returns Future.failed(
        new Exception(JsError("message"))
      )

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{"percent": 5}"""))
      val controller = inject[ClientIntegrationsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe INTERNAL_SERVER_ERROR
      bodyText mustBe "{\"message\":\"Error processing request\"}"
    }
  }
}
