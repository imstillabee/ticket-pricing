package com.eventdynamic.api.controllers
import com.eventdynamic.api.controllers.helpers.TestContext
import com.eventdynamic.utils._
import com.mohiva.play.silhouette.test._
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.test.{FakeRequest, Injecting, WithApplication}
import play.api.test.Helpers._
import play.api.libs.json.Json

import scala.concurrent.Future

class ClientsControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito {
  "ClientsController#getById(id: Int)" should {
    "return 401 if unauthenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[ClientsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 403 if user is not admin or have matching client id" in new WithApplication(
      application
    ) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[ClientsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe FORBIDDEN
      bodyText mustBe "{\"message\":\"Access Denied\"}"
    }

    "return status 404 if client could not be found" in new WithApplication(application) {
      clientService.getById(2) returns Future.successful(None)

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[ClientsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Client does not exist or could not be found\"}"
    }

    "return status 200 if client is returned" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[ClientsController]
      val result = controller.getById(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }
  }

  "ClientsController#get" should {
    "return 401 if unauthenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[ClientsController]
      val result = controller.get(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 200 if user is not admin" in new WithApplication(application) {
      clientService.getAll(1) returns Future.successful(Seq(client.get))

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[ClientsController]
      val result = controller.get(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }

    "return status 200 if client is returned" in new WithApplication(application) {
      clientService.getAll(4) returns Future.successful(Seq(client.get))

      val request = FakeRequest()
        .withAuthenticator(adminLoginInfo)
      val controller = inject[ClientsController]
      val result = controller.get(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }
  }

  "ClientsController#setActive" should {
    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[ClientsController]
      val result = controller.setActive(1)(request)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 400 if user unsuccessfully switches clients" in new WithApplication(application) {
      userService.setActiveClient(any[Int], any[Int]) returns Future.successful(None)

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[ClientsController]
      val result = controller.setActive(1)(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 200 if user successfully switches clients" in new WithApplication(application) {
      userService.setActiveClient(any[Int], any[Int]) returns Future.successful(Some(identity))

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[ClientsController]
      val result = controller.setActive(1)(request)

      status(result) mustBe OK
    }
  }
}
