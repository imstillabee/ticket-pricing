package com.eventdynamic.api.controllers

import java.net.URLEncoder

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.Helpers._
import play.api.test._

/**
  * Add your spec here.
  * You can mock out a whole application including requests, plugins etc.
  *
  * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
  */
class DocsControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  override def fakeApplication() =
    new GuiceApplicationBuilder()
      .configure(Map("application.baseUrl" -> "http://localhost:9000"))
      .build()

  "DocsController GET" should {
    "call the docs endpoint from the application" in {
      val controller = inject[DocsController]
      val docs = controller.redirectDocs().apply(FakeRequest(GET, "/"))

      status(docs) mustBe SEE_OTHER
      val baseUrl: String = app.configuration.get[String]("application.baseUrl")
      val encodedUrl: String =
        URLEncoder.encode(baseUrl + "swagger.json", "UTF-8")
      redirectLocation(docs) mustBe Some("/assets/lib/swagger-ui/index.html?url=" + encodedUrl)
    }

  }
}
