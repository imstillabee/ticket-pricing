package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.test.{FakeRequest, Injecting, WithApplication}
import com.mohiva.play.silhouette.test._
import play.api.test.Helpers._

import scala.concurrent.Future

class EventCategoriesControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito {

  "EventCategoriesController#list" should {
    "return 200 for success" in new WithApplication(application) {
      eventCategoryService.getAllForClient(1) returns Future.successful(Seq(eventCategory.get))

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[EventCategoriesController]
      val result = controller.list()(request)

      status(result) mustBe OK
    }

    "return 401 if the user is not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[EventCategoriesController]
      val result = controller.list()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }
  }

}
