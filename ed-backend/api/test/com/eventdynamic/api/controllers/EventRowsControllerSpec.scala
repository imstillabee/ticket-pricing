package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import com.eventdynamic.models.EventSeat
import com.eventdynamic.utils.SuccessResponse
import com.mohiva.play.silhouette.test._
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.test.{FakeRequest, Injecting, WithApplication}
import play.api.test.Helpers._
import play.api.libs.json.Json
import scala.concurrent.Future

class EventRowsControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito {

  "The EventRowsController #getAll(eventId: Int) method" should {
    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[EventRowsController]
      val result = controller.getAll(1)(request)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 404 if no EventRows are found for the specified event" in new WithApplication(
      application
    ) {
      eventRowsTransaction.getAllByEvent(any[Int]) returns Future
        .successful(Seq())

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[EventRowsController]
      val result = controller.getAll(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"No EventRows were found for this event\"}"
    }

    "return status 200 with all eventRows for non admin user with client id" in new WithApplication(
      application
    ) {
      eventRowsTransaction.getAllByEvent(any[Int]) returns Future
        .successful(Seq(eventRow.get))

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[EventRowsController]
      val result = controller.getAll(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
    }
  }

  "The EventRowsController #update method" should {
    "return status 400 if request body is invalid" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[EventRowsController]
      val result = controller.update()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
    }

    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request =
        FakeRequest().withBody(Json.parse("""{ "eventSeatIds": [1,2,3], "isListed": true }"""))
      val controller = inject[EventRowsController]
      val result = controller.update()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 200 if EventSeats successfully Updated" in new WithApplication(application) {
      eventSeatService.bulkUpdate(any[Seq[Int]], any[Option[BigDecimal]], any[Option[Boolean]]) returns Future
        .successful(1)

      val request = FakeRequest()
        .withBody(Json.parse("""{ "eventSeatIds": [1,2], "overridePrice": 35.35 }"""))
        .withAuthenticator(adminLoginInfo)
      val controller = inject[EventRowsController]
      val result = controller.update()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
    }
  }

}
