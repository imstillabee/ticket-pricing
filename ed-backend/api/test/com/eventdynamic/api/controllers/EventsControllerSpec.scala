package com.eventdynamic.api.controllers

import java.sql.Timestamp
import java.time.Instant

import com.eventdynamic.api.controllers.helpers.TestContext
import com.eventdynamic.api.models.{EventPrice, EventPricesModel, EventPricesModelFormat}
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models.ScheduledJobStatus.ScheduledJobStatus
import com.eventdynamic.models.{Event, JobPriorityType, JobTriggerType, JobType, ScheduledJob}
import com.eventdynamic.modelserver.models.ModelServerPriceAll.{
  ModelServerPriceAllResponse,
  ModelServerPriceAllResponseItem
}
import com.eventdynamic.modelserver.models.ModelServerSpringResponse
import com.eventdynamic.utils._
import com.mohiva.play.silhouette.test._
import org.mockito.Mockito.{times, verify}
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Injecting, WithApplication}

import scala.concurrent.Future

class EventsControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito
    with EventPricesModelFormat {
  import EventsControllerSpec._

  "EventsController#getById(id: Int)" should {
    "return status 401 if user is not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[EventsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 403 if user is not admin" in new WithApplication(application) {
      val event =
        Some(
          Event(
            id = Some(1),
            primaryEventId = None,
            createdAt = DateHelper.now(),
            modifiedAt = DateHelper.now(),
            name = "name",
            timestamp = None,
            clientId = 2,
            venueId = 2,
            eventCategoryId = 2,
            seasonId = None,
            isBroadcast = true,
            percentPriceModifier = 0,
            eventScore = None,
            eventScoreModifier = 0,
            spring = None,
            springModifier = 0
          )
        )
      eventService.getById(any[Int]) returns Future.successful(event)

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[EventsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe FORBIDDEN
      bodyText mustBe "{\"message\":\"Access Denied\"}"
    }

    "return status 404 if event could not be found" in new WithApplication(application) {
      eventService.getById(any[Int]) returns Future.successful(None)

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[EventsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Event does not exist or could not be found\"}"
    }

    "return status 200 if event is returned" in new WithApplication(application) {
      eventService.getById(any[Int]) returns Future.successful(event)
      inventoryStatsTransaction.getInventory(any[Int], any[Option[Int]], any[Option[Int]]) returns Future
        .successful(inventoryStats)
      clientIntegrationService.getByClientId(any[Int], any[Boolean], any[Boolean]) returns Future
        .successful(Seq(clientIntegrationComposite))
      scheduledJobQueue.getLatestForEvent(any[Int], any[JobType], any[Option[ScheduledJobStatus]]) returns Future
        .successful(Some(scheduledJobTriggerComposite))
      modelServerService.getAutomatedSpringValue(any[String], any[Double], any[Option[Timestamp]]) returns Future
        .successful(ModelServerSpringResponse(0.002))

      val clientId = 1
      val eventId = 1
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[EventsController]
      val result = controller.getById(1)(request)
      val bodyText = contentAsString(result)

      verify(inventoryStatsTransaction, times(1))
        .getInventory(clientId, Some(eventId), None)

      status(result) mustBe OK
      bodyText must include(""""id":1""")
      bodyText must include(""""totalInventory":25000""")
      bodyText must include(""""unsoldInventory":2500""")
    }
  }

  "EventsController#update" should {
    "return status 400 if request body is invalid" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[EventsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
    }

    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request =
        FakeRequest().withBody(Json.parse("""{  "name": "Mets @ Astros",
                                                            "timestamp": "2011-12-03T10:15:30",
                                                            "clientId": 2,
                                                            "venueId": 1,
                                                            "isBroadcast": true,
                                                            "percentPriceModifier": 0}"""))
      val controller = inject[EventsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 403 if user not admin" in new WithApplication(application) {
      val request = FakeRequest()
        .withBody(Json.parse("""{  "name": "Mets @ Astros",
                                                            "timestamp": "2011-12-03T10:15:30",
                                                            "clientId": 2,
                                                            "venueId": 1,
                                                            "isBroadcast": true,
                                                            "percentPriceModifier": 0}"""))
        .withAuthenticator(loginInfo)
      val controller = inject[EventsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe FORBIDDEN
      bodyText mustBe "{\"message\":\"Access Denied\"}"
    }

    "return status 400 if invalid date or time" in new WithApplication(application) {
      val request = FakeRequest()
        .withBody(Json.parse("""{  "name": "Mets @ Astros",
                                                            "timestamp": "Invalid",
                                                            "clientId": 2,
                                                            "venueId": 1,
                                                            "eventCategoryId": 1,
                                                            "isBroadcast": true,
                                                            "percentPriceModifier": 0}"""))
        .withAuthenticator(adminLoginInfo)
      val controller = inject[EventsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid Date or Time\"}"
    }

    "return status 400 if percentPriceModifier < -100" in new WithApplication(application) {
      val request = FakeRequest()
        .withBody(Json.parse("""{  "name": "Mets @ Astros",
                                                            "timestamp": "2011-12-03T10:15:30",
                                                            "clientId": 2,
                                                            "venueId": 1,
                                                            "eventCategoryId": 1,
                                                            "isBroadcast": true,
                                                            "percentPriceModifier": -101}"""))
        .withAuthenticator(adminLoginInfo)
      val controller = inject[EventsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid request body\"}"
    }

    "return status 400 if percentPriceModifier > 100" in new WithApplication(application) {
      val request = FakeRequest()
        .withBody(Json.parse("""{  "name": "Mets @ Astros",
                                                            "timestamp": "2011-12-03T10:15:30",
                                                            "clientId": 2,
                                                            "venueId": 1,
                                                            "eventCategoryId": 1,
                                                            "isBroadcast": true,
                                                            "percentPriceModifier": 101}"""))
        .withAuthenticator(adminLoginInfo)
      val controller = inject[EventsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid request body\"}"
    }

    "return status 404 if Event does not exist" in new WithApplication(application) {
      eventService.updateUnique(
        any[Int],
        Some(any[Int]),
        any[String],
        Some(any[Timestamp]),
        any[Int],
        any[Int],
        any[Int],
        Some(any[Int]),
        any[Int],
        any[Double],
        any[Double]
      ) returns Future.successful(NotFoundResponse)
      val request = FakeRequest()
        .withBody(Json.parse("""{  "name": "Mets @ Astros",
                                                            "timestamp": "2011-12-03T10:15:30",
                                                            "clientId": 2,
                                                            "venueId": 1,
                                                            "eventCategoryId": 1,
                                                            "isBroadcast": true,
                                                            "percentPriceModifier": 0}"""))
        .withAuthenticator(adminLoginInfo)
      val controller = inject[EventsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Event does not exist\"}"
    }

    "return status 409 if Event already exists" in new WithApplication(application) {
      eventService.updateUnique(
        any[Int],
        Some(any[Int]),
        any[String],
        Some(any[Timestamp]),
        any[Int],
        any[Int],
        any[Int],
        Some(any[Int]),
        any[Int],
        any[Double],
        any[Double]
      ) returns Future.successful(DuplicateResponse)
      val request = FakeRequest()
        .withBody(Json.parse("""{  "name": "Mets @ Astros",
                                                            "timestamp": "2011-12-03T10:15:30",
                                                            "clientId": 2,
                                                            "venueId": 1,
                                                            "eventCategoryId": 1,
                                                            "isBroadcast": true,
                                                            "percentPriceModifier": 0}"""))
        .withAuthenticator(adminLoginInfo)
      val controller = inject[EventsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe CONFLICT
      bodyText mustBe "{\"message\":\"Event Already Exists\"}"
    }

    "return status 200 if Event successfully Updated" in new WithApplication(application) {
      eventService.updateUnique(
        any[Int],
        Some(any[Int]),
        any[String],
        Some(any[Timestamp]),
        any[Int],
        any[Int],
        any[Int],
        Some(any[Int]),
        any[Int],
        any[Double],
        any[Double]
      ) returns Future.successful(SuccessResponse(event.get: Event))
      val request = FakeRequest()
        .withBody(Json.parse("""{  "name": "Mets @ Astros",
                                                            "timestamp": "2011-12-03T10:15:30",
                                                            "clientId": 2,
                                                            "venueId": 1,
                                                            "eventCategoryId": 1,
                                                            "isBroadcast": true,
                                                            "percentPriceModifier": 0}"""))
        .withAuthenticator(adminLoginInfo)
      val controller = inject[EventsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
    }
  }

  "EventController#get(seasonId: Option[Int])" should {
    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[EventsController]
      val result = controller.get(None)(request)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 200 if season does not have any events" in new WithApplication(application) {
      eventService.getAllBySeason(any[Int], any[Int], any[Boolean]) returns Future
        .successful(Seq())
      inventoryStatsTransaction.getInventoryForEvents(any[Int], any[Seq[Int]], any[Option[Int]]) returns Future
        .successful(Seq(inventoryStats.get))
      scheduledJobQueue.getLatestForEvent(any[Int], any[JobType], any[Option[ScheduledJobStatus]]) returns Future
        .successful(Some(scheduledJobTriggerComposite))

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[EventsController]
      val result = controller.get(Some(1))(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText mustBe "[]"
    }

    "return status 200 with all events for admin user" in new WithApplication(application) {
      eventService.getAll(any[Option[Int]], any[Boolean]) returns Future
        .successful(Seq(event.get))
      scheduledJobQueue.getLatestForEvent(any[Int], any[JobType], any[Option[ScheduledJobStatus]]) returns Future
        .successful(Some(scheduledJobTriggerComposite))

      modelServerService.getAutomatedSpringValue(any[String], any[Double], any[Option[Timestamp]]) returns Future
        .successful(ModelServerSpringResponse(0.002))

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[EventsController]
      val result = controller.get(None)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
    }

    "return status 200 with all events for non admin user with client id" in new WithApplication(
      application
    ) {
      eventService.getAll(any[Option[Int]], any[Boolean]) returns Future
        .successful(Seq(event.get))
      inventoryStatsTransaction.getInventoryForEvents(any[Int], any[Seq[Int]], any[Option[Int]]) returns Future
        .successful(Seq(inventoryStats.get))
      scheduledJobQueue.getLatestForEvent(any[Int], any[JobType], any[Option[ScheduledJobStatus]]) returns Future
        .successful(Some(scheduledJobTriggerComposite))
      modelServerService.getAutomatedSpringValue(any[String], any[Double], any[Option[Timestamp]]) returns Future
        .successful(ModelServerSpringResponse(0.002))

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[EventsController]
      val result = controller.get(None)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
    }

    "return status 200 with all events for season" in new WithApplication(application) {
      val eventWithSeason =
        Event(
          id = Some(2),
          primaryEventId = None,
          createdAt = now,
          modifiedAt = now,
          name = "Event2",
          timestamp = Some(now),
          clientId = 1,
          venueId = 1,
          eventCategoryId = 1,
          seasonId = Some(1),
          isBroadcast = true,
          percentPriceModifier = 0,
          eventScore = None,
          eventScoreModifier = 0,
          spring = None,
          springModifier = 0
        )
      eventService.getAllBySeason(any[Int], any[Int], any[Boolean]) returns Future
        .successful(Seq(eventWithSeason))
      scheduledJobQueue.getLatestForEvent(any[Int], any[JobType], any[Option[ScheduledJobStatus]]) returns Future
        .successful(Some(scheduledJobTriggerComposite))

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[EventsController]
      val result = controller.get(Some(1))(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":2")
    }

    "return status 200 with stored final spring for past events" in new WithApplication(application) {
      val finalSpring = Some(2.1)
      val eventWithSeason =
        Event(
          id = Some(1),
          primaryEventId = None,
          createdAt = now,
          modifiedAt = now,
          name = "EventInThePast",
          timestamp = Some(past),
          clientId = 1,
          venueId = 1,
          eventCategoryId = 1,
          seasonId = Some(1),
          isBroadcast = true,
          percentPriceModifier = 0,
          eventScore = None,
          eventScoreModifier = 0,
          spring = finalSpring,
          springModifier = 0
        )
      eventService.getAllBySeason(any[Int], any[Int], any[Boolean]) returns Future
        .successful(Seq(eventWithSeason))
      scheduledJobQueue.getLatestForEvent(any[Int], any[JobType], any[Option[ScheduledJobStatus]]) returns Future
        .successful(Some(scheduledJobTriggerComposite))

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[EventsController]
      val result = controller.get(Some(1))(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
      bodyText must include(s"""\"spring\":${finalSpring.get}""")
    }
  }

  "EventController#updatePercentPriceModifier" should {
    "return status 200 if an event percent price modifier is updated" in new WithApplication(
      application
    ) {
      val percentPriceModifier = 10
      eventService.getById(any[Int]) returns Future.successful(event)

      eventService.update(any[Event]) returns Future.successful(
        SuccessResponse(event.get.copy(percentPriceModifier = percentPriceModifier): Event)
      )

      val request = FakeRequest()
        .withBody(Json.parse(s"""{"percentPriceModifier":$percentPriceModifier}"""))
        .withAuthenticator(loginInfo)

      val controller = inject[EventsController]
      val result = controller.updatePercentPriceModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe OK
      bodyText must include(s"""\"percentPriceModifier\":$percentPriceModifier""")
    }

    "return 403 if event is not found" in new WithApplication(application) {
      val invalidId = -1
      eventService.getById(invalidId) returns Future.successful(None)

      val request = FakeRequest()
        .withBody(Json.parse("""{"percentPriceModifier": 10}"""))
        .withAuthenticator(loginInfo)

      val controller = inject[EventsController]
      val result = controller.updatePercentPriceModifier(invalidId)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe FORBIDDEN
      bodyText mustBe "{\"message\":\"Access Denied\"}"
    }

    "return 401 if user is not logged in" in new WithApplication(application) {
      val request = FakeRequest()
        .withBody(Json.parse("""{"percentPriceModifier": 10}"""))

      val controller = inject[EventsController]
      val result = controller.updatePercentPriceModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return 400 if percent price modifier < -50" in new WithApplication(application) {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{"percentPriceModifier": -51}"""))

      val controller = inject[EventsController]
      val result = controller.updatePercentPriceModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid request body\"}"
    }

    "return 400 if percent price modifier > 50" in new WithApplication(application) {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{"percentPriceModifier": 51}"""))

      val controller = inject[EventsController]
      val result = controller.updatePercentPriceModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid request body\"}"
    }
  }

  "EventController#updateAdminModifier" should {
    "return status 200 if admin modifiers are updated" in new WithApplication(application) {
      val eventScoreModifier = 10.5
      val springModifier = 1.5
      eventService.getById(any[Int]) returns Future.successful(
        Some(event.get.copy(eventScore = Some(10)))
      )

      clientService.getById(any[Int]) returns Future.successful(client)

      eventService.update(any[Event]) returns Future.successful(
        SuccessResponse(
          event.get
            .copy(
              eventScore = Some(1.0),
              eventScoreModifier = eventScoreModifier,
              springModifier = springModifier
            ): Event
        )
      )
      modelServerService.getAutomatedSpringValue(any[String], any[Double], any[Option[Timestamp]]) returns Future
        .successful(ModelServerSpringResponse(0.002))

      jobConfigService.getRawConfig(event.get.clientId, JobType.Pricing) returns Future.successful(
        Some(pricingJobConfig)
      )
      scheduledJobQueue.add(
        event.get.id.get,
        pricingJobConfig.id.get,
        JobTriggerType.Manual,
        JobPriorityType.ManualPricingModifierChange
      ) returns Future
        .successful(scheduledJob)

      val request = FakeRequest()
        .withBody(
          Json.parse(
            s"""{"eventScoreModifier":$eventScoreModifier, "springModifier":$springModifier}"""
          )
        )
        .withAuthenticator(adminLoginInfo)

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe OK
      bodyText must include(s"""\"eventScoreModifier\":$eventScoreModifier""")
      bodyText must include(s"""\"springModifier\":$springModifier""")

      verify(jobConfigService, times(1)).getRawConfig(event.get.clientId, JobType.Pricing)
      Thread.sleep(100) // Wait for a race condition
      verify(scheduledJobQueue, times(1)).add(
        event.get.id.get,
        pricingJobConfig.id.get,
        JobTriggerType.Manual,
        JobPriorityType.ManualPricingModifierChange
      )
    }

    "return 404 if event is not found" in new WithApplication(application) {
      val invalidId = -1
      eventService.getById(invalidId) returns Future.successful(None)

      val request = FakeRequest()
        .withBody(Json.parse("""{"eventScoreModifier": 10, "springModifier": 1}"""))
        .withAuthenticator(adminLoginInfo)

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(invalidId)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Event not found\"}"
    }

    "return 401 if user is not logged in" in new WithApplication(application) {
      val request = FakeRequest()
        .withBody(Json.parse("""{"eventScoreModifier": 10, "springModifier": 1}"""))

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return 403 if user is not admin" in new WithApplication(application) {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{"eventScoreModifier": 10, "springModifier": 1}"""))

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe FORBIDDEN
      bodyText mustBe "{\"message\":\"Access Denied\"}"
    }

    "return 400 if event score modifier < -100" in new WithApplication(application) {
      val request = FakeRequest()
        .withAuthenticator(adminLoginInfo)
        .withBody(Json.parse("""{"eventScoreModifier": -101, "springModifier": 1}"""))

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid request body\"}"
    }

    "return 400 if event score modifier > 100" in new WithApplication(application) {
      val request = FakeRequest()
        .withAuthenticator(adminLoginInfo)
        .withBody(Json.parse("""{"eventScoreModifier": 101, "springModifier": 1}"""))

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid request body\"}"
    }

    "return 400 if spring modifier < -2" in new WithApplication(application) {
      val request = FakeRequest()
        .withAuthenticator(adminLoginInfo)
        .withBody(Json.parse("""{"eventScoreModifier": 10, "springModifier": -2.5}"""))

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid request body\"}"
    }

    "return 400 if spring modifier > 2" in new WithApplication(application) {
      val request = FakeRequest()
        .withAuthenticator(adminLoginInfo)
        .withBody(Json.parse("""{"eventScoreModifier": 10, "springModifier": 2.5}"""))

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid request body\"}"
    }

    "return 400 if event score modifier change makes final event score < event score floor" in new WithApplication(
      application
    ) {
      modelServerService.getAutomatedSpringValue(any[String], any[Double], any[Option[Timestamp]]) returns Future
        .successful(ModelServerSpringResponse(0.02))

      eventService.getById(any[Int]) returns Future.successful(
        Some(event.get.copy(eventScore = Some(10)))
      )

      clientService.getById(any[Int]) returns Future.successful(client)

      val request = FakeRequest()
        .withAuthenticator(adminLoginInfo)
        .withBody(Json.parse("""{"eventScoreModifier": -10, "springModifier": 2}"""))

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Final Event Score cannot be below 2.00 \"}"
    }

    "return 400 if spring modifier change makes final spring < spring floor" in new WithApplication(
      application
    ) {
      modelServerService.getAutomatedSpringValue(any[String], any[Double], any[Option[Timestamp]]) returns Future
        .successful(ModelServerSpringResponse(0.02))

      eventService.getById(any[Int]) returns Future.successful(
        Some(event.get.copy(eventScore = Some(10)))
      )

      clientService.getById(any[Int]) returns Future.successful(client)

      val request = FakeRequest()
        .withAuthenticator(adminLoginInfo)
        .withBody(Json.parse("""{"eventScoreModifier": 10, "springModifier": -2}"""))

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Final Spring cannot be below 1.1250 \"}"
    }

    "return 400 if event score modifier change makes final event score > event score ceiling" in new WithApplication(
      application
    ) {
      modelServerService.getAutomatedSpringValue(any[String], any[Double], any[Option[Timestamp]]) returns Future
        .successful(ModelServerSpringResponse(0.02))

      eventService.getById(any[Int]) returns Future.successful(
        Some(event.get.copy(eventScore = Some(10)))
      )

      clientService.getById(any[Int]) returns Future.successful(
        Some(client.get.copy(eventScoreCeiling = Some(100)))
      )

      val request = FakeRequest()
        .withAuthenticator(adminLoginInfo)
        .withBody(Json.parse("""{"eventScoreModifier": 100, "springModifier": 2}"""))

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Final Event Score cannot exceed 100.00 \"}"
    }

    "return 400 if spring modifier change makes final spring score > spring ceiling" in new WithApplication(
      application
    ) {
      modelServerService.getAutomatedSpringValue(any[String], any[Double], any[Option[Timestamp]]) returns Future
        .successful(ModelServerSpringResponse(0.02))

      eventService.getById(any[Int]) returns Future.successful(
        Some(event.get.copy(eventScore = Some(10)))
      )

      clientService.getById(any[Int]) returns Future.successful(
        Some(client.get.copy(springCeiling = Some(3)))
      )

      val request = FakeRequest()
        .withAuthenticator(adminLoginInfo)
        .withBody(Json.parse("""{"eventScoreModifier": 100, "springModifier": 2}"""))

      val controller = inject[EventsController]
      val result = controller.updateAdminModifier(1)(request)
      val bodyText = contentAsString(result)
      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Final Spring cannot exceed 3.0000 \"}"
    }
  }

  "The EventController#toggleIsBroadcast" should {
    "return status 200 if isBroadcast successfully set on event" in new WithApplication() {
      eventService.toggleIsBroadcast(any[Int], any[Boolean]) returns Future
        .successful(Some(false, DateHelper.now()))

      val request = FakeRequest()
        .withBody(Json.parse("""{"isBroadcast": false}"""))
        .withAuthenticator(loginInfo)
      val controller = inject[EventsController]
      val result = controller.toggleIsBroadcast(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
    }

    "return status 400 if request body is invalid" in new WithApplication(application) {

      val request = FakeRequest()
      val controller = inject[EventsController]
      val result = controller.toggleIsBroadcast(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
    }

    "return status 404 if event doesn't exist" in new WithApplication(application) {
      eventService.toggleIsBroadcast(any[Int], any[Boolean]) returns Future
        .successful(None)

      val request = FakeRequest()
        .withBody(Json.parse("""{"isBroadcast": true}"""))
        .withAuthenticator(loginInfo)
      val controller = inject[EventsController]
      val result = controller.toggleIsBroadcast(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Event not found\"}"
    }
  }

  "EventsController#getPricesForEvent" should {
    val modelServerResponseItems = Seq(
      ModelServerPriceAllResponseItem("123", "1", 100),
      ModelServerPriceAllResponseItem("123", "2", 90)
    )
    val modelServerResponse = ModelServerPriceAllResponse(modelServerResponseItems)

    "use scores on the event by default" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val event = createEvent(
        eventScore = Some(5),
        eventScoreModifier = -.2,
        spring = Some(2),
        springModifier = .08
      )
      eventService.getById(any[Int]).returns(Future.successful(Some(event)))
      modelServerService
        .getAllPrices(any[String], any[Double], any[Double])
        .returns(Future.successful(modelServerResponse))

      val controller = inject[EventsController]
      val result = controller.getPricesForEvent(1, None, None)(request)
      val body = contentAsJson(result).validate[EventPricesModel].get

      assert(body.prices.length == 2)
      assert(body.prices(0) == EventPrice("123", "1", 100))
      assert(body.prices(1) == EventPrice("123", "2", 90))
      verify(modelServerService).getAllPrices("1", 4.8, 2.08)
    }

    "use event score override" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val event = createEvent(
        eventScore = Some(5),
        eventScoreModifier = -.2,
        spring = Some(2),
        springModifier = .08
      )
      eventService.getById(any[Int]).returns(Future.successful(Some(event)))
      modelServerService
        .getAllPrices(any[String], any[Double], any[Double])
        .returns(Future.successful(modelServerResponse))

      val controller = inject[EventsController]
      val result = controller.getPricesForEvent(1, Some(10), None)(request)
      val body = contentAsJson(result).validate[EventPricesModel].get

      assert(body.prices.length == 2)
      assert(body.prices(0) == EventPrice("123", "1", 100))
      assert(body.prices(1) == EventPrice("123", "2", 90))
      verify(modelServerService).getAllPrices("1", 10, 2.08)
    }

    "use spring override" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val event = createEvent(
        eventScore = Some(5),
        eventScoreModifier = -.2,
        spring = Some(2),
        springModifier = .08
      )
      eventService.getById(any[Int]).returns(Future.successful(Some(event)))
      modelServerService
        .getAllPrices(any[String], any[Double], any[Double])
        .returns(Future.successful(modelServerResponse))

      val controller = inject[EventsController]
      val result = controller.getPricesForEvent(1, None, Some(3))(request)
      val body = contentAsJson(result).validate[EventPricesModel].get

      assert(body.prices.length == 2)
      assert(body.prices(0) == EventPrice("123", "1", 100))
      assert(body.prices(1) == EventPrice("123", "2", 90))
      verify(modelServerService).getAllPrices("1", 4.8, 3)
    }

    "throw bad request when no event scores" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val event = createEvent(
        eventScore = None,
        eventScoreModifier = -.2,
        spring = Some(2),
        springModifier = .08
      )
      eventService.getById(any[Int]).returns(Future.successful(Some(event)))
      modelServerService
        .getAllPrices(any[String], any[Double], any[Double])
        .returns(Future.successful(modelServerResponse))

      val controller = inject[EventsController]
      val result = controller.getPricesForEvent(1, None, None)(request)

      status(result) mustBe BAD_REQUEST
    }

    "throw bad request when no springs" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val event = createEvent(
        eventScore = None,
        eventScoreModifier = -.2,
        spring = None,
        springModifier = .08
      )
      eventService.getById(any[Int]).returns(Future.successful(Some(event)))
      modelServerService
        .getAllPrices(any[String], any[Double], any[Double])
        .returns(Future.successful(modelServerResponse))

      val controller = inject[EventsController]
      val result = controller.getPricesForEvent(1, Some(7), None)(request)

      status(result) mustBe BAD_REQUEST
    }

    "return internal server error when query fails" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val event = createEvent(
        eventScore = Some(5),
        eventScoreModifier = -.2,
        spring = Some(2),
        springModifier = .08
      )
      eventService.getById(any[Int]).returns(Future.successful(Some(event)))
      modelServerService
        .getAllPrices(any[String], any[Double], any[Double])
        .returns(Future.failed(new Exception("Test")))

      val controller = inject[EventsController]
      val result = controller.getPricesForEvent(1, None, None)(request)
      status(result) mustBe INTERNAL_SERVER_ERROR
    }
  }

  "EventsController#getAutomatedSpringValue" should {
    "return a 401 if a user is not logged in" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[EventsController]
      val result = controller.getAutomatedSpringValue(1, 4.0)(request)

      status(result) mustBe UNAUTHORIZED
    }

    "return a 403 if event does not exist" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(adminLoginInfo)

      eventService.getById(any[Int]).returns(Future.successful(None))

      val controller = inject[EventsController]
      val result = controller.getAutomatedSpringValue(1, 4.0)(request)

      status(result) mustBe FORBIDDEN
    }
    "return a 200 on successful request" in new WithApplication(application) {
      val modelServerSpringResponse = ModelServerSpringResponse(springValue = 1.012)
      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val event = createEvent(
        eventScore = None,
        eventScoreModifier = -.2,
        timestamp = Some(Timestamp.from(Instant.now))
      )
      eventService.getById(any[Int]).returns(Future.successful(Some(event)))
      modelServerService
        .getAutomatedSpringValue(any[String], any[Double], any[Option[Timestamp]])
        .returns(Future.successful(modelServerSpringResponse))

      val controller = inject[EventsController]
      val result = controller.getAutomatedSpringValue(1, 4.0)(request)
      status(result) mustBe OK
    }
  }
}

object EventsControllerSpec {

  def createEvent(
    eventScore: Option[Double] = None,
    eventScoreModifier: Double = 0,
    spring: Option[Double] = None,
    springModifier: Double = 0,
    timestamp: Option[Timestamp] = None
  ): Event =
    Event(
      id = Some(1),
      primaryEventId = None,
      createdAt = DateHelper.now(),
      modifiedAt = DateHelper.now(),
      name = "name",
      timestamp = timestamp,
      clientId = 1,
      venueId = 2,
      eventCategoryId = 2,
      seasonId = None,
      isBroadcast = true,
      percentPriceModifier = 0,
      eventScore = eventScore,
      eventScoreModifier = eventScoreModifier,
      spring = spring,
      springModifier = springModifier
    )
}
