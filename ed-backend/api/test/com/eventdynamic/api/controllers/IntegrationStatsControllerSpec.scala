package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.test.{FakeRequest, Injecting, WithApplication}
import play.api.test.Helpers._
import com.mohiva.play.silhouette.test._

import scala.concurrent.Future

class IntegrationStatsControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito {
  "IntegrationStatsController#getIntegrationsForSeason" should {
    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[IntegrationStatsController]
      val result = controller.getIntegrationsStats(Some(1), None)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 404 if season does not exist" in new WithApplication(application) {
      seasonService.getById(any[Int], any[Option[Int]], any[Boolean]) returns Future.successful(
        None
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[IntegrationStatsController]
      val result = controller.getIntegrationsStats(Some(1), None)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
    }

    "return status 400 if no season or event is specified" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[IntegrationStatsController]
      val result = controller.getIntegrationsStats(None, None)(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 404 if event does not exist" in new WithApplication(application) {
      eventService.getById(any[Int]) returns Future.successful(None)

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[IntegrationStatsController]
      val result = controller.getIntegrationsStats(None, Some(1))(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
    }

    "return status 404 if event and season ids do not match" in new WithApplication(application) {
      eventService.getById(any[Int]) returns Future.successful(event)

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[IntegrationStatsController]
      val result = controller.getIntegrationsStats(Some(2), Some(1))(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
    }

    "return status 200 if integrations are returned for season" in new WithApplication(application) {
      seasonService.getById(any[Int], any[Option[Int]], any[Boolean]) returns Future.successful(
        season
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[IntegrationStatsController]
      val result = controller.getIntegrationsStats(Some(1), None)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
      bodyText must include("\"name\":\"Tickets.com\"")
      bodyText must include("\"logoUrl\":\"someurl\"")
      bodyText must include("\"sold\":10")
      bodyText must include("\"total\":100")
      bodyText must include("\"isActive\":true")
    }

    "return status 200 if integrations are returned for an event" in new WithApplication(
      application
    ) {
      eventService.getById(any[Int]) returns Future.successful(
        Some(event.get.copy(seasonId = Some(1)))
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[IntegrationStatsController]
      val result = controller.getIntegrationsStats(Some(1), Some(1))(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
      bodyText must include("\"name\":\"Tickets.com\"")
      bodyText must include("\"logoUrl\":\"someurl\"")
      bodyText must include("\"sold\":10")
      bodyText must include("\"total\":100")
      bodyText must include("\"isActive\":true")
    }
  }
}
