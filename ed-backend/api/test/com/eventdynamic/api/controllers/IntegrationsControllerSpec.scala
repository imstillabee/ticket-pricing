package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.test.{FakeRequest, Injecting, WithApplication}
import com.mohiva.play.silhouette.test._
import play.api.test.Helpers._

import scala.concurrent.Future

class IntegrationsControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito {
  "IntegrationsController#getById" should {
    "return status 200 if integration is returned" in new WithApplication(application) {
      integrationService.getById(any[Int]) returns Future.successful(Some(integration))

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[IntegrationsController]
      val result = controller.getById(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }

    "return status 401 if user is not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[IntegrationsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 404 if integration could not be found" in new WithApplication(application) {
      integrationService.getById(any[Int]) returns Future.successful(None)

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[IntegrationsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Integration not found\"}"
    }
  }
}
