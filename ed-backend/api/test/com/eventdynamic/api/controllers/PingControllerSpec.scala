package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test.Helpers._
import play.api.test._

/**
  * Add your spec here.
  * You can mock out a whole application including requests, plugins etc.
  *
  * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
  */
class PingControllerSpec extends TestContext with GuiceOneAppPerTest with Injecting {

  "PingController GET" should {
    "call the ping endpoint from a new instance of controller" in new WithApplication(application) {
      val controller = inject[PingController]
      val ping = controller.ping().apply(FakeRequest(GET, "/"))

      status(ping) mustBe OK
      contentType(ping) mustBe Some("text/plain")
      contentAsString(ping) must include("pong")
    }

  }
}
