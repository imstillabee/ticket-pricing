package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.test.{FakeRequest, Injecting, WithApplication}
import com.mohiva.play.silhouette.test._
import play.api.test.Helpers._

import scala.concurrent.Future

class PriceScalesControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito {
  "PriceScalesController#list" should {
    "return 200 for success" in new WithApplication(application) {
      priceScaleService.getAllForClient(1) returns Future.successful(Seq(priceScale.get))

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[PriceScalesController]
      val result = controller.list()(request)

      status(result) mustBe OK
    }

    "return 401 if the user is not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[PriceScalesController]
      val result = controller.list()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }
  }
}
