package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import com.eventdynamic.models.{ProVenuePricingRule, RoundingType}
import com.eventdynamic.models.RoundingType.RoundingType
import com.eventdynamic.services.ProVenuePricingRuleService
import com.eventdynamic.services.ProVenuePricingRuleService.CompoundProVenuePricingRule
import com.eventdynamic.utils.{DateHelper, SuccessResponse}
import com.mohiva.play.silhouette.test._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Injecting}

import scala.concurrent.Future

class ProVenuePricingRulesControllerSpec extends TestContext with Injecting {

  "ProVenuePricingRulesController#list" should {
    "return status 200 when successful" in {
      proVenuePricingRuleService.getAll(any[Int], any[Boolean]) returns Future.successful(
        Seq[CompoundProVenuePricingRule]()
      )
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.list()(request)

      status(result) mustBe OK
    }

    "return status 401 if the user is not authenticated" in {
      val request = FakeRequest()
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.list()(request)

      status(result) mustBe UNAUTHORIZED
    }
  }

  "ProVenuePricingRulesController#create" should {
    proVenuePricingRuleService.create(
      any[Int],
      any[Seq[Int]],
      any[Seq[Int]],
      any[Seq[String]],
      any[Boolean],
      any[Option[Int]],
      any[Option[Int]],
      any[Option[BigDecimal]],
      any[Option[String]],
      any[RoundingType]
    ) returns Future.successful(SuccessResponse(123))

    proVenuePricingRuleService.getConflicting(
      any[Option[Int]],
      any[Int],
      any[Seq[Int]],
      any[Seq[Int]],
      any[Seq[String]]
    ) returns Future.successful(Seq())

    val validBody =
      """
        |{
        | "name": "Pricing Rule",
        | "isActive": true,
        | "mirrorPriceScaleId": 1,
        | "percent": -10,
        | "constant": 10.01,
        | "round": "Ceil",
        | "eventIds": [1,2,3],
        | "priceScaleIds": [1,2,3],
        | "externalBuyerTypeIds": ["1","2","3"]
        |}
      """.stripMargin

    "return status 201 if the record is created successfully (with all values)" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse(validBody))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.create()(request)

      status(result) mustBe CREATED
      contentAsJson(result) mustBe Json.toJson(123)
    }

    "return status 201 if the record is created successfully (with defaults)" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
            |{
            | "constant": 10.01,
            | "eventIds": [1,2,3],
            | "priceScaleIds": [1,2,3],
            | "externalBuyerTypeIds": ["1","2","3"]
            |}
          """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.create()(request)

      status(result) mustBe CREATED
      contentAsJson(result) mustBe Json.toJson(123)
    }

    "return status 201 not check for conflicts if the record is not active" in {
      val conflictingId = 777
      proVenuePricingRuleService.getConflicting(
        any[Option[Int]],
        any[Int],
        any[Seq[Int]],
        any[Seq[Int]],
        any[Seq[String]]
      ) returns Future.successful(Seq(conflictingId))

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
                               |{
                               | "isActive": false,
                               | "constant": 10.01,
                               | "eventIds": [1,2,3],
                               | "priceScaleIds": [1,2,3],
                               | "externalBuyerTypeIds": ["1","2","3"]
                               |}
                             """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.create()(request)

      status(result) mustBe CREATED
      contentAsJson(result) mustBe Json.toJson(123)
    }

    "return status 400 if the body is malformed" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
              |{
              | "invalid": "body"
              |}
            """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.create()(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 400 if eventIds is empty" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
                               |{
                               | "constant": 10.01,
                               | "eventIds": [],
                               | "priceScaleIds": [1,2,3],
                               | "externalBuyerTypeIds": ["1","2","3"]
                               |}
                             """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.create()(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 400 if priceScaleIds is empty" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
                               |{
                               | "constant": 10.01,
                               | "eventIds": [1,2,3],
                               | "priceScaleIds": [],
                               | "externalBuyerTypeIds": ["1","2","3"]
                               |}
                             """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.create()(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 400 if an externalBuyerTypeIds is empty" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
                               |{
                               | "constant": 10.01,
                               | "eventIds": [1,2,3],
                               | "priceScaleIds": [1,2,3],
                               | "externalBuyerTypeIds": []
                               |}
                             """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.create()(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 400 if percent is less than -100" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
                               |{
                               | "percent": -101,
                               | "eventIds": [1,2,3],
                               | "priceScaleIds": [1,2,3],
                               | "externalBuyerTypeIds": []
                               |}
                             """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.create()(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 400 if percent is more than 100" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
                               |{
                               | "percent": 101,
                               | "eventIds": [1,2,3],
                               | "priceScaleIds": [1,2,3],
                               | "externalBuyerTypeIds": []
                               |}
                             """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.create()(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 401 if the user is not authenticated" in {
      val request = FakeRequest()
        .withBody(Json.parse(validBody))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.create()(request)
      status(result) mustBe UNAUTHORIZED
    }

    "return status 409 if there is an existing rule that conflicts with the one being created" in {
      val conflictingRuleId = 777
      proVenuePricingRuleService.getConflicting(
        any[Option[Int]],
        any[Int],
        any[Seq[Int]],
        any[Seq[Int]],
        any[Seq[String]]
      ) returns Future.successful(Seq(conflictingRuleId))

      val conflictingBuyerTypeId = "456"

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse(validBody))

      val controller = inject[ProVenuePricingRulesController]
      val result = controller.create()(request)

      status(result) mustBe CONFLICT
      contentAsJson(result) mustBe Json.parse(s"""
          |{
          | "proVenuePricingRules":[$conflictingRuleId]
          | }
        """.stripMargin)
    }
  }

  "ProVenuePricingRulesController#update" should {
    proVenuePricingRuleService.update(
      any[Int],
      any[Int],
      any[Seq[Int]],
      any[Seq[Int]],
      any[Seq[String]],
      any[Boolean],
      any[Option[Int]],
      any[Option[Int]],
      any[Option[BigDecimal]],
      any[Option[String]],
      any[RoundingType]
    ) returns Future.successful(SuccessResponse(123))

    val validBody =
      """
        |{
        | "name": "Pricing Rule",
        | "isActive": true,
        | "mirrorPriceScaleId": 1,
        | "percent": -10,
        | "constant": 10.01,
        | "round": "Ceil",
        | "eventIds": [1,2,3],
        | "priceScaleIds": [1,2,3],
        | "externalBuyerTypeIds": ["1","2","3"]
        |}
      """.stripMargin

    "return status 200 when the record is updated successfully" in {
      proVenuePricingRuleService.getConflicting(
        any[Option[Int]],
        any[Int],
        any[Seq[Int]],
        any[Seq[Int]],
        any[Seq[String]]
      ) returns Future.successful(Seq())

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse(validBody))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.update(123)(request)

      status(result) mustBe OK
      contentAsJson(result) mustBe Json.toJson(123)
    }

    "return status 200 and not check for conflicts if the record is not active" in {
      val conflictingId = 777
      proVenuePricingRuleService.getConflicting(
        any[Option[Int]],
        any[Int],
        any[Seq[Int]],
        any[Seq[Int]],
        any[Seq[String]]
      ) returns Future.successful(Seq(conflictingId))

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
                               |{
                               | "isActive": false,
                               | "constant": 10.01,
                               | "eventIds": [1,2,3],
                               | "priceScaleIds": [1,2,3],
                               | "externalBuyerTypeIds": ["1","2","3"]
                               |}
                             """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.update(123)(request)

      status(result) mustBe OK
      contentAsJson(result) mustBe Json.toJson(123)
    }

    "return status 400 if the body is malformed" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
            |{
            | "invalid": "body"
            |}
          """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.update(123)(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 400 if eventIds is empty" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
            |{
            | "constant": 10.01,
            | "eventIds": [],
            | "priceScaleIds": [1,2,3],
            | "externalBuyerTypeIds": ["1","2","3"]
            |}
          """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.update(123)(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 400 if priceScaleIds is empty" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
            |{
            | "constant": 10.01,
            | "eventIds": [1,2,3],
            | "priceScaleIds": [],
            | "externalBuyerTypeIds": ["1","2","3"]
            |}
          """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.update(123)(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 400 if an externalBuyerTypeIds is empty" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
            |{
            | "constant": 10.01,
            | "eventIds": [1,2,3],
            | "priceScaleIds": [1,2,3],
            | "externalBuyerTypeIds": []
            |}
          """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.update(123)(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 400 if the percent is less than 100" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
                               |{
                               | "percent": -101,
                               | "eventIds": [1,2,3],
                               | "priceScaleIds": [1,2,3],
                               | "externalBuyerTypeIds": []
                               |}
                             """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.update(123)(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 400 if the percent is more than 100" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""
                               |{
                               | "percent": 101,
                               | "eventIds": [1,2,3],
                               | "priceScaleIds": [1,2,3],
                               | "externalBuyerTypeIds": []
                               |}
                             """.stripMargin))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.update(123)(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 401 if the user is not authenticated" in {
      val request = FakeRequest()
        .withBody(Json.parse(validBody))
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.update(123)(request)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 409 if there is an existing rule that conflicts with the one being created" in {
      val conflictingId = 777
      proVenuePricingRuleService.getConflicting(
        any[Option[Int]],
        any[Int],
        any[Seq[Int]],
        any[Seq[Int]],
        any[Seq[String]]
      ) returns Future.successful(Seq(conflictingId))

      val conflictingBuyerTypeId = "456"
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse(validBody))

      val controller = inject[ProVenuePricingRulesController]
      val result = controller.update(123)(request)

      status(result) mustBe CONFLICT
      contentAsJson(result) mustBe Json.parse(s"""
           |{
           | "proVenuePricingRules":[$conflictingId]
           | }
        """.stripMargin)
    }
  }

  "ProVenuePricingRuleController#getOne" should {
    val rule = ProVenuePricingRuleService.CompoundProVenuePricingRule(
      ProVenuePricingRule(
        Some(1),
        DateHelper.now(),
        DateHelper.now(),
        1,
        None,
        true,
        None,
        None,
        None,
        RoundingType.Ceil
      ),
      Seq("1"),
      Seq(1),
      Seq(1)
    )
    proVenuePricingRuleService.getOne(any[Int], any[Int]) returns Future.successful(Some(rule))

    "return status 200 with a single pricing rule" in {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.getOne(123)(request)

      status(result) mustBe OK
    }

    "return status 404 if the rule cannot be found" in {
      proVenuePricingRuleService.getOne(any[Int], any[Int]) returns Future.successful(None)

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.getOne(123)(request)

      status(result) mustBe NOT_FOUND
    }
  }

  "ProVenuePricingRuleController#delete" should {
    "return status 200 if rule successfully deleted" in {
      proVenuePricingRuleService.delete(any[Int], any[Int]) returns Future.successful(true)
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.delete(1)(request)

      status(result) mustBe OK
    }

    "return status 401 if no user logged in" in {
      val request = FakeRequest()
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.delete(1)(request)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 404 if user has a clientID that doesn't match the rule clientID" in {
      proVenuePricingRuleService.delete(any[Int], any[Int]) returns Future.successful(false)
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
      val controller = inject[ProVenuePricingRulesController]
      val result = controller.delete(1)(request)

      status(result) mustBe NOT_FOUND
    }
  }
}
