package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Injecting, WithApplication}
import com.mohiva.play.silhouette.test._
import scala.concurrent.Future

class RevenueStatsControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito {
  "get all" should {
    "return status 200 when successful" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[RevenueStatsController]
      val result = controller.get(Some(1), None)(request)

      status(result) mustBe OK
    }

    "return status 401 when not logged in" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[RevenueStatsController]
      val result = controller.get(Some(1), None)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 404 if the eventId could not be found" in new WithApplication(application) {
      eventService.getById(any[Int]) returns Future.successful(None)

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[RevenueStatsController]
      val result = controller.get(Some(1), None)(request)

      status(result) mustBe NOT_FOUND
    }

    "return status 404 if the seasonId could not be found" in new WithApplication(application) {
      seasonService.getById(any[Int], any[Option[Int]], any[Boolean]) returns Future.successful(
        None
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[RevenueStatsController]
      val result = controller.get(None, Some(1))(request)

      status(result) mustBe NOT_FOUND
    }
  }
}
