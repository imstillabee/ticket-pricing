package com.eventdynamic.api.controllers

import java.sql.Timestamp

import com.eventdynamic.api.controllers.helpers.TestContext
import com.eventdynamic.models.Season
import com.eventdynamic.utils._
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.test.{FakeRequest, Injecting, WithApplication}
import play.api.test.Helpers._
import com.mohiva.play.silhouette.test._
import play.api.libs.json.Json

import scala.concurrent.Future

class SeasonsControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito {

  "SeasonsController#getById(id: Int)" should {
    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[SeasonsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 404 if season does not exist" in new WithApplication(application) {
      seasonService.getById(any[Int], any[Option[Int]], any[Boolean]) returns Future.successful(
        None
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[SeasonsController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Season does not exist or could not be found\"}"
    }

    "return status 200 if season is returned" in new WithApplication(application) {
      val firstTime = event.get.timestamp.get
      val lastTime = DateHelper.later(518400)
      seasonService.getById(any[Int], any[Option[Int]], any[Boolean]) returns Future.successful(
        season.map(_.copy(startDate = Some(firstTime), endDate = Some(lastTime)))
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[SeasonsController]
      val result = controller.getById(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
      bodyText must include("\"startTimestamp\":\"" + firstTime.toInstant().toString())
      bodyText must include("\"endTimestamp\":\"" + lastTime.toInstant().toString())
    }
  }

  "SeasonsController#getAll" should {
    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[SeasonsController]
      val result = controller.getAll()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 200 if there are no seasons" in new WithApplication(application) {
      seasonService.getAll(Some(any[Int]), any[Boolean]) returns Future.successful(Seq())

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[SeasonsController]
      val result = controller.getAll()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText mustBe "[]"
    }

    "return status 200 if season is returned with events" in new WithApplication(application) {
      val firstTime = event.get.timestamp.get
      val lastTime = DateHelper.later(518400)
      seasonService.getAll(Some(any[Int]), any[Boolean]) returns Future.successful(
        Seq(season.get.copy(startDate = Some(firstTime), endDate = Some(lastTime)))
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[SeasonsController]
      val result = controller.getAll()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
      bodyText must include("\"startTimestamp\":\"" + firstTime.toInstant().toString())
      bodyText must include("\"endTimestamp\":\"" + lastTime.toInstant().toString())
    }
  }

  "SeasonsController#create" should {
    "return status 400 if request body is invalid" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[SeasonsController]
      val result = controller.create(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request =
        FakeRequest().withBody(Json.parse("""{  "name": "MLB Mets 2018"}"""))
      val controller = inject[SeasonsController]
      val result = controller.create(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 201 if Season successfully created" in new WithApplication(application) {
      seasonService.create(Some("MLB Mets 2020"), 1, None, None) returns Future
        .successful(SuccessResponse(season.get))

      val request = FakeRequest()
        .withBody(Json.parse("""{  "name": "MLB Mets 2020", "clientId": 1 }"""))
        .withAuthenticator(loginInfo)
      val controller = inject[SeasonsController]
      val result = controller.create(request)

      status(result) mustBe CREATED
    }
  }

  "SeasonsController#update" should {
    "return status 400 if request body is invalid" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[SeasonsController]
      val result = controller.update(1)(request)

      status(result) mustBe BAD_REQUEST
    }

    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request =
        FakeRequest().withBody(Json.parse("""{  "name": "MLB Mets 2018"}"""))
      val controller = inject[SeasonsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 404 if season does not exist" in new WithApplication(application) {
      seasonService.update(any[Season]) returns Future.successful(NotFoundResponse)

      val request = FakeRequest()
        .withBody(Json.parse("""{  "name": "MLB Mets 2018", "clientId": 1}"""))
        .withAuthenticator(adminLoginInfo)
      val controller = inject[SeasonsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Season does not exist\"}"
    }

    "return status 200 if season successfully updated" in new WithApplication(application) {
      seasonService.update(any[Season]) returns Future.successful(SuccessResponse(season.get))

      val request = FakeRequest()
        .withBody(Json.parse("""{  "name": "MLB Mets 2018", "clientId": 1}"""))
        .withAuthenticator(adminLoginInfo)
      val controller = inject[SeasonsController]
      val result = controller.update(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }
  }

  "SeasonsController#delete" should {
    "return status 401 if user is not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[SeasonsController]
      val result = controller.delete(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 403 if user is not admin" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[SeasonsController]
      val result = controller.delete(1)(request)

      status(result) mustBe FORBIDDEN
    }

    "return status 404 if season does not exist" in new WithApplication(application) {
      seasonService.delete(any[Int]) returns Future.successful(false)

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[SeasonsController]
      val result = controller.delete(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Season does not exist or could not be found\"}"
    }

    "return status 200 if event successfully deleted" in new WithApplication(application) {
      seasonService.delete(any[Int]) returns Future.successful(true)

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[SeasonsController]
      val result = controller.delete(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText mustBe "{\"message\":\"Season successfully deleted\"}"
    }
  }

  "SeasonsController#resetDates" should {
    "return status 401 if user is not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[SeasonsController]
      val result = controller.resetDates(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 403 if user is not admin" in new WithApplication(application) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[SeasonsController]
      val result = controller.resetDates(1)(request)

      status(result) mustBe FORBIDDEN
    }

    "return status 404 if season does not exist" in new WithApplication(application) {
      seasonService.resetDates(any[Season]) returns Future.successful(NotFoundResponse)
      seasonService.getById(any[Int], any[Option[Int]], any[Boolean]) returns Future.successful(
        None
      )

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[SeasonsController]
      val result = controller.resetDates(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Season does not exist or could not be found\"}"
    }

    "return status 200 if event successfully reset dates" in new WithApplication(application) {
      seasonService.resetDates(any[Season]) returns Future.successful(SuccessResponse(season.get))
      seasonService.getById(any[Int], any[Option[Int]], any[Boolean]) returns Future.successful(
        season
      )

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[SeasonsController]
      val result = controller.resetDates(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }
  }
}
