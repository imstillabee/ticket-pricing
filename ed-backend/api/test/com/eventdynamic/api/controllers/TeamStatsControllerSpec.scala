package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import com.eventdynamic.models.{ClientPerformanceType, MLSSeasonStat, NFLSeasonStat, TeamStat}
import com.mohiva.play.silhouette.test._
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Injecting, WithApplication}

import scala.concurrent.Future

class TeamStatsControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito {
  "TeamStatsController#get" should {
    "return 401 if unauthenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[TeamStatsController]
      val result = controller.get(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return 200 with MLB stats if successful" in new WithApplication(application) {
      clientService.getById(any[Int]) returns Future.successful(client)
      teamStatService.getByClientId(any[Int]) returns Future.successful(
        Seq(
          TeamStat(Some(1), now, now, 1, 1, 10, 20, 40),
          TeamStat(Some(1), now, now, 1, 2, 25, 15, 40)
        )
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TeamStatsController]
      val result = controller.get(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include(""""seasonId":1""")
      bodyText must include(""""seasonId":2""")
    }

    "return 404 if teamStat could not be found" in new WithApplication(application) {
      teamStatService.getByClientId(any[Int]) returns Future.successful(Seq())

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TeamStatsController]
      val result = controller.get(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Team Statistics not found\"}"
    }

    "return 200 with NFL stats if successful" in new WithApplication(application) {
      val nflClient = client.get.copy(performanceType = ClientPerformanceType.NFL).toOption

      clientService.getById(any[Int]) returns Future.successful(nflClient)
      nflSeasonStatService.getByClientId(any[Int]) returns Future.successful(
        Seq(
          NFLSeasonStat(Some(1), now, now, 1, 1, 10, 20, 40, 100),
          NFLSeasonStat(Some(1), now, now, 1, 2, 25, 15, 10, 80)
        )
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TeamStatsController]
      val result = controller.get(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include(""""ties":40""")
      bodyText must include(""""ties":10""")
    }

    "return 404 if NFL stats could not be found" in new WithApplication(application) {
      val nflClient = client.get.copy(performanceType = ClientPerformanceType.NFL).toOption

      clientService.getById(any[Int]) returns Future.successful(nflClient)
      nflSeasonStatService.getByClientId(any[Int]) returns Future.successful(Seq())

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TeamStatsController]
      val result = controller.get(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"NFL Season Statistics not found\"}"
    }

    "return 200 with MLS stats if successful" in new WithApplication(application) {
      val mlsClient = client.get.copy(performanceType = ClientPerformanceType.MLS).toOption

      clientService.getById(any[Int]) returns Future.successful(mlsClient)
      mlsSeasonStatService.getByClientId(any[Int]) returns Future.successful(
        Seq(
          MLSSeasonStat(Some(1), now, now, 1, 10, 20, 20, 100),
          MLSSeasonStat(Some(1), now, now, 2, 25, 15, 13, 80)
        )
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TeamStatsController]
      val result = controller.get(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include(""""ties":20""")
      bodyText must include(""""ties":13""")
    }

    "return 404 if MLS stats could not be found" in new WithApplication(application) {
      val mlsClient = client.get.copy(performanceType = ClientPerformanceType.MLS).toOption

      clientService.getById(any[Int]) returns Future.successful(mlsClient)
      mlsSeasonStatService.getByClientId(any[Int]) returns Future.successful(Seq())

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TeamStatsController]
      val result = controller.get(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"MLS Season Statistics not found\"}"
    }
  }
}
