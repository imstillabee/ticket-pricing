package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import com.mohiva.play.silhouette.test._
import org.specs2.mock.Mockito
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Injecting}

import scala.concurrent.Future

class TimeStatsControllerSpec extends TestContext with Injecting with Mockito {
  "TimeStatsController#get" should {
    "return 200 when passed filters" in {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TimeStatsController]
      val result =
        controller.get(
          eventId = Some(1),
          seasonId = Some(1),
          startDate = "2018-05-01",
          endDate = "2018-05-02"
        )(request)

      status(result) mustBe OK
    }

    "return 400 when passed an invalid start time (start after end)" in {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TimeStatsController]
      val result =
        controller.get(
          eventId = Some(1),
          seasonId = Some(1),
          startDate = "2018-05-02",
          endDate = "2018-05-01"
        )(request)

      status(result) mustBe BAD_REQUEST
    }

    "return 401 when the user is not authenticated" in {
      val request = FakeRequest()
      val controller = inject[TimeStatsController]
      val result =
        controller.get(
          eventId = Some(1),
          seasonId = Some(1),
          startDate = "2018-05-01",
          endDate = "2018-05-02"
        )(request)

      status(result) mustBe UNAUTHORIZED
    }

    "return 404 when the event ID does not exist" in {
      eventService.getById(any[Int]) returns Future.successful(None)

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TimeStatsController]
      val result =
        controller.get(
          eventId = Some(1),
          seasonId = Some(1),
          startDate = "2018-05-01",
          endDate = "2018-05-02"
        )(request)

      status(result) mustBe NOT_FOUND
    }

    "return 404 when the season ID does not exist" in {
      seasonService.getById(any[Int], any[Option[Int]], any[Boolean]) returns Future.successful(
        None
      )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TimeStatsController]
      val result =
        controller.get(
          eventId = Some(1),
          seasonId = Some(1),
          startDate = "2018-05-01",
          endDate = "2018-05-02"
        )(request)

      status(result) mustBe NOT_FOUND
    }
  }
}
