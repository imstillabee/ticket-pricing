package com.eventdynamic.api.controllers

import java.sql.Timestamp

import akka.stream.scaladsl.Source
import akka.util.ByteString
import com.eventdynamic.api.controllers.helpers.TestContext
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.test.{FakeRequest, Injecting, WithApplication}
import com.mohiva.play.silhouette.test._
import org.specs2.mock.Mockito
import play.api.test.Helpers._

class TransactionReportControllerSpec
    extends TestContext
    with GuiceOneAppPerTest
    with Injecting
    with Mockito {
  "TransactionReportController#getBySeasonId" should {
    "return 401 if user is not logged in" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[TransactionReportController]
      val result = controller.getBySeasonId(1, None, None)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return 200 on successful report generation" in {
      transactionReportsService.generateReport(
        any[Option[Int]],
        any[Option[Int]],
        any[Option[Timestamp]],
        any[Option[Timestamp]],
        any[Int]
      ) returns Source.single(ByteString("report response"))

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TransactionReportController]
      val result = controller.getBySeasonId(1, None, None)(request)

      status(result) mustBe OK
    }

    "return 500 on unsuccessful report generation" in {
      doThrow(new IllegalArgumentException("Unable to report"))
        .when(transactionReportsService)
        .generateReport(
          any[Option[Int]],
          any[Option[Int]],
          any[Option[Timestamp]],
          any[Option[Timestamp]],
          any[Int]
        )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TransactionReportController]
      val result = controller.getBySeasonId(1, None, None)(request)

      status(result) mustBe INTERNAL_SERVER_ERROR
    }
  }

  "TransactionReportController#getByEventId" should {
    "return 401 if user is not logged in" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[TransactionReportController]
      val result = controller.getByEventId(1, None, None)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return 200 on successful report generation" in {
      doReturn(Source.single(ByteString("report response")))
        .when(transactionReportsService)
        .generateReport(
          any[Option[Int]],
          any[Option[Int]],
          any[Option[Timestamp]],
          any[Option[Timestamp]],
          any[Int]
        )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TransactionReportController]
      val result = controller.getByEventId(1, None, None)(request)

      status(result) mustBe OK
    }

    "return 500 on successful report generation" in {
      doThrow(new IllegalArgumentException("Unable to report"))
        .when(transactionReportsService)
        .generateReport(
          any[Option[Int]],
          any[Option[Int]],
          any[Option[Timestamp]],
          any[Option[Timestamp]],
          any[Int]
        )

      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[TransactionReportController]
      val result = controller.getByEventId(1, None, None)(request)

      status(result) mustBe INTERNAL_SERVER_ERROR
    }
  }
}
