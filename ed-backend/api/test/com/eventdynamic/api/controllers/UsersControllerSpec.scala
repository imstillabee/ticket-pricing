package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import com.eventdynamic.models.User
import com.eventdynamic.services.FailedToSendEmailException
import com.eventdynamic.services.TCode.TCode
import com.eventdynamic.utils._
import com.mohiva.play.silhouette.api.util.Credentials
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.test.{FakeRequest, Injecting, WithApplication}
import com.mohiva.play.silhouette.test._
import org.specs2.mock.Mockito
import play.api.libs.json.Json
import play.api.test.Helpers._
import scala.concurrent.Future

class UsersControllerSpec extends TestContext with GuiceOneAppPerTest with Injecting with Mockito {
  "The create user method" should {
    "return status 401 if not authenticated" in new WithApplication(application) {
      val request = FakeRequest().withBody(
        Json.parse(
          """{ "email": "test@dialexa.com", "password": "123456", "firstName": "Root", "lastName": "User", "isAdmin":false, "clientId": 1 }"""
        )
      )
      val controller = inject[UsersController]
      val result = controller.createUser(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 400 if not supply correct body" in new WithApplication(application) {
      val request = FakeRequest()
        .withBody(Json.parse("""{ "email": "root@dialexa.com", "password": "123456" }"""))
        .withAuthenticator(adminLoginInfo)
      val controller = inject[UsersController]
      val result = controller.createUser(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid request body\"}"
    }

    "return status 403 if user is not admin" in new WithApplication(application) {
      val request = FakeRequest()
        .withBody(
          Json.parse(
            """{ "email": "test@dialexa.com", "password": "123456", "firstName": "Root", "lastName": "User", "isAdmin":false, "clientId": 1 }"""
          )
        )
        .withAuthenticator(loginInfo)
      val controller = inject[UsersController]
      val result = controller.createUser(request)
      val bodyText = contentAsString(result)

      status(result) mustBe FORBIDDEN
      bodyText mustBe "{\"message\":\"Access Denied\"}"
    }

    "return status 201 on successful created User" in new WithApplication(application) {
      userService.create(
        any[String],
        any[String],
        any[String],
        any[Option[String]],
        any[Int],
        any[Option[String]],
        any[Boolean],
        any[Int]
      ) returns Future.successful((identity, "tempPass"))

      val request = FakeRequest()
        .withBody(
          Json.parse(
            """{ "email": "test4@dialexa.com", "password": "123456", "firstName": "Root", "lastName": "User", "isAdmin":false, "clientId": 1 }"""
          )
        )
        .withAuthenticator(adminLoginInfo)
      val controller = inject[UsersController]
      val result = controller.createUser(request)
      val bodyText = contentAsString(result)

      status(result) mustBe CREATED
      bodyText mustBe "{\"message\":\"User successfully created\"}"
    }

    "return status 201 if unable to send email after creation" in new WithApplication(application) {
      userService.create(
        any[String],
        any[String],
        any[String],
        any[Option[String]],
        any[Int],
        any[Option[String]],
        any[Boolean],
        any[Int]
      ) returns Future.successful((identity, "tempPass"))

      val request = FakeRequest()
        .withBody(
          Json.parse(
            """{ "email": "test4@dialexa.com", "password": "123456", "firstName": "Root", "lastName": "User", "isAdmin":false, "clientId": 1 }"""
          )
        )
        .withAuthenticator(adminLoginInfo)

      doThrow(new FailedToSendEmailException("Missing destination email"))
        .when(mailerService)
        .send(any[TCode], any[Map[String, String]])
      val controller = inject[UsersController]
      val result = controller.createUser(request)
      val bodyText = contentAsString(result)

      status(result) mustBe CREATED
      bodyText mustBe """{"message":"Failed to send email to test4@dialexa.com"}"""
    }

  }

  "The delete user method" should {
    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request = FakeRequest().withBody(Json.parse("""{ "email": "test@dialexa.com" }"""))
      val controller = inject[UsersController]
      val result = controller.delete(request)

      status(result) mustBe UNAUTHORIZED
    }

    "return status 400 if user does not provide correct request body" in new WithApplication(
      application
    ) {
      val request = FakeRequest()
        .withAuthenticator(adminLoginInfo)
        .withBody(Json.parse("""{ "": "test@dialexa.com" }"""))
      val controller = inject[UsersController]
      val result = controller.delete(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
    }

    "return status 403 if user is not admin" in new WithApplication(application) {
      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{ "email": "test@dialexa.com" }"""))
      val controller = inject[UsersController]
      val result = controller.delete(request)
      val bodyText = contentAsString(result)

      status(result) mustBe FORBIDDEN
      bodyText mustBe "{\"message\":\"Access Denied\"}"
    }

    "return status 200 if user successfully deleted" in new WithApplication(application) {
      userService.deleteByEmail(any[String]) returns Future.successful(true)

      val request = FakeRequest()
        .withAuthenticator(adminLoginInfo)
        .withBody(Json.parse("""{ "email": "test@dialexa.com" }"""))
      val controller = inject[UsersController]
      val result = controller.delete(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText mustBe "{\"message\":\"User successfully deleted\"}"
    }
  }

  "The get user by Id method" should {
    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[UsersController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 403 if user not admin and id does not match current user" in new WithApplication(
      application
    ) {
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[UsersController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe FORBIDDEN
      bodyText mustBe "{\"message\":\"Access Denied\"}"
    }

    "return status 404 if user could not be found" in new WithApplication(application) {
      userService.getById(any[Int]) returns Future.successful(None)
      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[UsersController]
      val result = controller.getById(4)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"User does not exist or could not be found\"}"
    }

    "return status 200 if user id matches current user" in new WithApplication(application) {
      userService.getById(any[Int]) returns Future.successful(Option(identity))
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[UsersController]
      val result = controller.getById(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }

    "return status 200 if user is admin" in new WithApplication(application) {
      userService.getById(any[Int]) returns Future.successful(Option(identity))
      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[UsersController]
      val result = controller.getById(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }
  }

  "UsersController#update" should {
    "return status 401 if user not authenticated" in new WithApplication(application) {
      val request = FakeRequest().withBody(
        Json.parse(
          """{ "email": "test@dialexa.com", "firstName": "Test", "lastName": "User", "password": "123456" }"""
        )
      )
      val controller = inject[UsersController]
      val result = controller.update(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 404 if user could not be found" in new WithApplication(application) {
      userService.updateUserInfo(any[Int], any[String], any[String], any[Option[String]]) returns Future
        .successful(NotFoundResponse)

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(
          Json.parse("""{ "firstName": "Test", "lastName": "User", "phoneNumber": "1234567890" }""")
        )
      val controller = inject[UsersController]
      val result = controller.update(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"User does not exist or could not be found\"}"
    }

    "return status 200 if successfully updated user information" in new WithApplication(application) {
      userService.updateUserInfo(any[Int], any[String], any[String], any[Option[String]]) returns Future
        .successful(SuccessResponse(identity: User))

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(
          Json.parse("""{ "firstName": "Test", "lastName": "User", "phoneNumber": "1234567890" }""")
        )
      val controller = inject[UsersController]
      val result = controller.update(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }

  }

  "UsersController#updateEmail" should {
    "return status 401 if not authenticated" in new WithApplication(application) {
      val request =
        FakeRequest().withBody(Json.parse("""{ "email": "test@ed.com", "password": "123456"}"""))
      val controller = inject[UsersController]
      val result = controller.updateEmail(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 404 if user could not be found" in new WithApplication(application) {
      userService.updateEmail(any[Int], any[String]) returns Future.successful(NotFoundResponse)
      credentialsProvider.authenticate(any[Credentials]) returns Future
        .successful(loginInfo)

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{ "email": "test@ed.com", "password": "123456"}"""))
      val controller = inject[UsersController]
      val result = controller.updateEmail(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"User does not exist or could not be found\"}"
    }

    "return status 400 if credentials are incorrect" in new WithApplication(application) {
      userService.updateEmail(any[Int], any[String]) returns Future.successful(
        SuccessResponse(identity: User)
      )
      credentialsProvider.authenticate(any[Credentials]) returns Future.failed(new Exception())

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{ "email": "test@ed.com", "password": "123456"}"""))
      val controller = inject[UsersController]
      val result = controller.updateEmail(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Incorrect Email or Password\"}"
    }
  }

  "UsersController#changePassword" should {
    "return status 401 if not authenticated" in new WithApplication(application) {
      val request =
        FakeRequest().withBody(Json.parse("""{ "password": "123456", "newPassword": "654321"}"""))
      val controller = inject[UsersController]
      val result = controller.changePassword(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 400 if credentials are incorrect" in new WithApplication(application) {
      userService.changePassword(any[Int], any[String]) returns Future
        .successful(NotFoundResponse)
      credentialsProvider.authenticate(any[Credentials]) returns Future.failed(new Exception())

      val request = FakeRequest()
        .withAuthenticator(loginInfo)
        .withBody(Json.parse("""{ "password": "123456", "newPassword": "654321" }"""))
      val controller = inject[UsersController]
      val result = controller.changePassword(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Incorrect Email or Password\"}"
    }
  }

  "UsersController#forgotPassword" should {
    "return status 400 if email is not provided" in new WithApplication(application) {
      userService.forgotPassword(any[String], any[Int]) returns Future.successful(None)

      val request =
        FakeRequest().withBody(Json.parse("""{ "wrong": "123456" }"""))
      val controller = inject[UsersController]
      val result = controller.forgotPassword(request)
      val bodyText = contentAsString(result)

      status(result) mustBe BAD_REQUEST
      bodyText mustBe "{\"message\":\"Invalid request body\"}"
    }

    "return status 200 if successfully processed forgot password request" in new WithApplication(
      application
    ) {
      userService.forgotPassword(any[String], any[Int]) returns Future.successful(
        Some((identity, "tempPass"))
      )

      doNothing.when(mailerService).send(any[TCode], any[Map[String, String]])

      val request = FakeRequest().withBody(Json.parse("""{ "email": "test@dialexa.com" }"""))
      val controller = inject[UsersController]
      val result = controller.forgotPassword(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("Password recovery email sent")
    }

    "return status 201 if email fails to send" in new WithApplication(application) {
      userService.forgotPassword(any[String], any[Int]) returns Future.successful(
        Some((identity, "tempPass"))
      )
      doThrow(new FailedToSendEmailException("Missing destination email"))
        .when(mailerService)
        .send(any[TCode], any[Map[String, String]])

      val request = FakeRequest().withBody(Json.parse("""{ "email": "test@dialexa.com" }"""))
      val controller = inject[UsersController]
      val result = controller.forgotPassword(request)
      val bodyText = contentAsString(result)

      status(result) mustBe CREATED
      bodyText mustBe """{"message":"Failed to send forgot password email to test@dialexa.com"}"""
    }
  }

  "UsersController#get" should {
    "return 200 with all non-admin users if user has clientID but not admin" in new WithApplication(
      application
    ) {
      userService.getAll(any[Int], any[Boolean]) returns Future.successful(Seq(identity))
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[UsersController]
      val result = controller.get()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("test@dialexa.com")
      bodyText must not include """"isAdmin":"true""""
      bodyText must not include "password"
    }

    "return 200 with all users if user has clientID and is admin" in new WithApplication(
      application
    ) {
      userService.getAll(any[Int], any[Boolean]) returns Future.successful(
        Seq(adminIdentity, identity)
      )
      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[UsersController]
      val result = controller.get()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("test@dialexa.com")
      bodyText must include("admin@dialexa.com")
      bodyText must include("\"isAdmin\":true")
      bodyText must include("\"isAdmin\":false")
      bodyText must not include "password"
    }

    "return 401 if a user is not authenticated" in {
      val request = FakeRequest()
      val controller = inject[UsersController]
      val result = controller.get()(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }
  }
}
