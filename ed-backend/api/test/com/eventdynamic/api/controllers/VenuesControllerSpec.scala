package com.eventdynamic.api.controllers

import com.eventdynamic.api.controllers.helpers.TestContext
import com.eventdynamic.models.Event
import com.eventdynamic.utils.DateHelper
import com.mohiva.play.silhouette.test._
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.test.{FakeRequest, Injecting, WithApplication}
import play.api.test.Helpers._

import scala.concurrent.Future

class VenuesControllerSpec extends TestContext with GuiceOneAppPerTest with Injecting with Mockito {

  "VenuesController#getById(id: Int)" should {
    "return 401 if unauthenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[VenuesController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 403 if user is not admin or have matching client id" in new WithApplication(
      application
    ) {
      eventService.getByClientAndVenue(any[Int], any[Int]).returns(Future.successful(Seq()))
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[VenuesController]
      val result = controller.getById(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe FORBIDDEN
      bodyText mustBe "{\"message\":\"Access Denied\"}"
    }

    "return status 404 if venue could not be found" in new WithApplication(application) {
      eventService
        .getByClientAndVenue(any[Int], any[Int])
        .returns(Future.successful(Seq(event.get)))
      venueService.getById(2) returns Future.successful(None)

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[VenuesController]
      val result = controller.getById(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"Venue does not exist or could not be found\"}"
    }

    "return status 200 if venue is returned" in new WithApplication(application) {
      eventService
        .getByClientAndVenue(any[Int], any[Int])
        .returns(Future.successful(Seq(event.get)))
      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[VenuesController]
      val result = controller.getById(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
      bodyText must include("\"name\":\"Wrigley Field\"")
      bodyText must include("\"svgUrl\":\"https://eventdynamic.com/fakeVenueSvg\"")
    }
  }

  "VenuesController#getPricescales(id: Int)" should {

    "return 401 if unauthenticated" in new WithApplication(application) {
      val request = FakeRequest()
      val controller = inject[VenuesController]
      val result = controller.getPriceScales(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe UNAUTHORIZED
      bodyText mustBe "{\"message\":\"Unauthenticated\"}"
    }

    "return status 403 if user is not admin or have matching client id" in new WithApplication(
      application
    ) {
      eventService.getByClientAndVenue(any[Int], any[Int]).returns(Future.successful(Seq()))
      val request = FakeRequest().withAuthenticator(loginInfo)
      val controller = inject[VenuesController]
      val result = controller.getPriceScales(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe FORBIDDEN
      bodyText mustBe "{\"message\":\"Access Denied\"}"
    }

    "return status 404 if venue could not be found" in new WithApplication(application) {
      eventService
        .getByClientAndVenue(any[Int], any[Int])
        .returns(Future.successful(Seq(event.get)))
      priceScaleService.getAllForVenue(2) returns Future.successful(Seq())

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[VenuesController]
      val result = controller.getPriceScales(2)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe NOT_FOUND
      bodyText mustBe "{\"message\":\"No PriceScales were found for the specified Venue\"}"
    }

    "return status 200 if priceScales are returned" in new WithApplication(application) {
      eventService
        .getByClientAndVenue(any[Int], any[Int])
        .returns(Future.successful(Seq(event.get)))
      priceScaleService.getAllForVenue(1) returns Future.successful(Seq(priceScale.get))

      val request = FakeRequest().withAuthenticator(adminLoginInfo)
      val controller = inject[VenuesController]
      val result = controller.getPriceScales(1)(request)
      val bodyText = contentAsString(result)

      status(result) mustBe OK
      bodyText must include("\"id\":1")
    }
  }
}
