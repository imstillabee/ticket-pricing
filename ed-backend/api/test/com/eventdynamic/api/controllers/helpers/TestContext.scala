package com.eventdynamic.api.controllers.helpers

import com.eventdynamic.IntegrationServiceBuilder
import com.eventdynamic.api.tasks.MaterializedViewRefreshTask
import com.eventdynamic.models._
import com.eventdynamic.modelserver.ModelServerService
import com.eventdynamic.provenue.ProVenueService
import com.eventdynamic.queues.ScheduledJobQueue
import com.eventdynamic.services._
import com.eventdynamic.tdcreplicatedproxy.TdcReplicatedProxyService
import com.eventdynamic.transactions._
import com.eventdynamic.utils.DateHelper
import com.google.inject.AbstractModule
import com.mohiva.play.silhouette.api.{Environment, LoginInfo}
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import com.mohiva.play.silhouette.test.FakeEnvironment
import net.codingwell.scalaguice.ScalaModule
import org.mindrot.jbcrypt.BCrypt
import org.scalatest.TestData
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import org.specs2.mock.Mockito
import play.api.inject.guice.GuiceApplicationBuilder
import utils.silhouette.{DefaultEnv, UserClientIdentity}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class TestContext extends PlaySpec with GuiceOneAppPerTest with Mockito {
  val now = DateHelper.now()
  val future = DateHelper.later(1440)
  val past = DateHelper.earlier(1440)
  val hash = BCrypt.hashpw("123456", BCrypt.gensalt())
  val tempHash = BCrypt.hashpw("temporary", BCrypt.gensalt())

  val identity = User(
    Some(1),
    now,
    now,
    "test@dialexa.com",
    "test",
    "User",
    Some(hash),
    None,
    Some(now),
    false,
    Some("1234567890"),
    1
  )
  val loginInfo = LoginInfo("credentials", identity.email)

  val adminIdentity = User(
    Some(4),
    now,
    now,
    "admin@dialexa.com",
    "test",
    "User",
    Some(hash),
    None,
    Some(now),
    true,
    None,
    1
  )
  val adminLoginInfo = LoginInfo("credentials", adminIdentity.email)

  implicit val env: Environment[DefaultEnv] = FakeEnvironment[DefaultEnv](
    Seq(
      loginInfo -> UserClientIdentity(identity, Seq(1)),
      adminLoginInfo -> UserClientIdentity(adminIdentity, Seq(1))
    )
  )

  val userService =
    mock[UserService].defaultReturn(Future.successful(adminIdentity))

  val credentialsProvider =
    mock[CredentialsProvider].defaultReturn(Future.successful(adminLoginInfo))

  val event = Some(
    Event(
      id = Some(1),
      primaryEventId = Some(101),
      createdAt = now,
      modifiedAt = now,
      name = "Event",
      timestamp = Some(now),
      clientId = 1,
      venueId = 1,
      eventCategoryId = 1,
      seasonId = None,
      isBroadcast = true,
      percentPriceModifier = 0,
      eventScore = None,
      eventScoreModifier = 0,
      spring = None,
      springModifier = 0
    )
  )
  val eventService = mock[EventService].defaultReturn(Future.successful(event))

  val eventSeat = Some(EventSeat(Some(1), 1, 1, 1, Some(50.00), None, true, now, now, false))
  val eventSeatService = mock[EventSeatService].defaultReturn(Future.successful(eventSeat))

  val eventRow = Some(
    EventRow(1, "AAA", "12", 1, "[1,2,3,4,5,6,7,8,9,10,11]", None, None, None, None, true)
  )
  val eventRowsTransaction = mock[EventRowsTransaction].defaultReturn(Future.successful(event))

  val eventCategory = Some(
    EventCategory(id = Some(1), name = "Classic", clientId = 3, createdAt = now, modifiedAt = now)
  )

  val eventCategoryService =
    mock[EventCategoryService].defaultReturn(Future.successful(eventCategory))

  val inventoryStats = Some(InventoryStats(Some(1), Some(1), 25000, 12500, 10000, 350000, 1000))

  val inventoryStatsTransaction =
    mock[InventoryStatsTransaction].defaultReturn(Future.successful(inventoryStats))

  val pricingJobConfig =
    JobConfig(id = Some(1), createdAt = now, modifiedAt = now, clientId = 1, jobId = 2, json = "")

  val jobConfigService = mock[JobConfigService]

  val client = Some(
    Client(
      Some(1),
      now,
      now,
      30,
      "Test Client",
      Some("https://dialexa.com/logoUrl"),
      2,
      None,
      1.125,
      None,
      ClientPerformanceType.MLB
    )
  )

  val clientService =
    mock[ClientService].defaultReturn(Future.successful(client))

  val identityUserClient = Some(
    UserClient(Some(1), identity.id.get, client.get.id.get, true, now, now)
  )

  val userClientService =
    mock[UserClientService].defaultReturn(Future.successful(identityUserClient))

  val teamStat = Some(
    TeamStat(
      id = Some(1),
      createdAt = now,
      modifiedAt = now,
      clientId = 1,
      seasonId = 1,
      wins = 10,
      losses = 20,
      gamesTotal = 40
    )
  )

  val nflSeasonStat = Some(
    NFLSeasonStat(
      id = Some(1),
      createdAt = now,
      modifiedAt = now,
      clientId = 1,
      seasonId = 1,
      wins = 10,
      losses = 20,
      ties = 2,
      gamesTotal = 40
    )
  )
  val teamStatService = mock[TeamStatService].defaultReturn(Future.successful(teamStat))

  val nflSeasonStatService =
    mock[NFLSeasonStatService].defaultReturn(Future.successful(nflSeasonStat))

  val mlsSeasonStat = Some(
    MLSSeasonStat(
      id = Some(1),
      createdAt = now,
      modifiedAt = now,
      seasonId = 2,
      wins = 12,
      losses = 2,
      ties = 5,
      gamesTotal = 20
    )
  )

  val mlsSeasonStatService =
    mock[MLSSeasonStatService].defaultReturn(Future.successful(mlsSeasonStat))

  val projection =
    Projection(
      id = Some(1),
      eventId = 1,
      timestamp = now.toInstant,
      inventory = 10,
      inventoryLowerBound = 5,
      inventoryUpperBound = 20,
      revenue = 100,
      revenueLowerBound = 50,
      revenueUpperBound = 200
    )
  val projectionService = mock[ProjectionService].defaultReturn(Future.successful(projection))

  val mailerService = mock[MailerService]

  val season = Some(Season(Some(1), Some("Season"), 1, Some(now), Some(now)))

  val seasonService =
    mock[SeasonService].defaultReturn(Future.successful(season))

  val jobPriority = JobPriority(
    id = Some(1),
    name = JobPriorityType.ManualPricingModifierChange,
    priority = 100,
    createdAt = now,
    modifiedAt = now
  )

  val scheduledJob = ScheduledJob(
    id = Some(1),
    eventId = 1,
    jobConfigId = 1,
    status = ScheduledJobStatus.Pending,
    jobPriorityId = jobPriority.id.get,
    startTime = None,
    endTime = None,
    error = None,
    createdAt = now,
    modifiedAt = now
  )

  val jobTriggerComposite = JobTriggerComposite(now, now, JobTriggerType.Manual)

  val scheduledJobTriggerComposite =
    ScheduledJobComposite(scheduledJob, Seq(jobTriggerComposite), jobPriority)

  val scheduledJobQueue =
    mock[ScheduledJobQueue].defaultReturn(Future.successful(Some(scheduledJob)))

  val venue = Some(
    Venue(
      id = Some(1),
      createdAt = DateHelper.now(),
      modifiedAt = DateHelper.now(),
      name = "Wrigley Field",
      capacity = 1,
      zipCode = "75201",
      timeZone = "America/New_York",
      svgUrl = Some("https://eventdynamic.com/fakeVenueSvg")
    )
  )
  val venueService = mock[VenueService].defaultReturn(Future.successful(venue))

  val priceScale = Some(
    PriceScale(Some(1), "priceScaleName", 1, 1, DateHelper.now(), DateHelper.now())
  )
  val priceScaleService = mock[PriceScaleService].defaultReturn(Future.successful(priceScale))

  val clientIntegration = ClientIntegration(Some(1), now, now, 1, 1, true, true, None, None)

  val clientIntegrationComposite = ClientIntegrationComposite(
    Some(1),
    now,
    now,
    1,
    1,
    "New York Mets",
    Some("v1"),
    Some("{ \"customers\": {\"222\": 7}}"),
    true,
    true,
    Some("http://eventdynamic.com/assets/logo.png"),
    Some(0),
    None
  )

  val clientIntegrationService =
    mock[ClientIntegrationService].defaultReturn(Future.successful(clientIntegration))

  val integration =
    Integration(Some(1), now, now, "tickets.com", Some("http://eventdynamic.com/assets/logo.png"))

  val integrationService = mock[IntegrationService].defaultReturn(Future.successful(integration))

  val revenueCategoryRevenue = Seq(
    RevenueCategoryRevenue(1, "Adult", 100.0, 20),
    RevenueCategoryRevenue(1, "Other", 200.0, 30)
  )

  val revenueStatsTransaction =
    mock[RevenueStatsTransaction].defaultReturn(Future.successful(revenueCategoryRevenue))

  val integrationStat = TransactionIntegration(1, "Tickets.com", "someurl", true, 10, 100)

  val integrationStatsTransaction =
    mock[IntegrationStatsTransaction].defaultReturn(Future.successful(Seq(integrationStat)))

  val timeStats = Seq(
    TimeStat(
      DateHelper.now(),
      DateHelper.now(),
      Some(199.99),
      Some(-10),
      Some(1999.99),
      Some(100),
      false
    )
  )

  val timeStatsTransaction: TimeStatsTransaction = mock[TimeStatsTransaction].defaultReturn(
    Future.successful(
      TimeStatsResponse(
        meta = TimeStatsMeta(timeZone = "America/New_York", interval = "Days"),
        data = timeStats
      )
    )
  )

  val materializedViewService =
    mock[MaterializedViewService].defaultReturn(Future.successful(Vector[String]()))

  val materializedViewRefreshTask = mock[MaterializedViewRefreshTask]

  val proVenueService = mock[ProVenueService]

  val tdcReplicatedProxyService = mock[TdcReplicatedProxyService]

  val proVenuePricingRuleService = mock[ProVenuePricingRuleService].defaultReturn(
    Future.successful(
      Seq(
        ProVenuePricingRuleService.CompoundProVenuePricingRule(
          ProVenuePricingRule(
            Some(1),
            DateHelper.now(),
            DateHelper.now(),
            1,
            None,
            true,
            None,
            None,
            None,
            RoundingType.Ceil
          ),
          Seq("1"),
          Seq(1),
          Seq(1)
        )
      )
    )
  )

  val secondaryPricingRuleService = mock[SecondaryPricingRulesService]

  val modelServerService = mock[ModelServerService]

  val transactionReportsService = mock[TransactionReportService]

  val integrationServiceBuilder = mock[IntegrationServiceBuilder]

  class FakeModule extends AbstractModule with ScalaModule {

    override def configure(): Unit = {
      bind[Environment[DefaultEnv]].toInstance(env)
      bind[CredentialsProvider].toInstance(credentialsProvider)
      bind[UserService].toInstance(userService)
      bind[EventService].toInstance(eventService)
      bind[EventCategoryService].toInstance(eventCategoryService)
      bind[ClientService].toInstance(clientService)
      bind[UserClientService].toInstance(userClientService)
      bind[TeamStatService].toInstance(teamStatService)
      bind[VenueService].toInstance(venueService)
      bind[PriceScaleService].toInstance(priceScaleService)
      bind[ProjectionService].toInstance(projectionService)
      bind[MailerService].toInstance(mailerService)
      bind[SeasonService].toInstance(seasonService)
      bind[ClientIntegrationService].toInstance(clientIntegrationService)
      bind[IntegrationService].toInstance(integrationService)
      bind[EventSeatService].toInstance(eventSeatService)
      bind[EventRowsTransaction].toInstance(eventRowsTransaction)
      bind[RevenueStatsTransaction].toInstance(revenueStatsTransaction)
      bind[TimeStatsTransaction].toInstance(timeStatsTransaction)
      bind[IntegrationStatsTransaction].toInstance(integrationStatsTransaction)
      bind[InventoryStatsTransaction].toInstance(inventoryStatsTransaction)
      bind[MaterializedViewService].toInstance(materializedViewService)
      bind[MaterializedViewRefreshTask].toInstance(materializedViewRefreshTask)
      bind[ProVenueService].toInstance(proVenueService)
      bind[ProVenuePricingRuleService].toInstance(proVenuePricingRuleService)
      bind[SecondaryPricingRulesService].toInstance(secondaryPricingRuleService)
      bind[JobConfigService].toInstance(jobConfigService)
      bind[ScheduledJobQueue].toInstance(scheduledJobQueue)
      bind[ModelServerService].toInstance(modelServerService)
      bind[TransactionReportService].toInstance(transactionReportsService)
      bind[IntegrationServiceBuilder].toInstance(integrationServiceBuilder)
      bind[NFLSeasonStatService].toInstance(nflSeasonStatService)
      bind[MLSSeasonStatService].toInstance(mlsSeasonStatService)
    }
  }

  val application = new GuiceApplicationBuilder().overrides(new FakeModule).build()

  implicit override def newAppForTest(td: TestData) = application
}
