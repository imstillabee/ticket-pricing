package com.eventdynamic.api.models

import java.sql.Timestamp

import com.eventdynamic.models.Season
import org.scalatest.FlatSpec

class SeasonApiModelSpec extends FlatSpec with SeasonApiModel {
  "SeasonsApiModel.fromSeasons" should "convert seasons to models" in {
    val seasons = Seq[Season](
      Season(None, None, 1, None, None),
      Season(None, None, 1, Some(new Timestamp(1000)), None),
      Season(None, None, 1, Some(new Timestamp(500)), None),
      Season(None, None, 1, Some(new Timestamp(100)), None)
    )

    val now = new Timestamp(750)

    val seasonModels = SeasonModel.fromSeasons(seasons, now)

    assert(seasonModels.length === 4)
    assert(seasonModels(0).isCurrentSeason === false)
    assert(seasonModels(1).isCurrentSeason === false)
    assert(seasonModels(2).isCurrentSeason === true)
    assert(seasonModels(3).isCurrentSeason === false)
  }
}
