package com.eventdynamic.api.tasks

import akka.actor.{ActorSystem, Props}
import akka.testkit.TestKit
import com.eventdynamic.services.MaterializedViewService
import org.scalatest.WordSpecLike
import org.specs2.mock.Mockito

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class MaterializedViewRefreshTaskSpec
    extends TestKit(ActorSystem())
    with WordSpecLike
    with Mockito {
  "RefreshActor" should {

    val materializedViewService =
      mock[MaterializedViewService].defaultReturn(Future.successful(Vector[String]()))
    val refreshActor = system.actorOf(Props(new RefreshActor(materializedViewService)))

    "call refresh when sent Refresh" in {
      refreshActor ! Refresh()

      there was exactly(1)(materializedViewService).refreshAll()
    }

    "not call refresh when sent other" in {
      refreshActor ! "test"

      there was exactly(0)(materializedViewService).refreshAll()
    }
  }
}
