# Backfill

This project takes historic data from different sources and merges them into a single observations table.  It takes files from the `backfill` directory, merges them into a single table, and exports it to a partition parquet table as `observations`.  

## Running

The script builds parameters from the arguments passed into the main function.  The params are as follows:

1. `master` - the address of the spark master (or `pass` if use the system default)
1. `baseUrl` - the base url of the client data
1. `daysPerBatch` - the number of days to process at each increment

### Running locally

For local running, use `sbt backfill/run local[*] /path/to/data 1`.  You could also set up your run configurations in Intellij to pass these params in on run.

### Running on AWS EMR

To run on an EMR cluster or via `spark-submit`, you need to export the project as a jar file.  Use `sbt backfill/assembly` to create a jar file.

## Backfill files

These are flat files in the `backfill` folder that we pull from various sources to merge together.  Because the same data for different clients can come from different sources/formats, we convert them to the below standards for consistency.

### Formats

#### `price-change-ranges.csv.gz`

Contains the listing prices on a scale level.  This data was originally pulled from QCue.

1. `event_date` - timestamp of when the event starts (used as unique key for event)
1. `scale` - supersection of seats, typically prices are set at this level (eg. Metropolitan Gold)
1. `price` - the listed price of the ticket
1. `change_date` - the start timestamp that the ticket started at this price
1. `next_change_date` - the end timestamp that the ticket

#### `transactions.csv.gz`

Contains all transactions for all events.  This data was originally pulled from TDC.

1. `event_date` - timestamp of when the event starts (used as unique key for event)
1. `price` - the price that the ticket was sold (or negative price for returns)
1. `timestamp` - the timestamp of the transaction
1. `scale` - supersection of seats, typically prices are set at this level (eg. Metropolitan Gold)
1. `section` - the section that the seat is in
1. `row` - the row in the section
1. `seat` - the seat in the row

#### `actual-weather.csv`

Contains the actual weather at a given timestamp.  This data was scraped from Wunderground's historic data.

The columns after `temp` are a one hot encoded condition vector.  Only one can be `1`, the others will be `0`.

1. `timestamp` - timestamp of the weather reading
1. `temp` - the actual temperature
1. `clear` - indicates clear weather
1. `clouds` - indicates light clouds
1. `clouds_heavy` - indicates overcast, fog, etc.
1. `rain_light` - indicates drizzling rain, mist, etc.
1. `rain_medium` - indicates a moderate amount of rain
1. `rain_heavy` - indicates a heavy rainfall
1. `snow_light` - indicates a light snowfall
1. `snow_medium` - indicates a moderate snowfall
1. `snow_heavy` - indicates a heavy snowfall

#### `average-weather.csv`

Contains the average weather per hour.  This data was pull from the NOAA government website.

1. `month` - the month of the reading, January = 1
1. `day` - the day of month of the reading
1. `hour` - the hour of the reading, midnight = 0, 11pm = 23
1. `temp` - the average temperature at the indicated hour

#### `mlb-stats.csv`

Contains the statistics of the team over time.  This data was pulled from baseball-reference.com.

1. `game_num` - the number of game of the season
1. `opponent` - currently just a string of the 3 letter initial of the opponent, to be expanded on later
1. `rank` - the divisional rank of the team
1. `streak` - the number of games won in a row (or negative if loss)
1. `wins` - the number of wins after this game
1. `losses` - the number of losses after this game
1. `game_length` - the length of the game - used to determine when to add stats
1. `games_ahead` - the number of games ahead the team is from second place, if behind the number is negative
1. `event_date` - the date of the event
1. `home_opener` - 1 if the event is a home opener, 0 if not
1. `home_series_start` - 1 if the event is the first of a home series, 0 if not
