package com.eventdynamic.backfill

import java.sql.Timestamp

import com.eventdynamic.backfill.models._
import com.eventdynamic.backfill.process._
import com.eventdynamic.backfill.utils.TimestampUtils
import org.apache.log4j.LogManager
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.storage.StorageLevel

/**
  * This is the main file for the backfill script.  The main function validates the parameters, creates a spark
  * context, and kicks off a backfill process.
  *
  * The backfill process incrementally builds on a dataframe that eventually contains all information needed for
  * the observations.  After the observation dataframe is built, it saves it to the observations.
  */
object Backfill {
  private val log = LogManager.getLogger(getClass.getName)

  val PRICE_CHANGE_FILE = "/backfill/price-change-ranges.csv.gz"
  val TRANSACTION_FILE = "/backfill/transactions.csv"
  val ACTUAL_WEATHER_FILE = "/backfill/actual-weather.csv"
  val AVERAGE_WEATHER_FILE = "/backfill/average-weather.csv"
  val MLB_STATS_FILE = "/backfill/mlb-stats.csv"
  val WAREHOUSE_DIR = "/hive"

  val MILLIS_PER_DAY: Int = 1000 * 60 * 60 * 24
  val MILLIS_PER_15_MIN: Int = 1000 * 60 * 15

  /**
    * Parses the input parameters and kicks off the backfill process.
    *
    * @param args args passed in, see readme/BackfillParams for details
    */
  def main(args: Array[String]): Unit = {
    // Validate that we have our parameters
    BackfillParams.validate(args) match {
      case None =>
        log.error("Invalid params")
      case Some(params) =>
        System.setSecurityManager(null)

        // Create the spark session
        var sparkBuilder = SparkSession
          .builder()
          .appName("ED Backfill")
          .enableHiveSupport()
          .config("hive.exec.dynamic.partition", "true")
          .config("hive.exec.dynamic.partition.mode", "nonstrict")
          .config("spark.sql.sources.partitionOverwriteMode", "dynamic")
          .config("spark.sql.warehouse.dir", params.baseUrl + WAREHOUSE_DIR)

        // Set the master if one is passed in
        if (params.master.nonEmpty) {
          sparkBuilder = sparkBuilder.master(params.master.get)
        }

        // Create the session
        val spark = sparkBuilder.getOrCreate()

        // Run our process function given params and spark session
        processBackfillIncrementally(spark, params)
    }
  }

  /**
    * Chains together the merging and manipulation processes to get the final observations table.  Save the
    * observations when done.
    *
    * @param spark the spark session to use
    * @param params parameters for the backfill job, from java args
    */
  def processBackfillIncrementally(spark: SparkSession, params: BackfillParams): Unit = {
    import spark.implicits._

    ///
    /// Read in data from files
    ///

    val priceChanges = PriceChange
      .fromFile(spark, params.baseUrl + PRICE_CHANGE_FILE)
      .persist(StorageLevel.MEMORY_AND_DISK)

    val transactions = Transaction
      .fromFile(spark, params.baseUrl + TRANSACTION_FILE)
      .filter($"timestamp" <= $"event_date")

    val actualWeather =
      ActualWeather.fromFile(spark, params.baseUrl + ACTUAL_WEATHER_FILE)
    val averageWeather =
      AverageWeather.fromFile(spark, params.baseUrl + AVERAGE_WEATHER_FILE)
    val mlbStats = MLBStats
      .fromFile(spark, params.baseUrl + MLB_STATS_FILE)
      .persist(StorageLevel.MEMORY_AND_DISK)
    val localMLBStats = MLBStats.createRankMap(mlbStats.collect())

    ///
    /// Derive data points from data from flat files
    ///

    val transWithNext = TransactionWithNext
      .fromTransactions(spark, transactions)
      .persist(StorageLevel.MEMORY_AND_DISK)
    val seatMappings =
      SeatMapping
        .fromTransactions(spark, transWithNext)
        .persist(StorageLevel.MEMORY_AND_DISK)
    val weatherMappings = WeatherMapping
      .combineWeatherData(spark, priceChanges, averageWeather, actualWeather)
      .persist(StorageLevel.MEMORY_AND_DISK)

    val (minTime, maxTime) = getMinMaxTimes(priceChanges)

    ///
    /// Maintain running table of most recent transactions and price changes
    ///

    var recentTrans = spark.emptyDataset[TransactionWithNext]
    var recentChanges = spark.emptyDataset[PriceChange]

    ///
    /// Prepare for iterative backfill process
    ///

    // Logging
    val millisInBatch = params.daysPerBatch * MILLIS_PER_DAY
    log.info(s"Min date: ${new Timestamp(minTime)}")
    log.info(s"Max date: ${new Timestamp(maxTime)}")
    log.info(s"Days per batch: ${params.daysPerBatch}")

    var currentTime = minTime
    while (currentTime < maxTime) {
      // Generate times for this batch
      val targetTime = Math.min(currentTime + millisInBatch, maxTime)
      val currentTimestamp = new Timestamp(currentTime)
      val targetTimestamp = new Timestamp(targetTime)

      // Logging
      log.info(s"Batch start: $currentTimestamp")
      log.info(s"Batch end: $targetTimestamp")

      // Get the transactions and price changes in this interval
      val currentTrans = transWithNext.filter(
        $"trans_timestamp" >= currentTimestamp &&
          $"trans_timestamp" < targetTimestamp
      )
      val currentChanges = priceChanges.filter(
        $"change_date" >= currentTimestamp &&
          $"change_date" < targetTimestamp
      )

      // Generate range and spread it to cluster
      val timeIntervalSeq =
        TimestampUtils.generateTimestampSeq15(currentTimestamp, targetTimestamp)
      val timeIntervals = spark.sparkContext
        .parallelize(timeIntervalSeq)
        .toDF("timestamp")

      // Merge current with recent to build off of
      val combinedTrans =
        recentTrans.union(currentTrans).persist(StorageLevel.MEMORY_AND_DISK)
      val combinedChanges =
        recentChanges
          .union(currentChanges)
          .persist(StorageLevel.MEMORY_AND_DISK)

      ///
      /// Merge difference data sources together
      ///

      // Create observations from all tables
      val seatData =
        SeatMappingProcessor.process(spark, seatMappings, timeIntervals)
      val changeData =
        PriceChangeProcessor.process(spark, combinedChanges, seatData, targetTimestamp)
      val transData =
        TransactionProcessor.process(spark, combinedTrans, changeData, targetTimestamp)
      val weatherData =
        WeatherProcessor.process(spark, weatherMappings, transData)
      val mlbData = MLBStatsProcessor.process(spark, mlbStats, localMLBStats, weatherData)

      // Convert last transformation to observations
      val observations = mlbData.as[Observation]

      ///
      /// Persist observations to disk
      ///

      observations.explain(true)

      val obsWriter = observations
        .withColumn("year", year($"timestamp"))
        .withColumn("month", month($"timestamp"))
        .withColumn("day", dayofmonth($"timestamp"))
        .write
        .mode(SaveMode.Overwrite)

      // Create table if it doesn't exist, else add to it
      if (spark.catalog.tableExists("observations")) {
        log.info("Inserting batch to table")
        obsWriter.insertInto("observations")
      } else {
        log.info("Creating table from batch")
        obsWriter
          .partitionBy("year", "month", "day")
          .saveAsTable("observations")
      }

      ///
      /// Clear old transactions and price ranges, prepare for next batch
      ///

      recentTrans = updateRecentTransactions(spark, combinedTrans, targetTimestamp).persist(
        StorageLevel.MEMORY_AND_DISK
      )
      recentChanges = updateRecentPriceChanges(spark, combinedChanges, targetTimestamp)
        .persist(StorageLevel.MEMORY_AND_DISK)

      // Increment current time by the batch size
      currentTime += millisInBatch
    }
  }

  /**
    * Get the date range of observations we need to generate based on the price changes.
    *
    * @param priceChanges the price change dataset
    * @return tuple of min date (rounded to the day) and the last event date
    */
  def getMinMaxTimes(priceChanges: Dataset[PriceChange]): (Long, Long) = {
    // Get the min and max dates from our price change data
    // Truncate the start time to the day
    val Row(minDate: Timestamp, maxDate: Timestamp) = priceChanges
      .agg(min(date_trunc("DD", priceChanges("change_date"))), max("event_date"))
      .head()

    (minDate.getTime, maxDate.getTime)
  }

  /**
    * Trims the recent transactions to just the most recent per seat for upcoming events.
    *
    * @param spark spark session
    * @param combinedTrans the current state of the transactions after blending
    * @param targetTimestamp the next timestamp
    * @return the trimmed transactions
    */
  def updateRecentTransactions(
    spark: SparkSession,
    combinedTrans: Dataset[TransactionWithNext],
    targetTimestamp: Timestamp
  ): Dataset[TransactionWithNext] = {
    import spark.implicits._

    combinedTrans
      .where($"event_date" >= targetTimestamp)
      .groupByKey(t => (t.section, t.row, t.seat, t.event_date))
      .reduceGroups(
        (t1, t2) =>
          if (t1.trans_timestamp.getTime > t2.trans_timestamp.getTime) t1
          else t2
      )
      .map(_._2)
  }

  /**
    * Trims the recent price changes to just the most recent per scale for upcoming events.
    *
    * @param spark spark session
    * @param combinedChanges the current state of the price changes after blending
    * @param targetTimestamp the next timestamp
    * @return the trimmed price changes
    */
  def updateRecentPriceChanges(
    spark: SparkSession,
    combinedChanges: Dataset[PriceChange],
    targetTimestamp: Timestamp
  ): Dataset[PriceChange] = {
    import spark.implicits._

    combinedChanges
      .where($"event_date" >= targetTimestamp)
      .groupByKey(pc => (pc.scale, pc.event_date))
      .reduceGroups(
        (pc1, pc2) =>
          if (pc1.change_date.getTime > pc2.change_date.getTime) pc1
          else pc2
      )
      .map(_._2)
  }
}
