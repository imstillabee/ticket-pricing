package com.eventdynamic.backfill.models

import java.sql.Timestamp

import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

/**
  * Represents the actual weather at a specific timestamp.  Flag fields are either 1 or 0.  They are one hot encoded
  * so only one is "true" at a time.
  *
  * @param timestamp the time of the weather reading
  * @param temp the temperature in fahrenheit
  * @param clear clear weather flag
  * @param clouds is some form of cloudy flag
  * @param clouds_heavy is overcast or foggy flag
  * @param rain_light  is light rain flag
  * @param rain_medium is moderate rain flag
  * @param rain_heavy is heavy rain flag
  * @param snow_light is lightly snowing flag
  * @param snow_medium is moderate snowing flag
  * @param snow_heavy is heady snowing flag
  */
case class ActualWeather(
  timestamp: Timestamp,
  temp: Float,
  clear: Int,
  clouds: Int,
  clouds_heavy: Int,
  rain_light: Int,
  rain_medium: Int,
  rain_heavy: Int,
  snow_light: Int,
  snow_medium: Int,
  snow_heavy: Int
)

object ActualWeather {

  def fromFile(spark: SparkSession, actWeatherFile: String): Dataset[ActualWeather] = {
    import spark.implicits._

    val actWeatherSchema = Encoders.product[ActualWeather].schema
    spark.sqlContext.read
      .format("csv")
      .option("header", "true")
      .schema(actWeatherSchema)
      .load(actWeatherFile)
      .as[ActualWeather]
  }
}
