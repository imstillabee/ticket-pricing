package com.eventdynamic.backfill.models

import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

/**
  * Represents the average weather for an hour in the year.
  *
  * @param month the month of the average (January = 1)
  * @param day the day of month of the average
  * @param hour the hour of the average (0 = midnight, 23 = 11pm)
  * @param temp the average temperature in deg fahrenheit
  */
case class AverageWeather(month: Int, day: Int, hour: Int, temp: Float)

object AverageWeather {

  def fromFile(spark: SparkSession, avgWeatherFile: String): Dataset[AverageWeather] = {
    import spark.implicits._

    val priceChangeSchema = Encoders.product[AverageWeather].schema
    spark.sqlContext.read
      .format("csv")
      .option("header", "true")
      .schema(priceChangeSchema)
      .load(avgWeatherFile)
      .as[AverageWeather]
  }
}
