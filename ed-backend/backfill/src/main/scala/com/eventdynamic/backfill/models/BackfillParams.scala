package com.eventdynamic.backfill.models

/**
  * Parameters for the backfill script.
  *
  * @param master the address of the spark master
  * @param baseUrl the location of the client files
  * @param daysPerBatch the number of days to process per batch
  */
case class BackfillParams(master: Option[String], baseUrl: String, daysPerBatch: Int)

object BackfillParams {

  def validate(args: Array[String]): Option[BackfillParams] = {
    if (args.length != 3) {
      None
    } else {
      // If pass, don't set master
      val master = if (args(0).toLowerCase() == "pass") None else Some(args(0))

      val daysPerBatch = Integer.parseInt(args(2))

      Option.apply(BackfillParams(master, args(1), daysPerBatch))
    }
  }
}
