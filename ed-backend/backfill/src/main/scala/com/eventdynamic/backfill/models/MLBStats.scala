package com.eventdynamic.backfill.models

import java.sql.Timestamp

import com.eventdynamic.backfill.process.MLBRank
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

case class MLBStats(
  game_num: Int,
  opponent: String,
  rank: Int,
  streak: Int,
  wins: Int,
  losses: Int,
  game_length: Int,
  games_ahead: Float,
  event_date: Timestamp,
  home_opener: Int,
  home_series_start: Int
)

object MLBStats {

  val MILLIS_IN_MINUTE: Int = 1000 * 60

  def fromFile(spark: SparkSession, mlbStatsFile: String): Dataset[MLBStats] = {
    import spark.implicits._

    val mlbStatsSchema = Encoders.product[MLBStats].schema
    spark.sqlContext.read
      .format("csv")
      .option("header", "true")
      .schema(mlbStatsSchema)
      .load(mlbStatsFile)
      .as[MLBStats]
  }

  /**
    * Creates a mapping for season: event list in order
    *
    * @param mlbStats
    * @return
    */
  def createRankMap(mlbStats: Seq[MLBStats]) = {
    mlbStats
      .map(
        mlb =>
          (
            new Timestamp(mlb.event_date.getTime + mlb.game_length * MILLIS_IN_MINUTE),
            MLBRank(mlb.rank, mlb.streak, mlb.wins, mlb.losses, mlb.games_ahead)
        )
      )
      .groupBy(_._1.toLocalDateTime.getYear)
      .mapValues(r => r.sortBy(_._1.getTime))
      .map(identity)
  }
}
