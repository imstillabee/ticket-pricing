package com.eventdynamic.backfill.models

import java.sql.Timestamp
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

/**
  * This represents a data input file for price changes.  A grouping of (event_date, scale) should contain a list of
  * all price changes for a scale leading up to an event - from when tickets went on sale to the start of the event.
  *
  * @param event_date date of the event
  * @param scale price scale (eg Metropolitan Gold)
  * @param price new price of the ticket
  * @param change_date date the price was set
  * @param next_change_date date of the next time the price changes, event time if never changes again
  */
case class PriceChange(
  event_date: Timestamp,
  scale: String,
  price: Option[Double],
  change_date: Timestamp,
  next_change_date: Timestamp
)

object PriceChange {

  def fromFile(spark: SparkSession, priceChangeFile: String): Dataset[PriceChange] = {
    import spark.implicits._

    val priceChangeSchema = Encoders.product[PriceChange].schema
    spark.sqlContext.read
      .format("csv")
      .option("header", "true")
      .schema(priceChangeSchema)
      .load(priceChangeFile)
      .as[PriceChange]
  }
}
