package com.eventdynamic.backfill.models

import org.apache.spark.sql.{Dataset, SparkSession}

case class SeatMapping(map_scale: String, map_section: String, map_row: String, map_seat: String)

object SeatMapping {

  /**
    * Get the mappings from scale to each seat.  We need this to make sure we cover price changes for each seat
    * at all times.
    *
    * @param transactions all of the transactions
    * @return a data set containing the distinct scale, section, row, and seat combinations
    */
  def fromTransactions(
    spark: SparkSession,
    transactions: Dataset[TransactionWithNext]
  ): Dataset[SeatMapping] = {
    import spark.implicits._

    // The mapping from scale to seats - based off of entire transaction history
    // If a seat is never sold, then we wouldn't find it here, but we should never hit that case
    transactions
      .select(
        transactions("trans_scale").alias("map_scale"),
        transactions("section").alias("map_section"),
        transactions("row").alias("map_row"),
        transactions("seat").alias("map_seat")
      )
      .distinct()
      .as[SeatMapping]
  }
}
