package com.eventdynamic.backfill.models

import java.sql.Timestamp
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

/**
  * Represents each transaction for a client.
  *
  * @param event_date date of the event
  * @param price price of the transaction, negative means return
  * @param timestamp date of the transaction
  * @param scale the price scale of the transaction
  * @param section the section in the stadium
  * @param row the row in the section
  * @param seat the seat in the row
  */
case class Transaction(
  event_date: Timestamp,
  price: Double,
  timestamp: Timestamp,
  scale: String,
  section: String,
  row: String,
  seat: String
)

object Transaction {

  def fromFile(spark: SparkSession, transactionFile: String): Dataset[Transaction] = {
    import spark.implicits._

    if (!spark.catalog.tableExists("transactions")) {
      val transactionSchema = Encoders.product[Transaction].schema
      val csvTransactions = spark.sqlContext.read
        .format("csv")
        .option("header", "true")
        .schema(transactionSchema)
        .load(transactionFile)
        .as[Transaction]

      csvTransactions.write.saveAsTable("transactions")
    }

    spark.sqlContext.table("transactions").as[Transaction]
  }
}
