package com.eventdynamic.backfill.models

import java.sql.Timestamp

import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.lag

/**
  * Represents each transaction for a client.
  *
  * @param event_date date of the event
  * @param trans_price price of the transaction, negative means return
  * @param trans_timestamp date of the transaction
  * @param trans_scale the price scale of the transaction
  * @param section the section in the stadium
  * @param row the row in the section
  * @param seat the seat in the row
  */
case class TransactionWithNext(
  event_date: Timestamp,
  trans_price: Double,
  trans_timestamp: Timestamp,
  trans_scale: String,
  section: String,
  row: String,
  seat: String,
  next_trans_timestamp: Timestamp
)

object TransactionWithNext {
  // Arbitrary date in the future
  val FUTURE_DATE = Timestamp.valueOf("3000-01-01 00:00:00")

  /**
    * Convert transactions from a point in time to a date range when the transaction applies.  This helps by
    * creating a more efficient merge process.  If the transaction is the last one for that event, then
    * we set the next timestamp to an arbitrary date far in the future.
    *
    * @param transactions the transaction dataset with some columns renamed.
    * @return A new transaction dataframe with a new "next_trans_timestep" column
    */
  def fromTransactions(
    spark: SparkSession,
    transactions: Dataset[Transaction]
  ): Dataset[TransactionWithNext] = {
    import spark.implicits._

    // Rename some columns for joining purposes
    val renamedTrans = transactions
      .withColumnRenamed("timestamp", "trans_timestamp")
      .withColumnRenamed("price", "trans_price")
      .withColumnRenamed("scale", "trans_scale")

    // Windowing function to group transactions of a single seat for an event
    val window = Window
      .partitionBy("event_date", "section", "row", "seat")
      .orderBy("trans_timestamp")

    // Function to generate the next timestamp column using a lag over the window
    val lagCol = lag("trans_timestamp", -1, FUTURE_DATE).over(window)

    // Uses the window function to get the next transaction's date
    // If there is no next transaction, set it to some far date in the future
    renamedTrans
      .withColumn("next_trans_timestamp", lagCol)
      .as[TransactionWithNext]
  }
}
