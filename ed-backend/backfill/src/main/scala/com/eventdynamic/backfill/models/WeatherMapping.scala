package com.eventdynamic.backfill.models

import java.sql.Timestamp

import org.apache.spark.api.java.function.ReduceFunction
import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.functions._

/**
  * Encapsulates both the actual weather and average weather for an event.  This is essentially a mapping that can be
  * easily joined to the observations in the future.
  *
  * @param event_date date of an event
  * @param average the closest average weather object
  * @param actual the closest actual weather object
  */
case class WeatherMapping(event_date: Timestamp, average: AverageWeather, actual: ActualWeather)

object WeatherMapping {

  /**
    * Generates a map between each event and the corresponding actual weather and average weather for that event.
    *
    * @param spark spark session
    * @param priceChanges the price changes, only needed to grab events from
    * @param averages the average weather dataset
    * @param actuals the actual weather dataset
    * @return a dataset containing the closest average weather and actual weather objects
    */
  def combineWeatherData(
    spark: SparkSession,
    priceChanges: Dataset[PriceChange],
    averages: Dataset[AverageWeather],
    actuals: Dataset[ActualWeather]
  ): Dataset[WeatherMapping] = {
    import spark.implicits._

    // Get the event dates
    val events = priceChanges.select("event_date").distinct()

    // Find the weather average closest to the event date
    val eventsWithAverage = events.joinWith(
      averages,
      month($"event_date") === $"month" &&
        dayofmonth($"event_date") === $"day" &&
        round(hour($"event_date") + (minute($"event_date") / 60.0)) === $"hour"
    )

    // Merge the weather data with average with the actuals
    // Match the days in this join, narrow down to the closest minute in a group by function
    val eventsWeatherByDay = eventsWithAverage
      .joinWith(
        actuals,
        year($"_1.event_date") === year($"timestamp") &&
          month($"_1.event_date") === month($"timestamp") &&
          dayofmonth($"_1.event_date") === dayofmonth($"timestamp")
      )
      .map(row => WeatherMapping(row._1._1.getTimestamp(0), row._1._2, row._2))

    // For each event, find the closest actual weather object down to the minute
    eventsWeatherByDay
      .groupByKey(_.event_date)
      .reduceGroups(new ReduceWeather())
      .map(_._2)
  }

  class ReduceWeather extends ReduceFunction[WeatherMapping] {

    /**
      * Returns the weather data with an actual timestamp closest to the event date.  If the two times are the same
      * distance, take the actual weather data after the event starts.
      *
      * @param v1 first weather to compare
      * @param v2 second weather to compare
      * @return the "closest weather"
      */
    override def call(v1: WeatherMapping, v2: WeatherMapping): WeatherMapping = {
      // Get time differences
      val v1Diff = Math.abs(v1.actual.timestamp.getTime - v1.event_date.getTime)
      val v2Diff = Math.abs(v2.actual.timestamp.getTime - v2.event_date.getTime)

      // Return closest
      if (v1Diff < v2Diff) {
        v1
      } else if (v2Diff < v1Diff) {
        v2
      } else {
        // If the same distance, return the later (when the event is happening)
        if (v1.actual.timestamp.getTime > v2.actual.timestamp.getTime) v1
        else v2
      }
    }
  }
}
