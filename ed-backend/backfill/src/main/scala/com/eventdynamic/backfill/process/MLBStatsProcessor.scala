package com.eventdynamic.backfill.process

import java.sql.Timestamp

import com.eventdynamic.backfill.models.MLBStats
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, SparkSession}

object MLBStatsProcessor {

  /**
    * Joins the MLB onto the observations.  The rank data is is attached based on the timestamp of the reading,
    * the event data is attached based on the event date.
    *
    * @param spark spark session to use
    * @param mlbStats the dataset of the mlb stats
    * @param mlbRanks the rankings with the timestamp of the game ending time attached
    * @param postWeather the observations to attach the mappings to
    * @return Dataset of PostSeatMap
    */
  def process(
    spark: SparkSession,
    mlbStats: Dataset[MLBStats],
    mlbRanks: Map[Int, Seq[(Timestamp, MLBRank)]],
    postWeather: Dataset[PostWeather]
  ): Dataset[PostMLBStats] = {
    import spark.implicits._

    val broadcastRanks = spark.sparkContext.broadcast(mlbRanks)

    postWeather
      .joinWith(broadcast(mlbStats), postWeather("event_date") === mlbStats("event_date"))
      .map(row => {
        // Shorthand notation for grabbing data
        val (pw, mlb) = row

        // Extract event data from joined table
        val mlbEvent = MLBEvent(mlb.game_num, mlb.opponent, mlb.home_opener, mlb.home_series_start)

        // Not the most efficient way of doing this...
        val mlbRank = broadcastRanks
          .value(pw.event_date.toLocalDateTime.getYear)
          .find(_._1.getTime < pw.timestamp.getTime)
          .map(_._2)
          .getOrElse(MLBRank(1, 0, 0, 0, 0))

        PostMLBStats(
          pw.timestamp,
          pw.event_date,
          pw.scale,
          pw.section,
          pw.row,
          pw.seat,
          pw.listed_price,
          pw.sold_price,
          pw.weather,
          mlbEvent,
          mlbRank
        )
      })
  }
}
