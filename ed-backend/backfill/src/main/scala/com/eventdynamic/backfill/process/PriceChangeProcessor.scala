package com.eventdynamic.backfill.process

import java.sql.Timestamp

import com.eventdynamic.backfill.models.PriceChange
import com.eventdynamic.backfill.utils.TimestampUtils
import org.apache.spark.sql._

object PriceChangeProcessor {

  /**
    * This takes a set of price changes, which contain the price of a scale between a set of dates, and attaches
    * those to the seat/timestamp table.
    *
    * @param spark spark session we're building
    * @param priceChanges the price changes in this batch
    * @param mapData dataframe with timestamp and map_scale
    * @return A dataframe of 15 minute price intervals with prices dictated by the input price ranges
    */
  def process(
    spark: SparkSession,
    priceChanges: Dataset[PriceChange],
    mapData: Dataset[PostSeatMap],
    maxTimestamp: Timestamp
  ): Dataset[PostPriceChange] = {
    import spark.implicits._

    // Encoder needed to map rows
    val pctEncoder = Encoders.product[PriceChangeTimestamp]

    // Expand the price changes from ranges to one for each 15 minutes
    val priceChangesExpanded = priceChanges.flatMap(pc => {
      // The ending timestamp for this sequence
      val targetTime =
        Seq(pc.event_date.getTime, pc.next_change_date.getTime, maxTimestamp.getTime).min

      // Generate the timestamps between the change and next change dates, map to new rows
      TimestampUtils
        .generateTimestampSeq15(pc.change_date, new Timestamp(targetTime))
        .map(PriceChangeTimestamp(_, pc.event_date, pc.scale, pc.price))
    })(pctEncoder)

    // Join the price changes with the dates to get prices for each timestamp
    priceChangesExpanded
      .join(
        mapData,
        mapData("timestamp") === priceChangesExpanded("timestamp") &&
          mapData("map_scale") === priceChangesExpanded("scale")
      )
      .drop("map_scale")
      .drop(priceChangesExpanded("timestamp"))
      .withColumnRenamed("price", "listed_price")
      .as[PostPriceChange]
  }

  /**
    * Converts price changes with range to price changes for each timestamp.
    *
    * @param timestamp
    * @param event_date
    * @param price
    */
  private case class PriceChangeTimestamp(
    timestamp: Timestamp,
    event_date: Timestamp,
    scale: String,
    price: Option[Double]
  )
}
