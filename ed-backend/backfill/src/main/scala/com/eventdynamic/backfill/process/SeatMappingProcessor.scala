package com.eventdynamic.backfill.process

import com.eventdynamic.backfill.models.SeatMapping
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

object SeatMappingProcessor {

  /**
    * Cross joins all time intervals with seat mappings.  This sets the base data for the time interval.
    *
    * @param spark spark session to use
    * @param seatMappings the transactions to attach
    * @param timeIntervals the price intervals to attach transactions to
    * @return Dataset of PostSeatMap
    */
  def process(
    spark: SparkSession,
    seatMappings: Dataset[SeatMapping],
    timeIntervals: DataFrame
  ): Dataset[PostSeatMap] = {
    import spark.implicits._

    timeIntervals
      .crossJoin(seatMappings)
      .as[PostSeatMap]
  }
}
