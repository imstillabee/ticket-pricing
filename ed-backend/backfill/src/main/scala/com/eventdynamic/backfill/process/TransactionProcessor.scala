package com.eventdynamic.backfill.process

import java.sql.Timestamp

import com.eventdynamic.backfill.models.TransactionWithNext
import com.eventdynamic.backfill.utils.TimestampUtils
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

object TransactionProcessor {

  /**
    * Takes in the price intervals and attaches the transaction information at the seat level.
    *
    * @param spark spark session to use
    * @param transactions the transactions to attach
    * @param changeData the price intervals to attach transactions to
    * @return
    */
  def process(
    spark: SparkSession,
    transactions: Dataset[TransactionWithNext],
    changeData: Dataset[PostPriceChange],
    maxTimestamp: Timestamp
  ): Dataset[PostTransaction] = {
    import spark.implicits._

    // Encoder needed to map rows
    val pctEncoder = Encoders.product[TransactionTimestamp]

    // Expand the price changes from ranges to one for each 15 minutes
    val transactionsExpanded = transactions.flatMap(t => {
      // The ending timestamp for this sequence
      val targetTime =
        Seq(t.event_date.getTime, t.next_trans_timestamp.getTime, maxTimestamp.getTime).min

      // Generate the timestamps between the change and next change dates, map to new rows
      TimestampUtils
        .generateTimestampSeq15(t.trans_timestamp, new Timestamp(targetTime))
        .map(
          TransactionTimestamp(
            _,
            t.event_date,
            t.trans_price,
            t.trans_scale,
            t.section,
            t.row,
            t.seat
          )
        )
    })(pctEncoder)

    // Join the seat level pricing intervals with the transactions to get the latest transaction price
    // If there is no transaction yet, it will be null
    changeData
      .join(
        transactionsExpanded,
        transactionsExpanded("timestamp") === changeData("timestamp") &&
          transactionsExpanded("event_date") === changeData("event_date") &&
          transactionsExpanded("trans_scale") === changeData("scale") &&
          transactionsExpanded("section") === changeData("map_section") &&
          transactionsExpanded("row") === changeData("map_row") &&
          transactionsExpanded("seat") === changeData("map_seat"),
        "left"
      )
      .drop("trans_scale", "section", "row", "seat") // Get rid of transaction location columns
      .drop(transactionsExpanded("event_date"))
      .drop(transactionsExpanded("timestamp"))
      .withColumnRenamed("trans_price", "sold_price")
      .withColumnRenamed("map_section", "section") // Make the map_location columns the standard (bc left join)
      .withColumnRenamed("map_row", "row")
      .withColumnRenamed("map_seat", "seat")
      .as[PostTransaction]
  }

  /**
    * Converter for transaction data with range to transaction data for every 15 minutes
    *
    * @param timestamp
    * @param event_date
    * @param trans_price
    * @param trans_scale
    * @param section
    * @param row
    * @param seat
    */
  private case class TransactionTimestamp(
    timestamp: Timestamp,
    event_date: Timestamp,
    trans_price: Double,
    trans_scale: String,
    section: String,
    row: String,
    seat: String
  )
}
