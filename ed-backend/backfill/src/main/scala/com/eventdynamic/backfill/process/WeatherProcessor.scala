package com.eventdynamic.backfill.process

import com.eventdynamic.backfill.models.WeatherMapping
import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.functions._

object WeatherProcessor {

  val MILLIS_PER_WEEK: Long = 1000 * 60 * 60 * 24 * 7

  /**
    * Joins the weather data onto the observations.  If the timestamp is over a week away, then we use the average
    * temperature and assume clear conditions.  If we're within a week, use actual weather data.
    *
    * @param spark spark session to use
    * @param weatherMappings the weather mappings to attach
    * @param postTrans the observations to attach the mappings to
    * @return Dataset of PostSeatMap
    */
  def process(
    spark: SparkSession,
    weatherMappings: Dataset[WeatherMapping],
    postTrans: Dataset[PostTransaction]
  ): Dataset[PostWeather] = {
    import spark.implicits._

    postTrans
      .joinWith(
        broadcast(weatherMappings),
        postTrans("event_date") === weatherMappings("event_date")
      )
      .map(row => {
        // Map from the join object to what we need as a result
        val t = row._1
        val w = if (t.event_date.getTime - t.timestamp.getTime > MILLIS_PER_WEEK) {
          // Take average temp, assume clear weather
          Weather(row._2.average.temp, 1, 0, 0, 0, 0, 0, 0, 0, 0)
        } else {
          // Convert actual weather to normal weather (strip timestamp)
          val aw = row._2.actual
          Weather(
            aw.temp,
            aw.clear,
            aw.clouds,
            aw.clouds_heavy,
            aw.rain_light,
            aw.rain_medium,
            aw.rain_heavy,
            aw.snow_light,
            aw.snow_medium,
            aw.snow_heavy
          )
        }

        // Create new observation
        PostWeather(
          t.timestamp,
          t.event_date,
          t.scale,
          t.section,
          t.row,
          t.seat,
          t.listed_price,
          t.sold_price,
          w
        )
      })
  }
}
