package com.eventdynamic.backfill

import java.sql.Timestamp

import com.eventdynamic.backfill.models.{ActualWeather, AverageWeather}

package object process {

  /*
   * Intermediate Models
   *
   * These are models that define the structure of the observations between each processor step.  The different data
   * sources (price changes, transactions, weather, etc.) are incrementally added.  Each source adds on new columns
   * which are added to the next model.  The last intermediate model should be what the observation becomes.
   */
  case class PostSeatMap(
    timestamp: Timestamp,
    map_scale: String,
    map_section: String,
    map_row: String,
    map_seat: String
  )

  case class PostPriceChange(
    timestamp: Timestamp,
    event_date: Timestamp,
    scale: String,
    map_section: String,
    map_row: String,
    map_seat: String,
    listed_price: Option[Double]
  )

  case class PostTransaction(
    timestamp: Timestamp,
    event_date: Timestamp,
    scale: String,
    section: String,
    row: String,
    seat: String,
    listed_price: Option[Double],
    sold_price: Option[Double]
  )

  case class PostWeather(
    timestamp: Timestamp,
    event_date: Timestamp,
    scale: String,
    section: String,
    row: String,
    seat: String,
    listed_price: Option[Double],
    sold_price: Option[Double],
    weather: Weather
  )

  case class PostMLBStats(
    timestamp: Timestamp,
    event_date: Timestamp,
    scale: String,
    section: String,
    row: String,
    seat: String,
    listed_price: Option[Double],
    sold_price: Option[Double],
    weather: Weather,
    mlb_event: MLBEvent,
    mlb_rank: MLBRank
  )

  type Observation = PostMLBStats

  /*
   * Sub Models
   *
   * These encapsulate different categories of data to keep the intermediate models trim.
   */

  case class Weather(
    temp: Float,
    clear: Int,
    clouds: Int,
    clouds_heavy: Int,
    rain_light: Int,
    rain_medium: Int,
    rain_heavy: Int,
    snow_light: Int,
    snow_medium: Int,
    snow_heavy: Int
  )

  case class MLBEvent(game_num: Int, opponent: String, home_opener: Int, home_series_start: Int)

  case class MLBRank(rank: Int, streak: Int, wins: Int, losses: Int, games_ahead: Float)
}
