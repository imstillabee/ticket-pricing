package com.eventdynamic.backfill.utils

import java.sql.Timestamp
import scala.math.ceil

object TimestampUtils {

  val MILLIS_PER_DAY: Int = 1000 * 60 * 60 * 24
  val MILLIS_PER_15_MIN: Int = 1000 * 60 * 15

  /**
    * Rounds the timestamp up to the closest even 15 minute increment
    * @param t
    * @return
    */
  def ceilTimestamp15(t: Timestamp) = {
    new Timestamp(ceil(t.getTime.toDouble / MILLIS_PER_15_MIN).toLong * MILLIS_PER_15_MIN)
  }

  /**
    * Returns a sequence of timestamps from start to end (exclusive) every 15 minutes.  The timestamps are ceiled
    * to the closest 15 minutes.
    *
    * Eg. 12:10 -> 12:55 = (12:15, 12:30, 12:45)
    *
    * @param start
    * @param end
    * @return
    */
  def generateTimestampSeq15(start: Timestamp, end: Timestamp) = {
    (ceilTimestamp15(start).getTime until ceilTimestamp15(end).getTime by MILLIS_PER_15_MIN)
      .map(new Timestamp(_))
  }
}
