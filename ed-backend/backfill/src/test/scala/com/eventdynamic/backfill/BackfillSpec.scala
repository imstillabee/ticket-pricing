package com.eventdynamic.backfill

import java.sql.Timestamp

import com.eventdynamic.backfill.models.{PriceChange, Transaction, TransactionWithNext}
import com.eventdynamic.backfill.test.SharedSparkSession
import org.scalatest.{FlatSpec, Matchers}

class BackfillSpec extends FlatSpec with Matchers with SharedSparkSession {
  import spark.implicits._

  "Backfill" should "get min and max times from price changes" in {
    // Event times
    val event1 = Timestamp.valueOf("2018-03-01 12:00:00")
    val event2 = Timestamp.valueOf("2018-04-01 12:00:00")

    // Create a dataframe of price changes
    val priceChanges = Seq[PriceChange](
      PriceChange(
        event1,
        "scale1",
        Option(10),
        Timestamp.valueOf("2018-03-01 11:00:00"),
        Timestamp.valueOf("2018-03-01 11:35:00")
      ),
      PriceChange(event1, "scale1", Option(15), Timestamp.valueOf("2018-03-01 11:35:00"), event1),
      PriceChange(
        event1,
        "scale2",
        Option(30),
        Timestamp.valueOf("2018-03-01 10:35:00"),
        Timestamp.valueOf("2018-03-01 11:10:00")
      ),
      PriceChange(event1, "scale2", Option(25), Timestamp.valueOf("2018-03-01 11:10:00"), event1),
      PriceChange(event2, "scale1", Option(15), Timestamp.valueOf("2018-04-01 11:45:00"), event2),
      PriceChange(
        event2,
        "scale1",
        Option(20),
        Timestamp.valueOf("2018-04-01 11:30:00"),
        Timestamp.valueOf("2018-04-01 11:45:00")
      )
    ).toDS()

    val (minTime, maxTime) = Backfill.getMinMaxTimes(priceChanges)

    minTime should equal(Timestamp.valueOf("2018-03-01 00:00:00").getTime)
    maxTime should equal(event2.getTime)
  }

  it should "update transactions for the next batch" in {
    // Event times
    val event1 = Timestamp.valueOf("2018-03-01 12:00:00")
    val event2 = Timestamp.valueOf("2018-04-01 12:00:00")

    // Transactions, buying a ticket then returning it 25 minutes later
    val transactions = Seq[Transaction](
      Transaction(event1, 15, Timestamp.valueOf("2018-02-01 10:55:00"), "scale1", "100", "10", "1"),
      Transaction(
        event1,
        -15,
        Timestamp.valueOf("2018-03-01 11:20:00"),
        "scale1",
        "100",
        "10",
        "1"
      ),
      Transaction(event1, 10, Timestamp.valueOf("2018-03-01 11:10:00"), "scale1", "100", "10", "2"),
      Transaction(event2, 15, Timestamp.valueOf("2018-02-01 11:20:00"), "scale1", "100", "10", "1"),
      Transaction(event2, -15, Timestamp.valueOf("2018-03-01 7:20:00"), "scale1", "100", "10", "1"),
      Transaction(event2, 40, Timestamp.valueOf("2018-03-01 11:20:00"), "scale1", "100", "10", "1"),
      Transaction(event2, 40, Timestamp.valueOf("2018-03-01 11:20:00"), "scale1", "100", "10", "2")
    ).toDS()

    // Convert transactions to format, timestamp is start of next day
    val transWithNext =
      TransactionWithNext.fromTransactions(spark, transactions)
    val targetTimestamp = Timestamp.valueOf("2018-03-02 00:00:00")

    val updatedTransactions = Backfill
      .updateRecentTransactions(spark, transWithNext, targetTimestamp)
      .collect()
    updatedTransactions.length should equal(2)

    val seat1 = updatedTransactions.find(_.seat == "1").get
    seat1.trans_price should equal(40)

    val seat2 = updatedTransactions.find(_.seat == "2").get
    seat2.trans_price should equal(40)
  }

  it should "update price change for the next batch" in {
    // Event times
    val event1 = Timestamp.valueOf("2018-03-01 12:00:00")
    val event2 = Timestamp.valueOf("2018-04-01 12:00:00")

    // Create a dataframe of price changes
    val priceChanges = Seq[PriceChange](
      PriceChange(
        event1,
        "scale1",
        Option(10),
        Timestamp.valueOf("2018-03-01 11:00:00"),
        Timestamp.valueOf("2018-03-01 11:35:00")
      ),
      PriceChange(event1, "scale1", Option(15), Timestamp.valueOf("2018-03-01 11:35:00"), event1),
      PriceChange(event1, "scale2", Option(25), Timestamp.valueOf("2018-03-01 11:10:00"), event1),
      PriceChange(
        event2,
        "scale1",
        Option(20),
        Timestamp.valueOf("2018-02-01 11:30:00"),
        Timestamp.valueOf("2018-03-01 11:45:00")
      ),
      PriceChange(event2, "scale1", Option(15), Timestamp.valueOf("2018-03-01 11:45:00"), event2),
      PriceChange(event2, "scale2", Option(25), Timestamp.valueOf("2018-02-01 11:10:00"), event2)
    ).toDS()

    // Convert transactions to format, timestamp is start of next day
    val targetTimestamp = Timestamp.valueOf("2018-03-02 00:00:00")

    val updatedChanges = Backfill
      .updateRecentPriceChanges(spark, priceChanges, targetTimestamp)
      .collect()
    updatedChanges.length should equal(2)

    val scale1 = updatedChanges.find(_.scale == "scale1").get
    scale1.price.get should equal(15)

    val scale2 = updatedChanges.find(_.scale == "scale2").get
    scale2.price.get should equal(25)
  }
}
