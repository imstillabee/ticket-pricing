package com.eventdynamic.backfill.models

import org.scalatest.FlatSpec

class BackfillParamsSpec extends FlatSpec {
  "BackfillParams" should "validate by size" in {
    val params = Array[String]("local[*]", "/test/dir", "1")

    val bp = BackfillParams.validate(params)

    assert(bp.nonEmpty)
    assert(bp.get.master.isDefined)
    assert(bp.get.master.get === "local[*]")
    assert(bp.get.baseUrl === "/test/dir")
    assert(bp.get.daysPerBatch === 1)
  }

  it should "return none when invalid" in {
    val params = Array[String]("local[*]")

    assert(BackfillParams.validate(params).isEmpty)
  }

  it should "make master none when `pass` is passed in" in {
    val params = Array[String]("pass", "/testing", "1")

    val bp = BackfillParams.validate(params)

    assert(bp.isDefined)
    assert(bp.get.master.isEmpty)
  }
}
