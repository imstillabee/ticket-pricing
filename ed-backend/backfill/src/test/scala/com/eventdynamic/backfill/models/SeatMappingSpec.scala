package com.eventdynamic.backfill.models

import java.sql.Timestamp

import com.eventdynamic.backfill.test.SharedSparkSession
import org.scalatest.FlatSpec

class SeatMappingSpec extends FlatSpec with SharedSparkSession {
  import spark.implicits._

  "SeatMapping" should "create mappings from transactions" in {
    // Transactions for three different seats over two games
    val transactionSeq = Seq[Transaction](
      Transaction(
        Timestamp.valueOf("2018-03-01 12:00:00"),
        15,
        Timestamp.valueOf("2018-03-01 10:55:00"),
        "scale1",
        "100",
        "10",
        "1"
      ),
      Transaction(
        Timestamp.valueOf("2018-03-01 12:00:00"),
        -15,
        Timestamp.valueOf("2018-03-01 11:20:00"),
        "scale1",
        "100",
        "10",
        "1"
      ),
      Transaction(
        Timestamp.valueOf("2018-03-01 12:00:00"),
        10,
        Timestamp.valueOf("2018-03-01 11:10:00"),
        "scale1",
        "100",
        "10",
        "2"
      ),
      Transaction(
        Timestamp.valueOf("2018-04-01 12:00:00"),
        10,
        Timestamp.valueOf("2018-03-01 11:10:00"),
        "scale1",
        "100",
        "10",
        "2"
      ),
      Transaction(
        Timestamp.valueOf("2018-03-01 12:00:00"),
        10,
        Timestamp.valueOf("2018-03-01 11:10:00"),
        "scale2",
        "101",
        "10",
        "1"
      )
    )
    val transactions = spark.sparkContext.parallelize(transactionSeq).toDS()
    val transactionWithNext =
      TransactionWithNext.fromTransactions(spark, transactions)

    val seatMappings =
      SeatMapping.fromTransactions(spark, transactionWithNext).collect()

    // Should only have three unique seats
    assert(seatMappings.length === 3)
  }
}
