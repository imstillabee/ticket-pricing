package com.eventdynamic.backfill.models

import java.sql.Timestamp

import com.eventdynamic.backfill.test.SharedSparkSession
import org.scalatest.FlatSpec

class TransactionWithNextSpec extends FlatSpec with SharedSparkSession {
  import spark.implicits._

  "TransactionWithNext" should "add next from transactions" in {
    // Transactions for three different seats over two games
    val transactionSeq = Seq[Transaction](
      Transaction(
        Timestamp.valueOf("2018-03-01 12:00:00"),
        15,
        Timestamp.valueOf("2018-03-01 10:55:00"),
        "scale1",
        "100",
        "10",
        "1"
      ),
      Transaction(
        Timestamp.valueOf("2018-03-01 12:00:00"),
        -15,
        Timestamp.valueOf("2018-03-01 11:20:00"),
        "scale1",
        "100",
        "10",
        "1"
      ),
      Transaction(
        Timestamp.valueOf("2018-03-01 12:00:00"),
        10,
        Timestamp.valueOf("2018-03-01 11:10:00"),
        "scale1",
        "100",
        "10",
        "2"
      ),
      Transaction(
        Timestamp.valueOf("2018-04-01 12:00:00"),
        10,
        Timestamp.valueOf("2018-03-01 11:15:00"),
        "scale1",
        "100",
        "10",
        "2"
      ),
      Transaction(
        Timestamp.valueOf("2018-03-01 12:00:00"),
        10,
        Timestamp.valueOf("2018-03-01 11:10:00"),
        "scale2",
        "101",
        "10",
        "1"
      )
    )
    val transactions = spark.sparkContext.parallelize(transactionSeq).toDS()

    val transWithNext =
      TransactionWithNext.fromTransactions(spark, transactions).collect()

    assert(transWithNext.length === 5)

    val trans1 = transWithNext.find(_.trans_price === 15).get
    assert(trans1.next_trans_timestamp === Timestamp.valueOf("2018-03-01 11:20:00"))

    val trans2 = transWithNext.find(_.trans_price === -15).get
    assert(trans2.next_trans_timestamp === TransactionWithNext.FUTURE_DATE)

    val trans3 = transWithNext
      .find(
        t =>
          t.event_date === Timestamp
            .valueOf("2018-03-01 12:00:00") && t.seat === "2"
      )
      .get
    assert(trans3.next_trans_timestamp === TransactionWithNext.FUTURE_DATE)
  }
}
