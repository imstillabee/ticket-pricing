package com.eventdynamic.backfill.models

import java.sql.Timestamp

import com.eventdynamic.backfill.test.SharedSparkSession
import org.scalatest.{FlatSpec, Matchers}

class WeatherMappingSpec extends FlatSpec with SharedSparkSession with Matchers {
  import spark.implicits._

  "WeatherMapping" should "find weather for each event" in {
    // Event times
    val event1 = Timestamp.valueOf("2018-03-01 12:30:00")
    val event2 = Timestamp.valueOf("2018-04-01 12:25:00")
    val event3 = Timestamp.valueOf("2018-05-01 12:35:00")
    val event4 = Timestamp.valueOf("2018-06-01 12:00:00")

    // Create a dataframe of price changes
    val priceChanges = Seq[PriceChange](
      PriceChange(
        event1,
        "scale1",
        Option(5),
        Timestamp.valueOf("2018-03-01 10:00:00"),
        Timestamp.valueOf("2018-03-01 11:00:00")
      ),
      PriceChange(
        event1,
        "scale1",
        Option(10),
        Timestamp.valueOf("2018-03-01 11:00:00"),
        Timestamp.valueOf("2018-03-01 11:35:00")
      ),
      PriceChange(event1, "scale1", Option(15), Timestamp.valueOf("2018-03-01 11:35:00"), event1),
      PriceChange(event2, "scale2", Option(25), Timestamp.valueOf("2018-03-01 11:10:00"), event2),
      PriceChange(
        event3,
        "scale1",
        Option(20),
        Timestamp.valueOf("2018-02-01 11:30:00"),
        Timestamp.valueOf("2018-03-01 11:45:00")
      ),
      PriceChange(event3, "scale1", Option(15), Timestamp.valueOf("2018-03-01 11:45:00"), event3),
      PriceChange(event4, "scale2", Option(25), Timestamp.valueOf("2018-02-01 11:10:00"), event4)
    ).toDS()

    // Create some weather averages to test edge cases
    val averages = Seq[AverageWeather](
      AverageWeather(3, 1, 12, 52f),
      AverageWeather(3, 1, 13, 53f),
      AverageWeather(4, 1, 12, 52f),
      AverageWeather(4, 1, 13, 53f),
      AverageWeather(5, 1, 12, 52f),
      AverageWeather(5, 1, 13, 53f),
      AverageWeather(6, 1, 11, 51f),
      AverageWeather(6, 1, 12, 52f),
      AverageWeather(6, 1, 13, 53f)
    ).toDS()

    // Create some weather actuals to test edge cases
    val actuals = Seq[ActualWeather](
      ActualWeather(Timestamp.valueOf("2018-03-01 12:00:00"), 50f, 1, 0, 0, 0, 0, 0, 0, 0, 0),
      ActualWeather(Timestamp.valueOf("2018-03-01 12:35:00"), 55f, 1, 0, 0, 0, 0, 0, 0, 0, 0),
      ActualWeather(Timestamp.valueOf("2018-04-01 12:00:00"), 52f, 1, 0, 0, 0, 0, 0, 0, 0, 0),
      ActualWeather(Timestamp.valueOf("2018-04-01 13:00:00"), 53f, 1, 0, 0, 0, 0, 0, 0, 0, 0),
      ActualWeather(Timestamp.valueOf("2018-05-01 12:30:00"), 53f, 1, 0, 0, 0, 0, 0, 0, 0, 0),
      ActualWeather(Timestamp.valueOf("2018-05-01 12:40:00"), 54f, 1, 0, 0, 0, 0, 0, 0, 0, 0),
      ActualWeather(Timestamp.valueOf("2018-06-01 11:59:00"), 49f, 1, 0, 0, 0, 0, 0, 0, 0, 0),
      ActualWeather(Timestamp.valueOf("2018-06-01 12:00:00"), 50f, 1, 0, 0, 0, 0, 0, 0, 0, 0),
      ActualWeather(Timestamp.valueOf("2018-06-01 12:02:00"), 51f, 1, 0, 0, 0, 0, 0, 0, 0, 0)
    ).toDS()

    // Combine weather data
    val weathers = WeatherMapping
      .combineWeatherData(spark, priceChanges, averages, actuals)
      .collect()

    // Debugging
    //weathers.foreach(println)

    // Should only have 4 events
    weathers.length should equal(4)

    // Check each event
    val w1 = weathers.find(_.event_date == event1).get
    w1.actual.temp should be(55f)
    w1.average.temp should be(53f)

    val w2 = weathers.find(_.event_date == event2).get
    w2.actual.temp should be(52f)
    w2.average.temp should be(52f)

    val w3 = weathers.find(_.event_date == event3).get
    w3.actual.temp should be(54f)
    w3.average.temp should be(53f)

    val w4 = weathers.find(_.event_date == event4).get
    w4.actual.temp should be(50f)
    w4.average.temp should be(52f)
  }
}
