package com.eventdynamic.backfill.process

import java.sql.Timestamp

import com.eventdynamic.backfill.models.MLBStats
import com.eventdynamic.backfill.test.SharedSparkSession
import org.scalatest.{FlatSpec, Matchers}

class MLBStatsProcessorSpec extends FlatSpec with Matchers with SharedSparkSession {
  import spark.implicits._

  val weather = Weather(0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

  "MLBStatsProcessor" should "add MLBStats to observations" in {
    // Event times
    val event1 = Timestamp.valueOf("2018-02-14 12:30:00")
    val event2 = Timestamp.valueOf("2018-04-14 12:30:00")

    val postWeather = Seq[PostWeather](
      PostWeather(
        Timestamp.valueOf("2018-02-14 12:15:00"),
        event1,
        "a",
        "",
        "",
        "",
        None,
        None,
        weather
      ),
      PostWeather(
        Timestamp.valueOf("2018-02-14 12:30:00"),
        event1,
        "b",
        "",
        "",
        "",
        None,
        None,
        weather
      ),
      PostWeather(
        Timestamp.valueOf("2018-02-14 13:00:00"),
        event2,
        "c",
        "",
        "",
        "",
        None,
        None,
        weather
      ),
      PostWeather(
        Timestamp.valueOf("2018-02-14 14:00:00"),
        event2,
        "d",
        "",
        "",
        "",
        None,
        None,
        weather
      ),
      PostWeather(
        Timestamp.valueOf("2018-04-14 12:30:00"),
        event2,
        "e",
        "",
        "",
        "",
        None,
        None,
        weather
      ),
      PostWeather(
        Timestamp.valueOf("2018-04-01 12:00:00"),
        event2,
        "f",
        "",
        "",
        "",
        None,
        None,
        weather
      )
    ).toDS()

    val mlbSeq = Seq[MLBStats](
      MLBStats(1, "NYY", 1, 1, 1, 0, 60, 1f, event1, 1, 1),
      MLBStats(2, "NYY", 1, 2, 2, 0, 60, 2f, event2, 0, 0)
    )
    val mlbRanks = MLBStats.createRankMap(mlbSeq)
    val mlbStats = mlbSeq.toDS()

    val postMlb = MLBStatsProcessor.process(spark, mlbStats, mlbRanks, postWeather).collect()

    //Debugging
    //postMlb.foreach(println)
    //mlbRanks.foreach(println)

    // Check that the columns are correct
    postMlb.length should equal(6)

    // Assert that all events have correct mlb event
    assert(postMlb.filter(_.event_date == event1).forall(_.mlb_event.game_num == 1))
    assert(postMlb.filter(_.event_date == event2).forall(_.mlb_event.game_num == 2))

    // Spot check ranking info
    assert(postMlb.find(_.scale == "a").get.mlb_rank.wins === 0)
    assert(postMlb.find(_.scale == "b").get.mlb_rank.wins === 0)
    assert(postMlb.find(_.scale == "c").get.mlb_rank.wins === 0)
    assert(postMlb.find(_.scale == "d").get.mlb_rank.wins === 1)
    assert(postMlb.find(_.scale == "e").get.mlb_rank.wins === 1)
    assert(postMlb.find(_.scale == "f").get.mlb_rank.wins === 1)
  }
}
