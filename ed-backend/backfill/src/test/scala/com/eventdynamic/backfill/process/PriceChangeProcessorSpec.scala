package com.eventdynamic.backfill.process

import java.sql.Timestamp
import com.eventdynamic.backfill.models.{PriceChange, Transaction}
import com.eventdynamic.backfill.test.SharedSparkSession
import org.scalatest.{FlatSpec, Matchers}

class PriceChangeProcessorSpec extends FlatSpec with Matchers with SharedSparkSession {
  import spark.implicits._

  "PriceChangeProcessor" should "attached price changes" in {
    // Event times
    val event1 = Timestamp.valueOf("2018-03-01 12:00:00")
    val event2 = Timestamp.valueOf("2018-04-01 12:00:00")

    // Create a dataframe of price changes
    val priceChanges = Seq[PriceChange](
      PriceChange(
        event1,
        "scale1",
        Option(5),
        Timestamp.valueOf("2018-03-01 10:00:00"),
        Timestamp.valueOf("2018-03-01 11:00:00")
      ),
      PriceChange(
        event1,
        "scale1",
        Option(10),
        Timestamp.valueOf("2018-03-01 11:00:00"),
        Timestamp.valueOf("2018-03-01 11:35:00")
      ),
      PriceChange(event1, "scale1", Option(15), Timestamp.valueOf("2018-03-01 11:35:00"), event1),
      PriceChange(event1, "scale2", Option(25), Timestamp.valueOf("2018-03-01 11:10:00"), event1),
      PriceChange(
        event2,
        "scale1",
        Option(20),
        Timestamp.valueOf("2018-02-01 11:30:00"),
        Timestamp.valueOf("2018-03-01 11:45:00")
      ),
      PriceChange(event2, "scale1", Option(15), Timestamp.valueOf("2018-03-01 11:45:00"), event2),
      PriceChange(event2, "scale2", Option(25), Timestamp.valueOf("2018-02-01 11:10:00"), event2)
    ).toDS()

    val mapData = Seq(
      PostSeatMap(Timestamp.valueOf("2018-03-01 10:30:00"), "scale1", "100", "1", "1"),
      PostSeatMap(Timestamp.valueOf("2018-03-01 10:30:00"), "scale1", "200", "2", "2"),
      PostSeatMap(Timestamp.valueOf("2018-03-01 11:00:00"), "scale1", "100", "1", "1"),
      PostSeatMap(Timestamp.valueOf("2018-03-01 11:30:00"), "scale1", "100", "1", "1"),
      PostSeatMap(Timestamp.valueOf("2018-03-01 11:45:00"), "scale1", "100", "1", "1"),
      PostSeatMap(Timestamp.valueOf("2018-03-01 13:00:00"), "scale1", "100", "1", "1"),
      PostSeatMap(Timestamp.valueOf("2018-03-01 11:00:00"), "scale2", "100", "1", "1")
    ).toDS()

    // Generate intervals and bring them back to the master
    val post = PriceChangeProcessor
      .process(spark, priceChanges, mapData, Timestamp.valueOf("2018-03-01 24:00:00"))
      .collect()

    // Debugging help
    //post.foreach(println)

    // Check counts
    post.length should equal(12)
    post.count(_.map_seat == "2") should equal(2)
    post.count(_.event_date == event2) should equal(7)
    post.count(ppc => ppc.event_date.getTime < ppc.timestamp.getTime) should equal(0)
    post.count(_.scale == "scale2") should equal(1)

    // Spot checks
    val ppc1 = post
      .find(
        ppc =>
          ppc.event_date == event1 &&
            ppc.scale == "scale1" &&
            ppc.timestamp == Timestamp.valueOf("2018-03-01 10:30:00")
      )
      .get
    ppc1.listed_price should equal(Option(5))

    val ppc2 = post
      .find(
        ppc =>
          ppc.event_date == event1 &&
            ppc.scale == "scale1" &&
            ppc.timestamp == Timestamp.valueOf("2018-03-01 11:00:00")
      )
      .get
    ppc2.listed_price should equal(Option(10))

    val ppc3 = post
      .find(
        ppc =>
          ppc.event_date == event1 &&
            ppc.scale == "scale1" &&
            ppc.timestamp == Timestamp.valueOf("2018-03-01 11:30:00")
      )
      .get
    ppc3.listed_price should equal(Option(10))
  }
}
