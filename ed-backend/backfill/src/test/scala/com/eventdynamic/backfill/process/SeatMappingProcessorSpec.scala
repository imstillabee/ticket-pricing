package com.eventdynamic.backfill.process

import java.sql.Timestamp

import com.eventdynamic.backfill.models.SeatMapping
import com.eventdynamic.backfill.test.SharedSparkSession
import org.scalatest.{FlatSpec, Matchers}

class SeatMappingProcessorSpec extends FlatSpec with Matchers with SharedSparkSession {
  import spark.implicits._

  "SeatMappingProcessor" should "merge time intervals and seat mappings" in {
    val seatMapping = Seq(
      SeatMapping("scale1", "100", "1", "1"),
      SeatMapping("scale1", "100", "1", "2"),
      SeatMapping("scale2", "200", "1", "1"),
      SeatMapping("scale2", "200", "1", "2")
    ).toDS()

    val startDate = Timestamp.valueOf("2018-03-01 12:00:00")
    val endDate = Timestamp.valueOf("2018-03-01 13:00:00")
    val timeIntervals =
      (startDate.getTime until endDate.getTime by 900000).toDF("timestamp")

    val seatData =
      SeatMappingProcessor.process(spark, seatMapping, timeIntervals)

    // Check that the columns are correct
    val columns = seatData.columns.toSeq.sortWith(_ < _)
    val expectedColumns =
      Seq("map_row", "map_scale", "map_seat", "map_section", "timestamp")
        .sortWith(_ < _)
    columns should equal(expectedColumns)

    // Check length
    seatData.count should equal(16)
  }
}
