package com.eventdynamic.backfill.process

import java.sql.Timestamp
import com.eventdynamic.backfill.models.{Transaction, TransactionWithNext}
import com.eventdynamic.backfill.test.SharedSparkSession
import org.scalatest.{FlatSpec, Matchers}

class TransactionProcessorSpec extends FlatSpec with Matchers with SharedSparkSession {
  import spark.implicits._

  "TransactionProcessor" should "attached transactions" in {
    // Event times
    val event1 = Timestamp.valueOf("2018-03-01 12:00:00")
    val event2 = Timestamp.valueOf("2018-04-01 12:00:00")

    // Transactions, buying a ticket then returning it 25 minutes later
    val transactions = Seq[Transaction](
      Transaction(event1, 15, Timestamp.valueOf("2018-02-01 10:55:00"), "scale1", "100", "1", "1"),
      Transaction(event1, -15, Timestamp.valueOf("2018-03-01 11:20:00"), "scale1", "100", "1", "1"),
      Transaction(event1, 10, Timestamp.valueOf("2018-03-01 11:00:00"), "scale1", "100", "2", "2"),
      Transaction(event2, 15, Timestamp.valueOf("2018-02-01 11:20:00"), "scale1", "100", "1", "1"),
      Transaction(event2, -15, Timestamp.valueOf("2018-03-01 7:20:00"), "scale1", "100", "1", "1"),
      Transaction(event2, 40, Timestamp.valueOf("2018-03-01 11:20:00"), "scale1", "100", "1", "1")
    ).toDS()

    // Convert transactions to format, timestamp is start of next day
    val transWithNext = TransactionWithNext.fromTransactions(spark, transactions)

    val pcData = Seq(
      PostPriceChange(
        Timestamp.valueOf("2018-03-01 10:30:00"),
        event1,
        "scale1",
        "100",
        "1",
        "1",
        Option(10)
      ),
      PostPriceChange(
        Timestamp.valueOf("2018-03-01 11:00:00"),
        event1,
        "scale1",
        "100",
        "2",
        "2",
        Option(10)
      ),
      PostPriceChange(
        Timestamp.valueOf("2018-03-01 11:00:00"),
        event1,
        "scale1",
        "100",
        "1",
        "1",
        Option(10)
      ),
      PostPriceChange(
        Timestamp.valueOf("2018-03-01 11:30:00"),
        event1,
        "scale1",
        "100",
        "1",
        "1",
        Option(10)
      ),
      PostPriceChange(
        Timestamp.valueOf("2018-03-01 11:45:00"),
        event1,
        "scale1",
        "100",
        "1",
        "1",
        Option(10)
      ),
      PostPriceChange(
        Timestamp.valueOf("2018-03-01 13:00:00"),
        event1,
        "scale1",
        "100",
        "1",
        "3",
        Option(10)
      ),
      PostPriceChange(
        Timestamp.valueOf("2018-03-01 11:45:00"),
        event2,
        "scale1",
        "100",
        "1",
        "1",
        Option(10)
      )
    ).toDS()

    // Generate intervals and bring them back to the master
    val post = TransactionProcessor
      .process(spark, transWithNext, pcData, Timestamp.valueOf("2018-03-01 24:00:00"))
      .collect()

    // Debugging help
    //post.foreach(println)

    // Check counts
    post.length should equal(7)

    // Spot checks
    val pt1 = post
      .find(
        pt =>
          pt.event_date == event1 &&
            pt.seat == "2"
      )
      .get
    pt1.sold_price should equal(Option(10))

    val pt3 = post
      .find(
        pt =>
          pt.event_date == event1 &&
            pt.seat == "1" &&
            pt.timestamp == Timestamp.valueOf("2018-03-01 11:00:00")
      )
      .get
    pt3.sold_price should equal(Option(15))

    val pt4 = post
      .find(
        pt =>
          pt.event_date == event1 &&
            pt.seat == "1" &&
            pt.timestamp == Timestamp.valueOf("2018-03-01 11:30:00")
      )
      .get
    pt4.sold_price should equal(Option(-15))

    val pt2 = post
      .find(
        pt =>
          pt.event_date == event1 &&
            pt.seat == "1" &&
            pt.timestamp == Timestamp.valueOf("2018-03-01 11:45:00")
      )
      .get
    pt2.sold_price should equal(Option(-15))

    val pt5 = post
      .find(
        pt =>
          pt.event_date == event1 &&
            pt.seat == "3"
      )
      .get
    pt5.sold_price should equal(None)
    pt5.seat should equal("3")

    val pt6 = post
      .find(
        pt =>
          pt.event_date == event2 &&
            pt.seat == "1" &&
            pt.timestamp == Timestamp.valueOf("2018-03-01 11:45:00")
      )
      .get
    pt6.sold_price should equal(Option(40))
  }
}
