package com.eventdynamic.backfill.process

import java.sql.Timestamp

import com.eventdynamic.backfill.models.{ActualWeather, AverageWeather, SeatMapping, WeatherMapping}
import com.eventdynamic.backfill.test.SharedSparkSession
import org.scalatest.{FlatSpec, Matchers}

class WeatherProcessorSpec extends FlatSpec with Matchers with SharedSparkSession {
  import spark.implicits._

  "WeatherProcessor" should "add weather to observations" in {
    // Event times
    val event1 = Timestamp.valueOf("2018-02-14 12:30:00")
    val event2 = Timestamp.valueOf("2018-04-14 12:30:00")

    val postTrans = Seq[PostTransaction](
      PostTransaction(
        Timestamp.valueOf("2018-02-14 12:15:00"),
        event1,
        "a",
        "",
        "",
        "",
        None,
        None
      ),
      PostTransaction(
        Timestamp.valueOf("2018-02-07 12:30:00"),
        event1,
        "b",
        "",
        "",
        "",
        None,
        None
      ),
      PostTransaction(
        Timestamp.valueOf("2018-02-07 12:15:00"),
        event1,
        "c",
        "",
        "",
        "",
        None,
        None
      ),
      PostTransaction(
        Timestamp.valueOf("2018-04-14 12:30:00"),
        event2,
        "d",
        "",
        "",
        "",
        None,
        None
      ),
      PostTransaction(Timestamp.valueOf("2018-04-01 12:00:00"), event2, "e", "", "", "", None, None)
    ).toDS()

    // Base weather models to build off of, times don't matter at this point
    val baseAverage = AverageWeather(0, 0, 0, 55f)
    val baseActual =
      ActualWeather(new Timestamp(0), 50f, 0, 1, 0, 0, 0, 0, 0, 0, 0)

    val weatherMappings = Seq[WeatherMapping](
      WeatherMapping(event1, baseAverage, baseActual),
      WeatherMapping(event2, baseAverage, baseActual)
    ).toDS()

    val weathers =
      WeatherProcessor.process(spark, weatherMappings, postTrans).collect()

    //Debugging
    //weathers.foreach(println)

    // Check that the columns are correct
    weathers.length should equal(5)

    // Spot check
    val w1 = weathers.find(_.scale == "a").get
    w1.weather.temp should equal(50)
    w1.weather.clear should equal(0)
    w1.weather.clouds should equal(1)

    val w2 = weathers.find(_.scale == "b").get
    w2.weather.temp should equal(50)
    w2.weather.clear should equal(0)
    w2.weather.clouds should equal(1)

    val w3 = weathers.find(_.scale == "c").get
    w3.weather.temp should equal(55)
    w3.weather.clear should equal(1)
    w3.weather.clouds should equal(0)

    val w4 = weathers.find(_.scale == "d").get
    w4.weather.temp should equal(50)
    w4.weather.clear should equal(0)
    w4.weather.clouds should equal(1)

    val w5 = weathers.find(_.scale == "e").get
    w5.weather.temp should equal(55)
    w5.weather.clear should equal(1)
    w5.weather.clouds should equal(0)
  }
}
