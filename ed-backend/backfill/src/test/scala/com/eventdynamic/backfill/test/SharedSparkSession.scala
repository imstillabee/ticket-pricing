package com.eventdynamic.backfill.test

import org.apache.spark.sql.SparkSession

trait SharedSparkSession {
  lazy val spark: SparkSession = {
    val s = SparkSession
      .builder()
      .master("local")
      .appName("spark session")
      .config("spark.sql.shuffle.partitions", "16") // running locally, no need to partition much
      .getOrCreate()

    // Clean up logs
    s.sparkContext.setLogLevel("ERROR")

    s
  }
}
