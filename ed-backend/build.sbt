import sbt.Keys.resolvers

// Versions
val postgresqlVersion = "42.2.4"
val silhouetteVersion = "6.0.0-RC1"
val slickVersion = "3.3.0"
val sparkVersion = "2.3.0"
val akkaVersion = "2.5.23"
val akkaHttpVersion = "10.1.8"

lazy val commonSettings = Seq(
  organization := "dialexa",
  scalaVersion := "2.12.9",
  version := "1",
  // Increase JVM memory for spark jobs
  javaOptions ++= Seq(
    "-Xms512M",
    "-Xmx2048M",
    "-XX:+CMSClassUnloadingEnabled",
    "-Dlogback.configurationFile=logback.xml",
    "-Duser.timezone=America/New_York"
  ),
  scalacOptions ++= Seq("-deprecation", "-feature"),
  // Testing configs
  parallelExecution in Test := false,
  testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-u", "test-result/junit"),
  // Assembly configs
  test in assembly := {},
  assemblyMergeStrategy in assembly := {
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case "logback-test.xml"            => MergeStrategy.discard
    case PathList("reference.conf")    => MergeStrategy.concat
    case _                             => MergeStrategy.first
  },
  scalafmtOnCompile := true
)

// Dependencies for all projects
lazy val commonDependencies = Seq(
  guice,
  "org.wvlet.airframe" %% "airframe-log" % "0.50",
  "net.codingwell" %% "scala-guice" % "4.1.0",
  "org.flywaydb" % "flyway-core" % "5.0.7",
  "org.scalatest" %% "scalatest" % "3.0.7" % Test
)

// Dependencies for logging
lazy val loggingDependencies = Seq(
  "org.slf4j" % "slf4j-api" % "1.7.25",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "net.logstash.logback" % "logstash-logback-encoder" % "5.3",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.9.8",
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.9.8",
  "com.typesafe.play" %% "play-logback" % "2.7.0"
)

// Root project, aggregate all projects
lazy val root = (project in file("."))
  .settings(scalaVersion := "2.12.8")
  .aggregate(
    api,
    ticketPricingBatch,
    services,
    backfill,
    onboard,
    ticketFulfillmentPipeline,
    inventorySync,
    ticketSync,
    externalServices,
    tdcProxy,
    projectionSync,
    featurePoller
  )

// Api project
lazy val api = (project in file("api"))
  .enablePlugins(PlayScala)
  .disablePlugins(RevolverPlugin)
  .settings(
    commonSettings,
    name := "ed-api",
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= Seq(
      specs2 % Test,
      "org.webjars" % "swagger-ui" % "3.13.0",
      "io.swagger" %% "swagger-play2" % "1.6.0",
      "com.typesafe.play" %% "play-json" % "2.7.2",
      "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.2" % Test,
      "com.mohiva" %% "play-silhouette" % silhouetteVersion,
      "com.mohiva" %% "play-silhouette-password-bcrypt" % silhouetteVersion,
      "com.mohiva" %% "play-silhouette-crypto-jca" % silhouetteVersion,
      "com.mohiva" %% "play-silhouette-persistence" % silhouetteVersion,
      "com.mohiva" %% "play-silhouette-testkit" % silhouetteVersion % Test,
      "com.github.tototoshi" %% "scala-csv" % "1.3.5",
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test
    ),
    coverageExcludedPackages := """com.eventdynamic.api.controllers.*Reverse.*;controllers\..*Reverse.*;router.Routes.*;""",
    resolvers += "Atlassian Releases" at "https://maven.atlassian.com/public/"
  )
  .aggregate(services, externalServices)
  .dependsOn(services, externalServices)

// Database services project
lazy val services = (project in file("services"))
  .disablePlugins(RevolverPlugin)
  .settings(
    commonSettings,
    name := "ed-services",
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= loggingDependencies,
    libraryDependencies ++= Seq(
      "com.typesafe.slick" %% "slick" % slickVersion,
      "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
      "com.typesafe.slick" %% "slick-testkit" % slickVersion % Test,
      "com.github.tminglei" %% "slick-pg" % "0.17.2",
      "org.postgresql" % "postgresql" % postgresqlVersion,
      "org.mindrot" % "jbcrypt" % "0.3m",
      "com.mohiva" %% "play-silhouette" % silhouetteVersion,
      "com.amazonaws" % "aws-java-sdk-ses" % "1.11.534",
      "org.mockito" % "mockito-core" % "2.27.0" % Test
    ),
    resolvers += "Atlassian Releases" at "https://maven.atlassian.com/public/"
  )

// External services project
lazy val externalServices = (project in file("external-services"))
  .disablePlugins(RevolverPlugin)
  .settings(
    commonSettings,
    name := "ed-external-services",
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= loggingDependencies,
    libraryDependencies ++= Seq(
      "com.softwaremill.sttp" %% "core" % "1.1.14",
      "com.softwaremill.sttp" %% "okhttp-backend" % "1.1.14",
      "org.mockito" % "mockito-all" % "1.10.19" % Test
    )
  )
  .aggregate(services)
  .dependsOn(services)

// Backfill project
lazy val backfill = (project in file("backfill"))
  .disablePlugins(RevolverPlugin)
  .settings(
    commonSettings,
    scalaVersion := "2.11.12",
    name := "ed-backfill",
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= loggingDependencies,
    libraryDependencies ++= Seq(
      "org.apache.spark" %% "spark-core" % sparkVersion,
      "org.apache.spark" %% "spark-sql" % sparkVersion,
      "org.apache.spark" %% "spark-hive" % sparkVersion
    ),
    // Needed for spark config
    fork in Test := true,
    // Do not include Scala in the assembled JAR
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
  )

// Ticket Pricing script
lazy val ticketPricingBatch = (project in file("ticket-pricing-batch"))
  .disablePlugins(RevolverPlugin)
  .settings(
    commonSettings,
    name := "ed-ticket-pricing",
    mainClass in (Compile, run) := Some("com.eventdynamic.batch.ticketpricing.TicketPricingBatch"),
    mainClass in assembly := Some("com.eventdynamic.batch.ticketpricing.TicketPricingBatch"),
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= loggingDependencies,
    libraryDependencies ++= Seq(
      specs2 % Test,
      "com.typesafe.slick" %% "slick" % slickVersion,
      "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
      "org.postgresql" % "postgresql" % postgresqlVersion,
      "com.github.tototoshi" %% "scala-csv" % "1.3.5",
      "com.jsuereth" %% "scala-arm" % "2.0",
      "com.github.seratch" %% "awscala" % "0.6.+",
      "org.scalaz" %% "scalaz-core" % "7.2.23",
      "com.softwaremill.sttp" %% "core" % "1.1.14",
      "com.softwaremill.sttp" %% "okhttp-backend" % "1.1.14",
      "com.typesafe.play" %% "play-json" % "2.7.2",
      "org.jpmml" % "pmml-model" % "1.4.2",
      "org.jpmml" % "pmml-evaluator" % "1.4.1",
      "org.jpmml" % "pmml-evaluator-extension" % "1.4.1"
    )
  )
  .aggregate(services, externalServices)
  .dependsOn(services, externalServices)

// Ticket Sync script
lazy val ticketSync = (project in file("ticket-sync"))
  .settings(
    commonSettings,
    name := "ed-ticket-sync",
    // sbt-assembly settings for compiling JAR file
    mainClass in (Compile, run) := Some("com.eventdynamic.batch.ticket.sync.TicketSync"),
    mainClass in assembly := Some("com.eventdynamic.batch.ticket.sync.TicketSync"),
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= loggingDependencies,
    libraryDependencies ++= Seq(
      "com.typesafe.slick" %% "slick" % slickVersion,
      "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
      "com.jsuereth" %% "scala-arm" % "2.0",
      "org.mockito" % "mockito-all" % "1.10.19" % Test
    )
  )
  .aggregate(services, externalServices)
  .dependsOn(services, externalServices)

// Onboarding script
lazy val onboard = (project in file("onboard"))
  .disablePlugins(RevolverPlugin)
  .settings(
    commonSettings,
    name := "ed-onboard",
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= loggingDependencies,
    libraryDependencies ++= Seq(
      "com.typesafe.play" %% "play-json" % "2.7.2",
      "com.github.tototoshi" %% "scala-csv" % "1.3.5",
      "com.nrinaudo" %% "kantan.csv" % "0.4.0",
      "com.nrinaudo" %% "kantan.csv-generic" % "0.4.0",
      "org.mockito" % "mockito-all" % "1.10.19" % Test
    ),
    resolvers += "Atlassian Releases" at "https://maven.atlassian.com/public/"
  )
  .aggregate(services)
  .dependsOn(services)

// Ticket Fulfillment Pipeline
lazy val ticketFulfillmentPipeline = (project in file("ticket-fulfillment"))
  .settings(
    commonSettings,
    name := "ed-ticket-fulfillment",
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= loggingDependencies,
    libraryDependencies ++= Seq(
      specs2 % Test,
      "com.typesafe.slick" %% "slick" % slickVersion,
      "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
      "org.postgresql" % "postgresql" % postgresqlVersion,
      "com.jsuereth" %% "scala-arm" % "2.0",
      "com.softwaremill.sttp" %% "core" % "1.1.14",
      "com.softwaremill.sttp" %% "okhttp-backend" % "1.1.14",
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion
    ),
    resolvers += "Atlassian Releases" at "https://maven.atlassian.com/public/"
  )
  .aggregate(services, externalServices)
  .dependsOn(services, externalServices)

// Inventory Sync script
lazy val inventorySync = (project in file("inventory-sync"))
  .disablePlugins(RevolverPlugin)
  .settings(
    commonSettings,
    name := "ed-inventory-sync",
    mainClass in (Compile, run) := Some("com.eventdynamic.inventorysync.InventorySync"),
    mainClass in assembly := Some("com.eventdynamic.inventorysync.InventorySync"),
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= loggingDependencies,
    libraryDependencies ++= Seq(
      specs2 % Test,
      "com.typesafe.slick" %% "slick" % slickVersion,
      "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
      "org.postgresql" % "postgresql" % postgresqlVersion,
      "com.jsuereth" %% "scala-arm" % "2.0",
      "com.softwaremill.sttp" %% "core" % "1.1.14",
      "com.softwaremill.sttp" %% "okhttp-backend" % "1.1.14",
      "com.typesafe.play" %% "play-json" % "2.7.2"
    ),
    resolvers += "Atlassian Releases" at "https://maven.atlassian.com/public/"
  )
  .aggregate(services, externalServices)
  .dependsOn(services, externalServices)

// TDC Proxy Service
lazy val tdcProxy = (project in file("tdc-proxy"))
  .enablePlugins(PlayScala)
  .disablePlugins(RevolverPlugin)
  .settings(
    commonSettings,
    name := "tdc-proxy",
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= loggingDependencies,
    libraryDependencies ++= Seq(
      caffeine,
      "org.webjars" % "swagger-ui" % "3.13.0",
      "io.swagger" %% "swagger-play2" % "1.6.0",
      "com.typesafe.play" %% "play-json" % "2.7.2",
      "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.2" % Test,
      "com.typesafe.slick" %% "slick" % slickVersion,
      "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
      "org.mockito" % "mockito-all" % "1.10.19" // Used for fake data
    )
  )

// Projection Sync script
lazy val projectionSync = (project in file("projection-sync"))
  .disablePlugins(RevolverPlugin)
  .settings(
    commonSettings,
    name := "projection-sync",
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= loggingDependencies,
    libraryDependencies ++= Seq("org.mockito" % "mockito-all" % "1.10.19" % Test)
  )
  .aggregate(services, externalServices)
  .dependsOn(services, externalServices)

// Feature Poller
lazy val featurePoller = (project in file("feature-poller"))
  .disablePlugins(RevolverPlugin)
  .settings(
    commonSettings,
    name := "ed-feature-poller",
    libraryDependencies ++= commonDependencies,
    libraryDependencies ++= loggingDependencies,
    libraryDependencies ++= Seq(
      specs2 % Test,
      "com.typesafe.slick" %% "slick" % slickVersion,
      "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
      "org.postgresql" % "postgresql" % postgresqlVersion,
      "org.scalaz" %% "scalaz-core" % "7.2.23",
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test
    )
  )
  .aggregate(services, externalServices)
  .dependsOn(services, externalServices)

resolvers += Resolver.sbtPluginRepo("releases")
