package com.eventdynamic

import com.eventdynamic.edtickets.{EDTicketsConfig, EDTicketsService}
import com.eventdynamic.mlbam.PricingGridService
import com.eventdynamic.provenue.{ProVenueConfig, ProVenueService}
import com.eventdynamic.skybox.{SkyboxConfig, SkyboxService}
import com.eventdynamic.stubhub.{StubHubConfig, StubHubService}
import com.eventdynamic.models.{
  EDTicketsClientIntegrationConfig,
  SkyboxClientIntegrationConfig,
  StubHubClientIntegrationConfig,
  TDCClientIntegrationConfig
}
import com.eventdynamic.tdcreplicatedproxy.{TdcReplicatedProxyConfig, TdcReplicatedProxyService}

/**
  * This class allows us to build out services based on passed in configurations.
  * When we have multiple clients each with their own version of the service, this
  * will help us build out services specific to each one.  It also allows us to mock
  * out the services while testing.
  */
class IntegrationServiceBuilder() {

  def buildProVenueService(config: TDCClientIntegrationConfig) = new ProVenueService(
    ProVenueConfig(
      config.agent,
      config.apiKey,
      config.appId,
      config.baseUrl,
      config.username,
      config.password,
      config.supplierId,
      config.sandboxEmail
    )
  )

  def buildEDTicketsService(config: EDTicketsClientIntegrationConfig) = new EDTicketsService(
    EDTicketsConfig(
      config.baseUrl,
      config.apiKey,
      config.appId,
      config.appDatabase,
      config.email,
      config.password,
      config.functionsUrl
    )
  )

  def buildSkyboxService(config: SkyboxClientIntegrationConfig) =
    new SkyboxService(SkyboxConfig(config.appKey, config.baseUrl, config.apiKey, config.accountId))

  def buildStubHubService(config: StubHubClientIntegrationConfig) = new StubHubService(
    StubHubConfig(
      config.baseUrl,
      config.consumerKey,
      config.consumerSecret,
      config.username,
      config.password
    )
  )

  def buildPricingGridService() = new PricingGridService()

  def buildTdcReplicatedProxyService(config: TDCClientIntegrationConfig) =
    new TdcReplicatedProxyService(TdcReplicatedProxyConfig(config.tdcProxyBaseUrl))
}
