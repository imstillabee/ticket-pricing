package com.eventdynamic

import java.net.InetAddress

import scala.concurrent.Future

object IpService {

  def getMyInternalIP: Future[Option[String]] = {
    Future.successful(Option(InetAddress.getLocalHost.getHostAddress))
  }
}
