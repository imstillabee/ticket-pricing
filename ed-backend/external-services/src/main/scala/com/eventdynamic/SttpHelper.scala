package com.eventdynamic

import com.softwaremill.sttp.{asString, Response, ResponseAs, StringBody}
import play.api.libs.json.{Json, Reads, Writes}

case class UnparseableResponseException[T](message: String, response: Response[T])
    extends Exception(message)
case class UnsuccessfulResponseException[T](message: String, response: Response[T])
    extends Exception(message)

// TODO move to util/common module when ready
object SttpHelper {

  def handleResponse[T](response: Response[T]): T =
    response match {
      case r if !r.isSuccess =>
        throw UnsuccessfulResponseException(s"Unsuccessful response: $r", r)
      case r if r.body.isLeft =>
        throw UnparseableResponseException(s"Unable to parse response body: ${r.body.left.get}", r)
      case r => r.body.right.get
    }

  def asJson[T](implicit reads: Reads[T]): ResponseAs[T, Nothing] = {
    asString.map(Json.parse(_).as[T])
  }

  def jsonBody[T](request: T)(implicit writes: Writes[T]): StringBody = {
    StringBody(Json.toJson(request).toString(), "UTF-8", Some("application/json"))
  }

  def successResponse[T](body: T) =
    Response(Right(body), 200, "success", scala.collection.immutable.Seq(), List())

  def failedResponse[T](): Response[T] = {
    Response[T](Left("Error"), 500, "failed", scala.collection.immutable.Seq(), List())
  }
}
