package com.eventdynamic

import com.softwaremill.sttp.{MonadError, Request, Response, SttpBackend}
import org.slf4j.LoggerFactory
import play.api.libs.json.Json

import scala.language.higherKinds

class SttpLoggingBackend[R[_], S](
  delegate: SttpBackend[R, Nothing],
  serviceName: String,
  headerBlacklist: Seq[String] = Seq.empty[String]
) extends SttpBackend[R, Nothing] {
  private val logger = LoggerFactory.getLogger(this.getClass)

  override def send[T](request: Request[T, Nothing]): R[Response[T]] = {
    val requestInfo =
      Map[String, String](
        "service" -> serviceName,
        "method" -> request.method.toString,
        "uri" -> request.uri.toString,
        "requestHeaders" ->
          request.headers.filter(h => !headerBlacklist.contains(h._1)).toMap.toString(),
        "requestBody" -> request.body.toString
      )
    logger.info(Json.toJson(requestInfo).toString())

    val startTime = System.currentTimeMillis()
    responseMonad.map(responseMonad.handleError(delegate.send(request)) {
      case e: Exception =>
        val time = System.currentTimeMillis() - startTime
        val exceptionInfo = Map[String, String]("status" -> "exception", "time_ms" -> time.toString)

        logger.error(Json.toJson(requestInfo ++ exceptionInfo).toString(), e)

        responseMonad.error(e)
    }) { response =>
      val time = System.currentTimeMillis() - startTime
      val responseInfo = Map[String, String](
        "status" -> response.statusText,
        "responseHeaders" ->
          response.headers.filter(h => !headerBlacklist.contains(h._1)).toMap.toString(),
        "time_ms" -> time.toString
      )

      if (response.isSuccess) {
        logger.info(Json.toJson(requestInfo ++ responseInfo).toString())
      } else {
        val body = response.body.left.getOrElse("No Body")
        logger.warn(Json.toJson(requestInfo ++ responseInfo + ("responseBody" -> body)).toString())
      }
      response
    }
  }

  override def close(): Unit = delegate.close()

  override def responseMonad: MonadError[R] = delegate.responseMonad
}
