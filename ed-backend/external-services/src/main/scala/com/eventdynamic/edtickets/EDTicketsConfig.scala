package com.eventdynamic.edtickets

case class EDTicketsConfig(
  baseUrl: String,
  apiKey: String,
  appId: String,
  appDatabase: String,
  email: String,
  password: String,
  functionsUrl: String,
)
