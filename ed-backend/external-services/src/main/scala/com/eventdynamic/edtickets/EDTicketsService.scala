package com.eventdynamic.edtickets

import com.eventdynamic.edtickets.models._
import com.softwaremill.sttp._
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Success

class EDTicketsService(
  private val config: EDTicketsConfig,
  val sttpBackend: SttpBackend[Future, Nothing] = OkHttpFutureBackend()
) extends EDListing.format
    with EDLoginResponse.format
    with EDLoginRequest.format
    with EDPrice.format
    with EDOrder.format
    with EDPendingOrder.format
    with EDListingsResponse.format {

  implicit val backend: SttpBackend[Future, Nothing] = sttpBackend

  private var auth_token = ""

  private val defaultHeaders = Map(
    "Content-Type" -> "application/json",
    "Accept" -> "application/json",
    "Accept-Encoding" -> "application/json",
    "Accept-Language" -> "en-US"
  )

  private def request: RequestT[Empty, String, Nothing] = sttp.auth
    .bearer(auth_token)
    .headers(defaultHeaders)

  /**
    * POST https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword
    */
  def authenticate: Future[Response[EDLoginResponse]] = {
    sttp
      .post(
        uri"https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=${config.apiKey}"
      )
      .body(EDLoginRequest(config.email, config.password, returnSecureToken = true))
      .response(asEDLoginResponse)
      .send()
      .andThen {
        case Success(response) if response.isSuccess => auth_token = response.body.right.get.idToken
      }
  }

  /**
    * POST /documents/listings
    */
  def list(listing: EDListing): Future[Response[EDListing]] = {
    request
      .body(listing)
      .post(
        uri"${config.baseUrl}${config.appId}/databases/${config.appDatabase}/documents/listings"
      )
      .response(asEDListing)
      .send()
  }

  /**
    * GET /documents/listings
    */
  def getListings(pageToken: Option[String] = None): Future[Response[EDListings]] = {
    val queryParams = Map("pageToken" -> pageToken.getOrElse(""))

    request
      .get(
        uri"${config.baseUrl}${config.appId}/databases/${config.appDatabase}/documents/listings?${queryParams}"
      )
      .response(asEDListings)
      .send()
  }

  /**
    * GET /documents/listings/{listing_id}
    */
  def getListing(listingId: String): Future[Response[EDListing]] = {
    request
      .get(
        uri"${config.baseUrl}${config.appId}/databases/${config.appDatabase}/documents/listings?name=${listingId}"
      )
      .response(asEDListing)
      .send()
  }

  /**
    * PATCH /documents/listings/{listing_id}
    */
  def updateListing(eDListing: EDListing): Future[Response[EDListing]] = {
    request
      .patch(
        uri"${config.baseUrl}${config.appId}/databases/${config.appDatabase}/documents/listings?document.name=${eDListing.id.get}"
      )
      .body(eDListing)
      .response(asEDListing)
      .send()
  }

  /**
    * POST /documents:commit
    */
  def updatePrices(prices: Seq[EDPrice]): Future[Response[String]] = {
    request
      .post(uri"${config.baseUrl}${config.appId}/databases/${config.appDatabase}/documents:commit")
      .body(prices)
      .send()
  }

  /**
    * GET /documents/orders/{order_id}
    */
  def getOrder(orderId: String): Future[Response[EDOrder]] = {
    request
      .get(
        uri"${config.baseUrl}${config.appId}/databases/${config.appDatabase}/documents/orders/$orderId"
      )
      .response(asEDOrder)
      .send()
  }

  /**
    * PATCH /documents/orders/{order_id}
    */
  def updateOrder(order: EDOrder): Future[Response[EDOrder]] = {
    request
      .patch(
        uri"${config.baseUrl}${config.appId}/databases/${config.appDatabase}/documents/orders?document.name=${order.id}"
      )
      .body(order)
      .response(asEDOrder)
      .send()
  }

  /**
    * GET /getPendingOrders
    */
  def getPendingOrders: Future[Response[EDPendingOrders]] = {
    sttp
      .headers(defaultHeaders)
      .get(uri"${config.functionsUrl}/getPendingOrders")
      .response(asEDPendingOrders)
      .send()
  }

  /**
    * Fetch Listings for event (Only used for Inventory Sync)
    */
  def fetchListings(eventId: Int): Future[Response[EDListingsResponse]] = {
    request
      .get(
        uri"https://us-central1-edtickets-f4ede.cloudfunctions.net/getListings?eventId=${eventId}"
      )
      .response(asEDListingsResponse)
      .send()
  }
}
