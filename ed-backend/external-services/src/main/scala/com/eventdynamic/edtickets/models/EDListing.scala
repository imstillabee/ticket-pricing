package com.eventdynamic.edtickets.models

import com.softwaremill.sttp.{asString, BodySerializer, ResponseAs, StringBody}
import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json.{Format, JsPath, Json}

case class EDSeat(externalId: Int, seat: String, available: Boolean)

case class EDListing(
  id: Option[String],
  externalId: String,
  eventId: String,
  status: String,
  section: String,
  row: String,
  price: BigDecimal,
  seats: Seq[EDSeat]
)
case class EDListings(nextPageToken: Option[String], documents: Option[Seq[EDListing]])

object EDListing {

  trait format {

    implicit val seatFormat: Format[EDSeat] =
      ((JsPath \ "mapValue" \ "fields" \ "externalId" \ "doubleValue").format[Int] and
        (JsPath \ "mapValue" \ "fields" \ "seat" \ "stringValue").format[String] and
        (JsPath \ "mapValue" \ "fields" \ "available" \ "booleanValue")
          .format[Boolean])(EDSeat.apply, unlift(EDSeat.unapply))

    implicit val listingFormat: Format[EDListing] = (
      (JsPath \ "name").formatNullable[String] and
        (JsPath \ "fields" \ "externalId" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "eventId" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "status" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "section" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "row" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "price" \ "doubleValue").format[BigDecimal] and
        (JsPath \ "fields" \ "seats" \ "arrayValue" \ "values").format[Seq[EDSeat]]
    )(EDListing.apply, unlift(EDListing.unapply))

    implicit val listingsFormat: Format[EDListings] = Json.format[EDListings]

    implicit val listingBodySerializer: BodySerializer[EDListing] = { request: EDListing =>
      val serialized = Json.toJson(request).toString()
      StringBody(serialized, "UTF-8", Some("application/json"))
    }

    def asEDListing: ResponseAs[EDListing, Nothing] = {
      asString.map(Json.parse(_).validate[EDListing].get)
    }

    def asEDListings: ResponseAs[EDListings, Nothing] = {
      asString.map(Json.parse(_).validate[EDListings].get)
    }

  }
}
