package com.eventdynamic.edtickets.models

import com.softwaremill.sttp.{asString, ResponseAs}
import play.api.libs.json.{Format, Json}

case class EDListingsResponse(listings: Seq[EDListing])

object EDListingsResponse {

  trait format {

    implicit val seatResponseFormat: Format[EDSeat] = Json.format[EDSeat]

    implicit val listingResponseFormat: Format[EDListing] = Json.format[EDListing]

    implicit val listingsResponseFormat: Format[EDListingsResponse] =
      Json.format[EDListingsResponse]

    def asEDListingsResponse: ResponseAs[EDListingsResponse, Nothing] = {
      asString.map(Json.parse(_).validate[EDListingsResponse].get)
    }
  }
}
