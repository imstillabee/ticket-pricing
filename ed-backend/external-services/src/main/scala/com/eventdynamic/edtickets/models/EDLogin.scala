package com.eventdynamic.edtickets.models

import com.softwaremill.sttp.{asString, BodySerializer, ResponseAs, StringBody}
import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json.{Format, JsPath, Json}

case class EDLoginRequest(email: String, password: String, returnSecureToken: Boolean)
case class EDLoginResponse(idToken: String, refreshToken: String)

object EDLoginRequest {

  trait format {
    implicit val loginRequestFormat: Format[EDLoginRequest] = (
      (JsPath \ "email").format[String] and
        (JsPath \ "password").format[String] and
        (JsPath \ "returnSecureToken").format[Boolean]
    )(EDLoginRequest.apply, unlift(EDLoginRequest.unapply))

    implicit val loginRequestSerializer: BodySerializer[EDLoginRequest] = {
      request: EDLoginRequest =>
        val serialized = Json.toJson(request).toString()
        StringBody(serialized, "UTF-8", Some("application/json"))
    }
  }
}

object EDLoginResponse {

  trait format {
    implicit val loginResponse: Format[EDLoginResponse] = Json.format[EDLoginResponse]

    def asEDLoginResponse: ResponseAs[EDLoginResponse, Nothing] = {
      asString.map(Json.parse(_).validate[EDLoginResponse].get)
    }
  }

}
