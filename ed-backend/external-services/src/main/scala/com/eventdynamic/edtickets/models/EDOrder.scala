package com.eventdynamic.edtickets.models

import com.softwaremill.sttp.{asString, BodySerializer, ResponseAs, StringBody}
import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json._

case class EDSeatWithBarcode(
  externalId: String,
  seat: String,
  available: Boolean,
  barcode: Option[String]
)
case class EDOrder(
  id: String,
  eventId: String,
  externalEventId: String,
  listingId: String,
  price: BigDecimal,
  row: String,
  section: String,
  status: String,
  timestamp: String,
  userId: String,
  seats: Seq[EDSeatWithBarcode]
)

object EDOrder {

  trait format {

    implicit val seatwithbarcodeFormat: Format[EDSeatWithBarcode] = (
      (JsPath \ "mapValue" \ "fields" \ "externalId" \ "integerValue").format[String] and
        (JsPath \ "mapValue" \ "fields" \ "seat" \ "stringValue").format[String] and
        (JsPath \ "mapValue" \ "fields" \ "available" \ "booleanValue").format[Boolean] and
        (JsPath \ "mapValue" \ "fields" \ "barcode" \ "stringValue").formatNullable[String]
    )(EDSeatWithBarcode.apply, unlift(EDSeatWithBarcode.unapply))

    implicit val orderFormat: Format[EDOrder] = (
      (JsPath \ "name").format[String] and
        (JsPath \ "fields" \ "eventId" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "externalId" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "listingId" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "price" \ "integerValue").format[BigDecimal] and
        (JsPath \ "fields" \ "row" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "section" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "status" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "timestamp" \ "timestampValue").format[String] and
        (JsPath \ "fields" \ "userId" \ "stringValue").format[String] and
        (JsPath \ "fields" \ "seats" \ "arrayValue" \ "values").format[Seq[EDSeatWithBarcode]]
    )(EDOrder.apply, unlift(EDOrder.unapply))

    implicit val orderBodySerializer: BodySerializer[EDOrder] = { request: EDOrder =>
      val serialized = Json.toJson(request).toString()
      StringBody(serialized, "UTF-8", Some("application/json"))
    }

    def asEDOrder: ResponseAs[EDOrder, Nothing] = {
      asString.map(Json.parse(_).validate[EDOrder].get)
    }

  }
}
