package com.eventdynamic.edtickets.models

import java.time.Instant

import com.eventdynamic.utils.InstantISOFormat
import com.softwaremill.sttp.{asString, ResponseAs}
import play.api.libs.json._

case class EDPendingOrderSeat(
  externalId: Int,
  seat: String,
  available: Boolean,
  barcode: Option[String]
)

case class EDPendingOrder(
  id: String,
  eventId: String,
  externalId: String,
  listingId: String,
  price: BigDecimal,
  row: String,
  section: String,
  status: String,
  timestamp: Instant,
  userId: String,
  seats: Seq[EDPendingOrderSeat]
)

case class EDPendingOrders(orders: Option[Seq[EDPendingOrder]])

object EDPendingOrder {

  trait format extends InstantISOFormat {
    implicit val edPendingOrderSeatReads: Reads[EDPendingOrderSeat] = Json.reads[EDPendingOrderSeat]

    implicit val edPendingOrderReads: Reads[EDPendingOrder] = Json.reads[EDPendingOrder]

    implicit val edPendingOrdersReads: Reads[EDPendingOrders] = Json.reads[EDPendingOrders]

    def asEDPendingOrders: ResponseAs[EDPendingOrders, Nothing] = {
      asString.map(Json.parse(_).validate[EDPendingOrders].get)
    }
  }
}
