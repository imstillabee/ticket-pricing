package com.eventdynamic.edtickets.models

import com.softwaremill.sttp.{BodySerializer, StringBody}
import play.api.libs.json._

case class EDPrice(listingId: String, updatePrice: BigDecimal)

object EDPrice {

  trait format {

    implicit val priceWrites = new Writes[EDPrice] {

      def writes(price: EDPrice): JsObject =
        Json.obj(
          "updateMask" ->
            Json.obj("fieldPaths" -> Json.arr("price")),
          "update" -> Json.obj(
            "name" -> price.listingId,
            "fields" -> Json.obj("price" -> Json.obj("doubleValue" -> price.updatePrice))
          )
        )
    }

    implicit val pricesWrites = new Writes[Seq[EDPrice]] {

      def writes(prices: Seq[EDPrice]): JsObject =
        Json.obj("writes" -> Json.arr(prices.map(p => p)))
    }

    implicit val priceBodySerializer: BodySerializer[Seq[EDPrice]] = { request: Seq[EDPrice] =>
      val serialized = Json.toJson(request).toString()
      StringBody(serialized, "UTF-8", Some("application/json"))
    }
  }
}
