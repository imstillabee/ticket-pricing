package com.eventdynamic.mlbam

import com.typesafe.config.{Config, ConfigFactory}

case class PricingGridConfig(baseUrl: String, sourceId: String)

object PricingGridConfig {

  def fromConfig(config: Config = ConfigFactory.load()): PricingGridConfig = {
    try {
      PricingGridConfig(
        config.getString("pricingGrid.baseUrl"),
        config.getString("pricingGrid.sourceId")
      )
    } catch {
      case _: Throwable => throw new Exception("There are no price grid configurations set")
    }
  }

  def default: PricingGridConfig = fromConfig()
}
