package com.eventdynamic.mlbam

import java.text.SimpleDateFormat

import com.eventdynamic.mlbam.models.PricingGridBatch
import com.eventdynamic.utils.DateHelper
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend

import scala.concurrent.Future
import com.softwaremill.sttp._
import scala.concurrent.duration._

class PricingGridService(
  private val conf: PricingGridConfig = PricingGridConfig.default,
  private val sttpBackend: SttpBackend[Future, Nothing] = OkHttpFutureBackend()
) {
  implicit val backend: SttpBackend[Future, Nothing] = sttpBackend

  private val sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss ZZZ")
  private val req = sttp.headers("Content-Type" -> "text/xml")

  def bulkUpdatePrices(
    batch: Seq[PricingGridBatch],
    teamId: Int,
    priceGroupId: Int
  ): Future[Response[String]] = {
    val currentTime = sdf.format(DateHelper.now())
    val sectionPrices = batch.map(row => {
      <sectionPrice>
        <teamId>{teamId}</teamId>
        <scheduleId>{row.scheduleId}</scheduleId>
        <sectionId>{row.sectionId}</sectionId>
        <price>{row.priceUpdate}</price>
        <featured>false</featured>
        <priceGroupId>{priceGroupId}</priceGroupId>
      </sectionPrice>
    })

    val body =
      <ns2:PriceFeed timestamp={currentTime} version="1" sourceId={conf.sourceId}
        xmlns:ns2="http://www.mlb.com/ticketing/price/dynamic">
        {sectionPrices.flatten}
      </ns2:PriceFeed>

    req
      .post(uri"${conf.baseUrl}/ticketing-admin/PriceChange.prc")
      .body(body.toString)
      .readTimeout(5.minutes)
      .send()
  }
}
