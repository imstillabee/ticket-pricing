package com.eventdynamic.mlbam.models

case class BuyerTypePrice(externalBuyerTypeId: String, price: Option[BigDecimal])

case class PricingGridBatch(scheduleId: Int, sectionId: Int, priceUpdate: BigDecimal)
