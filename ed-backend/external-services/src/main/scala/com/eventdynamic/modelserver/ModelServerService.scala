package com.eventdynamic.modelserver

import java.sql.Timestamp
import java.time.Instant

import com.eventdynamic.modelserver.models.ModelServerFormats._
import com.eventdynamic.modelserver.models.ModelServerPriceAll.ModelServerPriceAllResponse
import com.eventdynamic.modelserver.models.ModelServerSerializers._
import com.eventdynamic.modelserver.models._
import com.eventdynamic.{SttpHelper, SttpLoggingBackend}
import com.softwaremill.sttp._
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ModelServerService(
  val baseUrl: String,
  val sttpBackend: SttpBackend[Future, Nothing] =
    new SttpLoggingBackend(OkHttpFutureBackend(), "ModelServer")
) {

  implicit val backend: SttpBackend[Future, Nothing] = sttpBackend

  private val defaultHeaders = Map(
    "Content-Type" -> "application/json",
    "Accept" -> "application/json",
    "Accept-Encoding" -> "application/json",
    "Accept-Language" -> "en-US"
  )

  private def request: RequestT[Empty, String, Nothing] = sttp.headers(defaultHeaders)

  /**
    * POST /client/:clientId/price
    *
    * Gets prices from the model server.
    *
    * @param clientId
    * @param payload
    * @return
    */
  def getPrices(
    clientId: String,
    payload: ModelServerPricePayload
  ): Future[Response[ModelServerPriceResponse]] = {
    request
      .post(uri"$baseUrl/client/$clientId/price")
      .body(payload)
      .response(SttpHelper.asJson[ModelServerPriceResponse])
      .send()
  }

  /**
    * GET /client/:clientId/priceAll
    *
    * Gets all prices for a given context.
    *
    * @param clientId
    * @param eventScore
    * @param spring
    * @return
    */
  def getAllPrices(
    clientId: String,
    eventScore: Double,
    spring: Double
  ): Future[ModelServerPriceAllResponse] = {
    val queryParams = Map("event_score" -> eventScore, "spring" -> spring / 100.0)

    request
      .header("Content-Type", "", true)
      .get(uri"$baseUrl/client/$clientId/priceAll?$queryParams")
      .response(SttpHelper.asJson[ModelServerPriceAllResponse])
      .send()
      .map(SttpHelper.handleResponse)
  }

  /**
    * POST /client/:clientId/factors
    *
    * Gets event scores and spring values from the model server.
    *
    * NOTE: spring values are 1-based! On this side we store those as 100-based.
    */
  def getFactors(
    clientId: String,
    payload: ModelServerFactorPayload
  ): Future[Response[ModelServerFactorResponse]] = {
    request
      .post(uri"$baseUrl/client/$clientId/factors")
      .body(payload)
      .response(SttpHelper.asJson[ModelServerFactorResponse])
      .send()
  }

  /**
    * GET /client/:clientId/spring
    *
    * Get the spring value for a given event score and event date
    */
  def getAutomatedSpringValue(
    clientId: String,
    eventScore: Double,
    eventDate: Option[Timestamp]
  ): Future[ModelServerSpringResponse] = {
    val queryParams =
      if (eventDate.isDefined)
        Map("event_score" -> eventScore, "timestamp" -> Instant.now, "event_date" -> eventDate.get)
      else Map("event_score" -> eventScore)
    val uri = uri"$baseUrl/client/$clientId/spring?$queryParams"
    request
      .header("Content-Type", "", true)
      .get(uri)
      .response(SttpHelper.asJson[ModelServerSpringResponse])
      .send()
      .map(SttpHelper.handleResponse)
  }

  /**
    * GET /client/:clientId/projection
    *
    * Get projections for an event
    */
  def getProjections(
    clientId: String,
    payload: ModelServerProjectionPayload
  ): Future[Seq[ModelServerProjection]] = {
    request
      .post(uri"$baseUrl/client/$clientId/projection")
      .body(payload)
      .response(SttpHelper.asJson[ModelServerProjectionResponse])
      .send()
      .map(SttpHelper.handleResponse)
      .map(_.projection)
  }
}
