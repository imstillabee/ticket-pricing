package com.eventdynamic.modelserver.models

// Client-specific stats that are added to the Context of a Model Server Factor Payload
sealed trait ModelServerClientStats

case class MLBModelServerClientStats(wins: Int, losses: Int, rank: Option[Int], games_ahead: Float)
    extends ModelServerClientStats

case class NFLModelServerClientStats(wins: Int, losses: Int, ties: Int)
    extends ModelServerClientStats

case class MLSModelServerClientStats(wins: Int, losses: Int, ties: Int)
    extends ModelServerClientStats

case class NCAAFModelServerClientStats(wins: Int, losses: Int) extends ModelServerClientStats

case class NCAABModelServerClientStats(wins: Int, losses: Int) extends ModelServerClientStats
