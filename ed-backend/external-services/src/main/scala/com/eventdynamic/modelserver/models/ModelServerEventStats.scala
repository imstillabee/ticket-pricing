package com.eventdynamic.modelserver.models

// Event-specific stats that are added to individual Examples in a Model Server Factor Payload
sealed trait ModelServerEventStats

case class MLBModelServerEventStats(
  game_num: Int,
  home_opener: Int,
  home_series_start: Int,
  opp_losses: Int,
  opp_rank: Option[Int],
  opp_wins: Int,
  opponent: String
) extends ModelServerEventStats

case class NFLModelServerEventStats(opponent: String, is_preseason: Int)
    extends ModelServerEventStats

case class MLSModelServerEventStats(opponent: String, home_game_number: Int)
    extends ModelServerEventStats

case class NCAAFModelServerEventStats(opponent: String) extends ModelServerEventStats

case class NCAABModelServerEventStats(opponent: String) extends ModelServerEventStats
