package com.eventdynamic.modelserver.models

// Separates client-specific data from the rest of the context
case class ModelServerFactorContext(timestamp: String, clientStats: ModelServerClientStats)
