package com.eventdynamic.modelserver.models

// Separates client-specific event data from the rest of the example
case class ModelServerFactorExample(
  event_date: String,
  event_score_modifier: Double,
  spring_modifier: Double,
  weather: Option[ModelServerWeather],
  eventStats: ModelServerEventStats
)
