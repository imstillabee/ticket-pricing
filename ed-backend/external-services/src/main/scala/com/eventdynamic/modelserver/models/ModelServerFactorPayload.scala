package com.eventdynamic.modelserver.models

case class ModelServerFactorPayload(
  context: ModelServerFactorContext,
  examples: Seq[ModelServerFactorExample]
)
