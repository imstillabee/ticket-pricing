package com.eventdynamic.modelserver.models

case class ModelServerFactorResponse(eventScores: Seq[Double], springValues: Seq[Double])
