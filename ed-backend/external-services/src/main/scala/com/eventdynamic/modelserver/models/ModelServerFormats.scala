package com.eventdynamic.modelserver.models

import com.eventdynamic.modelserver.models.ModelServerPriceAll.{
  ModelServerPriceAllResponse,
  ModelServerPriceAllResponseItem
}
import com.eventdynamic.utils.InstantISOFormat
import play.api.libs.json._

object ModelServerFormats extends InstantISOFormat {
  implicit lazy val projectionPayload: OFormat[ModelServerProjectionPayload] =
    Json.format[ModelServerProjectionPayload]

  implicit lazy val projectionExample: OFormat[ModelServerProjectionExample] =
    Json.format[ModelServerProjectionExample]

  implicit lazy val projectionContext: OFormat[ModelServerProjectionContext] =
    Json.format[ModelServerProjectionContext]

  implicit lazy val projectionFormat: OFormat[ModelServerProjection] =
    Json.format[ModelServerProjection]

  implicit lazy val projectionResponseFormat: OFormat[ModelServerProjectionResponse] =
    Json.format[ModelServerProjectionResponse]

  implicit lazy val mlbClientStatsFormat = Json.format[MLBModelServerClientStats]

  implicit lazy val nflClientStatsFormat = Json.format[NFLModelServerClientStats]

  implicit lazy val mlsClientStatsFormat = Json.format[MLSModelServerClientStats]

  implicit lazy val ncaafClientStatsFormat = Json.format[NCAAFModelServerClientStats]

  implicit lazy val ncaabClientStatsFormat = Json.format[NCAABModelServerClientStats]

  implicit lazy val mlbEventStatsFormat = Json.format[MLBModelServerEventStats]

  implicit lazy val nflEventStatsFormat = Json.format[NFLModelServerEventStats]

  implicit lazy val mlsEventStatsFormat = Json.format[MLSModelServerEventStats]

  implicit lazy val ncaafEventStatsFormat = Json.format[NCAAFModelServerEventStats]

  implicit lazy val ncaabEventStatsFormat = Json.format[NCAABModelServerEventStats]

  implicit lazy val modelServerWeatherFormat = Json.format[ModelServerWeather]

  implicit lazy val modelServerFactorContextWrites = new OWrites[ModelServerFactorContext] {

    def writes(modelServerFactorContext: ModelServerFactorContext): JsObject = {
      // Create json object based on type of client stats
      val clientStatsJsObject = modelServerFactorContext.clientStats match {
        case mlbCs: MLBModelServerClientStats     => Json.toJsObject(mlbCs)
        case nflCs: NFLModelServerClientStats     => Json.toJsObject(nflCs)
        case mlsCs: MLSModelServerClientStats     => Json.toJsObject(mlsCs)
        case ncaafCs: NCAAFModelServerClientStats => Json.toJsObject(ncaafCs)
        case ncaabCs: NCAABModelServerClientStats => Json.toJsObject(ncaabCs)
      }

      // Move the properties of the client stats json object
      // to the same level as the properties in the rest of the context
      JsObject(Seq("timestamp" -> JsString(modelServerFactorContext.timestamp))) ++ clientStatsJsObject
    }
  }

  implicit lazy val modelServerFactorExampleWrites = new OWrites[ModelServerFactorExample] {

    def writes(modelServerFactorExample: ModelServerFactorExample): JsObject = {
      // Create json object based on type of event stats
      val eventStatsJsObject = modelServerFactorExample.eventStats match {
        case mlbEs: MLBModelServerEventStats     => Json.toJsObject(mlbEs)
        case nflEs: NFLModelServerEventStats     => Json.toJsObject(nflEs)
        case mlsEs: MLSModelServerEventStats     => Json.toJsObject(mlsEs)
        case ncaafEs: NCAAFModelServerEventStats => Json.toJsObject(ncaafEs)
        case ncaabEs: NCAABModelServerEventStats => Json.toJsObject(ncaabEs)
      }

      val weatherJsObject =
        modelServerFactorExample.weather.map(Json.toJsObject(_)).getOrElse(JsObject.empty)

      // Move the properties of the event stats json object
      // to the same level as the properties in the rest of the example
      JsObject(
        Seq(
          "event_date" -> JsString(modelServerFactorExample.event_date),
          "event_score_modifier" -> JsNumber(modelServerFactorExample.event_score_modifier),
          "spring_modifier" -> JsNumber(modelServerFactorExample.spring_modifier)
        )
      ) ++ eventStatsJsObject ++ weatherJsObject
    }
  }

  implicit lazy val factorPayloadWrites: OWrites[ModelServerFactorPayload] =
    Json.writes[ModelServerFactorPayload]

  implicit lazy val factorResponseFormat: Format[ModelServerFactorResponse] =
    Json.format[ModelServerFactorResponse]

  implicit lazy val priceContextFormat: Format[ModelServerPriceContext] =
    Json.format[ModelServerPriceContext]

  implicit lazy val priceExampleFormat: Format[ModelServerPriceExample] =
    Json.format[ModelServerPriceExample]

  implicit lazy val pricePayloadFormat: Format[ModelServerPricePayload] =
    Json.format[ModelServerPricePayload]

  implicit lazy val optionalBigDecimalFormat: Format[Option[BigDecimal]] =
    Format.optionWithNull[BigDecimal]

  implicit lazy val priceResponseFormat: Format[ModelServerPriceResponse] =
    Json.format[ModelServerPriceResponse]

  implicit lazy val springResponseFormat: Format[ModelServerSpringResponse] =
    Json.format[ModelServerSpringResponse]

  implicit lazy val priceAllResponseItemFormat: OFormat[ModelServerPriceAllResponseItem] =
    Json.format[ModelServerPriceAllResponseItem]

  implicit lazy val priceAllResponseFormat: OFormat[ModelServerPriceAllResponse] =
    Json.format[ModelServerPriceAllResponse]
}
