package com.eventdynamic.modelserver.models

object ModelServerPriceAll {
  case class ModelServerPriceAllResponse(prices: Seq[ModelServerPriceAllResponseItem])
  case class ModelServerPriceAllResponseItem(section: String, row: String, price: BigDecimal)
}
