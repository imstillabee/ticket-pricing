package com.eventdynamic.modelserver.models

case class ModelServerPriceContext(
  event_date: String,
  event_score: Double,
  spring: Double,
  timestamp: String,
  weather: Option[ModelServerWeather]
)
