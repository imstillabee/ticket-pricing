package com.eventdynamic.modelserver.models

case class ModelServerPriceExample(row: String, scale: String, section: String, qty_left: Int)
