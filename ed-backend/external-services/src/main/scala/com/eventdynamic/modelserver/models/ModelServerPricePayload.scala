package com.eventdynamic.modelserver.models

case class ModelServerPricePayload(
  context: ModelServerPriceContext,
  examples: Seq[ModelServerPriceExample]
)
