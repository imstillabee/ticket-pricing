package com.eventdynamic.modelserver.models

case class ModelServerPriceResponse(prices: Seq[Option[BigDecimal]])
