package com.eventdynamic.modelserver.models

import java.time.Instant

case class ModelServerProjection(
  timestamp: Instant,
  cumulative_inventory: Int,
  cumulative_inventory_ub: Int,
  cumulative_inventory_lb: Int,
  cumulative_revenue: BigDecimal,
  cumulative_revenue_ub: BigDecimal,
  cumulative_revenue_lb: BigDecimal
)
