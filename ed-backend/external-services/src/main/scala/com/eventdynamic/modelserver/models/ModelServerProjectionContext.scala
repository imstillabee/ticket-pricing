package com.eventdynamic.modelserver.models

import java.time.Instant

case class ModelServerProjectionContext(event_date: Instant, total_inventory: Int)
