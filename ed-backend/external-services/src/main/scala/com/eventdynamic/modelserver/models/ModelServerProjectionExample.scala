package com.eventdynamic.modelserver.models

import java.time.Instant

case class ModelServerProjectionExample(
  timestamp: Instant,
  cumulative_inventory: Int,
  cumulative_revenue: BigDecimal
)
