package com.eventdynamic.modelserver.models

case class ModelServerProjectionPayload(
  context: ModelServerProjectionContext,
  examples: Seq[ModelServerProjectionExample]
)
