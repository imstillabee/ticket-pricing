package com.eventdynamic.modelserver.models

case class ModelServerProjectionResponse(projection: Seq[ModelServerProjection])
