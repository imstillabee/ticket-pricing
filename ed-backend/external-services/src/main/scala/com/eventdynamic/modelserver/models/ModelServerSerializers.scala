package com.eventdynamic.modelserver.models

import com.eventdynamic.SttpHelper
import com.eventdynamic.modelserver.models.ModelServerFormats._
import com.softwaremill.sttp.BodySerializer

object ModelServerSerializers {

  implicit lazy val modelServerPayloadSerializer: BodySerializer[ModelServerProjectionPayload] =
    SttpHelper.jsonBody[ModelServerProjectionPayload]

  implicit lazy val factorPayloadSerializer: BodySerializer[ModelServerFactorPayload] =
    SttpHelper.jsonBody[ModelServerFactorPayload]

  implicit lazy val pricePayloadSerializer: BodySerializer[ModelServerPricePayload] =
    SttpHelper.jsonBody[ModelServerPricePayload]
}
