package com.eventdynamic.modelserver.models

case class ModelServerWeather(temp_average: Float, temp: Float, weather_condition: String)
