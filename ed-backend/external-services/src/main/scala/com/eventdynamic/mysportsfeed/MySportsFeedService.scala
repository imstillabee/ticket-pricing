package com.eventdynamic.mysportsfeed

import java.nio.charset.StandardCharsets
import java.util.Base64

import com.eventdynamic.SttpHelper
import com.eventdynamic.mysportsfeed.models.mlb.{
  Division,
  GameEntry,
  GameLogs,
  MLBGameStats,
  MLBStats,
  TeamStandings
}
import com.eventdynamic.mysportsfeed.models.nfl.{NFLGameLogs, NFLStandings}
import com.eventdynamic.mysportsfeed.models.nfl.Implicits._
import com.softwaremill.sttp._
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try

class MySportsFeedService(
  private val username: String,
  private val password: String,
  private val baseUrl: String,
  val sttpBackend: SttpBackend[Future, Nothing]
) {

  implicit val backend: SttpBackend[Future, Nothing] = sttpBackend

  val authTokenV1 =
    Base64.getEncoder.encodeToString(s"$username:$password".getBytes(StandardCharsets.UTF_8))

  val authTokenV2 =
    Base64.getEncoder.encodeToString(s"$username:MYSPORTSFEEDS".getBytes(StandardCharsets.UTF_8))

  private def reqV2 = sttp.header("Authorization", s"Basic $authTokenV2")

  object MLB {

    /** Define the team standings URL for getting divisional team statistics
      *
      * @param params
      * @return
      */
    def teamStandingsUrl(params: Map[String, String]): Uri =
      uri"$baseUrl/v1.2/pull/mlb/current/division_team_standings.json?$params"

    /** Define the team game logs URL for getting game statistics
      *
      * @param params
      * @return
      */
    def teamLogsUrl(params: Map[String, String]): Uri =
      uri"$baseUrl/v1.2/pull/mlb/current/team_gamelogs.json?$params"

    /** Define the team full schedule URL for getting games schedule
      *
      * @param params
      * @return
      */
    def teamFullSchedule(params: Map[String, String]): Uri =
      uri"$baseUrl/v1.2/pull/mlb/current/full_game_schedule.json?$params"

    /** Get Team Divisional Standings from MySportsFeedApi
      *
      * @param teamAbbr
      * @return
      */
    def getTeamStandings(teamAbbr: String): Future[Option[Seq[Division]]] = {
      val params = Map("teamStats" -> "W,L,GB")
      // FIXME: Include the team abbreviation when this issue is fixed: (https://dialexa.atlassian.net/browse/EVNT-197)
      request(teamStandingsUrl(params)).map(body => {
        if (body.isDefined) TeamStandings.fromResponse(body.get) else None
      })
    }

    /** Get Team Game Logs for all games up to today
      *
      * @param teamAbbr
      * @return
      */
    def getTeamGameLogs(teamAbbr: String): Future[Option[Seq[GameLogs]]] = {
      val params = Map(
        "team" -> teamAbbr,
        "teamStats" -> "W",
        "date" -> "until-today",
        "sort" -> "game.starttime"
      )

      request(teamLogsUrl(params)).map(body => {
        if (body.isDefined) GameLogs.fromResponse(body.get) else None
      })
    }

    /** Get Team Full Schedule
      *
      * @param teamAbbr
      * @return
      */
    def getTeamFullSchedule(teamAbbr: String): Future[Option[Seq[GameEntry]]] = {
      val params = Map("team" -> teamAbbr)

      request(teamFullSchedule(params)).map(body => {
        if (body.isDefined) GameEntry.fromResponse(body.get) else None
      })
    }

    /** Calculates the game stats from api results
      *
      * @param mlbStats
      * @param dateString
      * @return
      */
    def calculateGameStats(mlbStats: MLBStats, dateString: String): MLBGameStats = {
      val allGames = mlbStats.schedule.getOrElse(Seq()).sortBy(_.date)
      val game_day = Try(allGames.zipWithIndex.filter(_._1.date == dateString)).toOption

      val game = Try(game_day.get.head).toOption
      if (game.isEmpty)
        throw new Exception(s"Unable to find game for game statistics ($dateString)")

      val home_opener = allGames
        .filter(_.awayTeam != mlbStats.teamAbbreviation)
        .minBy(_.date)
        .date == dateString
      val previousOpponent = Try(allGames.apply(game.get._2 - 1).awayTeam).toOption
      val home_series_start =
        mlbStats.teamAbbreviation.contains(previousOpponent.getOrElse(mlbStats.teamAbbreviation))

      MLBGameStats(game.get._2, game.get._1.awayTeam, home_opener, home_series_start)
    }

    /** API helper for MySportsFeeds API
      *
      * @param uri
      * @return
      */
    def request(uri: Uri): Future[Option[String]] = sttp
      .header("Authorization", s"Basic $authTokenV1")
      .get(uri)
      .send()
      .map(res => {
        if (res.isSuccess) {
          Some(res.body.right.get)
        } else {
          None
        }
      })
  }

  object NFL {

    /**
      * Gets the game-level information for a team.  Provides the following fields per game:
      *   - Week #
      *   - Home team (abbr)
      *   - Away team (abbr)
      *   - Start time
      *
      * @param teamAbbreviations abbreviation of the home team
      * @return all games that the passed team is playing
      */
    def getGameLogs(teamAbbreviations: Seq[String]): Future[NFLGameLogs] = {
      val params = Map("team" -> teamAbbreviations.mkString(","), "stats" -> "none")
      val url = uri"$baseUrl/v2.1/pull/nfl/upcoming/games.json?$params"

      reqV2
        .get(url)
        .response(SttpHelper.asJson[NFLGameLogs])
        .send()
        .map(SttpHelper.handleResponse)
    }

    /**
      * Gets the standing information for the team provided.
      *
      * @param teamAbbreviations abbreviation of the home team
      * @return the standings of the passed in team
      */
    def getStandings(teamAbbreviations: Seq[String]): Future[NFLStandings] = {
      val params = Map("team" -> teamAbbreviations.mkString(","), "stats" -> "W,L,T")
      val url = uri"$baseUrl/v2.1/pull/nfl/upcoming/standings.json?$params"

      reqV2
        .get(url)
        .response(SttpHelper.asJson[NFLStandings])
        .send()
        .map(SttpHelper.handleResponse)
    }
  }
}

object MySportsFeedService {
  val defaultBackend = OkHttpFutureBackend()

  def fromConfig(
    conf: Config = ConfigFactory.load(),
    backend: SttpBackend[Future, Nothing] = defaultBackend
  ): MySportsFeedService = {
    val username = conf.getString("mySportsFeed.username")
    val password = conf.getString("mySportsFeed.password")
    val baseUrl = conf.getString("mySportsFeed.baseUrl")

    new MySportsFeedService(username, password, baseUrl, backend)
  }
}
