package com.eventdynamic.mysportsfeed.models.mlb

import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Failure, Success, Try}

case class TeamStandings(
  team: String,
  rank: String,
  wins: String,
  losses: String,
  gamesBack: String
)
case class Division(name: String, teamStandings: Seq[TeamStandings])
case class OverallStandings(lastUpdatedOn: String, division: Seq[Division])

object TeamStandings {

  def fromResponse(str: String): Option[Seq[Division]] = {
    Try(Json.parse(str)) match {
      case Failure(_) => None
      case Success(res) =>
        val validation = res.asOpt[OverallStandings]
        if (validation.isDefined) validation.map(_.division) else None
    }
  }

  implicit val teamStandingsReads: Reads[TeamStandings] = (
    (JsPath \ "team" \ "Abbreviation").read[String] and
      (JsPath \ "rank").read[String] and
      (JsPath \ "stats" \ "Wins" \ "#text").read[String] and
      (JsPath \ "stats" \ "Losses" \ "#text").read[String] and
      (JsPath \ "stats" \ "GamesBack" \ "#text").read[String]
  )(TeamStandings.apply _)

  implicit val divisionReads: Reads[Division] = (
    (JsPath \ "@name").read[String] and
      (JsPath \ "teamentry").read[Seq[TeamStandings]]
  )(Division.apply _)

  implicit val overallStandingsReads: Reads[OverallStandings] = (
    (JsPath \ "divisionteamstandings" \ "lastUpdatedOn").read[String] and
      (JsPath \ "divisionteamstandings" \ "division").read[Seq[Division]]
  )(OverallStandings.apply _)

}

case class GameLogs(date: String, time: String, opponent: String, won: String)
case class TeamGameLogs(lastUpdatedOn: String, gameLogs: Seq[GameLogs])

object GameLogs {

  def fromResponse(str: String): Option[Seq[GameLogs]] = {
    Try(Json.parse(str)) match {
      case Failure(_) => None
      case Success(res) =>
        val validation = res.asOpt[TeamGameLogs]
        if (validation.isDefined) validation.map(_.gameLogs) else None
    }
  }

  implicit val gameLogsReads: Reads[GameLogs] = (
    (JsPath \ "game" \ "date").read[String] and
      (JsPath \ "game" \ "time").read[String] and
      (JsPath \ "game" \ "awayTeam" \ "Abbreviation").read[String] and
      (JsPath \ "stats" \ "Wins" \ "#text").read[String]
  )(GameLogs.apply _)

  implicit val teamGameLogsReads: Reads[TeamGameLogs] = (
    (JsPath \ "teamgamelogs" \ "lastUpdatedOn").read[String] and
      (JsPath \ "teamgamelogs" \ "gamelogs").read[Seq[GameLogs]]
  )(TeamGameLogs.apply _)
}

case class GameEntry(date: String, time: String, awayTeam: String, homeTeam: String)
case class TeamFullSchedule(lastUpdatedOn: String, gameentry: Seq[GameEntry])

object GameEntry {

  def fromResponse(str: String): Option[Seq[GameEntry]] = {
    Try(Json.parse(str)) match {
      case Failure(_) => None
      case Success(res) =>
        val validation = res.asOpt[TeamFullSchedule]
        if (validation.isDefined) validation.map(_.gameentry) else None
    }
  }

  implicit val gameEntryReads: Reads[GameEntry] = (
    (JsPath \ "date").read[String] and
      (JsPath \ "time").read[String] and
      (JsPath \ "awayTeam" \ "Abbreviation").read[String] and
      (JsPath \ "homeTeam" \ "Abbreviation").read[String]
  )(GameEntry.apply _)

  implicit val teamFullScheduleReads: Reads[TeamFullSchedule] = (
    (JsPath \ "fullgameschedule" \ "lastUpdatedOn").read[String] and
      (JsPath \ "fullgameschedule" \ "gameentry").read[Seq[GameEntry]]
  )(TeamFullSchedule.apply _)
}
