package com.eventdynamic.mysportsfeed.models.mlb

case class MLBTeamStats(rank: Int, wins: Float, losses: Float, gamesAhead: Float, streak: Int)

case class MLBGameStats(
  gameNum: Int,
  opponent: String,
  homeOpener: Boolean,
  homeSeriesStart: Boolean
)

case class MLBStats(
  teamAbbreviation: String,
  teamStats: Option[MLBTeamStats],
  gameLogs: Option[Seq[GameLogs]],
  schedule: Option[Seq[GameEntry]]
)
