package com.eventdynamic.mysportsfeed.models.nfl

import java.time.Instant

import com.eventdynamic.utils.InstantISOFormat
import play.api.libs.json._

object Implicits extends InstantISOFormat {
  implicit val nflTeamStatsStandingsReads: Reads[NFLTeamStatsStandings] =
    Json.reads[NFLTeamStatsStandings]
  implicit val nflTeamStatsReads: Reads[NFLTeamStats] = Json.reads[NFLTeamStats]
  implicit val nflTeamInfoReads: Reads[NFLTeamInfo] = Json.reads[NFLTeamInfo]
  implicit val nflTeamReads: Reads[NFLTeam] = Json.reads[NFLTeam]
  implicit val nflStandingsReads: Reads[NFLStandings] = Json.reads[NFLStandings]

  implicit val nflGameScheduleTeamReads: Reads[NFLGameScheduleTeam] =
    Json.reads[NFLGameScheduleTeam]
  implicit val nflGameScheduleReads: Reads[NFLGameSchedule] = Json.reads[NFLGameSchedule]
  implicit val nflGameLogReads: Reads[NFLGameLog] = Json.reads[NFLGameLog]
  implicit val nflGameLogsReads: Reads[NFLGameLogs] = Json.reads[NFLGameLogs]
}

/**
  * Classes for the standings endpoint
  */

case class NFLStandings(teams: Seq[NFLTeam])

case class NFLTeam(team: NFLTeamInfo, stats: NFLTeamStats)

case class NFLTeamInfo(abbreviation: String)

case class NFLTeamStats(standings: NFLTeamStatsStandings)

case class NFLTeamStatsStandings(wins: Int, losses: Int, ties: Int)

/**
  * Classes for schedule endpoint
  */

case class NFLGameLogs(games: Seq[NFLGameLog])

case class NFLGameLog(schedule: NFLGameSchedule)

case class NFLGameSchedule(
  week: Int,
  homeTeam: NFLGameScheduleTeam,
  awayTeam: NFLGameScheduleTeam,
  startTime: Instant
)

case class NFLGameScheduleTeam(abbreviation: String)
