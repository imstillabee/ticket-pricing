package com.eventdynamic.mysportsfeed.models.nfl

case class NFLGameStats(opponent: String)

case class NFLStats(
  teamAbbreviation: String,
  teamStats: Option[NFLTeamStatsStandings],
  gameLogs: NFLGameLogs
)
