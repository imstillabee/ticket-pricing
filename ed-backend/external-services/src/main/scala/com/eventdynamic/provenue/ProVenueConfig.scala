package com.eventdynamic.provenue

case class ProVenueConfig(
  agent: String,
  apiKey: String,
  appId: String,
  baseUrl: String,
  username: String,
  password: String,
  supplierId: Int,
  sandboxEmail: Option[String]
)
