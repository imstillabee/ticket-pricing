package com.eventdynamic.provenue

import com.eventdynamic.{IpService, SttpLoggingBackend}
import com.eventdynamic.provenue.models._
import com.softwaremill.sttp._
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Success

/**
  * Services connecting to Tickets.com's ProVenue API.
  *
  * Information around general API usage: authentication, headers, etc.
  * @see https://developer.provenue.com/sites/default/files/PV_DC_Common_V2_API_Features.pdf
  *
  * @param config ProVenue Service Configuration
  * @param sttpBackend sttp backend
  */
class ProVenueService(
  private val config: ProVenueConfig,
  val sttpBackend: SttpBackend[Future, Nothing] = new SttpLoggingBackend(
    OkHttpFutureBackend(),
    "Provenue",
    Seq("PV-API-Key", "PV-Application-ID", "User-Agent")
  )
) extends ProVenuePricePointBulkResponse.format
    with ProVenuePriceStructureResponse.format
    with ProVenuePricePointBulkRequest.format
    with ProVenueLockSeatsResponse.format
    with ProVenueLockSeatsRequest.format
    with ProVenuePrintResponse.format
    with ProVenuePrintRequest.format
    with ProVenueCheckoutRequest.format
    with ProVenueCheckoutResponse.format
    with ProVenuePingResponse.format
    with ProVenueBuyerTypesResponse.format {

  // Overloaded constructor with defaults
  def this() {
    this(
      ProVenueConfig(
        "agent",
        "apiKey",
        "appId",
        "https://baseUrl",
        "username",
        "password",
        1,
        None
      ),
      new SttpLoggingBackend(
        OkHttpFutureBackend(),
        "Provenue",
        Seq("PV-API-Key", "PV-Application-ID", "User-Agent")
      )
    )
  }

  implicit val backend: SttpBackend[Future, Nothing] = sttpBackend

  // Store eTag (last updated value) by price structure ids
  private var priceStructureETags: Map[Int, String] = Map()

  // Store eTag (last updated value) by cart id
  private var cartETags: Map[Int, String] = Map()
  private def getCartETagFromResponse(
    response: Response[ProVenueLockSeatsResponse]
  ): (Int, String) = {
    val cartId = response.body.toOption.map(_.id).getOrElse(-1)
    cartId -> response.header("ETag").getOrElse("")
  }

  private val ipFuture = IpService.getMyInternalIP

  private def req(forwarded: Option[String] = None) = {
    (if (forwarded.isDefined) Future.successful(forwarded) else ipFuture)
      .map(xff => {
        sttp.auth
          .basic(config.username, config.password)
          .headers(
            Map(
              "Content-Type" -> "application/json",
              "Accept" -> "application/json",
              "Accept-Encoding" -> "gzip",
              "PV-API-Key" -> config.apiKey,
              "PV-Application-ID" -> config.appId,
              "User-Agent" -> config.agent
            ) ++ config.sandboxEmail.map(value => "PV-Sandbox-Email-Address" -> value)
              ++ xff.map(value => "X-Forwarded-For" -> value)
          )
      })
  }

  /**
    * GET /test/ping
    */
  def ping(): Future[Response[ProVenuePingResponse]] = {
    req().flatMap(
      _.get(uri"${config.baseUrl}/test/ping")
        .response(asProVenuePingResponse)
        .send()
    )
  }

  /**
    * GET /priceStructures/?eventId={eventId}&saleType={saleType}
    */
  def getPriceStructure(
    eventId: Int,
    saleType: String
  ): Future[Response[ProVenuePriceStructureResponse]] = {
    req().flatMap(
      _.get(uri"${config.baseUrl}/priceStructures?eventId=$eventId&saleType=$saleType")
        .response(asProVenuePriceStructureResponse)
        .send()
        .andThen {
          case Success(response) if response.isSuccess =>
            val priceStructureId = response.body.map(_.priceStructure.id).getOrElse(-1)
            priceStructureETags += (priceStructureId -> response.header("ETag").getOrElse(""))
        }
    )
  }

  /**
    * POST /priceStructures/{priceStructureId}/prices
    */
  def bulkUpdatePricePoints(
    priceStructureId: Int,
    pricePointActions: Seq[ProVenuePricePointAction]
  ): Future[Response[ProVenuePricePointBulkResponse]] = {
    req().flatMap(
      _.header("If-Match", priceStructureETags.getOrElse(priceStructureId, ""))
        .post(uri"${config.baseUrl}/priceStructures/$priceStructureId/prices")
        .body(ProVenuePricePointBulkRequest(returnSummaryOnly = false, pricePointActions))
        .response(asProVenuePricePointBulkResponse)
        .send()
        .andThen {
          case Success(response) if response.isSuccess =>
            priceStructureETags += (priceStructureId -> response.header("ETag").getOrElse(""))
        }
    )
  }

  /**
    * POST /carts/lock
    */
  def lockSeats(
    eventId: Int,
    buyerTypeId: Int,
    seatIds: Seq[Int]
  ): Future[Response[ProVenueLockSeatsResponse]] = {
    req()
      .flatMap(
        _.post(uri"${config.baseUrl}/carts/lock")
          .body(ProVenueLockSeatsRequest(ProVenueLockSeats(eventId, buyerTypeId, seatIds)))
          .response(asProVenueLockSeatsResponse)
          .send()
          .andThen {
            case Success(response) if response.isSuccess =>
              cartETags += getCartETagFromResponse(response)
          }
      )
  }

  /**
    * DELETE /carts/{id}
    */
  def cancelCart(cartId: Int): Future[Response[ProVenueLockSeatsResponse]] = {
    req().flatMap(
      _.header("If-Match", cartETags.getOrElse(cartId, ""))
        .delete(uri"${config.baseUrl}/carts/$cartId")
        .response(asProVenueLockSeatsResponse)
        .send()
        .andThen {
          case Success(response) if response.isSuccess =>
            cartETags += getCartETagFromResponse(response)
        }
    )
  }

  /**
    * POST /carts/{id}/checkout
    */
  def checkout(
    cartId: Int,
    amount: ProVenueMoney,
    deliveryMethodId: Int,
    paymentMethodId: Int,
    patronAccountId: Int
  ): Future[Response[ProVenueCheckoutResponse]] = {
    val body = ProVenueCheckoutRequest(
      ProVenueCheckoutRequestCart(
        cartId,
        amount,
        Seq(ProVenueCheckoutRequestDelivery(deliveryMethodId, ProVenuePerson("ED", "ED"))),
        Seq(ProVenuePayment(paymentMethodId, amount)),
        patronAccountId
      )
    )

    req().flatMap(
      _.header("If-Match", cartETags.getOrElse(cartId, ""))
        .post(uri"${config.baseUrl}/carts/$cartId/checkout")
        .body(body)
        .response(asProVenueCheckoutResponse)
        .send()
    )
  }

  /**
    * POST /print
    */
  def print(deliveryId: Int, reissue: Boolean = false): Future[Response[ProVenuePrintResponse]] = {
    val action = if (reissue) "REISSUE_PRINT" else "PRINT"
    val body = ProVenuePrintRequest(action, deliveryId)

    req().flatMap(
      _.post(uri"${config.baseUrl}/print")
        .response(asProVenuePrintResponse)
        .body(body)
        .send()
    )
  }
}
