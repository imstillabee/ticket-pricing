package com.eventdynamic.provenue.models

import play.api.libs.json.{Format, Json}

case class ProVenueBuyerType(id: Int, code: String, active: Boolean, publicDescription: String)

object ProVenueBuyerType {

  trait format {
    implicit lazy val buyerTypeFormat: Format[ProVenueBuyerType] = Json.format[ProVenueBuyerType]
  }
}
