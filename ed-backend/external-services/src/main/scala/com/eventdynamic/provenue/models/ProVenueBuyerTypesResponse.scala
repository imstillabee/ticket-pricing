package com.eventdynamic.provenue.models

import com.softwaremill.sttp.{asString, ResponseAs}
import play.api.libs.json.{JsPath, Json, Reads}

case class ProVenueBuyerTypesResponse(buyerTypes: Seq[ProVenueBuyerType])

object ProVenueBuyerTypesResponse {

  trait format extends ProVenueBuyerType.format {
    implicit lazy val proVenueBuyerTypesResponseReads: Reads[ProVenueBuyerTypesResponse] =
      (JsPath \ "buyerTypesResponse" \ "buyerTypes")
        .read[Seq[ProVenueBuyerType]]
        .map(ProVenueBuyerTypesResponse.apply)

    def asProVenueBuyerTypesResponse: ResponseAs[ProVenueBuyerTypesResponse, Nothing] =
      asString.map(Json.parse(_).validate[ProVenueBuyerTypesResponse].get)
  }
}
