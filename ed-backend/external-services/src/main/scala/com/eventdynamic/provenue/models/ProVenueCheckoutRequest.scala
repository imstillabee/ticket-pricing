package com.eventdynamic.provenue.models

import com.softwaremill.sttp.{BodySerializer, StringBody}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Json}

case class ProVenueCheckoutRequest(cart: ProVenueCheckoutRequestCart)

case class ProVenueCheckoutRequestCart(
  id: Int,
  totalSalesRevenue: ProVenueMoney,
  deliveries: Seq[ProVenueCheckoutRequestDelivery],
  payments: Seq[ProVenuePayment],
  transactionPatronAccountId: Int
)

case class ProVenueCheckoutRequestDelivery(deliveryMethodId: Int, person: ProVenuePerson)
case class ProVenuePerson(firstName: String, lastName: String)

case class ProVenuePayment(paymentMethodId: Int, amount: ProVenueMoney)

object ProVenueCheckoutRequest {

  trait format extends ProVenueMoney.format {
    implicit lazy val personFormat: Format[ProVenuePerson] = Json.format[ProVenuePerson]

    implicit lazy val deliveryFormat: Format[ProVenueCheckoutRequestDelivery] = (
      (JsPath \ "deliveryMethod" \ "id").format[Int] and
        (JsPath \ "person").format[ProVenuePerson]
    )(ProVenueCheckoutRequestDelivery.apply, unlift(ProVenueCheckoutRequestDelivery.unapply))

    implicit lazy val paymentFormat: Format[ProVenuePayment] = (
      (JsPath \ "paymentMethod" \ "id").format[Int] and
        (JsPath \ "amount").format[ProVenueMoney]
    )(ProVenuePayment.apply, unlift(ProVenuePayment.unapply))

    implicit lazy val checkoutRequestCartFormat: Format[ProVenueCheckoutRequestCart] = (
      (JsPath \ "id").format[Int] and
        (JsPath \ "totalSalesRevenue").format[ProVenueMoney] and
        (JsPath \ "deliveries").format[Seq[ProVenueCheckoutRequestDelivery]] and
        (JsPath \ "payments").format[Seq[ProVenuePayment]] and
        (JsPath \ "transactionPatronAccount" \ "id").format[Int]
    )(ProVenueCheckoutRequestCart.apply, unlift(ProVenueCheckoutRequestCart.unapply))

    implicit lazy val checkoutRequestFormat: Format[ProVenueCheckoutRequest] =
      Json.format[ProVenueCheckoutRequest]

    implicit val checkoutRequestSerializer: BodySerializer[ProVenueCheckoutRequest] = {
      request: ProVenueCheckoutRequest =>
        val serialized = Json.toJson(request).toString()
        StringBody(serialized, "UTF-8", Some("application/json"))
    }
  }
}
