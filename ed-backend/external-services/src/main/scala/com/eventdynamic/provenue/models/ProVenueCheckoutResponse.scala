package com.eventdynamic.provenue.models

import play.api.libs.functional.syntax._
import com.softwaremill.sttp.{asString, ResponseAs}
import play.api.libs.json.{Format, JsPath, Json}

case class ProVenueCheckoutResponse(cart: ProVenueCheckoutResponseCart)

case class ProVenueCheckoutResponseCart(
  transactionId: Int,
  deliveries: Seq[ProVenueCheckoutResponseDelivery]
)

case class ProVenueCheckoutResponseDelivery(id: Int, status: String)

object ProVenueCheckoutResponse {

  trait format {
    implicit val checkoutResponseDeliveryFormat: Format[ProVenueCheckoutResponseDelivery] =
      Json.format[ProVenueCheckoutResponseDelivery]

    implicit lazy val checkoutResponseCartFormat: Format[ProVenueCheckoutResponseCart] = (
      (JsPath \ "transaction" \ "id").format[Int] and
        (JsPath \ "reference" \ "deliveries").format[Seq[ProVenueCheckoutResponseDelivery]]
    )(ProVenueCheckoutResponseCart.apply, unlift(ProVenueCheckoutResponseCart.unapply))

    implicit lazy val checkoutResponseFormat: Format[ProVenueCheckoutResponse] =
      Json.format[ProVenueCheckoutResponse]

    def asProVenueCheckoutResponse: ResponseAs[ProVenueCheckoutResponse, Nothing] =
      asString.map(str => {
        val json = Json.parse(str)
        val validation = json.validate[ProVenueCheckoutResponse]
        validation.get
      })
  }
}
