package com.eventdynamic.provenue.models

import play.api.libs.json.{Json, Writes}

case class ProVenueLockSeats(
  eventId: Int,
  buyerType: Int,
  seatIds: Seq[Int],
  saleType: String = "SINGLE",
  transactionType: String = "SALE"
) {
  def numberOfSeats: Int = seatIds.length
}

object ProVenueLockSeats {

  trait format {
    implicit val lockSeatsWrites: Writes[ProVenueLockSeats] =
      (lockSeats: ProVenueLockSeats) =>
        Json.obj(
          "saleType" -> lockSeats.saleType,
          "transactionType" -> lockSeats.transactionType,
          "event" -> Json.obj("id" -> lockSeats.eventId),
          "numberOfSeats" -> lockSeats.numberOfSeats,
          "buyerTypeSeatsList" -> Json.arr(
            Json.obj(
              "buyerType" -> Json.obj("id" -> lockSeats.buyerType),
              "seats" -> lockSeats.seatIds.map(seatId => Json.obj("id" -> seatId))
            )
          )
      )
  }
}
