package com.eventdynamic.provenue.models

import com.softwaremill.sttp.{BodySerializer, StringBody}
import play.api.libs.json.{Json, Writes}

case class ProVenueLockSeatsRequest(lockSeats: ProVenueLockSeats)

object ProVenueLockSeatsRequest {

  trait format extends ProVenueLockSeats.format {
    implicit val lockSeatsRequestWrites: Writes[ProVenueLockSeatsRequest] =
      Json.writes[ProVenueLockSeatsRequest]

    implicit val lockSeatsRequestSerializer: BodySerializer[ProVenueLockSeatsRequest] = {
      request: ProVenueLockSeatsRequest =>
        val serialized = Json.toJson(request).toString()
        StringBody(serialized, "UTF-8", Some("application/json"))
    }
  }
}
