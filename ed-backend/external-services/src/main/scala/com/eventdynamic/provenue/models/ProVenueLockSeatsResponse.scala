package com.eventdynamic.provenue.models

import com.softwaremill.sttp.{asString, ResponseAs}
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, Reads}

case class ProVenueLockSeatsResponse(
  id: Int,
  totalSalesRevenue: ProVenueMoney,
  offers: Seq[ProVenueLockSeatsResponseOffer],
  buyerTypeRefs: Seq[ProVenueLockSeatsResponseBuyerType]
)

case class ProVenueLockSeatsResponseBuyerType(id: Int, code: String)

case class ProVenueLockSeatsResponseOffer(
  id: Int,
  lineItems: Seq[ProVenueLockSeatsResponseLineItem]
)

case class ProVenueLockSeatsResponseLineItem(
  tickets: Seq[ProVenueLockSeatsResponseTicket],
  eventId: String
)

case class ProVenueLockSeatsResponseTicket(
  id: Int,
  seatId: Int,
  price: ProVenueMoney,
  buyerTypeRefIndex: Int
)

object ProVenueLockSeatsResponse {

  trait format extends ProVenueMoney.format {
    implicit lazy val lockSeatsResponseBuyerTypeReads: Reads[ProVenueLockSeatsResponseBuyerType] = (
      (JsPath \ "id").read[Int] and
        (JsPath \ "code").read[String]
    )(ProVenueLockSeatsResponseBuyerType.apply _)

    implicit lazy val lockSeatsResponseTicketReads: Reads[ProVenueLockSeatsResponseTicket] = (
      (JsPath \ "id").read[Int] and
        (JsPath \ "seat" \ "id").read[Int] and
        (JsPath \ "price").read[ProVenueMoney] and
        (JsPath \ "buyerType" \ "refIndex").read[Int]
    )(ProVenueLockSeatsResponseTicket.apply _)

    implicit lazy val lockSeatsResponseOfferReads: Reads[ProVenueLockSeatsResponseOffer] = (
      (JsPath \ "id").read[Int] and
        (JsPath \ "lineItems").read[Seq[ProVenueLockSeatsResponseLineItem]]
    )(ProVenueLockSeatsResponseOffer.apply _)

    implicit lazy val lockSeatsResponseLineItemReads: Reads[ProVenueLockSeatsResponseLineItem] = (
      (JsPath \ "tickets").read[Seq[ProVenueLockSeatsResponseTicket]] and
        (JsPath \ "event" \ "refKey").read[String]
    )(ProVenueLockSeatsResponseLineItem.apply _)

    implicit lazy val lockSeatsResponseReads: Reads[ProVenueLockSeatsResponse] = (
      (JsPath \ "cart" \ "id").read[Int] and
        (JsPath \ "cart" \ "totalSalesRevenue").read[ProVenueMoney] and
        (JsPath \ "cart" \ "offers").read[Seq[ProVenueLockSeatsResponseOffer]] and
        (JsPath \ "cart" \ "reference" \ "buyerTypes")
          .read[Seq[ProVenueLockSeatsResponseBuyerType]]
    )(ProVenueLockSeatsResponse.apply _)

    def asProVenueLockSeatsResponse: ResponseAs[ProVenueLockSeatsResponse, Nothing] =
      asString.map(Json.parse(_).validate[ProVenueLockSeatsResponse].get)
  }
}
