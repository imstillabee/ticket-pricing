package com.eventdynamic.provenue.models

import play.api.libs.json.{Format, Json}

case class ProVenueMoney(currencyCode: String, value: BigDecimal)

object ProVenueMoney {

  trait format {
    implicit lazy val moneyFormat: Format[ProVenueMoney] = Json.format
  }
}
