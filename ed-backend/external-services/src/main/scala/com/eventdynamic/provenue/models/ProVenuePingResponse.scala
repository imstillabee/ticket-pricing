package com.eventdynamic.provenue.models

import com.softwaremill.sttp.{asString, ResponseAs}
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, Reads}

case class ProVenuePingResponse(
  clientId: String,
  proVenueAppVersion: String,
  currentUTC: String,
  timeZoneCode: String
)

object ProVenuePingResponse {

  trait format {
    implicit lazy val pingResponseFormat: Reads[ProVenuePingResponse] = (
      (JsPath \ "ping" \ "clientId").read[String] and
        (JsPath \ "ping" \ "proVenueAppVersion").read[String] and
        (JsPath \ "ping" \ "currentUTC").read[String] and
        (JsPath \ "ping" \ "timeZoneCode").read[String]
    )(ProVenuePingResponse.apply _)

    def asProVenuePingResponse: ResponseAs[ProVenuePingResponse, Nothing] =
      asString.map(Json.parse(_).validate[ProVenuePingResponse].get)
  }
}
