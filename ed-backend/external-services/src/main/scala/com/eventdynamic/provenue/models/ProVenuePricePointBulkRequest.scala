package com.eventdynamic.provenue.models

import com.softwaremill.sttp.{BodySerializer, StringBody}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Json, Writes}

case class ProVenuePricePointBulkRequest(
  returnSummaryOnly: Boolean,
  pricePoints: Seq[ProVenuePricePointAction]
)

case class ProVenuePricePointAction(
  id: Int,
  action: String,
  buyerTypeId: Int,
  priceScaleId: Int,
  price: ProVenueMoney
)

object ProVenuePricePointBulkRequest {

  trait format extends ProVenueMoney.format {
    implicit val pricePointActionFormat: Format[ProVenuePricePointAction] = (
      (JsPath \ "id").format[Int] and
        (JsPath \ "action").format[String] and
        (JsPath \ "buyerType" \ "id").format[Int] and
        (JsPath \ "priceScale" \ "id").format[Int] and
        (JsPath \ "price").format[ProVenueMoney]
    )(ProVenuePricePointAction.apply, unlift(ProVenuePricePointAction.unapply))

    implicit val pricePointBulkRequestFormat: Writes[ProVenuePricePointBulkRequest] = (
      (JsPath \ "priceActions" \ "returnSummaryOnly").write[Boolean] and
        (JsPath \ "priceActions" \ "pricePoints").write[Seq[ProVenuePricePointAction]]
    )(unlift(ProVenuePricePointBulkRequest.unapply))

    implicit val pricePointBulkRequestSerializer: BodySerializer[ProVenuePricePointBulkRequest] = {
      request: ProVenuePricePointBulkRequest =>
        val serialized = Json.toJson(request).toString()
        StringBody(serialized, "UTF-8", Some("application/json"))
    }
  }
}
