package com.eventdynamic.provenue.models

import com.softwaremill.sttp.{asString, ResponseAs}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Json}

case class ProVenuePricePointBulkResponse(
  requestCount: Int,
  errorCount: Int,
  createdCount: Int,
  notProcessedCount: Int,
  updatedCount: Int,
  deletedCount: Int,
  clearedCount: Int,
  unmodifiedCount: Int
)

object ProVenuePricePointBulkResponse {

  trait format {
    implicit val pricePointsBulkResponseFormat: Format[ProVenuePricePointBulkResponse] = (
      (JsPath \ "priceActions" \ "requestCount").format[Int] and
        (JsPath \ "priceActions" \ "errorCount").format[Int] and
        (JsPath \ "priceActions" \ "createdCount").format[Int] and
        (JsPath \ "priceActions" \ "notProcessedCount").format[Int] and
        (JsPath \ "priceActions" \ "updatedCount").format[Int] and
        (JsPath \ "priceActions" \ "deletedCount").format[Int] and
        (JsPath \ "priceActions" \ "clearedCount").format[Int] and
        (JsPath \ "priceActions" \ "unmodifiedCount").format[Int]
    )(ProVenuePricePointBulkResponse.apply, unlift(ProVenuePricePointBulkResponse.unapply))

    def asProVenuePricePointBulkResponse: ResponseAs[ProVenuePricePointBulkResponse, Nothing] =
      asString.map(Json.parse(_).validate[ProVenuePricePointBulkResponse].get)
  }
}
