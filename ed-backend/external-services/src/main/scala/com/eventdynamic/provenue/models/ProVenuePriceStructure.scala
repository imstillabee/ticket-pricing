package com.eventdynamic.provenue.models

import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Json}

case class ProVenuePriceStructure(
  id: Int,
  code: String,
  pricePoints: Seq[ProVenuePricePoint],
  buyerTypeRefs: Seq[ProVenueBuyerType],
  priceScaleRefs: Seq[ProVenuePriceScale]
)

case class ProVenuePricePoint(
  id: Int,
  price: ProVenueMoney,
  priceScaleRefIndex: Int,
  buyerTypeRefIndex: Int
)

case class ProVenuePriceScale(id: Int, code: String)

object ProVenuePriceStructure {

  trait format extends ProVenueMoney.format with ProVenueBuyerType.format {
    implicit val pricePointFormat: Format[ProVenuePricePoint] = (
      (JsPath \ "id").format[Int] and
        (JsPath \ "price").format[ProVenueMoney] and
        (JsPath \ "priceScale" \ "refIndex").format[Int] and
        (JsPath \ "buyerType" \ "refIndex").format[Int]
    )(ProVenuePricePoint.apply, unlift(ProVenuePricePoint.unapply))

    implicit val priceScaleFormat: Format[ProVenuePriceScale] = Json.format[ProVenuePriceScale]

    implicit val priceStructureFormat: Format[ProVenuePriceStructure] = (
      (JsPath \ "id").format[Int] and
        (JsPath \ "code").format[String] and
        (JsPath \ "pricePoints").format[Seq[ProVenuePricePoint]] and
        (JsPath \ "reference" \ "buyerTypes").format[Seq[ProVenueBuyerType]] and
        (JsPath \ "reference" \ "priceScales").format[Seq[ProVenuePriceScale]]
    )(ProVenuePriceStructure.apply, unlift(ProVenuePriceStructure.unapply))
  }
}
