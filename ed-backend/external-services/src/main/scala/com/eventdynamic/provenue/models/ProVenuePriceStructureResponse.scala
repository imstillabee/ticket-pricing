package com.eventdynamic.provenue.models

import com.softwaremill.sttp.{asString, ResponseAs}
import play.api.libs.json.{Json, Reads}

case class ProVenuePriceStructureResponse(priceStructure: ProVenuePriceStructure)

object ProVenuePriceStructureResponse {

  trait format extends ProVenuePriceStructure.format {
    implicit val priceStructureResponseReads: Reads[ProVenuePriceStructureResponse] =
      Json.reads[ProVenuePriceStructureResponse]

    def asProVenuePriceStructureResponse: ResponseAs[ProVenuePriceStructureResponse, Nothing] =
      asString.map(Json.parse(_).validate[ProVenuePriceStructureResponse].get)
  }
}
