package com.eventdynamic.provenue.models

import com.softwaremill.sttp.{BodySerializer, StringBody}
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, Writes}

case class ProVenuePrintRequest(printAction: String, deliveryId: Int)

object ProVenuePrintRequest {

  trait format {
    implicit val printRequestWrites: Writes[ProVenuePrintRequest] = (
      (JsPath \ "printRequest" \ "printAction").write[String] and
        (JsPath \ "printRequest" \ "deliveryId").write[Int]
    )(unlift(ProVenuePrintRequest.unapply))

    implicit val proVenuePricePointBulkRequest: BodySerializer[ProVenuePrintRequest] = {
      request: ProVenuePrintRequest =>
        val serialized = Json.toJson(request).toString()
        StringBody(serialized, "UTF-8", Some("application/json"))
    }
  }
}
