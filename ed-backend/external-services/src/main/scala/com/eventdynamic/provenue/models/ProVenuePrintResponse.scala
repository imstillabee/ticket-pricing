package com.eventdynamic.provenue.models

import com.softwaremill.sttp.{asString, ResponseAs}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Json, Reads}

case class ProVenuePrintResponse(
  numberOfTicketsPrinted: Int,
  numberOfTicketsRequestedToPrint: Int,
  numberOfPrintItems: Int,
  printAction: String,
  printItems: Seq[ProVenuePrintItem],
  buyerTypeRefs: Seq[ProVenuePrintResponseBuyerType],
  deliveryRefs: Seq[ProVenuePrintResponseDelivery]
)

case class ProVenuePrintResponseBuyerType(id: Int, code: String)
case class ProVenuePrintResponseDelivery(id: Int, transactionId: Int, status: String)

case class ProVenuePrintItem(deliveryRefIndex: Int, eventId: Int, ticket: ProVenueTicket)

case class ProVenueTicket(
  id: Int,
  price: ProVenueMoney,
  priceScaleId: Int,
  buyerTypeRefIndex: Int,
  seatId: Int,
  barcode: String
)

object ProVenuePrintResponse {

  trait format extends ProVenueMoney.format {
    implicit lazy val printResponseBuyerTypeFormat: Format[ProVenuePrintResponseBuyerType] =
      Json.format[ProVenuePrintResponseBuyerType]

    implicit lazy val printResponseDelivery: Format[ProVenuePrintResponseDelivery] = (
      (JsPath \ "id").format[Int] and
        (JsPath \ "transaction" \ "id").format[Int] and
        (JsPath \ "status").format[String]
    )(ProVenuePrintResponseDelivery.apply, unlift(ProVenuePrintResponseDelivery.unapply))

    implicit lazy val ticketFormat: Reads[ProVenueTicket] = (
      (JsPath \ "id").read[Int] and
        (JsPath \ "price").read[ProVenueMoney] and
        (JsPath \ "priceScale" \ "refKey").read[String].map(_.toInt) and
        (JsPath \ "buyerType" \ "refIndex").read[Int] and
        (JsPath \ "seat" \ "id").read[Int] and
        (JsPath \ "barcode").read[String]
    )(ProVenueTicket.apply _)

    implicit lazy val printItemFormat: Reads[ProVenuePrintItem] = (
      (JsPath \ "delivery" \ "refIndex").read[Int] and
        (JsPath \ "event" \ "refKey").read[String].map(_.toInt) and
        (JsPath \ "ticket").read[ProVenueTicket]
    )(ProVenuePrintItem.apply _)

    implicit lazy val printResponseReads: Reads[ProVenuePrintResponse] = (
      (JsPath \ "printResponse" \ "numberOfTicketsPrinted").read[Int] and
        (JsPath \ "printResponse" \ "numberOfTicketsRequestedToPrint").read[Int] and
        (JsPath \ "printResponse" \ "numberOfPrintItems").read[Int] and
        (JsPath \ "printResponse" \ "printAction").read[String] and
        (JsPath \ "printResponse" \ "printItems").read[Seq[ProVenuePrintItem]] and
        (JsPath \ "printResponse" \ "reference" \ "buyerTypes")
          .read[Seq[ProVenuePrintResponseBuyerType]] and
        (JsPath \ "printResponse" \ "reference" \ "deliveries")
          .read[Seq[ProVenuePrintResponseDelivery]]
    )(ProVenuePrintResponse.apply _)

    def asProVenuePrintResponse: ResponseAs[ProVenuePrintResponse, Nothing] =
      asString.map(str => {
        val json = Json.parse(str)
        val valid = json.validate[ProVenuePrintResponse]
        valid.get
      })
  }
}
