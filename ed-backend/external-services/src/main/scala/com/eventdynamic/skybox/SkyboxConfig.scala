package com.eventdynamic.skybox

case class SkyboxConfig(appKey: String, baseUrl: String, apiKey: String, accountId: Int)
