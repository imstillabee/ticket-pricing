package com.eventdynamic.skybox

import java.time.Instant

import com.eventdynamic.skybox.models.SkyboxFormats._
import com.eventdynamic.skybox.models.SkyboxSerializers._
import com.eventdynamic.skybox.models._
import com.eventdynamic.utils.JsonUtil
import com.eventdynamic.{SttpHelper, SttpLoggingBackend}
import com.softwaremill.sttp._
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import org.slf4j.LoggerFactory
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class SkyboxService(
  private val config: SkyboxConfig,
  private val sttpBackend: SttpBackend[Future, Nothing] = new SttpLoggingBackend(
    OkHttpFutureBackend(),
    "Skybox",
    Seq("X-Application-Token", "X-Api-Token", "X-Account")
  )
) {
  implicit val backend: SttpBackend[Future, Nothing] = sttpBackend

  private val logger = LoggerFactory.getLogger(this.getClass)

  // Expose for services
  val accountId: Int = config.accountId

  // Base request
  private val req = sttp.headers(
    "Content-Type" -> "application/json",
    "X-Application-Token" -> config.appKey,
    "X-Api-Token" -> config.apiKey,
    "X-Account" -> accountId.toString
  )

  /**
    * PUT /inventory
    *
    * @return true if 200 was returned
    */
  def bulkUpdatePrices(prices: Seq[SkyboxBulkPriceItem]): Future[Boolean] = {
    logger.info(s"Calling Skybox Bulk Update Endpoint: ${config.baseUrl}/inventory")
    logger.info(s"Pushing prices: ${JsonUtil.stringify(prices)}")
    req
      .put(uri"${config.baseUrl}/inventory")
      .body(prices)
      .send()
      .andThen {
        case Success(response) => logger.info(s"SkyBox response: $response", response)
        case Failure(e)        => logger.error(s"Error from SkyBox: $e", e)
      }
      .map(r => r.isSuccess)
  }

  /**
    * GET /inventory
    *
    * Gets inventory based on passed search parameters.
    *
    * @param eventIds eventIds to filter on
    */
  def getInventory(eventIds: Seq[Int]): Future[Response[SkyboxInventoryGetResponse]] = {
    val queryParams = eventIds.map(eventId => ("eventId", eventId))

    req
      .get(uri"${config.baseUrl}/inventory?$queryParams")
      .response(SttpHelper.asJson[SkyboxInventoryGetResponse])
      .send()
  }

  /**
    * GET /inventory
    *
    * Gets inventory based on passed search parameters.
    *
    * @param eventIds eventIds to filter on
    * @param lastUpdateFrom fetch records updated after this date
    * @param lastUpdateTo fetch records updated before this date
    */
  def getInventory(
    eventIds: Seq[Int],
    lastUpdateFrom: Instant,
    lastUpdateTo: Instant
  ): Future[Response[SkyboxInventoryGetResponse]] = {
    val queryParams = eventIds.map("eventId" -> _) ++ Map(
      "lastUpdateFrom" -> lastUpdateFrom,
      "lastUpdateTo" -> lastUpdateTo
    )

    req
      .get(uri"${config.baseUrl}/inventory?$queryParams")
      .response(SttpHelper.asJson[SkyboxInventoryGetResponse])
      .send()
  }

  /**
    * GET /inventory
    *
    * Gets inventory listings for a given row
    *
    * @param eventId eventId to filter by
    * @param section section to filter by
    * @param row row to filter by
    * @return a list of listings on that row
    */
  def getInventoryForRow(
    eventId: Int,
    section: String,
    row: String
  ): Future[Response[SkyboxPage[SkyboxListing]]] = {
    val queryParams = Map("eventId" -> eventId, "section" -> section, "row" -> row)

    req
      .get(uri"${config.baseUrl}/inventory?$queryParams")
      .response(SttpHelper.asJson[SkyboxPage[SkyboxListing]])
      .send()
  }

  /**
    * GET /inventory/sold
    *
    * Gets sold inventory based on passed search parameters.
    *
    * @param eventIds eventIds to filter on
    * @param invoiceDateFrom earliest invoice date
    * @param invoiceDateTo latest invoice date
    * @return Sttp response of sold inventory
    */
  def getSoldInventory(
    eventIds: Seq[Int],
    invoiceDateFrom: Instant,
    invoiceDateTo: Instant
  ): Future[Response[SkyboxInventoryGetResponse]] = {
    val queryParams = eventIds.map("eventId" -> _) ++ Map(
      "invoiceDateFrom" -> invoiceDateFrom,
      "invoiceDateTo" -> invoiceDateTo
    )

    req
      .get(uri"${config.baseUrl}/inventory/sold?$queryParams")
      .response(SttpHelper.asJson[SkyboxInventoryGetResponse])
      .send()
  }

  /**
    * GET /inventory
    *
    * Get sold inventory with matching inventory ids
    *
    * @param inventoryIds inventory ids to select on
    * @return Skybox listings
    */
  def getInventoryWithIds(inventoryIds: Seq[Int]): Future[Response[SkyboxInventoryGetResponse]] = {
    val queryParams = inventoryIds.map("inventoryId" -> _)

    req
      .get(uri"${config.baseUrl}/inventory?$queryParams")
      .response(SttpHelper.asJson[SkyboxInventoryGetResponse])
      .send()
  }

  /**
    * GET /inventory/sold
    *
    * Get sold inventory with matching inventory ids
    *
    * @param inventoryIds inventory ids to select on
    * @return Skybox listings
    */
  def getSoldInventoryWithIds(
    inventoryIds: Seq[Int]
  ): Future[Response[SkyboxInventoryGetResponse]] = {
    val queryParams = inventoryIds.map("inventoryId" -> _)

    req
      .get(uri"${config.baseUrl}/inventory/sold?$queryParams")
      .response(SttpHelper.asJson[SkyboxInventoryGetResponse])
      .send()
  }

  /**
    * GET /inventory/:id
    *
    * Get a single inventory item
    *
    * @param id inventory id
    * @return inventory item
    */
  def getInventoryById(id: Int): Future[Response[SkyboxListing]] = {
    req
      .get(uri"${config.baseUrl}/inventory/$id")
      .response(SttpHelper.asJson[SkyboxListing])
      .send()
  }

  /**
    * GET /inventory/sold
    *
    * Gets purchases based on passed search parameters
    *
    * @param eventIds Only fetch records matching these skybox event ids
    * @param afterDate Only fetch invoices created after this date
    * @return Skybox invoices
    */
  // TODO: refactor this to be getSoldInventory
  def getInvoices(
    eventIds: Seq[Int],
    afterDate: Instant,
    beforeDate: Instant = Instant.now()
  ): Future[Response[SkyboxSoldInventoryGetResponse]] = {
    val eventIdMap = eventIds.map("eventId" -> _)
    val queryParams = eventIdMap ++ Map(
      "invoiceDateFrom" -> afterDate,
      "invoiceDateTo" -> beforeDate,
      "sortBy" -> "INVOICE_DATE",
      "sortDir" -> "ASC"
    )

    req
      .get(uri"${config.baseUrl}/inventory/sold?$queryParams")
      .response(SttpHelper.asJson[SkyboxSoldInventoryGetResponse])
      .send()
  }

  /**
    * GET /invoices/:invoiceId/lines/:lineId/tickets
    *
    * Get tickets for an invoice line item
    *
    * @param invoiceId Invoice ID to filter by
    * @param lineId Line ID to filter by
    * @return List of Skybox tickets
    */
  def getInvoiceTickets(invoiceId: Int, lineId: Int): Future[Response[Seq[SkyboxTicket]]] = {
    req
      .get(uri"${config.baseUrl}/invoices/$invoiceId/lines/$lineId/tickets")
      .response(SttpHelper.asJson[Seq[SkyboxTicket]])
      .send()
  }

  /**
    * GET /tickets
    *
    * Get tickets for the given filters
    *
    * @param eventId Skybox event ID to filter on
    * @param section Section to filter on
    * @param row Row to filter on
    * @param seatNumbers Seat numbers to filter on
    * @return List of Skybox tickets
    */
  def getTickets(
    eventId: Int,
    section: Option[String],
    row: Option[String],
    seatNumbers: Seq[String],
    start: Option[Instant],
    end: Option[Instant]
  ): Future[Response[Seq[SkyboxTicket]]] = {
    val params = Seq(
      "eventId" -> eventId,
      "section" -> section,
      "row" -> row,
      "lastUpdateFrom" -> start,
      "lastUpdateTo" -> end
    ).filter {
      case (_, value) => value != None
    } ++ seatNumbers
      .map("seatNumber" -> _)

    req
      .get(uri"${config.baseUrl}/tickets?$params")
      .response(SttpHelper.asJson[Seq[SkyboxTicket]])
      .send()
  }

  /**
    * POST /invoices
    *
    * @param customerId Customer of the order
    * @param amount Total amount for the purchase
    * @param ticketIds Ticket ids for the purchase
    * @return Created Skybox Invoice
    */
  def createInvoice(
    customerId: Int,
    amount: BigDecimal,
    ticketIds: Seq[Int]
  ): Future[Response[SkyboxInvoice]] = {
    val lineItem = Json.obj(
      "amount" -> amount,
      "itemIds" -> ticketIds,
      "lineItemType" -> "INVENTORY",
      "lineItem" -> "PURCHASE"
    )
    val body = Json.obj("customerId" -> customerId, "lines" -> Seq(lineItem))

    req
      .post(uri"${config.baseUrl}/invoices")
      .body(body.toString)
      .response(SttpHelper.asJson[SkyboxInvoice])
      .send()
  }
}
