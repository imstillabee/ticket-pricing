package com.eventdynamic.skybox.models

case class SkyboxBulkPriceItem(id: Int, accountId: Int, listPrice: BigDecimal)
