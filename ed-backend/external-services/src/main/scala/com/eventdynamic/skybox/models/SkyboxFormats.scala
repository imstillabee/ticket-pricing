package com.eventdynamic.skybox.models

import com.eventdynamic.utils.InstantISOFormat
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Json, OFormat}

object SkyboxFormats extends InstantISOFormat {
  implicit val skyboxBulkPriceItemFormat: Format[SkyboxBulkPriceItem] = (
    (JsPath \ "id").format[Int] and
      (JsPath \ "accountId").format[Int] and
      (JsPath \ "listPrice").format[BigDecimal]
  )(SkyboxBulkPriceItem.apply, unlift(SkyboxBulkPriceItem.unapply))

  implicit val skyboxListingFormat = Json.format[SkyboxListing]

  implicit val skyboxInvoiceLineFormat: OFormat[SkyboxInvoiceLine] = Json.format[SkyboxInvoiceLine]

  implicit val skyboxInvoiceFormat: OFormat[SkyboxInvoice] = Json.format[SkyboxInvoice]

  implicit val invoiceFormat: OFormat[SkyboxSoldInventory] = Json.format[SkyboxSoldInventory]

  implicit val skyboxTicketFormat = Json.format[SkyboxTicket]

  implicit def skyboxPageFormat[B](implicit formatB: Format[B]): OFormat[SkyboxPage[B]] =
    Json.format[SkyboxPage[B]]

  implicit val getResponseFormat: OFormat[SkyboxInventoryGetResponse] =
    Json.format[SkyboxInventoryGetResponse]

  implicit val getSkyboxInvoiceResponseFormat: Format[SkyboxSoldInventoryGetResponse] =
    Json.format[SkyboxSoldInventoryGetResponse]

}
