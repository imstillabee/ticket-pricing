package com.eventdynamic.skybox.models

case class SkyboxInventoryGetResponse(rows: Seq[SkyboxListing])
