package com.eventdynamic.skybox.models

case class SkyboxInvoice(id: Int, lines: Seq[SkyboxInvoiceLine])
