package com.eventdynamic.skybox.models

case class SkyboxInvoiceLine(
  id: Int,
  accountId: Option[Int],
  quantity: Option[Int],
  amount: BigDecimal,
  lineItemType: String, // GENERIC or INVENTORY
  inventoryIds: Seq[Int]
)
