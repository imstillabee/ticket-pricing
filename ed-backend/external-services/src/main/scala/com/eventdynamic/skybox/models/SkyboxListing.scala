package com.eventdynamic.skybox.models

case class SkyboxListing(
  id: Int,
  eventId: Int,
  section: String,
  row: String,
  listPrice: Option[BigDecimal],
  status: String,
  lowSeat: Int,
  highSeat: Int,
  unitCostAverage: Option[BigDecimal]
)
