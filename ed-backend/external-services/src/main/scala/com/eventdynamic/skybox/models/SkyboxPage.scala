package com.eventdynamic.skybox.models

case class SkyboxPage[B](rows: Seq[B], rowCount: Int)
