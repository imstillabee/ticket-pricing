package com.eventdynamic.skybox.models

import com.softwaremill.sttp.{BodySerializer, StringBody}
import play.api.libs.json.Json
import com.eventdynamic.skybox.models.SkyboxFormats._

object SkyboxSerializers {
  implicit val skyboxBulkPriceRequestSerializer: BodySerializer[Seq[SkyboxBulkPriceItem]] = {
    request: Seq[SkyboxBulkPriceItem] =>
      val serialized = Json.toJson(request).toString
      StringBody(serialized, "UTF-8", Some("application/json"))
  }
}
