package com.eventdynamic.skybox.models

import java.time.Instant

import com.eventdynamic.utils.InstantISOFormat
import play.api.libs.json.{Format, Json}

case class SkyboxSoldInventory(
  id: Int,
  invoiceDate: Instant,
  unitTicketSales: BigDecimal,
  section: String,
  row: String,
  seatNumbers: String,
  eventId: Int,
  customerId: Int
) {
  val seatNumbersList: Seq[Int] = seatNumbers.split(",").map(_.toInt)
}

object SkyboxSoldInventory {

  trait format extends InstantISOFormat {
    implicit val invoiceFormat: Format[SkyboxSoldInventory] = Json.format[SkyboxSoldInventory]
  }
}
