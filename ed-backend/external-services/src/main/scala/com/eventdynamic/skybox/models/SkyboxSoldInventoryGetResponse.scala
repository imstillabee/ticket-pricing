package com.eventdynamic.skybox.models

case class SkyboxSoldInventoryGetResponse(rows: Seq[SkyboxSoldInventory])
