package com.eventdynamic.skybox.models

import java.time.Instant

case class SkyboxTicket(
  id: Int,
  createdDate: Instant,
  lastUpdate: Instant,
  inventoryId: Int,
  invoiceLineId: Int,
  purchaseLineId: Int,
  status: String,
  eventId: Int,
  section: String,
  row: String,
  seatNumber: Int,
  cost: BigDecimal,
  faceValue: BigDecimal,
  sellPrice: BigDecimal,
  barCode: Option[String]
)
