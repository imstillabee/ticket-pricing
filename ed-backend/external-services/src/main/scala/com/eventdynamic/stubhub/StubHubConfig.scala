package com.eventdynamic.stubhub

case class StubHubConfig(
  baseUrl: String,
  consumerKey: String,
  consumerSecret: String,
  username: String,
  password: String
)
