package com.eventdynamic.stubhub

import java.sql.Timestamp
import java.text.SimpleDateFormat

import com.eventdynamic.stubhub.models._
import com.eventdynamic.utils.DateHelper
import com.eventdynamic.utils.DateParts
import com.softwaremill.sttp._
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import org.slf4j.LoggerFactory
import play.api.libs.json.Json
import java.util.UUID.randomUUID

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try

/** StubHub Service
  * TODO: add deserializer for api responses
  *
  * @param config
  * @param sttpBackend
  */
class StubHubService(
  private val config: StubHubConfig,
  val sttpBackend: SttpBackend[Future, Nothing] = OkHttpFutureBackend()
) {
  private val logger = LoggerFactory.getLogger(this.getClass)

  implicit val backend: SttpBackend[Future, Nothing] = sttpBackend

  private var access_token: Option[String] = None: Option[String]

  private val defaultHeaders = Map(
    "Content-Type" -> "application/json",
    "Accept" -> "application/json",
    "Accept-Encoding" -> "application/json",
    "Accept-Language" -> "en-US"
  )

  private val authRequest = sttp.auth
    .basic(config.consumerKey, config.consumerSecret)
    .headers("Content-Type" -> "application/json")

  private def request(access_token: String): RequestT[Empty, String, Nothing] = sttp.auth
    .bearer(access_token)
    .headers(defaultHeaders)

  private val dateFormat = new SimpleDateFormat("yyyy-MM-dd")

  /** Response Mapping of StubHub API
    *
    * @param res
    * @return
    */
  def responseMapping(res: Response[String]): ApiResponse = {
    if (res.isSuccess) {
      ApiSuccess(res.code, ApiStringBody(res.body.right.get))
    } else {
      ApiError(res.code, ApiStringBody(res.body.left.get))
    }
  }

  /** Error Handler for Api Responses from StubHub
    *
    * @param error
    * @return
    */
  def errorHandle(error: ApiResponse): ApiResponse = {
    error match {
      case ApiError(code, ApiStringBody(res)) =>
        val body = StubHubErrorResponse.fromResponse(res).getOrElse(ApiStringBody(res))
        ApiError(code, body)
      case err => err
    }
  }

  /**
    * POST /sellers/oauth/accesstoken
    */
  def login(): Future[ApiResponse] = {
    val body = Json.obj("username" -> config.username, "password" -> config.password)

    access_token match {
      case Some(token) => Future.successful(ApiSuccess(200, ApiStringBody(token)))
      case None =>
        authRequest
          .post(uri"${config.baseUrl}/oauth/accesstoken?grant_type=client_credentials")
          .body(body.toString())
          .send()
          .map(
            res =>
              responseMapping(res) match {
                case ApiSuccess(code, ApiStringBody(str)) =>
                  val body = Try(StubHubLoginResponse.fromResponse(str).get).toOption
                  access_token = Some(Try(body.get.access_token).toOption.getOrElse(""))
                  ApiSuccess(code, body.getOrElse(ApiStringBody(access_token.get)))
                case err =>
                  access_token = None
                  errorHandle(err)
            }
          )
    }
  }

  /**
    * PATCH /listings/v3/?{externalId}
    */
  def delist(tickets: Seq[StubHubTicket], listingId: Int): Future[ApiResponse] = {
    val body = Json.obj("products" -> tickets.map(ticket => {
      Map("row" -> ticket.row, "seat" -> ticket.seat, "operation" -> "DELETE")
    }))

    login()
      .flatMap {
        //TODO: Stop using ApiSuccess
        case ApiSuccess(_, StubHubLoginResponse(token, _)) =>
          request(token)
            .patch(uri"${config.baseUrl}/listings/v3/$listingId")
            .body(body.toString())
            .send
            .map(
              res =>
                responseMapping(res) match {
                  case ApiSuccess(code, ApiStringBody(str)) =>
                    val body =
                      StubHubListingResponse.fromResponse(str).getOrElse(ApiStringBody(str))
                    ApiSuccess(code, body)
                  case err => errorHandle(err)
              }
            )
      }
  }

  /**
    * POST /fulfillment/barcode/v1/order
    */
  def fulfill(
    orderId: Int,
    ticketsWithBarcodes: Seq[StubHubTicketWithBarcode]
  ): Future[ApiResponse] = {

    val body = Json.obj("products" -> ticketsWithBarcodes.map(ticket => {
      Map("row" -> ticket.row, "seat" -> ticket.seat, "barcode" -> ticket.barcode, "type" -> "R")
    }))

    login()
      .flatMap {
        //TODO: Stop using ApiSuccess
        case ApiSuccess(_, StubHubLoginResponse(token, _)) =>
          request(token)
            .post(uri"${config.baseUrl}/sales/barcode/v3/$orderId")
            .body(body.toString())
            .send
            .map(
              res =>
                responseMapping(res) match {
                  case ApiSuccess(code, ApiStringBody(str)) =>
                    val body =
                      StubHubFulfillmentResponse.fromResponse(str).getOrElse(ApiStringBody(str))
                    ApiSuccess(code, body)
                  case err => errorHandle(err)
              }
            )
      }

//    Future.successful(ApiSuccess(200, StubHubFulfillmentResponse("1", "success"))) //for testing
  }

  /**
    * GET sellers/listings/v3/
    */
  def getInventory(start: Int, eventId: Option[String] = None): Future[ApiResponse] = {
    val params = Map("start" -> start, "rows" -> 200) ++ eventId.map(
      eId => "filters" -> s"EVENT:$eId"
    )
    login()
      .flatMap {
        //TODO: Stop using ApiSuccess
        case ApiSuccess(_, StubHubLoginResponse(token, _)) =>
          request(token)
            .get(uri"${config.baseUrl}/listings/v3?$params")
            .send()
            .map(
              res =>
                responseMapping(res) match {
                  case ApiSuccess(code, ApiStringBody(str)) =>
                    val body =
                      StubHubAccountManagementResponse
                        .fromResponse(str)
                        .getOrElse(ApiStringBody(str))
                    ApiSuccess(code, body)
                  case err => errorHandle(err)
              }
            )
      }
  }

  /**
    * POST /listings/v3
    */
  def list(
    inventory: Seq[StubHubInventory],
    eventId: String,
    listedPrice: BigDecimal,
    inhandDate: Timestamp = DateHelper.now()
  ): Future[ApiResponse] = {
    val body =
      Json.obj(
        // TODO: make the externalListngId meaningful
        // Stubhub requires an externalListingId but we don't use it anywhere during fulfillment right
        // now, so we give it a random UUID
        "externalListingId" -> randomUUID().toString,
        "eventId" -> eventId,
        "pricePerProduct" -> Json.obj("amount" -> listedPrice, "currency" -> "USD"),
        "section" -> inventory.head.section,
        "products" -> inventory.map(
          i =>
            Json.obj(
              "row" -> i.row,
              "seat" -> i.seat,
              "operation" -> "ADD",
              "externalId" -> s"${i.id}",
              "productType" -> "TICKET"
          )
        ),
        "paymentType" -> 1, // PayPal
        "stockTypes" -> Seq(
          Json.obj(
            "stockType" -> "BARCODE",
            "inhandDate" -> dateFormat.format(inhandDate),
            "adjustInhandDate" -> true
          )
        )
      )

    login()
      .flatMap {
        //TODO: Stop using ApiSuccess
        case ApiSuccess(_, StubHubLoginResponse(token, _)) =>
          request(token)
            .post(uri"${config.baseUrl}/listings/v3")
            .body(body.toString())
            .send()
            .map(
              res =>
                responseMapping(res) match {
                  case ApiSuccess(code, ApiStringBody(str)) =>
                    val body =
                      StubHubListingResponse.fromResponse(str).getOrElse(ApiStringBody(str))
                    ApiSuccess(code, body)
                  case err => errorHandle(err)
              }
            )
      }
  }

  /** Edit Listing
    * PATCH /listings/v3/{listingId}
    */
  def updateListing(listingId: Int, price: BigDecimal): Future[ApiResponse] = {
    val body =
      Json.obj("pricePerProduct" -> Json.obj("amount" -> price, "currency" -> "USD"))

    login()
      .flatMap {
        //TODO: Stop using ApiSuccess
        case ApiSuccess(_, StubHubLoginResponse(token, _)) =>
          request(token)
            .patch(uri"${config.baseUrl}/listings/v3/$listingId")
            .body(body.toString())
            .send()
            .map(
              res =>
                responseMapping(res) match {
                  case ApiSuccess(code, ApiStringBody(str)) =>
                    val body =
                      StubHubListingResponse.fromResponse(str).getOrElse(ApiStringBody(str))
                    ApiSuccess(code, body)
                  case err => errorHandle(err)
              }
            )
      }
  }

  /** Delete Listing
    * DELETE /listings/v3/{listingId}
    */
  def deleteListing(listingId: Int): Future[ApiResponse] = {
    login()
      .flatMap {
        //TODO: Stop using ApiSuccess
        case ApiSuccess(_, StubHubLoginResponse(token, _)) =>
          request(token)
            .delete(uri"${config.baseUrl}/listings/v3/$listingId")
            .send()
            .map(
              res =>
                responseMapping(res) match {
                  case ApiSuccess(code, ApiStringBody(str)) =>
                    val body =
                      StubHubListingResponse.fromResponse(str).getOrElse(ApiStringBody(str))
                    ApiSuccess(code, body)
                  case err => errorHandle(err)
              }
            )
      }
  }

  /** Get Recent Transactions
    * GET /sales/v3/?filters
    */
  def getMySales(start: Int, startDate: String, endDate: String): Future[ApiResponse] = {
    val params = Map(
      "rows" -> "200",
      "start" -> start,
      "filters" -> s"STATUS:PENDING PENDINGREVIEW CONFIRMED SHIPPED DELIVERED AND SALEDATE:$startDate TO $endDate"
    )

    login()
      .flatMap {
        //TODO: Stop using ApiSuccess
        case ApiSuccess(_, StubHubLoginResponse(token, _)) =>
          request(token)
            .get(uri"${config.baseUrl}/sales/v3?$params")
            .send()
            .map(
              res =>
                responseMapping(res) match {
                  case ApiSuccess(code, ApiStringBody(str)) =>
                    val body =
                      StubHubSalesResponse.fromResponse(str).getOrElse(ApiStringBody(str))
                    ApiSuccess(code, body)
                  case err => errorHandle(err)
              }
            )
      }
  }

  /** Get Sales From StubHub
    * The api for getting sales limits 200 sales per request.
    * Therefore here we iterate until we reach retrieve all sales
    * @return
    */
  def getStubHubTransactions(latest: Timestamp): Future[Seq[StubHubTransaction]] = {
    val pageSize: Float = 200
    val startTime = DateParts.from(latest)
    val endTime = DateParts.from(DateHelper.now())
    val startDate: String = s"${startTime.year}-${startTime.month}-${startTime.day}"
    val endDate: String = s"${endTime.year}-${endTime.month}-${endTime.day}"

    getMySales(start = 0, startDate, endDate)
      .flatMap {
        case ApiSuccess(_, sales: StubHubSalesResponse) =>
          val iteration = Seq(Future.successful(sales.sales.getOrElse(Seq()))) ++ Iterator
            .range(1, math.ceil(sales.numFound.toFloat / pageSize).toInt)
            .map(i => {
              getMySales(start = i * pageSize.toInt, startDate, endDate)
                .map {
                  case ApiSuccess(_, sales: StubHubSalesResponse) =>
                    sales.sales.getOrElse(Seq())
                  case _ => Seq()
                }
            })
            .toSeq
          Future.sequence(iteration).map(_.flatten)
        case _ =>
          Future.successful(Seq())
      }
  }
}
