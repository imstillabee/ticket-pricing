package com.eventdynamic.stubhub.models

abstract class ApiResponse
abstract class ApiBody

case class ApiSuccess(code: Int, body: ApiBody) extends ApiResponse
case class ApiError(code: Int, body: ApiBody) extends ApiResponse
case class ApiStringBody(body: String = "") extends ApiBody
