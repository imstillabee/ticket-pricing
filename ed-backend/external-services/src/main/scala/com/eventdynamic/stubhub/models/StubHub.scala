package com.eventdynamic.stubhub.models

case class StubHubTicketWithBarcode(row: String, seat: String, barcode: String)
case class StubHubTicket(row: String, seat: String)

abstract class StubHubBody extends ApiBody
