package com.eventdynamic.stubhub.models

import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Failure, Success, Try}

case class StubHubListing(
  id: String,
  eventId: String,
  status: String,
  currentPrice: BigDecimal,
  section: String,
  row: String,
  seats: String
)
case class StubHubAccountManagementResponse(numFound: Int, listings: Option[Seq[StubHubListing]])
    extends ApiBody

object StubHubAccountManagementResponse {

  def fromResponse(str: String): Option[StubHubAccountManagementResponse] = {
    Try(Json.parse(str)) match {
      case Failure(_)   => None
      case Success(res) => res.asOpt[StubHubAccountManagementResponse]
    }
  }

  implicit val listingReads: Reads[StubHubListing] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "eventId").read[String] and
      (JsPath \ "status").read[String] and
      (JsPath \ "pricePerTicket" \ "amount").read[BigDecimal] and
      (JsPath \ "section").read[String] and
      (JsPath \ "rows").read[String] and
      (JsPath \ "seats").read[String]
  )(StubHubListing.apply _)

  implicit val eventListingsReads: Reads[StubHubAccountManagementResponse] = (
    (JsPath \ "listings" \ "numFound").read[Int] and
      (JsPath \ "listings" \ "listing").readNullable[Seq[StubHubListing]]
  )(StubHubAccountManagementResponse.apply _)
}
