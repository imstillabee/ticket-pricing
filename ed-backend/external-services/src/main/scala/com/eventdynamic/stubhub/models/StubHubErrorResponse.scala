package com.eventdynamic.stubhub.models

import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Failure, Success, Try}

case class StubHubErrorResponse(code: String, description: String) extends StubHubBody

object StubHubErrorResponse {

  def fromResponse(str: String): Option[StubHubErrorResponse] = {
    Try(Json.parse(str)) match {
      case Failure(_)   => None
      case Success(res) => res.asOpt[StubHubErrorResponse]
    }
  }

  implicit val stubHubErrorResponseReads: Reads[StubHubErrorResponse] = (
    (JsPath \ "code").read[String] and
      (JsPath \ "description").read[String]
  )(StubHubErrorResponse.apply _)
}
