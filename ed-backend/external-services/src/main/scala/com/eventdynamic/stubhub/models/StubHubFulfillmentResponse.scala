package com.eventdynamic.stubhub.models

import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Failure, Success, Try}

case class StubHubFulfillmentResponse(orderId: String, status: String) extends StubHubBody

object StubHubFulfillmentResponse {

  def fromResponse(str: String): Option[StubHubFulfillmentResponse] = {
    Try(Json.parse(str)) match {
      case Failure(_)   => None
      case Success(res) => res.asOpt[StubHubFulfillmentResponse]
    }
  }

  implicit val stubHubFulfillmentResponseReads: Reads[StubHubFulfillmentResponse] = (
    (JsPath \ "orderId").read[String] and
      (JsPath \ "status").read[String]
  )(StubHubFulfillmentResponse.apply _)
}
