package com.eventdynamic.stubhub.models

case class StubHubInventory(
  id: Int,
  eventId: Int,
  eventIsBroadcast: Boolean,
  section: String,
  row: String,
  rowIsListed: Boolean,
  seat: String,
  listedPrice: BigDecimal,
  overridePrice: Option[BigDecimal],
  listingId: Option[Int] = None
) {

  def getPrice: BigDecimal = {
    this.overridePrice.getOrElse(this.listedPrice)
  }
}
