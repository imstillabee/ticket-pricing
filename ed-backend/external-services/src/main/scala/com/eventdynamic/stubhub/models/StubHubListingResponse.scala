package com.eventdynamic.stubhub.models

import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Failure, Success, Try}

case class StubHubListingResponse(listingId: String, status: String, deliveryOption: Option[String])
    extends StubHubBody

object StubHubListingResponse {

  def fromResponse(str: String): Option[StubHubListingResponse] = {
    Try(Json.parse(str)) match {
      case Failure(_)   => None
      case Success(res) => res.asOpt[StubHubListingResponse]
    }
  }

  implicit val stubHubListingResponseReads: Reads[StubHubListingResponse] = (
    (JsPath \ "listing" \ "id").read[String] and
      (JsPath \ "listing" \ "status").read[String] and
      (JsPath \ "listing" \ "deliveryOption").readNullable[String]
  )(StubHubListingResponse.apply _)
}
