package com.eventdynamic.stubhub.models

import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Failure, Success, Try}

case class StubHubLoginResponse(access_token: String, refresh_token: String) extends StubHubBody

object StubHubLoginResponse {

  def fromResponse(str: String): Option[StubHubLoginResponse] = {
    Try(Json.parse(str)) match {
      case Failure(_)   => None
      case Success(res) => res.asOpt[StubHubLoginResponse]
    }
  }

  implicit val stubHubloginResponseReads: Reads[StubHubLoginResponse] = (
    (JsPath \ "access_token").read[String] and
      (JsPath \ "refresh_token").read[String]
  )(StubHubLoginResponse.apply _)
}
