package com.eventdynamic.stubhub.models

import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Failure, Success, Try}

case class StubHubTransaction(
  saleId: String,
  listingId: String,
  status: String,
  eventId: String,
  section: String,
  row: String,
  seats: String,
  price: BigDecimal,
  saleDate: String
)
case class StubHubSalesResponse(numFound: Int, sales: Option[Seq[StubHubTransaction]])
    extends ApiBody

object StubHubTransaction {

  trait format {
    implicit val stubHubTransactionReads: Reads[StubHubTransaction] = (
      (JsPath \ "saleId").read[String] and
        (JsPath \ "listingId").read[String] and
        (JsPath \ "status").read[String] and
        (JsPath \ "eventId").read[String] and
        (JsPath \ "section").read[String] and
        (JsPath \ "rows").read[String] and
        (JsPath \ "seats").read[String] and
        (JsPath \ "payoutPerTicket" \ "amount").read[BigDecimal] and
        (JsPath \ "saleDate").read[String]
    )(StubHubTransaction.apply _)
  }
}

object StubHubSalesResponse extends StubHubTransaction.format {

  def fromResponse(str: String): Option[StubHubSalesResponse] = {
    Try(Json.parse(str)) match {
      case Failure(_)   => None
      case Success(res) => res.asOpt[StubHubSalesResponse]
    }
  }

  implicit val eventListingsReads: Reads[StubHubSalesResponse] = (
    (JsPath \ "numFound").read[Int] and
      (JsPath \ "sales").readNullable[Seq[StubHubTransaction]]
  )(StubHubSalesResponse.apply _)
}
