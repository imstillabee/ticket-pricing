package com.eventdynamic.tdcreplicatedproxy

import java.time.Instant

import com.eventdynamic.tdcreplicatedproxy.models.{TdcBuyerType, TdcInventory, TdcTransaction}
import com.eventdynamic.{SttpHelper, SttpLoggingBackend}
import com.softwaremill.sttp._
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend

import scala.concurrent.Future

class TdcReplicatedProxyService(
  private val conf: TdcReplicatedProxyConfig,
  implicit private val sttpBackend: SttpBackend[Future, Nothing] =
    new SttpLoggingBackend(OkHttpFutureBackend(), "TdcReplicatedProxy", Seq())
) extends TdcTransaction.format
    with TdcInventory.format
    with TdcBuyerType.format {

  // Base request, no headers for now
  private val req = sttp

  def getTransactions(
    after: Instant,
    before: Instant,
    eventIds: Seq[Int]
  ): Future[Response[Seq[TdcTransaction]]] = {
    val params = Seq("after" -> after, "before" -> before) ++ eventIds.map("eventId" -> _)

    req
      .get(uri"${conf.proxyBaseUrl}/transactions?$params")
      .response(SttpHelper.asJson[Seq[TdcTransaction]])
      .send()
  }

  def getInventory(
    eventIds: Seq[Int],
    after: Instant,
    before: Instant
  ): Future[Response[Seq[TdcInventory]]] = {
    val params = Seq("after" -> after, "before" -> before) ++ eventIds.map("eventId" -> _)

    req
      .get(uri"${conf.proxyBaseUrl}/inventory?$params")
      .response(SttpHelper.asJson[Seq[TdcInventory]])
      .send()
  }

  def listBuyerTypes(eventIds: Seq[Int]): Future[Response[Seq[TdcBuyerType]]] = {
    val params = eventIds.map("eventId" -> _)
    req
      .get(uri"${conf.proxyBaseUrl}/buyerTypes?$params")
      .response(SttpHelper.asJson[Seq[TdcBuyerType]])
      .send()
  }
}
