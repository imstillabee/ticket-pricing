package com.eventdynamic.tdcreplicatedproxy.models

import play.api.libs.json.{Json, OFormat}

case class TdcBuyerType(
  id: Long,
  code: String,
  active: Boolean,
  publicDescription: String,
  isInPriceStructure: Boolean
)

object TdcBuyerType {

  trait format {
    implicit lazy val tdcBuyerTypeFormat: OFormat[TdcBuyerType] = Json.format[TdcBuyerType]
  }
}
