package com.eventdynamic.tdcreplicatedproxy.models

import java.time.Instant

import com.eventdynamic.utils.InstantISOFormat
import play.api.libs.json.{Json, OFormat}

case class TdcInventory(
  eventId: Int,
  seatId: Int,
  priceScaleId: Int,
  createdAt: Instant,
  modifiedAt: Instant,
  listedPrice: Option[BigDecimal],
  section: String,
  row: String,
  seatNumber: String,
  holdCodeType: String
)

object TdcInventory extends InstantISOFormat {

  trait format {
    implicit lazy val tdcInventoryFormat: OFormat[TdcInventory] = Json.format[TdcInventory]
  }
}
