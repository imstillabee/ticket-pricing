package com.eventdynamic.tdcreplicatedproxy.models

import java.time.Instant

import com.eventdynamic.utils.InstantISOFormat
import play.api.libs.json.{Json, OFormat}

case class TdcTransaction(
  id: Long,
  createdAt: Instant,
  timestamp: Instant,
  eventId: Long,
  buyerTypeCode: String,
  seatId: Long,
  price: BigDecimal,
  section: String,
  row: String,
  seatNumber: String,
  transactionTypeCode: String,
  priceScaleId: Long,
  holdCodeType: String
)

object TdcTransaction extends InstantISOFormat {

  trait format {
    implicit lazy val tdcTransactionFormat: OFormat[TdcTransaction] = Json.format[TdcTransaction]
  }
}
