package com.eventdynamic.vividseats

case class VividSeatsConfig(baseUrl: String, apiToken: String)
