package com.eventdynamic.vividseats

import com.eventdynamic.vividseats.models.VividSeatsFormats._
import com.eventdynamic.vividseats.models._
import com.eventdynamic.{SttpHelper, SttpLoggingBackend}
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.softwaremill.sttp.{SttpBackend, _}

import scala.concurrent.Future

class VividSeatsService(
  private val config: VividSeatsConfig,
  private val sttpBackend: SttpBackend[Future, Nothing] =
    new SttpLoggingBackend(OkHttpFutureBackend(), "VividSeats")
) {

//  private val logger = getLogger(this.getClass)
  implicit val backend: SttpBackend[Future, Nothing] = sttpBackend
  private val req =
    sttp.headers("Api-Token" -> config.apiToken)

  def getListings(): Future[Response[VividSeatsListingGetResponse]] = {
    // TODO: Complete implementation
    val ps = Map("fromEventDate" -> "yyyy-MM-dd", "toEventDate" -> "yyyy-MM-dd")
    req
      .get(uri"${config.baseUrl}/listings/v2/get?$ps")
      .headers("Content-Type" -> "application/json", "Api-Token" -> config.apiToken)
      .response(SttpHelper.asJson[VividSeatsListingGetResponse])
      .send()
  }

  /**
    * POST /listings/v2/create
    */
  def createListing(): Future[Response[String]] = {
    // TODO: Complete implementation
    req
      .post(uri"${config.baseUrl}/listings/v2/create")
      .headers("Content-Type" -> "application/json", "Api-Token" -> config.apiToken)
      .body()
      .send()
  }

  /**
    * POST /listings/v1/updateListing
    */
  def updateListing(): Future[Response[String]] = {
    // TODO: Complete implementation
    req
      .post(uri"${config.baseUrl}/listings/v1/updateListing")
      .headers("Content-Type" -> "application/x-www-form-urlencoded")
      .body()
      .send()
  }

  /**
    * DELETE listings/v2/delete
    */
  def deListTicket(): Future[Response[String]] = {
    // TODO: Complete implementation
    req
      .delete(uri"${config.baseUrl}/listings/v2/delete")
      .headers("Content-Type" -> "application/json", "Api-Token" -> config.apiToken)
      .body()
      .send()
  }

  /**
    * POST /v1/confirmOrder
    */
  def fulfillOrder(): Future[Response[String]] = {
    // TODO: Complete implementation
    req
      .post(uri"${config.baseUrl}/v1/confirmOrder")
      .headers("Content-Type" -> "application/x-www-form-urlencoded")
      .body()
      .send()
  }

  /**
    * POST /v1/rejectOrder
    */
  def rejectOrder(): Future[Response[String]] = {
    // TODO: Complete implementation
    req
      .post(uri"${config.baseUrl}/v1/rejectOrder")
      .headers("Content-Type" -> "application/x-www-form-urlencoded")
      .body()
      .send()
  }
}
