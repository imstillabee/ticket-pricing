package com.eventdynamic.vividseats.models

trait VividSeatsBaseResponse {
  def success: Boolean
  def message: Option[String]
}
