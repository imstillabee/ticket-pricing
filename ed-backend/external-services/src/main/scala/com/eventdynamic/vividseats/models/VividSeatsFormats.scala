package com.eventdynamic.vividseats.models

import play.api.libs.json.{Json, OFormat}

object VividSeatsFormats {
  implicit val listingFormat: OFormat[VividSeatsListing] = Json.format[VividSeatsListing]
  implicit val getListingResponseFormat: OFormat[VividSeatsListingGetResponse] =
    Json.format[VividSeatsListingGetResponse]
}
