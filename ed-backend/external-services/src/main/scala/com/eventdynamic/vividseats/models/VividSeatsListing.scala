package com.eventdynamic.vividseats.models

case class VividSeatsListing(
  id: Option[String],
  productionId: Int,
  quantity: Int,
  ticketId: Option[String],
  price: Option[BigDecimal],
  eventName: Option[String],
  eventDate: Option[String],
  section: String,
  row: String,
  hasBarcodes: Option[Boolean]
)
