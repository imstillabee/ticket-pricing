package com.eventdynamic.vividseats.models

case class VividSeatsListingGetResponse(
  success: Boolean,
  message: Option[String],
  listings: Seq[VividSeatsListing],
  attribute: Option[String]
) extends VividSeatsBaseResponse
