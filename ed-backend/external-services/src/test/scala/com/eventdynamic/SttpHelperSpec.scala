package com.eventdynamic
import com.softwaremill.sttp.Response
import org.scalatest.WordSpec

class SttpHelperSpec extends WordSpec {
  val emptyHeaders = scala.collection.immutable.Seq.empty[(String, String)]
  val emptyHistory = List.empty[Response[Unit]]

  "SttpHelper#handleResponse" should {
    import SttpHelper.handleResponse

    "return body on success" in {
      val response = Response[String](Right("asdfasd"), 200, "", emptyHeaders, emptyHistory)
      val handled = handleResponse(response)
      assert(handled == "asdfasd")
    }

    "throw exception when wrong code" in {
      val response = Response[String](Left("asdfasd"), 400, "", emptyHeaders, emptyHistory)
      assertThrows[UnsuccessfulResponseException[String]](handleResponse(response))
    }

    "throw exception when left body" in {
      val response = Response[String](Left("asdfasd"), 200, "", emptyHeaders, emptyHistory)
      assertThrows[UnparseableResponseException[String]](handleResponse(response))
    }
  }
}
