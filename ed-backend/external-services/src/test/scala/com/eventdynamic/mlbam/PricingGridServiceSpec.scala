package com.eventdynamic.mlbam

import com.eventdynamic.mlbam.models.PricingGridBatch
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.softwaremill.sttp.testing.SttpBackendStub
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.{AsyncWordSpec, BeforeAndAfterEach, Matchers}
import org.scalatest.concurrent.ScalaFutures

class PricingGridServiceSpec
    extends AsyncWordSpec
    with MockitoSugar
    with BeforeAndAfterEach
    with ScalaFutures
    with Matchers {

  var conf: PricingGridConfig =
    new PricingGridConfig("testBaseUrl", "sourceId")

  override def beforeEach(): Unit = {
    super.beforeEach()

    conf = new PricingGridConfig("testBaseUrl", "sourceId")
  }

  "PricingGridService#bulkUpdatePrices" should {
    "return an empty response from a bad API request" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains("ticketing-admin/PriceChange.prc"))
        .thenRespondWithCode(400, "Bad Request")

      val pricingGridService = new PricingGridService(conf, backend)

      pricingGridService
        .bulkUpdatePrices(Seq(PricingGridBatch(1, 1, 1.0)), 1, 1)
        .map(result => {
          assert(result.code == 400)
          assert(result.body.isLeft)
        })
    }

    "return a Response[String] from a successful API request" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains("ticketing-admin/PriceChange.prc"))
        .thenRespondWithCode(
          200,
          """<ns2:PriceFeedResponse xmlns:ns2="http://www.mlb.com/ticketing/price/dynamic" hasErrors="false" totalRecords="1" totalGoodRecords="1" totalBadRecords="0" totalSkippedRecords="0" totalUnchangedRecords="0" totalPriceUpdates="1" totalFeaturedUpdates="0"/>"""
        )

      val pricingGridService = new PricingGridService(conf, backend)

      pricingGridService
        .bulkUpdatePrices(Seq(PricingGridBatch(1, 1, 1.0)), 1, 1)
        .map(result => {
          assert(result.code == 200)
          assert(result.body.isRight)
        })
    }
  }
}
