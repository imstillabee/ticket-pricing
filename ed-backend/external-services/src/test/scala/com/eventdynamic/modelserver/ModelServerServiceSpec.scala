package com.eventdynamic.modelserver

import java.sql.Timestamp
import java.time.Instant

import com.eventdynamic.modelserver.models._
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.softwaremill.sttp.testing.SttpBackendStub
import org.scalatest.AsyncWordSpec

class ModelServerServiceSpec extends AsyncWordSpec {

  "ModelServerServiceSpec#getPrices" should {
    // Fake payload data
    val fakeContext = ModelServerPriceContext(
      event_date = "2018-12-22 12:13:14.123",
      event_score = 10,
      spring = .02,
      timestamp = "2018-12-22 12:13:14.123",
      weather = Some(ModelServerWeather(temp_average = 0f, temp = 0, weather_condition = ""))
    )
    val fakeExamples = Seq(
      ModelServerPriceExample("1", "scale 1", "123", 0),
      ModelServerPriceExample("2", "scale 1", "123", 0)
    )
    val fakePayload = ModelServerPricePayload(fakeContext, fakeExamples)

    "parse success response" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains("baseurl.com/client/1/price"))
        .thenRespond("""{"prices":[123.00, 123.00, null]}""")

      val service = new ModelServerService("baseurl.com", backend)

      service
        .getPrices("1", fakePayload)
        .map(res => {
          assert(res.code === 200)
          assert(res.body.isRight)
          assert(res.body.right.get.prices.slice(0, 2).forall(_.contains(123)))
          assert(res.body.right.get.prices.last.isEmpty)
        })
    }

    "handle failed response" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains("baseurl.com/client/1/price"))
        .thenRespondServerError()

      val service = new ModelServerService("baseurl.com", backend)

      service
        .getPrices("1", fakePayload)
        .map(res => {
          assert(res.code === 500)
          assert(res.body.isLeft)
        })
    }
  }

  "ModelServerServiceSpec#getPrices without weather data" should {
    // Fake payload data
    val fakeContext = ModelServerPriceContext(
      event_date = "2018-12-22 12:13:14.123",
      event_score = 10,
      spring = .02,
      timestamp = "2018-12-22 12:13:14.123",
      weather = None
    )
    val fakeExamples = Seq(
      ModelServerPriceExample("1", "scale 1", "123", 0),
      ModelServerPriceExample("2", "scale 1", "123", 0)
    )
    val fakePayload = ModelServerPricePayload(fakeContext, fakeExamples)

    "parse success response" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains("baseurl.com/client/1/price"))
        .thenRespond("""{"prices":[123.00, 123.00, null]}""")

      val service = new ModelServerService("baseurl.com", backend)

      service
        .getPrices("1", fakePayload)
        .map(res => {
          assert(res.code === 200)
          assert(res.body.isRight)
          assert(res.body.right.get.prices.slice(0, 2).forall(_.contains(123)))
          assert(res.body.right.get.prices.last.isEmpty)
        })
    }

    "handle failed response" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains("baseurl.com/client/1/price"))
        .thenRespondServerError()

      val service = new ModelServerService("baseurl.com", backend)

      service
        .getPrices("1", fakePayload)
        .map(res => {
          assert(res.code === 500)
          assert(res.body.isLeft)
        })
    }
  }

  "ModelServerServiceSpec#getEventScores" should {
    // Fake payload data
    val fakeContext = ModelServerFactorContext(
      timestamp = "2018-12-22 12:13:14.123",
      MLBModelServerClientStats(losses = 0, rank = None, games_ahead = 0f, wins = 0)
    )
    val fakeExamples = Seq(
      ModelServerFactorExample(
        event_date = "2018-12-22 12:13:14.123",
        event_score_modifier = 3.0,
        spring_modifier = 1.2,
        eventStats = MLBModelServerEventStats(
          game_num = 1,
          home_opener = 1,
          opp_rank = None,
          home_series_start = 1,
          opp_losses = 1,
          opp_wins = 1,
          opponent = ""
        ),
        weather = Some(ModelServerWeather(temp_average = 80f, temp = 80f, weather_condition = ""))
      ),
      ModelServerFactorExample(
        event_date = "2018-12-22 12:13:14.123",
        event_score_modifier = 3.0,
        spring_modifier = 1.2,
        eventStats = MLBModelServerEventStats(
          game_num = 1,
          home_opener = 1,
          home_series_start = 1,
          opp_losses = 1,
          opp_rank = None,
          opp_wins = 1,
          opponent = ""
        ),
        weather = Some(ModelServerWeather(temp_average = 80f, temp = 80f, weather_condition = ""))
      )
    )
    val fakePayload = ModelServerFactorPayload(fakeContext, fakeExamples)

    "parse success response" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains("baseurl.com/client/1/factors"))
        .thenRespond("""{"eventScores":[12.0, 12], "springValues":[0.02, 0.002]}""")

      val service = new ModelServerService("baseurl.com", backend)

      service
        .getFactors("1", fakePayload)
        .map(res => {
          assert(res.code === 200)
          assert(res.body.isRight)
          assert(res.body.right.get.eventScores.forall(_ == 12.0))
          assert(res.body.right.get.springValues.head == .02)
          assert(res.body.right.get.springValues(1) == .002)
        })
    }

    "handle failed response" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains("baseurl.com/client/1/factors"))
        .thenRespondServerError()

      val service = new ModelServerService("baseurl.com", backend)

      service
        .getFactors("1", fakePayload)
        .map(res => {
          assert(res.code === 500)
          assert(res.body.isLeft)
        })
    }
  }

  "ModelServerServiceSpec#getEventScores without weather data" should {
    // Fake payload data
    val fakeContext = ModelServerFactorContext(
      timestamp = "2018-12-22 12:13:14.123",
      MLBModelServerClientStats(losses = 0, rank = None, games_ahead = 0f, wins = 0)
    )
    val fakeExamples = Seq(
      ModelServerFactorExample(
        event_date = "2018-12-22 12:13:14.123",
        event_score_modifier = 3.0,
        spring_modifier = 1.2,
        eventStats = MLBModelServerEventStats(
          game_num = 1,
          home_opener = 1,
          opp_rank = None,
          home_series_start = 1,
          opp_losses = 1,
          opp_wins = 1,
          opponent = ""
        ),
        weather = None
      ),
      ModelServerFactorExample(
        event_date = "2018-12-22 12:13:14.123",
        event_score_modifier = 3.0,
        spring_modifier = 1.2,
        eventStats = MLBModelServerEventStats(
          game_num = 1,
          home_opener = 1,
          home_series_start = 1,
          opp_losses = 1,
          opp_rank = None,
          opp_wins = 1,
          opponent = ""
        ),
        weather = None
      )
    )
    val fakePayload = ModelServerFactorPayload(fakeContext, fakeExamples)

    "parse success response" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains("baseurl.com/client/1/factors"))
        .thenRespond("""{"eventScores":[12.0, 12], "springValues":[0.02, 0.002]}""")

      val service = new ModelServerService("baseurl.com", backend)

      service
        .getFactors("1", fakePayload)
        .map(res => {
          assert(res.code === 200)
          assert(res.body.isRight)
          assert(res.body.right.get.eventScores.forall(_ == 12.0))
          assert(res.body.right.get.springValues.head == .02)
          assert(res.body.right.get.springValues(1) == .002)
        })
    }

    "handle failed response" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains("baseurl.com/client/1/factors"))
        .thenRespondServerError()

      val service = new ModelServerService("baseurl.com", backend)

      service
        .getFactors("1", fakePayload)
        .map(res => {
          assert(res.code === 500)
          assert(res.body.isLeft)
        })
    }
  }

  "ModelServerService#getAllPrices" should {
    "parse success response" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(req => {
          val correctUrl = req.uri.toString.contains("baseurl.com/client/1/priceAll")
          val correctSpring = req.uri.paramsMap("spring") == "0.02"

          correctUrl && correctSpring
        })
        .thenRespond(
          """{"prices":[{"row": "1", "section": "123", "price": 100.00}, {"row": "2", "section": "123", "price": 90.00}]}"""
        )

      val service = new ModelServerService("baseurl.com", backend)

      service
        .getAllPrices("1", 5, 2)
        .map(prices => {
          assert(prices.prices.length == 2)
          assert(prices.prices(0).row == "1")
          assert(prices.prices(1).row == "2")
        })
    }
  }

  "ModelSeverService#getAutomatedSpringValue" should {
    val baseUrl = "baseurl.com"
    "return a spring value given an event score and days before event" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(req => {
          req.uri.toString.contains(s"$baseUrl/client/1/spring")
        })
        .thenRespond("""{"springValue": 0.021604}""")

      val service = new ModelServerService(baseUrl, backend)

      service
        .getAutomatedSpringValue("1", 5.94, Some(Timestamp.from(Instant.now)))
        .map(response => assert(response.springValue == 0.021604))
    }
  }

  "ModelServerService#getProjections" should {
    val baseUrl = "http://baseurl.com"
    val clientId = "1"

    "parse success response" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(req => {
          println(req.uri.toString())
          req.uri.toString == s"$baseUrl/client/$clientId/projection"
        })
        .thenRespond("""
              |{
              |    "projection": [
              |        {
              |            "timestamp": "2019-06-09T23:00:00Z",
              |            "cumulative_inventory": 1374.0,
              |            "cumulative_inventory_ub": 1342.0,
              |            "cumulative_inventory_lb": 1407.0,
              |            "cumulative_revenue": 24701.54,
              |            "cumulative_revenue_ub": 25738.29,
              |            "cumulative_revenue_lb": 23732.91
              |        },
              |        {
              |            "timestamp": "2019-06-10T23:00:00Z",
              |            "cumulative_inventory": 1307.0,
              |            "cumulative_inventory_ub": 1237.0,
              |            "cumulative_inventory_lb": 1381.0,
              |            "cumulative_revenue": 26823.64,
              |            "cumulative_revenue_ub": 29178.28,
              |            "cumulative_revenue_lb": 24611.97
              |        },
              |        {
              |            "timestamp": "2019-06-11T23:00:00Z",
              |            "cumulative_inventory": 1016.0,
              |            "cumulative_inventory_ub": 763.0,
              |            "cumulative_inventory_lb": 1221.0,
              |            "cumulative_revenue": 32424.21,
              |            "cumulative_revenue_ub": 37811.49,
              |            "cumulative_revenue_lb": 27730.03
              |        }
              |    ]
              |}
            """.stripMargin)

      val payload = ModelServerProjectionPayload(
        context = ModelServerProjectionContext(event_date = Instant.EPOCH, total_inventory = 12),
        examples = (1 to 10).map(idx => {
          ModelServerProjectionExample(
            timestamp = Instant.EPOCH,
            cumulative_inventory = idx,
            cumulative_revenue = idx
          )
        })
      )

      val service = new ModelServerService(baseUrl, backend)
      service
        .getProjections(clientId, payload)
        .map(response => {
          assert(response.length == 3)
          assert(
            response.head == ModelServerProjection(
              timestamp = Instant.parse("2019-06-09T23:00:00Z"),
              cumulative_inventory = 1374,
              cumulative_inventory_ub = 1342,
              cumulative_inventory_lb = 1407,
              cumulative_revenue = 24701.54,
              cumulative_revenue_ub = 25738.29,
              cumulative_revenue_lb = 23732.91
            )
          )
        })
    }
  }
}
