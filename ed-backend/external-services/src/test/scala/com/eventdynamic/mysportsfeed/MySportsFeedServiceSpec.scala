package com.eventdynamic.mysportsfeed

import java.time.Instant

import com.eventdynamic.mysportsfeed.models.mlb.MLBStats
import com.softwaremill.sttp._
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.softwaremill.sttp.testing.SttpBackendStub
import org.scalatest.AsyncWordSpec

import scala.concurrent.Future

class MySportsFeedServiceSpec extends AsyncWordSpec {
  "MySportsFeedService#MLB" should {

    val teamAbbreviation = "NYM"
    val mlbTeamGameLogResponse =
      """{"teamgamelogs":{"lastUpdatedOn":"2018-04-13 3:07:34 PM","gamelogs":[{"game":{"id":"39255","date":"2017-09-02","time":"2:10PM","awayTeam":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"homeTeam":{"ID":"122","City":"Houston","Name":"Astros","Abbreviation":"HOU"},"location":"Minute Maid Park"},"team":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"stats":{"Wins":{"@category":"Standings","@abbreviation":"W","#text":"0"},"GamesBack":{"@category":"Standings","@abbreviation":"GB","#text":"0.0"}}},{"game":{"id":"39256","date":"2017-09-02","time":"8:10PM","awayTeam":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"homeTeam":{"ID":"122","City":"Houston","Name":"Astros","Abbreviation":"HOU"},"location":"Minute Maid Park"},"team":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"stats":{"Wins":{"@category":"Standings","@abbreviation":"W","#text":"0"},"GamesBack":{"@category":"Standings","@abbreviation":"GB","#text":"0.0"}}},{"game":{"id":"39257","date":"2017-09-03","time":"2:10PM","awayTeam":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"homeTeam":{"ID":"122","City":"Houston","Name":"Astros","Abbreviation":"HOU"},"location":"Minute Maid Park"},"team":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"stats":{"Wins":{"@category":"Standings","@abbreviation":"W","#text":"0"},"GamesBack":{"@category":"Standings","@abbreviation":"GB","#text":"0.0"}}},{"game":{"id":"39849","date":"2017-09-04","time":"1:10PM","awayTeam":{"ID":"129","City":"Philadelphia","Name":"Phillies","Abbreviation":"PHI"},"homeTeam":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"location":"Citi Field"},"team":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"stats":{"Wins":{"@category":"Standings","@abbreviation":"W","#text":"1"},"GamesBack":{"@category":"Standings","@abbreviation":"GB","#text":"0.0"}}},{"game":{"id":"39850","date":"2017-09-05","time":"7:10PM","awayTeam":{"ID":"129","City":"Philadelphia","Name":"Phillies","Abbreviation":"PHI"},"homeTeam":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"location":"Citi Field"},"team":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"stats":{"Wins":{"@category":"Standings","@abbreviation":"W","#text":"0"},"GamesBack":{"@category":"Standings","@abbreviation":"GB","#text":"0.0"}}}]}}"""
    val mlbTeamStandingsResponse =
      """{"divisionteamstandings":{"lastUpdatedOn":"2018-05-16 7:59:43 PM","division":[{"@name":"National League/East","teamentry":[{"team":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"rank":"4","stats":{"GamesPlayed":{"@abbreviation":"G","#text":"162"},"Wins":{"@category":"Standings","@abbreviation":"W","#text":"70"},"Losses":{"@category":"Standings","@abbreviation":"L","#text":"92"},"GamesBack":{"@category":"Standings","@abbreviation":"GB","#text":"27.0"}}}]}]}}"""
    val mlbTeamFullScheduleResponse =
      """{"fullgameschedule":{"lastUpdatedOn":"2018-04-18 5:48:50 PM","gameentry":[{"id":"39255","scheduleStatus":"Normal","originalDate":"2017-09-01","originalTime":"8:10PM","delayedOrPostponedReason":null,"date":"2017-09-02","time":"2:10PM","awayTeam":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"homeTeam":{"ID":"122","City":"Houston","Name":"Astros","Abbreviation":"HOU"},"location":"Minute Maid Park"},{"id":"39256","scheduleStatus":"Normal","originalDate":null,"originalTime":null,"delayedOrPostponedReason":null,"date":"2017-09-02","time":"8:10PM","awayTeam":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"homeTeam":{"ID":"122","City":"Houston","Name":"Astros","Abbreviation":"HOU"},"location":"Minute Maid Park"},{"id":"39257","scheduleStatus":"Normal","originalDate":null,"originalTime":null,"delayedOrPostponedReason":null,"date":"2017-09-03","time":"2:10PM","awayTeam":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"homeTeam":{"ID":"122","City":"Houston","Name":"Astros","Abbreviation":"HOU"},"location":"Minute Maid Park"},{"id":"39849","scheduleStatus":"Normal","originalDate":null,"originalTime":null,"delayedOrPostponedReason":null,"date":"2017-09-04","time":"1:10PM","awayTeam":{"ID":"129","City":"Philadelphia","Name":"Phillies","Abbreviation":"PHI"},"homeTeam":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"location":"Citi Field"},{"id":"39850","scheduleStatus":"Normal","originalDate":null,"originalTime":null,"delayedOrPostponedReason":null,"date":"2017-09-05","time":"7:10PM","awayTeam":{"ID":"129","City":"Philadelphia","Name":"Phillies","Abbreviation":"PHI"},"homeTeam":{"ID":"127","City":"New York","Name":"Mets","Abbreviation":"NYM"},"location":"Citi Field"}]}}"""

    val baseUrl = "baseUrl"

    "get correct url for team divisional statistics" in {
      val backend = SttpBackendStub(OkHttpFutureBackend()).whenAnyRequest
        .thenRespond(mlbTeamStandingsResponse)

      val mySportsFeedService =
        new MySportsFeedService("username", "password", baseUrl, backend)

      val params = Map("team" -> teamAbbreviation, "teamStats" -> "W,L,GB")

      val url = mySportsFeedService.MLB.teamStandingsUrl(params)

      assert(
        url == uri"$baseUrl/v1.2/pull/mlb/current/division_team_standings.json?team=NYM&teamStats=W,L,GB"
      )
    }

    "get correct url for team game logs" in {
      val backend = SttpBackendStub(OkHttpFutureBackend()).whenAnyRequest
        .thenRespond(mlbTeamStandingsResponse)

      val mySportsFeedService =
        new MySportsFeedService("username", "password", "baseUrl", backend)

      val params = Map(
        "team" -> teamAbbreviation,
        "teamStats" -> "W",
        "date" -> "until-today",
        "sort" -> "game.starttime"
      )

      val url = mySportsFeedService.MLB.teamLogsUrl(params)

      assert(
        url == uri"$baseUrl/v1.2/pull/mlb/current/team_gamelogs.json?team=NYM&teamStats=W&date=until-today&sort=game.starttime"
      )
    }

    "get correct url for team full schedule" in {
      val backend = SttpBackendStub(OkHttpFutureBackend()).whenAnyRequest
        .thenRespond(mlbTeamFullScheduleResponse)

      val mySportsFeedService =
        new MySportsFeedService("username", "password", "baseUrl", backend)

      val params = Map("team" -> teamAbbreviation)

      val url = mySportsFeedService.MLB.teamFullSchedule(params)

      assert(url == uri"$baseUrl/v1.2/pull/mlb/current/full_game_schedule.json?team=NYM")
    }

    "get Team Divisional statistics" in {
      val backend = SttpBackendStub(OkHttpFutureBackend()).whenAnyRequest
        .thenRespond(mlbTeamStandingsResponse)

      val mySportsFeedService =
        new MySportsFeedService("username", "password", "baseUrl", backend)

      mySportsFeedService.MLB
        .getTeamStandings(teamAbbreviation)
        .map(divisions => {
          assert(divisions.isDefined)
          assert(divisions.get.head.teamStandings.head.wins == "70")
          assert(divisions.get.head.teamStandings.head.losses == "92")
          assert(divisions.get.head.teamStandings.head.rank == "4")
          assert(divisions.get.head.teamStandings.head.gamesBack == "27.0")
        })
    }

    "get Team Game Logs" in {
      val backend = SttpBackendStub(OkHttpFutureBackend()).whenAnyRequest
        .thenRespond(mlbTeamGameLogResponse)

      val mySportsFeedService =
        new MySportsFeedService("username", "password", "baseUrl", backend)

      mySportsFeedService.MLB
        .getTeamGameLogs(teamAbbreviation)
        .map(gameLogs => {
          assert(gameLogs.isDefined)
          assert(gameLogs.get.head.won == "0")
          assert(gameLogs.get.head.opponent == "NYM")
          assert(gameLogs.get.head.date == "2017-09-02")
          assert(gameLogs.get.head.time == "2:10PM")
        })
    }

    "get Full Team Schedule Game Entries" in {
      val backend = SttpBackendStub(OkHttpFutureBackend()).whenAnyRequest
        .thenRespond(mlbTeamFullScheduleResponse)

      val mySportsFeedService =
        new MySportsFeedService("username", "password", "baseUrl", backend)

      mySportsFeedService.MLB
        .getTeamFullSchedule(teamAbbreviation)
        .map(gameEntry => {
          assert(gameEntry.isDefined)
          assert(gameEntry.get.head.awayTeam == "NYM")
          assert(gameEntry.get.head.homeTeam == "HOU")
          assert(gameEntry.get.head.date == "2017-09-02")
          assert(gameEntry.get.head.time == "2:10PM")

        })
    }

    "handle server error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend()).whenAnyRequest
        .thenRespondServerError()

      val mySportsFeedService =
        new MySportsFeedService("username", "password", "baseUrl", backend)

      mySportsFeedService.MLB
        .getTeamStandings(teamAbbreviation)
        .map(divisions => {
          assert(divisions.isEmpty)
        })

      mySportsFeedService.MLB
        .getTeamGameLogs(teamAbbreviation)
        .map(gameLogs => {
          assert(gameLogs.isEmpty)
        })

    }

    "handle client error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend()).whenAnyRequest
        .thenRespondNotFound()

      val mySportsFeedService =
        new MySportsFeedService("username", "password", "baseUrl", backend)

      mySportsFeedService.MLB
        .getTeamStandings(teamAbbreviation)
        .map(divisions => {
          assert(divisions.isEmpty)
        })

      mySportsFeedService.MLB
        .getTeamGameLogs(teamAbbreviation)
        .map(gameLogs => {
          assert(gameLogs.isEmpty)
        })

    }

    "calculate game statistics correctly" in {
      val backend = SttpBackendStub(OkHttpFutureBackend()).whenAnyRequest
        .thenRespond(mlbTeamGameLogResponse)

      val backend2 = SttpBackendStub(OkHttpFutureBackend()).whenAnyRequest
        .thenRespond(mlbTeamFullScheduleResponse)

      val mySportsFeedService =
        new MySportsFeedService("username", "password", "baseUrl", backend)
      val mySportsFeedService2 =
        new MySportsFeedService("username", "password", "baseUrl", backend2)

      for {
        mlbGameLogs <- mySportsFeedService.MLB.getTeamGameLogs(teamAbbreviation)
        mlbTeamFullSchedule <- mySportsFeedService2.MLB.getTeamFullSchedule(teamAbbreviation)
        result <- Future {
          mySportsFeedService.MLB.calculateGameStats(
            MLBStats(teamAbbreviation, None, mlbGameLogs, mlbTeamFullSchedule),
            "2017-09-02"
          )
        }
      } yield {
        assert(result.gameNum == 0)
        assert(result.homeSeriesStart)
        assert(!result.homeOpener)
        assert(result.opponent == "NYM")
      }
    }

  }

  "MySportsFeedService#NFL" should {
    val teamAbbreviation = "ATL"
    val baseUrl = "baseUrl"

    "get an NFL team's game log" in {
      // Note: I pulled out the "references" section in the payload because we ignore it and it's massive
      val nflTeamGameLogResponse =
        """{"lastUpdatedOn":"2019-06-28T12:58:11.265Z","games":[{"schedule":{"id":51465,"week":1,"startTime":"2019-09-08T17:00:00.000Z","endedTime":null,"awayTeam":{"id":68,"abbreviation":"ATL"},"homeTeam":{"id":63,"abbreviation":"MIN"},"venue":{"id":141,"name":"USBankStadium"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51491,"week":2,"startTime":"2019-09-16T00:20:00.000Z","endedTime":null,"awayTeam":{"id":54,"abbreviation":"PHI"},"homeTeam":{"id":68,"abbreviation":"ATL"},"venue":{"id":44,"name":"GeorgiaDome"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51499,"week":3,"startTime":"2019-09-22T17:00:00.000Z","endedTime":null,"awayTeam":{"id":68,"abbreviation":"ATL"},"homeTeam":{"id":65,"abbreviation":"IND"},"venue":{"id":56,"name":"LucasOilStadium"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51510,"week":4,"startTime":"2019-09-29T17:00:00.000Z","endedTime":null,"awayTeam":{"id":67,"abbreviation":"TEN"},"homeTeam":{"id":68,"abbreviation":"ATL"},"venue":{"id":44,"name":"GeorgiaDome"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51530,"week":5,"startTime":"2019-10-06T17:00:00.000Z","endedTime":null,"awayTeam":{"id":68,"abbreviation":"ATL"},"homeTeam":{"id":64,"abbreviation":"HOU"},"venue":{"id":55,"name":"NRGStadium"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51548,"week":6,"startTime":"2019-10-13T20:05:00.000Z","endedTime":null,"awayTeam":{"id":68,"abbreviation":"ATL"},"homeTeam":{"id":76,"abbreviation":"ARI"},"venue":{"id":43,"name":"UniversityofPhoenixStadium"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51558,"week":7,"startTime":"2019-10-20T17:00:00.000Z","endedTime":null,"awayTeam":{"id":77,"abbreviation":"LA"},"homeTeam":{"id":68,"abbreviation":"ATL"},"venue":{"id":44,"name":"GeorgiaDome"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51568,"week":8,"startTime":"2019-10-27T17:00:00.000Z","endedTime":null,"awayTeam":{"id":79,"abbreviation":"SEA"},"homeTeam":{"id":68,"abbreviation":"ATL"},"venue":{"id":44,"name":"GeorgiaDome"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51600,"week":10,"startTime":"2019-11-10T19:00:00.000Z","endedTime":null,"awayTeam":{"id":68,"abbreviation":"ATL"},"homeTeam":{"id":70,"abbreviation":"NO"},"venue":{"id":62,"name":"Mercedes-BenzStadium"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51610,"week":11,"startTime":"2019-11-17T19:00:00.000Z","endedTime":null,"awayTeam":{"id":68,"abbreviation":"ATL"},"homeTeam":{"id":69,"abbreviation":"CAR"},"venue":{"id":47,"name":"BankofAmericaStadium"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51628,"week":12,"startTime":"2019-11-24T19:00:00.000Z","endedTime":null,"awayTeam":{"id":71,"abbreviation":"TB"},"homeTeam":{"id":68,"abbreviation":"ATL"},"venue":{"id":44,"name":"GeorgiaDome"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51639,"week":13,"startTime":"2019-11-29T02:20:00.000Z","endedTime":null,"awayTeam":{"id":70,"abbreviation":"NO"},"homeTeam":{"id":68,"abbreviation":"ATL"},"venue":{"id":44,"name":"GeorgiaDome"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51654,"week":14,"startTime":"2019-12-08T19:00:00.000Z","endedTime":null,"awayTeam":{"id":69,"abbreviation":"CAR"},"homeTeam":{"id":68,"abbreviation":"ATL"},"venue":{"id":44,"name":"GeorgiaDome"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51681,"week":15,"startTime":"2019-12-15T22:25:00.000Z","endedTime":null,"awayTeam":{"id":68,"abbreviation":"ATL"},"homeTeam":{"id":78,"abbreviation":"SF"},"venue":{"id":68,"name":"Levi'sStadium"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51693,"week":16,"startTime":"2019-12-22T19:00:00.000Z","endedTime":null,"awayTeam":{"id":66,"abbreviation":"JAX"},"homeTeam":{"id":68,"abbreviation":"ATL"},"venue":{"id":44,"name":"GeorgiaDome"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}},{"schedule":{"id":51703,"week":17,"startTime":"2019-12-29T19:00:00.000Z","endedTime":null,"awayTeam":{"id":68,"abbreviation":"ATL"},"homeTeam":{"id":71,"abbreviation":"TB"},"venue":{"id":71,"name":"RaymondJamesStadium"},"venueAllegiance":"NEUTRAL","scheduleStatus":"NORMAL","originalStartTime":null,"delayedOrPostponedReason":null,"playedStatus":"UNPLAYED","attendance":null,"officials":[],"broadcasters":[],"weather":null},"score":{"currentQuarter":null,"currentQuarterSecondsRemaining":null,"currentIntermission":null,"teamInPossession":null,"currentDown":null,"currentYardsRemaining":null,"lineOfScrimmage":null,"awayScoreTotal":null,"homeScoreTotal":null,"quarters":[]}}]}"""
      val expectedUri =
        uri"$baseUrl/v2.1/pull/nfl/upcoming/games.json?team=$teamAbbreviation&stats=none"
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri == expectedUri)
        .thenRespond(nflTeamGameLogResponse)

      val mySportsFeedService =
        new MySportsFeedService("username", "password", "baseUrl", backend)

      mySportsFeedService.NFL
        .getGameLogs(Seq(teamAbbreviation))
        .map(gameLogs => {
          assert(gameLogs.games.length === 16)
          assert(gameLogs.games.count(_.schedule.homeTeam.abbreviation == teamAbbreviation) === 8)
          assert(gameLogs.games.head.schedule.week === 1)
          assert(
            gameLogs.games.head.schedule.startTime === Instant.parse("2019-09-08T17:00:00.000Z")
          )
        })
    }

    "get an NFL team's standings" in {
      val nflStandingsResponse =
        """{"lastUpdatedOn":"2019-06-28T16:45:57.575Z","teams":[{"team":{"id":68,"city":"Atlanta","name":"Falcons","abbreviation":"ATL","homeVenue":{"id":44,"name":"Georgia Dome"},"teamColoursHex":[],"socialMediaAccounts":[],"officialLogoImageSrc":null},"stats":{"gamesPlayed":16,"standings":{"wins":7,"losses":9,"ties":0}},"overallRank":{"rank":18,"gamesBack":6.0},"conferenceRank":{"conferenceName":"NFC","rank":8,"gamesBack":6.0},"divisionRank":{"divisionName":"NFC South","rank":2,"gamesBack":6.0},"playoffRank":{"conferenceName":"NFC","divisionName":null,"appliesTo":"CONFERENCE","rank":8}}],"references":{"teamStatReferences":[{"category":"Team Standings","fullName":"wins","description":"Wins","abbreviation":"W","type":"INTEGER"},{"category":"Team Standings","fullName":"losses","description":"Losses","abbreviation":"L","type":"INTEGER"},{"category":"Team Standings","fullName":"ties","description":"Ties","abbreviation":"T","type":"INTEGER"}]}}"""
      val expectedUri =
        uri"$baseUrl/v2.1/pull/nfl/upcoming/standings.json?team=$teamAbbreviation&stats=W,L,T"
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri == expectedUri)
        .thenRespond(nflStandingsResponse)

      val mySportsFeedService =
        new MySportsFeedService("username", "password", "baseUrl", backend)

      mySportsFeedService.NFL
        .getStandings(Seq(teamAbbreviation))
        .map(standings => {
          assert(standings.teams.length === 1)
          assert(standings.teams.head.stats.standings.losses === 9)
          assert(standings.teams.head.stats.standings.wins === 7)
          assert(standings.teams.head.stats.standings.ties === 0)
          assert(standings.teams.head.team.abbreviation == teamAbbreviation)
        })
    }
  }
}
