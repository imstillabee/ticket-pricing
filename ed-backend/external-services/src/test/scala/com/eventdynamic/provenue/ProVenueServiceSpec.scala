package com.eventdynamic.provenue

import com.eventdynamic.provenue.models._
import com.softwaremill.sttp.Uri.QueryFragment
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.softwaremill.sttp.testing.SttpBackendStub
import org.scalatest.AsyncWordSpec

import scala.io.Source

class ProVenueServiceSpec extends AsyncWordSpec {

  final val testFiles = "external-services/test-files/provenue"

  private val agent = "agent"
  private val apiKey = "apiKey"
  private val appId = "appId"
  private val baseUrl = "http://baseUrl"
  private val username = "username"
  private val password = "password"
  private val supplierId = 1
  private val proxyBaseUrl = "http://baseUrl"
  private val sandboxEmail = Some("sandbox@email.com")

  "ProVenueService#ping" should {
    "handle success" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains("test/ping"))
        .thenRespond(
          """{"ping":{"clientId":"sandbox","proVenueAppVersion":"5.0.00.10","currentUTC":"2019-01-23T14:19:09.139-08:00","timeZoneCode":"America/Los_Angeles"}}"""
        )
      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .ping()
        .map(res => {
          assert(res.code == 200)
          assert(res.body.isRight)
        })
    }

    "handle server error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains("test/ping"))
        .thenRespondServerError()
      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .ping()
        .map(res => {
          assert(res.code == 500)
          assert(res.body.isLeft)
        })
    }
  }

  "ProVenueService#getPriceStructure" should {
    "handle success" in {
      val eventId = 9007
      val saleType = "SINGLE"

      val responseBody =
        Source
          .fromFile(s"$testFiles/ProVenuePriceStructureResponse.json")
          .mkString
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(req => {
          req.uri.path.contains("priceStructures") &&
          req.uri.queryFragments.contains(QueryFragment.KeyValue("eventId", eventId.toString)) &&
          req.uri.queryFragments.contains(QueryFragment.KeyValue("saleType", saleType))
        })
        .thenRespond[String](responseBody)
      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .getPriceStructure(eventId, saleType)
        .map(res => {
          assert(res.code == 200)
          assert(res.body.isRight)

          val body = res.body.right.get.priceStructure

          assert(body.pricePoints.nonEmpty)
          assert(body.buyerTypeRefs.nonEmpty)
          assert(body.priceScaleRefs.nonEmpty)
        })
    }

    "handle server error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("priceStructures"))
        .thenRespondServerError()
      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .getPriceStructure(9007, "SINGLE")
        .map(res => {
          assert(res.code == 500)
          assert(res.body.isLeft)
        })
    }
  }

  "ProVenueService#bulkUpdatePricePoints" should {
    "handle success" in {
      val priceStructureId = 1234

      val responseBody =
        """
          |{
          |    "priceActions": {
          |        "requestCount": 1,
          |        "errorCount": 0,
          |        "createdCount": 0,
          |        "notProcessedCount": 0,
          |        "updatedCount": 1,
          |        "deletedCount": 0,
          |        "clearedCount": 0,
          |        "unmodifiedCount": 0
          |    }
          |}
        """.stripMargin

      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(
          _.uri.toString() == s"$baseUrl/priceStructures/$priceStructureId/prices"
        )
        .thenRespond[String](responseBody)
      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .bulkUpdatePricePoints(priceStructureId, Seq())
        .map(res => {
          res.body.map(_.isInstanceOf[ProVenuePricePointBulkResponse])
          assert(res.code == 200)
          assert(res.body.isRight)
        })
    }

    "handle server error" in {
      val priceStructureId = 1234

      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(
          _.uri.toString() == s"$baseUrl/priceStructures/$priceStructureId/prices"
        )
        .thenRespondServerError()
      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .bulkUpdatePricePoints(priceStructureId, Seq())
        .map(res => {
          assert(res.code == 500)
          assert(res.body.isLeft)
        })
    }
  }

  "ProVenueService#lockSeats" should {
    val eventId = 1
    val buyerTypeId = 1
    val seatIds = Seq(1, 2, 3)

    "handle success" in {
      val responseBody =
        Source
          .fromFile(s"$testFiles/ProVenueCartResponse.json")
          .mkString

      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString == s"$baseUrl/carts/lock")
        .thenRespond(responseBody)

      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .lockSeats(eventId, buyerTypeId, seatIds)
        .map(res => {
          assert(res.code == 200)
          assert(res.body.isRight)
          assert(res.body.exists(_.id == 140846085))
        })
    }

    "handle server error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString == s"$baseUrl/carts/lock")
        .thenRespondServerError()

      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .lockSeats(eventId, buyerTypeId, seatIds)
        .map(res => {
          assert(res.code == 500)
          assert(res.body.isLeft)
        })
    }
  }

  "ProVenueService#checkout" should {
    val cartId = 1234
    val amount = ProVenueMoney("USD", 19.99)
    val deliveryMethodId = 12
    val paymentMethodId = 23
    val patronAccountId = 34

    "handle success" in {
      val responseBody = Source
        .fromFile(
          "external-services/src/test/scala/com/eventdynamic/provenue/ProVenueCheckoutResponse.json"
        )
        .mkString

      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString == s"$baseUrl/carts/$cartId/checkout")
        .thenRespond(responseBody)

      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .checkout(cartId, amount, deliveryMethodId, paymentMethodId, patronAccountId)
        .map(res => {
          assert(res.isSuccess)
          assert(res.body.isRight)
        })
    }

    "handle server error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString == s"$baseUrl/carts/$cartId/checkout")
        .thenRespondServerError()

      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .checkout(cartId, amount, deliveryMethodId, paymentMethodId, patronAccountId)
        .map(res => {
          assert(res.isServerError)
          assert(res.body.isLeft)
        })
    }
  }

  "ProVenueService#print" should {
    val deliveryId = 1234

    "handle success" in {
      val responseBody =
        Source
          .fromFile(s"$testFiles/ProVenuePrintResponse.json")
          .mkString

      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString == s"$baseUrl/print")
        .thenRespond(responseBody)

      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .print(deliveryId)
        .map(res => {
          assert(res.isSuccess)
          assert(res.body.isRight)
        })
    }

    "handle server error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString == s"$baseUrl/print")
        .thenRespondServerError()

      val proVenueService = new ProVenueService(
        ProVenueConfig(agent, apiKey, appId, baseUrl, username, password, supplierId, sandboxEmail),
        backend
      )

      proVenueService
        .print(deliveryId)
        .map(res => {
          assert(res.code == 500)
          assert(res.body.isLeft)
        })
    }
  }
}
