package com.eventdynamic.skybox

import java.time.Instant
import java.time.temporal.ChronoUnit

import com.eventdynamic.skybox.models.{SkyboxBulkPriceItem, SkyboxListing}
import com.softwaremill.sttp._
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.softwaremill.sttp.testing.SttpBackendStub
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{AsyncWordSpec, BeforeAndAfterEach, Matchers}

class SkyboxServiceSpec
    extends AsyncWordSpec
    with BeforeAndAfterEach
    with ScalaFutures
    with Matchers {

  // Example response with a subset of fields
  private val skyboxInventory =
    """
      |{
      |  "id": 49656875,
      |  "inHandDate": "2018-12-06",
      |  "eventId": 2829841,
      |  "quantity": 10,
      |  "section": "123",
      |  "row": "1",
      |  "lowSeat": 1,
      |  "highSeat": 10,
      |  "cost": 1000.00,
      |  "ticketIds": [
      |    183571571,
      |    183571572,
      |    183571573,
      |    183571574,
      |    183571575,
      |    183571576,
      |    183571577,
      |    183571578,
      |    183571580,
      |    183571583
      |  ],
      |  "splitType": "DEFAULT",
      |  "listPrice": 50.00,
      |  "status": "AVAILABLE",
      |  "customerId": 222,
      |  "unitCostAverage": 850.00
      |}
    """.stripMargin

  private val appKey = "testAppKey"
  private val baseUrl = "testBaseUrl"
  private val apiKey = "testApiKey"
  private val accountId = 1
  private val skyboxConfig = SkyboxConfig(appKey, baseUrl, apiKey, accountId)

  private val req = sttp.headers(
    "Content-Type" -> "application/json",
    "X-Application-Token" -> appKey,
    "X-Api-Token" -> apiKey,
    "X-Account" -> accountId.toString
  )

  // constants
  val eventIds = Seq(1, 2)
  val afterDate = Instant.now().minus(1, ChronoUnit.DAYS)

  // clearing mocked services
  override def beforeEach(): Unit = {
    super.beforeEach()
  }

  // tests
  "SkyboxService#bulkUpdatePrices" should {
    "return an empty response from an unauthorized API request" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains("inventory"))
        .thenRespondWithCode(401, "Not authorized")

      val skyboxService =
        new SkyboxService(skyboxConfig, backend)

      skyboxService
        .bulkUpdatePrices(Seq(SkyboxBulkPriceItem(id = 1, accountId = 1, listPrice = 1.00)))
        .map(result => {
          assert(!result)
        })
    }

    "return 200 OK: Updated from a successful API request " in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains("inventory"))
        .thenRespondWithCode(200, "Updated")

      val skyboxService =
        new SkyboxService(skyboxConfig, backend)

      skyboxService
        .bulkUpdatePrices(Seq(SkyboxBulkPriceItem(id = 1, accountId = 1, listPrice = 1.00)))
        .map(result => {
          assert(result)
        })
    }
  }

  "SkyboxService#getInventory" should {
    "return an empty response from a bad API request" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains("inventory"))
        .thenRespondWithCode(401, "Not authorized")

      val skyboxService =
        new SkyboxService(skyboxConfig, backend)

      skyboxService
        .getInventory(eventIds)
        .map(result => {
          assert(result.code == 401)
          assert(result.body.isLeft)
        })
    }

    "return a SkyboxInventoryGetResponse from a successful API request" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains("inventory"))
        .thenRespond(s"""
            |{
            | "rows": [
            |   $skyboxInventory
            | ]
            |}
          """.stripMargin)

      val skyboxService =
        new SkyboxService(skyboxConfig, backend)

      skyboxService
        .getInventory(eventIds)
        .map(result => {
          assert(result.code == 200)
          assert(result.body.isRight)
        })
    }
  }

  "SkyboxService#getInventoryForRow" should {
    "return a page of skybox inventory from a successfull API request" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains("inventory"))
        .thenRespond(s"""
            |{
            | "rows": [
            |   $skyboxInventory
            | ],
            | "rowCount": 1
            |}
          """.stripMargin)

      val skyboxService = new SkyboxService(skyboxConfig, backend)

      skyboxService
        .getInventoryForRow(1, "section", "row")
        .map(result => {
          assert(result.code == 200)
          assert(result.body.isRight)
        })
    }
  }

  "SkyboxService#getInvoices" should {
    "return an empty response from a bad API request" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains("inventory/sold"))
        .thenRespondWithCode(401, "Not authorized")

      val skyboxService =
        new SkyboxService(skyboxConfig, backend)

      skyboxService
        .getInvoices(eventIds, afterDate)
        .map(result => {
          assert(result.code == 401)
          assert(result.body.isLeft)
        })
    }

    "return a SkyboxInvoiceGetResponse from a successful API request" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains("inventory/sold"))
        .thenRespond(
          """{"rows":[{"id":1, "invoiceDate": "2019-01-29T19:59:31.000Z", "unitTicketSales": 400.00, "section": "1", "row":"1", "seatNumbers":"1", "eventId":1, "customerId": 222, "lines": []}], "rowCount": 1}"""
        )

      val skyboxService =
        new SkyboxService(skyboxConfig, backend)

      skyboxService
        .getInvoices(eventIds, afterDate)
        .map(result => {
          assert(result.code == 200)
          assert(result.body.isRight)
        })
    }
  }

  "SkyboxService#getInventoryById" should {
    "parse inventory response" in {
      val inventoryId = 1
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains(s"/inventory/$inventoryId"))
        .thenRespondWithCode(200, skyboxInventory)

      val skyboxService =
        new SkyboxService(skyboxConfig, backend)

      skyboxService
        .getInventoryById(inventoryId)
        .map(result => {
          result.code shouldBe 200
          result.body.isRight shouldBe true
          result.body.right.get shouldBe a[SkyboxListing]
        })
    }

    "handle http errors" in {
      val body = "some string body"
      val inventoryId = 1
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains(s"/inventory/$inventoryId"))
        .thenRespondWithCode(500, body)

      val skyboxService =
        new SkyboxService(skyboxConfig, backend)

      skyboxService
        .getInventoryById(inventoryId)
        .map(result => {
          result.code shouldBe 500
          result.body.isLeft shouldBe true
          result.body.left.get shouldBe body
        })
    }
  }

  "SkyboxService#getInvoiceTickets" should {
    "return SkyboxTickets for a successful API request" in {
      val invoiceId = 123
      val lineId = 456

      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains(s"invoices/$invoiceId/lines/$lineId/tickets"))
        .thenRespond("""
            |[
            |  {
            |    "id": 183571571,
            |    "seatNumber": 1,
            |    "fileName": null,
            |    "barCode": null,
            |    "inventoryId": 74351383,
            |    "invoiceLineId": 106854854,
            |    "purchaseLineId": 66933002,
            |    "section": "123",
            |    "row": "1",
            |    "notes": "",
            |    "cost": 100.00,
            |    "faceValue": 0.00,
            |    "sellPrice": 500.0000,
            |    "stockType": "ELECTRONIC",
            |    "eventId": 2829841,
            |    "accountId": 3603,
            |    "status": "SOLD",
            |    "base64FileBytes": null,
            |    "disclosures": [],
            |    "attributes": [],
            |    "createdDate": "2018-12-06T21:03:27.000Z",
            |    "createdBy": null,
            |    "lastUpdate": "2019-05-06T20:38:55.000Z",
            |    "lastUpdateBy": null
            |  }
            |]
          """.stripMargin)

      val skyboxService = new SkyboxService(skyboxConfig, backend)

      skyboxService
        .getInvoiceTickets(invoiceId, lineId)
        .map(result => {
          assert(result.code == 200)
          assert(result.body.isRight)
        })
    }
  }

  "SkyboxService#createInvoice" should {
    "create a Skybox invoice returning the created record" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(r => {
          r.uri.toString().contains("/invoices")
          r.method == Method.POST
        })
        .thenRespond("""
            |{
            |  "id": 27914736,
            |  "customerId": 2988421,
            |  "accountId": 3603,
            |  "internalId": null,
            |  "lines": [
            |    {
            |      "id": 106866947,
            |      "accountId": 3603,
            |      "targetId": 27914736,
            |      "quantity": 1,
            |      "description": "Miami Marlins at New York Mets\nCiti Field, New York, NY\nSep 26, 2019 7:10PM\nSection: 519, Row: 6, Seat: 4",
            |      "amount": 1000.0000,
            |      "lineType": "INVOICE",
            |      "lineItemType": "INVENTORY",
            |      "itemIds": null,
            |      "inventory": {
            |        "inHandDate": "2019-02-01",
            |        "id": 74361915,
            |        "accountId": 0,
            |        "eventId": 2820160,
            |        "quantity": 1,
            |        "notes": "",
            |        "section": "519",
            |        "row": "6",
            |        "secondRow": null,
            |        "lowSeat": 4,
            |        "highSeat": 4,
            |        "cost": 15.56,
            |        "faceValue": null,
            |        "tickets": null,
            |        "ticketIds": null,
            |        "stockType": "ELECTRONIC",
            |        "splitType": "DEFAULT",
            |        "customSplit": null,
            |        "listPrice": 11.00,
            |        "expectedValue": null,
            |        "publicNotes": null,
            |        "attributes": [],
            |        "status": "DEPLETED",
            |        "inHandDaysBeforeEvent": null,
            |        "lastPriceUpdate": null,
            |        "createdBy": null,
            |        "lastUpdateBy": null,
            |        "version": 0,
            |        "tags": null,
            |        "seatType": "CONSECUTIVE",
            |        "eventMapping": {
            |          "eventName": "Miami Marlins at New York Mets",
            |          "venueName": "Citi Field",
            |          "eventDate": "2019-09-26T19:10:00.000",
            |          "valid": true
            |        },
            |        "mappingId": 0,
            |        "exchangePosId": 0,
            |        "broadcast": null,
            |        "zoneSeating": false,
            |        "electronicTransfer": null,
            |        "optOutAutoPrice": null,
            |        "hideSeatNumbers": null,
            |        "vsrOption": null,
            |        "replenishmentGroupId": null,
            |        "replenishmentGroup": null,
            |        "shownQuantity": null,
            |        "ticketsMerged": null,
            |        "ticketsSplit": null
            |      },
            |      "cancelled": false,
            |      "delete": false,
            |      "cancel": null,
            |      "fillLineId": null,
            |      "inventoryIds": [
            |        74361915
            |      ],
            |      "createdDate": "2019-05-06T22:02:57.000Z",
            |      "cretedBy": null,
            |      "lastUpdate": "2019-05-06T22:02:57.000Z",
            |      "lastUpdateBy": null
            |    }
            |  ],
            |  "salesTerm": "NET0",
            |  "paymentMethod": "OTHER",
            |  "paymentRef": null,
            |  "deliveryMethod": "PAPERLESS",
            |  "shippingAddressId": 5323322,
            |  "billingAddressId": 5323322,
            |  "taxAmount": 0.0000,
            |  "shippingAmount": 0.0000,
            |  "otherAmount": 0.0000,
            |  "internalNotes": null,
            |  "publicNotes": null,
            |  "createdDate": "2019-05-06T22:02:57.000Z",
            |  "lastUpdate": "2019-05-06T22:02:57.000Z",
            |  "dueDate": "2019-05-06T22:02:57.000Z",
            |  "tags": null,
            |  "createdBy": "Event Dynamic",
            |  "lastUpdateBy": null,
            |  "paymentStatus": "PAID",
            |  "fulfillmentStatus": "PENDING",
            |  "externalRef": null,
            |  "airbill": null,
            |  "status": null,
            |  "currencyCode": "USD",
            |  "payments": [
            |    {
            |      "id": 10059067,
            |      "accountId": 3603,
            |      "referenceNumber": null,
            |      "amount": 1000.0000,
            |      "paymentDate": "2019-05-06T22:02:57.000Z",
            |      "userId": 14695,
            |      "entityId": 27914736,
            |      "paymentType": "INVOICE",
            |      "paymentMethod": null,
            |      "invoiceId": 27914736
            |    }
            |  ],
            |  "invoiceDeliveryLink": null,
            |  "notes": [],
            |  "fulfillmentDate": null,
            |  "fulfillmentByUserId": null,
            |  "outstandingBalance": 0.0000,
            |  "barcodesEntered": true,
            |  "filesUploaded": false
            |}
          """.stripMargin)

      val skyboxService = new SkyboxService(skyboxConfig, backend)

      skyboxService
        .createInvoice(1, 19.99, Seq(1, 2, 3))
        .map(result => {
          assert(result.code == 200)
          assert(result.body.isRight)
        })
    }
  }
}
