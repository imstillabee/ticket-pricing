package com.eventdynamic.stubhub

import java.sql.Timestamp

import com.eventdynamic.stubhub.models._
import com.softwaremill.sttp.StringBody
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.softwaremill.sttp.testing.SttpBackendStub
import org.scalatest.AsyncWordSpec
import play.api.libs.json.Json

class StubHubServiceSpec extends AsyncWordSpec {

  val getListingsResponse =
    s"""{"listings": { "numFound": 1, "listing": [{"id": "1", "eventId": "1000", "status": "ACTIVE", "pricePerTicket" : { "amount": 10.00 }, "section": "Section", "rows": "4", "seats": "1,2,3,4,5"}]}}"""

  val postListingsResponse =
    """{"listing": {"id":"1", "status": "ACTIVE", "deliveryOption": "BARCODE" }}"""
  val updateListingResponse = """{"listing": {"id":"1", "status": "ACTIVE" }}"""
  val deleteListingResponse = """{"listing": {"id":"1", "status": "DELETED" }}"""

  val loginResponse =
    """
      {
        "refresh_token_expires_in" : "18143999",
        "refresh_token_status" : "approved",
        "api_product_list" : "[ShOauth, Event Catalog, Order Management, Market Intel, Sales Management, Inventory Management]",
        "api_product_list_json" : [ "ShOauth", "Event Catalog", "Order Management", "Market Intel", "Sales Management", "Inventory Management" ],
        "organization_name" : "stubhub",
        "developer.email" : "test@test.com",
        "token_type" : "BearerToken",
        "issued_at" : "1551124371840",
        "client_id" : "bbbqqTU2IDEbgbYWZgynyKTPsdSgf",
        "access_token" : "ZM2UYuH6qa8Pq96uqLf0JZ33SxJy",
        "refresh_token" : "OIQpy1R4yqSd5D32Mdbgd2SZiqWIRI2G",
        "application_name" : "6a97c562-1c3e-4804-a56a-0dbb222bfe36",
        "scope" : "",
        "refresh_token_issued_at" : "1551124371840",
        "expires_in" : "15551999",
        "refresh_count" : "0",
        "status" : "approved"
      }
    """

  private val baseUrl = "baseURL"
  private val consumerKey = "consumerKey"
  private val consumerSecret = "consumerSecret"
  private val username = "username"
  private val password = "password"

  val tickets = Seq(
    StubHubTicket("1", "1"),
    StubHubTicket("1", "2"),
    StubHubTicket("2", "1"),
    StubHubTicket("1", "1"),
    StubHubTicket("1", "1")
  )

  val stubHubInventory = Seq(
    StubHubInventory(4, 1, true, "1", "1", true, "4", 10.00, None, None),
    StubHubInventory(5, 1, true, "1", "1", true, "1", 10.00, None, None),
    StubHubInventory(6, 1, true, "1", "1", true, "3", 10.00, None, None)
  )

  "StubHubService#login" should {
    "handle success" in {
      val backend = SttpBackendStub(OkHttpFutureBackend()).whenAnyRequest
        .thenRespond(loginResponse)

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .login()
        .map(res => {
          assert(res.isInstanceOf[ApiSuccess])
        })
    }

    "handle 400 > errors" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespondWithCode(401, """{"code": "invalid token", "description": "Renew token"}""")

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .login()
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 401)
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .code === "invalid token"
          )
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .description === "Renew token"
          )
        })
    }

    "handle error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespondServerError()

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .login()
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 500)
        })
    }
  }

  "StubHubService#delist" should {
    "handle success" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespond(updateListingResponse)

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .delist(tickets, listingId = 1)
        .map(res => {
          assert(res.isInstanceOf[ApiSuccess])
        })
    }

    "handle 400 > errors" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespondWithCode(401, """{"code": "invalid token", "description": "Renew token"}""")

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .delist(tickets, listingId = 1)
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 401)
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .code === "invalid token"
          )
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .description === "Renew token"
          )
        })
    }

    "handle error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespondServerError()

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .delist(tickets, listingId = 1)
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 500)
        })
    }
  }

  "StubHubService#fulfill" should {
//    "handle success" in {
//      val backend = SttpBackendStub(OkHttpFutureBackend())
//        .whenRequestMatches(_.uri.path.contains("fulfillment"))
//        .thenRespond()
//
//      val stubHubService =
//    new StubHubService(
//      StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
//      backend
//    )
//
//      stubHubService
//        .delist(tickets, listingId = 1)
//        .map(res => {
//          assert(res.isInstanceOf[ApiSuccess])
//        })
//    }

    "handle 400 > errors" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("barcode"))
        .thenRespondWithCode(401, """{"code": "invalid token", "description": "Renew token"}""")

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .fulfill(1, Seq())
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 401)
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .code === "invalid token"
          )
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .description === "Renew token"
          )
        })
    }

    "handle error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("barcode"))
        .thenRespondServerError()

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .fulfill(1, Seq())
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 500)
        })
    }
  }

  "StubHubService#getInventory" should {
    "handle success" in {
      val start = 1
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(req => {
          req.uri.path.contains("listings") &&
          req.uri.paramsMap == Map("rows" -> "200", "start" -> start.toString)
        })
        .thenRespond(getListingsResponse)

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .getInventory(start)
        .map(res => {
          assert(res.isInstanceOf[ApiSuccess])
          val body = res
            .asInstanceOf[ApiSuccess]
            .body
            .asInstanceOf[StubHubAccountManagementResponse]

          assert(body.numFound == 1)
          assert(body.listings.get.length == 1)
        })
    }

    "handle success with event filter" in {
      val start = 1
      val eventId = "2"

      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(req => {
          req.uri.path.contains("listings") &&
          req.uri.paramsMap == Map(
            "rows" -> "200",
            "start" -> start.toString,
            "filters" -> s"EVENT:$eventId"
          )
        })
        .thenRespond(getListingsResponse)

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .getInventory(start = start, eventId = Some(eventId))
        .map(res => {
          assert(res.isInstanceOf[ApiSuccess])
          val body = res
            .asInstanceOf[ApiSuccess]
            .body
            .asInstanceOf[StubHubAccountManagementResponse]

          assert(body.numFound == 1)
          assert(body.listings.get.length == 1)
        })
    }

    "handle 400 > errors" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespondWithCode(401, """{"code": "invalid token", "description": "Renew token"}""")

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .getInventory(0)
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 401)
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .code === "invalid token"
          )
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .description === "Renew token"
          )
        })
    }

    "handle error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespondServerError()

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .getInventory(0)
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 500)
        })
    }
  }

  "StubHubService#list" should {
    "handle success" in {
      val inhandDate = Timestamp.valueOf("2019-12-30 00:00:00")
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(req => {
          val body = req.body match {
            case StringBody(s, _, _) => Json.parse(s)
            case _                   => Json.toJson("{}")
          }

          val stockType = body \ "stockTypes" \ 0

          req.uri.path.contains("listings") &&
          (stockType \ "inhandDate").validateOpt[String].get.contains("2019-12-30") &&
          (stockType \ "adjustInhandDate").validateOpt[Boolean].get.contains(true) &&
          (stockType \ "stockType").validateOpt[String].get.contains("BARCODE")
        })
        .thenRespond(postListingsResponse)

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .list(stubHubInventory, "1", 10.00, inhandDate)
        .map(res => {
          assert(res.isInstanceOf[ApiSuccess])
          assert(
            res
              .asInstanceOf[ApiSuccess]
              .body
              .asInstanceOf[StubHubListingResponse]
              .status === "ACTIVE"
          )
        })
    }

    "handle 400 > errors" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespondWithCode(401, """{"code": "invalid token", "description": "Renew token"}""")

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .list(stubHubInventory, "1", 10.00)
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 401)
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .code === "invalid token"
          )
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .description === "Renew token"
          )
        })
    }

    "handle error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespondServerError()

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .list(stubHubInventory, "1", 10.00)
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 500)
        })
    }
  }

  "StubHubService#updateListing" should {
    "handle success" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespond(updateListingResponse)

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .updateListing(1, 20.00)
        .map(res => {
          assert(res.isInstanceOf[ApiSuccess])
          assert(
            res
              .asInstanceOf[ApiSuccess]
              .body
              .asInstanceOf[StubHubListingResponse]
              .status === "ACTIVE"
          )
        })
    }

    "handle 400 > errors" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespondWithCode(401, """{"code": "invalid token", "description": "Renew token"}""")

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .updateListing(1, 20.00)
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 401)
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .code === "invalid token"
          )
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .description === "Renew token"
          )
        })
    }

    "handle error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespondServerError()

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .updateListing(1, 20.00)
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 500)
        })
    }
  }

  "StubHubService#deleteListing" should {
    "handle success" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespond(deleteListingResponse)

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .deleteListing(1)
        .map(res => {
          assert(res.isInstanceOf[ApiSuccess])
          assert(
            res
              .asInstanceOf[ApiSuccess]
              .body
              .asInstanceOf[StubHubListingResponse]
              .status === "DELETED"
          )
        })
    }

    "handle 400 > errors" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespondWithCode(401, """{"code": "invalid token", "description": "Renew token"}""")

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .deleteListing(1)
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 401)
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .code === "invalid token"
          )
          assert(
            res
              .asInstanceOf[ApiError]
              .body
              .asInstanceOf[StubHubErrorResponse]
              .description === "Renew token"
          )
        })
    }

    "handle error" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("oauth"))
        .thenRespond(loginResponse)
        .whenRequestMatches(_.uri.path.contains("listings"))
        .thenRespondServerError()

      val stubHubService =
        new StubHubService(
          StubHubConfig(baseUrl, consumerKey, consumerSecret, username, password),
          backend
        )

      stubHubService
        .deleteListing(1)
        .map(res => {
          assert(res.isInstanceOf[ApiError])
          assert(res.asInstanceOf[ApiError].code === 500)
        })
    }
  }
}
