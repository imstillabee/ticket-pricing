package com.eventdynamic.stubhub.models

import org.scalatest.WordSpec

class StubHubAccountManagementResponseSpec extends WordSpec {
  "create StubHubListing from response" in {
    val response =
      s"""{"listings": { "numFound": 1, "listing": [{"id": "1344475502", "eventId": "1000", "status": "ACTIVE", "pricePerTicket" : { "amount": 10.00 }, "section": "Section", "rows": "4", "seats": "1,2,3,4,5"} ]} }"""

    val stubHubListings = StubHubAccountManagementResponse.fromResponse(response)

    assert(stubHubListings.isDefined)
    assert(stubHubListings.get.listings.isDefined)
    assert(stubHubListings.get.listings.get.length === 1)
    assert(stubHubListings.get.listings.get.head.seats === "1,2,3,4,5")
  }
}
