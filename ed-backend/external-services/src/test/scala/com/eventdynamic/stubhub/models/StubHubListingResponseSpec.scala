package com.eventdynamic.stubhub.models

import org.scalatest.WordSpec

class StubHubListingResponseSpec extends WordSpec {

  "create StubHubResponse from response" in {
    val response = """{"listing": {"id": "1", "status": "ACTIVE", "deliveryOption": "BARCODE" }}"""

    val stubHubListingResponse = StubHubListingResponse.fromResponse(response)

    assert(stubHubListingResponse.isDefined)
    assert(stubHubListingResponse.get.listingId.toInt === 1)
    assert(stubHubListingResponse.get.deliveryOption.get === "BARCODE")
    assert(stubHubListingResponse.get.status === "ACTIVE")
  }
}
