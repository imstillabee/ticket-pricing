package com.eventdynamic.tdcreplicatedproxy

import java.time.Instant
import java.time.temporal.ChronoUnit

import com.eventdynamic.tdcreplicatedproxy.models.{TdcBuyerType, TdcInventory, TdcTransaction}
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.softwaremill.sttp.testing.SttpBackendStub
import org.scalatest.AsyncWordSpec
import play.api.libs.json.Json

class TdcReplicatedProxyServiceSpec
    extends AsyncWordSpec
    with TdcTransaction.format
    with TdcInventory.format
    with TdcBuyerType.format {
  private val conf =
    TdcReplicatedProxyConfig(proxyBaseUrl = "http://proxyBaseUrl")

  "TdcReplicatedProxyService#getTransactions" should {
    "return 200 OK with transactions" in {
      val responseBody =
        """
          |[
          |  {
          |    "id": 15777264,
          |    "createdAt": "2019-01-10T20:33:29.005Z",
          |    "timestamp": "2019-01-10T20:33:28.975Z",
          |    "eventId": 9266,
          |    "buyerTypeCode": "BUNDLE",
          |    "seatId": 746624,
          |    "price": 46.26,
          |    "section": "section",
          |    "row": "row",
          |    "seatNumber": "123",
          |    "transactionTypeCode": "SA",
          |    "priceScaleId": 1004,
          |    "holdCodeType": "O"
          |  }
          |]
        """.stripMargin

      val before = Instant.now()
      val after = before.minus(1, ChronoUnit.HOURS)
      val eventIds = Seq(3, 4, 5)

      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(req => {
          req.uri.toString.contains("transactions") &&
          req.uri.paramsSeq.contains("after" -> after.toString) &&
          req.uri.paramsSeq.contains("before" -> before.toString) &&
          eventIds.forall(id => req.uri.paramsSeq.contains("eventId" -> id.toString))
        })
        .thenRespondWithCode(200, responseBody)

      val service = new TdcReplicatedProxyService(conf, backend)

      service
        .getTransactions(after, before, eventIds)
        .map(result => {
          assert(result.isSuccess)
          assert(result.body.isRight)
          assert(Json.toJson(result.body.right.get) == Json.parse(responseBody))
        })
    }
  }

  "TdcReplicatedProxyService#getInventory" should {
    "return 200 OK with inventory" in {
      val responseBody =
        """
          |[
          |  {
          |    "eventId": 3,
          |    "seatId": 1,
          |    "priceScaleId": 1,
          |    "createdAt": "2019-01-10T20:33:29.005Z",
          |    "modifiedAt": "2019-01-10T20:33:28.975Z",
          |    "listedPrice": 46.26,
          |    "section": "section",
          |    "row": "row",
          |    "seatNumber": "123",
          |    "holdCodeType": "O"
          |  },
          |  {
          |    "eventId": 4,
          |    "seatId": 2,
          |    "priceScaleId": 1,
          |    "createdAt": "2019-01-10T20:33:29.005Z",
          |    "modifiedAt": "2019-01-10T20:33:28.975Z",
          |    "listedPrice": 56.26,
          |    "section": "section",
          |    "row": "row",
          |    "seatNumber": "123",
          |    "holdCodeType": "H"
          |  },
          |  {
          |    "eventId": 6,
          |    "seatId": 3,
          |    "priceScaleId": 1,
          |    "createdAt": "2019-01-10T20:33:29.005Z",
          |    "modifiedAt": "2019-01-10T20:33:28.975Z",
          |    "section": "section",
          |    "row": "row",
          |    "seatNumber": "123",
          |    "holdCodeType": "B"
          |  }
          |]
        """.stripMargin

      val before = Instant.now()
      val after = before.minus(1, ChronoUnit.HOURS)
      val eventIds = Seq(3, 4, 5)

      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(req => {
          req.uri.toString.contains("inventory") &&
          req.uri.paramsSeq.contains("after" -> after.toString) &&
          req.uri.paramsSeq.contains("before" -> before.toString) &&
          eventIds.forall(id => req.uri.paramsSeq.contains("eventId" -> id.toString))
        })
        .thenRespondWithCode(200, responseBody)

      val service = new TdcReplicatedProxyService(conf, backend)

      service
        .getInventory(eventIds, after, before)
        .map(result => {
          assert(result.isSuccess)
          assert(result.body.isRight)
          assert(Json.toJson(result.body.right.get) == Json.parse(responseBody))
        })
    }
  }

  "TdcReplicatedProxyService#listBuyerTypes" should {
    "handle success" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString().contains(s"/buyerTypes"))
        .thenRespond("""
            |[
            | {
            |   "id":3161,
            |   "active":true,
            |   "displayOrder":100,
            |   "publicDescription":"Adult",
            |   "shortPublicDescription":"ADL",
            |   "code":"ADULT",
            |   "editable":true,
            |   "description":"Adult",
            |   "isInPriceStructure": true
            | },
            | {
            |   "id":3201,
            |   "active":true,
            |   "displayOrder":100,
            |   "publicDescription":"BundleBuyer",
            |   "code":"BUNDLE",
            |   "editable":false,
            |   "description":"BundleBuyer",
            |   "isInPriceStructure": true
            | }
            |]""".stripMargin)

      val service = new TdcReplicatedProxyService(conf, backend)

      service
        .listBuyerTypes(Seq(1, 2, 3))
        .map(res => {
          assert(res.code === 200)
          assert(res.body.isRight)

          val body = res.body.right.get

          assert(body.length == 2)
        })
    }
  }
}
