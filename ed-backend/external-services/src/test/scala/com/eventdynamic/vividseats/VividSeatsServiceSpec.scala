package com.eventdynamic.vividseats

import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.softwaremill.sttp.testing.SttpBackendStub
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{AsyncWordSpec, BeforeAndAfterEach, Matchers}

class VividSeatsServiceSpec
    extends AsyncWordSpec
    with BeforeAndAfterEach
    with ScalaFutures
    with Matchers {

  private val apiToken = "testApiToken"
  private val baseUrl = "testBaseUrl"
  private val vividSeatsConfig = VividSeatsConfig(baseUrl, apiToken)

  "VividSeatsService#getListings" should {
    "return 200 and an empty listings array if no results founds" in {
      val response =
        s"""
           |{
           |  "success": true,
           |  "message": null,
           |  "listings": [],
           |  "attribute": null
           |}
         """.stripMargin

      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains("/listings/v2/get"))
        .thenRespond(200, response)

      val vividSeatsService = new VividSeatsService(vividSeatsConfig, backend)

      vividSeatsService.getListings.map(res => {
        assert(res.code === 200)
        assert(res.body.isRight)
      })
    }

    "return 200 and listings in an array" in {
      val response =
        s"""
           |{
           |  "success": true,
           |  "message": null,
           |  "listings": [
           |    {
           |      "id": 000000,
           |      "productionId": 000000,
           |      "quantity": 2,
           |      "section": "509",
           |      "row": "4",
           |      "seatFrom": "15",
           |      "seatThru": "16",
           |      "notes": "",
           |      "price": 33.00,
           |      "ticketId": "111111",
           |      "electronic": true,
           |      "electronicTransfer": false,
           |      "inHandDate": "2018-11-26T00:00:00",
           |      "listDate": "2018-11-12T13:24:31",
           |      "splitType": "NEVERLEAVEONE",
           |      "spec": false,
           |      "instantDownload": true,
           |      "passThrough": "",
           |      "stockType": "ELECTRONIC",
           |      "attributes": [],
           |      "tickets": [],
           |      "internalNotes": "",
           |      "eventName": "St. Louis Cardinals at New York Mets (Mets Gnome Giveaway)",
           |      "venue": "Citi Field",
           |      "city": "New York",
           |      "state": "NY",
           |      "eventDate": "2019-06-15T19:10:00",
           |      "cost": 0,
           |      "faceValue": 0,
           |      "hasFiles": false,
           |      "hasBarcodes": false,
           |      "lastUpdate": "2019-05-07T12:46:03",
           |      "electronicRaw": true,
           |      "electronicTransferRaw": false
           |    },
           |    {
           |      "id": 000000,
           |      "productionId": 000000,
           |      "quantity": 2,
           |      "section": "509",
           |      "row": "4",
           |      "seatFrom": "15",
           |      "seatThru": "16",
           |      "notes": "",
           |      "price": 9.00,
           |      "ticketId": "111111",
           |      "electronic": true,
           |      "electronicTransfer": false,
           |      "inHandDate": "2018-11-26T00:00:00",
           |      "listDate": "2018-11-12T13:24:31",
           |      "splitType": "NEVERLEAVEONE",
           |      "spec": false,
           |      "instantDownload": true,
           |      "passThrough": "",
           |      "stockType": "ELECTRONIC",
           |      "attributes": [],
           |      "tickets": [],
           |      "internalNotes": "",
           |      "eventName": "Washington Nationals at New York Mets",
           |      "venue": "Citi Field",
           |      "city": "New York",
           |      "state": "NY",
           |      "eventDate": "2019-05-22T19:10:00",
           |      "cost": 0,
           |      "faceValue": 0,
           |      "hasFiles": false,
           |      "hasBarcodes": false,
           |      "lastUpdate": "2019-05-07T10:06:43",
           |      "electronicRaw": true,
           |      "electronicTransferRaw": false
           |    }
           |  ],
           |  "attribute": null
           |}
         """.stripMargin

      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.toString.contains("/listings/v2/get"))
        .thenRespond(200, response)

      val vividSeatsService = new VividSeatsService(vividSeatsConfig, backend)

      vividSeatsService.getListings.map(res => {
        assert(res.code === 200)
        assert(res.body.isRight)
      })
    }
  }
}
