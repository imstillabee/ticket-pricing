# Feature Poller Service

The _Feature Poller Service_ will periodically fetch information that could influence pricing an event.

## Quick Start

### Setup
1. Set the environment variables (below) or override defaults.

### Running

1. Ensure you at least have a client with a `pricing` `JobConfig` entry and `Events`.
1. Ensure you have the following trigger types in the DB:
   1. `Time`
1. Run the script using sbt: `sbt featurePollerService/run`.
1. Hit the `POST /time-decay/start` endpoint to **start** the time-decay poller
1. Check the `ScheduledJobs`, `ScheduledJobTriggers` for new / updated records.
1. Hit the `POST /time-decay/stop` endpoint to **stop** the time-decay poller

## Configuration

### Environment Variables

| Name                    | Description                                                          |
| :---------------------- | :------------------------------------------------------------------- |
| ED_TIME_DECAY_FREQUENCY | Time Decay Poller Duration eg 15m or 1h                              |
| ED_NFL_STATS_FREQUENCY  | NFL Stats Poller Duration eg 15m or 1h                               | 
| ED_AUTO_POLL            | Boolean flag to toggle automatic polling once the server has started |

## Endpoints

| URL                      | Description                                                |
| ------------------------ | ---------------------------------------------------------- |
| `GET /healthy`           | Checks the health of the service                           |
| `POST /time-decay/start` | Starts the time decay poller with the configured frequency |
| `POST /time-decay/stop`  | Stops the time decay poller                                |
| `POST /time-decay/tick`  | Runs a single tick/period of the time decay poller         |
| `POST /nfl-stats/start`  | Starts the NFL stats poller with the configured frequency  |
| `POST /nfl-stats/stop`   | Stops the NFL stats poller                                 |
| `POST /nfl-stats/tick`   | Runs a single tick/period of the NFL stats poller          |

## Service Dependencies

## Troubleshooting

## Roadmap

In the future, this will have the ability to poll for `Weather`, and `Stats`.
