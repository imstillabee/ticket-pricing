package com.eventdynamic.feature.poller

import akka.actor.{ActorRef, ActorSystem}
import akka.event.Logging
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.util.Timeout
import com.eventdynamic.feature.poller.pollers.ServicePoller.{PollTick, StartPoller, StopPoller}
import com.eventdynamic.feature.poller.pollers.{ServicePoller, ServicePollerConfig}

import scala.concurrent.duration._

trait FeaturePollerRoutes {
  implicit def system: ActorSystem

  lazy val log = Logging(system, classOf[FeaturePollerRoutes])

  def timeDecayPoller: ActorRef
  def nflStatsPoller: ActorRef

  // Required by the `ask` (?) method below
  implicit lazy val timeout: Timeout = Timeout(10.seconds)

  protected lazy val systemRoutes: Route = path("health") {
    get {
      log.info("Healthy!")
      complete(StatusCodes.OK, "Healthy!")
    }
  }

  protected lazy val timeDecayRoutes: Route = pathPrefix("time-decay") {
    path("start") {
      post {
        val timeDecayFrequency =
          system.settings.config.getDuration("featurePollerService.timeDecayFrequency")
        val timeDecayPollerConfig = ServicePollerConfig(timeDecayFrequency)
        timeDecayPoller ? StartPoller(timeDecayPollerConfig)
        complete(
          StatusCodes.OK,
          s"Time decay poller started with configured frequency ${timeDecayFrequency.toMinutes} minute(s)"
        )
      }
    } ~
      path("stop") {
        post {
          timeDecayPoller ? StopPoller
          complete(StatusCodes.OK, "Time decay poller stopped!")
        }
      } ~
      path("tick") {
        post {
          timeDecayPoller ? PollTick
          complete(StatusCodes.OK, "Simulating a tick for time decay")
        }
      }
  }

  protected lazy val nflStatsRoutes: Route = pathPrefix("nfl-stats") {
    path("start") {
      post {
        val nflStatsFrequency =
          system.settings.config.getDuration("featurePollerService.nflStatsFrequency")
        val nflStatsPollerConfig = ServicePollerConfig(nflStatsFrequency)
        nflStatsPoller ? ServicePoller.StartPoller(nflStatsPollerConfig)
        complete(
          StatusCodes.OK,
          s"NFL stats poller started with configured frequency ${nflStatsFrequency.toMinutes} minute(s)"
        )
      }
    } ~
      path("stop") {
        post {
          nflStatsPoller ? StopPoller
          complete(StatusCodes.OK, "NFL stats poller stopped!")
        }
      } ~
      path("tick") {
        post {
          nflStatsPoller ? PollTick
          complete(StatusCodes.OK, "Simulating a tick for NFL stats")
        }
      }
  }

  lazy val featurePollerRoutes: Route = systemRoutes ~ timeDecayRoutes ~ nflStatsRoutes
}
