package com.eventdynamic.feature.poller

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.eventdynamic.db.EDContext
import com.eventdynamic.feature.poller.pollers.ServicePoller.StartPoller
import com.eventdynamic.feature.poller.pollers.{ServicePoller, ServicePollerConfig}
import com.eventdynamic.feature.poller.services.{NFLStatsService, ServiceHolder, TimeDecayService}
import com.eventdynamic.mysportsfeed.MySportsFeedService
import org.slf4j.LoggerFactory

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object FeaturePollerService extends App with FeaturePollerRoutes {
  private val logger = LoggerFactory.getLogger(this.getClass)

  logger.debug("Initializing Feature Poller Service")

  implicit val system = ActorSystem("feature-poller-service")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  implicit val edContext: EDContext = new EDContext
  edContext.registerShutdownHook()

  logger.debug("Loading Configuration")
  val conf = system.settings.config

  // Pull configs
  val timeDecayFrequency = conf.getDuration("featurePollerService.timeDecayFrequency")
  val timeDecayPollerConfig = ServicePollerConfig(timeDecayFrequency)
  logger.info(s"Configured time decay frequency is ${timeDecayFrequency.toMinutes} minute(s)")
  val nflStatsFrequency = conf.getDuration("featurePollerService.nflStatsFrequency")
  val nflStatsPollerConfig = ServicePollerConfig(nflStatsFrequency)
  logger.info(s"Configured NFL stats frequency is ${nflStatsFrequency.toMinutes} minute(s)")

  // Initialize services
  val serviceHolder = ServiceHolder()
  val mySportsFeedService = MySportsFeedService.fromConfig()
  val timeDecayClientList = conf.getString("featurePollerService.timeDecayClientList")
  val timeDecayService = new TimeDecayService(serviceHolder, timeDecayClientList)
  val nflStatsService = new NFLStatsService(mySportsFeedService, serviceHolder)

  // Create pollers
  val timeDecayPoller =
    system.actorOf(ServicePoller.props(timeDecayService), "timeDecayPoller")

  val nflStatsPoller =
    system.actorOf(ServicePoller.props(nflStatsService), "nflStatsPoller")

  lazy val routes = featurePollerRoutes

  val interface = conf.getString("featurePollerService.interface")
  val port = conf.getInt("featurePollerService.port")
  logger.info(s"Configured interface $interface and port $port")

  val serverBindings = Http().bindAndHandle(routes, interface, port).onComplete {
    case Success(bound) =>
      logger.info(
        s"Running feature-poller-service at http://${bound.localAddress.getHostString}:${bound.localAddress.getPort}"
      )
      val autoPoll = conf.getBoolean("featurePollerService.autoPoll")
      if (autoPoll) {
        logger.info("Configured to auto poll")
        timeDecayPoller ! StartPoller(timeDecayPollerConfig)
        nflStatsPoller ! StartPoller(nflStatsPollerConfig)
      }
    case Failure(e) =>
      logger.error("Server could not start!")
      e.printStackTrace()
      system.terminate()
  }

  Await.result(system.whenTerminated, Duration.Inf)
}
