package com.eventdynamic.feature.poller.pollers

import java.time.Duration

import akka.actor.{Actor, ActorLogging, Props, Timers}
import com.eventdynamic.feature.poller.services.PollerService

case class ServicePollerConfig(frequency: Duration)

class ServicePoller(val service: PollerService) extends Actor with Timers with ActorLogging {

  import ServicePoller._

  def start(servicePollerConfig: ServicePollerConfig): Unit =
    timers.startPeriodicTimer(ServicePollerTickKey, PollTick, servicePollerConfig.frequency)

  override def receive: Receive = {
    case StartPoller(servicePollerConfig: ServicePollerConfig) =>
      start(servicePollerConfig)
      log.info("Started!")

    case PollTick =>
      log.info("Polling...")
      service.queueJobs()

    case StopPoller =>
      log.info("Stopping...")
      timers.cancel(ServicePollerTickKey)
      log.info("Stopped!")
  }
}

object ServicePoller {

  final case object PollTick

  final case class StartPoller(servicePollerConfig: ServicePollerConfig)

  final case object StopPoller

  private case object ServicePollerTickKey

  def props(service: PollerService): Props =
    Props(new ServicePoller(service))
}
