package com.eventdynamic.feature.poller.services

import java.time.temporal.ChronoUnit

import com.eventdynamic.models._
import com.eventdynamic.mysportsfeed.MySportsFeedService
import com.eventdynamic.mysportsfeed.models.nfl.{
  NFLGameLog,
  NFLGameSchedule,
  NFLGameScheduleTeam,
  NFLTeam
}
import com.eventdynamic.utils.DateHelper
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Success

class NFLStatsService(mySportsFeedService: MySportsFeedService, serviceHolder: ServiceHolder)
    extends PollerService {
  import NFLStatsService._

  /**
    * Queues jobs for event that have NFL stats changes.  Persists any new data to the NFL stats tables.
    *
    * @return new scheduled jobs added to the queue
    */
  override def queueJobs(): Future[Seq[ScheduledJob]] = {
    val now = DateHelper.now()

    val jobFuture = for {
      clients <- serviceHolder.clientService.getByPerformance(ClientPerformanceType.NFL).andThen {
        case Success(clients) => logger.info(s"Polling for ${clients.length} NFL clients")
      }
      if !clients.isEmpty
      events <- serviceHolder.eventService.getUpcomingEvents(clients.map(_.id.get), now).andThen {
        case Success(events) => logger.info(s"Polling for ${events.length} NFL events")
      }
      abbrMap <- getTeamAbbreviationMap(clients.flatMap(_.id)).andThen {
        case Success(abbrMap) => logger.info(s"Found abbreviations for ${abbrMap.size} clients")
      }
      liveDataMap <- getLiveNFLData(abbrMap).andThen {
        case Success(liveDataMap) => logger.info(s"Got live data for ${liveDataMap.size} clients")
      }
      currentDataMap <- getCurrentNFLData(events).andThen {
        case Success(currentDataMap) =>
          logger.info(s"Got current data for ${currentDataMap.size} clients")
      }
      changedEventIds <- persistStatsChanges(liveDataMap, currentDataMap, events, abbrMap)
    } yield {
      logger.info(s"List of eventIds with updated stats: $changedEventIds")
      Seq.empty[ScheduledJob]
    }

    jobFuture.recover {
      case _: NoSuchElementException =>
        logger.info("No NFL clients")
        Seq.empty[ScheduledJob]
      case e: Exception =>
        logger.error("Failed to poll NFL stats", e)
        Seq.empty[ScheduledJob]
    }
  }

  /**
    * Gets a mapping of clientId -> abbreviations.
    *
    * @param clientIds list of client ids
    * @return map of clientId -> abbreviation
    */
  private def getTeamAbbreviationMap(clientIds: Seq[Int]): Future[Map[Int, String]] =
    serviceHolder.mySportsFeedInfoService
      .getByClientIds(clientIds)
      .map(_.map(info => info.clientId -> info.abbreviation).toMap)

  /**
    * Creates a map of clientId -> NFL relevant data
    *
    * @param abbrMap map of clientId -> abbreviations
    * @return map of clientId -> live NFL data (season and events/game logs)
    */
  private def getLiveNFLData(abbrMap: Map[Int, String]): Future[Map[Int, LiveNFLData]] = {
    // Start queries to the service, run in parallel
    val abbreviations = abbrMap.values.toSeq.distinct
    val gameLogsFuture = mySportsFeedService.NFL.getGameLogs(abbreviations)
    val standingsFuture = mySportsFeedService.NFL.getStandings(abbreviations)

    for {
      allGameLogs <- gameLogsFuture
      allStandings <- standingsFuture
    } yield
      abbrMap.keys
        .flatMap(clientId => {
          // Get the info for a specific team
          val abbreviation = abbrMap(clientId)
          val games = allGameLogs.games.filter(_.schedule.homeTeam.abbreviation == abbreviation)
          val standings = allStandings.teams.find(_.team.abbreviation == abbreviation)

          // If we're missing information, don't add it to the map
          if (games.isEmpty || standings.isEmpty)
            None
          else
            Some(clientId -> LiveNFLData(standings.get, games))
        })
        .toMap
  }

  /**
    * Gets a mapping of client id -> current NFL stats (seasons and events).  If a current data doesn't exist
    * for an event or season it gets created with blank data.
    *
    * @param events events to get NFL stats for
    * @return map of eventId to CurrentNFLData (season and event data)
    */
  private def getCurrentNFLData(events: Seq[Event]): Future[Map[Int, CurrentNFLData]] = {
    // Get the season and event stats for all events (and their seasons)
    val eventStatsFuture = serviceHolder.nflEventStatService.getByEventIds(events.map(_.id.get))
    val seasonStatsFuture =
      serviceHolder.nflSeasonStatService.getBySeasonIds(events.flatMap(_.seasonId).distinct)
    val now = DateHelper.now()

    for {
      eventStats <- eventStatsFuture
      seasonStats <- seasonStatsFuture
    } yield
      events
        .map(_.clientId)
        .distinct
        .map(clientId => {
          // Get the events and seasons for the client
          val eventIds = events.filter(_.clientId == clientId).map(_.id.get)
          val seasonIds = events.filter(_.clientId == clientId).flatMap(_.seasonId).distinct

          // Get the corresponding NFL stats
          val clientEventStats = eventStats.filter(s => eventIds.contains(s.eventId))
          val clientSeasonStats = seasonStats.filter(s => seasonIds.contains(s.seasonId))

          // Get the ids of the events and seasons that don't have stats
          val newEventStatIds = eventIds.filter(e => !clientEventStats.exists(_.eventId == e))
          val newSeasonStatIds = seasonIds.filter(s => !clientSeasonStats.exists(_.seasonId == s))

          // Create new event and season stats
          val newEventStats =
            newEventStatIds.map(
              NFLEventStat(None, now, now, clientId, _, NFLEventStat.MISSING_OPPONENT, false, false)
            )
          val newSeasonStats =
            newSeasonStatIds.map(NFLSeasonStat(None, now, now, clientId, _, 0, 0, 0, 16))

          clientId -> CurrentNFLData(
            clientSeasonStats ++ newSeasonStats,
            clientEventStats ++ newEventStats
          )
        })
        .toMap
  }

  /**
    * This function performs an event and season level diffs between data in our database and live data from our APIs.
    * All events and seasons that have updates are persisted to the database here.  We return all event ids that have
    * been updated either independently or has a season that has been updated (eg. when a team wins, all stats for
    * future events have been updated).  This function also handles creating stats that haven't been saved to the
    * database yet.
    *
    * @param liveDataMap live data pulled from MySportsFeed
    * @param currentDataMap data that's currently in our database (or new stats that need to be created)
    * @param events all events being processed, used to pull event timestamps
    * @param abbrMap map of abbreviations, used to pull event timestamps
    * @return a list of event ids that have stats changes
    */
  private def persistStatsChanges(
    liveDataMap: Map[Int, LiveNFLData],
    currentDataMap: Map[Int, CurrentNFLData],
    events: Seq[Event],
    abbrMap: Map[Int, String]
  ): Future[Seq[Int]] = {
    val clientPersists = liveDataMap.keys.map(clientId => {
      // Get live and current data for this client
      val liveData = liveDataMap(clientId)
      val currentDataOpt = currentDataMap.get(clientId)
      val currentData =
        currentDataOpt.getOrElse(CurrentNFLData(Seq.empty[NFLSeasonStat], Seq.empty[NFLEventStat]))
      val abbr = abbrMap(clientId)
      val clientEvents = events.filter(_.clientId == clientId)

      // Get differences between seasonal and event data
      val seasonDiff = diffSeasonData(liveData.standings, currentData.seasons)
      val eventDiff = diffEventData(liveData.games, currentData.events, clientEvents, abbr)

      // Persist diffs to the database
      for {
        seasonUpdates <- Future
          .sequence(seasonDiff.map(serviceHolder.nflSeasonStatService.createOrUpdate))
        eventUpdates <- Future
          .sequence(eventDiff.map(serviceHolder.nflEventStatService.upsert))
      } yield {
        // Get the event ids that have been updated
        // If the season has updated, update all upcoming events
        val eventIds = eventUpdates.map(_.eventId)
        val seasonEventIds = clientEvents
          .filter(_.seasonId.isDefined)
          .filter(e => seasonUpdates.map(_.seasonId).contains(e.seasonId.get))
          .map(_.id.get)

        (seasonEventIds ++ eventIds).distinct
      }
    })

    // Wait for each client's diffs to run, flatten eventIds to one list
    Future.sequence(clientPersists).map(_.flatten.toSeq)
  }
}

object NFLStatsService {
  import com.eventdynamic.utils.DateHelper.instantOrdering

  private val logger = LoggerFactory.getLogger(this.getClass)

  // Helper case classes to help with readability
  private case class LiveNFLData(standings: NFLTeam, games: Seq[NFLGameLog])
  private case class CurrentNFLData(seasons: Seq[NFLSeasonStat], events: Seq[NFLEventStat])

  /**
    * Calculates the difference between the live seasonal data (NFLTeam/standings) and what's currently in the
    * database (NFLSeasonStats/seasons).
    *
    * NOTE: [Tracked in EVNT-968] This makes a BAD assumption that the single standings that we get from the API
    * should be applied to ALL seasons.  It's HIGHLY unlikely that we would get multiple seasons from the events
    * in the database, but it could technically happen.  We need to account for this in the future by dynamically
    * querying for stats based off of season in the MySportsFeed API.
    *
    * @param standings the live standings data
    * @param seasons the current seasons data we have in the database
    * @return copies of NFLSeasonStats with updated stats (if different from live)
    */
  private def diffSeasonData(standings: NFLTeam, seasons: Seq[NFLSeasonStat]): Seq[NFLSeasonStat] =
    seasons.flatMap(season => {
      // Calculate if there are differences or the season is new
      val stats = standings.stats.standings
      val isNew = season.id.isEmpty
      val newSeasonStat = season.copy(wins = stats.wins, losses = stats.losses, ties = stats.ties)

      // If there are, return a copy of the season
      if (isNew || season != newSeasonStat)
        Some(newSeasonStat)
      else
        None
    })

  /**
    * Calculates the difference between the live event level data (NFLGameLog/games) and the current
    * data in the database (NFLEventStat/eventStats).
    *
    * @param games live data for all games
    * @param eventStats current data for all games
    * @param events all events that we're processing, used to match timestamps together
    * @param abbr the abbreviation of the client
    * @return copies of the NFLEventStats that have different
    */
  private def diffEventData(
    games: Seq[NFLGameLog],
    eventStats: Seq[NFLEventStat],
    events: Seq[Event],
    abbr: String
  ): Seq[NFLEventStat] = {
    val homeOpenerDate =
      games.filter(_.schedule.homeTeam.abbreviation == abbr).map(_.schedule.startTime).min

    eventStats.flatMap(eventStat => {
      // Find the event by timestamp
      val eventTimestampOpt = events.find(_.id.get == eventStat.eventId).flatMap(_.timestamp)

      if (eventTimestampOpt.isEmpty) {
        // We should theoretically never hit here, but just in case
        logger.error(s"Unable to lookup event ${eventStat.eventId} for diffing event data")
        None
      } else {
        // Find the corresponding game by timestamp and home team
        // TODO: Convert to eventId matching after EVNT-968 is resolved
        val gameOpt = games.find(
          game =>
            game.schedule.startTime.truncatedTo(ChronoUnit.DAYS) == eventTimestampOpt.get.toInstant
              .truncatedTo(ChronoUnit.DAYS) && game.schedule.homeTeam.abbreviation == abbr
        )

        // Grabbing the stats, or if they don't exist put in a temp data object
        val stats =
          gameOpt
            .map(_.schedule)
            .getOrElse(
              NFLGameSchedule(
                -1,
                NFLGameScheduleTeam(abbr),
                NFLGameScheduleTeam(NFLEventStat.MISSING_OPPONENT),
                eventTimestampOpt.get.toInstant
              )
            )

        // Ignore the opponent comparison if the opponent is unknown
        val isNew = eventStat.id.isEmpty
        val newAbbr =
          if (stats.awayTeam.abbreviation == NFLEventStat.MISSING_OPPONENT) eventStat.opponent
          else stats.awayTeam.abbreviation
        val newEventStat = eventStat.copy(
          opponent = newAbbr,
          isHomeOpener = stats.startTime == homeOpenerDate,
          isPreSeason = stats.startTime.getEpochSecond < homeOpenerDate.getEpochSecond
        )

        // If there are difference, return the different object
        if (isNew || eventStat != newEventStat)
          Some(newEventStat)
        else
          None
      }
    })
  }
}
