package com.eventdynamic.feature.poller.services

import com.eventdynamic.models.ScheduledJob

import scala.concurrent.Future

/**
  * Base class for all poller services to use in the generic poller.
  */
trait PollerService {
  def queueJobs(): Future[Seq[ScheduledJob]]
}
