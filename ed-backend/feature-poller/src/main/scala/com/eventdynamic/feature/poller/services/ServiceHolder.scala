package com.eventdynamic.feature.poller.services

import com.eventdynamic.db.EDContext
import com.eventdynamic.queues.ScheduledJobQueue
import com.eventdynamic.services.mlb.MySportsFeedInfoService
import com.eventdynamic.services.{
  ClientService,
  EventService,
  JobConfigService,
  NFLEventStatService,
  NFLSeasonStatService
}

import scala.concurrent.ExecutionContext

case class ServiceHolder(
  eventService: EventService,
  scheduledJobQueue: ScheduledJobQueue,
  jobConfigService: JobConfigService,
  clientService: ClientService,
  mySportsFeedInfoService: MySportsFeedInfoService,
  nflSeasonStatService: NFLSeasonStatService,
  nflEventStatService: NFLEventStatService
)

object ServiceHolder {

  def apply()(implicit ed: EDContext, excecutionContext: ExecutionContext): ServiceHolder = {
    val eventService = new EventService(ed)
    val scheduledJobQueue = new ScheduledJobQueue(ed)
    val jobConfigService = new JobConfigService(ed)
    val clientService = new ClientService(ed)
    val mySportsFeedInfoService = new MySportsFeedInfoService(ed)
    val nflSeasonStatService = new NFLSeasonStatService(ed)
    val nflEventStatService = new NFLEventStatService(ed)

    ServiceHolder(
      eventService,
      scheduledJobQueue,
      jobConfigService,
      clientService,
      mySportsFeedInfoService,
      nflSeasonStatService,
      nflEventStatService
    )
  }
}
