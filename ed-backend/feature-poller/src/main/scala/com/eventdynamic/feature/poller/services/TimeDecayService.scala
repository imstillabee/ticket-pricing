package com.eventdynamic.feature.poller.services

import java.sql.Timestamp
import java.time.temporal.ChronoUnit

import com.eventdynamic.models.JobPriorityType.JobPriorityType
import com.eventdynamic.models.JobType
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models.{
  Event,
  JobPriorityType,
  JobTriggerType,
  ScheduledJob,
  ScheduledJobStatus
}
import com.eventdynamic.utils.{DateHelper, FutureUtil}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class TimeDecayService(serviceHolder: ServiceHolder, timeDecayClientList: String = "*")
    extends PollerService {
  private val JOB_TYPE = JobType.Pricing
  private val HOURLY_LIMIT = 1
  private val DAILY_LIMIT = 7
  private val WEEKLY_LIMIT = 28
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Checks to see if the difference from now and the successful last job
    * compared with the difference from now to the event
    * fit into **one** of the time decay re-schedule windows
    * The windows are:
    * - It's the day of the event and the last was an hour ago
    * - The event is a week or less from now and the last job was at least a day ago
    * - The event is greater than a week but less than 28 days from now and the last job was a more than a week ago
    * @param now
    * @param eventTimestamp
    * @param lastJobTimestamp
    * @return Job priority
    */
  def findJobPriority(
    now: Timestamp,
    eventTimestamp: Timestamp,
    lastJobTimestamp: Option[Timestamp]
  ): Option[JobPriorityType] = {
    val eventDiffInDays = ChronoUnit.DAYS
      .between(now.toInstant(), eventTimestamp.toInstant())

    val lastJobDiffInDays = lastJobTimestamp.map(
      timestamp => ChronoUnit.DAYS.between(timestamp.toInstant(), now.toInstant())
    )
    val lastJobDiffInHours = lastJobTimestamp.map(
      timestamp => ChronoUnit.HOURS.between(timestamp.toInstant(), now.toInstant())
    )

    logger.info(
      s"Event diff in day(s) $eventDiffInDays -> last job diff in day(s) $lastJobDiffInDays -> last job diff in hours $lastJobDiffInHours"
    )
    if ((eventDiffInDays > DAILY_LIMIT &&
        eventDiffInDays < WEEKLY_LIMIT) &&
        lastJobDiffInDays.forall(_ >= DAILY_LIMIT)) {
      logger.info("Fits in weekly time window")
      Some(JobPriorityType.TimeDecayWeekly)
    } else if ((eventDiffInDays > 0 &&
               eventDiffInDays <= DAILY_LIMIT) &&
               lastJobDiffInDays.forall(_ >= 1)) {
      logger.info("Fits in daily time window")
      Some(JobPriorityType.TimeDecayDaily)
    } else if (eventDiffInDays == 0 &&
               lastJobDiffInHours.forall(_ >= HOURLY_LIMIT)) {
      logger.info("Fits in hourly time window")
      Some(JobPriorityType.TimeDecayHourly)
    } else {
      logger.info("Fits in no time window")
      None
    }
  }

  private def addJob(
    eventId: Int,
    jobConfigId: Int,
    jobPriorityType: JobPriorityType
  ): Future[ScheduledJob] = {
    serviceHolder.scheduledJobQueue
      .add(eventId, jobConfigId, JobTriggerType.Time, jobPriorityType)
      .andThen {
        case Success(s) => logger.info(s"Successfully added job with scheduledJobId ${s.id.get}")
        case Failure(e) => logger.error(s"Error adding job $e")
      }
  }

  private def scheduleNextJob(now: Timestamp, event: Event): Future[Option[ScheduledJob]] = {
    logger.info(s"Attempting to schedule the next job for eventId ${event.id.get}")
    for {
      scheduledJobComposite <- serviceHolder.scheduledJobQueue
        .getLatestForEvent(event.id.get, JOB_TYPE, Some(ScheduledJobStatus.Success))
      nextJob <- scheduledJobComposite match {
        case Some(s) =>
          findJobPriority(now, event.timestamp.get, Some(s.scheduledJob.modifiedAt)) match {
            case Some(priority) =>
              addJob(event.id.get, s.scheduledJob.jobConfigId, priority).map(Some(_))
            case None => Future.successful(None)
          }

        case None =>
          serviceHolder.jobConfigService
            .getRawConfig(event.clientId, JOB_TYPE)
            .flatMap {
              case Some(jobConfig) =>
                findJobPriority(now, event.timestamp.get, None) match {
                  case Some(priority) =>
                    addJob(event.id.get, jobConfig.id.get, priority).map(Some(_))
                  case None => Future.successful(None)
                }
              case None =>
                logger.error(
                  s"No job config for client ${event.clientId} with event ${event.id.get}"
                )
                Future.successful(None)
            }
      }
    } yield nextJob
  }

  private def processEvents(now: Timestamp, events: Seq[Event]): Future[Seq[ScheduledJob]] = {
    logger.info(s"Processing ${events.length} events")
    FutureUtil.serializeFutures(events)(event => scheduleNextJob(now, event)).map(_.flatten)
  }

  def findEvents(now: Timestamp): Future[Seq[Event]] = {
    val before = Timestamp.from(now.toInstant().plus(WEEKLY_LIMIT, ChronoUnit.DAYS))
    timeDecayClientList match {
      case "*" =>
        serviceHolder.eventService
          .getUpcomingEventsForAllClients(now, Some(before))
      case value =>
        val clientIds = value.split(",").map(_.trim.toInt).toList
        logger.info(s"TimeDecay enabled for the following client IDs $clientIds")
        serviceHolder.eventService.getUpcomingEvents(clientIds, now, Some(before))
    }
  }

  def queueJobs(): Future[Seq[ScheduledJob]] = {
    val now = DateHelper.now()
    logger.info(s"The current timestamp is ${now.toString}")
    for {
      events <- findEvents(now)
      jobs <- processEvents(now, events)
    } yield jobs
  }
}
