package com.eventdynamic.feature.poller

import akka.actor.ActorRef
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.eventdynamic.feature.poller.pollers.ServicePoller
import com.eventdynamic.feature.poller.services.{NFLStatsService, TimeDecayService}
import org.scalatest.WordSpec
import org.specs2.mock.Mockito

class FeaturePollerRoutesSpec
    extends WordSpec
    with ScalatestRouteTest
    with FeaturePollerRoutes
    with Mockito {

  override val timeDecayPoller: ActorRef =
    system.actorOf(ServicePoller.props(mock[TimeDecayService]), "timeDecayPoller")
  override val nflStatsPoller: ActorRef =
    system.actorOf(ServicePoller.props(mock[NFLStatsService]), "nflStatsPoller")

  lazy val routes = featurePollerRoutes

  "FeaturePollerService Basic Routes" should {
    "return a healthy response for (GET /health)" in {
      Get("/health") ~> routes ~> check {
        assert(responseAs[String].equals("Healthy!"))
      }
    }

  }

  "FeaturePollerService Time Decay Routes" should {
    "be able to start time decay poller (POST /time-decay/start)" in {
      val request = Post("/time-decay/start")

      request ~> routes ~> check {
        assert(responseAs[String].contains("Time decay poller started with configured frequency"))
      }
    }

    "be able to stop time decay poller (POST /time-decay/stop)" in {
      val request = Post("/time-decay/stop")

      request ~> routes ~> check {
        assert(responseAs[String].contains("Time decay poller stopped!"))
      }
    }

    "be able to simulate a tick on time decay poller (POST /time-decay/tick)" in {
      val request = Post("/time-decay/tick")

      request ~> routes ~> check {
        assert(responseAs[String].contains("Simulating a tick for time decay"))
      }
    }
  }

  "FeaturePollerService NFL Stats Routes" should {
    "be able to start time decay poller (POST /nfl-stats/start)" in {
      val request = Post("/nfl-stats/start")

      request ~> routes ~> check {
        assert(responseAs[String].contains("NFL stats poller started with configured frequency"))
      }
    }

    "be able to stop time decay poller (POST /nfl-stats/stop)" in {
      val request = Post("/nfl-stats/stop")

      request ~> routes ~> check {
        assert(responseAs[String].contains("NFL stats poller stopped!"))
      }
    }

    "be able to simulate a tick on time decay poller (POST /nfl-stats/tick)" in {
      val request = Post("/nfl-stats/tick")

      request ~> routes ~> check {
        assert(responseAs[String].contains("Simulating a tick for NFL stats"))
      }
    }
  }
}
