package com.eventdynamic.feature.poller

import com.eventdynamic.feature.poller.services.ServiceHolder
import com.eventdynamic.queues.ScheduledJobQueue
import com.eventdynamic.services.mlb.MySportsFeedInfoService
import com.eventdynamic.services.{
  ClientService,
  EventService,
  JobConfigService,
  NFLEventStatService,
  NFLSeasonStatService
}
import org.specs2.mock.Mockito

class TestHelper extends Mockito {

  /**
    * Gets a clean set of mocked services
    */
  def mockedServices: ServiceHolder =
    ServiceHolder(
      mock[EventService],
      mock[ScheduledJobQueue],
      mock[JobConfigService],
      mock[ClientService],
      mock[MySportsFeedInfoService],
      mock[NFLSeasonStatService],
      mock[NFLEventStatService]
    )
}
