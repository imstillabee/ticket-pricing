package com.eventdynamic.feature.poller.pollers

import java.time.Duration

import akka.actor
import akka.actor.Props
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit, TestProbe}
import com.eventdynamic.feature.poller.pollers.ServicePoller.{PollTick, StopPoller}
import com.eventdynamic.feature.poller.services.PollerService
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import org.specs2.mock.Mockito

import scala.concurrent.duration._
import scala.language.postfixOps

class ServicePollerSpec
    extends TestKit(actor.ActorSystem("ServicePollerSpec", ConfigFactory.empty()))
    with DefaultTimeout
    with ImplicitSender
    with WordSpecLike
    with Mockito
    with BeforeAndAfterAll {

  override def afterAll(): Unit = {
    super.afterAll()
    TestKit.shutdownActorSystem(system)
  }

  val duration = 500
  val timeDecayFrequency = Duration.ofMillis(5)
  val timeDecayPollerConfig = ServicePollerConfig(timeDecayFrequency)
  val probe = TestProbe()

  val actor = system.actorOf(Props(new ServicePoller(mock[PollerService]) {
    override def start(servicePollerConfig: ServicePollerConfig): Unit = {
      probe.ref ! servicePollerConfig
    }
  }))

  "ServicePoller Actor" should {
    //    "receive a PollStart msg" in {
    //      actor ! StartPoller(timeDecayPollerConfig)
    //      probe.expectMsg(timeDecayPollerConfig)
    //    }

    "receive a Poll msg" in {
      within(duration millis) {
        actor ! PollTick
        expectNoMessage()
      }
    }

    "receive a PollStop msg" in {
      within(duration millis) {
        actor ! StopPoller
        expectNoMessage()
      }
    }
  }
}
