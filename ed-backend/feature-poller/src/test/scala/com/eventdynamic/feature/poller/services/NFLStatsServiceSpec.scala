package com.eventdynamic.feature.poller.services

import java.sql.Timestamp
import java.time.Instant

import com.eventdynamic.models.mlb.MySportsFeedInfo
import com.eventdynamic.models.{Client, ClientPerformanceType, Event, NFLEventStat, NFLSeasonStat}
import com.eventdynamic.mysportsfeed.MySportsFeedService
import com.eventdynamic.mysportsfeed.models.nfl.{
  NFLGameLog,
  NFLGameLogs,
  NFLGameSchedule,
  NFLGameScheduleTeam,
  NFLStandings,
  NFLTeam,
  NFLTeamInfo,
  NFLTeamStats,
  NFLTeamStatsStandings
}
import com.eventdynamic.queues.ScheduledJobQueue
import com.eventdynamic.services.mlb.MySportsFeedInfoService
import com.eventdynamic.services._
import com.eventdynamic.utils.DateHelper
import org.mockito.Mockito._
import org.scalatest.AsyncWordSpec
import org.specs2.mock.Mockito

import scala.concurrent.Future

class NFLStatsServiceSpec extends AsyncWordSpec with Mockito {

  import NFLStatsServiceSpec._

  val serviceHolder =
    ServiceHolder(
      mock[EventService],
      mock[ScheduledJobQueue],
      mock[JobConfigService],
      mock[ClientService],
      mock[MySportsFeedInfoService],
      mock[NFLSeasonStatService],
      mock[NFLEventStatService]
    )

  val mySportsFeedService = mock[MySportsFeedService]
  val mySportsFeedNFLService = mock[mySportsFeedService.NFL.type]
  when(mySportsFeedService.NFL).thenReturn(mySportsFeedNFLService)

  when(serviceHolder.clientService.getByPerformance(ClientPerformanceType.NFL))
    .thenReturn(Future.successful(clients))

  when(
    serviceHolder.eventService
      .getUpcomingEvents(any[Seq[Int]], any[Timestamp], any[Option[Timestamp]])
  ).thenReturn(Future.successful(events))

  when(serviceHolder.mySportsFeedInfoService.getByClientIds(any[Seq[Int]]))
    .thenReturn(Future.successful(mySportsFeedInfos))

  when(mySportsFeedNFLService.getStandings(any[Seq[String]]))
    .thenReturn(Future.successful(nflStandings))

  when(mySportsFeedNFLService.getGameLogs(any[Seq[String]]))
    .thenReturn(Future.successful(nflGameLogs))

  when(serviceHolder.nflEventStatService.getByEventIds(any[Seq[Int]]))
    .thenReturn(Future.successful(nflEventStats))

  when(serviceHolder.nflSeasonStatService.getBySeasonIds(any[Seq[Int]]))
    .thenReturn(Future.successful(nflSeasonStats))

  val seasonUpdateMock = when(serviceHolder.nflEventStatService.upsert(any[NFLEventStat]))
    .then(a => Future.successful(a.getArgument(0).asInstanceOf[NFLEventStat]))

  when(serviceHolder.nflSeasonStatService.createOrUpdate(any[NFLSeasonStat]))
    .then(a => Future.successful(a.getArgument(0).asInstanceOf[NFLSeasonStat]))

  val nflStatsService = new NFLStatsService(mySportsFeedService, serviceHolder)

  "NFLStatsService" should {
    "process events" in {
      for {
        result <- nflStatsService.queueJobs()
      } yield {
        verify(serviceHolder.nflSeasonStatService, times(1)).createOrUpdate(any[NFLSeasonStat])
        verify(serviceHolder.nflEventStatService, times(2)).upsert(any[NFLEventStat])

        // We are not yet adding to the queue
        assert(result.isEmpty)
      }
    }
  }
}

object NFLStatsServiceSpec {

  /**
    * DATA DESCRIPTION
    *
    * 2 Clients - Falcons and Cowboys
    * 3 Events
    *   - 2 for Falcons
    *   - 1 for Cowboys
    *   - Same season per client
    *   - Cowboys and Falcons have event on same day/time
    *   - Falcons have a "preseason" game
    * 2 Abbreviations - ATL and DAL
    * 3 Live Game Data
    *   - Missing info for the preseason event
    *   - Extra event that's not recorded in our events
    * 2 Live Standings Data
    *   - Both teams starting with clean record (0-0-0)
    * 2 Current Event Data
    *   - Both for ATL
    *   - Preseason (event 1) should be updated
    *   - Event 2 should match
    *   - Event 3 for DAL should be created
    * 1 Current Season Data
    *   - For ATL
    *   - Should not need to be updated
    *   - DAL season data should be created
    */

  val client1 = Client(
    id = Some(1),
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.now(),
    pricingInterval = 0,
    name = "Falcons",
    logoUrl = None,
    eventScoreFloor = 0,
    eventScoreCeiling = None,
    springFloor = 0,
    springCeiling = None,
    performanceType = ClientPerformanceType.NFL
  )

  val client2 = Client(
    id = Some(1),
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.now(),
    pricingInterval = 0,
    name = "Cowboys",
    logoUrl = None,
    eventScoreFloor = 0,
    eventScoreCeiling = None,
    springFloor = 0,
    springCeiling = None,
    performanceType = ClientPerformanceType.NFL
  )

  val clients = Seq[Client](client1, client2)

  val event1 = Event(
    id = Some(1),
    primaryEventId = Some(1),
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.now(),
    name = "Event 1",
    timestamp = Some(Timestamp.from(Instant.parse("2019-09-07T00:20:00.000Z"))),
    clientId = client1.id.get,
    venueId = 1,
    eventCategoryId = 1,
    seasonId = Some(1),
    isBroadcast = true,
    percentPriceModifier = 0,
    eventScore = None,
    eventScoreModifier = 0,
    spring = None,
    springModifier = 0
  )

  val event2 = Event(
    id = Some(2),
    primaryEventId = Some(1),
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.now(),
    name = "Event 1",
    timestamp = Some(Timestamp.from(Instant.parse("2019-09-08T00:20:00.000Z"))),
    clientId = client1.id.get,
    venueId = 1,
    eventCategoryId = 1,
    seasonId = Some(1),
    isBroadcast = true,
    percentPriceModifier = 0,
    eventScore = None,
    eventScoreModifier = 0,
    spring = None,
    springModifier = 0
  )

  val event3 = Event(
    id = Some(3),
    primaryEventId = Some(1),
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.now(),
    name = "Event 1",
    timestamp = Some(Timestamp.from(Instant.parse("2019-09-08T00:20:00.000Z"))),
    clientId = client1.id.get,
    venueId = 1,
    eventCategoryId = 1,
    seasonId = Some(2),
    isBroadcast = true,
    percentPriceModifier = 0,
    eventScore = None,
    eventScoreModifier = 0,
    spring = None,
    springModifier = 0
  )

  val events = Seq[Event](event1, event2, event3)

  val info1 = MySportsFeedInfo(
    id = Some(1),
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.now(),
    clientId = 1,
    abbreviation = "ATL"
  )

  val info2 = MySportsFeedInfo(
    id = Some(2),
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.now(),
    clientId = 2,
    abbreviation = "DAL"
  )

  val mySportsFeedInfos = Seq(info1, info2)

  val nflGameLogForEvent2 = NFLGameLog(
    NFLGameSchedule(
      1,
      NFLGameScheduleTeam("ATL"),
      NFLGameScheduleTeam("TEAM"),
      Instant.parse("2019-09-08T00:20:00.000Z")
    )
  )

  val nflGameLogForEvent3 = NFLGameLog(
    NFLGameSchedule(
      1,
      NFLGameScheduleTeam("DAL"),
      NFLGameScheduleTeam("TEAM"),
      Instant.parse("2019-09-08T00:20:00.000Z")
    )
  )

  val nflGameLogForExtraEvent = NFLGameLog(
    NFLGameSchedule(
      2,
      NFLGameScheduleTeam("DAL"),
      NFLGameScheduleTeam("TEAM"),
      Instant.parse("2019-09-09T00:20:00.000Z")
    )
  )

  val nflGameLogs = NFLGameLogs(
    Seq(nflGameLogForEvent2, nflGameLogForEvent3, nflGameLogForExtraEvent)
  )

  val nflTeam1 =
    NFLTeam(NFLTeamInfo("ATL"), NFLTeamStats(NFLTeamStatsStandings(wins = 0, losses = 0, ties = 0)))

  val nflTeam2 =
    NFLTeam(NFLTeamInfo("DAL"), NFLTeamStats(NFLTeamStatsStandings(wins = 0, losses = 0, ties = 0)))

  val nflStandings = NFLStandings(Seq(nflTeam1, nflTeam2))

  val nflEventStatForEvent1 =
    NFLEventStat(
      id = Some(1),
      createdAt = DateHelper.now(),
      modifiedAt = DateHelper.now(),
      clientId = client1.id.get,
      eventId = event1.id.get,
      opponent = "TEAM",
      isHomeOpener = false,
      isPreSeason = false
    )

  val nflEventStatForEvent2 =
    NFLEventStat(
      id = Some(2),
      createdAt = DateHelper.now(),
      modifiedAt = DateHelper.now(),
      clientId = client1.id.get,
      eventId = event2.id.get,
      opponent = "TEAM",
      isHomeOpener = true,
      isPreSeason = false
    )

  val nflEventStats = Seq(nflEventStatForEvent1, nflEventStatForEvent2)

  val nflSeasonStatForSeason1 =
    NFLSeasonStat(
      id = Some(1),
      createdAt = DateHelper.now(),
      modifiedAt = DateHelper.now(),
      clientId = client1.id.get,
      seasonId = 1,
      wins = 0,
      losses = 0,
      ties = 0,
      gamesTotal = 16
    )

  val nflSeasonStats = Seq(nflSeasonStatForSeason1)
}
