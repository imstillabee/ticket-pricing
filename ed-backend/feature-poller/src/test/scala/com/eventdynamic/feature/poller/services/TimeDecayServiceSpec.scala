package com.eventdynamic.feature.poller.services

import java.sql.Timestamp
import java.time.temporal.ChronoUnit

import com.eventdynamic.feature.poller.TestHelper
import com.eventdynamic.models.JobPriorityType.JobPriorityType
import com.eventdynamic.models.JobTriggerType.JobTriggerType
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models.ScheduledJobStatus.ScheduledJobStatus
import com.eventdynamic.models.{JobTriggerType, ScheduledJobComposite, _}
import com.eventdynamic.utils.DateHelper
import org.mockito.Mockito._
import org.scalatest.AsyncWordSpec
import org.specs2.mock.Mockito

import scala.concurrent.Future

class TimeDecayServiceSpec extends AsyncWordSpec with Mockito {
  val t = new TestHelper
  val clientId = 1
  val eventId = 1
  val jobConfigId = 1
  val scheduledJobId = 1
  val jobId = 2

  val events = Seq[Event](
    Event(
      id = Some(eventId),
      primaryEventId = Some(1),
      createdAt = DateHelper.now(),
      modifiedAt = DateHelper.now(),
      name = "Event 1",
      timestamp = Some(DateHelper.later(60)),
      clientId = clientId,
      venueId = 1,
      eventCategoryId = 1,
      seasonId = None,
      isBroadcast = true,
      percentPriceModifier = 0,
      eventScore = None,
      eventScoreModifier = 0,
      spring = None,
      springModifier = 0
    )
  )

  val jobConfig = JobConfig(
    id = Some(jobConfigId),
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.now(),
    clientId = clientId,
    jobId = jobId,
    json = ""
  )

  val jobPriority = JobPriority(
    id = Some(1),
    name = JobPriorityType.ManualPricingModifierChange,
    priority = 100,
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.now()
  )

  val scheduledJob = ScheduledJob(
    id = Some(scheduledJobId),
    eventId = eventId,
    jobConfigId = jobConfigId,
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.earlier(80),
    status = ScheduledJobStatus.Success,
    jobPriorityId = jobPriority.id.get,
    startTime = None,
    endTime = None,
    error = None
  )

  val jobTriggerComposite = JobTriggerComposite(
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.now(),
    triggerType = JobTriggerType.Time
  )

  val scheduledJobTriggerComposite =
    ScheduledJobComposite(scheduledJob, Seq(jobTriggerComposite), jobPriority)

  private val serviceHolder = t.mockedServices

  when(
    serviceHolder.eventService
      .getUpcomingEventsForAllClients(any[Timestamp], any[Option[Timestamp]])
  ).thenReturn(Future.successful(events))
  when(
    serviceHolder.scheduledJobQueue
      .getLatestForEvent(any[Int], any[JobType], any[Option[ScheduledJobStatus]])
  ).thenReturn(Future.successful(Some(scheduledJobTriggerComposite)))
  when(serviceHolder.jobConfigService.getRawConfig(clientId, JobType.Pricing))
    .thenReturn(Future.successful(Some(jobConfig)))
  when(
    serviceHolder.scheduledJobQueue
      .add(any[Int], any[Int], any[JobTriggerType], any[JobPriorityType])
  ).thenReturn(Future.successful(scheduledJob))

  "TimeDecayService#whiteList" should {
    "find events for clients in whitelist" in {
      val whiteListClientId1 = 3
      val whiteListClientId2 = 4
      val timeDecayClientList = s"$whiteListClientId1,$whiteListClientId2"
      val timeDecayServiceWithWhiteList = new TimeDecayService(serviceHolder, timeDecayClientList)
      val whiteListEvents = Seq[Event](
        Event(
          id = Some(3),
          primaryEventId = Some(1),
          createdAt = DateHelper.now(),
          modifiedAt = DateHelper.now(),
          name = "Event 1",
          timestamp = Some(DateHelper.later(60)),
          clientId = whiteListClientId1,
          venueId = 1,
          eventCategoryId = 1,
          seasonId = None,
          isBroadcast = true,
          percentPriceModifier = 0,
          eventScore = None,
          eventScoreModifier = 0,
          spring = None,
          springModifier = 0
        ),
        Event(
          id = Some(4),
          primaryEventId = Some(1),
          createdAt = DateHelper.now(),
          modifiedAt = DateHelper.now(),
          name = "Event 1",
          timestamp = Some(DateHelper.later(60)),
          clientId = whiteListClientId2,
          venueId = 1,
          eventCategoryId = 1,
          seasonId = None,
          isBroadcast = true,
          percentPriceModifier = 0,
          eventScore = None,
          eventScoreModifier = 0,
          spring = None,
          springModifier = 0
        )
      )
      when(
        serviceHolder.eventService
          .getUpcomingEvents(any[Seq[Int]], any[Timestamp], any[Option[Timestamp]])
      ).thenReturn(Future.successful(whiteListEvents))
      val now = DateHelper.now()
      val result = timeDecayServiceWithWhiteList.findEvents(now)
      result.map { event =>
        assert(event == whiteListEvents)
      }
    }
  }

  "TimeDecayService" should {
    val timeDecayService = new TimeDecayService(serviceHolder)

    "process events" in {
      for {
        result <- timeDecayService.queueJobs()
      } yield {
        assert(result.head == scheduledJob)
      }
    }

    "fit if it's the day of the event and the last job was an hour ago" in {
      val now = DateHelper.now()
      val event = DateHelper.now()
      val lastJob = Some(DateHelper.earlier(60, now))
      val priority = timeDecayService.findJobPriority(now, event, lastJob)
      assert(priority.get == JobPriorityType.TimeDecayHourly)
    }

    "fit if the event is a week or less from now and the last job was at least a day ago" in {
      val now = DateHelper.now()
      val event = Timestamp.from(now.toInstant().plus(2, ChronoUnit.DAYS))
      val lastJob = Some(Timestamp.from(now.toInstant().minus(5, ChronoUnit.DAYS)))
      val priority = timeDecayService.findJobPriority(now, event, lastJob)
      assert(priority.get == JobPriorityType.TimeDecayDaily)
    }

    "fit if the event is greater than a week but less than 28 days from now and the last job was a week ago" in {
      val now = DateHelper.now()
      val event = Timestamp.from(now.toInstant().plus(10, ChronoUnit.DAYS))
      val lastJob = Some(Timestamp.from(now.toInstant().minus(8, ChronoUnit.DAYS)))
      val priority = timeDecayService.findJobPriority(now, event, lastJob)
      assert(priority.get == JobPriorityType.TimeDecayWeekly)
    }

    "not fit in any time window" in {
      val now = DateHelper.now()
      val event = Timestamp.from(now.toInstant().plus(10, ChronoUnit.DAYS))
      val lastJob = Some(Timestamp.from(now.toInstant().minus(2, ChronoUnit.DAYS)))
      val priority = timeDecayService.findJobPriority(now, event, lastJob)
      assert(priority.isEmpty)
    }

    "fit if it's the day of the event (no last job)" in {
      val now = DateHelper.now()
      val event = DateHelper.now()
      val priority = timeDecayService.findJobPriority(now, event, None)
      assert(priority.get == JobPriorityType.TimeDecayHourly)
    }

    "fit if the event is a week or less from now (no last job)" in {
      val now = DateHelper.now()
      val event = Timestamp.from(now.toInstant().plus(2, ChronoUnit.DAYS))
      val priority = timeDecayService.findJobPriority(now, event, None)
      assert(priority.get == JobPriorityType.TimeDecayDaily)
    }

    "fit if the event is greater than a week but less than 28 days from now (no last job)" in {
      val now = DateHelper.now()
      val event = Timestamp.from(now.toInstant().plus(10, ChronoUnit.DAYS))
      val priority = timeDecayService.findJobPriority(now, event, None)
      assert(priority.get == JobPriorityType.TimeDecayWeekly)
    }

    "not fit in any time window (no last job)" in {
      val now = DateHelper.now()
      val event = Timestamp.from(now.toInstant().plus(30, ChronoUnit.DAYS))
      val priority = timeDecayService.findJobPriority(now, event, None)
      assert(priority.isEmpty)
    }
  }
}
