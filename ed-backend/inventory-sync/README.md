# Inventory Sync (aka Secondary Sync)

> Inventory Sync (aka secondary sync) is a job that syncs inventory in Event Dynamic with secondary integrations.

## Running
To run the sync script locally use the following command in sbt:

`inventorySync/run --clientId 1`

* `clientId`: Client Id to sync inventory for
* `integration`: Integration name to sync tickets to [_Optional_]
* `seasonID`: Season Id used to sync or create inventory for a given season [_Optional_]
* `eventId`: The event Id you want to sync for [_Optional_]
* `section`: The section you want to sync for [_Optional_]