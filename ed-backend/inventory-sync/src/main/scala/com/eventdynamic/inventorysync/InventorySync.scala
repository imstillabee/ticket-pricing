package com.eventdynamic.inventorysync

import com.eventdynamic.IntegrationServiceBuilder
import com.eventdynamic.db.EDContext
import com.eventdynamic.inventorysync.integrations.{Lister, ListerFactory}
import com.eventdynamic.inventorysync.models.Params
import com.eventdynamic.inventorysync.services.ServiceHolder
import com.eventdynamic.models.{ClientIntegrationComposite, Event}
import org.slf4j.LoggerFactory
import resource._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object InventorySync {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Main function, parses args and initializes InventorySync class
    *
    * @param args command line args
    */
  def main(args: Array[String]): Unit = {
    val params = Params.fromArgs(args)

    logger.info(s"Client: ${params.clientId}")
    logger.info(s"Integration: ${params.eventId.getOrElse("--")}")
    logger.info(s"Season: ${params.seasonId.getOrElse("--")}")
    logger.info(s"Event: ${params.eventId.getOrElse("--")}")
    logger.info(s"Section: ${params.section.getOrElse("--")}")

    for {
      ed <- managed(new EDContext)
    } {
      val integrationServiceBuilder = new IntegrationServiceBuilder()
      val sync = new InventorySync(params, ServiceHolder(ed), integrationServiceBuilder)
      Await.result(sync.syncInventory(), Duration("900 seconds")) // Wait for 15 minutes
    }
  }

  class InventorySync(
    params: Params,
    serviceHolder: ServiceHolder,
    integrationServiceBuilder: IntegrationServiceBuilder
  ) {

    /**
      * Sync inventory for all events and integrations
      */
    def syncInventory(): Future[Seq[Unit]] = {
      for {
        events <- fetchEvents()
        listers <- ListerFactory.getListersForClient(
          params.clientId,
          serviceHolder,
          params,
          integrationServiceBuilder
        )
        result <- Future.sequence(events.map(e => syncEventInventory(e, listers)))
      } yield result.flatten
    }

    /**
      * Sync inventory for a single event
      *
      * @param event event to sync
      * @param listers list of client integration listers
      * @return
      */
    def syncEventInventory(
      event: Event,
      listers: Map[ClientIntegrationComposite, Lister]
    ): Future[Seq[Unit]] = {
      for {
        inventory <- serviceHolder.inventoryService.getInventory(
          params.clientId,
          event.id,
          params.section
        )
        results <- Future.sequence(
          listers
            .map(clientIntegrationLister => {
              val (clientIntegration, lister) = clientIntegrationLister
              logger.info(s"Syncing event ${event.id.get} to ${clientIntegration.name}")

              val priceModifiedInventory = inventory.map(i => {
                val modifiedPrice = serviceHolder.secondaryPricingRulesService
                  .applySecondaryPricingRule(clientIntegration, i.getPrice)
                i.copy(overridePrice = Some(modifiedPrice))
              })

              lister.sync(event.id.get, priceModifiedInventory, clientIntegration.id.get)
            })
            .toSeq
        )
      } yield results.flatten
    }

    /**
      * Fetch the event(s) matching the filter parameters passed in to the script.
      */
    private def fetchEvents(): Future[Seq[Event]] = {
      params match {
        case Params(_, _, _, Some(eventId), _) =>
          serviceHolder.eventService.getById(eventId).map(res => Seq(res.get))
        case Params(clientId, _, Some(seasonId), _, _) =>
          serviceHolder.eventService.getAllBySeason(seasonId, clientId, false)
        case Params(clientId, _, _, _, _) =>
          serviceHolder.eventService.getAll(Some(clientId), false)
      }
    }
  }
}
