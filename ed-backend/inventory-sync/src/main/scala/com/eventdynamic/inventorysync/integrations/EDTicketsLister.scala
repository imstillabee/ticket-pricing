package com.eventdynamic.inventorysync.integrations

import com.eventdynamic.edtickets.EDTicketsService
import com.eventdynamic.edtickets.models.{EDListing, EDListings, EDPrice, EDSeat}
import com.eventdynamic.inventorysync.models.Params
import com.eventdynamic.inventorysync.services.ServiceHolder
import com.eventdynamic.models.{ClientIntegrationEventSeat, Inventory}
import com.eventdynamic.utils.DateHelper
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class EDTicketsLister(
  override val serviceHolder: ServiceHolder,
  override val params: Params,
  override val clientIntegrationId: Int,
  val apiService: EDTicketsService
) extends Lister(params, serviceHolder, clientIntegrationId) {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /** Get Listings from StubHub and map listings to inventory to start sync
    *
    * @param inventory
    * @return
    */
  override protected def getListings(eventId: String, inventory: Seq[Inventory]): Seq[Inventory] = {
    val future = params.eventId match {
      case Some(eventId) => getEDTicketsListingsForEvent(eventId)
      case _             => getEDTicketsListings()
    }
    val listings = Await.result(future, Duration.Inf)
    mapListingsToInventory(listings)
  }

  private def mapListingsToInventory(listings: Seq[EDListing]): Seq[Inventory] = {
    listings.flatMap(listing => {
      listing.seats.map(seat => {
        Inventory(
          Some(seat.externalId),
          listing.externalId.toInt,
          true,
          listing.section,
          listing.row,
          true,
          seat.seat,
          listing.price,
          None,
          listing.id
        )
      })
    })
  }

  private def getEDTicketsListingsForEvent(eventId: Int): Future[Seq[EDListing]] = {
    apiService
      .fetchListings(eventId)
      .map(_.body.toOption match {
        case Some(res) => res.listings
        case _         => Seq()
      })
      .recover {
        case err =>
          logger.error(err.getMessage)
          Seq()
      }
  }

  /** Get ED Listings Recursively (Takes a while)
    *
    * Should probably convert this to be tailRecursive
    *
    * @param pageToken
    * @param listings
    * @return
    */
  private def getEDTicketsListings(
    pageToken: Option[String] = None,
    listings: Seq[EDListing] = Seq()
  ): Future[Seq[EDListing]] = {
    apiService
      .getListings()
      .flatMap(_.body.toOption match {
        case Some(EDListings(Some(pageToken), Some(l))) =>
          logger.info(s"Got ${l.length} Listings from $pageToken")
          val newListings = listings ++ l
          getEDTicketsListings(Some(pageToken), newListings)
        case Some(EDListings(_, Some(l))) =>
          logger.info(s"Got ${l.length} Listings from $pageToken")
          Future.successful(listings ++ l)
        case _ => Future.successful(Seq())
      })
  }

  /** Push Listings to EDTickets
    *
    * @param inventory
    */
  override def createListings(inventory: Seq[Inventory]): Seq[Future[Int]] = {
    Await.result(apiService.authenticate, Duration.Inf) // login to get authToken

    mapInventoryToEDListing(inventory).map(listing => {
      apiService
        .list(listing)
        .flatMap(_.body.toOption match {
          case Some(eDListing: EDListing) => saveListingsToED(eDListing)
          case _ =>
            logger.error("Error creating listing")
            Future.successful(0)
        })
    })
  }

  /** Saves Listings and EventSeats to ED
    *
    * @param listing
    * @return
    */
  def saveListingsToED(listing: EDListing): Future[Int] = {
    val clientIntegrationEventId =
      clientIntegrationEventIdMappings.getOrElse(listing.externalId.toInt, 0)

    serviceHolder.clientIntegrationListingsService
      .create(clientIntegrationEventId, listing.id.get)
      .flatMap(clientIntegrationListing => {
        logger.info(s"Saving Listing ${listing.id}")
        val cies = maplistingToCIES(listing, clientIntegrationListing.id.get)
        serviceHolder.clientIntegrationEventSeatsService.bulkInsert(cies).map(_.getOrElse(0))
      })
      .recover {
        case err =>
          logger.error(s"Unable to save listing ${listing.id} - $err")
          0
      }
  }

  /** Map EDTickets Listing to CIES
    *
    * @param listing
    * @param listingId
    * @return
    */
  private def maplistingToCIES(
    listing: EDListing,
    listingId: Int
  ): Seq[ClientIntegrationEventSeat] = {
    val now = DateHelper.now()
    listing.seats.map(seat => {
      ClientIntegrationEventSeat(None, now, now, listingId, seat.externalId)
    })
  }

  /** Push Updates to Listings on StubHub
    *
    * @param inventory
    */
  override def updateListings(inventory: Seq[Inventory]): Seq[Future[Int]] = {
    val updatePrices: Seq[EDPrice] = inventory
      .groupBy(_.listingId)
      .map(group => {
        EDPrice(
          s"projects/edtickets-f4ede/databases/(default)/documents/listings/${group._1.get}",
          group._2.head.listedPrice
        )
      })
      .toSeq

    Await.result(apiService.authenticate, Duration.Inf)

    updatePrices
      .grouped(25)
      .map(prices => {
        apiService
          .updatePrices(prices)
          .map(_.body.toOption match {
            case Some(_) =>
              logger.info(s"Updated ${prices.length} Prices on EDTickets")
              prices.length
            case None =>
              logger.error(s"Could not update prices for $prices")
              0
          })
          .recover {
            case err =>
              logger.error(err.getMessage)
              0
          }
      })
      .toSeq
  }

  /** Push Updates to Delist listings
    *
    * @param inventory
    */
  override def delistListings(inventory: Seq[Inventory]): Seq[Future[Int]] = {
    Seq(Future.successful(0))
  }

  /** Map inventory to an ED Listing
    *
    * @param inventory
    * @return
    */
  private def mapInventoryToEDListing(inventory: Seq[Inventory]): Seq[EDListing] = {
    inventory
      .groupBy(i => (i.eventId, i.section, i.row))
      .zipWithIndex
      .map {
        case (group, index) => {
          val price = group._2.maxBy(_.getPrice).getPrice
          EDListing(
            None,
            group._1._1.toString,
            eventIdMappings.getOrElse(group._1._1, "0"),
            "ACTIVE",
            group._1._2,
            group._1._3,
            price,
            group._2.map(seat => {
              EDSeat(seat.id.get, seat.seat, true)
            })
          )
        }
      }
      .toSeq
  }
}
