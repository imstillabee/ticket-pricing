package com.eventdynamic.inventorysync.integrations

import com.eventdynamic.inventorysync.models.Params
import com.eventdynamic.inventorysync.services.ServiceHolder
import com.eventdynamic.models.Inventory
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

abstract class Lister(
  val params: Params,
  val serviceHolder: ServiceHolder,
  val clientIntegrationId: Int
) {

  val eventIdMappings: Map[Int, String] = serviceHolder.integrationTransactions
    .getIntegrationEventIds(clientIntegrationId, params.seasonId)

  val clientIntegrationEventIdMappings: Map[Int, Int] = serviceHolder.integrationTransactions
    .getClientIntegrationEventIds(clientIntegrationId, params.seasonId)

  private val logger = LoggerFactory.getLogger(this.getClass)

  /** Main Sync Script that syncs inventory with integration
    *
    * @param inventory
    * @param clientIntegrationId
    * @return
    */
  def sync(eventId: Int, inventory: Seq[Inventory], clientIntegrationId: Int): Future[Seq[Unit]] = {
    if (!eventIdMappings.isDefinedAt(eventId)) {
      throw new Exception(
        s"No event id mapping for client integration $clientIntegrationId and event $eventId"
      )
    }

    val integrationInventory = getListings(eventIdMappings(eventId), inventory)

    val listFuture = Future {
      logger.info("Listing Inventory...")
      val newInventory = listInventory(inventory, integrationInventory)
      logger.info(s"New Inventory ${newInventory.length}")
      val total = Await.result(Future.sequence(createListings(newInventory)), Duration.Inf)
      logger.info(s"Done Posting ${total.sum} Tickets")
    }

    val delistFuture = Future {
      logger.info("Delisting Needed Inventory...")
      val delistings = delistInventory(inventory, integrationInventory)
      val total = Await.result(Future.sequence(delistListings(delistings)), Duration.Inf)
      logger.info(s"Done Delisting ${delistings.length} Tickets")
    }

    val updateFuture = Future {
      logger.info("Updating Inventory...")
      val updatings = updateInventory(inventory, integrationInventory)
      val total = Await.result(Future.sequence(updateListings(updatings)), Duration.Inf)
      logger.info(s"Done Updating ${total.sum} Tickets")
    }

    Future.sequence(Seq(listFuture, delistFuture, updateFuture))
  }

  /** Filters inventory that needs to be listed
    *
    * @param edInventory
    * @param integrationInventory
    * @return
    */
  def listInventory(
    edInventory: Seq[Inventory],
    integrationInventory: Seq[Inventory]
  ): Seq[Inventory] = {
    edInventory.flatMap(inventory => {
      if (integrationInventory.exists(i => i.matches(inventory))) {
        None
      } else {
        Some(inventory)
      }
    })
  }

  /** Filters inventory that needs to be delisted
    *
    * @param edInventory
    * @param integrationInventory
    * @return
    */
  def delistInventory(
    edInventory: Seq[Inventory],
    integrationInventory: Seq[Inventory]
  ): Seq[Inventory] = {
    // TODO: Delist inventory that has been bought (as opposed to removed)
    integrationInventory.flatMap(inventory => {
      if (!edInventory.exists(i => i.matches(inventory))) {
        Some(inventory)
      } else {
        None
      }
    })
  }

  /** Filters inventory where the prices need to be updated
    *
    * @param edInventory
    * @param integrationInventory
    * @return
    */
  def updateInventory(
    edInventory: Seq[Inventory],
    integrationInventory: Seq[Inventory]
  ): Seq[Inventory] = {
    integrationInventory
      .groupBy(_.listingId)
      .flatMap(group => {
        val ids = group._2.map(_.id)
        edInventory.filter(i => ids.contains(i.id)) match {
          case edGroup if edGroup.nonEmpty =>
            val edPrices =
              if (edGroup.exists(_.overridePrice.isDefined))
                edGroup.filter(_.overridePrice.isDefined)
              else edGroup
            val edPrice = edPrices.maxBy(_.getPrice).getPrice

            if (edPrice != group._2.head.getPrice) {
              group._2.map(_.copy(listedPrice = edPrice))
            } else {
              None
            }
          case _ => None
        }
      })
      .toSeq
  }

  protected def getListings(eventId: String, inventory: Seq[Inventory]): Seq[Inventory]

  protected def createListings(inventory: Seq[Inventory]): Seq[Future[Int]]

  protected def updateListings(inventory: Seq[Inventory]): Seq[Future[Int]]

  protected def delistListings(inventory: Seq[Inventory]): Seq[Future[Int]]
}
