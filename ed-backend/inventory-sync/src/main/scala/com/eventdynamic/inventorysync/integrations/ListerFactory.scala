package com.eventdynamic.inventorysync.integrations

import com.eventdynamic.IntegrationServiceBuilder
import com.eventdynamic.inventorysync.models.Params
import com.eventdynamic.inventorysync.services.ServiceHolder
import com.eventdynamic.models.{
  ClientIntegrationComposite,
  EDTicketsClientIntegrationConfig,
  StubHubClientIntegrationConfig
}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object ListerFactory
    extends StubHubClientIntegrationConfig.format
    with EDTicketsClientIntegrationConfig.format {
  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  def getListersForClient(
    clientId: Int,
    serviceHolder: ServiceHolder,
    params: Params,
    integrationServiceBuilder: IntegrationServiceBuilder
  ): Future[Map[ClientIntegrationComposite, Lister]] = {
    logger.info(s"Creating listers for client $clientId")

    for {
      clientIntegrations <- serviceHolder.clientIntegrationService
        .getByClientId(clientId, onlyActive = false)
    } yield {
      clientIntegrations
        .filter(integration => params.integration.forall(_ == integration.name))
        .map(clientIntegration => {
          clientIntegration -> buildLister(
            clientIntegration,
            serviceHolder,
            params,
            integrationServiceBuilder
          )
        })
        .collect {
          case (ci, Some(lister)) => (ci, lister)
        }
        .toMap
    }
  }

  def buildLister(
    clientIntegration: ClientIntegrationComposite,
    serviceHolder: ServiceHolder,
    params: Params,
    integrationServiceBuilder: IntegrationServiceBuilder
  ): Option[Lister] = {
    clientIntegration.name match {
      case "EDTickets" =>
        val edTicketsService = integrationServiceBuilder.buildEDTicketsService(
          clientIntegration.config[EDTicketsClientIntegrationConfig]
        )
        Some(new EDTicketsLister(serviceHolder, params, clientIntegration.id.get, edTicketsService))

      case "StubHub" =>
        val stubHubService = integrationServiceBuilder.buildStubHubService(
          clientIntegration.config[StubHubClientIntegrationConfig]
        )
        Some(new StubHubLister(serviceHolder, params, clientIntegration.id.get, stubHubService))

      case name =>
        logger.warn(s"Lister for integration $name is not supported.")
        None
    }
  }
}
