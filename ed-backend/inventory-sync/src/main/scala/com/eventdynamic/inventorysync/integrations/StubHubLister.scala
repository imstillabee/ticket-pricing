package com.eventdynamic.inventorysync.integrations

import com.eventdynamic.inventorysync.models._
import com.eventdynamic.inventorysync.services.ServiceHolder
import com.eventdynamic.models.{ClientIntegrationEventSeat, Event, Inventory}
import com.eventdynamic.stubhub.StubHubService
import com.eventdynamic.stubhub.models._
import com.eventdynamic.utils.DateHelper
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class StubHubLister(
  override val serviceHolder: ServiceHolder,
  override val params: Params,
  override val clientIntegrationId: Int,
  val apiService: StubHubService
) extends Lister(params, serviceHolder, clientIntegrationId) {

  private val logger = LoggerFactory.getLogger(this.getClass)

  // Number of seats per listing allowed. StubHub limit is 30
  final private val seatsPerListing: Int = 30

  /**
    * Get Listings from StubHub and map listings to inventory to start sync
    *
    * @param eventId StubHub event id
    * @param inventory ED inventory
    * @return StubHub inventory
    */
  override protected def getListings(eventId: String, inventory: Seq[Inventory]): Seq[Inventory] = {
    val future = for {
      listings <- fetchStubHubListings(eventId)
      inventory <- mapListingToInventory(eventId, listings)
    } yield inventory

    Await.result(future, Duration.Inf)
  }

  /**
    * Push Listings to StubHub
    */
  override def createListings(inventory: Seq[Inventory]): Seq[Future[Int]] = {
    case class RowKey(eventId: Int, section: String, row: String)

    // Group the listings by row
    val inventoryRows = stubHubInventory(inventory)
      .groupBy(i => RowKey(i.eventId, i.section, i.row))

    // Create listing(s) for each row of inventory
    val futures = inventoryRows.flatMap(inventoryRow => {
      val (key, items) = inventoryRow
      val price = items.map(_.getPrice).max

      // Break down large rows into multiple listings because StubHub limits listings to 30 seats
      val listings = items.grouped(seatsPerListing)

      // Save each listing in StubHub then EventDynamic
      listings.map(listing => {
        for {
          ApiSuccess(_, response: StubHubListingResponse) <- apiService
            .list(listing, eventIdMappings(key.eventId), price)
          count <- saveListingsToED(response, listing)
        } yield count
      })
    })

    futures.toSeq
  }

  /**
    * Push Updates to Listings on StubHub
    */
  override def updateListings(inventory: Seq[Inventory]): Seq[Future[Int]] = {
    Seq(Future.successful(0))
  }

  /**
    * Remove listings
    */
  override def delistListings(inventory: Seq[Inventory]): Seq[Future[Int]] = {
    stubHubInventory(inventory)
      .groupBy(i => i.listingId)
      .keys
      .map(listingId => {
        synchronized {
          apiService.deleteListing(listingId.get).map {
            case _: ApiSuccess => 1
            case err =>
              logger.error("Error deleting StubHub listing", err)
              0
          }
        }
      })
      .toSeq
  }

  /**
    * Map inventory to StubHubInventory
    */
  def stubHubInventory(inventory: Seq[Inventory]): Seq[StubHubInventory] = {
    inventory.map(i => {
      StubHubInventory(
        i.id.get,
        i.eventId,
        i.eventIsBroadcast,
        i.section,
        i.row,
        i.rowIsListed,
        i.seat,
        i.listedPrice,
        i.overridePrice,
        i.listingId.map(_.toInt)
      )
    })
  }

  /**
    * Saves Listings and EventSeats to ED
    */
  def saveListingsToED(
    response: StubHubListingResponse,
    inventory: Seq[StubHubInventory]
  ): Future[Int] = {
    val clientIntegrationEventId =
      clientIntegrationEventIdMappings.getOrElse(inventory.head.eventId, 0)

    serviceHolder.clientIntegrationListingsService
      .create(clientIntegrationEventId, response.listingId)
      .flatMap(clientIntegrationListing => {
        logger.info(s"Saving Listing ${response.listingId}")
        val cies = mapInventoryToCIES(inventory, clientIntegrationListing.id.get)
        serviceHolder.clientIntegrationEventSeatsService.bulkInsert(cies).map(_.getOrElse(0))
      })
      .recover {
        case err =>
          logger.error(s"Unable to save listing ${response.listingId} - $err")
          0
      }

  }

  /**
    * Maps Inventory to ClientIntegrationEventSeat for bulk insert into ED
    */
  def mapInventoryToCIES(
    inventory: Seq[StubHubInventory],
    listingId: Int
  ): Seq[ClientIntegrationEventSeat] = {
    val now = DateHelper.now()
    inventory.map(i => {
      ClientIntegrationEventSeat(None, now, now, listingId, i.id)
    })
  }

  /**
    * Get Listings From StubHub
    * The api for getting listings limits 200 listings per request.
    * Therefore here we iterate until we reach retrieve listings
    */
  def fetchStubHubListings(eventId: String): Future[Seq[StubHubListing]] = {
    val pageSize: Float = 200

    apiService
      .getInventory(start = 0, eventId = Some(eventId))
      .flatMap {
        case ApiSuccess(_, listings: StubHubAccountManagementResponse) =>
          logger.info(s"Getting Listing Page 1...")

          val iteration = Seq(Future.successful(listings.listings.getOrElse(Seq()))) ++ Iterator
            .range(1, math.ceil(listings.numFound.toFloat / pageSize).toInt)
            .map(i => {
              logger.info(s"Getting Listing Page ${i + 1}...")
              apiService
                .getInventory(i * pageSize.toInt)
                .map {
                  case ApiSuccess(_, listings: StubHubAccountManagementResponse) =>
                    listings.listings.getOrElse(Seq())
                  case _ => Seq()
                }
            })
            .toSeq
          Future.sequence(iteration).map(_.flatten)
      }
  }

  /**
    * Maps StubHubListings to ED Inventory
    */
  def mapListingToInventory(
    stubHubEventId: String,
    listings: Seq[StubHubListing]
  ): Future[Seq[Inventory]] = {
    for {
      event <- getEvent(stubHubEventId)
      inventory <- transformListings(listings, event.get)
    } yield inventory
  }

  private def transformListings(
    listings: Seq[StubHubListing],
    event: Event
  ): Future[Seq[Inventory]] = {
    Future.sequence(
      listings.flatMap(
        listing =>
          listing.seats
            .split(",")
            .map(
              seatNumber =>
                for {
                  seat <- serviceHolder.seatService
                    .get(seatNumber, listing.row, listing.section, event.primaryEventId.get)
                  eventSeat <- seat match {
                    case None    => Future.successful(None)
                    case Some(s) => serviceHolder.eventSeatService.get(s.id.get, event.id.get)
                  }
                } yield {
                  Inventory(
                    eventSeat.get.id.orElse(None),
                    event.id.get,
                    event.isBroadcast,
                    seat match {
                      case None    => listing.section
                      case Some(s) => s.section
                    },
                    seat match {
                      case None    => listing.row
                      case Some(s) => s.row
                    },
                    rowIsListed = true,
                    seat match {
                      case None    => seatNumber
                      case Some(s) => s.seat
                    },
                    listing.currentPrice,
                    None,
                    Some(listing.id)
                  )
              }
          )
      )
    )
  }

  private def getEvent(stubHubEventId: String): Future[Option[Event]] = {
    for {
      ci <- serviceHolder.clientIntegrationService.getById(clientIntegrationId)
      cie <- serviceHolder.clientIntegrationEventsService
        .getByExternalEventId(ci.get.id.get, stubHubEventId)
      event <- serviceHolder.eventService.getById(cie.get.eventId)
    } yield event
  }
}
