package com.eventdynamic.inventorysync.models

case class Params(
  clientId: Int,
  integration: Option[String],
  seasonId: Option[Int],
  eventId: Option[Int],
  section: Option[String]
)

object Params {

  /**
    * Parse params from command line strings
    *
    * @param args command line args string list
    * @return
    */
  def fromArgs(args: Array[String]): Params = {
    val opts = args.toList
      .grouped(2)
      .map(arg => {
        val Seq(command, value) = arg
        command.replace("-", "") -> value
      })
      .toMap

    val clientId = opts
      .get("clientId")
      .map(_.toInt)
      .getOrElse(throw new IllegalArgumentException("clientId is required"))

    val seasonId = opts.get("seasonId").map(_.toInt)
    val eventId = opts.get("eventId").map(_.toInt)
    val section = opts.get("section")
    val integration = opts.get("integration")

    Params(clientId, integration, seasonId, eventId, section)
  }
}
