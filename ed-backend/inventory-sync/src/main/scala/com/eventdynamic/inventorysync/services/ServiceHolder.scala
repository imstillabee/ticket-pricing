package com.eventdynamic.inventorysync.services

import com.eventdynamic.db.EDContext
import com.eventdynamic.services._
import com.eventdynamic.transactions.IntegrationTransactions

import scala.concurrent.ExecutionContext.Implicits.global

case class ServiceHolder(
  clientIntegrationService: ClientIntegrationService,
  clientIntegrationEventsService: ClientIntegrationEventsService,
  clientIntegrationEventSeatsService: ClientIntegrationEventSeatsService,
  clientIntegrationListingsService: ClientIntegrationListingsService,
  eventSeatService: EventSeatService,
  eventService: EventService,
  seatService: SeatService,
  venueService: VenueService,
  inventoryService: InventoryService,
  integrationTransactions: IntegrationTransactions,
  secondaryPricingRulesService: SecondaryPricingRulesService
)

object ServiceHolder {

  def apply(ed: EDContext): ServiceHolder = {
    val integrationService = new IntegrationService(ed)
    val secondaryPricingRulesService = new SecondaryPricingRulesService(ed)
    val clientIntegrationService = new ClientIntegrationService(ed)
    val clientIntegrationEventsService = new ClientIntegrationEventsService(ed)
    val clientIntegrationEventSeatsService = new ClientIntegrationEventSeatsService(ed)
    val clientIntegrationListingsService = new ClientIntegrationListingsService(ed)
    val eventSeatService = new EventSeatService(ed)
    val eventService = new EventService(ed)
    val seatService = new SeatService(ed)
    val venueService = new VenueService(ed)
    val inventoryService =
      new InventoryService(ed, eventSeatService, eventService, seatService, venueService)
    val integrationTransactions =
      new IntegrationTransactions(
        ed,
        clientIntegrationService,
        clientIntegrationEventsService,
        clientIntegrationEventSeatsService,
        clientIntegrationListingsService,
        eventService,
        eventSeatService,
        integrationService,
        seatService
      )

    ServiceHolder(
      clientIntegrationService,
      clientIntegrationEventsService,
      clientIntegrationEventSeatsService,
      clientIntegrationListingsService,
      eventSeatService,
      eventService,
      seatService,
      venueService,
      inventoryService,
      integrationTransactions,
      secondaryPricingRulesService
    )
  }
}
