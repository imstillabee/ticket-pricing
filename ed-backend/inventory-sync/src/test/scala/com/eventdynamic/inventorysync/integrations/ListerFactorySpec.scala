package com.eventdynamic.inventorysync.integrations

import com.eventdynamic.IntegrationServiceBuilder
import com.eventdynamic.inventorysync.models.Params
import com.eventdynamic.inventorysync.services.ServiceHolder
import com.eventdynamic.models.ClientIntegrationComposite
import com.eventdynamic.services._
import com.eventdynamic.transactions.IntegrationTransactions
import com.eventdynamic.utils.DateHelper
import org.mockito.Mockito._
import org.scalatest.{AsyncWordSpec, Matchers}
import org.specs2.mock.Mockito

import scala.concurrent.Future

class ListerFactorySpec extends AsyncWordSpec with Mockito with Matchers {

  private val serviceHolder = new ServiceHolder(
    mock[ClientIntegrationService],
    mock[ClientIntegrationEventsService],
    mock[ClientIntegrationEventSeatsService],
    mock[ClientIntegrationListingsService],
    mock[EventSeatService],
    mock[EventService],
    mock[SeatService],
    mock[VenueService],
    mock[InventoryService],
    mock[IntegrationTransactions],
    mock[SecondaryPricingRulesService]
  )

  private val integrationServiceBuilder = mock[IntegrationServiceBuilder]

  private def buildClientIntegration(
    id: Int,
    name: String,
    primary: Boolean = true,
    active: Boolean = true,
    json: String = ""
  ) =
    ClientIntegrationComposite(
      Some(id),
      DateHelper.now(),
      DateHelper.now(),
      1,
      id,
      name,
      None,
      Some(json),
      active,
      primary,
      None,
      None,
      None
    )

  val stubHubJson = s"""{
     |"mode": "mode",
     |"username": "username",
     |"password": "password",
     |"consumerKey": "key",
     |"consumerSecret": "secret",
     |"baseUrl": "https://baseurl",
     |"applicationToken": "token",
     |"sellerGUID": "70214d9b-a4c3-4a58-8d12-af4d0d60eed2"
     |}
     """.stripMargin
  val edTicketsJson = s"""{
     |"apiKey": "apikey",
     |"appId": "appid",
     |"baseUrl": "https://baseurl",
     |"appDatabase": "database",
     |"email": "email@email.com",
     |"password": "password",
     |"functionsUrl": "https://functionalurl"
     |}
     """.stripMargin
  val ticketsComJson = s"""{
      |"agent":"agent",
      |"apiKey":"appKey",
      |"appId":"appId",
      |"username":"username",
      |"password":"password",
      |"baseUrl":"https://baseUrl",
      |"supplierId":1,
      |"tdcProxyBaseUrl": "proxyBaseUrl",
      |"mlbamGridConfigs":[]
      |}
      """.stripMargin

  "ListerFactory#getListersForClient" should {
    val clientIntegrations = Seq(
      buildClientIntegration(1, "StubHub", false, false, json = stubHubJson),
      buildClientIntegration(2, "EDTickets", false, json = edTicketsJson),
      buildClientIntegration(3, "Tickets.com", true, json = ticketsComJson)
    )

    when(serviceHolder.clientIntegrationService.getByClientId(any[Int], any[Boolean], any[Boolean]))
      .thenReturn(Future.successful(clientIntegrations))

    "build a lister map for all client integrations" in {
      val params = Params(1, None, None, None, None)

      ListerFactory
        .getListersForClient(1, serviceHolder, params, integrationServiceBuilder)
        .map(clientIntegrationListers => {
          clientIntegrationListers.values.toList should have length 2

          val stubHub = clientIntegrationListers.find(_._1.name == "StubHub").get
          stubHub._2 shouldBe a[StubHubLister]

          val ed = clientIntegrationListers.find(_._1.name == "EDTickets").get
          ed._2 shouldBe a[EDTicketsLister]
        })
    }

    "build a lister map the client integration matching the integration filter" in {
      val params = Params(1, Some("StubHub"), None, None, None)

      ListerFactory
        .getListersForClient(1, serviceHolder, params, integrationServiceBuilder)
        .map(clientIntegrationListers => {
          clientIntegrationListers.values.toList should have length 1

          val stubHub = clientIntegrationListers.find(_._1.name == "StubHub").get
          stubHub._2 shouldBe a[StubHubLister]
        })
    }
  }

  "ListerFactory#buildLister" should {
    val params = Params(1, None, None, None, None)

    "build an EDTicketsLister" in {
      val integration = buildClientIntegration(id = 1, "EDTickets", json = edTicketsJson)
      val lister =
        ListerFactory.buildLister(integration, serviceHolder, params, integrationServiceBuilder)

      lister shouldBe defined
      lister.get shouldBe a[EDTicketsLister]
    }

    "build a StubHubLister" in {
      val integration = buildClientIntegration(id = 1, "StubHub", json = stubHubJson)
      val lister =
        ListerFactory.buildLister(integration, serviceHolder, params, integrationServiceBuilder)

      lister shouldBe defined
      lister.get shouldBe a[StubHubLister]
    }
  }
}
