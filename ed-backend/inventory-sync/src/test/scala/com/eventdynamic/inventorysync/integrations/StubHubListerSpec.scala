package com.eventdynamic.inventorysync.integrations

import java.sql.Timestamp

import com.eventdynamic.inventorysync.models.Params
import com.eventdynamic.inventorysync.services.ServiceHolder
import com.eventdynamic.models._
import com.eventdynamic.services._
import com.eventdynamic.stubhub.StubHubService
import com.eventdynamic.stubhub.models._
import com.eventdynamic.transactions.IntegrationTransactions
import com.eventdynamic.utils.DateHelper
import org.mockito.Mockito.{times, verify, when}
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll}
import org.specs2.mock.Mockito

import scala.concurrent.Future

class StubHubListerSpec extends AsyncWordSpec with Mockito with BeforeAndAfterAll {

  val serviceHolder = new ServiceHolder(
    mock[ClientIntegrationService],
    mock[ClientIntegrationEventsService],
    mock[ClientIntegrationEventSeatsService],
    mock[ClientIntegrationListingsService],
    mock[EventSeatService],
    mock[EventService],
    mock[SeatService],
    mock[VenueService],
    mock[InventoryService],
    mock[IntegrationTransactions],
    mock[SecondaryPricingRulesService]
  )

  val apiService = mock[StubHubService]

  val stubHubLister =
    new StubHubLister(serviceHolder, mock[Params], 1, apiService) {
      override val clientIntegrationEventIdMappings = Map(1 -> 1)
    }

  val inventory = Seq(
    Inventory(Some(1), 1, true, "1", "1", true, "1", 10.00, None, None),
    Inventory(Some(2), 1, true, "1", "1", true, "2", 10.00, None, None),
    Inventory(Some(3), 1, true, "1", "1", true, "3", 10.00, None, None)
  )

  val integrationInventory = Seq(
    Inventory(Some(4), 1, true, "1", "1", false, "4", 10.00, None, None),
    Inventory(Some(5), 1, true, "1", "1", true, "1", 10.00, None, None),
    Inventory(Some(6), 1, true, "1", "1", true, "3", 20.00, None, None)
  )

  val stubHubInventory = Seq(
    StubHubInventory(4, 1, true, "1", "1", true, "4", 10.00, None, None),
    StubHubInventory(5, 1, true, "1", "1", true, "1", 10.00, None, None),
    StubHubInventory(6, 1, true, "1", "1", true, "3", 10.00, None, None)
  )

  val listings = Seq(StubHubListing("1", "1", "ACTIVE", 10.00, "1", "1", "1,2,3,4,5"))

  val eventIdMappings = Map(1 -> "1", 2 -> "2")
  val clientIntegrationEventIDMappings = Map(1 -> 1, 2 -> 2)

  override def beforeAll(): Unit = {
    super.beforeAll()

    when(
      serviceHolder.integrationTransactions.getClientIntegrationEventIds(any[Int], any[Option[Int]])
    ).thenReturn(clientIntegrationEventIDMappings)
    when(serviceHolder.integrationTransactions.getIntegrationEventIds(any[Int], any[Option[Int]]))
      .thenReturn(eventIdMappings)
  }

  "StubHubLister#listInventory" should {
    "have no inventory to list if integration inventory and ed inventory are the same" in {
      val listings = stubHubLister.listInventory(inventory, inventory)

      assert(listings.isEmpty)
    }

    "get inventory to list" in {
      val listings = stubHubLister.listInventory(inventory, integrationInventory)

      assert(listings.nonEmpty)
      assert(listings.length == 1)
    }
  }

  "StubHubLister#delistInventory" should {
    "have no inventory to delist if integration inventory and ed inventory are the same" in {
      val listings = stubHubLister.delistInventory(inventory, inventory)

      assert(listings.isEmpty)
    }

    "get inventory to delist" in {
      val listings = stubHubLister.delistInventory(inventory, integrationInventory)

      assert(listings.nonEmpty)
      assert(listings.length == 1)
    }
  }

  "StubHubLister#updateInventory" should {
    "have no inventory to update if integration inventory and ed inventory are the same" in {
      val listings = stubHubLister.updateInventory(inventory, inventory)

      assert(listings.isEmpty)
    }
  }

  "StubHubLister#mapInventoryToCIES" should {
    "map the inventory to client integration event seats" in {
      val listings = stubHubLister.mapInventoryToCIES(stubHubInventory, 1)

      assert(listings.isInstanceOf[Seq[ClientIntegrationEventSeat]])
      assert(listings.length == 3)
    }
  }

  "StubHubLister#mapListingToInventory" should {
    "map stub hub listings to event dynamic inventory" in {
      val clientId = 2
      val eventId = 10
      val integrationEventId = "100"
      val primaryEventId = "12"
      val venueId = 3
      val now = DateHelper.now()

      when(serviceHolder.clientIntegrationService.getById(any[Int]))
        .thenReturn(
          Future.successful(
            Option(
              ClientIntegrationComposite(
                Some(1),
                new Timestamp(0),
                new Timestamp(0),
                clientId,
                12,
                "test",
                Some("Version"),
                Some("{'json': true}"),
                true,
                false,
                Some(""),
                Some(10),
                Some(6.0)
              )
            )
          )
        )

      when(serviceHolder.clientIntegrationEventsService.getByExternalEventId(any[Int], any[String]))
        .thenReturn(
          Future.successful(
            Option(
              ClientIntegrationEvent(
                Some(1),
                new Timestamp(0),
                11,
                eventId,
                integrationEventId,
                new Timestamp(0)
              )
            )
          )
        )

      when(serviceHolder.eventService.getById(any[Int]))
        .thenReturn(
          Future.successful(
            Option(
              Event(
                id = Some(eventId),
                primaryEventId = Some(primaryEventId.toInt),
                createdAt = new Timestamp(0),
                modifiedAt = new Timestamp(0),
                name = "Some event",
                timestamp = Some(new Timestamp(0)),
                clientId = clientId,
                venueId = venueId,
                eventCategoryId = 1,
                seasonId = Some(1),
                isBroadcast = true,
                percentPriceModifier = 0,
                eventScore = None,
                eventScoreModifier = 0,
                spring = None,
                springModifier = 0
              )
            )
          )
        )

      when(
        serviceHolder.seatService
          .get(any[String], any[String], any[String], primaryEventId = any[Int])
      ).thenReturn(
        Future.successful(
          Option(
            Seat(Some(1), Some(10), new Timestamp(0), new Timestamp(0), "1", "1", "C", venueId)
          )
        )
      )

      when(serviceHolder.eventSeatService.get(any[Int], any[Int]))
        .thenReturn(
          Future.successful(
            Option(EventSeat(Some(1), 1, eventId, 1, Some(6.00), Some(6.00), true, now, now, false))
          )
        )

      val mappedListings = stubHubLister.mapListingToInventory("1", listings)

      assert(mappedListings.isInstanceOf[Future[Seq[Inventory]]])
    }
  }

  "StubHubLister#getStubHubListings" should {
    "get 0 stubhub listing when numFound = 0" in {
      when(apiService.getInventory(any[Int], any[Option[String]]))
        .thenReturn(Future.successful(ApiSuccess(200, StubHubAccountManagementResponse(0, None))))

      stubHubLister
        .fetchStubHubListings("1")
        .map(list => {
          assert(list.isInstanceOf[Seq[StubHubListing]])
          assert(list.isEmpty)
        })
    }

    "get 1 stubhub listing when numFound = 1" in {
      when(apiService.getInventory(any[Int], any[Option[String]]))
        .thenReturn(
          Future.successful(ApiSuccess(200, StubHubAccountManagementResponse(1, Some(listings))))
        )

      stubHubLister
        .fetchStubHubListings("1")
        .map(list => {
          assert(list.isInstanceOf[Seq[StubHubListing]])
          assert(list.length == 1)
        })
    }

    "get 2 stubhub listings when numFound > pageSize" in {
      when(apiService.getInventory(any[Int], any[Option[String]]))
        .thenReturn(
          Future.successful(ApiSuccess(200, StubHubAccountManagementResponse(201, Some(listings))))
        )

      stubHubLister
        .fetchStubHubListings("1")
        .map(list => {
          assert(list.isInstanceOf[Seq[StubHubListing]])
          assert(list.length == 2)
        })
    }

    "get correct total stubhub listings when numFound > pageSize" in {
      when(apiService.getInventory(any[Int], any[Option[String]]))
        .thenReturn(
          Future
            .successful(ApiSuccess(200, StubHubAccountManagementResponse(10930, Some(listings))))
        )

      stubHubLister
        .fetchStubHubListings("1")
        .map(list => {
          assert(list.isInstanceOf[Seq[StubHubListing]])
          assert(list.length == 55)
        })
    }
  }

  "StubHubLister#saveListingsToED" should {
    "Save Listings" in {
      val now = DateHelper.now()
      when(serviceHolder.clientIntegrationListingsService.create(any[Int], any[String]))
        .thenReturn(Future.successful(ClientIntegrationListing(Some(1), now, now, 1, "1")))
      when(
        serviceHolder.clientIntegrationEventSeatsService
          .bulkInsert(any[Seq[ClientIntegrationEventSeat]])
      ).thenReturn(Future.successful(Some(1)))

      stubHubLister.saveListingsToED(StubHubListingResponse("1", "ACTIVE", None), stubHubInventory)

      verify(serviceHolder.clientIntegrationListingsService, times(1)).create(any[Int], any[String])
      assert(true)
    }
  }

}
