package com.eventdynamic.inventorysync.models

import org.scalatest.FlatSpec

class ParamsSpec extends FlatSpec {
  "Params" should "initialize from args" in {
    val clientId = "1"
    val args = Array[String]("--clientId", clientId)
    val params = Params.fromArgs(args)

    assert(params == Params(clientId.toInt, None, None, None, None))
  }

  it should "initialize with optional args" in {
    val clientId = "1"
    val integration = "StubHub"
    val eventId = "2"
    val seasonId = "3"
    val section = "abc"
    val args = Array[String](
      "--section",
      section,
      "--seasonId",
      seasonId,
      "--eventId",
      eventId,
      "--integration",
      integration,
      "--clientId",
      clientId
    )
    val params = Params.fromArgs(args)

    assert(
      params == Params(
        clientId = clientId.toInt,
        integration = Some(integration),
        seasonId = Some(seasonId.toInt),
        eventId = Some(eventId.toInt),
        section = Some(section)
      )
    )
  }
}
