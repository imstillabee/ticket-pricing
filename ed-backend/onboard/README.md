# Onboard

This project onboards a client into our platform.  We gather data about their historical events, transactions, etc and load them into the system.

Data is by-default read in the `data` folder, but that can be configured.

Tests run based off data in `example-data`.

## Usage

`sbt onboard/run [data]`

| Param | Default | Description | 
| :---  | :--- | :--- |
| __data__ | `onboard/data` | path to folder containing data for onboarding

## Files

See `example-data` for examples of each of these files.

### basic-info.json

Contains one-off data points (names, integrations, etc) or small lists of information.

### event-seats.csv

1. integrationSeatId - the seat id in the primary integration
1. integrationEventId - the event id in the primary integration
1. priceScaleId - the price scale id in the primary integration
1. listedPrice - the price that the ticket was last listed at
1. isHeld - the current hold status of an event seat that indicates if it is available for sale

### events.csv

1. timestamp - time that the event started
1. season - name of the season (same named season will be combined)
1. name - name of the event
1. primaryEventId - the event id in the primary integration
1. eventCategory - the event category in string format

### price-scales.csv

1. integrationId - the priceScale id in the primary integration
1. name - name of the price scale as it will be shown in the UI

### seats.csv

1. primarySeatId - the seat id in the primary integration
1. seat - the seat number
1. row - the row number
1. section - the section number

* note these "numbers" are actually strings

### transactions.csv

1. timestamp = the time the transaction occurred
1. integrationEventId - the event id in the primary integration
1. integrationSeatId - the seat id in the primary integration
1. buyerTypeCode - the buyer type code (adult, child, etc)
1. price - the price of the ticket
1. primaryTransactionId - the id of the transaction in the primary database
1. transactionType - the type of the transaction, either "Purchase" or "Return"

## Onboarding from TDC

The following are SQL queries used to import data from a fake client in the TDC Sandbox replicated environment.

Oracle SQLDeveloper was used to run these and export to csv.  DBeaver was too slow and timed out.

### event-seats.csv

```sql
SELECT
	es.seat_id AS "integrationSeatId",
	e.event_id AS "integrationEventId",
	ps.price_scale_id AS "priceScaleId",
	pp.price AS "listedPrice",
	(
		CASE WHEN hc.hold_code_type_code = 'O' THEN
			'false'
		ELSE 
			'true'
		END) AS "isHeld"
FROM event e
	INNER JOIN event_seat es ON e.event_id = es.event_id
	LEFT JOIN price_scale_map_seat psms ON psms.seat_id = es.seat_id
	LEFT JOIN price_scale_map psm ON psm.price_scale_map_id = psms.price_scale_map_id
	LEFT JOIN price_scale ps ON psm.price_scale_id = ps.price_scale_id
	LEFT JOIN price_point pp ON pp.price_scale_id = ps.price_scale_id AND e.price_structure_id = pp.price_structure_id
	LEFT JOIN buyer_type bt ON pp.buyer_type_id = bt.buyer_type_id
	LEFT JOIN hold_code hc ON es.hold_code_id = hc.hold_code_id
WHERE
	e.primary_organization_id = 1141 -- identifying Zephyr's (might not be the same for production)
	AND e.event_category_id = 1021 -- regular season game
	AND e.event_class_code = 'P' -- primary (?)
	AND e.event_true_type_code = 'S' -- single (?)
	AND e.event_inventory_type_code = 'I' -- inventories (?)
	AND bt.buyer_type_code = 'ADULT' -- only get prices for adult tickets
```

### events.csv

```sql
SELECT
	e.event_date AS "timestamp",
	s.description "season",
	e.description "name",
	e.event_id "primaryEventId",
	e.venue_id "venueId"
FROM event e
	INNER JOIN season s ON e.season_id = s.season_id
WHERE
	e.primary_organization_id = 1141 -- identifying Zephyr's (might not be the same for production)
	AND e.event_category_id = 1021 -- regular season game
	AND e.event_class_code = 'P' -- primary (?)
	AND e.event_true_type_code = 'S' -- single (?)
	AND e.event_inventory_type_code = 'I' -- inventories (?)
```

### seats.csv

```sql
SELECT
	st.seat_id "primarySeatId",
	st.seat_number "seat",
	st.row_ "row",
	s.section_code "section"
FROM venue v
	INNER JOIN section s ON v.venue_id = s.venue_id
	INNER JOIN seat st ON st.section_id = s.section_id
WHERE v.venue_id = 3140
```

### transactions.csv

```sql
SELECT
	t.transaction_date "timestamp", -- may need to be converted to YYYY-MM-DD HH-MM-SS.fff format
	oli.event_id "integrationEventId",
	tk.seat_id "integrationSeatId",
	bt.buyer_type_code "buyerTypeCode",
	tk.price "price"
FROM transaction t
	INNER JOIN order_line_item oli ON t.transaction_id = oli.transaction_id
	INNER JOIN event e ON e.event_id = oli.event_id
	INNER JOIN transaction_type tt ON tt.transaction_type_code = oli.transaction_type_code
	INNER JOIN ticket tk ON t.transaction_id = tk.transaction_id
	INNER JOIN buyer_type bt ON tk.buyer_type_id = bt.buyer_type_id
WHERE
	e.primary_organization_id = 1141 -- identifying Zephyr's (might not be the same for production)
	AND e.event_category_id = 1021 -- regular season game
	AND e.event_class_code = 'P' -- primary (?)
	AND e.event_true_type_code = 'S' -- single (?)
	AND e.event_inventory_type_code = 'I' -- inventories (?)
```
