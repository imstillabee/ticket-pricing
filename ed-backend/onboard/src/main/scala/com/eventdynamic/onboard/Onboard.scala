package com.eventdynamic.onboard

import com.eventdynamic.db.EDContext
import com.eventdynamic.models._
import com.eventdynamic.onboard.models._
import com.eventdynamic.services._
import com.eventdynamic.utils.{DuplicateResponse, SuccessResponse}
import com.github.tototoshi.csv.CSVReader
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success, Try}

/**
  * Main class for a script that onboards a client.  Currently used to populate data in dev and test environments.
  */
object Onboard {

  final val BASIC_INFO_FILE = "basic-info.json"
  final val EVENTS_FILE = "events.csv"
  final val SEATS_FILE = "seats.csv"
  final val EVENT_SEATS_FILE = "event-seats.csv"
  final val TRANSACTIONS_FILE = "transactions.csv"
  final val PRICE_SCALES_FILE = "price-scales.csv"

  def toInt(s: String): Option[Int] = Try(s.toInt).toOption
  def toInt(s: Option[String]): Option[Int] = s.flatMap(toInt)

  private val logger = LoggerFactory.getLogger(this.getClass)

  @deprecated("Use the kantan csv parser", "March 9th 2019")
  def readCSV(path: String): Seq[Map[String, String]] = {
    logger.info(s"Reading CSV ($path)")
    val reader = CSVReader.open(path)
    try {
      reader.allWithHeaders()
    } finally {
      reader.close()
      logger.info("Done reading CSV")
    }
  }

  /**
    * Main function parses arguments and creates contexts needed for process.
    *
    * @param args arguments needed for Onboarding
    */
  def main(args: Array[String]): Unit = {
    // Create context and services
    val context = new EDContext()
    val services = ServiceHolder.from(context)

    val basePath = args.headOption.getOrElse("onboard/data")

    // Start processing
    process(context, services, basePath)

    context.dispose()
  }

  /**
    * The onboard process function.
    *
    * @param ctx the database context
    */
  def process(ctx: EDContext, services: ServiceHolder, basePath: String): Unit = {
    // Process basic info first
    logger.info("Reading basic info file")
    val basicInfo = BasicInfo.fromFile(s"$basePath/$BASIC_INFO_FILE")
    val primaryIntegrationInfo = basicInfo.clientIntegrations.find(_.isPrimary).get

    val future = for {
      client <- createClient(services.client, basicInfo.client)

      _ <- services.teamAbbreviationService.create(basicInfo.client.teamAbbreviation, client.id.get)

      _ <- createClientIntegrations(
        client,
        services.clientIntegration,
        services.integration,
        basicInfo.clientIntegrations
      )

      _ <- createUsers(services.user, client, basicInfo.users)

      venue <- createVenue(services.venue, basicInfo.venue)

      revenueCategoryMappings <- createRevenueCategoryMappings(
        services.revenueCategory,
        services.revenueCategoryMapping,
        basicInfo.revenueCategories,
        client
      )

      (seasons, events) <- OnboardEvents.createSeasonsAndEvents(services, client, venue, basePath)

      priceScales <- OnboardPriceScales.createPriceScales(
        services.priceScale,
        basePath,
        venue.id.get
      )

      seats <- OnboardSeats.createSeats(services.seatService, basePath, venue.id.get)

      eventSeats <- OnboardEventSeats.createEventSeats(
        services,
        events,
        seats,
        priceScales,
        basePath
      )

      primaryIntegration <- services.integration.getByName(primaryIntegrationInfo.name)

      _ <- OnboardJobConfigs.createJobConfigs(
        client,
        primaryIntegrationInfo,
        services.jobConfigService
      )

    } yield {
      OnboardTransactions.createTransactions(
        services,
        seats,
        events,
        eventSeats,
        revenueCategoryMappings,
        primaryIntegration.get.id.get,
        client.id.get,
        basePath
      )

      // Need to do this after transaction are imported
      resetSeasonDates(services, seasons)
    }

    Await.ready(future, Duration.Inf).andThen {
      case Success(_) => logger.info("Done")
      case Failure(e) => logger.error("Failed to onboard", e)
    }
  }

  /**
    * Creates a client and returns it if created successfully.
    *
    * @param clientService injected client service
    * @param clientInfo    client info
    * @return client if successful, else none
    */
  def createClient(clientService: ClientService, clientInfo: ClientInfo): Future[Client] = {
    val clientPerformanceType = Try(ClientPerformanceType.withName(clientInfo.performanceType)) match {
      case Success(performanceType) => performanceType
      case Failure(_) =>
        throw new Exception(s"Invalid client performance type: ${clientInfo.performanceType}")
    }
    clientService
      .createUnique(
        clientInfo.name,
        15,
        clientInfo.eventScoreFloor,
        clientInfo.eventScoreCeiling,
        clientInfo.springFloor,
        clientInfo.springCeiling,
        clientPerformanceType,
        clientInfo.logoUrl
      )
      .map {
        case SuccessResponse(client: Client) =>
          logger.info(s"Created client ${client.name}")
          client
        case DuplicateResponse =>
          throw new Exception(s"Client ${clientInfo.name} already exists")
        case _ => throw new Exception("Unknown response from creating client")
      }
  }

  /** Sets primary and secondary integrations for client
    *
    * @param client
    * @param clientIntegrationService
    * @return
    */
  def createClientIntegrations(
    client: Client,
    clientIntegrationService: ClientIntegrationService,
    integrationService: IntegrationService,
    clientIntegrationInfo: Seq[ClientIntegrationInfo]
  ): Future[Seq[ClientIntegration]] = {
    val futures = clientIntegrationInfo.map(ci => {
      for {
        integration <- integrationService.getByName(ci.name).map(_.get)
        clientIntegration <- clientIntegrationService
          .create(client.id.get, integration.id.get, ci.isActive, ci.isPrimary)
          .andThen {
            case Success(_) => logger.info(s"Created client integration ${integration.name}")
            case Failure(e) =>
              logger.error(s"Failed to create client integration for ${integration.name}", e)
          }
      } yield clientIntegration
    })

    Future.sequence(futures).andThen {
      case Success(cis) => logger.info(s"Create ${cis.length} client integrations")
      case Failure(e)   => logger.error(s"Failed to create client integrations", e)
    }
  }

  /**
    * Creates each user defined in the sequence of user info.
    *
    * @param userService injected user service
    * @param client      the client to attach the users to
    * @param userInfo    each user's information
    */
  def createUsers(
    userService: UserService,
    client: Client,
    userInfo: Seq[UserInfo]
  ): Future[Seq[Int]] = {
    logger.info("Creating users")
    val userFutures = userInfo.map(user => {
      // Create user
      userService
        .create(
          user.email,
          user.first,
          user.last,
          Option(user.password),
          client.id.get,
          None,
          isAdmin = false,
          5760
        )
        .andThen {
          case Success((newUser, tPass)) => logger.info(s"Created user ${user.email}")
          case Failure(e)                => logger.error(s"Unable to create user ${user.email}", e)
        }
        .map(tuple => {
          val (newUser, _) = tuple
          newUser.id.get
        })
    })

    // Check success of all users
    Future.sequence(userFutures).andThen {
      case Success(ids) => logger.info(s"Done creating ${ids.length} users")
      case Failure(e)   => logger.error("Failed to create users", e)
    }
  }

  /**
    * Creates a venue from the venue info passed in from JSON
    *
    * @param venueService injected venue service
    * @param venueInfo    the basic venue data
    */
  def createVenue(venueService: VenueService, venueInfo: VenueInfo): Future[Venue] = {
    logger.info("Creating venue")

    val createFuture = if (venueInfo.timeZone.isDefined) {
      venueService
        .create(
          venueInfo.name,
          venueInfo.capacity,
          venueInfo.zipcode,
          venueInfo.svgUrl,
          timeZone = venueInfo.timeZone.get
        )
    } else {
      venueService
        .create(venueInfo.name, venueInfo.capacity, venueInfo.zipcode, venueInfo.svgUrl)
    }

    createFuture
      .flatMap(venueId => venueService.getById(venueId))
      .map(_.get)
      .andThen {
        case Success(id) => logger.info(s"Created venue ${venueInfo.name} with id $id")
        case Failure(e)  => logger.error(s"Failed to create venue ${venueInfo.name}", e)
      }
  }

  /** Creates the revenue category mappings for the client buyer type codes passed in from JSON
    *
    * @param revenueCategoryService
    * @param revenueCategoryMappingService
    * @param revenueCategoryInfo
    * @param client
    */
  def createRevenueCategoryMappings(
    revenueCategoryService: RevenueCategoryService,
    revenueCategoryMappingService: RevenueCategoryMappingService,
    revenueCategoryInfo: Seq[RevenueCategoryInfo],
    client: Client
  ): Future[Seq[RevenueCategoryMapping]] = {
    logger.info("Creating revenue category mappings")
    val futures = revenueCategoryInfo.map(category => {
      for {
        revenueCategory <- revenueCategoryService.getOrCreate(category.name)
        mapping <- revenueCategoryMappingService
          .create(client.id.get, revenueCategory.id.get, category.regex, category.order)
      } yield mapping
    })

    Future.sequence(futures).andThen {
      case Success(mappings) => logger.info(s"Done creating ${mappings.length} revenue categories")
      case Failure(e)        => logger.error(s"Failed to create revenue categories and mappings", e)
    }
  }

  /**
    * Resets the start/end dates on the season.  Needs to be run after events and transactions are imported.
    *
    * @param services
    * @param seasons
    * @return
    */
  def resetSeasonDates(services: ServiceHolder, seasons: Seq[Season]): Unit = {
    logger.info(s"Starting to reset dates for ${seasons.length} seasons")

    val future = Future.sequence(seasons.map(services.seasonService.resetDates)).andThen {
      case _ => logger.info(s"Done resetting dates for ${seasons.length} seasons")
    }

    Await.result(future, Duration.Inf)
  }
}
