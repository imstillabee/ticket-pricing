package com.eventdynamic.onboard

import java.io.File

import com.eventdynamic.models.{Event, EventSeat, PriceScale, Seat}
import com.eventdynamic.onboard.models.ServiceHolder
import com.eventdynamic.utils.DateHelper
import kantan.csv._
import kantan.csv.generic._
import kantan.csv.ops._
import org.slf4j.LoggerFactory

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object OnboardEventSeats {

  private val logger = LoggerFactory.getLogger(this.getClass)

  implicit private val eventSeatRowDecoder: HeaderDecoder[EventSeatRow] = HeaderDecoder.decoder(
    "integrationSeatId",
    "integrationEventId",
    "priceScaleId",
    "listedPrice",
    "isHeld"
  )(EventSeatRow.apply)

  /**
    * The format of event-seat.csv
    *
    * @param integrationSeatId
    * @param integrationEventId
    * @param priceScaleId
    * @param listedPrice
    */
  case class EventSeatRow(
    integrationSeatId: Int,
    integrationEventId: Int,
    priceScaleId: Int,
    listedPrice: Option[BigDecimal],
    isHeld: Boolean
  )

  /**
    * Streams event seats into an iterator
    *
    * @param path path of the event-seat.csv file
    * @return an iterator of event seats
    */
  def readEventSeatsCSV(path: String): Iterator[EventSeat] = {
    val reader = new File(path).asUnsafeCsvReader[EventSeatRow](rfc.withHeader)
    val now = DateHelper.now()

    reader
      .map(
        esr =>
          EventSeat(
            id = None,
            seatId = esr.integrationSeatId,
            eventId = esr.integrationEventId,
            priceScaleId = esr.priceScaleId,
            listedPrice = esr.listedPrice,
            overridePrice = None,
            isListed = true,
            createdAt = now,
            modifiedAt = now,
            isHeld = esr.isHeld
        )
      )
      .toIterator
  }

  /** Create eventSeats
    *
    * @param services serviceHolder
    */
  def createEventSeats(
    services: ServiceHolder,
    events: Seq[Event],
    seats: Seq[Seat],
    priceScales: Seq[PriceScale],
    basePath: String
  ): Future[Seq[EventSeat]] = {
    val CHUNK_SIZE = 10000

    val eventMap = events.map(e => (e.primaryEventId.get, e)).toMap
    val seatMap = seats.map(s => (s.primarySeatId.get, s)).toMap
    val priceScaleMap = priceScales.map(ps => (ps.integrationId, ps)).toMap

    logger.info("Reading and parsing event seats")
    val records = readEventSeatsCSV(s"$basePath/${Onboard.EVENT_SEATS_FILE}")

    logger.info(s"Saving event seats in groups of $CHUNK_SIZE")
    records
      .map(es => {
        val seat = seatMap(es.seatId)
        val event = eventMap(es.eventId)
        val priceScale = priceScaleMap(es.priceScaleId)

        es.copy(
          seatId = seat.id.get,
          eventId = event.id.get,
          overridePrice = None,
          isListed = true,
          priceScaleId = priceScale.id.get
        )
      })
      .grouped(CHUNK_SIZE)
      .foreach(eventSeats => {
        val eventSeatFuture = services.eventSeatService.bulkInsert(eventSeats)
        val updates = Await.result(eventSeatFuture, Duration.Inf)
        logger.info(s"Added ${updates.getOrElse(0)} event seats")
      })

    services.eventSeatService.getForEvents(events.map(_.id.get))
  }
}
