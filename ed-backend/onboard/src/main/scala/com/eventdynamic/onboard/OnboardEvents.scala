package com.eventdynamic.onboard

import java.io.File
import java.sql.Timestamp
import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneId, ZonedDateTime}

import com.eventdynamic.models._
import com.eventdynamic.onboard.models.ServiceHolder
import com.eventdynamic.utils.{DateHelper, SuccessResponse}
import kantan.csv._
import kantan.csv.generic._
import kantan.csv.ops._
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Success

object OnboardEvents {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * The format of events.csv
    *
    * @param timestamp
    * @param season
    * @param name
    * @param primaryEventId
    * @param eventCategory
    */
  case class EventCSVRow(
    timestamp: String,
    season: String,
    name: String,
    primaryEventId: Option[Int],
    eventCategory: String
  )

  /** Create the season and events for that season for the client. Events read in from CSV
    * Events are grouped by season, then season is created or retrieved from db, then events for that
    * season are bulk uploaded into the events table.
    */
  def createSeasonsAndEvents(
    services: ServiceHolder,
    client: Client,
    venue: Venue,
    basePath: String
  ): Future[(Seq[Season], Seq[Event])] = {
    logger.info("Creating event categories, seasons, and events...")
    val data = new File(s"$basePath/${Onboard.EVENTS_FILE}")
      .asUnsafeCsvReader[EventCSVRow](rfc.withHeader)
      .toArray[EventCSVRow]

    for {
      seasons <- createSeasons(services, data, client.id.get)
      eventCategories <- createEventCategories(services, data, client.id.get)
      events <- createEvents(services, data, client.id.get, venue, seasons, eventCategories)
    } yield (seasons, events)
  }

  private def createEventCategories(
    services: ServiceHolder,
    data: Iterable[EventCSVRow],
    clientId: Int
  ): Future[Seq[EventCategory]] = {
    logger.info("Creating event categories...")
    val categories = data
      .map(_.eventCategory)
      .toSet[String]
      .map(name => EventCategory(None, name, clientId, DateHelper.now(), DateHelper.now()))

    for {
      _ <- services.eventCategoryService.bulkInsert(categories.toArray[EventCategory])
      records <- services.eventCategoryService.getAllForClient(clientId)
    } yield records
  }

  private def createSeasons(
    services: ServiceHolder,
    data: Iterable[EventCSVRow],
    clientId: Int
  ): Future[Seq[Season]] = {
    logger.info("Creating seasons...")

    for {
      _ <- Future.sequence(
        data
          .map(_.season)
          .toSet[String]
          .map(season => services.seasonService.create(Option(season), clientId, None, None))
      )
      seasons <- services.seasonService.getAll(Option(clientId), false)
    } yield seasons
  }

  private def createEvents(
    services: ServiceHolder,
    data: Iterable[EventCSVRow],
    clientId: Int,
    venue: Venue,
    seasons: Seq[Season],
    eventCategories: Seq[EventCategory]
  ): Future[Seq[Event]] = {
    logger.info("Creating events...")

    val events = data.map(r => {
      val seasonId = seasons.find(_.name.get == r.season).get.id
      val eventCategoryId = eventCategories.find(_.name == r.eventCategory).get.id.get

      // Parse the time in the venue's local time
      val timestamp = parseTimestamp(r.timestamp, venue.timeZone)

      Event(
        id = None,
        primaryEventId = r.primaryEventId,
        createdAt = DateHelper.now(),
        modifiedAt = DateHelper.now(),
        name = r.name,
        timestamp = Some(Timestamp.from(timestamp)),
        clientId = clientId,
        venueId = venue.id.get,
        eventCategoryId = eventCategoryId,
        seasonId = seasonId,
        isBroadcast = true,
        percentPriceModifier = 0,
        eventScore = None,
        eventScoreModifier = 0,
        spring = None,
        springModifier = 0
      )
    })

    for {
      _ <- services.eventService.bulkInsert(events.toArray[Event]).andThen {
        case Success(SuccessResponse(n)) => logger.info(s"Created $n events")
        case _                           => logger.error("Created no events")
      }
      savedEvents <- services.eventService.getAll(Some(clientId), false)
    } yield savedEvents
  }

  /**
    * Try to parse the time as an ISO string. If that doesn't work default to a local time
    * in the venue's time zone.
    *
    * @param str time string
    * @param zone zone string
    */
  private def parseTimestamp(str: String, zone: String): Instant = {
    try {
      Instant.parse(str)
    } catch {
      case _: Throwable =>
        val zoneId = ZoneId.of(zone)
        val format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(zoneId)
        ZonedDateTime.parse(str, format).toInstant
    }
  }
}
