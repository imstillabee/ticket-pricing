package com.eventdynamic.onboard

import java.time.Instant
import java.time.temporal.ChronoUnit

import com.eventdynamic.models.jobconfigs._
import com.eventdynamic.models.jobconfigs.ticketfulfillment.{
  SkyboxFulfillmentConfig,
  StubHubFulfillmentConfig,
  TDCFulfillmentConfig
}
import com.eventdynamic.models.{Client, JobConfig, JobType}
import com.eventdynamic.onboard.models.ClientIntegrationInfo
import com.eventdynamic.services.JobConfigService
import com.eventdynamic.utils.DateHelper
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object OnboardJobConfigs
    extends TicketFulfillmentConfigFormat
    with TicketSyncFormat
    with PricingFormat {

  private val logger = LoggerFactory.getLogger(this.getClass)

  /** Sets job configs for client
    *
    * @param client
    * @return
    */
  def createJobConfigs(
    client: Client,
    primaryIntegrationInfo: ClientIntegrationInfo,
    jobConfigService: JobConfigService
  ): Future[Seq[JobConfig]] = {
    val lastRunInstant = DateHelper.now.toInstant
    for {
      pricingJobConfig <- jobConfigService
        .createConfig(client.id.get, JobType.Pricing, PricingConfig(lastRunInstant))
        .andThen {
          case Success(_) => logger.info(s"Created pricing job config")
          case Failure(e) =>
            logger.error(s"Failed to create pricing job config", e)
        }
      ticketFulfillmentJobConfig <- jobConfigService
        .createConfig(
          client.id.get,
          JobType.TicketFulfillment,
          createTicketFulfillmentConfig(primaryIntegrationInfo, lastRunInstant)
        )
        .andThen {
          case Success(_) => logger.info(s"Created ticket fulfillment job config")
          case Failure(e) =>
            logger.error(s"Failed to create ticket fulfillment job config", e)
        }
      primarySyncJobConfig <- jobConfigService
        .createConfig(
          client.id.get,
          JobType.PrimarySync,
          TicketSyncConfig(lastRunInstant.minus(7, ChronoUnit.DAYS))
        )
        .andThen {
          case Success(_) => logger.info(s"Created primary sync job config")
          case Failure(e) =>
            logger.error(s"Failed to create primary sync job config", e)
        }
    } yield {
      Seq(pricingJobConfig, ticketFulfillmentJobConfig, primarySyncJobConfig)
    }
  }

  def createTicketFulfillmentConfig(
    primaryIntegrationInfo: ClientIntegrationInfo,
    instant: Instant
  ): TicketFulfillmentConfig = {
    primaryIntegrationInfo.name match {
      case "Tickets.com" if primaryIntegrationInfo.tdc.isDefined =>
        TicketFulfillmentConfig(
          Some(
            TDCFulfillmentConfig(
              primaryIntegrationInfo.tdc.get.paymentMethodId,
              primaryIntegrationInfo.tdc.get.deliveryMethodId,
              primaryIntegrationInfo.tdc.get.patronAccountId,
              primaryIntegrationInfo.tdc.get.defaultBuyerTypeId,
              instant
            )
          ),
          None,
          None
        )
      case "Skybox" =>
        TicketFulfillmentConfig(None, Some(SkyboxFulfillmentConfig(instant)), None)
      case "Stubhub" =>
        TicketFulfillmentConfig(None, None, Some(StubHubFulfillmentConfig(instant)))
      case _ => TicketFulfillmentConfig(None, None, None)
    }
  }
}
