package com.eventdynamic.onboard

import java.io.File

import com.eventdynamic.models.PriceScale
import com.eventdynamic.services.PriceScaleService
import com.eventdynamic.utils.DateHelper
import org.slf4j.LoggerFactory
import kantan.csv._
import kantan.csv.generic._
import kantan.csv.ops._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object OnboardPriceScales {

  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * The format of price-scales.csv
    *
    * @param integrationId
    * @param name
    */
  case class PriceScaleRow(integrationId: Int, name: String)

  /**
    * Streams price scales into a sequence
    *
    * @param path path of the price-scales.csv file
    * @return a sequence of price-scales
    */
  def readPriceScalesCSV(path: String, venueId: Int): Seq[PriceScale] = {
    val now = DateHelper.now()
    val reader = new File(path).asUnsafeCsvReader[PriceScaleRow](rfc.withHeader)

    reader
      .map(psr => PriceScale(None, psr.name, venueId, psr.integrationId, now, now))
      .toSeq
  }

  /** Create priceScales for the specified venue
    *
    * @param priceScaleService
    */
  def createPriceScales(
    priceScaleService: PriceScaleService,
    basePath: String,
    venueId: Int
  ): Future[Seq[PriceScale]] = {
    logger.info("Creating priceScales")
    val priceScales = readPriceScalesCSV(s"$basePath/${Onboard.PRICE_SCALES_FILE}", venueId)

    for {
      _ <- priceScaleService.bulkInsert(priceScales).andThen {
        case Success(Some(numberOfPriceScales)) =>
          logger.info(s"Bulk created $numberOfPriceScales priceScales")
        case _ =>
          logger.error("Exception on bulk inserting priceScales data")
      }
      savedPriceScales <- priceScaleService.getAllForVenue(venueId).andThen {
        case Success(ps) =>
          logger.info(s"Grabbed ${ps.length} price scales for venue from database")
        case Failure(e) => logger.error("Failed to grab price scales from the database", e)
      }
    } yield savedPriceScales
  }

}
