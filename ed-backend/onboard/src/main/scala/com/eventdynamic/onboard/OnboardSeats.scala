package com.eventdynamic.onboard
import com.eventdynamic.models.Seat
import com.eventdynamic.onboard.Onboard.{readCSV, toInt}
import com.eventdynamic.services.SeatService
import com.eventdynamic.utils.{DateHelper, SuccessResponse}
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

object OnboardSeats {

  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Parse a raw Seat Row from the seats csv into a seat object
    *
    * @param row
    * @return
    */
  private def parseSeatRow(row: Map[String, String], venueId: Int): Option[Seat] = {
    for {
      primarySeatId <- toInt(row.get("primarySeatId"))
      seat <- row.get("seat")
      rowNum <- row.get("row")
      section <- row.get("section")
    } yield {
      // TODO: make primarySeatId optional
      Seat(
        None,
        Some(primarySeatId),
        DateHelper.now(),
        DateHelper.now(),
        seat,
        rowNum,
        section,
        venueId
      )
    }
  }

  /** Create seats for the specified venue
    *
    * @param seatService
    */
  def createSeats(seatService: SeatService, basePath: String, venueId: Int): Future[Seq[Seat]] = {
    logger.info("Creating seats")
    val seats =
      readCSV(s"$basePath/${Onboard.SEATS_FILE}").flatMap(row => parseSeatRow(row, venueId))

    for {
      _ <- seatService.bulkInsert(seats).andThen {
        case Success(SuccessResponse(numberOfSeats)) =>
          logger.info(s"Bulk created $numberOfSeats seats")
        case _ =>
          logger.error("Created no seats")
      }
      savedSeats <- seatService.get(venueId = Some(venueId))
    } yield savedSeats
  }
}
