package com.eventdynamic.onboard

import java.io.File
import java.sql.Timestamp

import kantan.csv._
import kantan.csv.generic._
import kantan.csv.ops._
import com.eventdynamic.models._
import com.eventdynamic.onboard.models.ServiceHolder
import com.eventdynamic.utils.DateHelper
import org.slf4j.LoggerFactory
import scala.concurrent.duration.Duration
import scala.concurrent.Await

object OnboardTransactions {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Format of transactions.csv
    *
    * @param timestamp
    * @param integrationEventId
    * @param integrationSeatId
    * @param buyerTypeCode
    * @param price
    */
  case class TransactionRow(
    timestamp: String,
    integrationEventId: Int,
    integrationSeatId: Int,
    buyerTypeCode: String,
    price: BigDecimal,
    primaryTransactionId: Int,
    transactionType: Option[String]
  )

  /**
    * Streams transactions into an iterator
    *
    * @param path path of the transactions.csv file
    * @return an iterator of transactions
    */
  def readTransactionsCSV(path: String, integrationId: Int): Iterator[(Int, Int, Transaction)] = {
    val reader = new File(path).asCsvReader[TransactionRow](rfc.withHeader)

    reader.map {
      case Left(e) =>
        logger.warn(e.getMessage)
        None
      case Right(t) =>
        Some(
          (
            t.integrationEventId,
            t.integrationSeatId,
            Transaction(
              id = None,
              createdAt = DateHelper.now(),
              modifiedAt = DateHelper.now(),
              timestamp = Timestamp.valueOf(t.timestamp),
              price = t.price,
              eventSeatId = -1,
              integrationId = integrationId,
              buyerTypeCode = t.buyerTypeCode,
              transactionType = TransactionType.withName(t.transactionType.getOrElse("Purchase")),
              revenueCategoryId = None,
              primaryTransactionId = Some(t.primaryTransactionId.toString)
            )
          )
        )
    }.flatten
  }

  /** Create transactions
    *
    * @param services service holder
    * @param integrationId id of primary integration
    */
  def createTransactions(
    services: ServiceHolder,
    seats: Seq[Seat],
    events: Seq[Event],
    eventSeats: Seq[EventSeat],
    revenueCategoryMappings: Seq[RevenueCategoryMapping],
    integrationId: Int,
    clientId: Int,
    basePath: String
  ): Unit = {
    val CHUNK_SIZE = 1000

    val eventMap = events.map(e => (e.primaryEventId.get, e)).toMap
    val seatMap = seats.map(s => (s.primarySeatId.get, s)).toMap
    val eventSeatMap = eventSeats.map(es => ((es.eventId, es.seatId), es)).toMap

    logger.info("Reading and parsing transactions")
    val transactions = readTransactionsCSV(s"$basePath/${Onboard.TRANSACTIONS_FILE}", integrationId)

    logger.info(s"Saving transactions in groups of $CHUNK_SIZE")
    transactions
      .map(row => {
        val (primaryEventId, primarySeatId, transaction) = row
        val event = eventMap(primaryEventId)
        val seat = seatMap(primarySeatId)
        val eventSeat = eventSeatMap((event.id.get, seat.id.get))
        val revenueCategoryMapping = services.revenueCategoryMapping
          .findMatch(revenueCategoryMappings, transaction.buyerTypeCode)

        transaction
          .copy(
            eventSeatId = eventSeat.id.get,
            revenueCategoryId = revenueCategoryMapping.map(_.revenueCategoryId)
          )
      })
      .grouped(CHUNK_SIZE)
      .foreach(chunk => {
        val transactionsFuture = services.transactionService.bulkInsert(chunk)
        Await.result(transactionsFuture, Duration.Inf) match {
          case Some(n) => logger.info(s"Created $n transactions")
          case _       => logger.error("Created 0 transactions")
        }
      })

    logger.info("Done saving transactions")
  }
}
