package com.eventdynamic.onboard.models

import java.io.FileInputStream
import java.sql.Timestamp

import com.eventdynamic.models.ClientPerformanceType
import com.eventdynamic.models.ClientPerformanceType.ClientPerformanceType
import org.apache.commons.lang3.EnumUtils
import play.api.libs.json._
import play.api.libs.functional.syntax._

case class BasicInfo(
  client: ClientInfo,
  clientIntegrations: Seq[ClientIntegrationInfo],
  venue: VenueInfo,
  users: Seq[UserInfo],
  revenueCategories: Seq[RevenueCategoryInfo]
)

object BasicInfo {

  def fromFile(file: String): BasicInfo = {
    // Read in json from file
    val basicFile = new FileInputStream(file)
    val basicJson = try {
      Json.parse(basicFile)
    } finally {
      basicFile.close()
    }

    // Convert to object
    basicJson.validate[BasicInfo].get
  }

  implicit val basicInfoReads: Reads[BasicInfo] = (
    (__ \ "client").read[ClientInfo] and
      (__ \ "integrations").read[Seq[ClientIntegrationInfo]] and
      (__ \ "venue").read[VenueInfo] and
      (__ \ "users").read[Seq[UserInfo]] and
      (__ \ "buyerTypes").read[Seq[RevenueCategoryInfo]]
  )(BasicInfo.apply _)
}

case class ClientInfo(
  name: String,
  teamAbbreviation: String,
  eventScoreFloor: Double,
  eventScoreCeiling: Option[Double],
  springFloor: Double,
  springCeiling: Option[Double],
  performanceType: String,
  logoUrl: Option[String]
)

object ClientInfo {
  implicit lazy val clientInfoReads: Reads[ClientInfo] = Json.reads[ClientInfo]
}

case class VenueInfo(
  name: String,
  capacity: Int,
  zipcode: String,
  timeZone: Option[String],
  svgUrl: Option[String]
)

object VenueInfo {
  implicit lazy val venueInfoReads: Reads[VenueInfo] = Json.reads[VenueInfo]
}

case class UserInfo(email: String, first: String, last: String, password: String)

object UserInfo {
  implicit lazy val userInfoReads: Reads[UserInfo] = Json.reads[UserInfo]
}

case class RevenueCategoryInfo(name: String, regex: String, order: Int)

object RevenueCategoryInfo {
  implicit lazy val revenueCategoryInfoReads: Reads[RevenueCategoryInfo] =
    Json.reads[RevenueCategoryInfo]

}

case class ClientIntegrationInfo(
  name: String,
  isActive: Boolean,
  isPrimary: Boolean,
  tdc: Option[TDCIntegrationInfo]
)

object ClientIntegrationInfo {
  implicit lazy val clientIntegrationInfoReads: Reads[ClientIntegrationInfo] =
    Json.reads[ClientIntegrationInfo]
}

case class TDCIntegrationInfo(
  paymentMethodId: Int,
  deliveryMethodId: Int,
  patronAccountId: Int,
  defaultBuyerTypeId: Int
)

object TDCIntegrationInfo {
  implicit lazy val tdcIntegrationInfoReads: Reads[TDCIntegrationInfo] =
    Json.reads[TDCIntegrationInfo]
}

case class Row(
  name: String,
  timestamp: Option[Timestamp] = None,
  integrationId: Option[Int] = None,
  season: Option[String] = None
)
case class SeatsRow(id: Int, integrationId: Int, row: String, seat: String, section: String)
