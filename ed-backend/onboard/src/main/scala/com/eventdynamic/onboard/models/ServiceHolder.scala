package com.eventdynamic.onboard.models

import com.eventdynamic.db.EDContext
import com.eventdynamic.services.mlb.MySportsFeedInfoService
import com.eventdynamic.services.{MailerService, _}
import com.eventdynamic.transactions.SeasonTransaction

import scala.concurrent.ExecutionContext

/**
  * Convenience class for passing services around the onboarding script.  Using this allows us to mock out the
  * services in tests with ease.
  */
case class ServiceHolder(
  client: ClientService,
  clientIntegration: ClientIntegrationService,
  eventService: EventService,
  eventSeatService: EventSeatService,
  integration: IntegrationService,
  revenueCategory: RevenueCategoryService,
  revenueCategoryMapping: RevenueCategoryMappingService,
  seasonService: SeasonService,
  seatService: SeatService,
  teamAbbreviationService: MySportsFeedInfoService,
  user: UserService,
  venue: VenueService,
  transactionService: TransactionService,
  priceScale: PriceScaleService,
  eventCategoryService: EventCategoryService,
  secondaryPricingRulesService: SecondaryPricingRulesService,
  jobConfigService: JobConfigService
)

object ServiceHolder {

  def from(ctx: EDContext)(implicit ec: ExecutionContext): ServiceHolder = {
    val clientService = new ClientService(ctx)
    val eventSeatService = new EventSeatService(ctx)
    val integrationService = new IntegrationService(ctx)
    val secondaryPricingRulesService = new SecondaryPricingRulesService(ctx)
    val venueService = new VenueService(ctx)
    val eventService = new EventService(ctx)
    val transactionService = new TransactionService(ctx)
    val priceScaleService = new PriceScaleService(ctx)
    val seasonTransaction =
      new SeasonTransaction(ctx, eventService, eventSeatService, transactionService)
    val seasonService = new SeasonService(ctx, seasonTransaction)
    val seatService = new SeatService(ctx)
    val teamAbbreviationService = new MySportsFeedInfoService(ctx)
    val userService = new UserService(ctx)
    val clientIntegrationService = new ClientIntegrationService(ctx)
    val revenueCategoryService = new RevenueCategoryService(ctx)
    val revenueCategoryMappingService =
      new RevenueCategoryMappingService(ctx)
    val eventCategoryService = new EventCategoryService(ctx)
    val jobConfigService = new JobConfigService(ctx)

    ServiceHolder(
      clientService,
      clientIntegrationService,
      eventService,
      eventSeatService,
      integrationService,
      revenueCategoryService,
      revenueCategoryMappingService,
      seasonService,
      seatService,
      teamAbbreviationService,
      userService,
      venueService,
      transactionService,
      priceScaleService,
      eventCategoryService,
      secondaryPricingRulesService,
      jobConfigService
    )
  }
}
