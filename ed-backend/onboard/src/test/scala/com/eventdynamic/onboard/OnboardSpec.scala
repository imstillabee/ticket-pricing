package com.eventdynamic.onboard

import java.sql.Timestamp

import com.eventdynamic.db.EDContext
import com.eventdynamic.models.ClientPerformanceType.ClientPerformanceType
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models._
import com.eventdynamic.models.jobconfigs._
import com.eventdynamic.models.mlb.MySportsFeedInfo
import com.eventdynamic.onboard.models.{ClientInfo, ServiceHolder}
import com.eventdynamic.services._
import com.eventdynamic.services.mlb.MySportsFeedInfoService
import com.eventdynamic.utils.{DateHelper, DuplicateResponse, SuccessResponse}
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.Writes

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success, Try}

class OnboardSpec
    extends FlatSpec
    with Matchers
    with MockitoSugar
    with BeforeAndAfterAll
    with JobConfigFormats {
  import OnboardSpec._

  val fakeContext = mock[EDContext]

  val services = ServiceHolder(
    mock[ClientService],
    mock[ClientIntegrationService],
    mock[EventService],
    mock[EventSeatService],
    mock[IntegrationService],
    mock[RevenueCategoryService],
    mock[RevenueCategoryMappingService],
    mock[SeasonService],
    mock[SeatService],
    mock[MySportsFeedInfoService],
    mock[UserService],
    mock[VenueService],
    mock[TransactionService],
    mock[PriceScaleService],
    mock[EventCategoryService],
    mock[SecondaryPricingRulesService],
    mock[JobConfigService]
  )

  val clientDialexa =
    Client(
      Some(1),
      new Timestamp(0),
      new Timestamp(0),
      15,
      "Dialexa",
      Some("https://dialexa.com/logoUrl"),
      2,
      None,
      1.125,
      None,
      ClientPerformanceType.MLB
    )

  val venueDialexa =
    Venue(
      Some(1),
      new Timestamp(0),
      new Timestamp(0),
      "Dialexa Home",
      100,
      "75201",
      "America/New_York",
      Some("https://dialexa.com/venueMap")
    )

  val userRowdy = User(
    Some(1),
    new Timestamp(0),
    new Timestamp(0),
    "rowdy@dialexa.com",
    "Rowdy",
    "Howell",
    None,
    None,
    Option(new Timestamp(0)),
    false,
    None,
    clientDialexa.id.get
  )

  val userAdmin = User(
    Some(2),
    new Timestamp(0),
    new Timestamp(0),
    "admin@dialexa.com",
    "Admin",
    "Admin",
    None,
    None,
    Option(new Timestamp(0)),
    false,
    None,
    clientDialexa.id.get
  )

  val revenueCategory =
    RevenueCategory(Some(1), DateHelper.now, "Group Ticket", DateHelper.now, true)

  val revenueCategoryMapping = RevenueCategoryMapping(
    Some(1),
    clientDialexa.id.get,
    DateHelper.now,
    DateHelper.now,
    revenueCategory.id.get,
    ".*",
    1
  )

  val integration = Integration(Some(1), DateHelper.now, DateHelper.now, "Tickets.com", None)

  val clientIntegration = ClientIntegration(
    Some(1),
    DateHelper.now,
    DateHelper.now,
    clientDialexa.id.get,
    integration.id.get,
    true,
    true,
    None,
    None
  )

  val eventCategories = Seq(
    EventCategory(Some(1), "PREMIUM", 1, DateHelper.now, DateHelper.now),
    EventCategory(Some(2), "MARQUEE", 1, DateHelper.now, DateHelper.now),
    EventCategory(Some(3), "CLASSIC", 1, DateHelper.now, DateHelper.now),
    EventCategory(Some(4), "VALUE", 1, DateHelper.now, DateHelper.now)
  )

  val season = Season(Some(1), Some("2018"), 1, None, None)

  // These should match with events.csv
  val events = Seq(
    createEvent(9201),
    createEvent(9171),
    createEvent(9169),
    createEvent(9167),
    createEvent(9165),
    createEvent(9196),
    createEvent(9160),
    createEvent(9159),
    createEvent(9158),
    createEvent(9157)
  )

  val seats = Seq(
    createSeat(734051),
    createSeat(734053),
    createSeat(734058),
    createSeat(734063),
    createSeat(734065),
    createSeat(734070),
    createSeat(734076),
    createSeat(734090),
    createSeat(734104),
    createSeat(734107)
  )

  val priceScale = PriceScale(Some(1), "PriceScale Name", 1, 2295, DateHelper.now, DateHelper.now)

  val eventSeat =
    EventSeat(Some(1), 1, 1, 1, None, None, true, DateHelper.now, DateHelper.now, false)

  val teamAbbreviation =
    MySportsFeedInfo(Some(1), DateHelper.now, DateHelper.now, 1, "NYM")

  override def beforeAll() = {
    super.beforeAll()

    when(
      services.client.createUnique(
        "Dialexa",
        15,
        2,
        None,
        1.125,
        None,
        ClientPerformanceType.MLB,
        Some("https://eventdynamic.com/team_logo")
      )
    ).thenReturn(Future.successful(SuccessResponse(clientDialexa)))

    when(
      services.user
        .create("rowdy@dialexa.com", "Rowdy", "Howell", Some("RowdyED"), 1, None, false, 5760)
    ).thenReturn(Future.successful((userRowdy, "tempPass")))

    when(
      services.user
        .create("admin@dialexa.com", "Admin", "Admin", Some("adminpass"), 1, None, false, 5760)
    ).thenReturn(Future.successful((userAdmin, "tempPass")))

    when(
      services.venue.create(
        "Dialexa Home",
        100,
        "75201",
        Some("https://dialexa.com/venueMap"),
        "America/Chicago"
      )
    ).thenReturn(Future.successful(1))

    when(services.venue.getById(1)).thenReturn(
      Future.successful(
        Some(
          Venue(
            id = Some(1),
            createdAt = new Timestamp(0),
            modifiedAt = new Timestamp(0),
            name = "Dialexa Home",
            capacity = 100,
            zipCode = "75201",
            svgUrl = Some("https://dialexa.com/venueMap"),
            timeZone = "America/Chicago"
          )
        )
      )
    )

    when(services.revenueCategory.getOrCreate(any[String]))
      .thenReturn(Future.successful(revenueCategory))

    when(
      services.revenueCategoryMapping
        .create(any[Int], any[Int], any[String], any[Int])
    ).thenReturn(Future.successful(revenueCategoryMapping))

    when(
      services.clientIntegration
        .create(any[Int], any[Int], any[Boolean], any[Boolean])
    ).thenReturn(Future.successful(clientIntegration))

    when(services.integration.getByName(any[String]))
      .thenReturn(Future.successful(Some(integration)))

    when(
      services.seasonService
        .create(any[Option[String]], anyInt, any[Option[Timestamp]], any[Option[Timestamp]])
    ).thenReturn(Future.successful(SuccessResponse(season)))

    when(
      services.seasonService
        .getAll(any[Option[Int]], any[Boolean])
    ).thenReturn(Future.successful(Seq(season)))

    when(services.eventCategoryService.bulkInsert(any[Seq[EventCategory]]))
      .thenReturn(Future.successful(Some(eventCategories.size)))

    when(services.eventCategoryService.getAllForClient(any[Int]))
      .thenReturn(Future.successful(eventCategories))

    when(services.seasonService.resetDates(any[Season]))
      .thenReturn(Future.successful(SuccessResponse(season)))

    when(services.priceScale.bulkInsert(any[Seq[PriceScale]]))
      .thenReturn(Future.successful(Some(33)))

    when(services.priceScale.getAllForVenue(any[Int]))
      .thenReturn(Future.successful(Seq(priceScale)))

    when(services.priceScale.getByIntegrationId(any[Int]))
      .thenReturn(Future.successful(Some(priceScale)))

    when(services.eventService.bulkInsert(any[Seq[Event]]))
      .thenReturn(Future.successful(SuccessResponse(1)))
    when(services.eventService.getAll(any[Option[Int]], any[Boolean]))
      .thenReturn(Future.successful(events))

    when(services.seatService.bulkInsert(any[Seq[Seat]]))
      .thenReturn(Future.successful(SuccessResponse(1)))
    when(
      services.seatService
        .get(any[Option[String]], any[Option[String]], any[Option[String]], any[Option[Int]])
    ).thenReturn(Future.successful(seats))

    when(services.eventSeatService.bulkInsert(any[Seq[EventSeat]]))
      .thenReturn(Future.successful(Some(1)))
    when(services.eventSeatService.getForEvents(any[Seq[Int]]))
      .thenReturn(Future.successful(Seq(eventSeat)))

    when(services.transactionService.bulkInsert(any[Seq[Transaction]]))
      .thenReturn(Future.successful(Some(1)))

    when(services.revenueCategoryMapping.findMatch(any[Seq[RevenueCategoryMapping]], any[String]))
      .thenCallRealMethod()

    when(services.teamAbbreviationService.create(any[String], any[Int]))
      .thenReturn(Future.successful(SuccessResponse(teamAbbreviation)))

    when(
      services.jobConfigService
        .createConfig[TicketFulfillmentConfig](
          any[Int],
          any[JobType],
          any[TicketFulfillmentConfig]
        )(any[Writes[TicketFulfillmentConfig]])
    ).thenReturn(Future.successful(JobConfig(Some(1), DateHelper.now, DateHelper.now, 1, 1, "{}")))

    when(
      services.jobConfigService
        .createConfig[TicketSyncConfig](any[Int], any[JobType], any[TicketSyncConfig])(
          any[Writes[TicketSyncConfig]]
        )
    ).thenReturn(Future.successful(JobConfig(Some(2), DateHelper.now, DateHelper.now, 2, 2, "{}")))

    when(
      services.jobConfigService
        .createConfig[PricingConfig](any[Int], any[JobType], any[PricingConfig])(
          any[Writes[PricingConfig]]
        )
    ).thenReturn(Future.successful(JobConfig(Some(3), DateHelper.now, DateHelper.now, 3, 3, "{}")))

    Onboard.process(fakeContext, services, "onboard/example-data")
  }

  /**
    * Integration Tests
    */
  "Onboard#process" should "call the client service" in {
    verify(services.client, times(1)).createUnique(
      "Dialexa",
      15,
      2,
      None,
      1.125,
      None,
      ClientPerformanceType.MLB,
      Some("https://eventdynamic.com/team_logo")
    )
  }

  it should "call the user service" in {
    verify(services.user, times(1)).create(
      "rowdy@dialexa.com",
      "Rowdy",
      "Howell",
      Some("RowdyED"),
      1,
      None,
      false,
      5760
    )
    verify(services.user, times(1)).create(
      "admin@dialexa.com",
      "Admin",
      "Admin",
      Some("adminpass"),
      1,
      None,
      false,
      5760
    )
  }

  it should "call the venue service" in {
    verify(services.venue, times(1)).create(
      "Dialexa Home",
      100,
      "75201",
      Some("https://dialexa.com/venueMap"),
      "America/Chicago"
    )
  }

  it should "call the revenue category service" in {
    verify(services.revenueCategory, times(1)).getOrCreate("Group")
    verify(services.revenueCategory, times(1)).getOrCreate("Package 20")
    verify(services.revenueCategory, times(1)).getOrCreate("Package 40")
    verify(services.revenueCategory, times(1)).getOrCreate("Package")
    verify(services.revenueCategory, times(1)).getOrCreate("Single Game")
  }

  it should "call the revenue category mapping service" in {
    verify(services.revenueCategoryMapping, times(1)).create(
      clientDialexa.id.get,
      revenueCategory.id.get,
      "^G.*",
      1
    )
  }

  it should "call the client integration service" in {
    verify(services.clientIntegration, times(1)).create(
      clientDialexa.id.get,
      integration.id.get,
      true,
      true
    )
    verify(services.clientIntegration, times(1)).create(
      clientDialexa.id.get,
      integration.id.get,
      true,
      false
    )
  }

  it should "call the integration service" in {
    verify(services.integration, times(2)).getByName("Tickets.com")
  }

  it should "call the season service" in {
    verify(services.seasonService, times(1)).create(
      any[Option[String]],
      anyInt,
      any[Option[Timestamp]],
      any[Option[Timestamp]]
    )
  }

  it should "call the seasons reset dates service" in {
    verify(services.seasonService, times(1)).resetDates(any[Season])
  }

  it should "call the priceScale service" in {
    verify(services.priceScale, times(1)).bulkInsert(any[Seq[PriceScale]])
  }

  it should "call the event service" in {
    verify(services.eventService, times(1)).bulkInsert(any[Seq[Event]])
  }

  it should "call the seat service" in {
    verify(services.seatService, times(1)).bulkInsert(any[Seq[Seat]])
  }

  it should "call the team abbreviation service" in {
    verify(services.teamAbbreviationService, times(1))
      .create(any[String], any[Int])
  }

  it should "call the job config service" in {
    // The mocking library does not distinguish between the different generic types
    // so we only verify on one job config type
    // The 3 number of invocations represents the job configs created for each of the job types
    verify(services.jobConfigService, times(3))
      .createConfig[PricingConfig](any[Int], any[JobType], any[PricingConfig])(
        any[Writes[PricingConfig]]
      )
  }

  /**
    * Unit Tests
    */
  "Onboard#createClient" should "return none on duplicate client" in {
    val mockClientService = mock[ClientService]

    when(
      mockClientService
        .createUnique(
          "Dupe Client",
          15,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB,
          Some("https://eventdynamic.com/team_logo")
        )
    ).thenReturn(Future.successful(DuplicateResponse))

    val clientFuture =
      Onboard
        .createClient(
          mockClientService,
          ClientInfo(
            "Dupe Client",
            "NYM",
            2,
            None,
            1.125,
            None,
            "MLB",
            Some("https://eventdynamic.com/team_logo")
          )
        )
        .map(Some(_))
        .recover {
          case _ => None
        }

    val clientOpt = Await.result(clientFuture, Duration.Inf)

    verify(mockClientService, times(1)).createUnique(
      "Dupe Client",
      15,
      2,
      None,
      1.125,
      None,
      ClientPerformanceType.MLB,
      Some("https://eventdynamic.com/team_logo")
    )
    clientOpt should equal(None)
  }

  "Onboard#createClient" should "throw an error on invalid client peformance type" in {
    val mockClientService = mock[ClientService]

    val clientTry = Try(
      Onboard
        .createClient(
          mockClientService,
          ClientInfo(
            "Dupe Client",
            "NYM",
            2,
            None,
            1.125,
            None,
            "MMMMMLB",
            Some("https://eventdynamic.com/team_logo")
          )
        )
    )

    clientTry match {
      case Success(_) => fail()
      case Failure(e) => {
        e.getMessage should equal("Invalid client performance type: MMMMMLB")
        verify(mockClientService, times(0)).createUnique(
          any[String],
          any[Int],
          any[Double],
          any[Option[Double]],
          any[Double],
          any[Option[Double]],
          any[ClientPerformanceType],
          any[Option[String]]
        )
      }
    }
  }
}

object OnboardSpec {

  def createEvent(primaryEventId: Int): Event = Event(
    id = Some(1),
    primaryEventId = Some(primaryEventId),
    createdAt = DateHelper.now,
    modifiedAt = DateHelper.now,
    name = "name",
    timestamp = None,
    clientId = 1,
    venueId = 1,
    eventCategoryId = 1,
    seasonId = Some(1),
    isBroadcast = true,
    percentPriceModifier = 0,
    eventScore = None,
    eventScoreModifier = 0,
    spring = None,
    springModifier = 0
  )

  def createSeat(primarySeatId: Int): Seat =
    Seat(
      Some(1),
      Some(primarySeatId),
      DateHelper.now(),
      DateHelper.now(),
      "seat",
      "row",
      "section",
      1
    )
}
