# Projection Sync

_Projection sync is a job to fetch inventory and revenue projections from the model server and save them in the
EventDynamic database for use in the platform._

## Quick Start
1. Onboard client(s)

1. Start a model server by...
    1. Running the model server projection localy _OR_
    1. Pointing `ED_MODEL_SERVER_URL` to deployed model server like QA

1. Set the `ED_PROJECTION_SYNC_CLIENT_IDS` environment variable for the client(s) you want to sync

1. Run the script using sbt: `sbt projectionSync/run`

1. Check for the ingested data in the _TimeStatsProjections_ table

## Configuration
### Environment Variables

| Name | Description |
| :--- | :--- |
| `ED_PROJECTION_SYNC_CLIENT_IDS` | which client(s) to sync |
| `ED_MODEL_SERVER_URL` | location of the model server |

## Service Dependencies
* Model Server | [github](https://github.com/dialexa/ed-model-server)

## Troubleshooting
_TODO_