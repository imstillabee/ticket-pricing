package com.eventdynamic.batch.projectionsync

import com.google.inject.Guice

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object ProjectionSync {

  def main(args: Array[String]): Unit = {
    val injector = Guice.createInjector(new ProjectionSyncModule)

    import net.codingwell.scalaguice.InjectorExtensions._
    val projectionSyncer = injector.instance[ProjectionSyncer]

    Await.result(projectionSyncer.sync(), Duration.Inf)
  }
}
