package com.eventdynamic.batch.projectionsync

import com.eventdynamic.modelserver.ModelServerService
import com.google.inject.{AbstractModule, Provides}
import com.typesafe.config.{Config, ConfigFactory}
import net.codingwell.scalaguice.ScalaModule

import scala.concurrent.ExecutionContext

class ProjectionSyncModule extends AbstractModule with ScalaModule {
  private val config = ConfigFactory.load()

  override def configure(): Unit = {
    bind[Config].toInstance(config)
  }

  @Provides
  def executionContext(): ExecutionContext = {
    ExecutionContext.global
  }

  @Provides
  def modelServerService(): ModelServerService = {
    val baseUrl = config.getString("services.modelServer.baseUrl")
    new ModelServerService(baseUrl)
  }
}
