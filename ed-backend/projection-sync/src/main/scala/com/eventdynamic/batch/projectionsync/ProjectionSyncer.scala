package com.eventdynamic.batch.projectionsync

import java.sql.Timestamp
import java.time.{Instant, ZoneId}
import java.time.temporal.ChronoUnit

import com.eventdynamic.models.{Event, Projection, Venue}
import com.eventdynamic.modelserver.ModelServerService
import com.eventdynamic.modelserver.models.{
  ModelServerProjectionContext,
  ModelServerProjectionExample,
  ModelServerProjectionPayload
}
import com.eventdynamic.services.{EventService, ProjectionService, VenueService}
import com.eventdynamic.transactions.{InventoryStatsTransaction, TimeStatsTransaction}
import com.eventdynamic.utils.FutureUtil
import com.google.inject.Inject
import com.typesafe.config.Config
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

/**
  * Core logic for syncing projections from the model server to Event Dynamic
  */
class ProjectionSyncer @Inject()(
  config: Config,
  eventService: EventService,
  inventoryStatsTransaction: InventoryStatsTransaction,
  modelServerService: ModelServerService,
  projectionService: ProjectionService,
  timeStatsTransaction: TimeStatsTransaction,
  venueService: VenueService
)(implicit ec: ExecutionContext) {
  private val logger = LoggerFactory.getLogger(this.getClass)
  private val clientIds = config.getString("projectionSync.clientIds").split(",").map(_.toInt)

  def sync(): Future[Unit.type] = {
    val now = Instant.now()

    for {
      events <- FutureUtil
        .serializeFutures(clientIds)(
          clientId =>
            eventService.getUpcomingEvents(clientIds = Seq(clientId), after = Timestamp.from(now))
        )
        .map(_.flatten)
        .andThen {
          case Success(e) => logger.info(s"Syncing projections for ${e.length} events")
        }

      venueIds = events.map(_.venueId).distinct
      venues <- FutureUtil.serializeFutures(venueIds)(venueService.getById).map(_.flatten)

      _ <- FutureUtil.serializeFutures(events)(event => {
        val venue = venues.find(_.id.get == event.venueId).get
        syncEventProjections(now, event, venue)
      })

    } yield {
      logger.info(s"Done syncing projections")
      Unit
    }
  }

  private def syncEventProjections(now: Instant, event: Event, venue: Venue): Future[Int] = {
    logger.info(s"Syncing ${printEvent(event)}")
    // Look at historic sales for the last 8 days
    val zoneId = ZoneId.of(venue.timeZone)
    val end = now.atZone(zoneId).truncatedTo(ChronoUnit.DAYS)
    val start = end.minus(8, ChronoUnit.DAYS)

    for {
      // Fetch inventory
      eventInventory <- inventoryStatsTransaction
        .getInventory(clientId = event.clientId, eventId = event.id)
        .map(_.map(_.total))
        .map {
          case Some(inventory) => inventory
          case None            => throw new Exception("Inventory does not exist")
        }

      // Fetch time stats
      timeStats <- timeStatsTransaction.getActual(
        start = start,
        end = end,
        zoneId = zoneId,
        interval = ChronoUnit.DAYS,
        clientId = event.clientId,
        eventId = event.id,
        seasonId = None
      )

      // Fetch projections using time stats and event data
      payload = ModelServerProjectionPayload(
        context = ModelServerProjectionContext(
          event_date = event.timestamp.get.toInstant,
          total_inventory = eventInventory
        ),
        examples = timeStats.map(stat => {
          ModelServerProjectionExample(
            timestamp = stat.intervalEnd.toInstant,
            cumulative_inventory = stat.cumulativeInventory.getOrElse(0),
            cumulative_revenue = stat.cumulativeRevenue.getOrElse(BigDecimal(0))
          )
        })
      )
      modelServerProjections <- modelServerService.getProjections(event.clientId.toString, payload)

      // Update projections store in Event Dynamic
      projections = modelServerProjections.map(
        p =>
          Projection(
            eventId = event.id.get,
            timestamp = p.timestamp,
            inventory = p.cumulative_inventory,
            inventoryLowerBound = p.cumulative_inventory_lb,
            inventoryUpperBound = p.cumulative_inventory_ub,
            revenue = p.cumulative_revenue,
            revenueLowerBound = p.cumulative_revenue_lb,
            revenueUpperBound = p.cumulative_revenue_ub
        )
      )
      nProjections <- projectionService.bulkCleanAndInsert(projections)
    } yield {
      logger.info(s"Done syncing $nProjections for ${printEvent(event)}")
      nProjections
    }
  }

  private def printEvent(event: Event): String = {
    s"${event.name} -- ${event.timestamp.get.toString} [${event.id.get}]"
  }
}
