package com.eventdynamic.batch.projectionsync

import java.sql.Timestamp
import java.time.temporal.ChronoUnit
import java.time.{Instant, ZoneId, ZonedDateTime}

import com.eventdynamic.models.Projection
import com.eventdynamic.modelserver.ModelServerService
import com.eventdynamic.modelserver.models.{ModelServerProjection, ModelServerProjectionPayload}
import com.eventdynamic.services.{EventService, ProjectionService, VenueService}
import com.eventdynamic.transactions.{
  InventoryStats,
  InventoryStatsTransaction,
  TimeStatsTransaction
}
import com.eventdynamic.utils.RandomModelUtil
import com.typesafe.config.Config
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.AsyncWordSpec
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future

class ProjectionSyncerSpec extends AsyncWordSpec with MockitoSugar {
  // Generators
  private def projections(n: Int = 10): Seq[ModelServerProjection] = {
    val start = Instant.parse("2019-01-01T00:00:00Z")
    (1 to n).map(idx => {
      ModelServerProjection(
        timestamp = start.plus(idx, ChronoUnit.DAYS),
        cumulative_inventory = RandomModelUtil.int(),
        cumulative_inventory_ub = RandomModelUtil.int(),
        cumulative_inventory_lb = RandomModelUtil.int(),
        cumulative_revenue = BigDecimal(RandomModelUtil.double()),
        cumulative_revenue_ub = BigDecimal(RandomModelUtil.double()),
        cumulative_revenue_lb = BigDecimal(RandomModelUtil.double())
      )
    })
  }

  "ProjectionSyncer#sync" should {
    // Constants
    val clientIds = List(1, 2)
    val venueId = RandomModelUtil.int()
    val nEvents = 5

    "sync projections" in {
      val config = mock[Config]
      when(config.getString("projectionSync.clientIds")).thenReturn(clientIds.mkString(","))

      val modelServerService = mock[ModelServerService]
      when(modelServerService.getProjections(any[String], any[ModelServerProjectionPayload]))
        .thenAnswer(_ => Future.successful(projections(100)))

      val eventService = mock[EventService]
      when(eventService.getUpcomingEvents(any[Seq[Int]], any[Timestamp], any[Option[Timestamp]]))
        .thenAnswer(
          _ =>
            Future.successful(
              (1 to nEvents).map(
                _ =>
                  RandomModelUtil.event(
                    venueId = venueId,
                    timestamp = Some(Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS)))
                )
              )
          )
        )

      val timeStatsTransaction = mock[TimeStatsTransaction]
      when(
        timeStatsTransaction.getActual(
          any[ZonedDateTime],
          any[ZonedDateTime],
          any[ZoneId],
          any[ChronoUnit],
          any[Int],
          any[Option[Int]],
          any[Option[Int]]
        )
      ).thenAnswer(_ => {
        val start = Instant.parse("2019-01-01T00:00:00Z")
        Future.successful((1 to 10).map(idx => {
          RandomModelUtil.timeStat(
            intervalStart = Timestamp.from(start.plus(idx - 1, ChronoUnit.DAYS)),
            intervalEnd = Timestamp.from(start.plus(idx, ChronoUnit.DAYS))
          )
        }))
      })

      val inventoryStatsTransaction = mock[InventoryStatsTransaction]
      when(inventoryStatsTransaction.getInventory(any[Int], any[Option[Int]], any[Option[Int]]))
        .thenReturn(
          Future.successful(
            Some(
              InventoryStats(
                eventId = None,
                seasonId = None,
                total = 123,
                soldIndividual = 123,
                soldOther = 123,
                revenue = BigDecimal(19.95),
                unsold = 123
              )
            )
          )
        )

      val projectionService = mock[ProjectionService]
      when(projectionService.bulkCleanAndInsert(any[Seq[Projection]])).thenAnswer(req => {
        val arg = req.getArgumentAt(0, classOf[Seq[Projection]])
        Future.successful(arg.length)
      })

      val venueService = mock[VenueService]
      when(venueService.getById(any[Int]))
        .thenReturn(Future.successful(Some(RandomModelUtil.venue(id = Some(venueId)))))

      val projectionSyncer =
        new ProjectionSyncer(
          config,
          eventService,
          inventoryStatsTransaction,
          modelServerService,
          projectionService,
          timeStatsTransaction,
          venueService
        )

      projectionSyncer
        .sync()
        .map(res => {
          verify(eventService, times(clientIds.length))
            .getUpcomingEvents(any[Seq[Int]], any[Timestamp], any[Option[Timestamp]])

          verify(timeStatsTransaction, times(clientIds.length * nEvents))
            .getActual(
              any[ZonedDateTime],
              any[ZonedDateTime],
              any[ZoneId],
              any[ChronoUnit],
              any[Int],
              any[Option[Int]],
              any[Option[Int]]
            )

          verify(modelServerService, times(clientIds.length * nEvents))
            .getProjections(any[String], any[ModelServerProjectionPayload])

          assert(res == Unit)
        })
    }
  }
}
