package com.eventdynamic

import java.sql.Timestamp
import java.time.Instant

import com.eventdynamic.db.EDPostgresProfile
import com.eventdynamic.models.ClientPerformanceType.ClientPerformanceType
import com.eventdynamic.models.JobPriorityType.JobPriorityType
import com.eventdynamic.models.JobTriggerType.JobTriggerType
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models.RoundingType.RoundingType
import com.eventdynamic.models.ScheduledJobStatus.ScheduledJobStatus
import com.eventdynamic.models.TransactionType.TransactionType
import com.eventdynamic.models._
import com.eventdynamic.models.mlb.MySportsFeedInfo
import slick.ast.BaseTypedType
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcType
import slick.lifted.ForeignKeyQuery

object Tables extends {
  val databaseConfig = DatabaseConfig.forConfig[EDPostgresProfile]("services.database")
  val profile = databaseConfig.profile
} with Tables

trait Tables {
  val profile: EDPostgresProfile
  import profile.api._

  /**
    * ClientIntegrationsEventSeats
    */
  class ClientIntegrationEventSeats(tag: Tag)
      extends Table[ClientIntegrationEventSeat](tag, "ClientIntegrationEventSeats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def clientIntegrationListingId = column[Int]("clientIntegrationListingId")
    def eventSeatId = column[Int]("eventSeatId")

    def * =
      (id.?, createdAt, modifiedAt, clientIntegrationListingId, eventSeatId) <> (ClientIntegrationEventSeat.tupled, ClientIntegrationEventSeat.unapply)
  }
  lazy val ClientIntegrationEventSeats = TableQuery[ClientIntegrationEventSeats]

  /**
    * ClientIntegrationEvents
    */
  class ClientIntegrationEvents(tag: Tag)
      extends Table[ClientIntegrationEvent](tag, "ClientIntegrationEvents") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def clientIntegrationId = column[Int]("clientIntegrationId")
    def eventId = column[Int]("eventId")
    def integrationEventId = column[String]("integrationEventId")

    def * =
      (id.?, createdAt, clientIntegrationId, eventId, integrationEventId, modifiedAt) <> (ClientIntegrationEvent.tupled, ClientIntegrationEvent.unapply)
  }
  lazy val ClientIntegrationEvents = TableQuery[ClientIntegrationEvents]

  /**
    * ClientIntegrationListings
    */
  class ClientIntegrationListings(tag: Tag)
      extends Table[ClientIntegrationListing](tag, "ClientIntegrationListings") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def clientIntegrationEventsId = column[Int]("clientIntegrationEventsId")
    def listingId = column[String]("listingId")

    def * =
      (id.?, createdAt, modifiedAt, clientIntegrationEventsId, listingId) <> (ClientIntegrationListing.tupled, ClientIntegrationListing.unapply)
  }
  lazy val ClientIntegrationListings = TableQuery[ClientIntegrationListings]

  /**
    * ClientIntegrations
    */
  class ClientIntegrations(tag: Tag) extends Table[ClientIntegration](tag, "ClientIntegrations") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def clientId = column[Int]("clientId")
    def integrationId = column[Int]("integrationId")
    def isActive = column[Boolean]("isActive")
    def isPrimary = column[Boolean]("isPrimary")
    def version = column[String]("version")
    def json = column[String]("json")

    def * =
      (id.?, createdAt, modifiedAt, clientId, integrationId, isActive, isPrimary, version.?, json.?) <> (ClientIntegration.tupled, ClientIntegration.unapply)

    def integration = foreignKey("fk_integration_id", integrationId, Integrations)(_.id)
  }
  lazy val ClientIntegrations = TableQuery[ClientIntegrations]

  /**
    * Clients
    */
  class Clients(tag: Tag) extends Table[Client](tag, "Clients") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def pricingInterval = column[Int]("pricingInterval")
    def name = column[String]("name")
    def logoUrl = column[String]("logoUrl")
    def eventScoreFloor = column[Double]("eventScoreFloor")
    def eventScoreCeiling = column[Option[Double]]("eventScoreCeiling")
    def springFloor = column[Double]("springFloor")
    def springCeiling = column[Option[Double]]("springCeiling")
    def performanceType = column[ClientPerformanceType]("performanceType")

    def * =
      (
        id.?,
        createdAt,
        modifiedAt,
        pricingInterval,
        name,
        logoUrl.?,
        eventScoreFloor,
        eventScoreCeiling,
        springFloor,
        springCeiling,
        performanceType
      ) <> (Client.tupled, Client.unapply)
  }
  lazy val Clients = TableQuery[Clients]

  /**
    * EventCategories
    */
  class EventCategories(tag: Tag) extends Table[EventCategory](tag, "EventCategories") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def clientId = column[Int]("clientId")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def createdAt = column[Timestamp]("createdAt")

    def * =
      (id.?, name, clientId, modifiedAt, createdAt) <> (EventCategory.tupled, EventCategory.unapply)
  }
  lazy val EventCategories = TableQuery[EventCategories]

  /**
    * EventSeats
    */
  class EventSeats(tag: Tag) extends Table[EventSeat](tag, "EventSeats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def seatId = column[Int]("seatId")
    def eventId = column[Int]("eventId")
    def priceScaleId = column[Int]("priceScaleId")
    def listedPrice = column[BigDecimal]("listedPrice")
    def overridePrice = column[Option[BigDecimal]]("overridePrice")
    def isListed = column[Boolean]("isListed")
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def isHeld = column[Boolean]("isHeld")

    def * =
      (
        id.?,
        seatId,
        eventId,
        priceScaleId,
        listedPrice.?,
        overridePrice,
        isListed,
        createdAt,
        modifiedAt,
        isHeld
      ) <> (EventSeat.tupled, EventSeat.unapply)
  }
  lazy val EventSeats = TableQuery[EventSeats]

  /**
    * Events
    */
  class Events(tag: Tag) extends Table[Event](tag, "Events") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def primaryEventId = column[Int]("primaryEventId")
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def name = column[String]("name")
    def timestamp = column[Timestamp]("timestamp")
    def clientId = column[Int]("clientId")
    def venueId = column[Int]("venueId")
    def eventCategoryId = column[Int]("eventCategoryId")
    def seasonId = column[Int]("seasonId")
    def isBroadcast = column[Boolean]("isBroadcast")
    def percentPriceModifier = column[Int]("percentPriceModifier")
    def eventScore = column[Double]("eventScore")
    def eventScoreModifier = column[Double]("eventScoreModifier")
    def spring = column[Double]("spring")
    def springModifier = column[Double]("springModifier")

    def * = (
      id.?,
      primaryEventId.?,
      createdAt,
      modifiedAt,
      name,
      timestamp.?,
      clientId,
      venueId,
      eventCategoryId,
      seasonId.?,
      isBroadcast,
      percentPriceModifier,
      eventScore.?,
      eventScoreModifier,
      spring.?,
      springModifier
    ) <> (Event.tupled, Event.unapply)
  }
  lazy val Events = TableQuery[Events]

  /**
    * Integrations
    */
  class Integrations(tag: Tag) extends Table[Integration](tag, "Integrations") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def name = column[String]("name")
    def logoUrl = column[String]("logoUrl")

    def * =
      (id.?, createdAt, modifiedAt, name, logoUrl.?) <> (Integration.tupled, Integration.unapply)
  }
  lazy val Integrations = TableQuery[Integrations]

  /**
    * Jobs
    */
  class Jobs(tag: Tag) extends Table[Job](tag, "Jobs") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def jobType = column[JobType]("type")

    def * =
      (id.?, jobType, createdAt, modifiedAt) <> (Job.tupled, Job.unapply)
  }
  lazy val Jobs = TableQuery[Jobs]

  /**
    * JobConfigs
    */
  class JobConfigs(tag: Tag) extends Table[JobConfig](tag, "JobConfigs") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def clientId = column[Int]("clientId")
    def jobId = column[Int]("jobId")
    def json = column[String]("json")

    def job: ForeignKeyQuery[Jobs, Job] =
      foreignKey("job_fk", jobId, TableQuery[Jobs])(_.id)

    def * =
      (id.?, createdAt, modifiedAt, clientId, jobId, json) <> (JobConfig.tupled, JobConfig.unapply)
  }
  lazy val JobConfigs = TableQuery[JobConfigs]

  /**
    * JobTriggers
    */
  class JobTriggers(tag: Tag) extends Table[JobTrigger](tag, "JobTriggers") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def triggerType = column[JobTriggerType]("type")
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")

    def * =
      (id.?, triggerType, createdAt, modifiedAt) <> (JobTrigger.tupled, JobTrigger.unapply)
  }
  lazy val JobTriggers = TableQuery[JobTriggers]

  class JobPriorities(tag: Tag) extends Table[JobPriority](tag, "JobPriorities") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[JobPriorityType]("name")
    def priority = column[Int]("priority")
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")

    def * =
      (id.?, name, priority, createdAt, modifiedAt) <> (JobPriority.tupled, JobPriority.unapply)
  }
  lazy val JobPriorities = TableQuery[JobPriorities]

  /**
    * MLBAMScheduleToEventMap
    */
  class MLBAMScheduleToEventMap(tag: Tag)
      extends Table[MLBAMScheduleToEvent](tag, "MLBAMScheduleToEvent") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def eventId = column[Int]("eventId")
    def scheduleId = column[Int]("scheduleId")

    def * =
      (id.?, eventId, scheduleId) <> (MLBAMScheduleToEvent.tupled, MLBAMScheduleToEvent.unapply)
  }
  lazy val MLBAMScheduleToEventMap = TableQuery[MLBAMScheduleToEventMap]

  /**
    * MLBAMSectionToPriceScaleMap
    */
  class MLBAMSectionToPriceScaleMap(tag: Tag)
      extends Table[MLBAMSectionToPriceScale](tag, "MLBAMSectionToPriceScale") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def priceScaleId = column[Int]("priceScaleId")
    def sectionId = column[Int]("sectionId")

    def * =
      (id.?, priceScaleId, sectionId) <> (MLBAMSectionToPriceScale.tupled, MLBAMSectionToPriceScale.unapply)
  }
  lazy val MLBAMSectionToPriceScaleMap = TableQuery[MLBAMSectionToPriceScaleMap]

  /**
    * MLBAMTeamToClientMap
    */
  class MLABAMTeamToClientMap(tag: Tag) extends Table[MLBAMTeamToClient](tag, "MLBAMTeamToClient") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def clientId = column[Int]("clientId")
    def teamId = column[Int]("teamId")

    def * = (id.?, clientId, teamId) <> (MLBAMTeamToClient.tupled, MLBAMTeamToClient.unapply)
  }
  lazy val MLBAMTeamToClientMap = TableQuery[MLABAMTeamToClientMap]

  /**
    * MLSSeasonStats
    */
  class MLSSeasonStats(tag: Tag) extends Table[MLSSeasonStat](tag, "MLSSeasonStats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def seasonId = column[Int]("seasonId")
    def wins = column[Int]("wins")
    def losses = column[Int]("losses")
    def ties = column[Int]("ties")
    def gamesTotal = column[Int]("gamesTotal")

    def * =
      (id.?, createdAt, modifiedAt, seasonId, wins, losses, ties, gamesTotal) <> (MLSSeasonStat.tupled, MLSSeasonStat.unapply)
  }
  lazy val MLSSeasonStats = TableQuery[MLSSeasonStats]

  /**
    * MLSEventStats
    */
  class MLSEventStats(tag: Tag) extends Table[MLSEventStat](tag, "MLSEventStats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def eventId = column[Int]("eventId")
    def opponent = column[String]("opponent")
    def homeGameNumber = column[Int]("homeGameNumber")

    def * =
      (id.?, createdAt, modifiedAt, eventId, opponent, homeGameNumber) <> (MLSEventStat.tupled, MLSEventStat.unapply)
  }
  lazy val MLSEventStats = TableQuery[MLSEventStats]

  /**
    * NFLSeasonStats
    */
  class NFLSeasonStats(tag: Tag) extends Table[NFLSeasonStat](tag, "NFLSeasonStats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def clientId = column[Int]("clientId")
    def seasonId = column[Int]("seasonId")
    def wins = column[Int]("wins")
    def losses = column[Int]("losses")
    def ties = column[Int]("ties")
    def gamesTotal = column[Int]("gamesTotal")

    def * =
      (id.?, createdAt, modifiedAt, clientId, seasonId, wins, losses, ties, gamesTotal) <> (NFLSeasonStat.tupled, NFLSeasonStat.unapply)
  }
  lazy val NFLSeasonStats = TableQuery[NFLSeasonStats]

  /**
    * NFLEventStats
    */
  class NFLEventStats(tag: Tag) extends Table[NFLEventStat](tag, "NFLEventStats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def clientId = column[Int]("clientId")
    def eventId = column[Int]("eventId")
    def opponent = column[String]("opponent")
    def isHomeOpener = column[Boolean]("isHomeOpener")
    def isPreSeason = column[Boolean]("isPreSeason")

    // (NFLEventStat.apply _).tupled is a workaround for a known issue (http://queirozf.com/entries/slick-error-message-value-tupled-is-not-a-member-of-object)
    def * =
      (id.?, createdAt, modifiedAt, clientId, eventId, opponent, isHomeOpener, isPreSeason) <> ((NFLEventStat.apply _).tupled, NFLEventStat.unapply)
  }
  lazy val NFLEventStats = TableQuery[NFLEventStats]

  /**
    * NCAAFSeasonStats
    */
  class NCAAFSeasonStats(tag: Tag) extends Table[NCAAFSeasonStat](tag, "NCAAFootballSeasonStats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def seasonId = column[Int]("seasonId")
    def wins = column[Int]("wins")
    def losses = column[Int]("losses")
    def gamesTotal = column[Int]("gamesTotal")

    def * =
      (id.?, createdAt, modifiedAt, seasonId, wins, losses, gamesTotal) <> (NCAAFSeasonStat.tupled, NCAAFSeasonStat.unapply)
  }
  lazy val NCAAFSeasonStats = TableQuery[NCAAFSeasonStats]

  /**
    * NCAAFEventStats
    */
  class NCAAFEventStats(tag: Tag) extends Table[NCAAFEventStat](tag, "NCAAFootballEventStats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def eventId = column[Int]("eventId")
    def opponent = column[String]("opponent")
    def isHomeOpener = column[Boolean]("isHomeOpener")
    def isPreSeason = column[Boolean]("isPreSeason")

    // (NCAAFEventStat.apply _).tupled is a workaround for a known issue (http://queirozf.com/entries/slick-error-message-value-tupled-is-not-a-member-of-object)
    def * =
      (id.?, createdAt, modifiedAt, eventId, opponent, isHomeOpener, isPreSeason) <> ((NCAAFEventStat.apply _).tupled, NCAAFEventStat.unapply)
  }
  lazy val NCAAFEventStats = TableQuery[NCAAFEventStats]

  /**
    * NCAAFSeasonStats
    */

  class NCAABSeasonStats(tag: Tag)
      extends Table[NCAABSeasonStat](tag, "NCAABasketballSeasonStats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def seasonId = column[Int]("seasonId")
    def wins = column[Int]("wins")
    def losses = column[Int]("losses")
    def gamesTotal = column[Int]("gamesTotal")

    def * =
      (id.?, createdAt, modifiedAt, seasonId, wins, losses, gamesTotal) <> (NCAABSeasonStat.tupled, NCAABSeasonStat.unapply)
  }
  lazy val NCAABSeasonStats = TableQuery[NCAABSeasonStats]

  /**
    * NCAABEventStats
    */
  class NCAABEventStats(tag: Tag) extends Table[NCAABEventStat](tag, "NCAABasketballEventStats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def eventId = column[Int]("eventId")
    def opponent = column[String]("opponent")
    def isHomeOpener = column[Boolean]("isHomeOpener")
    def isPreSeason = column[Boolean]("isPreSeason")

    // (NCAABEventStat.apply _).tupled is a workaround for a known issue (http://queirozf.com/entries/slick-error-message-value-tupled-is-not-a-member-of-object)
    def * =
      (id.?, createdAt, modifiedAt, eventId, opponent, isHomeOpener, isPreSeason) <> ((NCAABEventStat.apply _).tupled, NCAABEventStat.unapply)
  }

  lazy val NCAABEventStats = TableQuery[NCAABEventStats]

  /**
    * PriceGuards
    */
  class PriceGuards(tag: Tag) extends Table[PriceGuard](tag, "PriceGuards") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def eventId = column[Int]("eventId")
    def section = column[String]("section")
    def row = column[String]("row")
    def minimumPrice = column[Int]("minimumPrice")
    def maximumPrice = column[Option[Int]]("maximumPrice")
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")

    def * =
      (id.?, eventId, section, row, minimumPrice, maximumPrice, createdAt, modifiedAt) <> (PriceGuard.tupled, PriceGuard.unapply)
  }
  lazy val PriceGuards = TableQuery[PriceGuards]

  /**
    * PriceScales
    */
  class PriceScales(tag: Tag) extends Table[PriceScale](tag, "PriceScales") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def venueId = column[Int]("venueId")
    def integrationId = column[Int]("integrationId")
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")

    def * =
      (id.?, name, venueId, integrationId, createdAt, modifiedAt) <> (PriceScale.tupled, PriceScale.unapply)
  }
  lazy val PriceScales = TableQuery[PriceScales]

  /**
    * PricingPreviewSections
    */
  class PricingPreviewSections(tag: Tag)
      extends Table[PricingPreviewSection](tag, "PricingPreviewSections") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Instant]("createdAt")
    def modifiedAt = column[Instant]("modifiedAt")
    def clientId = column[Int]("clientId")
    def section = column[String]("section")

    def * =
      (id.?, createdAt, modifiedAt, clientId, section) <> (PricingPreviewSection.tupled, PricingPreviewSection.unapply)
  }
  lazy val PricingPreviewSections = TableQuery[PricingPreviewSections]

  /**
    * Projections
    */
  class Projections(tag: Tag) extends Table[Projection](tag, "TimeStatProjections") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def timestamp = column[Instant]("timestamp")
    def eventId = column[Int]("eventId")
    def inventory = column[Int]("inventory")
    def inventoryLowerBound = column[Int]("inventoryLowerBound")
    def inventoryUpperBound = column[Int]("inventoryUpperBound")
    def revenue = column[BigDecimal]("revenue")
    def revenueLowerBound = column[BigDecimal]("revenueLowerBound")
    def revenueUpperBound = column[BigDecimal]("revenueUpperBound")

    def * =
      (
        id.?,
        createdAt,
        modifiedAt,
        eventId,
        timestamp,
        inventory,
        inventoryLowerBound,
        inventoryUpperBound,
        revenue,
        revenueLowerBound,
        revenueUpperBound
      ) <> (Projection.tupled, Projection.unapply)
    def event: ForeignKeyQuery[Events, Event] = foreignKey("event_fk", eventId, Events)(_.id)
  }
  lazy val Projections = TableQuery[Projections]

  /**
    * ProVenuePricingRules
    */
  class ProVenuePricingRules(tag: Tag)
      extends Table[ProVenuePricingRule](tag, "ProVenuePricingRules") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def clientId = column[Int]("clientId")
    def name = column[String]("name")
    def isActive = column[Boolean]("isActive")
    def mirrorPriceScaleId = column[Int]("mirrorPriceScaleId")
    def percent = column[Int]("percent")
    def constant = column[BigDecimal]("constant")
    def round = column[RoundingType]("round")

    def * = (
      id.?,
      createdAt,
      modifiedAt,
      clientId,
      name.?,
      isActive,
      mirrorPriceScaleId.?,
      percent.?,
      constant.?,
      round
    ) <> (ProVenuePricingRule.tupled, ProVenuePricingRule.unapply)
  }
  lazy val ProVenuePricingRules = TableQuery[ProVenuePricingRules]

  /**
    * ProVenuePricingRulePriceScales
    */
  class ProVenuePricingRulePriceScales(tag: Tag)
      extends Table[ProVenuePricingRulePriceScale](tag, "ProVenuePricingRulePriceScales") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def priceScaleId = column[Int]("priceScaleId")
    def proVenuePricingRuleId = column[Int]("proVenuePricingRuleId")

    def * =
      (id.?, createdAt, modifiedAt, priceScaleId, proVenuePricingRuleId) <> (ProVenuePricingRulePriceScale.tupled, ProVenuePricingRulePriceScale.unapply)
  }
  lazy val ProVenuePricingRulePriceScales = TableQuery[ProVenuePricingRulePriceScales]

  /**
    * ProVenuePricingRuleBuyerTypes
    */
  class ProVenuePricingRuleBuyerTypes(tag: Tag)
      extends Table[ProVenuePricingRuleBuyerType](tag, "ProVenuePricingRuleBuyerTypes") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def externalBuyerTypeId = column[String]("externalBuyerTypeId")
    def proVenuePricingRuleId = column[Int]("proVenuePricingRuleId")

    def * =
      (id.?, createdAt, modifiedAt, externalBuyerTypeId, proVenuePricingRuleId) <> (ProVenuePricingRuleBuyerType.tupled, ProVenuePricingRuleBuyerType.unapply)
  }
  lazy val ProVenuePricingRuleBuyerTypes = TableQuery[ProVenuePricingRuleBuyerTypes]

  /**
    * ProVenuePricingRuleEvents
    */
  class ProVenuePricingRuleEvents(tag: Tag)
      extends Table[ProVenuePricingRuleEvent](tag, "ProVenuePricingRuleEvents") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def eventId = column[Int]("eventId")
    def proVenuePricingRuleId = column[Int]("proVenuePricingRuleId")

    def * =
      (id.?, createdAt, modifiedAt, eventId, proVenuePricingRuleId) <> (ProVenuePricingRuleEvent.tupled, ProVenuePricingRuleEvent.unapply)
  }
  lazy val ProVenuePricingRuleEvents = TableQuery[ProVenuePricingRuleEvents]

  /**
    * RevenueCategoryMappings
    */
  class RevenueCategoryMappings(tag: Tag)
      extends Table[RevenueCategoryMapping](tag, "RevenueCategoryMappings") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def clientId = column[Int]("clientId")
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def revenueCategoryId = column[Int]("revenueCategoryId")
    def regex = column[String]("regex")
    def order = column[Int]("order")

    def * =
      (id.?, clientId, createdAt, modifiedAt, revenueCategoryId, regex, order) <> (RevenueCategoryMapping.tupled, RevenueCategoryMapping.unapply)
  }
  lazy val RevenueCategoryMappings = TableQuery[RevenueCategoryMappings]

  /**
    * RevenueCategories
    */
  class RevenueCategories(tag: Tag) extends Table[RevenueCategory](tag, "RevenueCategories") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def name = column[String]("name")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def isDisplayed = column[Boolean]("isDisplayed")

    def * =
      (id.?, createdAt, name, modifiedAt, isDisplayed) <> (RevenueCategory.tupled, RevenueCategory.unapply)
  }
  lazy val RevenueCategories = TableQuery[RevenueCategories]

  /**
    * Seasons
    */
  class Seasons(tag: Tag) extends Table[Season](tag, "Seasons") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def clientId = column[Int]("clientId")
    def startDate = column[Option[Timestamp]]("startDate")
    def endDate = column[Option[Timestamp]]("endDate")
    def * = (id.?, name.?, clientId, startDate, endDate) <> (Season.tupled, Season.unapply)
  }
  lazy val Seasons = TableQuery[Seasons]

  /**
    * Seats
    */
  class Seats(tag: Tag) extends Table[Seat](tag, "Seats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def primarySeatId = column[Int]("primarySeatId")
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def seat = column[String]("seat")
    def row = column[String]("row")
    def section = column[String]("section")
    def venueId = column[Int]("venueId")

    def * =
      (id.?, primarySeatId.?, createdAt, modifiedAt, seat, row, section, venueId) <> (Seat.tupled, Seat.unapply)
  }
  lazy val Seats = TableQuery[Seats]

  /**
    * SecondaryPricingRules
    */
  class SecondaryPricingRules(tag: Tag)
      extends Table[SecondaryPricingRule](tag, "SecondaryPricingRules") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def clientIntegrationId = column[Int]("clientIntegrationId")
    def percent = column[Int]("percent")
    def constant = column[BigDecimal]("constant")

    def * =
      (id.?, createdAt, modifiedAt, clientIntegrationId, percent.?, constant.?) <> (SecondaryPricingRule.tupled, SecondaryPricingRule.unapply)
  }
  lazy val SecondaryPricingRules = TableQuery[SecondaryPricingRules]

  /**
    * ScheduledJobs
    */
  class ScheduledJobs(tag: Tag) extends Table[ScheduledJob](tag, "ScheduledJobs") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def eventId = column[Int]("eventId")
    def jobConfigId = column[Int]("jobConfigId")
    def status = column[ScheduledJobStatus]("status")
    def jobPriorityId = column[Int]("jobPriorityId")
    def startTime = column[Timestamp]("startTime")
    def endTime = column[Timestamp]("endTime")
    def error = column[String]("error")
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")

    def * =
      (
        id.?,
        eventId,
        jobConfigId,
        status,
        jobPriorityId,
        startTime.?,
        endTime.?,
        error.?,
        createdAt,
        modifiedAt
      ) <> (ScheduledJob.tupled, ScheduledJob.unapply)

    def jobPriority: ForeignKeyQuery[JobPriorities, JobPriority] =
      foreignKey("jobPriority_fk", jobPriorityId, TableQuery[JobPriorities])(_.id)

    def jobConfig: ForeignKeyQuery[JobConfigs, JobConfig] =
      foreignKey("jobConfig_fk", jobConfigId, TableQuery[JobConfigs])(_.id)
  }
  lazy val ScheduledJobs = TableQuery[ScheduledJobs]

  /**
    * ScheduledJobTriggers
    */
  class ScheduledJobTriggers(tag: Tag)
      extends Table[ScheduledJobTrigger](tag, "ScheduledJobTriggers") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def scheduledJobId = column[Int]("scheduledJobId")
    def jobTriggerId = column[Int]("jobTriggerId")
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")

    def * =
      (id.?, scheduledJobId, jobTriggerId, createdAt, modifiedAt) <> (ScheduledJobTrigger.tupled, ScheduledJobTrigger.unapply)

    def scheduledJob: ForeignKeyQuery[ScheduledJobs, ScheduledJob] =
      foreignKey("scheduledJob_fk", scheduledJobId, TableQuery[ScheduledJobs])(_.id)

    def jobTrigger: ForeignKeyQuery[JobTriggers, JobTrigger] =
      foreignKey("jobTrigger_fk", jobTriggerId, TableQuery[JobTriggers])(_.id)
  }
  lazy val ScheduledJobTriggers = TableQuery[ScheduledJobTriggers]

  /**
    * TeamAbbreviations
    */
  class TeamAbbreviations(tag: Tag)
      extends Table[MySportsFeedInfo](tag, "MySportsFeedsTeamAbbreviations") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def clientId = column[Int]("clientId")
    def abbreviation = column[String]("abbreviation")

    def * =
      (id.?, createdAt, modifiedAt, clientId, abbreviation) <> (MySportsFeedInfo.tupled, MySportsFeedInfo.unapply)
  }
  lazy val TeamAbbreviations = TableQuery[TeamAbbreviations]

  /**
    * TeamStats
    */
  class TeamStats(tag: Tag) extends Table[TeamStat](tag, "TeamStats") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def clientId = column[Int]("clientId")
    def seasonId = column[Int]("seasonId")
    def wins = column[Int]("wins")
    def losses = column[Int]("losses")
    def gamesTotal = column[Int]("gamesTotal")

    def * =
      (id.?, createdAt, modifiedAt, clientId, seasonId, wins, losses, gamesTotal) <> (TeamStat.tupled, TeamStat.unapply)
  }
  lazy val TeamStats = TableQuery[TeamStats]

  /**
    * Transactions
    */
  class Transactions(tag: Tag) extends Table[Transaction](tag, "Transactions") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def timestamp = column[Timestamp]("timestamp")
    def eventSeatId = column[Int]("eventSeatId")
    def price = column[BigDecimal]("price")
    def integrationId = column[Int]("integrationId")
    def buyerTypeCode = column[String]("buyerTypeCode")
    def revenueCategoryId = column[Int]("revenueCategoryId")
    def primaryTransactionId = column[String]("primaryTransactionId")
    def secondaryTransactionId = column[String]("secondaryTransactionId")
    def transactionType = column[TransactionType]("type")

    def * =
      (
        id.?,
        createdAt,
        modifiedAt,
        timestamp,
        price,
        eventSeatId,
        integrationId,
        buyerTypeCode,
        transactionType,
        revenueCategoryId.?,
        primaryTransactionId.?,
        secondaryTransactionId.?
      ) <> (Transaction.tupled, Transaction.unapply)
  }
  lazy val Transactions = TableQuery[Transactions]

  /**
    * Users
    */
  class Users(tag: Tag) extends Table[User](tag, "Users") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def email = column[String]("email", O.Unique)
    def firstName = column[String]("firstName")
    def lastName = column[String]("lastName")
    def password = column[Option[String]]("password")
    def tempPass = column[Option[String]]("tempPass")
    def tempExpire = column[Option[Timestamp]]("tempExpire")
    def isAdmin = column[Boolean]("isAdmin")
    def phoneNumber = column[Option[String]]("phoneNumber")
    def activeClientId = column[Int]("activeClientId")

    def * =
      (
        id.?,
        createdAt,
        modifiedAt,
        email,
        firstName,
        lastName,
        password,
        tempPass,
        tempExpire,
        isAdmin,
        phoneNumber,
        activeClientId
      ) <> (User.tupled, User.unapply)
  }
  lazy val Users = TableQuery[Users]

  /**
    * UserClients
    */
  class UserClients(tag: Tag) extends Table[UserClient](tag, "UserClients") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def userId = column[Int]("userId")
    def clientId = column[Int]("clientId")
    def isDefault = column[Boolean]("isDefault")
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")

    def * =
      (id.?, userId, clientId, isDefault, createdAt, modifiedAt) <> (UserClient.tupled, UserClient.unapply)
  }
  lazy val UserClients = TableQuery[UserClients]

  /**
    * Venues
    */
  class Venues(tag: Tag) extends Table[Venue](tag, "Venues") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def name = column[String]("name")
    def capacity = column[Int]("capacity")
    def zipCode = column[String]("zipCode")
    def timeZone = column[String]("timeZone")
    def svgUrl = column[Option[String]]("svgUrl")

    def * =
      (id.?, createdAt, modifiedAt, name, capacity, zipCode, timeZone, svgUrl) <> (Venue.tupled, Venue.unapply)
  }
  lazy val Venues = TableQuery[Venues]

  /**
    * WeatherAverages
    */
  class WeatherAverages(tag: Tag) extends Table[WeatherAverage](tag, "WeatherAverages") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def createdAt = column[Timestamp]("createdAt")
    def modifiedAt = column[Timestamp]("modifiedAt")
    def venueId = column[Int]("venueId")
    def month = column[Int]("month")
    def day = column[Int]("day")
    def hour = column[Int]("hour")
    def temp = column[Double]("temp")

    def * =
      (id.?, createdAt, modifiedAt, venueId, month, day, hour, temp) <> (WeatherAverage.tupled, WeatherAverage.unapply)
  }
  lazy val WeatherAverages = TableQuery[WeatherAverages]

  /**
    * Rounding type enum conversion
    */
  implicit val roundingTypeColumnType: JdbcType[RoundingType] with BaseTypedType[RoundingType] =
    MappedColumnType.base[RoundingType, String]({ round =>
      round.toString
    }, { str =>
      RoundingType.withName(str)
    })

  /**
    * Job Priority Type enum conversion
    */
  implicit val jobPriorityTypeColumnType: JdbcType[JobPriorityType]
    with BaseTypedType[JobPriorityType] = MappedColumnType.base[JobPriorityType, String]({
    jobPriorityType =>
      jobPriorityType.toString
  }, { str =>
    JobPriorityType.withName(str)
  })

  /**
    * Scheduled Job Status enum conversion
    */
  implicit val scheduledJobStatusColumnType: JdbcType[ScheduledJobStatus]
    with BaseTypedType[ScheduledJobStatus] =
    MappedColumnType.base[ScheduledJobStatus, String]({ scheduledJobStatus =>
      scheduledJobStatus.toString
    }, { str =>
      ScheduledJobStatus.withName(str)
    })

  /**
    * Job Trigger Type enum conversion
    */
  implicit val jobTriggerTypeColumnType: JdbcType[JobTriggerType]
    with BaseTypedType[JobTriggerType] =
    MappedColumnType.base[JobTriggerType, String]({ jobTriggerType =>
      jobTriggerType.toString
    }, { str =>
      JobTriggerType.withName(str)
    })

  /**
    * Transaction Type enum conversion
    */
  implicit val transactionTypeColumnType: JdbcType[TransactionType]
    with BaseTypedType[TransactionType] =
    MappedColumnType.base[TransactionType, String]({ transactionType =>
      transactionType.toString
    }, { str =>
      TransactionType.withName(str)
    })

  /**
    * Client Performance Type enum conversion
    */
  implicit val clientPerformanceTypeColumnType: JdbcType[ClientPerformanceType]
    with BaseTypedType[ClientPerformanceType] =
    MappedColumnType.base[ClientPerformanceType, String]({ clientPerformanceType =>
      clientPerformanceType.toString
    }, { str =>
      ClientPerformanceType.withName(str)
    })

  /**
    * Job Type enum conversion
    */
  implicit val jobTypeColumnType: JdbcType[JobType] with BaseTypedType[JobType] =
    MappedColumnType.base[JobType, String]({ jobType =>
      jobType.toString
    }, { str =>
      JobType.withName(str)
    })
}
