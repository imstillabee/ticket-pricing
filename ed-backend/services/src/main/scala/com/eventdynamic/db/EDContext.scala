package com.eventdynamic.db

import javax.inject.Singleton
import org.slf4j.LoggerFactory
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

// This class contains the database configuration that's passed around services
// Only one needs to be created per application
@Singleton
class EDContext() {
  private val logger = LoggerFactory.getLogger(this.getClass)

  logger.info("Creating instance of EDContext")

  // Loading the configuration from the passed configuration files
  // The test/dev/qa/prod.conf files can have different connection strings or even backing databases
  // This object will let us work with all using the same code
  private val databaseConfig =
    DatabaseConfig.forConfig[EDPostgresProfile]("services.database")

  // The profile is what translates the queries to the database specific language
  // Could be a H2 for tests or Postgres for production
  // This should used for imports wherever db queries are run
  // eg. `import context.profile.api._`
  lazy val profile = databaseConfig.profile

  // Use this to actually make calls
  lazy val db = databaseConfig.db

  // Contains the configuration used to create the context
  // Used for test migrations
  lazy val config = databaseConfig.config

  // Cleanup database connection
  def dispose() = {
    this.db.close()
  }

  /**
    * Ensures that the database connections close at the end of the program's lifecycle.
    */
  def registerShutdownHook(): Unit = {
    logger.info("Creating EDContext shutdown hook")
    sys.addShutdownHook {
      println("Shutting down EDContext from system shutdown hook")
      db.close()
    }
  }
}
