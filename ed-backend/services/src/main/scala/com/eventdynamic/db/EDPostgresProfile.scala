package com.eventdynamic.db

import com.github.tminglei.slickpg._

trait EDPostgresProfile extends ExPostgresProfile with PgDate2Support with PgArraySupport {

  def pgjson = "jsonb"

  override val api = new API with DateTimeImplicits with Date2DateTimePlainImplicits
  with ArrayImplicits with SimpleArrayPlainImplicits {}
}

object EDPostgresProfile extends EDPostgresProfile
