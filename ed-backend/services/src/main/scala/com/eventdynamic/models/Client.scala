package com.eventdynamic.models

import java.sql.Timestamp

import com.eventdynamic.models.ClientPerformanceType.ClientPerformanceType

case class Client(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  pricingInterval: Int,
  name: String,
  logoUrl: Option[String],
  eventScoreFloor: Double,
  eventScoreCeiling: Option[Double],
  springFloor: Double,
  springCeiling: Option[Double],
  performanceType: ClientPerformanceType
)
