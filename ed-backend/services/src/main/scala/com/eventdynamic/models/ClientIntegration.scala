package com.eventdynamic.models

import java.sql.Timestamp

import play.api.libs.json._

trait ClientIntegrationConfig

/**
  * Configuration for Skybox
  *
  * @param customers map of Skybox customer ID to EventDynamic integration ID
  *                  This map is used to
  *                   1. Find the integration ID for Skybox orders bought on a secondary (e.g. "Vivid Seats - Stubhub")
  *                   when ingesting Skybox transactions in ticket-sync or ticket-fulfillment. See EVNT-372.
  *                   2. Find the Skybox customer ID for a given EventDynamic Integration when creating an invoice in
  *                   the Skybox Reserver
  */
case class SkyboxClientIntegrationConfig(
  appKey: String,
  baseUrl: String,
  accountId: Int,
  apiKey: String,
  defaultVendorId: Int,
  customers: Map[String, Int],
  customersToIgnore: Seq[String]
) extends ClientIntegrationConfig {

  def getCustomerIdForIntegrationId(integrationId: Int): Option[String] = {
    customers.find(_._2 == integrationId).map(_._1)
  }
}

object SkyboxClientIntegrationConfig {

  trait format {
    implicit lazy val skyboxClientIntegrationConfigFormat: Format[SkyboxClientIntegrationConfig] =
      Json.format[SkyboxClientIntegrationConfig]
  }
}

case class PriceGridConfig(priceGroupId: Int, externalBuyerTypeIds: Seq[String])
    extends ClientIntegrationConfig

object PriceGridConfig {

  trait format {
    implicit lazy val PriceGridConfigFormat: Format[PriceGridConfig] =
      Json.format[PriceGridConfig]
  }
}

case class TDCClientIntegrationConfig(
  agent: String,
  apiKey: String,
  appId: String,
  baseUrl: String,
  password: String,
  username: String,
  supplierId: Int,
  tdcProxyBaseUrl: String,
  mlbamGridConfigs: Seq[PriceGridConfig],
  sandboxEmail: Option[String]
) extends ClientIntegrationConfig

object TDCClientIntegrationConfig extends PriceGridConfig.format {

  trait format {
    implicit lazy val TDCClientIntegrationConfigFormat: Format[TDCClientIntegrationConfig] =
      Json.format[TDCClientIntegrationConfig]
  }
}

case class StubHubClientIntegrationConfig(
  mode: String,
  username: String,
  password: String,
  consumerKey: String,
  consumerSecret: String,
  baseUrl: String,
  applicationToken: String,
  sellerGUID: String
) extends ClientIntegrationConfig

object StubHubClientIntegrationConfig {

  trait format {
    implicit lazy val stubHubClientIntegrationConfigFormat: Format[StubHubClientIntegrationConfig] =
      Json.format[StubHubClientIntegrationConfig]
  }
}

case class EDTicketsClientIntegrationConfig(
  baseUrl: String,
  apiKey: String,
  appId: String,
  appDatabase: String,
  email: String,
  password: String,
  functionsUrl: String,
) extends ClientIntegrationConfig

object EDTicketsClientIntegrationConfig {

  trait format {
    implicit lazy val edTicketsClientIntegrationConfig: Format[EDTicketsClientIntegrationConfig] =
      Json.format[EDTicketsClientIntegrationConfig]
  }
}

case class ClientIntegration(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  clientId: Int,
  integrationId: Int,
  isActive: Boolean,
  isPrimary: Boolean,
  version: Option[String],
  json: Option[String]
)

case class ClientIntegrationComposite(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  clientId: Int,
  integrationId: Int,
  name: String,
  version: Option[String],
  json: Option[String],
  isActive: Boolean,
  isPrimary: Boolean,
  logoUrl: Option[String],
  percent: Option[Int],
  constant: Option[BigDecimal]
) extends SkyboxClientIntegrationConfig.format
    with TDCClientIntegrationConfig.format {

  def config[T <: ClientIntegrationConfig](implicit reads: Reads[T]): T = {
    Json.parse(json.get).as[T]
  }
}
