package com.eventdynamic.models

import java.sql.Timestamp

case class ClientIntegrationEvent(
  id: Option[Int],
  createdAt: Timestamp,
  clientIntegrationId: Int,
  eventId: Int,
  integrationEventId: String,
  modifiedAt: Timestamp
)
