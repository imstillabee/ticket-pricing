package com.eventdynamic.models

import java.sql.Timestamp

case class ClientIntegrationEventSeat(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  clientIntegrationListingId: Int,
  eventSeatId: Int
)
