package com.eventdynamic.models

import java.sql.Timestamp

case class ClientIntegrationListing(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  clientIntegrationEventsId: Int,
  listingId: String
)
