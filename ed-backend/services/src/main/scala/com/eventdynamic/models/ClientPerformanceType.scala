package com.eventdynamic.models

object ClientPerformanceType extends Enumeration {
  type ClientPerformanceType = Value
  val MLB, NFL, MLS, NCAAF, NCAAB = Value
}
