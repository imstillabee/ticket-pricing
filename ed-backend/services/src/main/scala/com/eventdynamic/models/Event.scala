package com.eventdynamic.models

import java.sql.Timestamp

case class Event(
  id: Option[Int],
  primaryEventId: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  name: String,
  timestamp: Option[Timestamp],
  clientId: Int,
  venueId: Int,
  eventCategoryId: Int,
  seasonId: Option[Int],
  isBroadcast: Boolean,
  percentPriceModifier: Int,
  eventScore: Option[Double],
  eventScoreModifier: Double,
  spring: Option[Double],
  springModifier: Double
)
