package com.eventdynamic.models

import java.sql.Timestamp

case class EventCategory(
  id: Option[Int],
  name: String,
  clientId: Int,
  createdAt: Timestamp,
  modifiedAt: Timestamp
)
