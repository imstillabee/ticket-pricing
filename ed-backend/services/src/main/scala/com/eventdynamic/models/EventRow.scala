package com.eventdynamic.models

case class EventRow(
  eventId: Int,
  section: String,
  row: String,
  priceScaleId: Int,
  seats: String, // json array of eventSeatIds for unsold seats in row
  listedPrice: Option[BigDecimal],
  overridePrice: Option[BigDecimal],
  minimumPrice: Option[Int],
  maximumPrice: Option[Int],
  isListed: Boolean
)
