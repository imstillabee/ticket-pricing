package com.eventdynamic.models

import java.sql.Timestamp

/**
  *
  * @param id
  * @param seatId
  * @param eventId
  * @param priceScaleId
  * @param listedPrice
  * @param overridePrice
  * @param isListed
  * @param createdAt
  * @param modifiedAt
  * @param isHeld
  */
case class EventSeat(
  id: Option[Int] = None,
  seatId: Int,
  eventId: Int,
  priceScaleId: Int,
  listedPrice: Option[BigDecimal],
  overridePrice: Option[BigDecimal],
  isListed: Boolean,
  createdAt: Timestamp = new Timestamp(0),
  modifiedAt: Timestamp = new Timestamp(0),
  isHeld: Boolean
)

// TODO move the below models to the service

// Trait for case classes that bulk update event seat listed prices
sealed trait EventSeatListedPriceUpdate {
  val eventId: Int
  val price: BigDecimal
}

// Used to price ticket by groups of seats in the same scale
case class EventSeatListedPriceUpdateByScale(eventId: Int, priceScaleId: Int, price: BigDecimal)
    extends EventSeatListedPriceUpdate

// Used to price tickets by rows of seats with the same price
case class EventSeatListedPriceUpdateByRow(
  eventId: Int,
  section: String,
  row: String,
  price: BigDecimal
) extends EventSeatListedPriceUpdate

// Used by the API
case class BulkEventSeatUpdate(
  eventSeatIds: Seq[Int],
  price: Option[BigDecimal],
  isListed: Option[Boolean]
)

sealed trait EventSeatUniqueIdentifier
case class EventSeatByForeignKeys(eventId: Int, seatId: Int) extends EventSeatUniqueIdentifier
case class EventSeatByPrimaryIdentifiers(primaryEventId: Int, primarySeatId: Int)
    extends EventSeatUniqueIdentifier
case class EventSeatByExternalSeatLocator(
  primaryEventId: Int,
  section: String,
  row: String,
  seat: String
) extends EventSeatUniqueIdentifier
case class EventSeatBySeatLocator(eventId: Int, section: String, row: String, seat: String)
    extends EventSeatUniqueIdentifier
