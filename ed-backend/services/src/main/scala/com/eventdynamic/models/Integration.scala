package com.eventdynamic.models

import java.sql.Timestamp

case class Integration(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  name: String,
  logoUrl: Option[String]
)
