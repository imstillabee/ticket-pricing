package com.eventdynamic.models

case class Inventory(
  id: Option[Int], // This is the eventSeatId, consider renaming this field
  eventId: Int,
  eventIsBroadcast: Boolean,
  section: String,
  row: String,
  rowIsListed: Boolean,
  seat: String,
  listedPrice: BigDecimal,
  overridePrice: Option[BigDecimal],
  listingId: Option[String]
) {

  def getPrice: BigDecimal = {
    this.overridePrice.getOrElse(this.listedPrice)
  }

  def matches(inventory: Inventory): Boolean = {
    this.identifiers == inventory.identifiers
  }

  private def identifiers: (Int, String, String, String) =
    (this.eventId, this.section, this.row, this.seat)

  def matchesWithPrice(inventory: Inventory): Boolean = {
    matches(inventory) && this.listedPrice == inventory.getPrice
  }
}
