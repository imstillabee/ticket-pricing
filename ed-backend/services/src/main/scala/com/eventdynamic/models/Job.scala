package com.eventdynamic.models

import java.sql.Timestamp

import com.eventdynamic.models.JobType.JobType

case class Job(id: Option[Int], jobType: JobType, createdAt: Timestamp, modifiedAt: Timestamp)
