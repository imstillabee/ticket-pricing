package com.eventdynamic.models

import java.sql.Timestamp

case class JobConfig(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  clientId: Int,
  jobId: Int,
  json: String
)
