package com.eventdynamic.models

import java.sql.Timestamp

import com.eventdynamic.models.JobPriorityType.JobPriorityType

case class JobPriority(
  id: Option[Int],
  name: JobPriorityType,
  priority: Int,
  createdAt: Timestamp,
  modifiedAt: Timestamp
)
