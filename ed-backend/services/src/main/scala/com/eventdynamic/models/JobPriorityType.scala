package com.eventdynamic.models

object JobPriorityType extends Enumeration {
  type JobPriorityType = Value

  val Default, TimeDecayWeekly, TimeDecayDaily, TimeDecayHourly, WeatherTemperature,
  WeatherCondition, EventStatsChange, VelocityLow, VelocityHigh, ManualPricingModifierChange,
  ManualOverride = Value
}
