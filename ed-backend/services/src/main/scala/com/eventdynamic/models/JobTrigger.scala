package com.eventdynamic.models

import java.sql.Timestamp

import com.eventdynamic.models.JobTriggerType.JobTriggerType

case class JobTrigger(
  id: Option[Int],
  triggerType: JobTriggerType,
  createdAt: Timestamp,
  modifiedAt: Timestamp
)
