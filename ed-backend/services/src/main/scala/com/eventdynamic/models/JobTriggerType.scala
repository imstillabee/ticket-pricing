package com.eventdynamic.models

object JobTriggerType extends Enumeration {
  type JobTriggerType = Value
  val Manual, Time = Value
}
