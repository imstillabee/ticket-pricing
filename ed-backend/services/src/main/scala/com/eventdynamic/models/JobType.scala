package com.eventdynamic.models

object JobType extends Enumeration {
  type JobType = Value
  val PrimarySync, TicketFulfillment, Pricing = Value
}
