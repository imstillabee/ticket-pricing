package com.eventdynamic.models

case class MLBAMTeamToClient(id: Option[Int], clientId: Int, teamId: Int)

case class MLBAMSectionToPriceScale(id: Option[Int], priceScaleId: Int, sectionId: Int)

case class MLBAMScheduleToEvent(id: Option[Int], eventId: Int, scheduleId: Int)
