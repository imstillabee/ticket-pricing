package com.eventdynamic.models

import java.sql.Timestamp

case class MLSEventStat(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  eventId: Int,
  opponent: String,
  homeGameNumber: Int
)
