package com.eventdynamic.models

import java.sql.Timestamp

case class MLSSeasonStat(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  seasonId: Int,
  wins: Int,
  losses: Int,
  ties: Int,
  gamesTotal: Int
)
