package com.eventdynamic.models

import com.typesafe.config.{Config, ConfigFactory}

case class MailerConfig(
  fromEmail: String,
  supportEmail: String,
  edSiteLink: String,
  mailEnabled: Boolean,
  whitelist: Array[String],
  userTempPasswordExpiration: String,
  userResetPasswordExpiration: String
)

object MailerConfig {

  def fromConfig(config: Config = ConfigFactory.load()): MailerConfig = {
    try {
      MailerConfig(
        fromEmail = config.getString("mail.from"),
        supportEmail = config.getString("mail.supportEmail"),
        edSiteLink = config.getString("mail.edUrl") + config.getString("mail.edPath"),
        mailEnabled = config.getString("mail.isEnabled").toBoolean,
        whitelist =
          config.getString("mail.whitelist").split(",").map(_.toLowerCase.trim).filter(!_.isEmpty),
        userTempPasswordExpiration = (config.getInt("user.tempPasswordExpiration") / 60).toString,
        userResetPasswordExpiration = (config.getInt("user.resetPasswordExpiration") / 60).toString
      )
    } catch {
      case _: Throwable => throw new Exception("There are no mail configurations set")
    }
  }

  def default: MailerConfig = fromConfig()
}
