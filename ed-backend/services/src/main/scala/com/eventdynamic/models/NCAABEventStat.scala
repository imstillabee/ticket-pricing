package com.eventdynamic.models

import java.sql.Timestamp

case class NCAABEventStat(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  eventId: Int,
  opponent: String,
  isHomeOpener: Boolean,
  isPreSeason: Boolean
)
