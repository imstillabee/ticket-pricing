package com.eventdynamic.models

import java.sql.Timestamp

case class NCAABSeasonStat(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  seasonId: Int,
  wins: Int,
  losses: Int,
  gamesTotal: Int
)
