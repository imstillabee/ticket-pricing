package com.eventdynamic.models

import java.sql.Timestamp

case class NCAAFSeasonStat(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  seasonId: Int,
  wins: Int,
  losses: Int,
  gamesTotal: Int
)
