package com.eventdynamic.models

import java.sql.Timestamp

case class NFLEventStat(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  clientId: Int,
  eventId: Int,
  opponent: String,
  isHomeOpener: Boolean,
  isPreSeason: Boolean
)

object NFLEventStat {
  // This is the default value for an NFL game where we can't find the opponent
  // This could happen for games that don't exist in our APIs (preseason games)
  val MISSING_OPPONENT = "???"
}
