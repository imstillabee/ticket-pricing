package com.eventdynamic.models

import java.sql.Timestamp

case class NFLSeasonStat(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  clientId: Int,
  seasonId: Int,
  wins: Int,
  losses: Int,
  ties: Int,
  gamesTotal: Int
)
