package com.eventdynamic.models

import java.sql.Timestamp

import com.eventdynamic.models.TransactionType.TransactionType

case class ObservationBase(
  seatId: Int,
  eventId: Int,
  eventName: String,
  integrationEventId: Int,
  integrationSeatId: Int,
  venueId: Int,
  venueZip: String,
  venueTimeZone: String,
  eventDate: Timestamp,
  seat: String,
  row: String,
  section: String,
  eventSeatId: Int,
  listPrice: Option[BigDecimal],
  overridePrice: Option[BigDecimal],
  priceScaleId: Int,
  externalPriceScaleId: Int, // Integration id of the price scale
  priceScale: String,
  soldPrice: Option[BigDecimal],
  revenueCategoryId: Option[Int],
  buyerTypeCode: Option[String],
  integrationId: Option[Int],
  transactionType: Option[TransactionType],
  isListed: Boolean,
  isEventListed: Boolean,
  eventScoreModifier: Double,
  springModifier: Double,
  isHeld: Boolean
)
