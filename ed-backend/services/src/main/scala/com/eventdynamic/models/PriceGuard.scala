package com.eventdynamic.models

import java.sql.Timestamp

case class PriceGuard(
  id: Option[Int],
  eventId: Int,
  section: String,
  row: String,
  minimumPrice: Int,
  maximumPrice: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp
)
