package com.eventdynamic.models

import java.sql.Timestamp

case class PriceScale(
  id: Option[Int],
  name: String,
  venueId: Int,
  integrationId: Int,
  createdAt: Timestamp,
  modifiedAt: Timestamp
)
