package com.eventdynamic.models

import java.time.Instant

case class PricingPreviewSection(
  id: Option[Int],
  createdAt: Instant,
  modifiedAt: Instant,
  clientId: Int,
  section: String
)
