package com.eventdynamic.models

import java.sql.Timestamp

import com.eventdynamic.models.RoundingType.RoundingType

case class ProVenuePricingRule(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  clientId: Int,
  name: Option[String],
  isActive: Boolean,
  mirrorPriceScaleId: Option[Int],
  percent: Option[Int],
  constant: Option[BigDecimal],
  round: RoundingType
)
