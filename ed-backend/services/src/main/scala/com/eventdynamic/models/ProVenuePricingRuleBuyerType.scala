package com.eventdynamic.models

import java.sql.Timestamp

case class ProVenuePricingRuleBuyerType(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  externalBuyerTypeId: String,
  proVenuePricingRuleId: Int
)
