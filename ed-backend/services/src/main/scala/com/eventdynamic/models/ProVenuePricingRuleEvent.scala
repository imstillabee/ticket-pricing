package com.eventdynamic.models

import java.sql.Timestamp

case class ProVenuePricingRuleEvent(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  eventId: Int,
  proVenuePricingRuleId: Int
)
