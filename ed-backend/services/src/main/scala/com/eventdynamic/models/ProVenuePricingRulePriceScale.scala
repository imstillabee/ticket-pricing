package com.eventdynamic.models

import java.sql.Timestamp

case class ProVenuePricingRulePriceScale(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  priceScaleId: Int,
  proVenuePricingRuleId: Int
)
