package com.eventdynamic.models

import java.sql.Timestamp
import java.time.Instant

/**
  * A projection represents the cumulative inventory and revenue for a single event for a single day.
  *
  * @param eventId event the record is associated with
  * @param timestamp instant representing midnight at the event venue.
  *
  *                  This is a ceiled timestamp for the day, the last timestamp that the projection is valid for.
  * @param inventory cumulative inventory
  * @param revenue cumulative revenue
  */
// TODO: change the timestamp to a date
case class Projection(
  id: Option[Int] = None,
  createdAt: Timestamp = new Timestamp(0),
  modifiedAt: Timestamp = new Timestamp(0),
  eventId: Int,
  timestamp: Instant,
  inventory: Int,
  inventoryLowerBound: Int,
  inventoryUpperBound: Int,
  revenue: BigDecimal,
  revenueLowerBound: BigDecimal,
  revenueUpperBound: BigDecimal
)
