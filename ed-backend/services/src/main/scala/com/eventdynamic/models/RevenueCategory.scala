package com.eventdynamic.models

import java.sql.Timestamp

case class RevenueCategory(
  id: Option[Int],
  createdAt: Timestamp,
  name: String,
  modifiedAt: Timestamp,
  isDisplayed: Boolean
)

/**
  * Model representing the cumulative revenue for a revenue category
  *
  * @param id revenue category id
  * @param name revenue category name
  * @param revenue cumulative revenue
  */
case class RevenueCategoryRevenue(id: Int, name: String, revenue: BigDecimal, ticketsSold: Int)
