package com.eventdynamic.models

import java.sql.Timestamp

case class RevenueCategoryMapping(
  id: Option[Int],
  clientId: Int,
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  revenueCategoryId: Int,
  regex: String,
  order: Int
)
