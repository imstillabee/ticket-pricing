package com.eventdynamic.models

object RoundingType extends Enumeration {
  type RoundingType = Value
  val Ceil, Floor, None, Round = Value
}
