package com.eventdynamic.models

import java.sql.Timestamp

import com.eventdynamic.models.ScheduledJobStatus.ScheduledJobStatus

case class ScheduledJob(
  id: Option[Int],
  eventId: Int,
  jobConfigId: Int,
  status: ScheduledJobStatus,
  jobPriorityId: Int,
  startTime: Option[Timestamp],
  endTime: Option[Timestamp],
  error: Option[String],
  createdAt: Timestamp,
  modifiedAt: Timestamp
)
