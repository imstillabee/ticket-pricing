package com.eventdynamic.models

import java.sql.Timestamp

import com.eventdynamic.models.JobTriggerType.JobTriggerType

case class JobTriggerComposite(
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  triggerType: JobTriggerType
)

case class ScheduledJobComposite(
  scheduledJob: ScheduledJob,
  triggers: Seq[JobTriggerComposite],
  jobPriority: JobPriority
)
