package com.eventdynamic.models

object ScheduledJobStatus extends Enumeration {
  type ScheduledJobStatus = Value
  val Pending, Running, Success, Failure = Value
}
