package com.eventdynamic.models

import java.sql.Timestamp

case class ScheduledJobTrigger(
  id: Option[Int],
  scheduledJobId: Int,
  jobTriggerId: Int,
  createdAt: Timestamp,
  modifiedAt: Timestamp
)
