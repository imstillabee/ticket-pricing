package com.eventdynamic.models

import java.sql.Timestamp

case class Season(
  id: Option[Int],
  name: Option[String],
  clientId: Int,
  startDate: Option[Timestamp],
  endDate: Option[Timestamp]
)
