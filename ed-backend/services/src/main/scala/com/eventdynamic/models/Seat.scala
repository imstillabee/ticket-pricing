package com.eventdynamic.models

import java.sql.Timestamp

case class Seat(
  id: Option[Int] = None,
  primarySeatId: Option[Int],
  createdAt: Timestamp = new Timestamp(0),
  modifiedAt: Timestamp = new Timestamp(0),
  seat: String,
  row: String,
  section: String,
  venueId: Int
)
