package com.eventdynamic.models

import java.sql.Timestamp

case class SecondaryPricingRule(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  clientIntegrationId: Int,
  percent: Option[Int],
  constant: Option[BigDecimal]
)
