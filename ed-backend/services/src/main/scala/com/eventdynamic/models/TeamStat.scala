package com.eventdynamic.models

import java.sql.Timestamp

case class TeamStat(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  clientId: Int,
  seasonId: Int,
  wins: Int,
  losses: Int,
  gamesTotal: Int
)
