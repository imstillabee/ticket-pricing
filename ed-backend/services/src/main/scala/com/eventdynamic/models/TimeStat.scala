package com.eventdynamic.models

import java.sql.Timestamp

case class TimeStat(
  intervalStart: Timestamp,
  intervalEnd: Timestamp,
  revenue: Option[BigDecimal],
  inventory: Option[Int],
  cumulativeRevenue: Option[BigDecimal],
  cumulativeInventory: Option[Int],
  isProjected: Boolean
)

case class TimeStatsResponse(meta: TimeStatsMeta, data: Seq[TimeStat])

case class TimeStatsMeta(timeZone: String, interval: String)
