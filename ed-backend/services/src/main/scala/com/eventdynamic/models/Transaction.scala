package com.eventdynamic.models

import java.sql.Timestamp

import com.eventdynamic.models.TransactionType.TransactionType

/**
  * Transaction DB model
  *
  * @param buyerTypeCode Tickets.com buyer type code, e.g. "Adult"
  * @param primaryTransactionId transaction ID in the primary integration
  *                              For specific integrations
  *                               - Skybox Invoice ID
  *                               - Tickets.com Transaction ID
  */
case class Transaction(
  id: Option[Int] = None,
  createdAt: Timestamp = new Timestamp(0),
  modifiedAt: Timestamp = new Timestamp(0),
  timestamp: Timestamp,
  price: BigDecimal,
  eventSeatId: Int,
  integrationId: Int,
  buyerTypeCode: String,
  transactionType: TransactionType,
  revenueCategoryId: Option[Int] = None,
  primaryTransactionId: Option[String] = None,
  secondaryTransactionId: Option[String] = None,
)
