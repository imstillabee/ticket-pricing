package com.eventdynamic.models

object TransactionType extends Enumeration {
  type TransactionType = Value
  val Purchase, Return = Value
}
