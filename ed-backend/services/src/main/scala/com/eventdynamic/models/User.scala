package com.eventdynamic.models

import java.sql.Timestamp

case class User(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  email: String,
  firstName: String,
  lastName: String,
  password: Option[String],
  tempPass: Option[String],
  tempExpire: Option[Timestamp],
  isAdmin: Boolean,
  phoneNumber: Option[String],
  activeClientId: Int
)
