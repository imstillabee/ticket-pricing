package com.eventdynamic.models

import java.sql.Timestamp

case class UserClient(
  id: Option[Int],
  userId: Int,
  clientId: Int,
  isDefault: Boolean,
  createdAt: Timestamp,
  modifiedAt: Timestamp
)
