package com.eventdynamic.models

import java.sql.Timestamp

case class Venue(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  name: String,
  capacity: Int,
  zipCode: String,
  timeZone: String,
  svgUrl: Option[String]
)
