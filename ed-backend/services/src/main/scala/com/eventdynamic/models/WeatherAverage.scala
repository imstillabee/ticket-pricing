package com.eventdynamic.models

import java.sql.Timestamp

case class WeatherAverage(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  venueId: Int,
  month: Int,
  day: Int,
  hour: Int,
  temp: Double
)
