package com.eventdynamic.models.jobconfigs

import com.eventdynamic.models.jobconfigs.ticketfulfillment.{
  SkyboxFulfillmentConfig,
  StubHubFulfillmentConfig,
  TDCFulfillmentConfig
}
import com.eventdynamic.utils.{DurationFormat, InstantISOFormat}
import play.api.libs.json.{Json, OFormat}

trait JobConfigFormats
    extends InstantISOFormat
    with TicketFulfillmentConfigFormat
    with TicketSyncFormat
    with PricingFormat {}

trait PricingFormat extends InstantISOFormat {
  implicit lazy val pricingFormat: OFormat[PricingConfig] = Json.format[PricingConfig]
}

trait TicketSyncFormat extends InstantISOFormat with DurationFormat {
  implicit lazy val ticketSyncFormat: OFormat[TicketSyncConfig] = Json.format[TicketSyncConfig]
}

trait TicketFulfillmentConfigFormat
    extends TDCFulfillmentConfig.format
    with SkyboxFulfillmentConfig.format
    with StubHubFulfillmentConfig.format {
  implicit lazy val ticketFulfillmentConfigFormat: OFormat[TicketFulfillmentConfig] =
    Json.format[TicketFulfillmentConfig]
}
