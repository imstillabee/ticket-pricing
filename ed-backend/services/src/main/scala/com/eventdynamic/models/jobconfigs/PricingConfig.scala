package com.eventdynamic.models.jobconfigs

import java.time.Instant

case class PricingConfig(lastRun: Instant)
