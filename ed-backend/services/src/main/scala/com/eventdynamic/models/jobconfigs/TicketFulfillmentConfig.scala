package com.eventdynamic.models.jobconfigs

import com.eventdynamic.models.jobconfigs.ticketfulfillment.{
  SkyboxFulfillmentConfig,
  StubHubFulfillmentConfig,
  TDCFulfillmentConfig
}

case class TicketFulfillmentConfig(
  tdc: Option[TDCFulfillmentConfig],
  skybox: Option[SkyboxFulfillmentConfig],
  stubhub: Option[StubHubFulfillmentConfig]
)
