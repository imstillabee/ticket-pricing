package com.eventdynamic.models.jobconfigs

import java.time.{Duration, Instant}

case class TicketSyncConfig(lastRun: Instant, transactionSyncStep: Option[Duration] = None)
