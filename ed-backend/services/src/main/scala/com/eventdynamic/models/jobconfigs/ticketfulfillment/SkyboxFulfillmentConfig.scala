package com.eventdynamic.models.jobconfigs.ticketfulfillment

import java.time.Instant

import com.eventdynamic.utils.InstantISOFormat
import play.api.libs.json._

case class SkyboxFulfillmentConfig(lastTransactionPoll: Instant)

object SkyboxFulfillmentConfig extends InstantISOFormat {

  trait format {
    implicit lazy val skyboxFulfillmentConfigFormat: Format[SkyboxFulfillmentConfig] =
      Json.format[SkyboxFulfillmentConfig]
  }
}
