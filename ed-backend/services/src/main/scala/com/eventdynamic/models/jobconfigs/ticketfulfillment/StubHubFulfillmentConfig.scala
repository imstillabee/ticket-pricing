package com.eventdynamic.models.jobconfigs.ticketfulfillment

import java.time.Instant

import com.eventdynamic.utils.InstantISOFormat
import play.api.libs.json._

case class StubHubFulfillmentConfig(lastTransactionPoll: Instant)

object StubHubFulfillmentConfig extends InstantISOFormat {

  trait format {
    implicit lazy val stubhubFulfillmentConfigFormat: Format[StubHubFulfillmentConfig] =
      Json.format[StubHubFulfillmentConfig]
  }
}
