package com.eventdynamic.models.jobconfigs.ticketfulfillment

import java.time.Instant

import com.eventdynamic.utils.InstantISOFormat
import play.api.libs.json._

case class TDCFulfillmentConfig(
  paymentMethodId: Int,
  deliveryMethodId: Int,
  patronAccountId: Int,
  defaultBuyerTypeId: Int,
  lastTransactionPoll: Instant
)

object TDCFulfillmentConfig extends InstantISOFormat {

  trait format {
    implicit lazy val tdcFulfillmentConfigFormat: Format[TDCFulfillmentConfig] =
      Json.format[TDCFulfillmentConfig]
  }
}
