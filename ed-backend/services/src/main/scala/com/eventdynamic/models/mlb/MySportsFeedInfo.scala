package com.eventdynamic.models.mlb

import java.sql.Timestamp

case class MySportsFeedInfo(
  id: Option[Int],
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  clientId: Int,
  abbreviation: String
)
