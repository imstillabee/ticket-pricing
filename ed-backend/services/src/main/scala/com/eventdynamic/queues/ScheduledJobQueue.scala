package com.eventdynamic.queues

import java.io.{PrintWriter, StringWriter}

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.JobPriorityType._
import com.eventdynamic.models.JobTriggerType._
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models.ScheduledJobStatus._
import com.eventdynamic.models._
import com.eventdynamic.utils.DateHelper.now
import com.eventdynamic.utils.JsonUtil
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ScheduledJobQueue @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import Tables.{
    jobPriorityTypeColumnType,
    jobTriggerTypeColumnType,
    jobTypeColumnType,
    scheduledJobStatusColumnType
  }
  import ctx.profile.api._

  private val events = Tables.Events
  private val jobs = Tables.Jobs
  private val jobConfigs = Tables.JobConfigs
  private val jobTriggers = Tables.JobTriggers
  private val jobPriorities = Tables.JobPriorities
  private val scheduledJobs = Tables.ScheduledJobs
  private val scheduledJobTriggers = Tables.ScheduledJobTriggers

  /**
    * When adding a job, we have to cover 2 scenarios:
    *
    * 1) A pending job already exists for the event
    *   - Update the pending job's priority with the given priority
    * 2) A pending job doesn't exist for the event
    *   - Create a job for the event with the given priority
    *
    * @param eventId
    * @param jobConfigId
    * @param jobTriggerType
    * @param jobPriorityType
    * @return Scheduled job
    */
  def add(
    eventId: Int,
    jobConfigId: Int,
    jobTriggerType: JobTriggerType,
    jobPriorityType: JobPriorityType = Default
  ): Future[ScheduledJob] = {
    val query = for {
      // Create or update the existing job record
      jobId <- upsert(eventId, jobConfigId, jobPriorityType)
      trigger <- jobTriggers.filter(_.triggerType === jobTriggerType).result.headOption
      _ <- upsertScheduledJobTrigger(jobId, trigger, jobTriggerType)
      // Return the scheduled jobs
      job <- scheduledJobs
        .filter(_.id === jobId)
        .result
        .head

    } yield job
    ctx.db.run(query.transactionally)
  }

  private def upsertScheduledJobTrigger(
    scheduledJobId: Int,
    jobTrigger: Option[JobTrigger],
    jobTriggerType: JobTriggerType
  ): DBIOAction[Int, NoStream, Effect.Read with Effect.Write] = {
    for {
      existing <- scheduledJobTriggers
        .filter(_.scheduledJobId === scheduledJobId)
        .filter(_.jobTriggerId === jobTrigger.get.id.get)
        .result
        .headOption
      res <- (scheduledJobTriggers returning scheduledJobTriggers.map(_.id)).insertOrUpdate(
        ScheduledJobTrigger(
          id = existing.flatMap(_.id),
          scheduledJobId = scheduledJobId,
          jobTriggerId = jobTrigger.get.id.get,
          createdAt = existing.map(_.createdAt).getOrElse(now()),
          modifiedAt = now()
        )
      )
    } yield existing.flatMap(_.id).getOrElse(res.get)
  }

  def getScheduledJobTrigger(
    scheduledJobId: Int,
    jobTriggerId: Int
  ): Future[Option[ScheduledJobTrigger]] = {
    for {
      scheduledJobTriggers <- ctx.db.run(
        scheduledJobTriggers
          .filter(_.scheduledJobId === scheduledJobId)
          .filter(_.jobTriggerId === jobTriggerId)
          .result
          .headOption
      )
    } yield scheduledJobTriggers
  }

  private def maxJobPriority(a: Option[JobPriority], b: Option[JobPriority]): Option[JobPriority] =
    (a, b) match {
      case (Some(p), Some(q)) => if (p.priority > q.priority) Some(p) else Some(q)
      case (Some(p), None)    => Some(p)
      case (None, Some(q))    => Some(q)
      case (None, None)       => None
    }

  /**
    * Create or update the existing scheduled job record
    *
    * @param eventId
    * @param jobConfigId
    * @param jobPriorityType
    * @return
    */
  private def upsert(
    eventId: Int,
    jobConfigId: Int,
    jobPriorityType: JobPriorityType
  ): DBIOAction[Int, NoStream, Effect.Read with Effect.Write] = for {
    existing <- scheduledJobs
      .filter(_.eventId === eventId)
      .filter(_.jobConfigId === jobConfigId)
      .filter(_.status === Pending)
      .result
      .headOption
    existingJobPriority <- jobPriorities
      .filter(_.id === existing.map(_.jobPriorityId))
      .result
      .headOption
    jobPriority <- jobPriorities.filter(_.name === jobPriorityType).result.headOption
    maxJob <- DBIO.successful(maxJobPriority(existingJobPriority, jobPriority))
    res <- (scheduledJobs returning scheduledJobs.map(_.id)).insertOrUpdate(
      ScheduledJob(
        existing.flatMap(_.id),
        eventId,
        jobConfigId,
        Pending,
        maxJob.get.id.get,
        None,
        None,
        None,
        existing.map(_.createdAt).getOrElse(now()),
        now()
      )
    )
  } yield existing.flatMap(_.id).getOrElse(res.get)

  /**
    * Fetch a scheduled job (of any status) by it's ID
    *
    * @param scheduledJobId
    * @return Scheduled job
    */
  def getById(scheduledJobId: Int): Future[Option[ScheduledJob]] = {
    ctx.db.run(
      scheduledJobs
        .filter(_.id === scheduledJobId)
        .result
        .headOption
    )
  }

  /**
    * Return the scheduled job results for a particular event
    *
    * @param jobConfigId
    * @param eventId
    * @return
    */
  def resultsByEvent(
    jobConfigId: Int,
    eventId: Int
  ): Future[Map[ScheduledJobStatus, Seq[ScheduledJob]]] = {
    val q = for {
      jobs <- scheduledJobs
        .filter(_.jobConfigId === jobConfigId)
        .filter(_.eventId === eventId)
        .result
    } yield jobs.groupBy(_.status)

    ctx.db.run(q)
  }

  /**
    * Get the latest scheduled job for an event with optional status filter
    * @param eventId
    * @param jobType
    * @param status
    * @return A scheduled job composite containing the scheduled job, job priority,
    *         list of job triggers sorted by most recent
    */
  def getLatestForEvent(
    eventId: Int,
    jobType: JobType,
    status: Option[ScheduledJobStatus]
  ): Future[Option[ScheduledJobComposite]] = {
    val q = for {
      s <- scheduledJobs
        .filter(_.eventId === eventId)
        .filterOpt(status)(_.status === _)
        .sortBy(_.modifiedAt.desc)
        .take(1)

      jc <- s.jobConfig
      _ <- jc.job.filter(_.jobType === jobType)

      p <- s.jobPriority
      sjt <- scheduledJobTriggers
        .sortBy(_.modifiedAt.desc)
        .filter(_.scheduledJobId === s.id)
      t <- sjt.jobTrigger
    } yield (s, p, sjt, t)

    ctx.db
      .run(q.result)
      .map(rows => {
        rows
          .groupBy(_._1.id)
          .map {
            case (_, g) =>
              ScheduledJobComposite(
                g.head._1,
                g.map(r => JobTriggerComposite(r._3.createdAt, r._3.modifiedAt, r._4.triggerType)),
                g.head._2
              )
          }
          .headOption
      })
  }

  /**
    * Retrieves, but does not remove, the next scheduled (Pending) job in this queue
    * The query orders the event timestamp in ascending order (closest appear first)
    * and orders the job priorites in descending order (highest first)
    * @param jobConfigId
    * @return Scheduled job
    */
  def peek(jobConfigId: Int): Future[Option[ScheduledJob]] = {
    val query = scheduledJobs
      .filter(_.status === Pending)
      .join(jobPriorities)
      .on(_.jobPriorityId === _.id)
      .join(events)
      .on(_._1.eventId === _.id)
      .join(jobConfigs)
      .on(_._1._1.jobConfigId === _.id)
      .filter(_._2.id === jobConfigId)
      .sortBy(r => (r._1._1._2.priority.desc, r._1._2.timestamp.asc))
      .take(1)
      .map(_._1._1._1)

    ctx.db.run(query.result.headOption)
  }

  /**
    * Marks the next pending scheduled job for a config as active
    *
    * @param jobConfigId
    * @return Scheduled job
    */
  def poll(jobConfigId: Int): Future[Option[ScheduledJob]] = {
    peek(jobConfigId).flatMap {
      case Some(originalJob) =>
        val q = for {
          _ <- scheduledJobs
            .filter(_.id === originalJob.id.get)
            .update(originalJob.copy(status = Running, startTime = Some(now()), modifiedAt = now()))
          updatedJob <- scheduledJobs.filter(_.id === originalJob.id.get).result.headOption
        } yield updatedJob

        ctx.db.run(q)

      case None => Future.successful(None)
    }
  }

  /**
    * Marks a running scheduled job as complete
    *
    * @param jobId
    * @return Scheduled job
    */
  def complete(jobId: Int): Future[Int] = {
    val q = for {
      job <- DBIO.from(getById(jobId)) if job.isDefined
      jobId <- scheduledJobs
        .filter(_.id === jobId)
        .filter(_.status === Running)
        .update(job.get.copy(status = Success, endTime = Some(now()), modifiedAt = now()))
    } yield jobId

    ctx.db.run(q)
  }

  /**
    * Marks a running scheduled job as failed, along with the error message
    */
  def fail(jobId: Int, reason: String): Future[Int] = {
    val q = for {
      job <- DBIO.from(getById(jobId)) if job.isDefined
      jobId <- scheduledJobs
        .filter(_.id === jobId)
        .filter(_.status === Running)
        .update(
          job.get
            .copy(status = Failure, endTime = Some(now()), error = Some(reason), modifiedAt = now())
        )

    } yield jobId

    ctx.db.run(q)
  }

  private case class ErrorMessage(message: String, stackTrace: String, exceptionType: String)

  /**
    * Marks a running scheduled job as failed, along with the error message
    */
  def fail(jobId: Int, reason: Throwable): Future[Int] = {
    val stringWriter = new StringWriter()
    reason.printStackTrace(new PrintWriter(stringWriter))
    val message = ErrorMessage(reason.getMessage, stringWriter.toString, reason.getClass.getName)

    fail(jobId, JsonUtil.stringify(message))
  }
}
