package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.ClientIntegrationEventSeat
import com.eventdynamic.utils.DateHelper
import com.google.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class ClientIntegrationEventSeatsService @Inject()(val ctx: EDContext)(
  implicit ec: ExecutionContext
) {
  import ctx.profile.api._
  lazy val clientIntegrationEventSeats = Tables.ClientIntegrationEventSeats

  /** Create Client Integration Event Seat
    *
    * @param clientIntegrationListingId
    * @param eventSeatId
    * @return
    */
  def create(
    clientIntegrationListingId: Int,
    eventSeatId: Int
  ): Future[ClientIntegrationEventSeat] = {
    val now = DateHelper.now()
    ctx.db.run(
      clientIntegrationEventSeats returning clientIntegrationEventSeats.map(_.id)
        into ((clientIntegration, id) => clientIntegration.copy(id = Some(id)))
        += ClientIntegrationEventSeat(None, now, now, clientIntegrationListingId, eventSeatId)
    )
  }

  /**
    * Create Client Integration Event Seats in bulk
    *
    * @param records CIES records
    * @return number of created CIES
    */
  def bulkInsert(records: Seq[ClientIntegrationEventSeat]): Future[Option[Int]] = {
    val q = clientIntegrationEventSeats ++= records.map(_.copy(id = None))
    ctx.db.run(q)
  }
}
