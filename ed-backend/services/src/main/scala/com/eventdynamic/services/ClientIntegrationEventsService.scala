package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.ClientIntegrationEvent
import com.eventdynamic.utils.DateHelper
import com.google.inject.Inject
import javax.inject.Singleton

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ClientIntegrationEventsService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {

  import ctx.profile.api._

  val clientIntegrationEvents = Tables.ClientIntegrationEvents

  /** Creates a ClientIntegrationEvents Mapping
    *
    * @param clientIntegrationId
    * @param eventId
    * @param integrationEventId
    * @return
    */
  def create(
    clientIntegrationId: Int,
    eventId: Int,
    integrationEventId: String
  ): Future[ClientIntegrationEvent] = {
    val now = DateHelper.now()
    ctx.db.run(
      clientIntegrationEvents returning clientIntegrationEvents.map(_.id)
        into ((clientIntegration, id) => clientIntegration.copy(id = Some(id)))
        += ClientIntegrationEvent(None, now, clientIntegrationId, eventId, integrationEventId, now)
    )
  }

  /** Get Client Integration Event by the Client Integration Id and Event Id
    *
    * @param clientIntegrationId
    * @param eventId
    * @return
    */
  def getBy(clientIntegrationId: Int, eventId: Int): Future[Option[ClientIntegrationEvent]] = {
    ctx.db.run(
      clientIntegrationEvents
        .filter(cie => cie.eventId === eventId && cie.clientIntegrationId === clientIntegrationId)
        .result
        .headOption
    )
  }

  /** Get Client Integration Event by the Client Integration Id and Integration Event Id
    *
    * @param clientIntegrationId
    * @param externalEventId
    * @return
    */
  def getByExternalEventId(
    clientIntegrationId: Int,
    externalEventId: String
  ): Future[Option[ClientIntegrationEvent]] = {
    ctx.db.run(
      clientIntegrationEvents
        .filter(
          cie =>
            cie.integrationEventId === externalEventId && cie.clientIntegrationId === clientIntegrationId
        )
        .result
        .headOption
    )
  }
}
