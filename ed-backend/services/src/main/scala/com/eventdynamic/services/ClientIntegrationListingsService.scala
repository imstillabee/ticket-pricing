package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.ClientIntegrationListing
import com.eventdynamic.utils._
import com.google.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class ClientIntegrationListingsService @Inject()(val ctx: EDContext)(
  implicit ec: ExecutionContext
) {

  import ctx.profile.api._

  val clientIntegrationListings = Tables.ClientIntegrationListings

  /** Create Client Integration Listing
    *
    * @param clientIntegrationEventsId
    * @param listingId
    * @return
    */
  def create(
    clientIntegrationEventsId: Int,
    listingId: String
  ): Future[ClientIntegrationListing] = {
    val now = DateHelper.now()
    ctx.db.run(
      clientIntegrationListings returning clientIntegrationListings
        .map(_.id) into ((clientIntegration, id) => clientIntegration.copy(id = Some(id)))
        += ClientIntegrationListing(None, now, now, clientIntegrationEventsId, listingId)
    )
  }

  /** Get ClientIntegrationListing
    *
    * @param clientIntegrationEventId
    * @param listingId
    * @return
    */
  def get(
    clientIntegrationEventId: Int,
    listingId: String
  ): Future[Option[ClientIntegrationListing]] = {
    ctx.db.run(
      clientIntegrationListings
        .filter(
          cil =>
            cil.listingId === listingId && cil.clientIntegrationEventsId === clientIntegrationEventId
        )
        .result
        .headOption
    )
  }

  /** Get Or Create for ClientIntegrationListing
    *
    * @param clientIntegrationEventsId
    * @param listingId
    * @return
    */
  def getOrCreate(
    clientIntegrationEventsId: Int,
    listingId: String
  ): Future[ClientIntegrationListing] = {
    get(clientIntegrationEventsId, listingId).flatMap {
      case Some(listing) => Future.successful(listing)
      case None          => create(clientIntegrationEventsId, listingId)
    }
  }

}
