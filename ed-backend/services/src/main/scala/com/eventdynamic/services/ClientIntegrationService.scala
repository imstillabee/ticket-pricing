package com.eventdynamic.services

import com.eventdynamic.db.EDContext
import com.eventdynamic.models.{ClientIntegration, ClientIntegrationComposite}
import javax.inject.Singleton
import com.eventdynamic.Tables
import com.eventdynamic.utils._
import com.google.inject.Inject
import play.api.libs.json.{Json, Reads}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ClientIntegrationService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {

  import ctx.profile.api._
  val clientIntegrations = Tables.ClientIntegrations
  val secondaryPricingRules = Tables.SecondaryPricingRules
  val integrations = Tables.Integrations

  /**
    * Creates a clientIntegration
    *
    * @param clientId
    * @param integrationId
    * @param isActive
    * @param isPrimary
    * @return created record with id
    */
  def create(
    clientId: Int,
    integrationId: Int,
    isActive: Boolean,
    isPrimary: Boolean
  ): Future[ClientIntegration] = {
    val now = DateHelper.now()
    ctx.db.run(
      clientIntegrations returning clientIntegrations.map(_.id)
        into ((clientIntegration, id) => clientIntegration.copy(id = Some(id)))
        += ClientIntegration(
          None,
          now,
          now,
          clientId,
          integrationId,
          isActive,
          isPrimary,
          None,
          None
        )
    )
  }

  /**
    * Delete a clientIntegration by id
    *
    * @param id
    * @return boolean
    */
  def delete(id: Int): Future[Boolean] =
    ctx.db.run(clientIntegrations.filter(_.id === id).delete.map(_ == 1))

  /**
    * Get clientIntegration by id
    *
    * @param id
    * @return clientIntegration if it exists
    */
  def getById(id: Int): Future[Option[ClientIntegrationComposite]] = {
    val joinQuery = for {
      (c, s) <- clientIntegrations
        .filter(_.id === id)
        .joinLeft(secondaryPricingRules)
        .on(_.id === _.clientIntegrationId)
      i <- integrations if c.integrationId === i.id
    } yield
      (
        c.id.?,
        c.createdAt,
        c.modifiedAt,
        c.clientId,
        c.integrationId,
        i.name,
        c.version.?,
        c.json.?,
        c.isActive,
        c.isPrimary,
        i.logoUrl.?,
        s.map(_.percent),
        s.map(_.constant)
      ).mapTo[ClientIntegrationComposite]
    ctx.db.run(joinQuery.result).map(x => x.headOption)
  }

  /**
    * Get clientIntegration by integrationId
    *
    * @return clientIntegration if it exists
    */
  def getByIntegrationAndClientId(
    integrationId: Int,
    clientId: Int
  ): Future[Option[ClientIntegration]] = {
    ctx.db.run(
      clientIntegrations
        .filter(_.integrationId === integrationId)
        .filter(_.clientId === clientId)
        .result
        .headOption
    )
  }

  /**
    * Get ClientIntegrationComposite by clientId
    *
    * @return ClientIntegrationComposite
    */
  def getByClientId(
    id: Int,
    onlyActive: Boolean,
    onlyPrimary: Boolean = false
  ): Future[Seq[ClientIntegrationComposite]] = {
    val joinQuery = for {
      (c, s) <- clientIntegrations
        .filterIf(onlyActive)(_.isActive)
        .filterIf(onlyPrimary)(_.isPrimary)
        .filter(_.clientId === id)
        .joinLeft(secondaryPricingRules)
        .on(_.id === _.clientIntegrationId)
      i <- integrations if c.integrationId === i.id
    } yield
      (
        c.id.?,
        c.createdAt,
        c.modifiedAt,
        c.clientId,
        c.integrationId,
        i.name,
        c.version.?,
        c.json.?,
        c.isActive,
        c.isPrimary,
        i.logoUrl.?,
        s.map(_.percent),
        s.map(_.constant)
      ).mapTo[ClientIntegrationComposite]

    ctx.db.run(joinQuery.result)
  }

  /**
    * Fetch a single client integration by its name
    *
    * @param clientId The client ID
    * @param name The integration name
    * @param onlyActive If true, only return active clientIntegrations
    * @return The composed client integration if it exists
    */
  def getByName(
    clientId: Int,
    name: String,
    onlyActive: Boolean = false
  ): Future[Option[ClientIntegrationComposite]] = {
    val joinQuery = for {
      (c, s) <- clientIntegrations
        .filterIf(onlyActive)(_.isActive)
        .filter(_.clientId === clientId)
        .joinLeft(secondaryPricingRules)
        .on(_.id === _.clientIntegrationId)
      i <- integrations.filter(_.name === name) if c.integrationId === i.id
    } yield
      (
        c.id.?,
        c.createdAt,
        c.modifiedAt,
        c.clientId,
        c.integrationId,
        i.name,
        c.version.?,
        c.json.?,
        c.isActive,
        c.isPrimary,
        i.logoUrl.?,
        s.map(_.percent),
        s.map(_.constant)
      ).mapTo[ClientIntegrationComposite]

    ctx.db.run(joinQuery.result.headOption)
  }

  def getClientIntegrationConfiguration[T](clientId: Integer, integrationId: Integer)(
    implicit reads: Reads[T]
  ): Future[Option[T]] = {
    getByIntegrationAndClientId(integrationId, clientId).map(_.map(ci => {
      Json.parse(ci.json.get).as[T]
    }))
  }

  def update(clientIntegration: ClientIntegration): Future[ResponseCode] = {
    val now = DateHelper.now()
    ctx.db.run(
      clientIntegrations
        .filter(_.id === clientIntegration.id)
        .update(clientIntegration.copy(modifiedAt = now))
        .map {
          case 0 => NotFoundResponse
          case _ => SuccessResponse(clientIntegration)
        }
    )
  }

  def updateVersion(id: Int, version: String): Future[ResponseCode] = {
    getById(id).flatMap {
      case Some(composite) =>
        update(
          ClientIntegration(
            composite.id,
            composite.createdAt,
            composite.modifiedAt,
            composite.clientId,
            composite.integrationId,
            composite.isActive,
            composite.isPrimary,
            Some(version),
            composite.json
          )
        )
      case _ => Future.successful(NotFoundResponse)
    }
  }
}
