package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.Client
import com.eventdynamic.models.ClientPerformanceType.ClientPerformanceType
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ClientService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {

  // Import driver from database context
  import ctx.profile.api._
  import Tables.clientPerformanceTypeColumnType

  /** Object to query table from */
  val clients = Tables.Clients
  val userClients = Tables.UserClients

  /** Create a client
    *
    * @param name            string to be used as the Team name
    * @param pricingInterval interval in minutes between successive pricing updates
    * @return created client id
    */
  def create(
    name: String,
    pricingInterval: Int,
    eventScoreFloor: Double,
    eventScoreCeiling: Option[Double],
    springFloor: Double,
    springCeiling: Option[Double],
    performanceType: ClientPerformanceType,
    logoUrl: Option[String] = None
  ): Future[ResponseCode] = {
    val now = DateHelper.now()
    val client = ctx.db.run(
      clients returning clients
        .map(_.id) into ((client, id) => client.copy(id = Some(id))) += Client(
        None,
        now,
        now,
        pricingInterval,
        name,
        logoUrl,
        eventScoreFloor,
        eventScoreCeiling,
        springFloor,
        springCeiling,
        performanceType
      )
    )
    client.map(SuccessResponse(_))
  }

  /** Create client if client does not exist
    *
    * @param name            string to be used as the Team name
    * @param pricingInterval interval in minutes between successive pricing updates
    * @return id of created client or 0 if duplicate found
    */
  def createUnique(
    name: String,
    pricingInterval: Int,
    eventScoreFloor: Double,
    eventScoreCeiling: Option[Double],
    springFloor: Double,
    springCeiling: Option[Double],
    performanceType: ClientPerformanceType,
    logoUrl: Option[String]
  ): Future[ResponseCode] = {
    getUnique(name).flatMap {
      case None =>
        create(
          name,
          pricingInterval,
          eventScoreFloor,
          eventScoreCeiling,
          springFloor,
          springCeiling,
          performanceType,
          logoUrl
        )
      case Some(_) => Future.successful(DuplicateResponse)
    }
  }

  /** Get unique client by name
    *
    * @param name string to be used as the Team name
    * @return client if it exists
    */
  def getUnique(name: String): Future[Option[Client]] = { // will return a client, optionally - if one is found
    ctx.db.run(clients.filter(c => c.name.toLowerCase === name.toLowerCase).result.headOption)
  }

  /** Get a client by its id
    *
    * @param id unique, auto-incremented & indexed identifier
    * @return client if it exists
    */
  def getById(id: Int): Future[Option[Client]] = { // will return a client, optionally - if one is found
    ctx.db.run(clients.filter(_.id === id).result.headOption)
  }

  /** Delete client by id
    *
    * @param id unique, auto-incremented & indexed identifier
    * @return true if client deleted
    */
  def delete(id: Int): Future[Boolean] = {
    ctx.db.run(clients.filter(_.id === id).delete).map(_ == 1)
  }

  /** Update Unique client
    *
    * @param id              unique, auto-incremented & indexed identifier
    * @param name            string to be used as the Team name
    * @param pricingInterval interval in minutes between successive pricing updates
    * @return client if it exists
    */
  def updateUnique(
    id: Int,
    name: String,
    pricingInterval: Int,
    eventScoreFloor: Option[Double],
    eventScoreCeiling: Option[Double],
    springFloor: Option[Double],
    springCeiling: Option[Double]
  ): Future[ResponseCode] = { // this seems to indicate that the returned result will be a response code?
    val now = DateHelper.now()
    getById(id).flatMap {
      case None => Future.successful(NotFoundResponse) // client does not exist
      case Some(client) =>
        getUnique(name).flatMap {
          case None => // there is no client with the name provided in the update client object
            update(
              Client(
                Some(id),
                client.createdAt,
                now,
                pricingInterval,
                name,
                client.logoUrl,
                eventScoreFloor.getOrElse(client.eventScoreFloor),
                eventScoreCeiling.orElse(client.eventScoreCeiling),
                springFloor.getOrElse(client.springFloor),
                springCeiling.orElse(client.springCeiling),
                client.performanceType
              )
            )
          case Some(`client`) =>
            if (client.id.get == id) // the client with the same name as the update object name is this client
              update(
                Client(
                  Some(id),
                  client.createdAt,
                  now,
                  pricingInterval,
                  name,
                  client.logoUrl,
                  eventScoreFloor.getOrElse(client.eventScoreFloor),
                  eventScoreCeiling.orElse(client.eventScoreCeiling),
                  springFloor.getOrElse(client.springFloor),
                  springCeiling.orElse(client.springCeiling),
                  client.performanceType
                )
              )
            else
              Future.successful(DuplicateResponse) // cannot set the current client's name to be the same as that of another client
          case _ =>
            Future.successful(NotFoundResponse) // to preclude the warning for match not being exhaustive
        }
      case _ =>
        Future.successful(NotFoundResponse) // to preclude the warning for match not being exhaustive
    }
  }

  /** Update client
    *
    * @param client client object
    * @return updated client
    */
  def update(client: Client): Future[ResponseCode] = { // was Future[Int]
    val now = DateHelper.now()
    ctx.db.run(clients.filter(_.id === client.id).update(client.copy(modifiedAt = now)).map {
      case 0 => NotFoundResponse
      case _ => SuccessResponse(client)
    })
  }

  /** Get all clients
    *
    * @return All clients
    */
  def getAll(userId: Int): Future[Seq[Client]] = {
    ctx.db.run(
      userClients
        .filter(_.userId === userId)
        .join(clients)
        .on(_.clientId === _.id)
        .sortBy(_._2.name)
        .map(_._2)
        .result
    )
  }

  /**
    * Gets all clients associated with a performance type.
    *
    * @param clientPerformanceType client performance type enum
    * @return sequence of clients (or empty if none)
    */
  def getByPerformance(clientPerformanceType: ClientPerformanceType): Future[Seq[Client]] =
    ctx.db.run(clients.filter(_.performanceType === clientPerformanceType).result)
}
