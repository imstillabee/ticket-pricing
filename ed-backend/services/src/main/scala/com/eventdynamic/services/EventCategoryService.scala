package com.eventdynamic.services

import java.sql.{SQLException, Timestamp}

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.EventCategory
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class EventCategoryService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val eventCategories = Tables.EventCategories

  /** Create an event category
    *
    * @param name
    * @param clientId
    *
    * @return created record id
    */
  def create(name: String, clientId: Int): Future[Int] = {
    val now = DateHelper.now()
    val q = eventCategories returning eventCategories.map(_.id) += EventCategory(
      None,
      name,
      clientId,
      now,
      now
    )

    ctx.db.run(q)
  }

  /**
    * Create event categories in bulk
    *
    * @param records event category records
    * @return number of created events
    */
  def bulkInsert(records: Seq[EventCategory]): Future[Option[Int]] = {
    ctx.db.run(eventCategories ++= records.map(_.copy(id = None)))
  }

  /** Get an event category by id
    *
    * @param id
    * @return event category (if it exists)
    */
  def getById(id: Int): Future[Option[EventCategory]] = {
    ctx.db.run(eventCategories.filter(_.id === id).result.headOption)
  }

  /** Get All EventCategories for a client
    *
    * @param clientId
    * @return
    */
  def getAllForClient(clientId: Int): Future[Seq[EventCategory]] = {
    ctx.db.run(eventCategories.filter(_.clientId === clientId).result)
  }
}
