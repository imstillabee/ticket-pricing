package com.eventdynamic.services

import com.eventdynamic.db.EDContext
import com.eventdynamic.models._
import com.eventdynamic.utils.{DateHelper, DuplicateResponse, ResponseCode, SuccessResponse}
import com.google.inject.{Inject, Singleton}
import java.sql.SQLException

import com.eventdynamic.Tables

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class EventSeatService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  import ctx.profile.api._

  val eventSeats = Tables.EventSeats
  val seats = Tables.Seats
  val events = Tables.Events

  /**
    * Create an event seat record
    *
    * @param eventSeat event seat record
    * @return response code for success or error cases
    */
  def create(eventSeat: EventSeat): Future[ResponseCode] = {
    val q = eventSeats returning eventSeats.map(_.id) into ((es, id) => es.copy(id = Some(id))) += eventSeat
    ctx.db
      .run(q)
      .map(SuccessResponse(_))
      .recover {
        case err: SQLException if err.getMessage contains "duplicate" => DuplicateResponse
      }
  }

  /**
    * Generic bulk update list price function.
    *
    * @param changes list of price changes, must be an extension of a list price update object
    * @return number of updated records
    */
  def updateListedPrices(changes: Seq[_ <: EventSeatListedPriceUpdate]): Future[Int] = {
    val action = DBIO.sequence(changes.map {
      case EventSeatListedPriceUpdateByScale(eventId, priceScaleId, price) => {
        val q = for {
          es <- eventSeats
          if es.eventId === eventId && es.priceScaleId === priceScaleId
        } yield (es.listedPrice, es.modifiedAt)
        q.update((price, DateHelper.now()))
      }
      case EventSeatListedPriceUpdateByRow(eventId, section, row, price) => {
        val now = DateHelper.now()
        sql"""
             UPDATE "EventSeats" AS es
             SET "listedPrice" = $price,
             "modifiedAt" = $now
             FROM "Seats" s
             WHERE es."seatId" = s."id"
             	AND s."section" = $section
             	AND s."row" = $row
             	AND es."eventId" = $eventId
           """.as[Int].head
      }
    })

    ctx.db.run(action.transactionally).map(_.sum)
  }

  /**
    * Fetch an EventSeat record by its id
    *
    * @param id eventSeatId
    * @return the matching event seat if it exists
    */
  def getById(id: Int): Future[Option[EventSeat]] = {
    ctx.db.run(eventSeats.filter(_.id === id).result.headOption)
  }

  /**
    * Upsert an EventSeat record
    *
    * Identifying fields
    *   - eventId
    *   - seatId
    *
    * Updatable fields
    *   - priceScaleId
    *   - listedPrice
    *
    * Ignored fields (will insert but not update)
    *   - isListed
    *   - overridePrice
    *
    * @param record upsert record
    * @return id of inserted or updated record
    */
  def upsert(record: EventSeat): Future[Int] = {
    ctx.db.run(upsertQuery(record).transactionally)
  }

  /**
    * Upsert EventSeat records in bulk
    *
    * @param records EventSeat records to upsert
    * @return Sequence of IDs corresponding the the upserted record(s)
    */
  def bulkUpsert(records: Seq[EventSeat]): Future[Seq[Int]] = {
    ctx.db.run(DBIO.sequence(records.map(upsertQuery)).transactionally)
  }

  /**
    * Upsert by eventId / seatId
    */
  private def upsertQuery(
    record: EventSeat
  ): DBIOAction[Int, NoStream, Effect.Read with Effect.Write] = {
    val now = DateHelper.now()
    for {
      existing <- eventSeats
        .filter(_.eventId === record.eventId)
        .filter(_.seatId === record.seatId)
        .result
        .headOption
      res <- (eventSeats returning eventSeats.map(_.id)).insertOrUpdate(
        record.copy(
          // Use existing id if we have it
          id = existing.flatMap(_.id),
          // Default to existing values for ignored fields
          isListed = existing.map(_.isListed).getOrElse(record.isListed),
          overridePrice = existing.map(_.overridePrice).getOrElse(record.overridePrice),
          createdAt = existing.map(_.createdAt).getOrElse(now),
          modifiedAt = now
        )
      )
    } yield existing.flatMap(_.id).getOrElse(res.get)
  }

  /** Bulk Update - set overridePrice or isListed Boolean for multiple EventSeats to the submitted value, in one transaction
    *
    * @param ids ids of EventSeat records to be updated
    * @param price new overridePrice, which can be a BigDecimal, or a None if overridePrice should be NULL
    * @param listed new state for isListed, or None if this will be an overridePrice change
    * @return integer number of EventSeat records that were successfully updated
    */
  def bulkUpdate(ids: Seq[Int], price: Option[BigDecimal], listed: Option[Boolean]): Future[Int] = {
    val now = DateHelper.now()
    val query =
      listed match { // listed is only None when the requested update is an overridePrice change
        case None =>
          eventSeats
            .filter(_.id.inSet(ids))
            .map(es => (es.overridePrice, es.modifiedAt))
            .update((price, now))
        case _ =>
          eventSeats
            .filter(_.id.inSet(ids))
            .map(es => (es.isListed, es.modifiedAt))
            .update((listed.get, now))
      }
    ctx.db.run(query)
  }

  /**
    * Get by event ID and seat ID
    *
    * @param seatId seat ID
    * @param eventId event ID
    * @return EventSeat if found or None
    */
  def get(seatId: Int, eventId: Int): Future[Option[EventSeat]] = {
    val q = eventSeats.filter(row => row.seatId === seatId && row.eventId === eventId)
    ctx.db.run(q.result.headOption)
  }

  /**
    * Get by event ids
    *
    * @param eventIds event IDs
    * @return EventSeat seq
    */
  def getForEvents(eventIds: Seq[Int]): Future[Seq[EventSeat]] = {
    val q = eventSeats.filter(row => row.eventId.inSet(eventIds))
    ctx.db.run(q.result)
  }

  /**
    * Create event seats in bulk
    *
    * @param records event seat records
    * @return number of created events
    */
  def bulkInsert(records: Seq[EventSeat]): Future[Option[Int]] = {
    val now = DateHelper.now()
    val q = eventSeats ++= records.map(_.copy(id = None, createdAt = now, modifiedAt = now))
    ctx.db.run(q)
  }

  /**
    * Get all for a section / row
    *
    * @param venueId seat venue
    * @param section seat section
    * @param row seat row
    */
  def getForRow(venueId: Int, section: String, row: String): Future[Seq[EventSeat]] = {
    val query = for {
      s <- seats
        .filter(_.venueId === venueId)
        .filter(_.section === section)
        .filter(_.row === row)
      es <- eventSeats if s.id === es.seatId
    } yield es

    ctx.db.run(query.result)
  }

  /**
    * Retrieve an event seat by unique identifiers
    *
    * @param uniqueIdentifier uniquely identifying parameters
    * @return the event seat if it exists
    */
  def getOne(uniqueIdentifier: EventSeatUniqueIdentifier): Future[Option[EventSeat]] = {
    val query = uniqueIdentifier match {
      case EventSeatByForeignKeys(eventId: Int, seatId: Int) =>
        eventSeats.filter(_.eventId === eventId).filter(_.seatId === seatId)

      case EventSeatByPrimaryIdentifiers(primaryEventId: Int, primarySeatId: Int) =>
        for {
          event <- events.filter(_.primaryEventId === primaryEventId)
          seat <- seats.filter(_.primarySeatId === primarySeatId)
          eventSeat <- eventSeats if eventSeat.seatId === seat.id && eventSeat.eventId === event.id
        } yield eventSeat

      case EventSeatByExternalSeatLocator(
          primaryEventId: Int,
          section: String,
          row: String,
          seat: String
          ) =>
        for {
          event <- events.filter(_.primaryEventId === primaryEventId)
          seat <- seats.filter(_.section === section).filter(_.row === row).filter(_.seat === seat)
          eventSeat <- eventSeats if eventSeat.seatId === seat.id && eventSeat.eventId === event.id
        } yield eventSeat

      case EventSeatBySeatLocator(eventId: Int, section: String, row: String, seat: String) =>
        for {
          seat <- seats.filter(_.section === section).filter(_.row === row).filter(_.seat === seat)
          eventSeat <- eventSeats if eventSeat.seatId === seat.id && eventSeat.eventId === eventId
        } yield eventSeat
    }

    ctx.db.run(query.result.headOption)
  }
}
