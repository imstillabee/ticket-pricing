package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.Event
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class EventService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val events = Tables.Events
  val clientIntegrations = Tables.ClientIntegrations
  val clientIntegrationEvents = Tables.ClientIntegrationEvents

  /** Create an event
    *
    * @param primaryEventId
    * @param name
    * @param timestamp
    * @param clientId
    * @param venueId
    * @param eventCategoryId
    * @param seasonId
    * @param isBroadcast
    * @param percentPriceModifier
    * @return created record id
    */
  def create(
    primaryEventId: Option[Int],
    name: String,
    timestamp: Option[Timestamp],
    clientId: Int,
    venueId: Int,
    eventCategoryId: Int,
    seasonId: Option[Int] = None,
    isBroadcast: Boolean = true,
    percentPriceModifier: Int = 0,
    eventScore: Option[Double] = None,
    eventScoreModifier: Double = 0,
    spring: Option[Double] = None,
    springModifier: Double = 0
  ): Future[ResponseCode] = {
    val now = DateHelper.now()
    val event = ctx.db.run(
      events returning events.map(_.id) into ((event, id) => event.copy(id = Some(id))) += Event(
        None,
        primaryEventId,
        now,
        now,
        name,
        timestamp,
        clientId,
        venueId,
        eventCategoryId,
        seasonId,
        isBroadcast,
        percentPriceModifier,
        eventScore,
        eventScoreModifier,
        spring,
        springModifier
      )
    )
    event.map(SuccessResponse(_))
  }

  /** Create an event if a event does not already exist with the same unique identifiers (name, timestamp, clientId, primaryEventId, and venueId)
    *
    * @param primaryEventId
    * @param name
    * @param timestamp
    * @param clientId
    * @param venueId
    * @param seasonId
    * @param isBroadcast
    * @param percentPriceModifier
    * @return existing record id if a matching record exists otherwise returns the created record id
    */
  def createUnique(
    primaryEventId: Option[Int] = None,
    name: String,
    timestamp: Option[Timestamp],
    clientId: Int,
    venueId: Int,
    eventCategoryId: Int,
    seasonId: Option[Int] = None,
    isBroadcast: Boolean = true,
    percentPriceModifier: Int = 0,
    eventScore: Option[Double] = None,
    eventScoreModifier: Double = 0,
    spring: Option[Double] = None,
    springModifier: Double = 0
  ): Future[ResponseCode] = {
    getUnique(name, timestamp, clientId, venueId, seasonId, primaryEventId, percentPriceModifier)
      .flatMap {
        case None =>
          create(
            primaryEventId,
            name,
            timestamp,
            clientId,
            venueId,
            eventCategoryId,
            seasonId,
            isBroadcast,
            percentPriceModifier,
            eventScore,
            eventScoreModifier,
            spring,
            springModifier
          )
        case Some(_) =>
          Future.successful(DuplicateResponse)
      }
  }

  /** Get an event by its unique identifiers
    *
    * @param name
    * @param timestamp
    * @param clientId
    * @param venueId
    * @param primaryEventId
    * @return event if it exists
    */
  def getUnique(
    name: String,
    timestamp: Option[Timestamp],
    clientId: Int,
    venueId: Int,
    seasonId: Option[Int] = None,
    primaryEventId: Option[Int] = None,
    percentPriceModifier: Int
  ): Future[Option[Event]] = {
    ctx.db.run(
      events
        .filter(e => {
          val timestampsMatch = (e.timestamp === timestamp) || (e.timestamp.?.isEmpty && timestamp.isEmpty)
          val seasonsMatch = (e.seasonId === seasonId) || (e.seasonId.?.isEmpty && seasonId.isEmpty)
          val integrationIdsMatch = (e.primaryEventId === primaryEventId) || (e.primaryEventId.?.isEmpty && primaryEventId.isEmpty)

          e.name === name &&
          timestampsMatch &&
          e.clientId === clientId &&
          e.venueId === venueId &&
          seasonsMatch &&
          integrationIdsMatch &&
          e.percentPriceModifier === percentPriceModifier
        })
        .result
        .headOption
    )
  }

  /** Bulk Insert events for onboarding script
    *
    * @param records event records
    * @return
    */
  def bulkInsert(records: Seq[Event]): Future[ResponseCode] = {
    val insert = events ++= records.map(
      _.copy(id = None, createdAt = DateHelper.now(), modifiedAt = DateHelper.now())
    )
    ctx.db.run(insert).map(SuccessResponse(_))
  }

  /** Get an event by its Id
    *
    * @param id
    * @return event if it exists
    */
  def getById(id: Int): Future[Option[Event]] = {
    ctx.db.run(events.filter(_.id === id).result.headOption)
  }

  /**
    * Get events by a client & venue
    * @param clientId of the client
    * @param venueId of the venue
    *
    * @return events for the client and venue
    */
  def getByClientAndVenue(clientId: Int, venueId: Int): Future[Seq[Event]] = {
    ctx.db.run(
      events
        .filter(_.clientId === clientId)
        .filter(_.venueId === venueId)
        .result
    )
  }

  /** Get events by their ID
    *
    * @param ids
    *
    * @return events with matching IDs
    */
  def getByIdBulk(ids: Set[Int]): Future[Seq[Event]] = {
    ctx.db.run(events.filter(_.id inSet ids).result)
  }

  /**
    * Get an event by its primaryEventId and clientId combo
    *
    * @param primaryEventId
    * @param clientId
    * @return
    */
  def getByIntegrationId(primaryEventId: Int, clientId: Int): Future[Option[Event]] = {
    ctx.db.run(
      events
        .filter(_.primaryEventId === primaryEventId)
        .filter(_.clientId === clientId)
        .result
        .headOption
    )
  }

  /** Updates an event
    *
    * @param id
    * @param name
    * @param timestamp
    * @param clientId
    * @param venueId
    * @param eventCategoryId
    * @param seasonId
    * @param percentPriceModifier
    * @return event if it exists
    */
  def updateUnique(
    id: Int,
    primaryEventId: Option[Int],
    name: String,
    timestamp: Option[Timestamp],
    clientId: Int,
    venueId: Int,
    eventCategoryId: Int,
    seasonId: Option[Int] = None,
    percentPriceModifier: Int = 0,
    eventScoreModifier: Double = 0,
    springModifier: Double = 0
  ): Future[ResponseCode] = {
    getById(id).flatMap {
      case None => Future.successful(NotFoundResponse)
      case Some(event) =>
        getUnique(
          name,
          timestamp,
          clientId,
          venueId,
          seasonId,
          primaryEventId,
          percentPriceModifier
        ).flatMap {
          case None =>
            update(
              Event(
                Some(id),
                primaryEventId,
                event.createdAt,
                event.modifiedAt,
                name,
                timestamp,
                clientId,
                venueId,
                eventCategoryId,
                seasonId,
                event.isBroadcast,
                percentPriceModifier,
                event.eventScore,
                eventScoreModifier,
                event.spring,
                springModifier
              )
            )
          case Some(e) =>
            if (e.id.get == id)
              Future.successful(SuccessResponse(e))
            else {
              Future.successful(DuplicateResponse)
            }
        }
      case _ => Future.successful(NotFoundResponse)
    }
  }

  /** Update an Event
    *
    * @param event
    * @return
    */
  def update(event: Event): Future[ResponseCode] = {
    val now = DateHelper.now()
    ctx.db.run(events.filter(_.id === event.id).update(event.copy(modifiedAt = now)).map {
      case 0 => NotFoundResponse
      case _ => SuccessResponse(event)
    })
  }

  /** Get all Events for a clientId, or all Events (for all clients?) if user is Admin
    *
    * @param clientId
    * @param isAdmin
    * @return
    */
  def getAll(clientId: Option[Int], isAdmin: Boolean): Future[Seq[Event]] = {
    ctx.db.run(
      events
        .filter(e => {
          e.clientId === clientId || (clientId.isEmpty && isAdmin)
        })
        .sortBy(e => e.timestamp)
        .result
    )
  }

  /**
    * Gets events for a client after a certain date.
    *
    * @param clientIds
    * @param after
    * @return
    */
  def getUpcomingEvents(
    clientIds: Seq[Int],
    after: Timestamp,
    before: Option[Timestamp] = None
  ): Future[Seq[Event]] = {
    ctx.db.run(
      events
        .filter(_.clientId.inSet(clientIds))
        .filter(_.timestamp >= after)
        .filterOpt(before)(_.timestamp <= _)
        .sortBy(_.timestamp)
        .result
    )
  }

  /**
    * Gets events for **all** client after a certain date
    * and an optional before date
    *
    * @param after
    * @param before
    * @return
    */
  def getUpcomingEventsForAllClients(
    after: Timestamp,
    before: Option[Timestamp] = None
  ): Future[Seq[Event]] = {
    ctx.db.run(
      events
        .filter(_.timestamp >= after)
        .filterOpt(before)(_.timestamp <= _)
        .sortBy(_.timestamp)
        .result
    )
  }

  /** Get all Events for a Season, in chronological order
    *
    * @param seasonId
    * @param clientId
    * @param isAdmin
    * @return
    */
  def getAllBySeason(seasonId: Int, clientId: Int, isAdmin: Boolean): Future[Seq[Event]] = {
    val eventsQuery = for {
      e <- events if e.seasonId === seasonId && (e.clientId === clientId || isAdmin == true)
    } yield e
    ctx.db.run(eventsQuery.sortBy(_.timestamp).result)
  }

  /** Toggle whether the event prices should be broadcast to ticket integrations
    *
    * @param eventId
    * @param isBroadcast
    * @return modified event fields
    */
  def toggleIsBroadcast(
    eventId: Int,
    isBroadcast: Boolean
  ): Future[Option[(Boolean, Timestamp)]] = {
    val now = DateHelper.now()
    val query = for {
      event <- events if event.id === eventId
    } yield (event.isBroadcast, event.modifiedAt)
    ctx.db.run(query.update(isBroadcast, now).map {
      case 0 => None
      case _ => Some(isBroadcast, now)
    })
  }

  /**
    * Updates the event score and spring based on the timestamp of the event
    *
    * @param id
    * @param eventScore
    * @param spring
    * @return
    */
  def updateFactors(id: Int, eventScore: Option[Double], spring: Option[Double]): Future[Int] = {
    val now = DateHelper.now()
    val q = events.filter(_.id === id).map(e => (e.eventScore.?, e.spring.?, e.modifiedAt))
    ctx.db.run(q.update((eventScore, spring, now)))
  }

  /**
    * Get an event by it's secondary event ID by looking it up in the ClientIntegrationEvents table
    *
    * @param clientId id of the client
    * @param integrationId id of the secondary integration
    * @param secondaryEventId id of the event in the secondary
    * @return an event record if it exists
    */
  def getBySecondaryEventId(
    clientId: Int,
    integrationId: Int,
    secondaryEventId: String
  ): Future[Option[Event]] = {
    val q = for {
      clientIntegration <- clientIntegrations
        .filter(_.integrationId === integrationId)
        .filter(_.clientId === clientId)
      clientIntegrationEvent <- clientIntegrationEvents.filter(
        _.integrationEventId === secondaryEventId
      ) if clientIntegrationEvent.clientIntegrationId === clientIntegration.id
      event <- events if event.id === clientIntegrationEvent.eventId
    } yield event

    ctx.db.run(q.result.headOption)
  }
}
