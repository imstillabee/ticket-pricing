package com.eventdynamic.services

import java.time.Instant

import com.eventdynamic.db.EDContext
import com.eventdynamic.models.TimeStat
import com.google.inject.{Inject, Singleton}
import slick.jdbc.GetResult

import scala.concurrent.Future

@Singleton
class EventTimeGroupsService @Inject()(ctx: EDContext) {
  import ctx.profile.api._

  implicit val getTimeStatResult: AnyRef with GetResult[TimeStat] = GetResult(
    r =>
      TimeStat(
        intervalStart = r.nextTimestamp(),
        intervalEnd = r.nextTimestamp(),
        revenue = r.nextBigDecimalOption(),
        inventory = r.nextIntOption(),
        cumulativeRevenue = r.nextBigDecimalOption(),
        cumulativeInventory = r.nextIntOption(),
        isProjected = r.nextBoolean()
    )
  )

  def getActualTimeStats(
    clientId: Int,
    from: Instant,
    to: Instant,
    totalInventory: Int = 0,
    seasonId: Option[Int] = None,
    eventId: Option[Int] = None
  ): Future[Vector[TimeStat]] = {
    if (eventId.isEmpty && seasonId.isEmpty) {
      throw new IllegalArgumentException(
        "A season or event filter must be specified for projected time stats"
      )
    }

    val query = sql"""
      WITH
        "ceiledTrans" as (
          SELECT
            etg."hourCeiledTimestamp" as "ceiledTimestamp",
            SUM(etg."revenue") as "revenue",
            SUM(etg."inventory") as "inventory"
          FROM "EventTimeGroups" etg
            INNER JOIN "Events" e on e.id = etg."eventId"
          WHERE
            e."clientId" = $clientId
            AND (${eventId.isEmpty} or e."id" = $eventId)
            AND (${seasonId.isEmpty} or e."seasonId" = $seasonId)
          GROUP BY "ceiledTimestamp"
        ),
        "intervals" as (
          SELECT
            n "intervalEnd",
            (n - interval '1 hour') as "intervalStart"
          FROM generate_series(
            LEAST($from::timestamptz, (select min("ceiledTimestamp") from "ceiledTrans")),
            GREATEST($to::timestamptz, (select max("ceiledTimestamp") from "ceiledTrans")),
            interval '1 hour'
          ) n
        ),
        "timeStats" as (
          SELECT
            i."intervalStart",
            i."intervalEnd",
            COALESCE(ct.revenue, 0),
            COALESCE(ct.inventory, 0),
            SUM(COALESCE(ct."revenue", 0)) OVER (ORDER BY i."intervalEnd") as "cumulativeRevenue",
            $totalInventory + SUM(COALESCE(ct."inventory", 0)) OVER (ORDER BY i."intervalEnd") as "cumulativeInventory",
            false as "isProjected"
          FROM
            "intervals" i
            LEFT JOIN "ceiledTrans" ct on ct."ceiledTimestamp" = i."intervalEnd"
          ORDER BY i."intervalEnd"
        )
        SELECT *
        FROM "timeStats"
          WHERE "intervalStart" >= $from and "intervalEnd" <= $to
     """.as[TimeStat]

    ctx.db.run(query)
  }
}
