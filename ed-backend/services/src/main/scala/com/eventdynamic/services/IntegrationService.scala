package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.Integration
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class IntegrationService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {

  import ctx.profile.api._

  /** Object to query table from  */
  val integrations = Tables.Integrations

  /**
    * Get all integrations
    *
    * @return integration if it exists
    */
  def getAll(): Future[Seq[Integration]] = {
    ctx.db.run(integrations.result)
  }

  /**
    * Get integration by id
    *
    * @param id
    * @return integration if it exists
    */
  def getById(id: Int): Future[Option[Integration]] = {
    ctx.db.run(integrations.filter(_.id === id).result.headOption)
  }

  /** Get integration by matching name
    *
    * @param name
    * @return
    */
  def getByName(name: String): Future[Option[Integration]] = {
    ctx.db.run(integrations.filter(_.name === name).result.headOption)
  }
}
