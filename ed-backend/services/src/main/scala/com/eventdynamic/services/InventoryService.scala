package com.eventdynamic.services

import com.eventdynamic.db.EDContext
import com.eventdynamic.models.Inventory

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

class InventoryService(
  val ctx: EDContext,
  val eventSeatService: EventSeatService,
  val eventService: EventService,
  val seatService: SeatService,
  val venueService: VenueService
) {
  import ctx.profile.api._

  /** Gets inventory for an event
    *
    * @param clientId
    * @param eventId: optional
    * @return
    */
  def getInventory(
    clientId: Int,
    eventId: Option[Int] = None,
    section: Option[String] = None
  ): Future[Seq[Inventory]] = {
    val joinQuery = for {
      e <- eventService.events
        .filter(_.clientId === clientId)
        .filter(_.isBroadcast === true)
        .filterOpt(eventId)(_.id === _)
      es <- eventSeatService.eventSeats.filter(_.isListed === true) if es.eventId === e.id
      s <- seatService.seats.filterOpt(section)(_.section === _) if s.id === es.seatId
    } yield
      (
        es.id,
        e.id,
        e.isBroadcast,
        s.section,
        s.row,
        es.isListed,
        s.seat,
        es.listedPrice,
        es.overridePrice
      )

    ctx.db
      .run(joinQuery.result)
      .map(
        _.map(
          tuple =>
            Inventory(
              Some(tuple._1),
              tuple._2,
              tuple._3,
              tuple._4,
              tuple._5,
              tuple._6,
              tuple._7,
              tuple._8,
              tuple._9,
              None
          )
        )
      )
  }
}
