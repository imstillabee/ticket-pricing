package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.JobConfig
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models.jobconfigs.JobConfigFormats
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}
import play.api.libs.json.{Json, Reads, Writes}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class JobConfigService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext)
    extends JobConfigFormats {
  // Import driver from database context
  import ctx.profile.api._
  import Tables.jobTypeColumnType

  /** Object to query table from */
  val jobs = Tables.Jobs
  val jobConfigs = Tables.JobConfigs

  /**
    * Get config
    *
    * @param clientId client ID
    * @param jobType job type
    * @param reads read definition for the config
    * @tparam T config type
    * @return Config or None if it can't be found
    */
  def getConfig[T](clientId: Int, jobType: JobType)(implicit reads: Reads[T]): Future[Option[T]] = {
    val query = jobConfigs
      .filter(_.clientId === clientId)
      .join(jobs)
      .on(_.jobId === _.id)
      .filter(_._2.jobType === jobType)
      .map(_._1)
      .map(_.json)
      .result
      .headOption

    ctx.db.run(query).map(_.map(json => Json.parse(json).as[T]))
  }

  /**
    * Get raw config
    *
    * @param clientId client ID
    * @param jobType job type
    * @return Config or None if it can't be found
    */
  def getRawConfig(clientId: Int, jobType: JobType): Future[Option[JobConfig]] = {
    val query = jobConfigs
      .filter(_.clientId === clientId)
      .join(jobs)
      .on(_.jobId === _.id)
      .filter(_._2.jobType === jobType)
      .map(_._1)
      .result
      .headOption

    ctx.db.run(query)
  }

  /**
    * Update config
    *
    * @param clientId client ID
    * @param jobType job type
    * @param config config value
    * @param writes write definition for the config
    * @tparam T config type
    * @return number of configs that were updated
    */
  def updateConfig[T](clientId: Int, jobType: JobType, config: T)(
    implicit writes: Writes[T]
  ): Future[Int] = {
    val json = Json.toJson(config).toString()

    val query = for {
      job <- jobs.filter(_.jobType === jobType).result.head
      config <- jobConfigs
        .filter(c => c.clientId === clientId && c.jobId === job.id)
        .map(c => (c.modifiedAt, c.json))
        .update((DateHelper.now(), json))
    } yield {
      config
    }
    ctx.db.run(query)
  }

  /**
    * Create config
    *
    * @param clientId client ID
    * @param jobType job type
    * @param config config value
    * @param writes write definition for the config
    * @tparam T config type
    * @return the created job config
    */
  def createConfig[T](clientId: Int, jobType: JobType, config: T)(
    implicit writes: Writes[T]
  ): Future[JobConfig] = {
    val json = Json.toJson(config).toString()
    val now = DateHelper.now()

    val query = for {
      job <- jobs
        .filter(_.jobType === jobType)
        .result
        .head

      jobConfig = JobConfig(None, now, now, clientId, job.id.get, json)

      jobQuery <- jobConfigs returning jobConfigs
        .map(_.id) into ((c, id) => c.copy(id = Some(id))) += jobConfig
    } yield {
      jobQuery
    }
    ctx.db.run(query)
  }

  /**
    * Delete config
    *
    * @param clientId client ID
    * @param jobType job type
    * @return number of deleted configs
    */
  def deleteConfig(clientId: Int, jobType: JobType): Future[Int] = {
    val query = for {
      job <- jobs.filter(_.jobType === jobType).result.head
      config <- jobConfigs
        .filter(c => c.clientId === clientId && c.jobId === job.id.get)
        .delete
    } yield {
      config
    }

    ctx.db.run(query)
  }
}
