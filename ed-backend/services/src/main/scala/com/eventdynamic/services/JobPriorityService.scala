package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.JobPriority
import com.eventdynamic.models.JobPriorityType.JobPriorityType
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class JobPriorityService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  import ctx.profile.api._

  import Tables.jobPriorityTypeColumnType

  val jobPriorities = Tables.JobPriorities

  def getByJobPriorityType(jobPriorityType: JobPriorityType): Future[Option[JobPriority]] = {
    ctx.db.run(jobPriorities.filter(_.name === jobPriorityType).result.headOption)
  }
}
