package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.JobTrigger
import com.eventdynamic.models.JobTriggerType.JobTriggerType
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class JobTriggerService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  import ctx.profile.api._

  import Tables.jobTriggerTypeColumnType

  /** Object to query table from */
  val jobTriggers = Tables.JobTriggers

  /**
    * Get job trigger by its Id
    * @param id
    * @return
    */
  def getById(id: Int): Future[Option[JobTrigger]] = {
    ctx.db.run(jobTriggers.filter(_.id === id).result.headOption)
  }

  def getByTriggerType(triggerType: JobTriggerType): Future[Option[JobTrigger]] = {
    ctx.db.run(
      jobTriggers
        .filter(_.triggerType === triggerType)
        .result
        .headOption
    )
  }

  def getAll(): Future[Seq[JobTrigger]] = {
    ctx.db.run(jobTriggers.result)
  }
}
