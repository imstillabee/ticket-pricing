package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.{MLBAMScheduleToEvent, MLBAMSectionToPriceScale, MLBAMTeamToClient}
import com.google.inject.{Inject, Singleton}
import com.eventdynamic.utils._

import scala.concurrent.{ExecutionContext, Future}
import org.slf4j.LoggerFactory

@Singleton
class MLBAMMappingService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {

  import ctx.profile.api._

  private val logger = LoggerFactory.getLogger(this.getClass)

  val mlbamTeamToClient = Tables.MLBAMTeamToClientMap
  val mlbamSectionToPriceScale = Tables.MLBAMSectionToPriceScaleMap
  val mlbamScheduleToEvent = Tables.MLBAMScheduleToEventMap

  def createTeamClientMap(teamId: Int, clientId: Int): Future[ResponseCode] = {
    val teamClientMap = ctx.db.run(
      mlbamTeamToClient returning mlbamTeamToClient
        .map(_.id) into ((mapping, id) => mapping.copy(id = Some(id))) += MLBAMTeamToClient(
        None,
        clientId,
        teamId
      )
    )
    teamClientMap.map(SuccessResponse(_))
  }

  def createSectionPriceScaleMap(sectionId: Int, priceScaleId: Int): Future[ResponseCode] = {
    val sectionPriceScaleMap = ctx.db.run(
      mlbamSectionToPriceScale returning mlbamSectionToPriceScale
        .map(_.id) into ((mapping, id) => mapping.copy(id = Some(id))) += MLBAMSectionToPriceScale(
        None,
        priceScaleId,
        sectionId
      )
    )
    sectionPriceScaleMap.map(SuccessResponse(_))
  }

  def createScheduleEventMap(scheduleId: Int, eventId: Int): Future[ResponseCode] = {
    val scheduleEventMap = ctx.db.run(
      mlbamScheduleToEvent returning mlbamScheduleToEvent
        .map(_.id) into ((mapping, id) => mapping.copy(id = Some(id))) += MLBAMScheduleToEvent(
        None,
        eventId,
        scheduleId
      )
    )
    scheduleEventMap.map(SuccessResponse(_))
  }

  def getTeamIdByClientId(clientId: Int): Future[Option[Int]] = {
    ctx.db
      .run(
        mlbamTeamToClient
          .filter(_.clientId === clientId)
          .result
          .headOption
      )
      .map(_.map(_.teamId))
  }

  def getSectionIdByPriceScaleId(priceScaleId: Int): Future[Option[Int]] = {
    ctx.db
      .run(
        mlbamSectionToPriceScale
          .filter(_.priceScaleId === priceScaleId)
          .result
          .headOption
      )
      .map(_.map(_.sectionId))
  }

  def getScheduleIdByEventId(eventId: Int): Future[Option[Int]] = {
    ctx.db
      .run(
        mlbamScheduleToEvent
          .filter(_.eventId === eventId)
          .result
          .headOption
      )
      .map(_.map(_.scheduleId))
  }
}
