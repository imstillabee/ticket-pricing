package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.MLSEventStat
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class MLSEventStatService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val mlsEventStats = Tables.MLSEventStats
  val events = Tables.Events

  /** Create an MLSEventStat
    *
    * @param eventId
    * @param opponent
    * @param gameNumber
    * @return created MLSEventStat id
    */
  def create(eventId: Int, opponent: String, gameNumber: Int): Future[MLSEventStat] = {
    val now = DateHelper.now()

    ctx.db.run(
      mlsEventStats returning mlsEventStats
        .map(_.id) into ((mlsEventStat, id) => mlsEventStat.copy(id = Some(id))) += MLSEventStat(
        None,
        now,
        now,
        eventId,
        opponent,
        gameNumber
      )
    )
  }

  /** Get an MLSEventStat by id
    *
    * @param id
    * @return MLSEventStat if it exists
    */
  def getById(id: Int): Future[Option[MLSEventStat]] = {
    ctx.db.run(mlsEventStats.filter(_.id === id).result.headOption)
  }

  /** Get an MLSEventStats collection by the client id
    *
    * @param id
    * @return MLSEventStat collection if it exists
    */
  def getByClientId(id: Int): Future[Seq[MLSEventStat]] = {
    ctx.db.run(
      events.filter(_.clientId === id).join(mlsEventStats).on(_.id === _.eventId).map(_._2).result
    )
  }

  /** Get MLSEventStats by event id
    *
    * @param eventId
    * @return MLSEventStat collection if it exists
    */
  def getByEventId(eventId: Int): Future[Option[MLSEventStat]] = {
    ctx.db.run(
      mlsEventStats
        .filter(_.eventId === eventId)
        .result
        .headOption
    )
  }

  /**
    * Get MLSEventStats by a list of eventIds
    *
    * @param eventIds
    * @return a sequence of MLSEventStats
    */
  def getByEventIds(eventIds: Seq[Int]): Future[Seq[MLSEventStat]] =
    ctx.db.run(mlsEventStats.filter(_.eventId.inSet(eventIds)).result)

  /**
    * Upserts the passed event stats object.  Determines which based on empty id.
    *
    * @param mlsEventStat new or existing MLSEventStat
    * @return the saved event stat, or an exception if updating non-existant
    */
  def upsert(mlsEventStat: MLSEventStat): Future[MLSEventStat] = {
    if (mlsEventStat.id.isEmpty) {
      create(mlsEventStat.eventId, mlsEventStat.opponent, mlsEventStat.homeGameNumber)
    } else {
      val now = DateHelper.now()
      ctx.db.run(
        mlsEventStats
          .filter(_.id === mlsEventStat.id)
          .update(mlsEventStat.copy(modifiedAt = now))
          .map {
            case 0 =>
              throw new Exception(s"Unable to update MLSEventStats with id ${mlsEventStat.id.get}")
            case _ => mlsEventStat
          }
      )
    }
  }
}
