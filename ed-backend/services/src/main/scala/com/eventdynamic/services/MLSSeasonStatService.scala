package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.MLSSeasonStat
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class MLSSeasonStatService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val mlsSeasonStats = Tables.MLSSeasonStats
  val seasons = Tables.Seasons

  /** Create an MLSSeasonStat
    *
    * @param seasonId
    * @param wins
    * @param losses
    * @param ties
    * @param gamesTotal
    * @return created MLSSeasonStat id
    */
  def create(
    seasonId: Int,
    wins: Int,
    losses: Int,
    ties: Int,
    gamesTotal: Int
  ): Future[MLSSeasonStat] = {
    val now = DateHelper.now()

    ctx.db.run(
      mlsSeasonStats returning mlsSeasonStats
        .map(_.id) into ((mlsSeasonStat, id) => mlsSeasonStat.copy(id = Some(id))) += MLSSeasonStat(
        None,
        now,
        now,
        seasonId,
        wins,
        losses,
        ties,
        gamesTotal
      )
    )
  }

  /** Get an MLSSeasonStat by id
    *
    * @param id
    * @return MLSSeasonStat if it exists
    */
  def getById(id: Int): Future[Option[MLSSeasonStat]] = {
    ctx.db.run(mlsSeasonStats.filter(_.id === id).result.headOption)
  }

  /** Get an MLSSeasonStats collection by the client id
    *
    * @param id
    * @return MLSSeasonStat collection if it exists
    */
  def getByClientId(id: Int): Future[Seq[MLSSeasonStat]] = {
    ctx.db.run(
      seasons
        .filter(_.clientId === id)
        .join(mlsSeasonStats)
        .on(_.id === _.seasonId)
        .map(_._2)
        .result
    )
  }

  /** Get MLSSeasonStats by season id
    *
    * @param seasonId
    * @return MLSSeasonStat collection if it exists
    */
  def getBySeasonId(seasonId: Int): Future[Option[MLSSeasonStat]] = {
    ctx.db.run(
      mlsSeasonStats
        .filter(_.seasonId === seasonId)
        .result
        .headOption
    )
  }

  /**
    * Gets MLSSeasonStats for multiple seasons
    *
    * @param seasonIds
    * @return all MLSSeasonStats for the passed in IDs
    */
  def getBySeasonIds(seasonIds: Seq[Int]): Future[Seq[MLSSeasonStat]] =
    ctx.db.run(mlsSeasonStats.filter(_.seasonId.inSet(seasonIds)).result)

  /**
    * Creates or updates the passed season stats object.  Determines which based on empty id.
    *
    * @param mlsSeasonStat new or existing MLSSeasonStat
    * @return the saved season stat, or an exception if updating non-existant
    */
  def createOrUpdate(mlsSeasonStat: MLSSeasonStat): Future[MLSSeasonStat] = {
    if (mlsSeasonStat.id.isEmpty) {
      create(
        mlsSeasonStat.seasonId,
        mlsSeasonStat.wins,
        mlsSeasonStat.losses,
        mlsSeasonStat.ties,
        mlsSeasonStat.gamesTotal
      )
    } else {
      val now = DateHelper.now()
      ctx.db.run(
        mlsSeasonStats
          .filter(_.id === mlsSeasonStat.id)
          .update(mlsSeasonStat.copy(modifiedAt = now))
          .map {
            case 0 =>
              throw new Exception(
                s"Unable to update MLSSeasonStats with id ${mlsSeasonStat.id.get}"
              )
            case _ => mlsSeasonStat
          }
      )
    }
  }
}
