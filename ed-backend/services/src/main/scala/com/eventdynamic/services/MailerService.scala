package com.eventdynamic.services

import com.amazonaws.regions.Regions
import com.amazonaws.services.simpleemail._
import com.amazonaws.services.simpleemail.model._
import com.eventdynamic.models.MailerConfig
import com.eventdynamic.services.TCode.TCode
import com.google.inject.Singleton
import org.slf4j.LoggerFactory

import scala.io.Source

object TCode extends Enumeration {
  type TCode = Value
  val new_user = Value("new_user")
  val reset_pass = Value("reset_pass")
}

class FailedToSendEmailException(message: String) extends Exception(message) {}

@Singleton
class MailerService(val conf: MailerConfig) {
  def this() = this(MailerConfig.default)

  private val logger = LoggerFactory.getLogger(this.getClass)

  private val client: AmazonSimpleEmailService = AmazonSimpleEmailServiceClientBuilder
    .standard()
    // Replace US_WEST_2 with the AWS Region you're using for
    // Amazon SES.
    .withRegion(Regions.US_EAST_1)
    .build()

  @throws(classOf[FailedToSendEmailException])
  def send(templateCode: TCode, data: Map[String, String]): Unit = {
    if (!conf.mailEnabled) {
      logger.warn("Email is not enabled")
      return
    }
    logger.info(s"Starting a ${templateCode} email process")
    try {
      getRecipientAddress(data) match {
        case Some(destination) => {
          val confData = Map(
            "visitLink" -> conf.edSiteLink,
            "supportEmail" -> conf.supportEmail,
            "tempPasswordExpiration" -> conf.userTempPasswordExpiration,
            "resetPasswordExpiration" -> conf.userResetPasswordExpiration
          )
          val request: SendEmailRequest = new SendEmailRequest()
            .withDestination(destination)
            .withMessage(buildMessage(templateCode, data ++ confData))
            .withSource(conf.fromEmail)

          val result: SendEmailResult = client.sendEmail(request)
          logger.info(s"Email sent! Message ID: ${result.getMessageId}")
        }
        case None => {
          throw new Exception("Missing destination email")
        }
      }
    } catch {
      case ex: Throwable => {
        logger.error("Email was not sent.", ex)
        throw new FailedToSendEmailException(ex.getMessage)
      }
    }
  }

  private def buildMessage(templateCode: TCode, data: Map[String, String]): Message = {
    val body = generateBodyFromTemplate(templateCode, data)
    new Message().withBody(body).withSubject(getSubject(templateCode))
  }

  private def generateBodyFromTemplate(templateCode: TCode, data: Map[String, String]): Body = {
    val content = new Content().withCharset("UTF-8").withData(template(templateCode, data))
    new Body().withHtml(content)
  }

  private def getSubject(templateCode: TCode): Content = {
    val content = new Content().withCharset("UTF-8")
    val data = templateCode match {
      case TCode.new_user   => "Welcome to Event Dynamic"
      case TCode.reset_pass => "Requested Event Dynamic Password Reset"
      case _                => throw new Exception("Unrecognized Template")
    }
    content.withData(data)
  }

  /** Wrap provided info with separate template file - like a mail-merge
    *
    * @param templateCode   template 'short-name' used as html-file name, and send method argument
    * @param data           Map of string keys to string values to be merged into template
    * @return               string bodyContent complete with provided inputs 'merged' into template
    */
  private def template(templateCode: TCode, data: Map[String, String]): String = {
    val filePath = s"api/email-templates/$templateCode.html"
    logger.trace(s"in template - filePath is $filePath")

    // read the email message body content from the template file
    var fC = Source.fromFile(filePath).getLines.mkString

    logger.trace(s"in template - template fileContents is $fC")

    // replace all the mail-merge placeholders in the body with appropriate strings from data
    for ((k, v) <- data) { fC = fC.replaceAll(s"##${k}##", v) }

    logger.trace(s"in template - finalContent is $fC")

    fC
  }

  /** gets recipient address and returns a destination
    *
    * @param data           Map of string keys to string values to be merged into template
    * @return               Some[Destination] if email is in whitelist or if whitelist is empty and None if email not in whitelist
    */
  private def getRecipientAddress(data: Map[String, String]): Option[Destination] = {
    val mergeData = data.withDefaultValue("Not Found")

    val toEmail = mergeData("toEmail")
    if (conf.whitelist.isEmpty || (conf.whitelist.contains(toEmail.split("@")(1).toLowerCase())))
      Some(new Destination().withToAddresses(toEmail))
    else {
      logger.info("Email is not contained within the whitelist")
      None
    }
  }
}
