package com.eventdynamic.services

import com.eventdynamic.db.EDContext
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class MaterializedViewService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  import ctx.profile.api._

  def refreshAll(): Future[Seq[Vector[String]]] = {
    Future.sequence(
      Seq(
        refreshEventTimeGroups(),
        refreshEventRevenueGroups(),
        refreshEventIntegrationGroups(),
        refreshEventSeatCounts()
      )
    )
  }

  def refreshEventTimeGroups(): Future[Vector[String]] = {
    ctx.db
      .run(sql"""REFRESH MATERIALIZED VIEW CONCURRENTLY "EventTimeGroups"""".as[String])
  }

  def refreshEventRevenueGroups(): Future[Vector[String]] = {
    ctx.db
      .run(sql"""REFRESH MATERIALIZED VIEW CONCURRENTLY "EventRevenueGroups"""".as[String])
  }

  def refreshEventIntegrationGroups(): Future[Vector[String]] = {
    ctx.db
      .run(sql"""REFRESH MATERIALIZED VIEW CONCURRENTLY "EventIntegrationGroups"""".as[String])
  }

  def refreshEventSeatCounts(): Future[Vector[String]] = {
    ctx.db
      .run(sql"""REFRESH MATERIALIZED VIEW CONCURRENTLY "EventSeatCounts"""".as[String])
  }
}
