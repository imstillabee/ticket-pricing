package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.NCAABEventStat
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class NCAABEventStatService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val ncaabEventStats = Tables.NCAABEventStats
  val events = Tables.Events

  /** Create an NCAABEventStat
    *
    * @param eventId
    * @param opponent
    * @param isHomeOpener
    * @param isPreSeason
    * @return created NCAABEventStat id
    */

  def create(
    eventId: Int,
    opponent: String,
    isHomeOpener: Boolean,
    isPreSeason: Boolean
  ): Future[NCAABEventStat] = {
    val now = DateHelper.now()

    ctx.db.run(
      ncaabEventStats returning ncaabEventStats
        .map(_.id) into ((ncaabEventStat, id) => ncaabEventStat.copy(id = Some(id))) += NCAABEventStat(
        None,
        now,
        now,
        eventId,
        opponent,
        isHomeOpener,
        isPreSeason
      )
    )
  }

  /** Get an NCAABEventStat by id
    *
    * @param id
    * @return NCAABEventStat if it exists
    */
  def getById(id: Int): Future[Option[NCAABEventStat]] = {
    ctx.db.run(ncaabEventStats.filter(_.id === id).result.headOption)
  }

  /** Get NCAABEventStats by event id
    *
    * @param eventId
    * @return NCAABEventStat collection if it exists
    */
  def getByEventId(eventId: Int): Future[Option[NCAABEventStat]] = {
    ctx.db.run(
      ncaabEventStats
        .filter(_.eventId === eventId)
        .result
        .headOption
    )
  }

  /** Get an NCAABEventStats collection by the client id
    *
    * @param id
    * @return NCAABEventStat collection if it exists
    */
  def getByClientId(id: Int): Future[Seq[NCAABEventStat]] = {
    ctx.db.run(
      events.filter(_.clientId === id).join(ncaabEventStats).on(_.id === _.eventId).map(_._2).result
    )
  }

  /**
    * Get NCAABEventStats by a list of eventIds
    *
    * @param eventIds
    * @return a sequence of NCAABEventStats
    */
  def getByEventIds(eventIds: Seq[Int]): Future[Seq[NCAABEventStat]] =
    ctx.db.run(ncaabEventStats.filter(_.eventId.inSet(eventIds)).result)

  /**
    * Upserts the passed event stats object.  Determines which based on empty id.
    *
    * @param ncaabEventStat new or existing NCAABEventStat
    * @return the id of the saved event stat, or an exception if updating non-existant
    */
  def upsert(ncaabEventStat: NCAABEventStat): Future[Int] = {
    val now = DateHelper.now

    val query = for {
      existing <- ncaabEventStats
        .filterIf(ncaabEventStat.id.isDefined)(_.id === ncaabEventStat.id.get)
        .filter(_.eventId === ncaabEventStat.eventId)
        .result
        .headOption

      res <- (ncaabEventStats returning ncaabEventStats.map(_.id)).insertOrUpdate(
        ncaabEventStat
          .copy(
            id = existing.flatMap(_.id),
            createdAt = existing.map(_.createdAt).getOrElse(now),
            modifiedAt = now
          )
      )
    } yield existing.flatMap(_.id).getOrElse(res.get)

    ctx.db.run(query)
  }
}
