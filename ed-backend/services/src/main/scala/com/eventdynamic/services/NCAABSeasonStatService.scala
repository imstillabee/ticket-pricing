package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.NCAABSeasonStat
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class NCAABSeasonStatService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val ncaabSeasonStats = Tables.NCAABSeasonStats
  val seasons = Tables.Seasons

  /** Create an NCAABSeasonStat
    *
    * @param seasonId
    * @param wins
    * @param losses
    * @param gamesTotal
    * @return created NCAABSeasonStat id
    */
  def create(seasonId: Int, wins: Int, losses: Int, gamesTotal: Int): Future[NCAABSeasonStat] = {
    val now = DateHelper.now()

    ctx.db.run(
      ncaabSeasonStats returning ncaabSeasonStats
        .map(_.id) into ((ncaabSeasonStat, id) => ncaabSeasonStat.copy(id = Some(id))) += NCAABSeasonStat(
        None,
        now,
        now,
        seasonId,
        wins,
        losses,
        gamesTotal
      )
    )
  }

  /** Get an NCAABSeasonStat by id
    *
    * @param id
    * @return NCAABSeasonStat if it exists
    */
  def getById(id: Int): Future[Option[NCAABSeasonStat]] = {
    ctx.db.run(ncaabSeasonStats.filter(_.id === id).result.headOption)
  }

  /** Get NCAABSeasonStats by season id
    *
    * @param seasonId
    * @return NCAABSeasonStat collection if it exists
    */
  def getBySeasonId(seasonId: Int): Future[Option[NCAABSeasonStat]] = {
    ctx.db.run(
      ncaabSeasonStats
        .filter(_.seasonId === seasonId)
        .result
        .headOption
    )
  }

  /**
    * Gets NCAABSeasonStats for multiple seasons
    *
    * @param seasonIds
    * @return all NCAABSeasonStats for the passed in IDs
    */
  def getBySeasonIds(seasonIds: Seq[Int]): Future[Seq[NCAABSeasonStat]] =
    ctx.db.run(ncaabSeasonStats.filter(_.seasonId.inSet(seasonIds)).result)

  /** Get an MLSSeasonStats collection by the client id
    *
    * @param id
    * @return MLSSeasonStat collection if it exists
    */
  def getByClientId(id: Int): Future[Seq[NCAABSeasonStat]] = {
    ctx.db.run(
      seasons
        .filter(_.clientId === id)
        .join(ncaabSeasonStats)
        .on(_.id === _.seasonId)
        .map(_._2)
        .result
    )
  }

  /**
    * Creates or updates the passed season stats object.  Determines which based on empty id.
    *
    * @param ncaabSeasonStat new or existing NCAABSeasonStat
    * @return the id of the saved season stat, or an exception if updating non-existant
    */
  def upsert(ncaabSeasonStat: NCAABSeasonStat): Future[Int] = {
    val now = DateHelper.now()

    val query = for {

      existing <- ncaabSeasonStats
        .filterIf(ncaabSeasonStat.id.isDefined)(_.id === ncaabSeasonStat.id.get)
        .filter(_.seasonId === ncaabSeasonStat.seasonId)
        .result
        .headOption

      res <- (ncaabSeasonStats returning ncaabSeasonStats.map(_.id)).insertOrUpdate(
        ncaabSeasonStat
          .copy(
            id = existing.flatMap(_.id),
            createdAt = existing.map(_.createdAt).getOrElse(now),
            modifiedAt = now
          )
      )
    } yield existing.flatMap(_.id).getOrElse(res.get)

    ctx.db.run(query)
  }
}
