package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.NCAAFEventStat
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class NCAAFEventStatService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val ncaafEventStats = Tables.NCAAFEventStats
  val events = Tables.Events

  /** Create an NCAAFEventStat
    *
    * @param eventId
    * @param opponent
    * @param isHomeOpener
    * @param isPreSeason
    * @return created NCAAFEventStat id
    */
  def create(
    eventId: Int,
    opponent: String,
    isHomeOpener: Boolean,
    isPreSeason: Boolean
  ): Future[NCAAFEventStat] = {
    val now = DateHelper.now()

    ctx.db.run(
      ncaafEventStats returning ncaafEventStats
        .map(_.id) into ((ncaafEventStat, id) => ncaafEventStat.copy(id = Some(id))) += NCAAFEventStat(
        None,
        now,
        now,
        eventId,
        opponent,
        isHomeOpener,
        isPreSeason
      )
    )
  }

  /** Get an NCAAFEventStat by id
    *
    * @param id
    * @return NCAAFEventStat if it exists
    */
  def getById(id: Int): Future[Option[NCAAFEventStat]] = {
    ctx.db.run(ncaafEventStats.filter(_.id === id).result.headOption)
  }

  /** Get NCAAFEventStats by event id
    *
    * @param eventId
    * @return NCAAFEventStat collection if it exists
    */
  def getByEventId(eventId: Int): Future[Option[NCAAFEventStat]] = {
    ctx.db.run(
      ncaafEventStats
        .filter(_.eventId === eventId)
        .result
        .headOption
    )
  }

  /** Get an NCAAFEventStats collection by the client id
    *
    * @param id
    * @return NCAABEventStat collection if it exists
    */
  def getByClientId(id: Int): Future[Seq[NCAAFEventStat]] = {
    ctx.db.run(
      events.filter(_.clientId === id).join(ncaafEventStats).on(_.id === _.eventId).map(_._2).result
    )
  }

  /**
    * Get NCAAFEventStats by a list of eventIds
    *
    * @param eventIds
    * @return a sequence of NCAAFEventStats
    */
  def getByEventIds(eventIds: Seq[Int]): Future[Seq[NCAAFEventStat]] =
    ctx.db.run(ncaafEventStats.filter(_.eventId.inSet(eventIds)).result)

  /**
    * Upserts the passed event stats object.  Determines which based on empty id.
    *
    * @param ncaafEventStat new or existing NCAAFEventStat
    * @return the id of the saved event stat, or an exception if updating non-existant
    */
  def upsert(ncaafEventStat: NCAAFEventStat): Future[Int] = {
    val now = DateHelper.now

    val query = for {
      existing <- ncaafEventStats
        .filterIf(ncaafEventStat.id.isDefined)(_.id === ncaafEventStat.id.get)
        .filter(_.eventId === ncaafEventStat.eventId)
        .result
        .headOption

      res <- (ncaafEventStats returning ncaafEventStats.map(_.id)).insertOrUpdate(
        ncaafEventStat
          .copy(
            id = existing.flatMap(_.id),
            createdAt = existing.map(_.createdAt).getOrElse(now),
            modifiedAt = now
          )
      )
    } yield existing.flatMap(_.id).getOrElse(res.get)

    ctx.db.run(query)
  }
}
