package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.NCAAFSeasonStat
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class NCAAFSeasonStatService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context import ctx.profile.api._
  import ctx.profile.api._

  /** Object to query table from */
  val ncaafSeasonStats = Tables.NCAAFSeasonStats
  val seasons = Tables.Seasons

  /** Create an NCAAFSeasonStat
    *
    * @param seasonId
    * @param wins
    * @param losses
    * @param gamesTotal
    * @return created NCAAFSeasonStat id
    */
  def create(seasonId: Int, wins: Int, losses: Int, gamesTotal: Int): Future[NCAAFSeasonStat] = {
    val now = DateHelper.now()

    ctx.db.run(
      ncaafSeasonStats returning ncaafSeasonStats
        .map(_.id) into ((ncaafSeasonStat, id) => ncaafSeasonStat.copy(id = Some(id))) += NCAAFSeasonStat(
        None,
        now,
        now,
        seasonId,
        wins,
        losses,
        gamesTotal
      )
    )
  }

  /** Get an NCAAFSeasonStat by id
    *
    * @param id
    * @return NCAAFSeasonStat if it exists
    */
  def getById(id: Int): Future[Option[NCAAFSeasonStat]] = {
    ctx.db.run(ncaafSeasonStats.filter(_.id === id).result.headOption)
  }

  /** Get an MLSSeasonStats collection by the client id
    *
    * @param id
    * @return MLSSeasonStat collection if it exists
    */
  def getByClientId(id: Int): Future[Seq[NCAAFSeasonStat]] = {
    ctx.db.run(
      seasons
        .filter(_.clientId === id)
        .join(ncaafSeasonStats)
        .on(_.id === _.seasonId)
        .map(_._2)
        .result
    )
  }

  /** Get NCAAFSeasonStats by season id
    *
    * @param seasonId
    * @return NCAAFSeasonStat collection if it exists
    */
  def getBySeasonId(seasonId: Int): Future[Option[NCAAFSeasonStat]] = {
    ctx.db.run(
      ncaafSeasonStats
        .filter(_.seasonId === seasonId)
        .result
        .headOption
    )
  }

  /**
    * Gets NCAAFSeasonStats for multiple seasons
    *
    * @param seasonIds
    * @return all NCAAFSeasonStats for the passed in IDs
    */
  def getBySeasonIds(seasonIds: Seq[Int]): Future[Seq[NCAAFSeasonStat]] =
    ctx.db.run(ncaafSeasonStats.filter(_.seasonId.inSet(seasonIds)).result)

  /**
    * Creates or updates the passed season stats object.  Determines which based on empty id.
    *
    * @param ncaafSeasonStat new or existing NCAAFSeasonStat
    * @return the saved season stat, or an exception if updating non-existant
    */
  def upsert(ncaafSeasonStat: NCAAFSeasonStat): Future[Int] = {
    val now = DateHelper.now()

    val query = for {

      existing <- ncaafSeasonStats
        .filterIf(ncaafSeasonStat.id.isDefined)(_.id === ncaafSeasonStat.id.get)
        .filter(_.seasonId === ncaafSeasonStat.seasonId)
        .result
        .headOption

      res <- (ncaafSeasonStats returning ncaafSeasonStats.map(_.id)).insertOrUpdate(
        ncaafSeasonStat
          .copy(
            id = existing.flatMap(_.id),
            createdAt = existing.map(_.createdAt).getOrElse(now),
            modifiedAt = now
          )
      )
    } yield existing.flatMap(_.id).getOrElse(res.get)

    ctx.db.run(query)
  }
}
