package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.NFLEventStat
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class NFLEventStatService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val nflEventStats = Tables.NFLEventStats

  /** Create an NFLEventStat
    *
    * @param clientId
    * @param eventId
    * @param opponent
    * @param isHomeOpener
    * @param isPreSeason
    * @return created NFLEventStat id
    */
  def create(
    clientId: Int,
    eventId: Int,
    opponent: String,
    isHomeOpener: Boolean,
    isPreSeason: Boolean
  ): Future[NFLEventStat] = {
    val now = DateHelper.now()

    ctx.db.run(
      nflEventStats returning nflEventStats
        .map(_.id) into ((nflEventStat, id) => nflEventStat.copy(id = Some(id))) += NFLEventStat(
        None,
        now,
        now,
        clientId,
        eventId,
        opponent,
        isHomeOpener,
        isPreSeason
      )
    )
  }

  /** Get an NFLEventStat by id
    *
    * @param id
    * @return NFLEventStat if it exists
    */
  def getById(id: Int): Future[Option[NFLEventStat]] = {
    ctx.db.run(nflEventStats.filter(_.id === id).result.headOption)
  }

  /** Get an NFLEventStats collection by the client id
    *
    * @param id
    * @return NFLEventStat collection if it exists
    */
  def getByClientId(id: Int): Future[Seq[NFLEventStat]] = {
    ctx.db.run(nflEventStats.filter(_.clientId === id).result)
  }

  /** Get NFLEventStats by event id
    *
    * @param eventId
    * @return NFLEventStat collection if it exists
    */
  def getByEventId(eventId: Int): Future[Option[NFLEventStat]] = {
    ctx.db.run(
      nflEventStats
        .filter(_.eventId === eventId)
        .result
        .headOption
    )
  }

  /**
    * Get NFLEventStats by a list of eventIds
    *
    * @param eventIds
    * @return a sequence of NFLEventStats
    */
  def getByEventIds(eventIds: Seq[Int]): Future[Seq[NFLEventStat]] =
    ctx.db.run(nflEventStats.filter(_.eventId.inSet(eventIds)).result)

  /**
    * Upserts the passed event stats object.  Determines which based on empty id.
    *
    * @param nflEventStat new or existing NFLEventStat
    * @return the saved event stat, or an exception if updating non-existant
    */
  def upsert(nflEventStat: NFLEventStat): Future[NFLEventStat] = {
    if (nflEventStat.id.isEmpty) {
      create(
        nflEventStat.clientId,
        nflEventStat.eventId,
        nflEventStat.opponent,
        nflEventStat.isHomeOpener,
        nflEventStat.isPreSeason
      )
    } else {
      val now = DateHelper.now()
      ctx.db.run(
        nflEventStats
          .filter(_.id === nflEventStat.id)
          .update(nflEventStat.copy(modifiedAt = now))
          .map {
            case 0 =>
              throw new Exception(s"Unable to update NFLEventStats with id ${nflEventStat.id.get}")
            case _ => nflEventStat
          }
      )
    }
  }
}
