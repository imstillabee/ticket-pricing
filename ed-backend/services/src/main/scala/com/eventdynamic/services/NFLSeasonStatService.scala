package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.NFLSeasonStat
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class NFLSeasonStatService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val nflSeasonStats = Tables.NFLSeasonStats

  /** Create an NFLSeasonStat
    *
    * @param clientId
    * @param seasonId
    * @param wins
    * @param losses
    * @param ties
    * @param gamesTotal
    * @return created NFLSeasonStat id
    */
  def create(
    clientId: Int,
    seasonId: Int,
    wins: Int,
    losses: Int,
    ties: Int,
    gamesTotal: Int
  ): Future[NFLSeasonStat] = {
    val now = DateHelper.now()

    ctx.db.run(
      nflSeasonStats returning nflSeasonStats
        .map(_.id) into ((nflSeasonStat, id) => nflSeasonStat.copy(id = Some(id))) += NFLSeasonStat(
        None,
        now,
        now,
        clientId,
        seasonId,
        wins,
        losses,
        ties,
        gamesTotal
      )
    )
  }

  /** Get an NFLSeasonStat by id
    *
    * @param id
    * @return NFLSeasonStat if it exists
    */
  def getById(id: Int): Future[Option[NFLSeasonStat]] = {
    ctx.db.run(nflSeasonStats.filter(_.id === id).result.headOption)
  }

  /** Get an NFLSeasonStats collection by the client id
    *
    * @param id
    * @return NFLSeasonStat collection if it exists
    */
  def getByClientId(id: Int): Future[Seq[NFLSeasonStat]] = {
    ctx.db.run(nflSeasonStats.filter(_.clientId === id).result)
  }

  /** Get NFLSeasonStats by season id
    *
    * @param seasonId
    * @return NFLSeasonStat collection if it exists
    */
  def getBySeasonId(seasonId: Int): Future[Option[NFLSeasonStat]] = {
    ctx.db.run(
      nflSeasonStats
        .filter(_.seasonId === seasonId)
        .result
        .headOption
    )
  }

  /**
    * Gets NFLSeasonStats for multiple seasons
    *
    * @param seasonIds
    * @return all NFLSeasonStats for the passed in IDs
    */
  def getBySeasonIds(seasonIds: Seq[Int]): Future[Seq[NFLSeasonStat]] =
    ctx.db.run(nflSeasonStats.filter(_.seasonId.inSet(seasonIds)).result)

  /**
    * Creates or updates the passed season stats object.  Determines which based on empty id.
    *
    * @param nflSeasonStat new or existing NFLSeasonStat
    * @return the saved season stat, or an exception if updating non-existant
    */
  def createOrUpdate(nflSeasonStat: NFLSeasonStat): Future[NFLSeasonStat] = {
    if (nflSeasonStat.id.isEmpty) {
      create(
        nflSeasonStat.clientId,
        nflSeasonStat.seasonId,
        nflSeasonStat.wins,
        nflSeasonStat.losses,
        nflSeasonStat.ties,
        nflSeasonStat.gamesTotal
      )
    } else {
      val now = DateHelper.now()
      ctx.db.run(
        nflSeasonStats
          .filter(_.id === nflSeasonStat.id)
          .update(nflSeasonStat.copy(modifiedAt = now))
          .map {
            case 0 =>
              throw new Exception(
                s"Unable to update NFLSeasonStats with id ${nflSeasonStat.id.get}"
              )
            case _ => nflSeasonStat
          }
      )
    }
  }
}
