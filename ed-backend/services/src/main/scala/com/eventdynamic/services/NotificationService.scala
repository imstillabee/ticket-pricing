package com.eventdynamic.services

import com.eventdynamic.utils.JsonUtil
import org.slf4j.LoggerFactory

import scala.collection.mutable

class NotificationService(clientId: Int, service: Option[String]) {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    *
    * @param message
    */
  def sendAlert(message: String): Unit = {
    logger.error(enhancedMessage(message))
  }

  /**
    *
    * @param message
    * @param th
    */
  def sendAlert(message: String, th: Throwable): Unit = {
    logger.error(enhancedMessage(message), th)
  }

  private def enhancedMessage(message: String): String = {
    val context: mutable.Map[String, String] =
      mutable.Map(("clientId", s"${clientId}"), ("message", message))
    if (service.isDefined) context += ("service" -> service.get)

    JsonUtil.stringify(context)
  }
}
