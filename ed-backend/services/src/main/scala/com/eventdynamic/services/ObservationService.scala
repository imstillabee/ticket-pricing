package com.eventdynamic.services

import com.eventdynamic.db.EDContext
import com.eventdynamic.models.{ObservationBase, TransactionType}
import com.google.inject.Inject
import org.slf4j.LoggerFactory
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}

class ObservationService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {

  import ctx.profile.api._

  implicit val getSeatResult: AnyRef with GetResult[ObservationBase] = GetResult(
    r =>
      ObservationBase(
        r.nextInt,
        r.nextInt,
        r.nextString,
        r.nextInt,
        r.nextInt,
        r.nextInt,
        r.nextString,
        r.nextString,
        r.nextTimestamp,
        r.nextString,
        r.nextString,
        r.nextString,
        r.nextInt,
        r.nextBigDecimalOption,
        r.nextBigDecimalOption,
        r.nextInt,
        r.nextInt,
        r.nextString,
        r.nextBigDecimalOption,
        r.nextIntOption,
        r.nextStringOption,
        r.nextIntOption,
        r.nextStringOption.map(TransactionType.withName),
        r.nextBoolean,
        r.nextBoolean,
        r.nextDouble,
        r.nextDouble,
        r.nextBoolean
    )
  )

  private val logger = LoggerFactory.getLogger(this.getClass)

  def getAllForEvents(eventIds: Seq[Int], clientId: Int): Future[Seq[ObservationBase]] = {
    logger.info(s"Getting base observations for client ($clientId) with ids $eventIds")

    // For left join on Transactions, use the most recent transaction. If there are two transactions
    // for the same timestamp (i.e. there was an exchange for the same ticket), use the "Purchase"
    // transaction. This is achieved through sorting by "type" ascending because "Purchase" is less
    // than "Return".
    val observations = sql"""
      SELECT
        s."id",
        e."id",
        e."name",
        e."primaryEventId",
        s."primarySeatId",
        e."venueId",
        v."zipCode",
        v."timeZone",
        e."timestamp",
        s."seat",
        s."row",
        s."section",
        es."id",
        es."listedPrice",
        es."overridePrice",
        ps."id",
        ps."integrationId",
        ps."name",
        t."price",
        t."revenueCategoryId",
        t."buyerTypeCode",
        t."integrationId",
        t."type",
        es."isListed",
        e."isBroadcast" as "isEventListed",
        e."eventScoreModifier",
        e."springModifier",
        es."isHeld"
      FROM "Events" e
      JOIN "EventSeats" es on e.id = es."eventId"
      LEFT JOIN (SELECT DISTINCT ON ("eventSeatId") * FROM "Transactions" ORDER BY "eventSeatId", "timestamp" DESC, "type" ASC) t on t."eventSeatId" = es."id"
      JOIN "Seats" s on s.id = es."seatId"
      JOIN "Venues" v on v.id = e."venueId"
      JOIN "PriceScales" ps on es."priceScaleId" = ps."id"
      WHERE
        e."id" IN (#${eventIds.mkString(",")})
        AND e."clientId" = $clientId
        AND e."isBroadcast"
        AND es."isListed"
    """.as[ObservationBase]

    ctx.db.run(observations)
  }
}
