package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.PriceGuard
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class PriceGuardService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  private val logger = LoggerFactory.getLogger(this.getClass)

  /** Object to query table from */
  val priceGuards = Tables.PriceGuards

  /**
    * Create a price guard for an event, section, and row
    *
    * @param eventId
    * @param section
    * @param row
    * @param minimumPrice
    * @param maximumPrice
    * @return created record id
    */
  def createForSectionAndRow(
    eventId: Int,
    section: String,
    row: String,
    minimumPrice: Int,
    maximumPrice: Option[Int]
  ): Future[Int] = {
    val now = DateHelper.now()
    val q = priceGuards returning priceGuards.map(_.id) += PriceGuard(
      None,
      eventId,
      section,
      row,
      minimumPrice,
      maximumPrice,
      now,
      now
    )

    ctx.db.run(q)
  }

  /**
    * Get a price guard by id
    *
    * @param id
    * @return priceScale if it exists
    */
  def getById(id: Int): Future[Option[PriceGuard]] = {
    ctx.db.run(priceGuards.filter(_.id === id).result.headOption)
  }

  /**
    * Get All PriceGuards for an Event
    *
    * @param eventIds
    * @return
    */
  def getAllForEvents(eventIds: Set[Int]): Future[Seq[PriceGuard]] = {
    ctx.db.run(priceGuards.filter(_.eventId inSet eventIds).result)
  }

  /**
    * Get All PriceGuards for in a set of Event and rows
    *
    * @param eventIds
    * @param rows
    *
    * @return
    */
  def getAllForEventsSectionsAndRows(
    eventIds: Set[Int],
    sections: Set[String],
    rows: Set[String]
  ): Future[Seq[PriceGuard]] = {
    ctx.db.run(
      priceGuards
        .filter(_.eventId inSet eventIds)
        .filter(_.section inSet sections)
        .filter(_.row inSet rows)
        .result
    )
  }

  /**
    * Upsert price guard records in bulk
    *
    * Identifying fields
    *   - eventId
    *   - section
    *   - row
    *
    * Updatable fields
    *   - minimumPrice
    *
    * @return Sequence of IDs corresponding the the upserted record(s)
    */
  def bulkUpsert(records: Seq[PriceGuard]): Future[Seq[Int]] = {
    val now = DateHelper.now()
    ctx.db.run(DBIO.sequence(records.map(upsertQuery(_, now))).transactionally)
  }

  private def upsertQuery(
    record: PriceGuard,
    now: Timestamp = DateHelper.now()
  ): DBIOAction[Int, NoStream, Effect.Read with Effect.Write] = {
    for {
      existing <- priceGuards
        .filter(_.eventId === record.eventId)
        .filter(_.section === record.section)
        .filter(_.row === record.row)
        .result
        .headOption

      res <- (priceGuards returning priceGuards.map(_.id)).insertOrUpdate(
        record.copy(
          id = existing.flatMap(_.id),
          maximumPrice = existing.map(_.maximumPrice).getOrElse(None),
          createdAt = existing.map(_.createdAt).getOrElse(now),
          modifiedAt = now
        )
      )
    } yield {
      if (existing.isDefined)
        logger.info(
          s"Updating price guard for " +
            s"Event ${record.eventId}, " +
            s"Section ${record.section}, " +
            s"Row ${record.row}: " +
            s"Minimum Price from ${existing.get.minimumPrice} to ${record.minimumPrice}"
        )
      else
        logger.info(
          s"Inserting price guard for " +
            s"Event ${record.eventId}, " +
            s"Section ${record.section}, " +
            s"Row ${record.row}: " +
            s"Minimum Price to ${record.minimumPrice}"
        )
      existing.flatMap(_.id).getOrElse(res.get)
    }
  }
}
