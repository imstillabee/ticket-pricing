package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.PriceScale
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class PriceScaleService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  val PriceScales = Tables.PriceScales

  implicit val getPriceScales: AnyRef with GetResult[PriceScale] = GetResult(
    r =>
      PriceScale(
        r.nextIntOption(),
        r.nextString(),
        r.nextInt(),
        r.nextInt(),
        r.nextTimestamp(),
        r.nextTimestamp()
    )
  )

  /** Create a priceScale
    *
    * @param name
    * @param venueId
    * @param integrationId
    * @return created priceScale id
    */
  def create(name: String, venueId: Int, integrationId: Int): Future[Int] = {
    val now = DateHelper.now()
    ctx.db.run(
      PriceScales returning PriceScales
        .map(_.id) += PriceScale(None, name, venueId, integrationId, now, now)
    )
  }

  /**
    * Create PriceScales in bulk
    *
    * @param records priceScales records
    * @return number of created priceScales
    */
  def bulkInsert(records: Seq[PriceScale]): Future[Option[Int]] = {
    val q = PriceScales ++= records.map(_.copy(id = None))
    ctx.db.run(q)
  }

  /** Get a priceScale by id
    *
    * @param id
    * @return priceScale if it exists
    */
  def getById(id: Int): Future[Option[PriceScale]] = {
    ctx.db.run(PriceScales.filter(_.id === id).result.headOption)
  }

  /** Get All PriceScales for a Venue
    *
    * @param venueId
    * @return
    */
  def getAllForVenue(venueId: Int): Future[Seq[PriceScale]] = {
    ctx.db.run(PriceScales.filter(_.venueId === venueId).result)
  }

  def getAllForClient(clientId: Int): Future[Seq[PriceScale]] = {
    val query = sql"""
      SELECT
        p.*
      FROM "PriceScales" p
      WHERE p."venueId" IN (
        SELECT e."venueId"
        FROM "Events" e
        WHERE e."clientId" = $clientId
      )
    """.as[PriceScale]

    ctx.db.run(query)
  }

  /** Get a priceScale by integration id
    *
    * @param integrationId
    * @return priceScale if it exists
    */
  def getByIntegrationId(integrationId: Int): Future[Option[PriceScale]] = {
    ctx.db.run(PriceScales.filter(_.integrationId === integrationId).result.headOption)
  }
}
