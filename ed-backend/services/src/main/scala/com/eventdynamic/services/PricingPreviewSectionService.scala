package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.PricingPreviewSection
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class PricingPreviewSectionService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  def getForClient(clientId: Int): Future[Seq[PricingPreviewSection]] = {
    val query = Tables.PricingPreviewSections.filter(_.clientId === clientId).result
    ctx.db.run(query)
  }
}
