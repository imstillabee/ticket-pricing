package com.eventdynamic.services

import java.sql.{BatchUpdateException, Timestamp}

import com.eventdynamic.{models, Tables}
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.RoundingType.RoundingType
import com.eventdynamic.models._
import com.eventdynamic.services.ProVenuePricingRuleService.CompoundProVenuePricingRule
import com.eventdynamic.utils.{DateHelper, NotFoundResponse, ResponseCode, SuccessResponse}
import com.google.inject.{Inject, Singleton}
import org.slf4j.LoggerFactory
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ProVenuePricingRuleService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  private val logger = LoggerFactory.getLogger(this.getClass)

  val pricingRules = Tables.ProVenuePricingRules
  val pricingRuleBuyerTypes = Tables.ProVenuePricingRuleBuyerTypes
  val pricingRulePriceScales = Tables.ProVenuePricingRulePriceScales
  val pricingRuleEvents = Tables.ProVenuePricingRuleEvents

  def create(
    clientId: Int,
    eventIds: Seq[Int],
    priceScaleIds: Seq[Int],
    externalBuyerTypeIds: Seq[String],
    isActive: Boolean = false,
    mirrorPriceScaleId: Option[Int] = None,
    percent: Option[Int] = None,
    constant: Option[BigDecimal] = None,
    name: Option[String] = None,
    round: RoundingType = RoundingType.Ceil
  ): Future[ResponseCode] = {
    val now = DateHelper.now()

    val newPricingRule = ProVenuePricingRule(
      None,
      now,
      now,
      clientId,
      name,
      isActive,
      mirrorPriceScaleId,
      percent,
      constant,
      round
    )

    val q = for {
      pricingRuleId <- (pricingRules returning pricingRules.map(_.id)) += newPricingRule
      _ <- pricingRulePriceScales ++= priceScaleIds.map(
        ProVenuePricingRulePriceScale(None, now, now, _, pricingRuleId)
      )
      _ <- pricingRuleEvents ++= eventIds.map(
        ProVenuePricingRuleEvent(None, now, now, _, pricingRuleId)
      )
      _ <- pricingRuleBuyerTypes ++= externalBuyerTypeIds.map(
        ProVenuePricingRuleBuyerType(None, now, now, _, pricingRuleId)
      )
    } yield pricingRuleId

    ctx.db
      .run(q.transactionally)
      .map(SuccessResponse(_))
  }

  def update(
    id: Int,
    clientId: Int,
    eventIds: Seq[Int],
    priceScaleIds: Seq[Int],
    externalBuyerTypeIds: Seq[String],
    isActive: Boolean = false,
    mirrorPriceScaleId: Option[Int] = None,
    percent: Option[Int] = None,
    constant: Option[BigDecimal] = None,
    name: Option[String] = None,
    round: RoundingType = RoundingType.Ceil
  ): Future[ResponseCode] = {
    val now = DateHelper.now()
    getOne(id, clientId).flatMap {
      case None => Future.successful(NotFoundResponse)
      case Some(existing) =>
        val pricingRuleId = existing.proVenuePricingRule.id.get
        val query = for {
          // Update proVenuePricingRule
          r <- pricingRules
            .filter(_.id === id)
            .update(
              existing.proVenuePricingRule.copy(
                modifiedAt = now,
                isActive = isActive,
                mirrorPriceScaleId = mirrorPriceScaleId,
                percent = percent,
                constant = constant,
                name = name,
                round = round
              )
            )
          // Delete proVenuePricingRuleEvents and create new ones
          _ <- pricingRuleEvents.filter(_.proVenuePricingRuleId === pricingRuleId).delete
          _ <- pricingRuleEvents ++= eventIds.map(
            ProVenuePricingRuleEvent(None, now, now, _, pricingRuleId)
          )
          // Delete proVenuePricingRulePriceScales and create new ones
          _ <- pricingRulePriceScales.filter(_.proVenuePricingRuleId === pricingRuleId).delete
          _ <- pricingRulePriceScales ++= priceScaleIds.map(
            ProVenuePricingRulePriceScale(None, now, now, _, pricingRuleId)
          )
          // Delete proVenuePricingRuleBuyerTypes and create new ones
          _ <- pricingRuleBuyerTypes.filter(_.proVenuePricingRuleId === pricingRuleId).delete
          _ <- pricingRuleBuyerTypes ++= externalBuyerTypeIds.map(
            ProVenuePricingRuleBuyerType(None, now, now, _, pricingRuleId)
          )
        } yield r

        ctx.db
          .run(query.transactionally)
          .map(_ => SuccessResponse(pricingRuleId))
          .recover {
            // TODO: rework error handling to provide more information
            case err: BatchUpdateException
                if err.getMessage.toLowerCase contains "foreign key constraint" =>
              NotFoundResponse
          }
    }
  }

  def getConflicting(
    id: Option[Int],
    clientId: Int,
    eventIds: Seq[Int],
    priceScaleIds: Seq[Int],
    externalBuyerTypeIds: Seq[String]
  ): Future[Seq[Int]] = {
    val q = for {
      rule <- pricingRules
        .filter(_.clientId === clientId)
        .filter(_.isActive === true)
        .filterOpt(id)(_.id =!= _)
      buyerType <- pricingRuleBuyerTypes.filter(_.externalBuyerTypeId inSet externalBuyerTypeIds)
      if buyerType.proVenuePricingRuleId === rule.id
      priceScale <- pricingRulePriceScales.filter(_.priceScaleId inSet priceScaleIds)
      if priceScale.proVenuePricingRuleId === rule.id
      event <- pricingRuleEvents.filter(_.eventId inSet eventIds)
      if event.proVenuePricingRuleId === rule.id
    } yield rule.id

    ctx.db.run(q.distinct.result)
  }

  def getConflicting(clientId: Int, externalBuyerTypeIds: Seq[String]): Future[Seq[Int]] = {
    val q = for {
      rule <- pricingRules.filter(_.clientId === clientId).filter(_.isActive === true)
      buyerType <- pricingRuleBuyerTypes.filter(_.externalBuyerTypeId inSet externalBuyerTypeIds)
      if buyerType.proVenuePricingRuleId === rule.id
    } yield rule

    ctx.db.run(q.map(_.id).distinct.result)
  }

  def getOne(id: Int, clientId: Int): Future[Option[CompoundProVenuePricingRule]] = {
    val q = for {
      rule <- pricingRules.filter(_.id === id).filter(_.clientId === clientId)
      buyerType <- pricingRuleBuyerTypes if buyerType.proVenuePricingRuleId === rule.id
      priceScale <- pricingRulePriceScales if priceScale.proVenuePricingRuleId === rule.id
      event <- pricingRuleEvents if event.proVenuePricingRuleId === rule.id
    } yield (rule, buyerType, priceScale, event)

    val grouped = q.result
      .map(
        _.groupBy {
          case (rule, _, _, _) => rule.id
        }.values
          .map { values =>
            val rule = values.map(_._1).head
            val buyerTypes = values.map(_._2.externalBuyerTypeId).distinct
            val priceScales = values.map(_._3.priceScaleId).distinct
            val events = values.map(_._4.eventId).distinct

            CompoundProVenuePricingRule(rule, buyerTypes, priceScales, events)
          }
          .toSeq
          .headOption
      )

    ctx.db.run(grouped)
  }

  val getAllGetResult = GetResult(
    r =>
      CompoundProVenuePricingRule(
        proVenuePricingRule = ProVenuePricingRule(
          id = r.nextIntOption(),
          createdAt = r.nextTimestamp(),
          modifiedAt = r.nextTimestamp(),
          clientId = r.nextInt(),
          name = r.nextStringOption(),
          isActive = r.nextBoolean(),
          mirrorPriceScaleId = r.nextIntOption(),
          percent = r.nextIntOption(),
          constant = r.nextBigDecimalOption(),
          round = RoundingType.withName(r.nextString())
        ),
        externalBuyerTypeIds = r.nextArray[String],
        priceScaleIds = r.nextArray[Int],
        eventIds = r.nextArray[Int]
    )
  )

  def getAll(
    clientId: Int,
    onlyActive: Boolean = false
  ): Future[Seq[CompoundProVenuePricingRule]] = {
    val q = sql"""
SELECT
    r.id,
    r."createdAt",
    r."modifiedAt",
    r."clientId",
    r.name,
    r."isActive",
    r."mirrorPriceScaleId",
    r.percent,
    r.constant,
    r.round,
    ARRAY(SELECT "externalBuyerTypeId" FROM "ProVenuePricingRuleBuyerTypes" WHERE "proVenuePricingRuleId" = r.id),
    ARRAY(SELECT "priceScaleId" FROM "ProVenuePricingRulePriceScales" WHERE "proVenuePricingRuleId" = r.id),
    ARRAY(SELECT "eventId" FROM "ProVenuePricingRuleEvents" WHERE "proVenuePricingRuleId" = r.id)
FROM
    "ProVenuePricingRules" r
WHERE
    r."clientId" = $clientId
    AND (${!onlyActive} OR r."isActive" = true);
    """.as[CompoundProVenuePricingRule](getAllGetResult)

    ctx.db.run(q)
  }

  def delete(id: Int, clientId: Int): Future[Boolean] = {
    val query = for {
      rule <- pricingRules.filter(_.id === id).filter(_.clientId === clientId)
    } yield rule
    ctx.db.run(query.delete).map(_ == 1)
  }
}

object ProVenuePricingRuleService {
  case class CompoundProVenuePricingRule(
    proVenuePricingRule: ProVenuePricingRule,
    externalBuyerTypeIds: Seq[String],
    priceScaleIds: Seq[Int],
    eventIds: Seq[Int]
  )
}
