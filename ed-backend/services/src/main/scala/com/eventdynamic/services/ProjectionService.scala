package com.eventdynamic.services

import java.sql.Timestamp
import java.time.{Instant, LocalDate}
import java.time.temporal.ChronoUnit

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.{Projection, TimeStat}
import com.eventdynamic.utils._
import com.github.tminglei.slickpg.window.PgWindowFuncSupport.WindowFunctions
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ProjectionService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext)
    extends WindowFunctions {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val projections = Tables.Projections

  /** Bulk insert projections
    *
    * @param records
    * @return integer representing the number of records inserted
    */
  def bulkInsert(records: Seq[Projection]): Future[Int] = {
    val now = DateHelper.now()
    val query = projections ++= records.map(_.copy(id = None, createdAt = now, modifiedAt = now))
    ctx.db.run(query).map(_.sum)
  }

  private def bulkInsertQuery(records: Seq[Projection]) = {
    val now = DateHelper.now()
    projections ++= records.map(_.copy(id = None, createdAt = now, modifiedAt = now))
  }

  /** Deletes projections for any events passed in
    *
    * @param records
    * @return integer representing the number of records deleted
    */
  def deleteForEvents(records: Seq[Projection]): Future[Int] = {
    val delete = deleteForEventsQuery(records)
    ctx.db.run(delete)
  }

  private def deleteForEventsQuery(records: Seq[Projection]): DBIO[Int] = {
    val eventIds = records.map(_.eventId).toSet
    projections
      .filter(p => {
        p.eventId inSet eventIds
      })
      .delete
  }

  /** Removes old projections from the table and inserts future projections
    * ran transactionally so changes will only be committed if the new projections are saved
    *
    * @param records
    * @return number of inserted records
    */
  def bulkCleanAndInsert(records: Seq[Projection]): Future[Int] = {
    val actions = for {
      _ <- deleteForEventsQuery(records)
      insertCount <- bulkInsertQuery(records).map(_.sum)
    } yield insertCount

    // execute queries as a transaction
    ctx.db.run(actions.transactionally)
  }

  /**
    * Get projections in the time stat format with both cumulative and periodic revenue and inventory
    *
    * @param from earliest instant to fetch projections for, inclusive
    * @param to latest instant to fetch projections for, inclusive
    * @param seasonId optional season filter
    * @param eventId option event filter
    * @return a list of time stats in daily interval
    */
  def getProjectedTimeStats(
    from: Instant,
    to: Instant,
    seasonId: Option[Int] = None,
    eventId: Option[Int] = None,
    startingInventory: Option[Int] = None,
    startingRevenue: Option[BigDecimal] = None
  ): Future[Seq[TimeStat]] = {
    if (eventId.isEmpty && seasonId.isEmpty) {
      throw new IllegalArgumentException(
        "A season or event filter must be specified for projected time stats"
      )
    }

    val query =
      projections
        .filterOpt(eventId)(_.eventId === _)
        .filterOpt(seasonId)((row, sId) => {
          row.eventId in row.event.filter(_.seasonId === sId).map(_.id)
        })
        .filter(_.timestamp >= from)
        .filter(_.timestamp <= to)
        .groupBy(_.timestamp)
        .map(group => {
          val (timestamp, records) = group
          val cumulativeRevenue = records.map(_.revenue).sum
          val cumulativeInventory = records.map(_.inventory).sum
          val lastCumulativeRevenue = lag(cumulativeRevenue) :: Over
            .sortBy(timestamp)
          val lastCumulativeInventory = lag(cumulativeInventory) :: Over
            .sortBy(timestamp)

          (
            timestamp,
            cumulativeRevenue,
            cumulativeInventory,
            lastCumulativeRevenue,
            lastCumulativeInventory
          )
        })

    val action = query.result
      .map(_.map(row => {
        val (
          timestamp,
          cumulativeRevenue,
          cumulativeInventory,
          lastCumulativeRevenue,
          lastCumulativeInventory
        ) = row

        TimeStat(
          intervalStart = Timestamp.from(timestamp.minus(1, ChronoUnit.DAYS)),
          intervalEnd = Timestamp.from(timestamp),
          revenue = for {
            f <- cumulativeRevenue
            i <- lastCumulativeRevenue
          } yield f - i,
          inventory = for {
            f <- cumulativeInventory
            i <- lastCumulativeInventory
          } yield f - i,
          cumulativeRevenue = cumulativeRevenue,
          cumulativeInventory = cumulativeInventory,
          isProjected = true
        )
      }).toList)
      .map {
        case (head: TimeStat) :: rest =>
          head.copy(inventory = for {
            f <- head.cumulativeInventory
            i <- startingInventory
          } yield f - i, revenue = for {
            f <- head.cumulativeRevenue
            i <- startingRevenue
          } yield f - i) :: rest
        case r => r
      }

    ctx.db.run(action)
  }
}
