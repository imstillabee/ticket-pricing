package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.RevenueCategoryMapping
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.matching.Regex

@Singleton
class RevenueCategoryMappingService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {

  import ctx.profile.api._

  /** Object to query table from */
  val revenueCategoryMappings = Tables.RevenueCategoryMappings

  /** Creates a RevenueCategoryMapping for a revenue category
    *
    * @param clientId
    * @param revenueCategoryId
    * @param regex
    * @param order
    * @return
    */
  def create(
    clientId: Int,
    revenueCategoryId: Int,
    regex: String,
    order: Int
  ): Future[RevenueCategoryMapping] = {
    val now = DateHelper.now()
    ctx.db.run(
      revenueCategoryMappings returning revenueCategoryMappings.map(_.id) into
        ((revenueCategoryMapping, id) => revenueCategoryMapping.copy(id = Some(id)))
        += RevenueCategoryMapping(None, clientId, now, now, revenueCategoryId, regex, order)
    )
  }

  /** Get all revenue mappings for a client
    *
    * @param clientId
    * @return
    */
  def getByClientId(clientId: Int): Future[Seq[RevenueCategoryMapping]] = {
    ctx.db.run(revenueCategoryMappings.filter(_.clientId === clientId).sortBy(r => r.order).result)
  }

  /** get all revenue mappings for client and match code to revenue category id
    *
    * @param clientId
    * @param code
    * @return
    */
  def regexMatch(clientId: Int, code: String): Future[Option[Int]] = {
    getByClientId(clientId).map { categories =>
      findMatch(categories, code).map(_.revenueCategoryId)
    }
  }

  /** iterate through mappings in order and run regex checks to find correct revenue category id
    *
    * @param categories
    * @param code
    * @return
    */
  def findMatch(
    categories: Seq[RevenueCategoryMapping],
    code: String
  ): Option[RevenueCategoryMapping] = {
    categories.find { category =>
      val check = new Regex(category.regex)
      check.findFirstIn(code).isDefined
    }
  }
}
