package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.RevenueCategory
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class RevenueCategoryService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  import ctx.profile.api._

  /** Object to query table from */
  val revenueCategories = Tables.RevenueCategories

  /** Creates a revenueCategory with a name
    *
    * @param name
    * @return revenue category
    */
  def create(name: String): Future[RevenueCategory] = {
    val now = DateHelper.now()
    ctx.db.run(
      revenueCategories returning revenueCategories.map(_.id) into
        ((revenueCategory, id) => revenueCategory.copy(id = Some(id)))
        += RevenueCategory(None, now, name, now, true)
    )
  }

  /** Get Revenue Category or Create if none is found
    *
    * @param name
    * @return
    */
  def getOrCreate(name: String): Future[RevenueCategory] = {
    getByName(name).flatMap {
      case Some(revenueCategory) => Future.successful(revenueCategory)
      case None                  => create(name)
    }
  }

  /** Get Revenue Category by name
    *
    * @param name
    * @return
    */
  def getByName(name: String): Future[Option[RevenueCategory]] = {
    ctx.db.run(revenueCategories.filter(_.name === name).result.headOption)
  }

  /** Gets a revenue category by id
    *
    * @param id
    * @return revenue category
    */
  def getById(id: Int): Future[Option[RevenueCategory]] = {
    ctx.db.run(revenueCategories.filter(_.id === id).result.headOption)
  }
}
