package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.{Event, Season, Transaction}
import com.eventdynamic.utils._
import com.eventdynamic.transactions.SeasonTransaction
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

// TODO: refactor to remove seasonTransaction from season service (services should not depend on transactions)
@Singleton
class SeasonService @Inject()(val ctx: EDContext, val seasonTransaction: SeasonTransaction)(
  implicit ec: ExecutionContext
) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val seasons = Tables.Seasons

  /** Create a season
    *
    * @param name
    * @param clientId
    * @return created record id
    */
  def create(
    name: Option[String],
    clientId: Int,
    startDate: Option[Timestamp] = None,
    endDate: Option[Timestamp] = None
  ): Future[ResponseCode] = {
    val season = ctx.db.run(
      seasons returning seasons
        .map(_.id) into ((season, id) => season.copy(id = Some(id))) += Season(
        None,
        name,
        clientId,
        startDate,
        endDate
      )
    )
    season.map(SuccessResponse(_))
  }

  def getOrCreate(name: Option[String], clientId: Int): Future[Option[Int]] = {
    if (name.isDefined) {
      getByName(name.get).flatMap {
        case Some(season) => Future.successful(season.id)
        case _ =>
          create(name, clientId).map {
            case SuccessResponse(season: Season) => season.id
            case _                               => None
          }
      }
    } else {
      Future.successful(None)
    }
  }

  def getByName(name: String): Future[Option[Season]] = {
    ctx.db.run(seasons.filter(_.name === name).result.headOption)
  }

  /** Get a season by its Id
    *
    * @param id
    * @return season if it exists
    */
  def getById(id: Int, clientId: Option[Int], isAdmin: Boolean): Future[Option[Season]] = {
    ctx.db.run(
      seasons
        .filter(s => s.id === id && (s.clientId === clientId || isAdmin == true))
        .result
        .headOption
    )
  }

  /** Get all seasons
    *
    * @return all seasons, in order by startDate, largest date to smallest date
    */
  def getAll(clientId: Option[Int], isAdmin: Boolean): Future[Seq[Season]] = {
    if (clientId.isDefined) {
      ctx.db.run(seasons.filter(_.clientId === clientId).sortBy(_.endDate.desc.nullsFirst).result)
    } else if (isAdmin) {
      ctx.db.run(seasons.sortBy(_.endDate.desc.nullsFirst).result)
    } else {
      Future.successful(Seq())
    }
  }

  def getCurrent(clientId: Option[Int], isAdmin: Boolean): Future[Option[Season]] = {
    for {
      seasons <- getAll(clientId, false)
      currentSeason = seasons.find(
        s => s.startDate.isDefined && s.startDate.get.before(DateHelper.now())
      )
    } yield {
      currentSeason
    }
  }

  /** Update
    *
    * @param season
    * @return non-zero for success
    */
  def update(season: Season): Future[ResponseCode] = {
    ctx.db.run(seasons.filter(_.id === season.id).update(season)).map {
      case 0 => NotFoundResponse
      case _ => SuccessResponse(season)
    }
  }

  /** Delete season
    *
    * @param id
    * @return true if it exists
    */
  def delete(id: Int): Future[Boolean] = {
    ctx.db.run(seasons.filter(_.id === id).delete).map(_ == 1)
  }

  /**
    * Adds the start and end dates to a season if they exist.
    *
    * Start date = first transaction of the season (saved), or now if none exist
    * End date = last event of the season (saved), or 1 year + start if none exist
    *
    * @param season season to add dates to
    * @return a season with start/end dates
    */
  def resetDates(season: Season): Future[ResponseCode] = {
    // Get the dates needed
    val startDateFuture = seasonTransaction
      .getEarliestTransactionDateBySeason(season.id.get)
    val endDateFuture = seasonTransaction
      .getLastEventBySeason(season.id.get)

    // Wait for them then save the
    Future
      .sequence(Seq(startDateFuture, endDateFuture))
      .flatMap(dates => update(season.copy(startDate = dates(0), endDate = dates(1))))
  }
}
