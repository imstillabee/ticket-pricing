package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.Seat
import com.eventdynamic.utils.{DateHelper, ResponseCode, SuccessResponse}
import com.google.inject.{Inject, Singleton}
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class SeatService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  import ctx.profile.api._

  val seats = Tables.Seats

  def create(
    primarySeatId: Int,
    seat: String,
    row: String,
    section: String,
    venueId: Int
  ): Future[ResponseCode] = {
    val now = DateHelper.now()

    val seatRecord = ctx.db.run(
      (seats returning seats.map(_.id)
        into ((seat, id) => seat.copy(id = Some(id)))) += Seat(
        None,
        Some(primarySeatId),
        now,
        now,
        seat,
        row,
        section,
        venueId
      )
    )
    seatRecord.map(SuccessResponse(_))
  }

  def getById(id: Int): Future[Option[Seat]] = {
    ctx.db.run(seats.filter(_.id === id).result.headOption)
  }

  def getByIntegrationId(primarySeatId: Int, venueId: Int): Future[Option[Seat]] = {
    ctx.db.run(
      seats
        .filter(_.primarySeatId === primarySeatId)
        .filter(_.venueId === venueId)
        .result
        .headOption
    )
  }

  def get(
    seat: Option[String] = None,
    row: Option[String] = None,
    section: Option[String] = None,
    venueId: Option[Int] = None
  ): Future[Seq[Seat]] = {
    val q = seats
      .filterOpt(venueId)(_.venueId === _)
      .filterOpt(section)(_.section === _)
      .filterOpt(row)(_.row === _)
      .filterOpt(seat)(_.seat === _)
    ctx.db.run(q.result)
  }

  def get(seat: String, row: String, section: String, primaryEventId: Int): Future[Option[Seat]] = {
    implicit val getSeatResult: GetResult[Seat] = GetResult(
      r =>
        Seat(
          r.nextIntOption,
          r.nextIntOption,
          r.nextTimestamp,
          r.nextTimestamp,
          r.nextString,
          r.nextString,
          r.nextString,
          r.nextInt
      )
    )

    val q =
      sql"""
           SELECT s."id", s."primarySeatId", s."createdAt", s."modifiedAt", s."seat", s."row", s."section", s."venueId" FROM "Seats" s
           INNER JOIN "Venues" v on s."venueId" = v."id"
           INNER JOIN "Events" e on e."venueId" = v."id"
           WHERE s."seat" = $seat AND s."row" = $row AND s."section" = $section AND e."primaryEventId" = $primaryEventId
         """.as[Seat]

    ctx.db.run(q.headOption)
  }

  /** Bulk Insert seats for onboarding script
    *
    * @param records
    * @return
    */
  def bulkInsert(records: Seq[Seat]): Future[ResponseCode] = {
    val insert = seats ++= records.map(
      _.copy(id = None, createdAt = DateHelper.now(), modifiedAt = DateHelper.now())
    )
    ctx.db.run(insert).map(SuccessResponse(_))
  }

  /**
    * Upsert a Seat record
    *
    * Identifying fields
    *   - venueId
    *   - section
    *   - row
    *   - seat
    *
    * Updatable fields
    *   - primarySeatId
    *
    * @return id of the inserted or updated record
    */
  def upsert(record: Seat): Future[Int] = {
    ctx.db.run(upsertQuery(record).transactionally)
  }

  /**
    * Upsert seat records in bulk
    *
    * Identifying fields
    *   - venueId
    *   - section
    *   - row
    *   - seat
    *
    * Updatable fields
    *   - primarySeatId
    *
    * @return Sequence of IDs corresponding the the upserted record(s)
    */
  def bulkUpsert(records: Seq[Seat]): Future[Seq[Int]] = {
    val now = DateHelper.now()
    ctx.db.run(DBIO.sequence(records.map(upsertQuery(_, now))).transactionally)
  }

  private def upsertQuery(
    record: Seat,
    now: Timestamp = DateHelper.now()
  ): DBIOAction[Int, NoStream, Effect.Read with Effect.Write] = {
    for {
      existing <- seats
        .filter(_.venueId === record.venueId)
        .filter(_.section === record.section)
        .filter(_.row === record.row)
        .filter(_.seat === record.seat)
        .result
        .headOption
      res <- (seats returning seats.map(_.id)).insertOrUpdate(
        record.copy(
          id = existing.flatMap(_.id),
          createdAt = existing.map(_.createdAt).getOrElse(now),
          modifiedAt = now
        )
      )
    } yield existing.flatMap(_.id).getOrElse(res.get)
  }
}
