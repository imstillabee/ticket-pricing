package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.{ClientIntegrationComposite, Inventory, SecondaryPricingRule}
import com.eventdynamic.utils._
import com.google.inject.Inject
import javax.inject.Singleton
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}
import scala.math.BigDecimal.RoundingMode

@Singleton
class SecondaryPricingRulesService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {

  import ctx.profile.api._

  private val logger = LoggerFactory.getLogger(this.getClass)

  val secondaryPricingRules = Tables.SecondaryPricingRules

  def create(
    clientIntegrationId: Int,
    percent: Option[Int] = None,
    constant: Option[BigDecimal] = None
  ): Future[SecondaryPricingRule] = {
    val now = DateHelper.now()

    ctx.db.run(
      secondaryPricingRules returning secondaryPricingRules.map(_.id)
        into ((secondaryPricingRule, id) => secondaryPricingRule.copy(id = Some(id)))
        += SecondaryPricingRule(None, now, now, clientIntegrationId, percent, constant)
    )
  }

  /**
    * Get secondaryPricingRule by id
    *
    * @param id
    * @return SecondaryPricingRule
    */
  def getById(id: Int): Future[Option[SecondaryPricingRule]] = {
    ctx.db.run(secondaryPricingRules.filter(_.id === id).result.headOption)
  }

  /**
    * Get secondaryPricingRule by clientIntegrationId
    *
    * @param clientIntegrationId
    * @return SecondaryPricingRule
    */
  def getByClientIntegrationId(clientIntegrationId: Int): Future[Option[SecondaryPricingRule]] = {
    ctx.db.run(
      secondaryPricingRules.filter(_.clientIntegrationId === clientIntegrationId).result.headOption
    )
  }

  def saveSettings(
    clientIntegrationId: Int,
    percent: Option[Int],
    constant: Option[BigDecimal]
  ): Future[Int] = {
    val now = DateHelper.now()
    val record = if (constant.isEmpty) {
      SecondaryPricingRule(None, now, now, clientIntegrationId, percent, None)
    } else {
      SecondaryPricingRule(
        None,
        now,
        now,
        clientIntegrationId,
        None,
        constant.map(_.setScale(2, RoundingMode.HALF_UP))
      )
    }
    ctx.db.run(upsert(record))
  }

  def applySecondaryPricingRule(
    clientIntegration: ClientIntegrationComposite,
    originalPrice: BigDecimal
  ): BigDecimal = {
    val roundingMode = getRoundingMode(clientIntegration)
    val hasSecondaryPricingRule = clientIntegration.percent.isDefined || clientIntegration.constant.isDefined

    if (!hasSecondaryPricingRule) {
      logger.debug(s"No secondary pricing rule found for ${clientIntegration.name}")
      originalPrice.setScale(0, roundingMode)
    } else {
      val rule =
        if (clientIntegration.percent.isDefined) s"${clientIntegration.percent.get} percent"
        else s"${clientIntegration.constant.get} dollar"
      logger.info(
        s"${clientIntegration.name}: Applying a $rule change to original price: $originalPrice"
      )
      val finalPrice = (clientIntegration match {
        case ci if ci.percent.isDefined && ci.constant.isDefined =>
          throw new Exception("Invalid Secondary Pricing Rule")
        case ci if ci.percent.isDefined =>
          val priceModifier = BigDecimal(1.00 + ci.percent.get / 100.00)
          originalPrice * priceModifier
        case ci if ci.constant.isDefined =>
          originalPrice + ci.constant.get
      }).setScale(0, roundingMode)
      logger.debug(s"Original Price: $originalPrice \t\tModified Price: $finalPrice")
      finalPrice
    }
  }

  private def getRoundingMode(clientIntegration: ClientIntegrationComposite): RoundingMode.Value = {
    if ((clientIntegration.percent.isDefined && clientIntegration.percent.get < 0) ||
        (clientIntegration.constant.isDefined && clientIntegration.constant.get < 0))
      RoundingMode.DOWN
    else
      RoundingMode.CEILING
  }

  private def upsert(record: SecondaryPricingRule) = {
    for {
      row <- secondaryPricingRules
        .filter(_.clientIntegrationId === record.clientIntegrationId)
        .result
        .headOption
      query <- secondaryPricingRules.insertOrUpdate {
        record.copy(
          id = row.flatMap(_.id),
          createdAt = row.map(_.createdAt).getOrElse(record.createdAt)
        )
      }
    } yield query
  }
}
