package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.TeamStat
import com.eventdynamic.utils.{DateHelper, NotFoundResponse, ResponseCode, SuccessResponse}
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class TeamStatService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val teamStats = Tables.TeamStats

  /** Create an TeamStat
    *
    * @param clientId
    * @param wins
    * @param losses
    * @param gamesTotal
    * @return created TeamStat id
    */
  def create(
    clientId: Int,
    seasonId: Int,
    wins: Int,
    losses: Int,
    gamesTotal: Int
  ): Future[ResponseCode] = {
    val now = DateHelper.now()

    val newTeamStat = ctx.db.run(
      teamStats returning teamStats
        .map(_.id) into ((teamStat, id) => teamStat.copy(id = Some(id))) += TeamStat(
        None,
        now,
        now,
        clientId,
        seasonId,
        wins,
        losses,
        gamesTotal
      )
    )
    newTeamStat.map(SuccessResponse(_))
  }

  /** Get a TeamStat by id
    *
    * @param id
    * @return TeamStat if it exists
    */
  def getById(id: Int): Future[Option[TeamStat]] = {
    ctx.db.run(teamStats.filter(_.id === id).result.headOption)
  }

  /** Get a TeamStats collection by the client id
    *
    * @param id
    * @return TeamStat collection if it exists
    */
  def getByClientId(id: Int): Future[Seq[TeamStat]] = {
    ctx.db.run(teamStats.filter(_.clientId === id).result)
  }

  /** Get TeamStats by the client id and the season id
    *
    * @param clientId
    * @param seasonId
    * @return TeamStat collection if it exists
    */
  def getByClientIdAndSeasonId(clientId: Int, seasonId: Int): Future[Option[TeamStat]] = {
    ctx.db.run(
      teamStats
        .filter(_.clientId === clientId)
        .filter(_.seasonId === seasonId)
        .result
        .headOption
    )
  }

  /**
    *
    * @param teamStat
    * @return
    */
  def update(teamStat: TeamStat): Future[ResponseCode] = {
    val now = DateHelper.now()
    ctx.db.run(
      teamStats
        .filter(_.id === teamStat.id)
        .update(teamStat.copy(modifiedAt = now))
        .map {
          case 0 => NotFoundResponse
          case _ => SuccessResponse(teamStat)
        }
    )
  }
}
