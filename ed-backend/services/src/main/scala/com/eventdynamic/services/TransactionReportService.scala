package com.eventdynamic.services

import java.sql.Timestamp
import java.time.ZoneId
import java.time.format.DateTimeFormatter

import akka.NotUsed
import akka.stream.scaladsl.Source
import akka.util.ByteString
import com.eventdynamic.db.EDContext
import com.google.inject.Inject
import slick.jdbc.{GetResult, ResultSetConcurrency, ResultSetType}

import scala.concurrent.ExecutionContext

case class ReportRow(
  transactionType: String,
  eventName: String,
  eventTime: Timestamp,
  transactionTime: Timestamp,
  section: String,
  row: String,
  seats: String,
  unitPrice: BigDecimal,
  salesTotal: BigDecimal,
  marketPlace: String,
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  timeZone: String
)

class TransactionReportService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  import TransactionReportService._
  import ctx.profile.api._

  private val headers: Seq[String] = Seq(
    "Event Name",
    "Event Timestamp",
    "Transaction Timestamp",
    "Section",
    "Row",
    "Seats",
    "Average Unit Price",
    "Sales Total",
    "Market Place",
    "Transaction Type",
    "Seat Created At",
    "Seat Modified At"
  )

  implicit val getRowResult: AnyRef with GetResult[ReportRow] = GetResult(
    r =>
      ReportRow(
        r.nextString,
        r.nextString,
        r.nextTimestamp,
        r.nextTimestamp,
        r.nextString,
        r.nextString,
        r.nextString,
        r.nextBigDecimal,
        r.nextBigDecimal,
        r.nextString,
        r.nextTimestamp,
        r.nextTimestamp,
        r.nextString
    )
  )

  def getTransactionData(
    seasonId: Option[Int] = None,
    eventId: Option[Int] = None,
    start: Option[Timestamp] = None,
    end: Option[Timestamp] = None,
    clientId: Int
  ): Source[ReportRow, NotUsed] = {
    val query = sql"""
      SELECT
        t.type as "transactionType",
        MIN(e.name) as "eventName",
        MIN(e.timestamp) as "eventTimestamp",
        MIN(t.timestamp) as "transactionTimestamp",
        s.section as "section",
        s.row as "row",
        string_agg(s.seat, ', ' order by LPAD(s.seat, 4, '0')) as "seats",
        AVG(t.price) as "unitPrice",
        SUM(t.price) as "salesTotal",
        MIN(i.name) as "marketPlace",
        MAX(es."createdAt") as "createdAt",
        MAX(es."modifiedAt") as "modifiedAt",
        MIN(v."timeZone") as "timeZone"
      FROM "Transactions" t
      LEFT JOIN "EventSeats" es on t."eventSeatId" = es.id
      LEFT JOIN "Events" e on e.id = es."eventId"
      LEFT JOIN "Seats" s on s.id = es."seatId"
      LEFT JOIN "Integrations" i on i.id = t."integrationId"
      LEFT JOIN "Venues" v on v.id = e."venueId"
      WHERE (${seasonId.isEmpty} OR e."seasonId" = $seasonId)
      AND (${eventId.isEmpty} OR e."id" = $eventId)
      AND (${start.isEmpty} OR (t."timestamp" >= $start))
      AND (${end.isEmpty} OR (t."timestamp" < $end))
      AND e."clientId" = $clientId
      AND t."primaryTransactionId" IS NOT NULL
      GROUP BY t."primaryTransactionId", t."type", e."id", s."section", s."row"
      ORDER BY "eventTimestamp", "transactionTimestamp"
        """.as[ReportRow]

    Source
      .fromPublisher(
        ctx.db.stream(
          query
            .withStatementParameters(
              rsType = ResultSetType.ForwardOnly,
              rsConcurrency = ResultSetConcurrency.ReadOnly,
              fetchSize = 10000
            )
            .transactionally
        )
      )
  }

  def generateReport(
    seasonId: Option[Int] = None,
    eventId: Option[Int] = None,
    start: Option[Timestamp] = None,
    end: Option[Timestamp] = None,
    clientId: Int
  ): Source[ByteString, NotUsed] = {
    val headerSource = Source.single(headers)
    val dataSource =
      getTransactionData(seasonId, eventId, start, end, clientId)
        .map(row => {
          Seq(
            wrapInQuotes(row.eventName),
            row.eventTime.toInstant
              .atZone(ZoneId.of(row.timeZone))
              .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME),
            row.transactionTime.toInstant
              .atZone(ZoneId.of(row.timeZone))
              .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME),
            wrapInQuotes(row.section),
            wrapInQuotes(row.row),
            wrapInQuotes(row.seats),
            row.unitPrice.toString,
            row.salesTotal.toString,
            wrapInQuotes(row.marketPlace),
            wrapInQuotes(row.transactionType),
            row.createdAt.toInstant
              .atZone(ZoneId.of(row.timeZone))
              .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME),
            row.modifiedAt.toInstant
              .atZone(ZoneId.of(row.timeZone))
              .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
          )
        })
        .recover {
          case ex: Throwable => throw ex
        }

    val combinedSources = headerSource.concat(dataSource)
    combinedSources.map(line => ByteString(line.mkString(",") + "\n"))
  }
}

object TransactionReportService {
  def wrapInQuotes(text: String): String = "\"" + text + "\""
}
