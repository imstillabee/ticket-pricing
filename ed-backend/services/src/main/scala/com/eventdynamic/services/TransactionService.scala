package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.Transaction
import com.eventdynamic.models.TransactionType.TransactionType
import com.eventdynamic.utils.DateHelper
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class TransactionService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {

  import ctx.profile.api._

  import Tables.transactionTypeColumnType

  val transactions = Tables.Transactions

  /**
    * Create a transaction
    *
    * @param timestamp         when the transaction occurred
    * @param price             price is positive for a sale and negative for a return
    * @param eventSeatId       reference to ED event seat
    * @param integrationId     reference to an ED integration
    * @param buyerTypeCode     raw buyer type code from integration
    * @param transactionType   transaction type (purchase or return)
    * @param revenueCategoryId reference to an ED revenue category
    * @return created transaction
    */
  def create(
    timestamp: Timestamp,
    price: BigDecimal,
    eventSeatId: Int,
    integrationId: Int,
    buyerTypeCode: String,
    transactionType: TransactionType,
    revenueCategoryId: Option[Int],
    primaryTransactionId: Option[String] = None,
    secondaryTransactionId: Option[String] = None,
  ): Future[Transaction] = {
    // Should we calculate the revenue categoryId here?
    val record = Transaction(
      None,
      DateHelper.now(),
      DateHelper.now(),
      timestamp,
      price,
      eventSeatId,
      integrationId,
      buyerTypeCode,
      transactionType,
      revenueCategoryId,
      primaryTransactionId,
      secondaryTransactionId
    )
    val q = transactions returning transactions.map(_.id) into ((t, id) => t.copy(id = Some(id))) += record
    ctx.db.run(q)
  }

  /**
    * Fetch a transaction by either its primary transaction ID
    *
    * @param primaryTransactionId
    * @param eventSeatId
    * @return
    */
  def getOne(primaryTransactionId: String, eventSeatId: Int): Future[Option[Transaction]] = {
    val q = transactions
      .filter(
        t => (t.primaryTransactionId === primaryTransactionId) && t.eventSeatId === eventSeatId
      )
      .result
      .headOption

    ctx.db.run(q)
  }

  /**
    * Bulk insert
    *
    * @param records transaction records
    * @return the number of created records
    */
  def bulkInsert(records: Seq[Transaction]): Future[Option[Int]] = {
    val q = transactions ++= records.map(_.copy(id = None, createdAt = DateHelper.now()))
    ctx.db.run(q)
  }

  /**
    * Bulk update transactions by the transaction id. These fields will be updated:
    *   - timestamp
    *   - buyerTypeCode
    *   - price
    *   - revenueCategoryId
    *   - modifiedAt (value set automatically)
    *
    * @param records list of transactions (they should have ids)
    * @return number of updated records
    */
  def bulkUpdate(records: Seq[Transaction]): Future[Int] = {
    val now = DateHelper.now()
    val q: DBIO[Seq[Int]] = DBIO.sequence(
      records
        .filter(_.id.isDefined)
        .map {
          case record if record.revenueCategoryId.isDefined =>
            transactions
              .filter(_.id === record.id.get)
              .map(t => (t.timestamp, t.buyerTypeCode, t.price, t.revenueCategoryId, t.modifiedAt))
              .update(
                (
                  record.timestamp,
                  record.buyerTypeCode,
                  record.price,
                  record.revenueCategoryId.get,
                  now
                )
              )
          case record =>
            transactions
              .filter(_.id === record.id.get)
              .map(t => (t.timestamp, t.buyerTypeCode, t.price, t.modifiedAt))
              .update((record.timestamp, record.buyerTypeCode, record.price, now))
        }
    )

    ctx.db.run(q.map(_.sum))

  }

  /**
    * Bulk update transaction's integration ID by transaction ID
    *
    * @param updates A list of tuples in the form (transactionId, integrationId)
    * @return number of updated records
    */
  def bulkUpdateIntegrationIds(updates: Seq[(Int, Int)]): Future[Int] = {
    val q: DBIO[Seq[Int]] = DBIO.sequence(updates.map(update => {
      val (transactionId, integrationId) = update
      transactions.filter(_.id === transactionId).map(_.integrationId).update(integrationId)
    }))

    ctx.db.run(q.map(_.sum))
  }

  def getAllCreatedAfter(timestamp: Timestamp, integrationId: Int): Future[Seq[Transaction]] = {
    val q = transactions
      .filter(t => {
        t.createdAt > timestamp && t.integrationId === integrationId
      })
      .sortBy(_.createdAt.desc)
      .result

    ctx.db.run(q)
  }

  /**
    * Upsert Transaction record
    *
    * Identifying fields
    *   - eventSeatId
    *   - primaryTransactionId or secondaryTransactionId
    *   - integrationId
    *
    * Updatable fields
    *   - price
    *   - revenueCategoryId
    *   - timestamp
    *
    * @return ID of the inserted or updated record
    */
  def upsert(record: Transaction): Future[Int] = {
    ctx.db.run(upsertQuery(record).transactionally)
  }

  /**
    * Upsert transaction records in bulk
    *
    * Identifying fields
    *   - eventSeatId
    *   - primaryTransactionId or secondaryTransactionId
    *   - integrationId
    *   - transaction type
    *
    * Updatable fields
    *   - price
    *   - revenueCategoryId
    *   - timestamp
    *
    * @return Sequence of IDs corresponding to the upserted record(s)
    */
  def bulkUpsert(records: Seq[Transaction]): Future[Seq[Int]] = {
    val now = DateHelper.now()
    ctx.db.run(DBIO.sequence(records.map(upsertQuery(_, now))).transactionally)
  }

  private def upsertQuery(
    record: Transaction,
    now: Timestamp = DateHelper.now()
  ): DBIOAction[Int, NoStream, Effect.Read with Effect.Write] = {
    for {
      existing <- transactions
        .filter(_.eventSeatId === record.eventSeatId)
        .filter(
          t =>
            t.primaryTransactionId === record.primaryTransactionId || t.secondaryTransactionId === record.secondaryTransactionId
        )
        .filter(_.integrationId === record.integrationId)
        .filter(_.transactionType === record.transactionType)
        .result
        .headOption
      res <- (transactions returning transactions.map(_.id)).insertOrUpdate(
        record.copy(
          id = existing.flatMap(_.id),
          createdAt = existing.map(_.createdAt).getOrElse(now),
          modifiedAt = now
        )
      )
    } yield existing.flatMap(_.id).getOrElse(res.get)
  }
}
