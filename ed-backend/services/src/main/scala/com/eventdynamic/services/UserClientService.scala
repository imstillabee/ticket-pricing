package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.UserClient
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserClientService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {

  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val userClients = Tables.UserClients

  /** Create a client
    *
    * @param userId
    * @param clientId
    * @return created client id
    */
  def create(userId: Int, clientId: Int): Future[ResponseCode] = {
    val now = DateHelper.now()

    for {
      clientIds <- getAllClientIdsForUser(Some(userId))
      isDefault = clientIds.isEmpty
      newUserClient <- ctx.db
        .run(
          (userClients returning userClients) += UserClient(
            None,
            userId,
            clientId,
            isDefault,
            now,
            now
          )
        )
        .map(SuccessResponse(_))
    } yield {
      newUserClient
    }
  }

  /** Get user client mapping by user id and client id
    *
    * @param userId
    * @param clientId
    * @return UserClient object, if exists
    */
  def get(userId: Int, clientId: Int): Future[Option[UserClient]] = {
    ctx.db.run(
      userClients.filter(uc => uc.userId === userId && uc.clientId === clientId).result.headOption
    )
  }

  /** Get the default client id for a user by user id
    *
    * @param id - User Id
    * @return Default client id
    */
  def getDefaultClientIdForUserById(id: Option[Int]): Future[Option[Int]] = {
    ctx.db
      .run(
        userClients
          .filter(_.userId === id)
          .sortBy(_.createdAt.desc)
          .sortBy(_.isDefault.desc)
          .map(_.clientId)
          .result
          .headOption
      )
  }

  /** Get all client ids for a user
    *
    * @param userId - User Id
    * @return All client ids for a user
    */
  def getAllClientIdsForUser(userId: Option[Int]): Future[Seq[Int]] = {
    ctx.db.run(
      userClients
        .filter(_.userId === userId)
        .sortBy(_.createdAt.desc)
        .map(_.clientId)
        .result
    )
  }
}
