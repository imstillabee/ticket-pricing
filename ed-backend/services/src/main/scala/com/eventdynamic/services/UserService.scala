package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.{User, UserClient}
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}
import org.mindrot.jbcrypt.BCrypt
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

@Singleton
class UserService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  private val logger = LoggerFactory.getLogger(this.getClass)

  // Object to query tables from
  val clients = Tables.Clients
  val users = Tables.Users
  val userClients = Tables.UserClients

  val random = new Random()

  // Get a user by id
  def getById(id: Int): Future[Option[User]] = {
    ctx.db.run(users.filter(_.id === id).result.headOption)
  }

  // Get a user by email
  def getByEmail(email: String): Future[Option[User]] = {
    ctx.db.run(users.filter(_.email === email).result.headOption)
  }

  // Get all users with clientId
  def getAll(clientId: Int, isAdmin: Boolean): Future[Seq[User]] = {
    ctx.db.run(
      userClients
        .filter(_.clientId === clientId)
        .join(users)
        .on(_.userId === _.id)
        .map(_._2)
        .filterIf(!isAdmin)(!_.isAdmin)
        .result
    )

  }

  // Authenticate a user
  // First we grab the user by email
  // Then map that Future to a new one where we authenticate the password
  // In case of failure (connection failure) we return false
  def authenticate(email: String, password: String): Future[Boolean] = {
    // Gets the user from the database
    getByEmail(email)
      .map {
        case Some(user) => BCrypt.checkpw(password, user.password.getOrElse(""))
        case None       => false
      }
      .recover { case _ => false } // In case of failure
  }

  // Creates a user, returns the new user and temporary password if created
  // if no password is provided, creates a tempPass and emails it to user
  // the default password ttl is 96 hours
  def create(
    email: String,
    firstName: String,
    lastName: String,
    password: Option[String],
    clientId: Int,
    phoneNumber: Option[String],
    isAdmin: Boolean,
    passwordTtl: Int = 5760
  ): Future[(User, String)] = {
    val now = DateHelper.now()
    var pHash, tpHash, tpass: String = ""
    var tempExpire: Option[Timestamp] = Some(now)
    val sendEmail: Boolean = password.forall(_.isEmpty)
    logger.debug(s"in UserService.create - sendEmail is $sendEmail")
    if (sendEmail) {
      tpass = random.alphanumeric.take(8).mkString
      tpHash = BCrypt.hashpw(tpass, BCrypt.gensalt())
      val expires = DateHelper.later(passwordTtl)
      tempExpire = Some(expires)
    } else {
      pHash = BCrypt.hashpw(password.getOrElse(""), BCrypt.gensalt())
    }

    val user = User(
      None,
      now,
      now,
      email,
      firstName,
      lastName,
      Some(pHash),
      Some(tpHash),
      tempExpire,
      isAdmin,
      phoneNumber,
      clientId
    )

    // Transactionally create new user and new user-client mapping
    val query =
      (for {
        newUser <- (users returning users) += user
        _ <- DBIO
          .seq(
            (userClients returning userClients) += UserClient(
              None,
              newUser.id.get,
              clientId,
              true,
              now,
              now
            )
          )
          .map(_ => (newUser, tpass))
      } yield (newUser)).transactionally

    // Execute query
    ctx.db.run(query).map(newUser => (newUser, tpass))
  }

  // Deletes a user, returns true if a user was deleted
  def delete(id: Int): Future[Boolean] = {
    val user = users.filter(_.id === id)
    val userClient = userClients.filter(_.userId in user.map(_.id))
    ctx.db.run((userClient.delete andThen user.delete).transactionally).map(_ == 1)
  }

  // Deletes a user, returns true if a user was deleted
  def deleteByEmail(email: String): Future[Boolean] = {
    val user = users.filter(_.email === email)
    val userClient = userClients.filter(_.userId in user.map(_.id))
    ctx.db.run((userClient.delete andThen user.delete).transactionally).map(_ == 1)
  }

  // Creates a tempPass for a user
  // Returns an optional tuple containing the new user and temporary password if the user was found and None otherwise
  // the default password ttl is 24 hours
  def forgotPassword(email: String, passwordTtl: Int = 1440): Future[Option[(User, String)]] = {
    logger.debug(s"in UserService.forgotPassword - email is $email")
    getByEmail(email).flatMap {
      case Some(user) => {
        val tpass: String = random.alphanumeric.take(8).mkString
        addTempPass(user.id.get, tpass, passwordTtl).map {
          case NotFoundResponse => {
            logger.debug(s"in UserService.forgotPassword - User with email $email was not found")
            None
          }
          case SuccessResponse(user: User) => {
            Some((user, tpass))
          }
          case _ => None
        }
      }
      case _ => Future.successful(None)
    }
  }

  // Updates user information
  def updateUserInfo(
    id: Int,
    firstName: String,
    lastName: String,
    phoneNumber: Option[String]
  ): Future[ResponseCode] = {

    getById(id).flatMap {
      case Some(user) =>
        update(user.copy(firstName = firstName, lastName = lastName, phoneNumber = phoneNumber))
      case _ => Future.successful(NotFoundResponse)
    }
  }

  // Updates user email
  def updateEmail(id: Int, email: String): Future[ResponseCode] = {
    getById(id).flatMap {
      case Some(user) => update(user.copy(email = email))
      case _          => Future.successful(NotFoundResponse)
    }
  }

  // updates user password, and resets tempPass & tempExpire
  def changePassword(id: Int, newPassword: String): Future[ResponseCode] = {
    getById(id).flatMap {
      case Some(user) =>
        val hash = BCrypt.hashpw(newPassword, BCrypt.gensalt())
        val now = DateHelper.now()
        update(user.copy(password = Some(hash), tempPass = None, tempExpire = Some(now)))
      case _ => Future.successful(NotFoundResponse)
    }
  }

  // adds a tempPass password, and sets tempExpire
  def addTempPass(id: Int, tempPass: String, ttl: Int): Future[ResponseCode] = {
    getById(id).flatMap {
      case Some(user) =>
        val expires = DateHelper.later(ttl)
        val hash = BCrypt.hashpw(tempPass, BCrypt.gensalt())
        update(user.copy(tempPass = Some(hash), tempExpire = Some(expires)))
      case _ => Future.successful(NotFoundResponse)
    }
  }

  // Updates a user, returns success(userObject) if a user was updated
  def update(user: User): Future[ResponseCode] = {
    val now = DateHelper.now()
    ctx.db.run(users.filter(_.id === user.id).update(user.copy(modifiedAt = now)).map {
      case 0 => NotFoundResponse
      case _ => SuccessResponse(user)
    })
  }

  // Set the active client id for a user, returns success(userObject) if a user was updated
  def setActiveClient(userId: Int, clientId: Int): Future[Option[User]] = {
    for {
      foundUser <- ctx.db.run(
        users
          .filter(_.id === userId)
          .join(userClients)
          .on(_.id === _.userId)
          .filter(_._2.clientId === clientId)
          .map(_._1)
          .result
          .headOption
      )

      updatedUser <- if (foundUser.isDefined) {
        update(foundUser.get.copy(activeClientId = clientId)).map {
          case SuccessResponse(updatedUser: User) => Some(updatedUser)
          case _                                  => None
        }
      } else {
        Future.successful(None)
      }
    } yield { updatedUser }
  }
}
