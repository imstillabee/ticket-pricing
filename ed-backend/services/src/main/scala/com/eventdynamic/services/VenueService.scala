package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.Venue
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class VenueService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val venues = Tables.Venues
  val events = Tables.Events

  /** Create a venue
    *
    * @param name
    * @param capacity
    * @param zipCode
    * @param svgUrl
    * @return created venue id
    */
  def create(
    name: String,
    capacity: Int,
    zipCode: String,
    svgUrl: Option[String],
    timeZone: String = "America/New_York"
  ): Future[Int] = {
    val now = DateHelper.now()
    ctx.db.run(
      venues returning venues
        .map(_.id) += Venue(None, now, now, name, capacity, zipCode, timeZone, svgUrl)
    )
  }

  /** Get a venue by id
    *
    * @param id
    * @return venue if it exists
    */
  def getById(id: Int): Future[Option[Venue]] = {
    ctx.db.run(venues.filter(_.id === id).result.headOption)
  }

  /**
    * Get all the venues for a season
    *
    * @param seasonId season ID to filter by
    * @return distinct venues for the season
    */
  def getForSeason(seasonId: Int): Future[Seq[Venue]] = {
    val query = for {
      event <- events.filter(_.seasonId === seasonId)
      venue <- venues if venue.id === event.venueId
    } yield venue

    ctx.db.run(query.distinct.result)
  }

  /**
    * Get the venue for the event
    *
    * @param eventId event ID to filter by
    * @return venue, if it exissts
    */
  def getForEvent(eventId: Int): Future[Option[Venue]] = {
    val query = for {
      event <- events.filter(_.id === eventId)
      venue <- venues if venue.id === event.venueId
    } yield venue

    ctx.db.run(query.result.headOption)
  }
}
