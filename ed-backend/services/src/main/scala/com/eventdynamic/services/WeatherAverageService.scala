package com.eventdynamic.services

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.WeatherAverage
import com.eventdynamic.utils._
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class WeatherAverageService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {
  import ctx.profile.api._

  val weatherAverages = Tables.WeatherAverages

  def create(venueId: Int, month: Int, day: Int, hour: Int, temp: Double): Future[ResponseCode] = {
    val now = DateHelper.now()

    val q = (weatherAverages returning weatherAverages.map(_.id)) += WeatherAverage(
      None,
      now,
      now,
      venueId,
      month,
      day,
      hour,
      temp
    )
    ctx.db.run(q).map(SuccessResponse(_))
  }

  def get(venueId: Int, month: Int, day: Int, hour: Int): Future[Option[WeatherAverage]] = {
    val q = weatherAverages.filter(avg => {
      avg.venueId === venueId && avg.month === month && avg.day === day && avg.hour === hour
    })

    ctx.db.run(q.result.headOption)
  }
}
