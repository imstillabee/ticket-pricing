package com.eventdynamic.services.mlb

import com.eventdynamic.Tables
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.mlb.MySportsFeedInfo
import com.eventdynamic.utils.{DateHelper, ResponseCode, SuccessResponse}
import com.google.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class MySportsFeedInfoService @Inject()(val ctx: EDContext)(implicit ec: ExecutionContext) {

  // Import driver from database context
  import ctx.profile.api._

  /** Object to query table from */
  val teamAbbreviations = Tables.TeamAbbreviations

  /** Create a Team Abbreviation for a client
    *
    * @param abbreviation
    * @param clientId
    * @return
    */
  def create(abbreviation: String, clientId: Int): Future[ResponseCode] = {
    val now = DateHelper.now()
    val teamAbbreviation = ctx.db.run(
      teamAbbreviations returning teamAbbreviations.map(_.id) into (
        (
          teamAbbreviation,
          id
        ) =>
          teamAbbreviation
            .copy(id = Some(id))
      ) += MySportsFeedInfo(None, now, now, clientId, abbreviation)
    )
    teamAbbreviation.map(SuccessResponse(_))
  }

  /** Get a team abbreviation by client id
    *
    * @param clientId
    * @return
    */
  def getByClientId(clientId: Int): Future[Option[MySportsFeedInfo]] =
    ctx.db.run(teamAbbreviations.filter(_.clientId === clientId).result.headOption)

  /**
    * Get a team abbreviations by client ids
    *
    * @param clientIds
    * @return
    */
  def getByClientIds(clientIds: Seq[Int]): Future[Seq[MySportsFeedInfo]] =
    ctx.db.run(teamAbbreviations.filter(_.clientId.inSet(clientIds)).result)

}
