package com.eventdynamic.transactions

import com.eventdynamic.db.EDContext
import com.eventdynamic.models.{EventRow, PriceGuard}
import com.eventdynamic.services._
import com.google.inject.Inject
import org.slf4j.LoggerFactory
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}

class EventRowsTransaction @Inject()(
  val ctx: EDContext,
  eventService: EventService,
  seatService: SeatService,
  eventSeatService: EventSeatService,
  priceGuardService: PriceGuardService,
  transactionService: TransactionService
)(implicit val ec: ExecutionContext) {

  import ctx.profile.api._

  implicit val getEventRowResult = GetResult(
    r => EventRow(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, None, None, r.<<)
  )

  private val logger = LoggerFactory.getLogger(this.getClass)

  /** Get all EventRows for an Event, in section and row order
    *
    * @param eventId
    * @return
    */
  def getAllByEvent(eventId: Int): Future[Seq[EventRow]] = {
    for {
      eventRows <- getEventRows(eventId)
      priceGuards <- getPriceGuards(eventId, eventRows)
    } yield {
      val map = priceGuards.map(pg => ((pg.section, pg.row), pg)).toMap

      eventRows.map(er => {
        val pg = map.get((er.section, er.row))

        if (pg.isDefined) {
          er.copy(minimumPrice = Option(pg.get.minimumPrice), maximumPrice = pg.get.maximumPrice)
        } else er
      })
    }
  }

  private def getEventRows(eventId: Int): Future[Seq[EventRow]] = {
    val q = sql"""
      SELECT
        MAX(es."eventId") AS eventId,
        s."section" AS section,
        s."row" AS row,
        MAX(es."priceScaleId") AS priceScaleId,
        JSON_AGG(es."id") AS "seats",
        MAX(es."listedPrice") AS listedPrice,
        MAX(es."overridePrice") AS overridePrice,
        EVERY(es."isListed") AS isListed
      FROM "EventSeats" AS es

      INNER JOIN "Seats" AS s
      ON es."seatId" = s."id"

      LEFT JOIN "Events" as e
      ON es."eventId" = e."id"

      LEFT JOIN (SELECT DISTINCT ON ("eventSeatId") * FROM "Transactions" ORDER BY "eventSeatId", "timestamp" DESC, "type" ASC) lt on lt."eventSeatId" = es."id"

      WHERE es."eventId" = $eventId
        AND (lt.type IS NULL OR lt.type = 'Return')
        AND es."isHeld" = false
      GROUP BY s."section", s."row", "isListed"
      ORDER BY s."section", s."row"
      """.as[EventRow]

    ctx.db.run(q)
  }

  private def getPriceGuards(eventId: Int, eventRows: Seq[EventRow]): Future[Seq[PriceGuard]] = {
    val sections = eventRows.map(_.section).toSet
    val rows = eventRows.map(_.row).toSet

    for {
      priceGuards <- priceGuardService.getAllForEventsSectionsAndRows(Set(eventId), sections, rows)
    } yield priceGuards.toSet.toSeq
  }
}
