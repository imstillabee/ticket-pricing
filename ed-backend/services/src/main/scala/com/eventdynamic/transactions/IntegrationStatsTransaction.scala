package com.eventdynamic.transactions

import com.eventdynamic.db.EDContext
import com.eventdynamic.services._
import com.google.inject.{Inject, Singleton}
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}

case class TransactionIntegration(
  id: Int,
  name: String,
  logoUrl: String,
  isActive: Boolean,
  sold: Int,
  total: Int
)

@Singleton
class IntegrationStatsTransaction @Inject()(
  val ctx: EDContext,
  eventService: EventService,
  eventSeatService: EventSeatService
)(implicit val ec: ExecutionContext) {

  import ctx.profile.api._

  implicit val transactionGetResult = GetResult(
    r => TransactionIntegrationNoTotal(r.<<, r.<<, r.<<, r.<<, r.<<)
  )

  case class TransactionIntegrationNoTotal(
    id: Int,
    name: String,
    logoUrl: String,
    isActive: Boolean,
    sold: Int
  )

  /**
    * Calculates total sold per integration by season
    *
    * @param seasonId
    * @param eventId
    * @return
    */
  def getIntegrationTotals(
    clientId: Int,
    seasonId: Option[Int],
    eventId: Option[Int]
  ): Future[Seq[TransactionIntegration]] = {
    ctx.db
      .run(sql"""
              SELECT
                i."id" AS "id",
                i."name" AS "name",
                i."logoUrl" AS "logoUrl",
                ci."isActive" AS "isActive",
                COALESCE(sums."ticketsSold", 0) AS "sold"
              FROM
                "Integrations" i
                INNER JOIN "ClientIntegrations" ci ON ci."integrationId" = i."id"
                LEFT JOIN (
                  SELECT
                    eig."integrationId",
                    SUM(eig."revenue") as "revenue",
                    SUM(eig."ticketsSold") as "ticketsSold"
                  FROM
                    "EventIntegrationGroups" eig
                    INNER JOIN "Events" e ON e."id" = eig."eventId"
                  WHERE
                    e."clientId" = $clientId
                    AND (${seasonId.isEmpty} OR e."seasonId" = $seasonId)
                    AND (${eventId.isEmpty} OR eig."eventId" = $eventId)
                  GROUP BY
                    eig."integrationId"
                ) sums on sums."integrationId" = i."id"
              WHERE
                ci."clientId" = $clientId
           """.as[TransactionIntegrationNoTotal])
      .map(rows => {
        // Sum the sold column
        val sum = rows.map(_.sold).sum

        // Map the rows to the output with sold
        rows.map(
          row => TransactionIntegration(row.id, row.name, row.logoUrl, row.isActive, row.sold, sum)
        )
      })
  }
}
