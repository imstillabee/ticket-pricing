package com.eventdynamic.transactions

import com.eventdynamic.db.EDContext
import com.eventdynamic.services._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

case class ClientIntegrationTicket(
  clientIntegrationEventSeatId: Int,
  clientIntegrationListingId: Int,
  clientIntegrationListingExternalId: String,
  eventSeatId: Int,
  row: String,
  seat: String
)

class IntegrationTransactions(
  val ctx: EDContext,
  val clientIntegrationService: ClientIntegrationService,
  val clientIntegrationEventsService: ClientIntegrationEventsService,
  val clientIntegrationEventSeatsService: ClientIntegrationEventSeatsService,
  val clientIntegrationListingsService: ClientIntegrationListingsService,
  val eventsService: EventService,
  val eventSeatService: EventSeatService,
  val integrationService: IntegrationService,
  val seatService: SeatService
) {

  import ctx.profile.api._

  /** Get Mappings of ED Event Ids to Integration Event Ids
    *
    * @param clientIntegrationId
    * @param seasonId
    * @return
    */
  def getIntegrationEventIds(clientIntegrationId: Int, seasonId: Option[Int]): Map[Int, String] = {
    val query = for {
      cie <- clientIntegrationEventsService.clientIntegrationEvents
      if cie.clientIntegrationId === clientIntegrationId
      e <- eventsService.events.filterOpt(seasonId)(_.seasonId === _) if e.id === cie.eventId
    } yield e.id -> cie.integrationEventId

    val action = ctx.db
      .run(query.result)
      .map(_.toMap)

    Await.result(action, 480.seconds)
  }

  /** Get Mappings of ED EventIds to ClientIntegrationEvents ids
    *
    * @param clientIntegrationId
    * @param seasonId
    * @return
    */
  def getClientIntegrationEventIds(
    clientIntegrationId: Int,
    seasonId: Option[Int]
  ): Map[Int, Int] = {
    val query = for {
      cie <- clientIntegrationEventsService.clientIntegrationEvents
      if cie.clientIntegrationId === clientIntegrationId
      e <- eventsService.events.filterOpt(seasonId)(_.seasonId === _) if e.id === cie.eventId
    } yield e.id -> cie.id

    val action = ctx.db
      .run(query.result)
      .map(_.toMap)

    Await.result(action, 480.seconds)
  }

  /** Get ClientIntegration Event Seat Listing
    *
    * @param eventSeatIds event seat IDs to filter by
    * @param clientIntegrationId clientIntegrationId to filter by
    * @return
    */
  def getClientEventSeatListing(
    eventSeatIds: Set[Int],
    clientIntegrationId: Int
  ): Future[Seq[ClientIntegrationTicket]] = {

    val query = for {
      es <- eventSeatService.eventSeats if es.id inSet eventSeatIds
      e <- eventsService.events if es.eventId === e.id
      cies <- clientIntegrationEventSeatsService.clientIntegrationEventSeats
      if cies.eventSeatId === es.id
      cil <- clientIntegrationListingsService.clientIntegrationListings
      if cil.id === cies.clientIntegrationListingId
      cie <- clientIntegrationEventsService.clientIntegrationEvents
      if cie.clientIntegrationId === clientIntegrationId
      s <- seatService.seats if s.id === es.seatId
    } yield (cies.id, cil.id, cil.listingId, es.id, s.row, s.seat)

    ctx.db.run(
      query.distinct.result
        .map(tickets => tickets.map(ticket => (ClientIntegrationTicket.apply _).tupled(ticket)))
    )
  }

  /** Get Mapping of EventSeat id to ClientIntegrationListing id
    *
    * @param clientIntegrationId
    * @return
    */
  def getListingsMap(clientIntegrationId: Int): Future[Map[Int, String]] = {
    val query = for {
      es <- eventSeatService.eventSeats
      e <- eventsService.events if es.eventId === e.id
      cies <- clientIntegrationEventSeatsService.clientIntegrationEventSeats
      if cies.eventSeatId === es.id
      cil <- clientIntegrationListingsService.clientIntegrationListings
      if cil.id === cies.clientIntegrationListingId
      cie <- clientIntegrationEventsService.clientIntegrationEvents
      if cie.id === cil.clientIntegrationEventsId && cie.clientIntegrationId === clientIntegrationId
    } yield es.id -> cil.listingId

    ctx.db.run(query.result).map(_.toMap)
  }

  /** delete a client integration event seat
    *
    * @param ids
    * @return
    */
  def deleteClientIntegrationEventSeats(ids: Seq[Int]): Future[Int] = {
    val query = clientIntegrationEventSeatsService.clientIntegrationEventSeats
      .filter(_.id inSet ids)
      .delete
      .transactionally

    ctx.db.run(query)
  }

  /** Delete Listing
    *
    * @param listingId
    * @return
    */
  def deleteListing(listingId: Int): Future[Int] = {
    val query =
      clientIntegrationListingsService.clientIntegrationListings.filter(_.id === listingId).delete

    ctx.db.run(query)
  }
}
