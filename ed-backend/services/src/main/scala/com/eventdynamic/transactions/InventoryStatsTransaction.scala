package com.eventdynamic.transactions

import com.eventdynamic.db.EDContext
import com.google.inject.{Inject, Singleton}
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}

case class InventoryStats(
  eventId: Option[Int],
  seasonId: Option[Int],
  total: Int,
  soldIndividual: Int,
  soldOther: Int,
  revenue: BigDecimal,
  unsold: Int
) {
  val soldTotal = soldIndividual + soldOther
  val remaining = Math.max(0, total - soldTotal)
}

@Singleton
class InventoryStatsTransaction @Inject()(val ctx: EDContext)(implicit val ec: ExecutionContext) {
  import ctx.profile.api._

  implicit val getInventoryStatsResult: AnyRef with GetResult[InventoryStats] = GetResult(
    r =>
      InventoryStats(
        r.nextIntOption(),
        r.nextIntOption(),
        r.nextInt(),
        r.nextInt(),
        r.nextInt(),
        r.nextBigDecimal(),
        r.nextInt()
    )
  )

  /**
    * Gets a single set of inventory stats for event or season
    *
    * @param clientId required clientId filter
    * @param eventId optional eventId filter
    * @param seasonId optional seasonId filter
    * @return
    */
  def getInventory(
    clientId: Int,
    eventId: Option[Int] = None,
    seasonId: Option[Int] = None
  ): Future[Option[InventoryStats]] = {
    val eventSeq = if (eventId.isDefined) Seq(eventId.get) else Seq()
    for {
      inventorySeq <- getInventoryForEvents(clientId, eventSeq, seasonId)
    } yield {
      val totalInv = inventorySeq.map(inv => inv.total).reduceOption((a, b) => a + b).getOrElse(0)
      val soldIndividualInv =
        inventorySeq.map(inv => inv.soldIndividual).reduceOption((a, b) => a + b).getOrElse(0)
      val soldOtherInv =
        inventorySeq.map(inv => inv.soldOther).reduceOption((a, b) => a + b).getOrElse(0)
      val revenue =
        inventorySeq.map(inv => inv.revenue).reduceOption((a, b) => a + b).getOrElse(BigDecimal(0))
      val unsold = inventorySeq.map(inv => inv.unsold).reduceOption((a, b) => a + b).getOrElse(0)
      Some(
        InventoryStats(
          eventId,
          seasonId,
          totalInv,
          soldIndividualInv,
          soldOtherInv,
          revenue,
          unsold
        )
      )
    }
  }

  /**
    * Gets inventory stats for a series of events
    *
    * @param clientId required clientId filter
    * @param eventIds optional sequence of eventId filter
    * @param seasonId optional seasonId filter
    * @return
    */
  def getInventoryForEvents(
    clientId: Int,
    eventIds: Seq[Int],
    seasonId: Option[Int] = None
  ): Future[Seq[InventoryStats]] = {
    val commaSeparatedEventIds = eventIds.mkString(",")

    val query =
      sql"""
         WITH "eventTimeGroups" as (
          SELECT
            etg."eventId" as "eventId",
            SUM(etg."revenue") as "revenue"
          FROM
            "Events" e
            INNER JOIN "EventTimeGroups" etg ON e.id = etg."eventId"
          WHERE
            e."clientId" = $clientId
            AND (${seasonId.isEmpty} OR e."seasonId" = $seasonId)
            AND (${commaSeparatedEventIds.isEmpty} OR e."id" IN (#${if (commaSeparatedEventIds.isEmpty)
        null
      else commaSeparatedEventIds}))
          GROUP BY etg."eventId"
         )
         SELECT
          e."id" as "eventId",
          e."seasonId" as "seasonId",
          COALESCE(SUM(esc."total"), 0) as "total",
          COALESCE(SUM(esc."soldIndividual"), 0) as "soldIndividual",
          COALESCE(SUM(esc."soldOther"), 0) as "soldOther",
          COALESCE(SUM(etg."revenue"), 0) as "revenue",
          COALESCE(SUM(esc."unsold"), 0) as "unsold"
         FROM
          "EventSeatCounts" esc
          INNER JOIN "Events" e on esc."eventId" = e."id"
          LEFT JOIN "eventTimeGroups" etg on e."id" = etg."eventId"
         WHERE
          e."clientId" = $clientId
          AND (${seasonId.isEmpty} OR e."seasonId" = $seasonId)
          AND (${commaSeparatedEventIds.isEmpty} OR e."id" IN (#${if (commaSeparatedEventIds.isEmpty)
        null
      else commaSeparatedEventIds}))
         GROUP BY e."id", e."seasonId";
      """.as[InventoryStats]

    ctx.db.run(query)
  }
}
