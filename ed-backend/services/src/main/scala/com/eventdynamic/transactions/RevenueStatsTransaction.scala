package com.eventdynamic.transactions

import com.eventdynamic.db.EDContext
import com.eventdynamic.models.RevenueCategoryRevenue
import com.eventdynamic.services.{
  EventSeatService,
  EventService,
  RevenueCategoryService,
  TransactionService
}
import com.google.inject.{Inject, Singleton}
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class RevenueStatsTransaction @Inject()(
  val ctx: EDContext,
  eventService: EventService,
  eventSeatService: EventSeatService,
  revenueCategoryService: RevenueCategoryService,
  transactionService: TransactionService
)(implicit val ec: ExecutionContext) {
  import ctx.profile.api._

  implicit val getRevCatResult: AnyRef with GetResult[RevenueCategoryRevenue] = GetResult(
    r => RevenueCategoryRevenue(r.nextInt(), r.nextString(), r.nextBigDecimal(), r.nextInt())
  )

  /**
    * Gets revenue category with calculated revenue and inventory values
    *
    * @param clientId required clientId filter
    * @param eventId optional eventId filter
    * @param seasonId optional seasonId filter
    * @return
    */
  def getRevenueByCategory(
    clientId: Int,
    eventId: Option[Int] = None,
    seasonId: Option[Int] = None
  ): Future[Seq[RevenueCategoryRevenue]] = {

    val query =
      sql"""
           SELECT
           	rc."id" AS "id",
           	MAX(rc."name") AS "name",
           	SUM(COALESCE(erg."revenue", 0)) AS "revenue",
           	SUM(COALESCE(erg."ticketsSold", 0)) AS "ticketsSold"
           FROM
           	"EventRevenueGroups" erg
           	INNER JOIN "Events" e ON
           		e."id" = erg."eventId"
              AND e."clientId" = $clientId
              AND (${seasonId.isEmpty} OR e."seasonId" = $seasonId)
              AND (${eventId.isEmpty} OR erg."eventId" = $eventId)
            RIGHT JOIN "RevenueCategoryMappings" rcm ON erg."revenueCategoryId" = rcm."revenueCategoryId"
            JOIN "RevenueCategories" rc ON rcm."revenueCategoryId" = rc."id"
            WHERE rc."isDisplayed" = true
              AND rcm."clientId" = $clientId
           GROUP BY
             rc."id";
      """.as[RevenueCategoryRevenue]

    ctx.db.run(query)
  }
}
