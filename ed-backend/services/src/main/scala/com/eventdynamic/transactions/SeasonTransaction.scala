package com.eventdynamic.transactions

import java.sql.Timestamp

import com.eventdynamic.db.EDContext
import com.eventdynamic.services.{EventSeatService, EventService, TransactionService}
import com.google.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class SeasonTransaction @Inject()(
  val ctx: EDContext,
  eventService: EventService,
  eventSeatService: EventSeatService,
  transactionService: TransactionService
)(implicit val ec: ExecutionContext) {
  import ctx.profile.api._

  /**
    * Returns the latest event start date for this season.
    *
    * @param seasonId
    * @return
    */
  def getLastEventBySeason(seasonId: Int): Future[Option[Timestamp]] = {
    ctx.db.run(
      eventService.events
        .filter(_.seasonId === seasonId)
        .map(_.timestamp)
        .max
        .result
    )
  }

  /**
    * Returns the minimum timestamp for transactions in the given season.
    *
    * @param seasonId
    * @return
    */
  def getEarliestTransactionDateBySeason(seasonId: Int): Future[Option[Timestamp]] = {
    val joinQuery = for {
      e <- eventService.events if e.seasonId === seasonId
      es <- eventSeatService.eventSeats if es.eventId === e.id
      t <- transactionService.transactions if t.eventSeatId === es.id
    } yield t

    val minDate = joinQuery.map(_.timestamp).min

    ctx.db.run(minDate.result)
  }
}
