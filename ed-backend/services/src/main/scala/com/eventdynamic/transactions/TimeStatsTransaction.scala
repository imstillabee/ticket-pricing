package com.eventdynamic.transactions

import java.sql.Timestamp
import java.time.temporal.ChronoUnit
import java.time._

import com.eventdynamic.db.EDContext
import com.eventdynamic.models.{TimeStat, TimeStatsMeta, TimeStatsResponse}
import com.eventdynamic.services.{
  EventService,
  EventTimeGroupsService,
  ProjectionService,
  VenueService
}
import com.eventdynamic.utils.DateHelper
import com.eventdynamic.utils.DateHelper.timestampOrdering
import com.google.inject.{Inject, Singleton}
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class TimeStatsTransaction @Inject()(
  val ctx: EDContext,
  val inventoryStatsTransaction: InventoryStatsTransaction,
  val eventService: EventService,
  val eventTimeGroupsService: EventTimeGroupsService,
  val projectionService: ProjectionService,
  val venueService: VenueService
)(implicit val ec: ExecutionContext) {
  import ctx.profile.api._

  def all(filters: Option[Rep[Boolean]]*): ctx.profile.api.Rep[Boolean] = {
    filters
      .collect({ case Some(f) => f })
      .reduceLeftOption(_ && _)
      .getOrElse(true: Rep[Boolean])
  }

  implicit val getTimeStatResult: AnyRef with GetResult[TimeStat] = GetResult(
    r =>
      TimeStat(
        r.nextTimestamp(),
        r.nextTimestamp(),
        r.nextBigDecimalOption(),
        r.nextIntOption(),
        r.nextBigDecimalOption(),
        r.nextIntOption(),
        r.nextBoolean()
    )
  )

  /**
    * Calculate proper time interval based on the distance between the start and end timestamp
    *
    * @param start start timestamp
    * @param end end timestamp
    * @return interval in hours
    */
  private def calculateInterval(start: LocalDate, end: LocalDate): ChronoUnit = {
    val daysInRange = ChronoUnit.DAYS.between(start, end)

    if (daysInRange < 14) {
      ChronoUnit.HOURS
    } else {
      ChronoUnit.DAYS
    }
  }

  /**
    * Get actual time stats matching filter (queries event seats to get the total inventory first)
    */
  def getActual(
    start: ZonedDateTime,
    end: ZonedDateTime,
    zoneId: ZoneId,
    interval: ChronoUnit,
    clientId: Int,
    eventId: Option[Int],
    seasonId: Option[Int]
  ): Future[Seq[TimeStat]] = {
    val from = start.toInstant
    val to = end.toInstant
    for {
      inventory <- inventoryStatsTransaction
        .getInventory(clientId, eventId, seasonId)
        .map {
          case None    => throw new Exception("Could not fetch inventory")
          case Some(i) => i.total
        }
      actual <- eventTimeGroupsService.getActualTimeStats(
        clientId,
        from,
        to,
        inventory,
        seasonId,
        eventId
      )
    } yield {
      coalesceToInterval(actual, interval, zoneId)
    }
  }

  private def coalesceToInterval(
    timeStats: Seq[TimeStat],
    interval: ChronoUnit,
    zoneId: ZoneId
  ): Seq[TimeStat] = {
    timeStats
      .groupBy(ts => {
        ts.intervalStart.toInstant.atZone(zoneId).truncatedTo(interval)
      })
      .map(group => {
        val (startTime, items) = group
        val inventory = items.map(_.inventory.getOrElse(0)).sum
        val revenue = items.map(_.revenue.getOrElse(BigDecimal(0.0))).sum
        TimeStat(
          intervalStart = Timestamp.from(startTime.toInstant),
          intervalEnd = Timestamp.from(startTime.plus(1, interval).toInstant),
          revenue = Some(revenue),
          inventory = Some(inventory),
          cumulativeRevenue = items.last.cumulativeRevenue,
          cumulativeInventory = items.last.cumulativeInventory,
          isProjected = items.last.isProjected
        )
      })
      .toSeq
      .sortBy(_.intervalStart)
  }

  /**
    * Fetch the zoneId for the event or season by querying the venue
    */
  private def getZoneId(
    clientId: Int,
    seasonId: Option[Int],
    eventId: Option[Int]
  ): Future[ZoneId] = {
    val venueFuture = if (eventId.isDefined) {
      venueService.getForEvent(eventId.get).map {
        case Some(venue) => venue
        case None        => throw new Exception(s"Venue does not exist for event $eventId")
      }
    } else if (seasonId.isDefined) {
      venueService.getForSeason(seasonId.get).map {
        case res if res.size == 1 => res.head
        case res if res.isEmpty   => throw new Exception(s"Venue does not exist for season $seasonId")
        case _ =>
          throw new Exception("Cannot handle seasons with events for different venues")
      }
    } else {
      throw new IllegalArgumentException("eventId or seasonId must be defined")
    }

    venueFuture.map(v => ZoneId.of(v.timeZone))
  }

  private def fillRangeWithEmptyTimeStats(
    start: Instant,
    end: Instant,
    step: Duration,
    timeStats: Seq[TimeStat],
    isProjected: Boolean = false
  ): Seq[TimeStat] = {
    DateHelper
      .dateRange(start, end, step)
      .map(range => {
        val (rangeStart, rangeEnd) = range

        val matching = timeStats.find(_.intervalEnd.toInstant == rangeEnd)

        matching.getOrElse(
          TimeStat(
            intervalStart = Timestamp.from(rangeStart),
            intervalEnd = Timestamp.from(rangeEnd),
            revenue = None,
            inventory = None,
            cumulativeRevenue = None,
            cumulativeInventory = None,
            isProjected = isProjected
          )
        )
      })
      .toSeq
  }

  /**
    * Get time stats matching the event and season filters
    *
    * @param start start time
    * @param end end time
    * @param clientId client ID filter
    * @param eventId event ID filter
    * @param seasonId season ID filter
    * @return list of time stats
    */
  def get(
    start: LocalDate,
    end: LocalDate,
    clientId: Int,
    eventId: Option[Int] = None,
    seasonId: Option[Int] = None,
    now: Instant = Instant.now()
  ): Future[TimeStatsResponse] = {
    val interval = calculateInterval(start, end)

    for {
      zoneId <- getZoneId(clientId, seasonId, eventId)

      roundedNow = now.atZone(zoneId).truncatedTo(ChronoUnit.DAYS).plus(1, ChronoUnit.DAYS)
      roundedStart = start.atStartOfDay(zoneId)
      roundedEnd = end.atStartOfDay(zoneId).plus(1, ChronoUnit.DAYS)

      actual <- if (interval == ChronoUnit.HOURS) {
        getActual(roundedStart, roundedEnd, zoneId, interval, clientId, eventId, seasonId)
      } else {
        getActual(
          roundedStart,
          DateHelper.min(roundedNow, roundedEnd),
          zoneId,
          interval,
          clientId,
          eventId,
          seasonId
        )
      }

      projected <- if (interval == ChronoUnit.DAYS) {
        projectionService
          .getProjectedTimeStats(
            from = DateHelper.max(roundedNow, roundedStart).toInstant,
            to = roundedEnd.toInstant,
            seasonId,
            eventId = eventId,
            startingInventory = actual.lastOption.flatMap(_.cumulativeInventory),
            startingRevenue = actual.lastOption.flatMap(_.cumulativeRevenue)
          )
          .map(
            fillRangeWithEmptyTimeStats(
              DateHelper.max(roundedNow, roundedStart).toInstant,
              roundedEnd.toInstant,
              Duration.of(1, interval),
              _,
              isProjected = true
            )
          )
      } else {
        Future.successful(Seq())
      }
    } yield {
      TimeStatsResponse(
        meta = TimeStatsMeta(timeZone = zoneId.toString, interval = interval.toString),
        data = actual ++ projected
      )
    }
  }
}
