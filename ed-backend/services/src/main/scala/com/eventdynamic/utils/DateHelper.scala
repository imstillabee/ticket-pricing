package com.eventdynamic.utils

import java.sql.Timestamp
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.time.{Duration, Instant, ZonedDateTime}
import java.util.{Calendar, TimeZone}

object DateHelper {
  def now(): Timestamp = Timestamp.from(Instant.now())

  /**
    * returns the timestamp for a time the specified number of minutes in the future
    *
    * @param minutes
    * @return
    */
  def later(minutes: Int, timestamp: Timestamp = now()): Timestamp = {
    Timestamp.from(timestamp.toInstant.plus(minutes, ChronoUnit.MINUTES))
  }

  /**
    * returns the timestamp for a time the specified number of minutes in the past
    *
    * @param minutes
    * @return
    */
  def earlier(minutes: Int, timestamp: Timestamp = now()): Timestamp = {
    Timestamp.from(timestamp.toInstant.minus(minutes, ChronoUnit.MINUTES))
  }

  /**
    * Prints timestamp to ISO string
    *
    * @param ts
    * @return
    */
  def toISOString(ts: Timestamp): String = {
    ts.toLocalDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
  }

  /**
    * Round a timestamp down to the nearest hour
    */
  def startOfHour(ts: Timestamp): Timestamp = {
    val cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
    cal.setTime(ts)

    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)

    new Timestamp(cal.getTime.getTime)
  }

  /**
    * Round a timestamp down to the nearest hour
    */
  def endOfHour(ts: Timestamp): Timestamp = {
    val cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
    cal.setTime(ts)

    cal.set(Calendar.MINUTE, 59)
    cal.set(Calendar.SECOND, 59)
    cal.set(Calendar.MILLISECOND, 999)

    new Timestamp(cal.getTime.getTime)
  }

  /**
    * Round a timestamp to the end of day
    */
  def endOfDay(ts: Timestamp): Timestamp = {
    val cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
    cal.setTime(ts)

    cal.set(Calendar.HOUR_OF_DAY, 23)
    cal.set(Calendar.MINUTE, 59)
    cal.set(Calendar.SECOND, 59)
    cal.set(Calendar.MILLISECOND, 999)

    new Timestamp(cal.getTime.getTime)
  }

  /**
    * Ordering to use for Timestamps
    */
  implicit val timestampOrdering: Ordering[Timestamp] = Ordering.by(_.getTime)
  implicit val instantOrdering: Ordering[Instant] = Ordering.by(_.getEpochSecond)

  def dateRange(from: Instant, to: Instant, step: Duration): Iterator[(Instant, Instant)] = {
    Iterator
      .iterate(from)(_.plus(step))
      .takeWhile(_.isBefore(to))
      .map(rangeStart => {
        val rangeStartPlusStep = rangeStart.plus(step)
        val rangeEnd = if (rangeStartPlusStep.isBefore(to)) rangeStartPlusStep else to
        (rangeStart, rangeEnd)
      })
  }

  def min(a: ZonedDateTime, b: ZonedDateTime): ZonedDateTime = {
    if (b.isBefore(a)) b else a
  }

  def max(a: ZonedDateTime, b: ZonedDateTime): ZonedDateTime = {
    if (b.isAfter(a)) b else a
  }

}
