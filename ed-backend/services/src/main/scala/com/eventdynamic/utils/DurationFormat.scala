package com.eventdynamic.utils

import java.time.Duration

import play.api.libs.json._

trait DurationFormat {
  implicit val durationFormat: Format[Duration] = new Format[Duration] {
    def writes(d: Duration): JsValue = Json.toJson(d.toString)

    def reads(jsValue: JsValue): JsResult[Duration] = {
      try {
        val duration = Duration.parse(jsValue.as[String])
        JsSuccess(duration)
      } catch {
        case _: Throwable => JsError(s"'${jsValue.as[String]}' is not a valid duration")
      }
    }
  }
}
