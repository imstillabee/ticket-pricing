package com.eventdynamic.utils

import scala.concurrent.{ExecutionContext, Future}

object FutureUtil {

  /**
    * This function chains a sequence of futures together to make them run serially instead of in parallel. We are
    * using this to spread out the save processes.
    *
    * Adapted from this code:
    * https://www.michaelpollmeier.com/execute-scala-futures-in-serial-one-after-the-other-non-blocking
    *
    * @param l iterable list
    * @param n max futures to have in flight at once
    * @param fn a function to apply to each element in the list (think .map function)
    * @tparam A the type of elements in the list
    * @tparam B the output type
    * @return a single future that will finish after each map is complete, outputs inside the list
    */
  def serializeFutures[A, B](l: Iterable[A], n: Int = 1)(
    fn: A => Future[B]
  )(implicit ec: ExecutionContext): Future[List[B]] = {
    l.grouped(n).foldLeft(Future(List.empty[B])) { (previousFuture, chunk) =>
      for {
        previousResults <- previousFuture
        next <- Future.sequence(chunk.map(fn))
      } yield previousResults ++ next
    }
  }
}
