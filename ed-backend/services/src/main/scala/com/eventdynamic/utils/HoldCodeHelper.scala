package com.eventdynamic.utils

object HoldCodeHelper {

  def checkIsHeld(holdCodeType: String): Boolean = {
    val openHoldCodes = Set("O")
    !openHoldCodes.contains(holdCodeType)
  }
}
