package com.eventdynamic.utils

import java.time.Instant

import play.api.libs.json.{Format, JsResult, JsSuccess, JsValue}
import play.api.libs.json.Json.toJson

trait InstantISOFormat {
  implicit val timestampFormat: Format[Instant] = new Format[Instant] {
    def writes(t: Instant): JsValue = toJson(t.toString)
    def reads(json: JsValue): JsResult[Instant] = JsSuccess(Instant.parse(json.as[String]))
  }
}
