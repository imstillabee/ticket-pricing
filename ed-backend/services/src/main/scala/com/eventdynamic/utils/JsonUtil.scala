package com.eventdynamic.utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule

object JsonUtil {
  val mapper = new ObjectMapper()
  mapper.registerModule(DefaultScalaModule)

  def stringify[A](item: A): String = {
    mapper.writeValueAsString(item)
  }
}
