package com.eventdynamic.utils

import java.time.temporal.ChronoUnit

import com.eventdynamic.models.Projection

trait ProjectionUtil {

  def generateNProjections(
    n: Int,
    varyTimestamp: Boolean,
    projection: Projection
  ): Seq[Projection] = {
    val projections = Seq.fill(n)(projection)
    if (varyTimestamp) {
      projections.map(p => {
        val min = p.timestamp.truncatedTo(ChronoUnit.HOURS)
        val max = min.plus(1, ChronoUnit.HOURS)
        p.copy(timestamp = RandomModelUtil.instant(min, max))
      })
    } else {
      projections
    }
  }

  def generateNProjectionMappings(n: Int, mapping: Map[String, Any]): List[Map[String, Any]] = {
    List.fill(n)(mapping)
  }
}
