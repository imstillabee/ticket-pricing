package com.eventdynamic.utils

import java.sql.Timestamp
import java.time.Instant

import com.eventdynamic.models._

import scala.util.Random

/**
  * Utility to create random models intended for use in unit tests. For creating random DB records, use
  * com.eventdynamic.test.RandomUtil instead.
  */
object RandomModelUtil {
  def string(len: Int = 20): String = Random.nextString(len)
  def int(): Int = Random.nextInt()
  def boolean(): Boolean = Random.nextBoolean()
  def double(): Double = Random.nextDouble()
  def float(): Float = Random.nextFloat()
  def timestamp(): Timestamp = DateHelper.now()

  def long(min: Long = Long.MinValue, max: Long = Long.MaxValue): Long = {
    if (max <= min) {
      throw new IllegalArgumentException("min should be less than max")
    }
    val diff = max - min
    min + (Math.abs(Random.nextLong()) % diff)
  }

  def instant(min: Instant = Instant.MIN, max: Instant = Instant.MAX): Instant = {
    if (max.isBefore(min)) {
      throw new IllegalArgumentException("min should be less than max")
    }

    Instant.ofEpochMilli(long(min.toEpochMilli, max.toEpochMilli))
  }

  def event(
    id: Option[Int] = Some(int()),
    createdAt: Timestamp = new Timestamp(0),
    modifiedAt: Timestamp = new Timestamp(0),
    primaryEventId: Option[Int] = None,
    name: String = string(),
    timestamp: Option[Timestamp] = None,
    clientId: Int = 0,
    venueId: Int = 0,
    eventCategoryId: Int = 0,
    seasonId: Option[Int] = None,
    isBroadcast: Boolean = true,
    percentPriceModifier: Int = 0,
    eventScore: Option[Double] = None,
    eventScoreModifier: Double = 0,
    spring: Option[Double] = None,
    springModifier: Double = double()
  ): Event = {
    Event(
      id,
      primaryEventId,
      createdAt,
      modifiedAt,
      name,
      timestamp,
      clientId,
      venueId,
      eventCategoryId,
      seasonId,
      isBroadcast,
      percentPriceModifier,
      eventScore,
      eventScoreModifier,
      spring,
      springModifier
    )
  }

  def eventSeat(
    id: Option[Int] = Some(int()),
    seatId: Int = int(),
    eventId: Int = int(),
    priceScaleId: Int = int(),
    listedPrice: Option[BigDecimal] = None,
    overridePrice: Option[BigDecimal] = None,
    isListed: Boolean = boolean(),
    createdAt: Timestamp = new Timestamp(0),
    modifiedAt: Timestamp = new Timestamp(0),
    isHeld: Boolean = boolean()
  ): EventSeat = {
    EventSeat(
      id,
      seatId,
      eventId,
      priceScaleId,
      listedPrice,
      overridePrice,
      isListed,
      createdAt,
      modifiedAt,
      isHeld
    )
  }

  def seat(
    id: Option[Int] = Some(int()),
    primarySeatId: Option[Int] = None,
    createdAt: Timestamp = new Timestamp(0),
    modifiedAt: Timestamp = new Timestamp(0),
    seat: String = string(),
    row: String = string(),
    section: String = string(),
    venueId: Int = int()
  ): Seat = {
    Seat(id, primarySeatId, createdAt, modifiedAt, seat, row, section, venueId)
  }

  def priceScale(
    id: Option[Int] = Some(int()),
    name: String = string(),
    venueId: Int = int(),
    integrationId: Int = int(),
    createdAt: Timestamp = timestamp(),
    modifiedAt: Timestamp = timestamp()
  ): PriceScale = {
    PriceScale(id, name, venueId, integrationId, createdAt, modifiedAt)
  }

  def clientIntegrationComposite(
    id: Option[Int] = Some(int()),
    createdAt: Timestamp = timestamp(),
    modifiedAt: Timestamp = timestamp(),
    clientId: Int = int(),
    integrationId: Int = int(),
    name: String = string(),
    version: Option[String] = None,
    json: Option[String] = None,
    isActive: Boolean = boolean(),
    isPrimary: Boolean = boolean(),
    logoUrl: Option[String] = None,
    percent: Option[Int] = None,
    constant: Option[BigDecimal] = None
  ): ClientIntegrationComposite = {
    ClientIntegrationComposite(
      id,
      createdAt,
      modifiedAt,
      clientId,
      integrationId,
      name,
      version,
      json,
      isActive,
      isPrimary,
      logoUrl,
      percent,
      constant
    )
  }

  def clientIntegrationEvent(
    id: Option[Int] = Some(int()),
    createdAt: Timestamp = timestamp(),
    clientIntegrationId: Int = int(),
    eventId: Int = int(),
    integrationEventId: String = string(),
    modifiedAt: Timestamp = timestamp
  ): ClientIntegrationEvent = {
    ClientIntegrationEvent(
      id,
      createdAt,
      clientIntegrationId,
      eventId,
      integrationEventId,
      modifiedAt,
    )
  }

  def timeStat(
    intervalStart: Timestamp = timestamp(),
    intervalEnd: Timestamp = timestamp(),
    revenue: Option[BigDecimal] = Some(BigDecimal(double())),
    inventory: Option[Int] = Some(int()),
    cumulativeRevenue: Option[BigDecimal] = Some(BigDecimal(double())),
    cumulativeInventory: Option[Int] = Some(int()),
    isProjected: Boolean = false
  ): TimeStat = {
    TimeStat(
      intervalStart,
      intervalEnd,
      revenue,
      inventory,
      cumulativeRevenue,
      cumulativeInventory,
      isProjected
    )
  }

  def venue(
    id: Option[Int] = Some(int()),
    createdAt: Timestamp = timestamp(),
    modifiedAt: Timestamp = timestamp(),
    name: String = string(),
    capacity: Int = int(),
    zipCode: String = string(),
    timeZone: String = "America/New_York",
    svgUrl: Option[String] = None
  ): Venue = {
    Venue(id, createdAt, modifiedAt, name, capacity, zipCode, timeZone, svgUrl)
  }

  def projection(
    id: Option[Int] = Some(int()),
    createdAt: Timestamp = new Timestamp(0),
    modifiedAt: Timestamp = new Timestamp(0),
    eventId: Int = int(),
    timestamp: Instant = Instant.EPOCH,
    inventory: Int = int() % 10000,
    revenue: BigDecimal = BigDecimal(double() % 10000)
  ): Projection = {
    Projection(
      id,
      createdAt,
      modifiedAt,
      eventId,
      timestamp,
      inventory,
      inventory,
      inventory,
      revenue,
      revenue,
      revenue
    )
  }
}
