package com.eventdynamic.utils

sealed trait ResponseCode

case class SuccessResponse[T](payload: T) extends ResponseCode
case object NotFoundResponse extends ResponseCode
case object DuplicateResponse extends ResponseCode
