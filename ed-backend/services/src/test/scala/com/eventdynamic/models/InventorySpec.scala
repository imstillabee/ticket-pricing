package com.eventdynamic.models

import org.scalatest.{Matchers, WordSpec}

class InventorySpec extends WordSpec with Matchers {
  "Inventory#matches" should {
    "should match when identical" in {
      val a = Inventory(Some(1), 1, true, "section", "row", true, "seat", 19.99, None, None)
      val b = Inventory(Some(1), 1, true, "section", "row", true, "seat", 19.99, None, None)

      a.matches(b) shouldBe true
    }

    "should match when non-identifier values are different" in {
      val a = Inventory(Some(1), 1, true, "section", "row", true, "seat", 19.99, None, None)
      val b = a.copy(
        id = Some(2),
        eventIsBroadcast = false,
        rowIsListed = false,
        listedPrice = 29.99,
        overridePrice = Some(19.99),
        listingId = Some("listingId")
      )

      a.matches(b) shouldBe true
    }

    "should not match if an identifier value is different" in {
      val a = Inventory(Some(1), 1, true, "section", "row", true, "seat", 19.99, None, None)
      val b = a.copy(eventId = 2)
      val c = a.copy(section = "different section")
      val d = a.copy(row = "different row")
      val e = a.copy(seat = "different seat")

      a.matches(b) shouldBe false
      a.matches(c) shouldBe false
      a.matches(d) shouldBe false
      a.matches(e) shouldBe false
    }
  }
}
