package com.eventdynamic.queues

import java.sql.Timestamp

import com.eventdynamic.Tables
import com.eventdynamic.models.{JobPriorityType, JobTriggerType, JobType, ScheduledJobStatus}
import com.eventdynamic.services.{JobConfigService, JobPriorityService, JobTriggerService}
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import org.scalatest.AsyncWordSpec

class ScheduledJobQueueSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val jobConfigService = new JobConfigService(ctx)
  val scheduledJobService = new ScheduledJobQueue(ctx)
  val jobTriggerService = new JobTriggerService(ctx)
  val jobPriorityService = new JobPriorityService(ctx)
  val scheduledJobTriggers = Tables.ScheduledJobTriggers

  "ScheduledJobQueue#add" should {
    "add a new job with a default priority" in {
      for {
        clientId <- r.client().map(_.id.get)
        eventId <- r.event().map(_.id.get)
        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)
        job <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.Default
        )
        jobTrigger <- jobTriggerService.getByTriggerType(JobTriggerType.Manual)
        scheduledJobTrigger <- scheduledJobService.getScheduledJobTrigger(
          job.id.get,
          jobTrigger.get.id.get
        )
        jobPriority <- jobPriorityService.getByJobPriorityType(JobPriorityType.Default)
      } yield {
        assert(job.eventId === eventId)
        assert(job.jobConfigId === jobConfigId)
        assert(job.startTime.isEmpty)
        assert(job.endTime.isEmpty)
        assert(job.error.isEmpty)
        assert(scheduledJobTrigger.isDefined)
        assert(job.jobPriorityId === jobPriority.get.id.get)
      }
    }

    "add a new job with a specified priority" in {
      for {
        clientId <- r.client().map(_.id.get)
        eventId <- r.event().map(_.id.get)
        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)
        job <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )
        jobTrigger <- jobTriggerService.getByTriggerType(JobTriggerType.Manual)
        scheduledJobTrigger <- scheduledJobService.getScheduledJobTrigger(
          job.id.get,
          jobTrigger.get.id.get
        )
        jobPriority <- jobPriorityService.getByJobPriorityType(
          JobPriorityType.ManualPricingModifierChange
        )
      } yield {
        assert(job.eventId === eventId)
        assert(job.jobConfigId === jobConfigId)
        assert(job.jobPriorityId === jobPriority.get.id.get)
        assert(job.startTime.isEmpty)
        assert(job.endTime.isEmpty)
        assert(job.error.isEmpty)
        assert(scheduledJobTrigger.isDefined)
      }
    }

    "not add another job for the same event if a pending job for that event already exists" in {
      for {
        clientId <- r.client().map(_.id.get)
        eventId <- r.event().map(_.id.get)
        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)
        _ <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )
        _ <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )
        _ <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )
        _ <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )
        results <- scheduledJobService.resultsByEvent(eventId, jobConfigId)
        jobTrigger <- jobTriggerService.getByTriggerType(JobTriggerType.Manual)
        scheduledJobTrigger <- scheduledJobService.getScheduledJobTrigger(
          results(ScheduledJobStatus.Pending).head.id.get,
          jobTrigger.get.id.get
        )
      } yield {
        assert(results.keySet.size === 1)
        assert(results.keys.head === ScheduledJobStatus.Pending)
        assert(results.get(ScheduledJobStatus.Pending).isDefined)
        assert(results(ScheduledJobStatus.Pending).size === 1)
        assert(scheduledJobTrigger.isDefined)
      }
    }

    "update the priority for a pending job if it is higher than the existing priority" in {
      for {
        clientId <- r.client().map(_.id.get)
        eventId <- r.event().map(_.id.get)
        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)
        _ <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Time,
          JobPriorityType.TimeDecayHourly
        )
        // Higher priority job
        _ <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )
        results <- scheduledJobService.resultsByEvent(eventId, jobConfigId)
        jobTrigger <- jobTriggerService.getByTriggerType(JobTriggerType.Manual)
        scheduledJobTrigger <- scheduledJobService.getScheduledJobTrigger(
          results(ScheduledJobStatus.Pending).head.id.get,
          jobTrigger.get.id.get
        )
        jobPriority <- jobPriorityService.getByJobPriorityType(
          JobPriorityType.ManualPricingModifierChange
        )
      } yield {
        assert(results.keySet.size === 1)
        assert(results.keys.head === ScheduledJobStatus.Pending)
        assert(results.get(ScheduledJobStatus.Pending).size === 1)
        assert(results(ScheduledJobStatus.Pending).head.jobPriorityId === jobPriority.get.id.get)
        assert(scheduledJobTrigger.isDefined)
      }
    }

    "keep the priority for a pending job if it is equal to the existing priority" in {
      for {
        clientId <- r.client().map(_.id.get)
        eventId <- r.event().map(_.id.get)
        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)
        _ <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )
        _ <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )
        results <- scheduledJobService.resultsByEvent(eventId, jobConfigId)
        jobTrigger <- jobTriggerService.getByTriggerType(JobTriggerType.Manual)
        scheduledJobTrigger <- scheduledJobService.getScheduledJobTrigger(
          results(ScheduledJobStatus.Pending).head.id.get,
          jobTrigger.get.id.get
        )
        jobPriority <- jobPriorityService.getByJobPriorityType(
          JobPriorityType.ManualPricingModifierChange
        )
      } yield {
        assert(results.keySet.size === 1)
        assert(results.keys.head === ScheduledJobStatus.Pending)
        assert(results.get(ScheduledJobStatus.Pending).size === 1)
        assert(results(ScheduledJobStatus.Pending).head.jobPriorityId === jobPriority.get.id.get)
        assert(scheduledJobTrigger.isDefined)
      }
    }

    "NOT update the priority for a pending job if it is lower than the existing priority" in {
      for {
        clientId <- r.client().map(_.id.get)
        eventId <- r.event().map(_.id.get)
        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)
        _ <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )
        // Lower priority job
        _ <- scheduledJobService.add(
          eventId,
          jobConfigId,
          JobTriggerType.Time,
          JobPriorityType.TimeDecayWeekly
        )
        results <- scheduledJobService.resultsByEvent(eventId, jobConfigId)
        jobTrigger <- jobTriggerService.getByTriggerType(JobTriggerType.Manual)
        scheduledJobTrigger <- scheduledJobService.getScheduledJobTrigger(
          results(ScheduledJobStatus.Pending).head.id.get,
          jobTrigger.get.id.get
        )
        jobPriority <- jobPriorityService.getByJobPriorityType(
          JobPriorityType.ManualPricingModifierChange
        )
      } yield {
        assert(results.keySet.size === 1)
        assert(results.keys.head === ScheduledJobStatus.Pending)
        assert(results.get(ScheduledJobStatus.Pending).size === 1)
        assert(results(ScheduledJobStatus.Pending).head.jobPriorityId === jobPriority.get.id.get)
        assert(scheduledJobTrigger.isDefined)
      }
    }
  }

  "ScheduledJobQueue#peek" should {
    "peek at the next pending job (by event date w/ same priority)" in {
      for {
        clientId <- r.client().map(_.id.get)

        event1 <- r.event(timestamp = Option(new Timestamp(1553843616721L))).map(_.id.get)
        event2 <- r.event(timestamp = Option(new Timestamp(1553843616720L))).map(_.id.get)
        event3 <- r.event(timestamp = Option(new Timestamp(1553843616722L))).map(_.id.get)

        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)
        // Here we're using the default priority type -> JobPriorityType.Default
        _ <- scheduledJobService.add(event1, jobConfigId, JobTriggerType.Manual)
        _ <- scheduledJobService.add(event2, jobConfigId, JobTriggerType.Manual)
        _ <- scheduledJobService.add(event3, jobConfigId, JobTriggerType.Manual)
        pendingJob <- scheduledJobService.peek(jobConfigId)
        jobPriority <- jobPriorityService.getByJobPriorityType(JobPriorityType.Default)
      } yield {
        assert(pendingJob.isDefined)

        val job = pendingJob.get
        assert(job.eventId === event2)
        assert(job.jobConfigId === jobConfigId)
        assert(job.jobPriorityId === jobPriority.get.id.get)
        assert(job.startTime.isEmpty)
        assert(job.endTime.isEmpty)
        assert(job.error.isEmpty)
      }
    }

    "peek at the next pending job (by event date w/ highest priority)" in {
      for {
        clientId <- r.client().map(_.id.get)

        event1 <- r.event(timestamp = Option(new Timestamp(1553843616721L))).map(_.id.get)
        event2 <- r.event(timestamp = Option(new Timestamp(1553843616720L))).map(_.id.get)
        event3 <- r.event(timestamp = Option(new Timestamp(1553843616722L))).map(_.id.get)

        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)
        _ <- scheduledJobService.add(
          event1,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualOverride
        )
        _ <- scheduledJobService.add(event2, jobConfigId, JobTriggerType.Manual)
        _ <- scheduledJobService.add(event3, jobConfigId, JobTriggerType.Manual)
        pendingJob <- scheduledJobService.peek(jobConfigId)
        jobPriority <- jobPriorityService.getByJobPriorityType(JobPriorityType.ManualOverride)
      } yield {
        assert(pendingJob.isDefined)

        val job = pendingJob.get
        assert(job.eventId === event1)
        assert(job.jobConfigId === jobConfigId)
        assert(job.jobPriorityId === jobPriority.get.id.get)
        assert(job.startTime.isEmpty)
        assert(job.endTime.isEmpty)
        assert(job.error.isEmpty)
      }
    }

    "peek at the next pending job (highest priority)" in {
      for {
        clientId <- r.client().map(_.id.get)

        event1 <- r.event().map(_.id.get)
        event2 <- r.event().map(_.id.get)
        event3 <- r.event().map(_.id.get)

        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)

        _ <- scheduledJobService.add(
          event1,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )
        // Highest priority
        _ <- scheduledJobService.add(
          event2,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualOverride
        )
        _ <- scheduledJobService.add(
          event3,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualPricingModifierChange
        )

        pendingJob <- scheduledJobService.peek(jobConfigId)
        jobPriority <- jobPriorityService.getByJobPriorityType(JobPriorityType.ManualOverride)
      } yield {
        assert(pendingJob.isDefined)

        val job = pendingJob.get
        assert(job.eventId === event2)
        assert(job.jobConfigId === jobConfigId)
        assert(job.jobPriorityId === jobPriority.get.id.get)
        assert(job.startTime.isEmpty)
        assert(job.endTime.isEmpty)
        assert(job.error.isEmpty)
      }
    }
  }

  "ScheduledJobQueue#poll" should {
    "poll the next pending job (by event date w/ same priority)" in {
      for {
        clientId <- r.client().map(_.id.get)

        event1 <- r.event(timestamp = Option(new Timestamp(1553843616721L))).map(_.id.get)
        event2 <- r.event(timestamp = Option(new Timestamp(1553843616720L))).map(_.id.get)
        event3 <- r.event(timestamp = Option(new Timestamp(1553843616722L))).map(_.id.get)

        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)

        _ <- scheduledJobService.add(event1, jobConfigId, JobTriggerType.Manual)
        _ <- scheduledJobService.add(event2, jobConfigId, JobTriggerType.Manual)
        _ <- scheduledJobService.add(event3, jobConfigId, JobTriggerType.Manual)

        runningJob <- scheduledJobService.poll(jobConfigId)
        pendingJob <- scheduledJobService.peek(jobConfigId)
        jobPriority <- jobPriorityService.getByJobPriorityType(JobPriorityType.Default)
      } yield {
        assert(runningJob.isDefined)

        var job = runningJob.get
        assert(job.eventId === event2)
        assert(job.jobConfigId === jobConfigId)
        assert(job.jobPriorityId === jobPriority.get.id.get)
        assert(job.status === ScheduledJobStatus.Running)
        assert(job.startTime.isDefined)
        assert(job.endTime.isEmpty)
        assert(job.error.isEmpty)

        assert(pendingJob.isDefined)
        job = pendingJob.get
        assert(job.eventId === event1)
        assert(job.jobConfigId === jobConfigId)
        assert(job.jobPriorityId === jobPriority.get.id.get)
        assert(job.status === ScheduledJobStatus.Pending)
        assert(job.startTime.isEmpty)
        assert(job.endTime.isEmpty)
        assert(job.error.isEmpty)
      }
    }

    "poll the next pending job (highest priority)" in {
      for {
        clientId <- r.client().map(_.id.get)

        event1 <- r.event().map(_.id.get)
        event2 <- r.event().map(_.id.get)
        event3 <- r.event().map(_.id.get)

        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)

        _ <- scheduledJobService.add(event1, jobConfigId, JobTriggerType.Manual)
        _ <- scheduledJobService.add(
          event2,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualOverride
        )
        _ <- scheduledJobService.add(event3, jobConfigId, JobTriggerType.Manual)

        runningJob <- scheduledJobService.poll(jobConfigId)
        pendingJob <- scheduledJobService.peek(jobConfigId)
        runningJobPriority <- jobPriorityService.getByJobPriorityType(
          JobPriorityType.ManualOverride
        )
        pendingJobPriority <- jobPriorityService.getByJobPriorityType(JobPriorityType.Default)
      } yield {
        assert(runningJob.isDefined)

        var job = runningJob.get
        assert(job.eventId === event2)
        assert(job.jobConfigId === jobConfigId)
        assert(job.jobPriorityId === runningJobPriority.get.id.get)
        assert(job.status === ScheduledJobStatus.Running)
        assert(job.startTime.isDefined)
        assert(job.endTime.isEmpty)
        assert(job.error.isEmpty)

        assert(pendingJob.isDefined)
        job = pendingJob.get
        assert(job.eventId === event3)
        assert(job.jobConfigId === jobConfigId)
        assert(job.jobPriorityId === pendingJobPriority.get.id.get)
        assert(job.status === ScheduledJobStatus.Pending)
        assert(job.startTime.isEmpty)
        assert(job.endTime.isEmpty)
        assert(job.error.isEmpty)
      }
    }

    "poll returns none when queue is empty" in {
      for {
        clientId <- r.client().map(_.id.get)

        event1 <- r.event().map(_.id.get)

        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)

        // Put in a completed job
        job1 <- scheduledJobService.add(event1, jobConfigId, JobTriggerType.Manual)
        _ <- scheduledJobService.poll(jobConfigId)
        _ <- scheduledJobService.complete(job1.id.get)

        // Put in a failed job
        job2 <- scheduledJobService.add(
          eventId = event1,
          jobConfigId = jobConfigId,
          JobTriggerType.Manual
        )
        _ <- scheduledJobService.poll(jobConfigId)
        _ <- scheduledJobService.fail(job2.id.get, "test")

        runningJob <- scheduledJobService.poll(jobConfigId)
        pendingJob <- scheduledJobService.peek(jobConfigId)
      } yield {
        assert(runningJob.isEmpty)
        assert(pendingJob.isEmpty)
      }
    }
  }

  "ScheduledJobQueue#complete" should {
    "mark a running job as successful" in {
      for {
        clientId <- r.client().map(_.id.get)

        event1 <- r.event().map(_.id.get)
        event2 <- r.event().map(_.id.get)
        event3 <- r.event().map(_.id.get)

        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)

        _ <- scheduledJobService.add(event1, jobConfigId, JobTriggerType.Manual)
        _ <- scheduledJobService.add(
          event2,
          jobConfigId,
          JobTriggerType.Time,
          JobPriorityType.TimeDecayHourly
        )
        _ <- scheduledJobService.add(event3, jobConfigId, JobTriggerType.Manual)

        runningJob <- scheduledJobService.poll(jobConfigId)
        _ <- scheduledJobService.complete(runningJob.get.id.get)

        successfulJob <- scheduledJobService.getById(runningJob.get.id.get)
        jobPriority <- jobPriorityService.getByJobPriorityType(JobPriorityType.TimeDecayHourly)
      } yield {
        assert(successfulJob.isDefined)

        val job = successfulJob.get
        assert(job.eventId === event2)
        assert(job.jobConfigId === jobConfigId)
        assert(job.jobPriorityId === jobPriority.get.id.get)
        assert(job.status === ScheduledJobStatus.Success)
        assert(job.startTime.isDefined)
        assert(job.endTime.isDefined)
        assert(job.error.isEmpty)
      }
    }
  }

  "ScheduledJobQueue#fail" should {
    "mark a running job as failed (reason is a string)" in {
      for {
        clientId <- r.client().map(_.id.get)

        event1 <- r.event().map(_.id.get)
        event2 <- r.event().map(_.id.get)
        event3 <- r.event().map(_.id.get)

        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)

        _ <- scheduledJobService.add(event1, jobConfigId, JobTriggerType.Manual)
        _ <- scheduledJobService.add(
          event2,
          jobConfigId,
          JobTriggerType.Time,
          JobPriorityType.TimeDecayHourly
        )
        _ <- scheduledJobService.add(event3, jobConfigId, JobTriggerType.Manual)

        runningJob <- scheduledJobService.poll(jobConfigId)
        _ <- scheduledJobService.fail(runningJob.get.id.get, "This is an error message")

        successfulJob <- scheduledJobService.getById(runningJob.get.id.get)
        jobPriority <- jobPriorityService.getByJobPriorityType(JobPriorityType.TimeDecayHourly)
      } yield {
        assert(successfulJob.isDefined)

        val job = successfulJob.get
        assert(job.eventId === event2)
        assert(job.jobConfigId === jobConfigId)
        assert(job.jobPriorityId === jobPriority.get.id.get)
        assert(job.status === ScheduledJobStatus.Failure)
        assert(job.startTime.isDefined)
        assert(job.endTime.isDefined)
        assert(job.error.isDefined)
      }
    }

    "mark a running job as failed (reason is an exception)" in {
      for {
        clientId <- r.client().map(_.id.get)

        event1 <- r.event().map(_.id.get)
        event2 <- r.event().map(_.id.get)
        event3 <- r.event().map(_.id.get)

        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)

        _ <- scheduledJobService.add(event1, jobConfigId, JobTriggerType.Manual)
        _ <- scheduledJobService.add(
          event2,
          jobConfigId,
          JobTriggerType.Time,
          JobPriorityType.TimeDecayHourly
        )
        _ <- scheduledJobService.add(event3, jobConfigId = jobConfigId, JobTriggerType.Manual)

        runningJob <- scheduledJobService.poll(jobConfigId)
        _ <- scheduledJobService.fail(
          runningJob.get.id.get,
          new Exception("This is an error message")
        )

        successfulJob <- scheduledJobService.getById(runningJob.get.id.get)
        jobPriority <- jobPriorityService.getByJobPriorityType(JobPriorityType.TimeDecayHourly)
      } yield {
        assert(successfulJob.isDefined)

        val job = successfulJob.get
        assert(job.eventId === event2)
        assert(job.jobConfigId === jobConfigId)
        assert(job.jobPriorityId === jobPriority.get.id.get)
        assert(job.status === ScheduledJobStatus.Failure)
        assert(job.startTime.isDefined)
        assert(job.endTime.isDefined)

        assert(job.error.isDefined)
      }
    }
  }

  "ScheduledJobQueue#getLatestForEvent" should {
    "find latest job for an event with multiple triggers" in {
      for {
        clientId <- r.client().map(_.id.get)
        event1 <- r.event().map(_.id.get)
        event2 <- r.event().map(_.id.get)
        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)
        _ <- scheduledJobService.add(event1, jobConfigId, JobTriggerType.Time)
        runningJob <- scheduledJobService.add(event2, jobConfigId, JobTriggerType.Time)
        _ <- scheduledJobService.complete(runningJob.id.get)
        latestJob <- scheduledJobService.add(
          event2,
          jobConfigId,
          JobTriggerType.Manual,
          JobPriorityType.ManualOverride
        )
        scheduledJobComposite <- scheduledJobService.getLatestForEvent(
          event2,
          JobType.PrimarySync,
          Some(ScheduledJobStatus.Pending)
        )
      } yield {
        assert(scheduledJobComposite.isDefined)
        val job = scheduledJobComposite.get.scheduledJob
        assert(job.eventId === event2)
        assert(job.jobConfigId === jobConfigId)
        // Check it's the latest job
        assert(job.modifiedAt === latestJob.modifiedAt)

        val priority = scheduledJobComposite.get.jobPriority
        assert(priority.name === JobPriorityType.ManualOverride)

        val triggers = scheduledJobComposite.get.triggers
        assert(triggers.length == 2)
        // Check it's ordered by modifiedAt timestamp in descending order
        assert(triggers(0).triggerType === JobTriggerType.Manual)
        assert(triggers(1).triggerType === JobTriggerType.Time)
      }
    }

    "NOT find any job for an event" in {
      for {
        clientId <- r.client().map(_.id.get)
        event1 <- r.event().map(_.id.get)
        event2 <- r.event().map(_.id.get)
        jobConfigId <- jobConfigService
          .createConfig[String](
            clientId = clientId,
            jobType = JobType.PrimarySync,
            config = "This is a test"
          )
          .map(_.id.get)
        _ <- scheduledJobService.add(event1, jobConfigId, JobTriggerType.Time)
        _ <- scheduledJobService.add(event1, jobConfigId, JobTriggerType.Time)
        scheduledJobComposite <- scheduledJobService.getLatestForEvent(
          event2,
          JobType.PrimarySync,
          None
        )
      } yield {
        assert(scheduledJobComposite.isEmpty)
      }
    }
  }
}
