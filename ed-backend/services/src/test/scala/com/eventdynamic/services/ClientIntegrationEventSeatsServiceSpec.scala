package com.eventdynamic.services

import com.eventdynamic.models.{ClientIntegrationEventSeat, ClientIntegrationListing}
import com.eventdynamic.test.{DatabaseSuite, RandomUtil, ServiceHolder}
import org.scalatest.AsyncWordSpec
import org.scalatest.Matchers._

import scala.concurrent.Future

class ClientIntegrationEventSeatsServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "ClientIntegrationEventSeatsService" should {
    val serviceHolder = new ServiceHolder(ctx)
    val r = new RandomUtil(ctx)
    val clientIntegrationEventsService = new ClientIntegrationEventsService(ctx)
    val clientIntegrationListingsService = new ClientIntegrationListingsService(ctx)
    val clientIntegrationEventSeatsService = new ClientIntegrationEventSeatsService(ctx)

    def create(): Future[ClientIntegrationEventSeat] = {
      for {
        client <- r.client()
        integration <- serviceHolder.integration.getById(1).map(_.get)
        clientIntegration <- serviceHolder.clientIntegration.create(
          client.id.get,
          integration.id.get,
          true,
          true
        )
        event <- r.event()
        eventSeat <- r.eventSeat(eventId = event.id.get)
        clientIntegrationEvent <- clientIntegrationEventsService.create(
          clientIntegration.id.get,
          event.id.get,
          r.string()
        )
        clientIntegrationListing <- clientIntegrationListingsService.create(
          clientIntegrationEvent.id.get,
          r.string()
        )
        clientIntegrationEventSeat <- clientIntegrationEventSeatsService.create(
          clientIntegrationListing.id.get,
          eventSeat.id.get
        )
      } yield {
        clientIntegrationEventSeat
      }
    }

    "Create a client integration event seat" in {
      for {
        createdEventSeat <- create()
      } yield {
        assert(createdEventSeat.id.isDefined)
      }
    }

    "Bulk insert client integrations" in {
      val COUNT = 20
      for {
        someClientEventSeats <- r.some(_ => create(), COUNT)
        createdEventSeats <- clientIntegrationEventSeatsService.bulkInsert(someClientEventSeats)
      } yield {
        assert(someClientEventSeats.length === COUNT)
        assert(createdEventSeats.get === COUNT)
      }
    }

  }
}
