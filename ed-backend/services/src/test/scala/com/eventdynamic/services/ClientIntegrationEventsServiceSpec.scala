package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.models.ClientIntegrationEvent
import com.eventdynamic.test.{DatabaseSuite, RandomUtil, ServiceHolder}
import org.scalatest.AsyncWordSpec
import org.scalatest.Matchers._

import scala.concurrent.Future

class ClientIntegrationEventsServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "ClientIntegrationEventsService" should {
    val serviceHolder = new ServiceHolder(ctx)
    val clientIntegrationEventsService = new ClientIntegrationEventsService(ctx)
    val r = new RandomUtil(ctx)

    def create(): Future[ClientIntegrationEvent] = {
      for {
        client <- r.client()
        integration <- serviceHolder.integration.getById(1).map(_.get)
        clientIntegration <- serviceHolder.clientIntegration.create(
          client.id.get,
          integration.id.get,
          true,
          true
        )
        event <- r.event()
        clientIntegrationEvent <- clientIntegrationEventsService.create(
          clientIntegration.id.get,
          event.id.get,
          r.string()
        )
      } yield {
        clientIntegrationEvent
      }
    }

    "Should Create a ClientIntegrationEvent" in {
      for {
        clientIntegrationEvent <- create()
      } yield {
        assert(clientIntegrationEvent.createdAt.isInstanceOf[Timestamp])
        assert(clientIntegrationEvent.id.isInstanceOf[Option[Int]])
      }
    }

    "Should Get a ClientIntegrationEvent by clientIntegrationId and eventId" in {
      for {
        createdEvent <- create()
        clientIntegrationEvent <- clientIntegrationEventsService.getBy(
          createdEvent.clientIntegrationId,
          createdEvent.eventId
        )
      } yield {
        assert(clientIntegrationEvent.get == createdEvent)
      }
    }

  }
}
