package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.models.ClientIntegrationListing
import com.eventdynamic.test.{DatabaseSuite, RandomUtil, ServiceHolder}
import org.scalatest.AsyncWordSpec
import org.scalatest.Matchers._

import scala.concurrent.Future

class ClientIntegrationListingsServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "ClientIntegrationListingsService" should {
    val serviceHolder = new ServiceHolder(ctx)
    val r = new RandomUtil(ctx)
    val clientIntegrationEventsService = new ClientIntegrationEventsService(ctx)
    val clientIntegrationListingsService = new ClientIntegrationListingsService(ctx)

    def create(): Future[ClientIntegrationListing] = {
      for {
        client <- r.client()
        integration <- serviceHolder.integration.getById(1).map(_.get)
        clientIntegration <- serviceHolder.clientIntegration.create(
          client.id.get,
          integration.id.get,
          true,
          true
        )
        event <- r.event()
        clientIntegrationEvent <- clientIntegrationEventsService.create(
          clientIntegration.id.get,
          event.id.get,
          r.string()
        )
        clientIntegrationListing <- clientIntegrationListingsService.create(
          clientIntegrationEvent.id.get,
          r.string()
        )
      } yield {
        clientIntegrationListing
      }
    }

    "create a listing" in {
      for {
        createdListing <- create()
      } yield {
        createdListing.createdAt shouldBe a[Timestamp]
        createdListing.id.get shouldBe a[Integer]
        createdListing.clientIntegrationEventsId shouldBe a[Integer]
        createdListing.listingId shouldBe a[String]
      }
    }

    "get a listing" in {
      for {
        createdListing <- create()
        listing <- clientIntegrationListingsService.get(
          createdListing.clientIntegrationEventsId,
          createdListing.listingId
        )
      } yield {
        assert(listing.get === createdListing)
      }
    }

    "get or create a listing" in {
      for {
        client <- r.client()
        integration <- serviceHolder.integration.getById(1).map(_.get)
        clientIntegration <- serviceHolder.clientIntegration.create(
          client.id.get,
          integration.id.get,
          true,
          true
        )
        event <- r.event()
        clientIntegrationEvent <- clientIntegrationEventsService.create(
          clientIntegration.id.get,
          event.id.get,
          r.string()
        )
        clientIntegrationListing <- clientIntegrationListingsService.getOrCreate(
          clientIntegrationEvent.id.get,
          r.string()
        )
      } yield {
        assert(clientIntegrationListing.id.isDefined)
      }
    }

  }
}
