package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.models.{Client, ClientPerformanceType}
import com.eventdynamic.test.{DatabaseSuite, ServiceHolder}
import com.eventdynamic.utils.SuccessResponse
import org.scalatest.AsyncWordSpec
import org.scalatest.Inspectors._
import org.scalatest.Matchers._
import org.scalatest.OptionValues._

class ClientIntegrationServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "ClientIntegrationService" should {
    val services = new ServiceHolder(ctx)
    val clientService = services.client
    val integrationService = services.integration
    val clientIntegrationService = services.clientIntegration

    def createClientIntegration() = {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          "New York Mets",
          15,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        integration <- integrationService.getById(1).map(_.get)
        clientIntegration <- clientIntegrationService.create(
          client.id.get,
          integration.id.get,
          true,
          true
        )
      } yield clientIntegration
    }

    "create a clientIntegration" in {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          "New York Mets",
          15,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        integration <- integrationService.getById(1).map(_.get)
        clientIntegration <- clientIntegrationService.create(
          client.id.get,
          integration.id.get,
          true,
          true
        )
      } yield {
        clientIntegration.id.value shouldEqual 1
        clientIntegration.clientId shouldEqual client.id.get
        clientIntegration.integrationId shouldEqual integration.id.get
        clientIntegration.isActive shouldBe true
        clientIntegration.isPrimary shouldBe true
        clientIntegration.createdAt shouldBe a[Timestamp]
        clientIntegration.modifiedAt shouldBe a[Timestamp]
      }
    }

    "delete a clientIntegration" in {
      for {
        clientIntegration <- createClientIntegration()
        isDeleted <- clientIntegrationService.delete(clientIntegration.id.get)
      } yield {
        isDeleted shouldBe true
      }
    }

    "get a clientIntegration by Id returns a clientIntegrationRuleComposite with correct information" in {
      for {
        clientIntegration <- createClientIntegration()
        fetchedClientIntegrationComposite <- clientIntegrationService.getById(
          clientIntegration.id.get
        )
      } yield {
        fetchedClientIntegrationComposite.value.id shouldEqual clientIntegration.id
        fetchedClientIntegrationComposite.value.integrationId shouldEqual clientIntegration.integrationId
        fetchedClientIntegrationComposite.value.createdAt shouldEqual clientIntegration.createdAt
        fetchedClientIntegrationComposite.value.modifiedAt shouldEqual clientIntegration.modifiedAt
        fetchedClientIntegrationComposite.value.clientId shouldEqual clientIntegration.clientId
        fetchedClientIntegrationComposite.value.isActive shouldEqual clientIntegration.isActive
        fetchedClientIntegrationComposite.value.isPrimary shouldEqual clientIntegration.isPrimary
        fetchedClientIntegrationComposite.value.version shouldEqual clientIntegration.version
      }
    }

    "get clientIntegrations by clientId" in {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          "New York Mets",
          15,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        integration <- integrationService.getById(1).map(_.get)
        _ <- clientIntegrationService.create(client.id.get, integration.id.get, true, true)
        fetchedClientIntegrations <- clientIntegrationService.getByClientId(
          client.id.get,
          false,
          false
        )
      } yield {
        forAll(fetchedClientIntegrations) { f =>
          f.clientId shouldEqual client.id.get
        }
        forAll(fetchedClientIntegrations) { f =>
          f.integrationId shouldEqual integration.id.get
        }
        forAll(fetchedClientIntegrations) { f =>
          f.name shouldEqual integration.name
        }
        forAll(fetchedClientIntegrations) { f =>
          f.version shouldEqual None
        }
      }
    }

    "get filtered active clientIntegrations by clientId" in {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          "New York Mets",
          15,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        SuccessResponse(client2: Client) <- clientService.create(
          name = "Falcons",
          pricingInterval = 12,
          eventScoreFloor = 2,
          eventScoreCeiling = None,
          springFloor = 1.125,
          springCeiling = None,
          performanceType = ClientPerformanceType.MLB
        )
        integration <- integrationService.getById(1).map(_.get)
        _ <- clientIntegrationService.create(client.id.get, integration.id.get, true, true)
        _ <- integrationService.getById(1).map(_.get)
        _ <- clientIntegrationService.create(client2.id.get, integration.id.get, false, false)
        fetchedClientIntegrations <- clientIntegrationService.getByClientId(
          client.id.get,
          true,
          false
        )
      } yield {
        fetchedClientIntegrations.head.clientId shouldEqual client.id.value

        forAll(fetchedClientIntegrations) { f =>
          f.integrationId shouldEqual integration.id.get
        }
        forAll(fetchedClientIntegrations) { f =>
          f.name shouldEqual integration.name
        }
        forAll(fetchedClientIntegrations) { f =>
          f.version shouldEqual None
        }
      }
    }

    "get filtered primary clientIntegrations by clientId" in {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          "New York Mets",
          15,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        SuccessResponse(client2: Client) <- clientService.create(
          name = "Falcons",
          pricingInterval = 12,
          eventScoreFloor = 2,
          eventScoreCeiling = None,
          springFloor = 1.125,
          springCeiling = None,
          performanceType = ClientPerformanceType.MLB
        )
        integration <- integrationService.getById(1).map(_.get)
        _ <- clientIntegrationService.create(client.id.get, integration.id.get, true, true)
        _ <- integrationService.getById(1).map(_.get)
        _ <- clientIntegrationService.create(client2.id.get, integration.id.get, false, false)
        fetchedClientIntegrations <- clientIntegrationService.getByClientId(
          client.id.get,
          false,
          true
        )
      } yield {
        fetchedClientIntegrations.head.clientId shouldEqual client.id.value

        forAll(fetchedClientIntegrations) { f =>
          f.integrationId shouldEqual integration.id.get
        }
        forAll(fetchedClientIntegrations) { f =>
          f.name shouldEqual integration.name
        }
        forAll(fetchedClientIntegrations) { f =>
          f.version shouldEqual None
        }
      }
    }

    "get a clientIntegration by its integration name" in {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          "New York Mets",
          15,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        Some(integration) <- integrationService.getById(1)
        created <- clientIntegrationService.create(client.id.get, integration.id.get, true, true)
        result <- clientIntegrationService.getByName(client.id.get, integration.name)
      } yield {
        assert(result.isDefined)
        result.get.name shouldEqual integration.name
        result.get.id shouldEqual created.id
      }
    }
  }
}
