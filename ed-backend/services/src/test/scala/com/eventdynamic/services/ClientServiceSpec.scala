package com.eventdynamic.services

import com.eventdynamic.models.{Client, ClientPerformanceType}
import com.eventdynamic.test.DatabaseSuite
import com.eventdynamic.utils._
import org.scalatest.AsyncWordSpec

class ClientServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "ClientService" should {
    val clientService = new ClientService(ctx)
    val userService = new UserService(ctx)

    def createClient() = {
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "test",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
      } yield createdClient.id.get
    }

    "create client" in {
      createClient().map { id =>
        assert(id == 1)
      }
    }

    "get client by id" in {
      for {
        clientId <- createClient()
        Some(client) <- clientService.getById(clientId)
      } yield {
        assert(clientId == client.id.get)
      }
    }

    "delete client by id" in {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          "Client",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        _ <- clientService.delete(client.id.get)
        deletedClient <- clientService.getById(client.id.get)
      } yield assert(deletedClient.isEmpty)
    }

    "update client" in {
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "Client",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        client <- clientService.getById(createdClient.id.get)
        _ <- clientService.update(client.get.copy(name = "New Client", pricingInterval = 60))
        updatedClient <- clientService.getById(client.get.id.get)
      } yield
        assert(updatedClient.get.name === "New Client" && updatedClient.get.pricingInterval === 60)
    }

    "update client by id and name" in {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          "Client",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        _ <- clientService.updateUnique(client.id.get, "New Client", 60, None, None, None, None)
        updatedClient <- clientService.getById(client.id.get)
      } yield
        assert(updatedClient.get.name === "New Client" && updatedClient.get.pricingInterval === 60)
    }

    "not update if client could not be found" in {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          "Client",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        response <- clientService.updateUnique(
          client.id.get + 1,
          "New Client",
          60,
          None,
          None,
          None,
          None
        )
      } yield assert(response === NotFoundResponse)
    }

    "get all clients for a user" in {
      for {
        SuccessResponse(client1: Client) <- clientService.create(
          "Client 1",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        _ <- clientService.create("Client 2", 60, 2, None, 1.125, None, ClientPerformanceType.MLB)
        (newUser, _) <- userService.create(
          "test@dialexa.com",
          "First",
          "Last",
          None,
          client1.id.get,
          None,
          false
        )
        response <- clientService.getAll(newUser.id.get)
      } yield
        assert(response.toString.contains("Client 1") && !response.toString.contains("Client 2"))
    }

    "get all clients for a performance type" in {
      for {
        _ <- clientService.create("Client 1", 30, 2, None, 1.125, None, ClientPerformanceType.MLB)
        _ <- clientService.create("Client 2", 60, 2, None, 1.125, None, ClientPerformanceType.MLB)
        _ <- clientService.create("Client 3", 60, 2, None, 1.125, None, ClientPerformanceType.NFL)
        response <- clientService.getByPerformance(ClientPerformanceType.MLB)
      } yield {
        assert(response.length === 2)
        assert(response.forall(_.performanceType === ClientPerformanceType.MLB))
      }
    }
  }
}
