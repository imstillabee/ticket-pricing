package com.eventdynamic.services

import com.eventdynamic.models.{Client, ClientPerformanceType}
import com.eventdynamic.test.DatabaseSuite
import com.eventdynamic.utils.SuccessResponse
import org.scalatest.AsyncWordSpec

import scala.concurrent.Future

class EventCategoryServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "EventCategoryService" should {
    val clientService = new ClientService(ctx)

    def createClient(name: String = "SOS"): Future[Int] = {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          name,
          pricingInterval = 1500,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
      } yield client.id.get
    }

    val eventCategoryService = new EventCategoryService(ctx)

    def createEventCategory(name: String, clientId: Int): Future[Int] = {
      for {
        eventCategoryId <- eventCategoryService.create(name, clientId)
      } yield eventCategoryId
    }

    "create an eventCategory" in {
      for {
        clientId <- createClient()
        createdId <- createEventCategory("Marquee", clientId)
      } yield {
        assert(createdId == 1)
      }
    }

    "get eventCategory by id" in {
      for {
        clientId <- createClient()
        createdId <- createEventCategory("Marquee", clientId)
        createdEventCategory <- eventCategoryService.getById(createdId)
      } yield {
        createdEventCategory match {
          case Some(eventCategory) => {
            assert(eventCategory.id == Some(createdId))
            assert(eventCategory.name == "Marquee")
          }
          case None => fail()
        }
      }
    }

    "get all eventCategories for a client" in {
      for {
        c1 <- createClient("SOS")
        c2 <- createClient("ED")
        _ <- createEventCategory("Marquee", c1)
        _ <- createEventCategory("Premium", c1)
        _ <- createEventCategory("Classic", c2)
        venue1EventCategories <- eventCategoryService.getAllForClient(c1)
      } yield {
        venue1EventCategories match {
          case Seq() => fail()
          case eventCategories => {
            eventCategories.foreach(eventCategory => {
              assert(eventCategory.name != "Classic")
              assert(eventCategory.clientId == c1)
            })
            assert(eventCategories.length == 2)
          }
        }
      }
    }
  }
}
