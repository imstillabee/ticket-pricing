package com.eventdynamic.services

import com.eventdynamic.models._
import com.eventdynamic.test.{DatabaseSuite, RandomUtil, ServiceHolder}
import com.eventdynamic.utils.{DateHelper, DuplicateResponse, SuccessResponse}
import org.scalatest.AsyncWordSpec
import play.api.libs.json.Json
import scala.concurrent.Future
import scala.math.BigDecimal

class EventSeatServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val services = new ServiceHolder(ctx)
  val eventSeatService = services.eventSeat

  "EventSeatService#create" should {
    "create an eventSeat" in {
      val listedPrice = 50
      val now = DateHelper.now()

      for {
        event <- r.event()
        seat <- r.seat()
        priceScaleId <- r.priceScaleId()
        SuccessResponse(eventSeat: EventSeat) <- eventSeatService.create(
          EventSeat(
            None,
            seat.id.get,
            event.id.get,
            priceScaleId,
            Some(listedPrice),
            None,
            true,
            now,
            now,
            false
          )
        )
      } yield {
        assert(eventSeat.isInstanceOf[EventSeat])
        assert(eventSeat.eventId == event.id.get)
        assert(eventSeat.seatId == seat.id.get)
        assert(eventSeat.priceScaleId == priceScaleId)
        assert(eventSeat.listedPrice.get == listedPrice)
        assert(eventSeat.createdAt == now)
        assert(eventSeat.modifiedAt == now)
      }
    }

    "not create an eventSeat because one already exists" in {
      val listedPrice = 50
      val now = DateHelper.now()

      for {
        es <- r.eventSeat()
        res <- eventSeatService.create(
          EventSeat(
            None,
            es.seatId,
            es.eventId,
            es.priceScaleId,
            Some(listedPrice),
            None,
            true,
            now,
            now,
            false
          )
        )
      } yield {
        assert(res == DuplicateResponse)
      }
    }
  }

  "EventSeatService#updatedListedPrices" should {
    "update listed prices" in {
      val newListPrice = 99
      val N_SEATS = 20
      val now = DateHelper.now()

      for {
        priceScaleToUpdateId <- r.priceScaleId()
        event <- r.event()
        seats <- r.some(_ => r.seat(), N_SEATS)
        _ <- Future.sequence(
          seats
            .slice(0, N_SEATS / 2)
            .map(
              s =>
                eventSeatService
                  .create(
                    EventSeat(
                      None,
                      s.id.get,
                      event.id.get,
                      priceScaleToUpdateId,
                      None,
                      None,
                      true,
                      now,
                      now,
                      false
                    )
                )
            )
        )
        priceScaleId <- r.priceScaleId()
        _ <- Future.sequence(
          seats
            .slice(N_SEATS / 2, N_SEATS)
            .map(
              s =>
                eventSeatService
                  .create(
                    EventSeat(
                      None,
                      s.id.get,
                      event.id.get,
                      priceScaleId,
                      None,
                      None,
                      true,
                      now,
                      now,
                      false
                    )
                )
            )
        )
        nUpdatedRecords <- eventSeatService.updateListedPrices(
          Seq(EventSeatListedPriceUpdateByScale(event.id.get, priceScaleId, newListPrice))
        )
      } yield {
        assert(nUpdatedRecords == N_SEATS / 2)
      }
    }
  }

  "EventSeatService#bulkUpdate" should {
    "update override prices for an array of eventSeatIds" in {
      for {
        event <- r.event()

        row1seat1 <- r.seat(seat = "seat1", row = "row1", section = "section1")
        row1seat2 <- r.seat(seat = "seat2", row = "row1", section = "section1")

        row1eSeat1 <- r.eventSeat(
          seatId = row1seat1.id.get,
          eventId = event.id.get,
          overridePrice = None
        )
        row1eSeat2 <- r.eventSeat(
          seatId = row1seat2.id.get,
          eventId = event.id.get,
          overridePrice = None
        )

        seatsIdsToUpdate: Array[Int] = Array(row1eSeat1.id.get, row1eSeat2.id.get)
        newOverridePrice = Some(BigDecimal(50.00))

        esUpdatedCount <- eventSeatService.bulkUpdate(seatsIdsToUpdate, newOverridePrice, None)
        row1eSeat1AfterUpdate <- eventSeatService.get(row1eSeat1.seatId, row1eSeat1.eventId)
        row1eSeat2AfterUpdate <- eventSeatService.get(row1eSeat2.seatId, row1eSeat2.eventId)
      } yield {
        assert(esUpdatedCount == 2)
        assert(row1eSeat1AfterUpdate.get.overridePrice.get == 50.00)
        assert(row1eSeat2AfterUpdate.get.overridePrice.get == 50.00)
      }
    }

    "update override prices back to Null for an array of eventSeatIds" in {
      for {
        event <- r.event()

        row1seat1 <- r.seat(seat = "seat1", row = "row1", section = "section1")
        row1seat2 <- r.seat(seat = "seat2", row = "row1", section = "section1")

        row1eSeat1 <- r.eventSeat(
          seatId = row1seat1.id.get,
          eventId = event.id.get,
          overridePrice = Some(BigDecimal(50.00))
        )
        row1eSeat2 <- r.eventSeat(
          seatId = row1seat2.id.get,
          eventId = event.id.get,
          overridePrice = Some(BigDecimal(50.00))
        )

        seatsIdsToUpdate: Array[Int] = Array(row1eSeat1.id.get, row1eSeat2.id.get)
        newOverridePrice = None

        esUpdatedCount <- eventSeatService.bulkUpdate(seatsIdsToUpdate, newOverridePrice, None)
        row1eSeat1AfterUpdate <- eventSeatService.get(row1eSeat1.seatId, row1eSeat1.eventId)
        row1eSeat2AfterUpdate <- eventSeatService.get(row1eSeat2.seatId, row1eSeat2.eventId)
      } yield {
        assert(esUpdatedCount == 2)
        assert(row1eSeat1AfterUpdate.get.overridePrice.isEmpty)
        assert(row1eSeat2AfterUpdate.get.overridePrice.isEmpty)
      }
    }

    "update isListed boolean for an array of eventSeatIds" in {
      for {
        event <- r.event()

        row1seat1 <- r.seat(seat = "seat1", row = "row1", section = "section1")
        row1seat2 <- r.seat(seat = "seat2", row = "row1", section = "section1")

        row1eSeat1 <- r.eventSeat(
          seatId = row1seat1.id.get,
          eventId = event.id.get,
          isListed = true
        )
        row1eSeat2 <- r.eventSeat(
          seatId = row1seat2.id.get,
          eventId = event.id.get,
          isListed = true
        )

        seatsIdsToUpdate: Array[Int] = Array(row1eSeat1.id.get, row1eSeat2.id.get)
        newIsListed = false

        esUpdatedCount <- eventSeatService.bulkUpdate(seatsIdsToUpdate, None, Some(newIsListed))
        row1eSeat1AfterUpdate <- eventSeatService.get(row1eSeat1.seatId, row1eSeat1.eventId)
        row1eSeat2AfterUpdate <- eventSeatService.get(row1eSeat2.seatId, row1eSeat2.eventId)
      } yield {
        assert(esUpdatedCount == 2)
        assert(row1eSeat1AfterUpdate.get.isListed == false)
        assert(row1eSeat2AfterUpdate.get.isListed == false)
      }
    }
  }

  "EventSeatService#get" should {
    "find event seat by eventId and seatId" in {
      for {
        s <- r.seat()
        e <- r.event()
        _ <- r.eventSeat(seatId = s.id.get, eventId = e.id.get)
        es <- eventSeatService.get(s.id.get, e.id.get)
      } yield {
        assert(es.isDefined)

        val eventSeat = es.get
        assert(eventSeat.seatId == s.id.get)
        assert(eventSeat.eventId == e.id.get)
      }
    }

    "not find an event seat by eventId and seatId" in {
      for {
        s <- r.seat()
        e <- r.event()
        _ <- r.eventSeat(seatId = s.id.get, eventId = e.id.get)
        es <- eventSeatService.get(s.id.get + 1, e.id.get)
      } yield {
        assert(es.isEmpty)
      }
    }
  }

  "EventSeatService#upsert" should {
    "update an existing event seat" in {
      val now = DateHelper.now()

      for {
        event <- r.event()
        seat <- r.seat()
        priceScaleId <- r.priceScaleId()
        original <- r.eventSeat(
          seatId = seat.id.get,
          eventId = event.id.get,
          isListed = false,
          overridePrice = Some(17.99)
        )
        _ <- r.some(_ => r.eventSeat(eventId = event.id.get))
        beforeCount <- eventSeatService.getForEvents(Seq(event.id.get)).map(_.length)
        res <- eventSeatService.upsert(
          EventSeat(
            id = None,
            seatId = seat.id.get,
            eventId = event.id.get,
            priceScaleId = priceScaleId,
            listedPrice = Some(19.99),
            overridePrice = Some(29.99),
            isListed = true,
            createdAt = now,
            modifiedAt = now,
            isHeld = false
          )
        )
        afterCount <- eventSeatService.getForEvents(Seq(event.id.get)).map(_.length)
        updated <- eventSeatService.getById(res)
      } yield {
        assert(afterCount == beforeCount)

        assert(updated.isDefined)
        assert(updated.get.eventId == event.id.get)
        assert(updated.get.seatId == seat.id.get)
        assert(updated.get.listedPrice.contains(19.99))
        assert(updated.get.priceScaleId == priceScaleId)
        // isListed and overridePrice should be ignored for updates
        assert(updated.get.overridePrice == original.overridePrice)
        assert(updated.get.isListed == original.isListed)

        assert(original.eventId == event.id.get)
        assert(original.seatId == seat.id.get)
        assert(!original.listedPrice.contains(19.99))
        assert(!original.overridePrice.contains(29.99))
        assert(original.priceScaleId != priceScaleId)
        assert(!original.isListed)
      }
    }

    "insert a new event seat" in {
      val now = DateHelper.now()

      for {
        event <- r.event()
        seat <- r.seat()
        priceScaleId <- r.priceScaleId()
        _ <- r.some(_ => r.eventSeat(eventId = event.id.get, isListed = false))
        beforeCount <- eventSeatService.getForEvents(Seq(event.id.get)).map(_.length)
        res <- eventSeatService.upsert(
          EventSeat(
            id = None,
            seatId = seat.id.get,
            eventId = event.id.get,
            priceScaleId = priceScaleId,
            listedPrice = Some(19.99),
            overridePrice = Some(29.99),
            isListed = true,
            createdAt = now,
            modifiedAt = now,
            isHeld = false
          )
        )
        afterCount <- eventSeatService.getForEvents(Seq(event.id.get)).map(_.length)
        updated <- eventSeatService.getById(res)
      } yield {
        assert(afterCount - beforeCount == 1)

        assert(updated.isDefined)
        assert(updated.get.eventId == event.id.get)
        assert(updated.get.seatId == seat.id.get)
        assert(updated.get.listedPrice.contains(19.99))
        assert(updated.get.overridePrice.contains(29.99))
        assert(updated.get.priceScaleId == priceScaleId)
        assert(updated.get.isListed)
      }
    }
  }

  "EventSeatService#getByRow" should {
    "fetch event seats matching filter" in {
      for {
        event <- r.event()
        priceScaleId <- r.priceScaleId()
        seats <- r.some(_ => r.seat(section = "AAA", row = "1", venueId = event.venueId))
        sectionOffSeats <- r.some(_ => r.seat(section = "BBB", row = "1", venueId = event.venueId))
        rowOffSeats <- r.some(_ => r.seat(section = "AAA", row = "2", venueId = event.venueId))
        eventSeats <- r.eventSeats(seats, Seq(event), priceScaleId)
        _ <- r.eventSeats(sectionOffSeats, Seq(event), priceScaleId)
        _ <- r.eventSeats(rowOffSeats, Seq(event), priceScaleId)
        result <- eventSeatService.getForRow(event.venueId, "AAA", "1")
      } yield {
        assert(eventSeats.toSet == result.toSet)
      }
    }
  }

  "EventSeatService#getOne" should {
    "get by EventSeatByExternalSeatLocator" in {
      val primaryEventId = 123
      val section = "AAA"
      val row = "1"
      val seatNumber = "10"

      for {
        event <- r.event(primaryEventId = Some(primaryEventId))
        seat <- r.seat(section = section, row = row, seat = seatNumber)
        eventSeat <- r.eventSeat(eventId = event.id.get, seatId = seat.id.get)
        _ <- r.some(_ => r.eventSeat(eventId = event.id.get))
        result <- eventSeatService.getOne(
          EventSeatByExternalSeatLocator(primaryEventId, section, row, seat = seatNumber)
        )
      } yield {
        assert(result.isDefined)
        assert(result.contains(eventSeat))
      }
    }

    "get by EventSeatByForeignKeys" in {
      for {
        event <- r.event()
        seat <- r.seat()
        eventSeat <- r.eventSeat(eventId = event.id.get, seatId = seat.id.get)
        _ <- r.some(_ => r.eventSeat(eventId = event.id.get))
        result <- eventSeatService.getOne(EventSeatByForeignKeys(event.id.get, seat.id.get))
      } yield {
        assert(result.isDefined)
        assert(result.contains(eventSeat))
      }
    }

    "get by EventSeatByPrimaryIdentifiers" in {
      val primaryEventId = 123
      val primarySeatId = 456

      for {
        event <- r.event(primaryEventId = Some(primaryEventId))
        seat <- r.seat(primarySeatId = primarySeatId)
        eventSeat <- r.eventSeat(eventId = event.id.get, seatId = seat.id.get)
        _ <- r.some(_ => r.eventSeat(eventId = event.id.get))
        result <- eventSeatService.getOne(
          EventSeatByPrimaryIdentifiers(primaryEventId, primarySeatId)
        )
      } yield {
        assert(result.isDefined)
        assert(result.contains(eventSeat))
      }
    }
  }
}
