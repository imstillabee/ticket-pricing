package com.eventdynamic.services

import java.sql.Timestamp
import com.eventdynamic.models.Event
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils._
import com.eventdynamic.transactions.SeasonTransaction
import org.scalatest.Matchers._
import org.scalatest._
import java.time.Instant
import java.time.temporal.ChronoUnit

class EventServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "EventService" should {
    val r = new RandomUtil(ctx)
    val eventService = new EventService(ctx)
    val eventSeatService = new EventSeatService(ctx)
    val transactionService = new TransactionService(ctx)
    val seasonTransaction =
      new SeasonTransaction(ctx, eventService, eventSeatService, transactionService)
    val seasonService = new SeasonService(ctx, seasonTransaction)

    "create event" in {
      val time = DateHelper.now()
      for {
        client <- r.client()
        venueId <- r.venue()
        eventCategoryId <- r.eventCategoryId("Marquee", client.id.get)
        SuccessResponse(createdEvent: Event) <- eventService.create(
          Some(1),
          "Event",
          Some(time),
          client.id.get,
          venueId,
          eventCategoryId
        )
      } yield {
        assert(createdEvent.id.get == 1)
      }
    }

    "get unique event" in {
      val time = Some(DateHelper.now())
      val name = "Event"

      for {
        client <- r.client()
        venueId <- r.venue()
        _ <- r.event(
          timestamp = time,
          name = name,
          clientId = client.id.get,
          venueId = venueId,
          seasonId = None,
          primaryEventId = Some(1)
        )
        event <- eventService.getUnique(name, time, client.id.get, venueId, None, Some(1), 0)
      } yield {
        event match {
          case None => fail()
          case Some(e) => {
            assert(e.name == name)
            assert(e.timestamp == time)
            assert(e.clientId == client.id.get)
            assert(e.venueId == venueId)
          }
        }
      }
    }

    "get event for unique (with None / null timestamp)" in {
      val time = None
      val name = "Event"

      for {
        createdClient <- r.client()
        venueId <- r.venue()
        eventCategoryId <- r.eventCategoryId("MARQUEE", createdClient.id.get)
        _ <- eventService.create(
          Some(1),
          name,
          time,
          createdClient.id.get,
          venueId,
          eventCategoryId
        )
        event <- eventService.getUnique(name, time, createdClient.id.get, venueId, None, Some(1), 0)
      } yield {
        event match {
          case None => fail()
          case Some(e) =>
            assert(
              e.name == name &&
                e.timestamp == time &&
                e.clientId == createdClient.id.get &&
                e.venueId == venueId
            )
        }
      }
    }

    "get None for unique" in {
      eventService
        .getUnique("Event", None, 1, 1, None, None, 0)
        .map(e => assert(e.isEmpty))
    }

    "get events by client ID & venue ID" in {
      for {
        // Create some clients
        c1 <- r.client()
        c2 <- r.client()
        // Create some venues
        v1Id <- r.venue()
        v2Id <- r.venue()
        // Create some events
        _ <- r.event(clientId = c1.id.get, venueId = v1Id)
        _ <- r.event(clientId = c2.id.get, venueId = v2Id)
        _ <- r.event(clientId = c2.id.get, venueId = v1Id)
        _ <- r.event(clientId = c1.id.get, venueId = v2Id)
        // Fetch 1 of the event combinations
        events <- eventService
          .getByClientAndVenue(clientId = c1.id.get, venueId = v1Id)
      } yield {
        assert(events.length == 1)
        assert(events.head.venueId == v1Id)
        assert(events.head.clientId == c1.id.get)
      }
    }

    "get event by integration ID" in {
      val time = DateHelper.now()
      for {
        createdClient <- r.client()
        venueId <- r.venue()
        eventCategoryId <- r.eventCategoryId("MARQUEE", createdClient.id.get)
        SuccessResponse(createdEvent: Event) <- eventService.create(
          Some(1),
          "Event",
          Some(time),
          createdClient.id.get,
          venueId,
          eventCategoryId
        )
        fetchedEvent <- eventService.getByIntegrationId(1, createdClient.id.get)
      } yield {
        assert(fetchedEvent.isDefined)
        assert(fetchedEvent.get.primaryEventId.get == createdEvent.primaryEventId.get)
      }
    }

    "get None matching integration ID" in {
      eventService
        .getByIntegrationId(1, 1)
        .map(res => {
          assert(res.isEmpty)
        })
    }

    "create unique event" in {
      val time = None
      val name = "Event"

      for {
        createdClient <- r.client()
        venueId <- r.venue()
        eventCategoryId <- r.eventCategoryId("MARQUEE", createdClient.id.get)
        SuccessResponse(event: Event) <- eventService.createUnique(
          Some(1),
          name,
          time,
          createdClient.id.get,
          venueId,
          eventCategoryId
        )
      } yield {
        assert(event.id.get == 1)
      }
    }

    "not create unique event, but return DuplicateResponse if event already exists" in {
      val time = None
      val name = "Event"

      for {
        createdClient <- r.client()
        venueId <- r.venue()
        eventCategoryId <- r.eventCategoryId("MARQUEE", createdClient.id.get)
        _ <- eventService.create(
          Some(1),
          name,
          time,
          createdClient.id.get,
          venueId,
          eventCategoryId
        )
        response <- eventService.createUnique(
          Some(1),
          name,
          time,
          createdClient.id.get,
          venueId,
          eventCategoryId
        )
      } yield {
        assert(DuplicateResponse == response)
      }
    }

    "get event by id" in {
      val time = DateHelper.now()
      for {
        createdClient <- r.client()
        venueId <- r.venue()
        eventCategoryId <- r.eventCategoryId("MARQUEE", createdClient.id.get)
        SuccessResponse(event: Event) <- eventService.create(
          Some(1),
          "Event",
          Some(time),
          createdClient.id.get,
          venueId,
          eventCategoryId
        )
        createdEvent <- eventService.getById(event.id.get)
      } yield {
        createdEvent match {
          case Some(e) => assert(e.id.get == event.id.get)
          case None    => fail()
        }
      }
    }

    "update event" in {
      for {
        createdEvent <- r.event()
        event <- eventService.getById(createdEvent.id.get)
        _ <- eventService.update(event.get.copy(name = "Updated Event"))
        updatedOptEvent <- eventService.getById(createdEvent.id.get)
      } yield
        assert(
          updatedOptEvent.get.name == "Updated Event" && updatedOptEvent.get.id.get == createdEvent.id.get
        )
    }

    "update unique event and return successful if event exists" in {
      val time = DateHelper.now()
      for {
        client <- r.client()
        venueId <- r.venue()
        eventCategoryId <- r.eventCategoryId("MARQUEE", client.id.get)
        createdEvent <- r.event()
        _ <- eventService.updateUnique(
          createdEvent.id.get,
          Some(1),
          "Updated Event",
          Some(time),
          client.id.get,
          venueId,
          eventCategoryId,
          None
        )
        updatedOptEvent <- eventService.getById(createdEvent.id.get)
      } yield {
        assert(updatedOptEvent.get.name == "Updated Event")
        assert(updatedOptEvent.get.id.get == createdEvent.id.get)
      }
    }

    "update unique event and return NotFoundResponse if event does not exist" in {
      val time = DateHelper.now()
      for {
        client <- r.client()
        venueId <- r.venue()
        eventCategoryId <- r.eventCategoryId("MARQUEE", client.id.get)
        response <- eventService.updateUnique(
          1,
          Some(1),
          "Updated Event",
          Some(time),
          client.id.get,
          venueId,
          eventCategoryId,
          None
        )
      } yield assert(response === NotFoundResponse)
    }

    "get all events for admin user" in {
      for {
        client1 <- r.client()
        client2 <- r.client()
        createdEvent1 <- r.event(clientId = client1.id.get)
        createdEvent2 <- r.event(clientId = client2.id.get)
        response <- eventService.getAll(None, isAdmin = true)
      } yield {
        assert(response.head == createdEvent1)
        assert(response.apply(1) == createdEvent2)
      }
    }

    "get all events for admin user with clientId specified" in {
      for {
        createdClient <- r.client()
        createdClient2 <- r.client()
        event <- r.event(clientId = createdClient.id.get)
        _ <- r.event(clientId = createdClient2.id.get)
        response <- eventService.getAll(Option(createdClient.id.get), isAdmin = true)
      } yield {
        assert(response.head == event)
        assert(response.length == 1)
      }
    }

    "add events to a season" in {
      val time = DateHelper.now()
      for {
        season <- r.season()
        client <- r.client()
        eventCategoryId <- r.eventCategoryId("MARQUEE", client.id.get)
        venueId <- r.venue()
        SuccessResponse(createdEvent: Event) <- eventService.create(
          Some(1),
          "Event",
          Some(time),
          client.id.get,
          venueId,
          eventCategoryId,
          season.id
        )
      } yield {
        assert(createdEvent.seasonId == season.id)
      }
    }

    "cascade null when season is deleted" in {
      for {
        createdSeason <- r.season()
        createdEvent <- r.event(seasonId = createdSeason.id)
        _ <- seasonService.delete(createdSeason.id.get)
        event <- eventService.getById(createdEvent.id.get)
      } yield {
        assert(event.get.seasonId === None)
      }
    }

    "toggle isBroadcast for an event" in {
      for {
        createdEvent <- r.event()
        updatedEvent <- eventService.toggleIsBroadcast(createdEvent.id.get, false)
      } yield {
        updatedEvent.get._1 shouldBe false
        updatedEvent.get._2 shouldBe a[Timestamp]
      }
    }

    "update the factors" in {
      for {
        event1 <- r.event()
        event2 <- r.event()
        event3 <- r.event(eventScore = Some(10))
        event4 <- r.event(eventScore = Some(10))
        _ <- eventService.updateFactors(event1.id.get, None, Some(2))
        _ <- eventService.updateFactors(event2.id.get, Some(15), None)
        _ <- eventService.updateFactors(event3.id.get, None, None)
        _ <- eventService.updateFactors(event4.id.get, Some(15), Some(2))
        updatedEvents <- eventService.getByIdBulk(
          Set[Int](event1.id.get, event2.id.get, event3.id.get, event4.id.get)
        )
      } yield {
        updatedEvents.size shouldBe 4

        updatedEvents(0).eventScore shouldBe None
        updatedEvents(1).eventScore shouldBe Some(15)
        updatedEvents(2).eventScore shouldBe None
        updatedEvents(3).eventScore shouldBe Some(15)

        updatedEvents(0).spring shouldBe Some(2)
        updatedEvents(1).spring shouldBe None
        updatedEvents(2).spring shouldBe None
        updatedEvents(3).spring shouldBe Some(2)
      }
    }

    "get upcoming events" in {
      val futureTime = Timestamp.from(Instant.now().plus(5, ChronoUnit.DAYS))
      val pastTime = Timestamp.from(Instant.now().minus(5, ChronoUnit.DAYS))
      val currentTime = DateHelper.now()
      for {
        client <- r.client()
        event1 <- r.event(clientId = client.id.get, timestamp = Some(futureTime))
        _ <- r.event(clientId = client.id.get, timestamp = Some(pastTime))
        event3 <- r.event(clientId = client.id.get, timestamp = Some(currentTime))
        response <- eventService.getUpcomingEvents(Seq(client.id.get), currentTime)
      } yield {
        assert(response.length == 2)
        assert(response.contains(event1))
        assert(response.contains(event3))
      }
    }

    "get upcoming events for all clients" in {
      val futureTime = Timestamp.from(Instant.now().plus(5, ChronoUnit.DAYS))
      val pastTime = Timestamp.from(Instant.now().minus(5, ChronoUnit.DAYS))
      val currentTime = DateHelper.now()
      for {
        client1 <- r.client()
        client2 <- r.client()
        event1 <- r.event(clientId = client1.id.get, timestamp = Some(futureTime))
        _ <- r.event(clientId = client1.id.get, timestamp = Some(pastTime))
        event3 <- r.event(clientId = client1.id.get, timestamp = Some(currentTime))
        event4 <- r.event(clientId = client2.id.get, timestamp = Some(futureTime))
        _ <- r.event(clientId = client2.id.get, timestamp = Some(pastTime))
        event6 <- r.event(clientId = client2.id.get, timestamp = Some(currentTime))
        response <- eventService.getUpcomingEventsForAllClients(currentTime, None)
      } yield {
        assert(response.length == 4)
        assert(response.contains(event1))
        assert(response.contains(event3))
        assert(response.contains(event4))
        assert(response.contains(event6))
      }
    }

    "get upcoming events for all clients with an after and before date" in {
      val nextFutureTime = Timestamp.from(Instant.now().plus(10, ChronoUnit.DAYS))
      val futureTime = Timestamp.from(Instant.now().plus(5, ChronoUnit.DAYS))
      val pastTime = Timestamp.from(Instant.now().minus(5, ChronoUnit.DAYS))
      val currentTime = DateHelper.now()
      for {
        client1 <- r.client()
        client2 <- r.client()
        event1 <- r.event(clientId = client1.id.get, timestamp = Some(futureTime))
        _ <- r.event(clientId = client1.id.get, timestamp = Some(pastTime))
        event3 <- r.event(clientId = client1.id.get, timestamp = Some(currentTime))
        event4 <- r.event(clientId = client2.id.get, timestamp = Some(nextFutureTime))
        _ <- r.event(clientId = client2.id.get, timestamp = Some(pastTime))
        event6 <- r.event(clientId = client2.id.get, timestamp = Some(currentTime))
        response <- eventService.getUpcomingEventsForAllClients(currentTime, Some(futureTime))
      } yield {
        assert(response.length == 3)
        assert(response.contains(event1))
        assert(response.contains(event3))
        assert(response.contains(event6))
      }
    }
  }
}
