package com.eventdynamic.services

import java.time.{LocalDate, ZoneId}

import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import org.scalatest.AsyncWordSpec

class EventTimeGroupsServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val eventTimeGroupsService = new EventTimeGroupsService(ctx)
  val materializedView = new MaterializedViewService(ctx)
  private val tz = ZoneId.of("America/Los_Angeles")

  "EventTimeGroupsService#getActualTimeStats" should {
    "return time stats for the range inclusively when there are no transactions" in {
      val start = LocalDate.parse("2019-01-01")
      val end = LocalDate.parse("2019-01-02")

      for {
        venueId <- r.venue(timeZone = tz.toString)
        event <- r.event(venueId = venueId)
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        stats <- eventTimeGroupsService.getActualTimeStats(
          clientId = event.clientId,
          from = start.atStartOfDay(tz).toInstant,
          to = end.atStartOfDay(tz).toInstant,
          eventId = event.id
        )
      } yield {
        assert(stats.head.intervalStart.toInstant == start.atStartOfDay(tz).toInstant)
        assert(stats.last.intervalEnd.toInstant == end.atStartOfDay(tz).toInstant)
        assert(stats.length == 24)
      }
    }
  }

}
