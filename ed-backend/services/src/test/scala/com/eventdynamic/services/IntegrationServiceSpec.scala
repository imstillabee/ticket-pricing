package com.eventdynamic.services

import com.eventdynamic.test.DatabaseSuite
import org.scalatest.AsyncWordSpec
import org.scalatest.Matchers._
import org.scalatest.OptionValues._

class IntegrationServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val numSeededIntegrations = 9

  "IntegrationService" should {
    val integrationService = new IntegrationService(ctx)

    "get an integration by Id" in {
      for {
        fetchedIntegration <- integrationService.getById(1)
      } yield {
        fetchedIntegration.value.name shouldEqual "Tickets.com"
      }
    }

    "get an integration by Name" in {
      for {
        fetchedIntegration <- integrationService.getByName("Skybox")
      } yield {
        assert(fetchedIntegration.get.id.get === 5)
      }
    }

    "get all integrations" in {
      integrationService
        .getAll()
        .map(integrations => assert(integrations.length === numSeededIntegrations))
    }
  }
}
