package com.eventdynamic.services

import com.eventdynamic.models.EventSeat
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest.AsyncWordSpec

import scala.concurrent.Future

class InventoryServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val services = new com.eventdynamic.test.ServiceHolder(ctx)
  val r = new RandomUtil(ctx)

  val inventoryService =
    new InventoryService(ctx, services.eventSeat, services.event, services.seat, services.venue)

  "InventoryService" should {
    "get all inventory for season and client" in {
      val N_SEATS = 20
      val now = DateHelper.now()

      for {
        client <- r.client()
        venue <- r.venue()
        season <- r.season()
        priceScaleId <- r.priceScaleId()
        event <- r.event(
          seasonId = season.id,
          clientId = client.id.get,
          venueId = venue,
          timestamp = Some(DateHelper.now())
        )
        seats <- r.some(_ => r.seat(), N_SEATS)
        _ <- Future.sequence(
          seats
            .slice(0, N_SEATS / 2)
            .map(
              s =>
                services.eventSeat
                  .create(
                    EventSeat(
                      None,
                      s.id.get,
                      event.id.get,
                      priceScaleId,
                      Some(20),
                      None,
                      true,
                      now,
                      now,
                      false
                    )
                )
            )
        )
        _ <- Future.sequence(
          seats
            .slice(0, N_SEATS / 2)
            .map(
              s =>
                services.eventSeat
                  .create(
                    EventSeat(
                      None,
                      s.id.get,
                      event.id.get,
                      priceScaleId,
                      Some(20),
                      None,
                      false,
                      now,
                      now,
                      false
                    )
                )
            )
        )
        inventory <- inventoryService.getInventory(clientId = event.clientId)
      } yield {
        assert(inventory.length === 10)
      }
    }
  }
}
