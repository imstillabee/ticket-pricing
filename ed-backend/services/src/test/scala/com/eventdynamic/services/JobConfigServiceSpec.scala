package com.eventdynamic.services

import com.eventdynamic.models.JobType
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import org.scalatest.AsyncWordSpec
import play.api.libs.json.{Format, Json}

class JobConfigServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val jobConfigService = new JobConfigService(ctx)

  case class Config(id: Int, str1: String, str2: String)

  implicit val configFormat: Format[Config] = Json.format[Config]

  val jobType = JobType.PrimarySync
  val config = Config(1, "aString", "bString")

  "JobConfigService" should {
    "perform crud on a config" in {
      val newConfig = config.copy(str1 = "some other string")

      for {
        client <- r.client()
        createResult <- jobConfigService.createConfig(client.id.get, jobType, config)
        createdConfig <- jobConfigService.getConfig(client.id.get, jobType)
        createdRawConfig <- jobConfigService.getRawConfig(client.id.get, jobType)
        updateResult <- jobConfigService.updateConfig(client.id.get, jobType, newConfig)
        updatedConfig <- jobConfigService.getConfig(client.id.get, jobType)
        deleteResult <- jobConfigService.deleteConfig(client.id.get, jobType)
        deletedConfig <- jobConfigService.getConfig(client.id.get, jobType)
      } yield {
        assert(createResult.clientId == client.id.get)
        assert(createdConfig.contains(config))
        assert(createdRawConfig.get.json.contains("aString"))
        assert(updateResult == 1)
        assert(updatedConfig.contains(newConfig))
        assert(deleteResult == 1)
        assert(deletedConfig.isEmpty)
      }
    }
  }
}
