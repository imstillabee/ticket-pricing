package com.eventdynamic.services

import com.eventdynamic.models.JobTriggerType
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import org.scalatest.AsyncWordSpec

class JobTriggerServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val jobTriggerService = new JobTriggerService(ctx)
  val numSeededJobTriggers = 2

  "JobTriggerService" should {
    "get job trigger by id" in {
      for {
        jobTrigger <- jobTriggerService.getById(1)
      } yield {
        assert(jobTrigger.isDefined && jobTrigger.get.id.get === 1)
      }
    }

    "get by trigger type" in {
      for {
        getByTriggerTypeResult <- jobTriggerService.getByTriggerType(JobTriggerType.Manual)
      } yield {
        assert(
          getByTriggerTypeResult.isDefined && getByTriggerTypeResult.get.triggerType === JobTriggerType.Manual
        )
      }
    }

    "get all job triggers" in {
      for {
        jobTriggers <- jobTriggerService.getAll()
      } yield {
        assert(jobTriggers.length === numSeededJobTriggers)
      }
    }
  }
}
