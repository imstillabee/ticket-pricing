package com.eventdynamic.services

import com.eventdynamic.models.{MLBAMScheduleToEvent, MLBAMSectionToPriceScale, MLBAMTeamToClient}
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.SuccessResponse
import org.scalatest.AsyncWordSpec

class MLBAMMappingServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val mlbamMappingService = new MLBAMMappingService(ctx)

  "MLBAMMappingService" should {
    "TeamClientMapping" should {
      val testTeamId = 121
      "return created mapping" in {
        for {
          client <- r.client()
          teamClientMapping <- mlbamMappingService.createTeamClientMap(testTeamId, client.id.get)
        } yield {
          teamClientMapping match {
            case SuccessResponse(mapping: MLBAMTeamToClient) => {
              assert(mapping.teamId == testTeamId)
              assert(mapping.clientId == client.id.get)
            }
            case _ => fail()
          }
        }
      }

      "return teamId given valid clientId" in {
        for {
          client <- r.client()
          insertedTeamClientMap <- r.teamToClientMap(testTeamId, client.id.get)
          teamId <- mlbamMappingService.getTeamIdByClientId(client.id.get)
        } yield {
          assert(teamId.get == insertedTeamClientMap.teamId)
        }
      }

      "return None when given invalid clientId" in {
        for {
          client <- r.client()
          map <- r.teamToClientMap(testTeamId, client.id.get)
          teamId <- mlbamMappingService.getTeamIdByClientId(-1)
        } yield {
          assert(teamId == None)
        }
      }
    }

    "SectionPriceScaleMapping" should {
      val testSectionId = 10
      "return created mapping" in {
        for {
          priceScaleId <- r.priceScaleId()
          sectionPriceScaleMapping <- mlbamMappingService.createSectionPriceScaleMap(
            testSectionId,
            priceScaleId
          )
        } yield {
          sectionPriceScaleMapping match {
            case SuccessResponse(mapping: MLBAMSectionToPriceScale) => {
              assert(mapping.sectionId == testSectionId)
              assert(mapping.priceScaleId == priceScaleId)
            }
            case _ => fail()
          }
        }
      }

      "return sectionId given valid priceScaleId" in {
        for {
          priceScaleId <- r.priceScaleId()
          insertedSectionPriceScaleMap <- r.sectionToPriceScaleMap(testSectionId, priceScaleId)
          sectionId <- mlbamMappingService.getSectionIdByPriceScaleId(priceScaleId)
        } yield {
          assert(sectionId.get == insertedSectionPriceScaleMap.sectionId)
        }
      }

      "return None when given invalid priceScaleId" in {
        for {
          priceScaleId <- r.priceScaleId()
          map <- r.sectionToPriceScaleMap(testSectionId, priceScaleId)
          sectionId <- mlbamMappingService.getSectionIdByPriceScaleId(-1)
        } yield {
          assert(sectionId == None)
        }
      }
    }

    "ScheduleEventMapping" should {
      val testScheduleId = 50
      "return created mapping" in {
        for {
          event <- r.event()
          scheduleEventMapping <- mlbamMappingService.createScheduleEventMap(
            testScheduleId,
            event.id.get
          )
        } yield {
          scheduleEventMapping match {
            case SuccessResponse(mapping: MLBAMScheduleToEvent) => {
              assert(mapping.scheduleId == testScheduleId)
              assert(mapping.eventId == event.id.get)
            }
            case _ => fail()
          }
        }
      }

      "return scheduleId given valid eventId" in {
        for {
          event <- r.event()
          insertedScheduleEventMap <- r.scheduleEventMap(testScheduleId, event.id.get)
          scheduleId <- mlbamMappingService.getScheduleIdByEventId(event.id.get)
        } yield {
          assert(scheduleId.get == insertedScheduleEventMap.scheduleId)
        }
      }

      "return None when given invalid eventId" in {
        for {
          event <- r.event()
          mapping <- r.scheduleEventMap(testScheduleId, event.id.get)
          scheduleId <- mlbamMappingService.getScheduleIdByEventId(-1)
        } yield {
          assert(scheduleId == None)
        }
      }
    }
  }
}
