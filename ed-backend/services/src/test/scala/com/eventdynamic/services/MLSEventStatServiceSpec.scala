package com.eventdynamic.services

import com.eventdynamic.models.MLSEventStat
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest._

class MLSEventStatServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "MLSEventStatService" should {
    val r = new RandomUtil(ctx)
    val mlsEventStatService = r.services.mlsEventStat

    def createMLSEventStatForClient() = {
      for {
        createdClient <- r.client()
        createdEvent <- r.event(clientId = createdClient.id.get)
        mlsEventStat: MLSEventStat <- r.services.mlsEventStat.create(createdEvent.id.get, "NYC", 1)
      } yield mlsEventStat
    }

    "create MLS Event Stats" in {
      for {
        mlsEventStat <- createMLSEventStatForClient()
      } yield assert(mlsEventStat.id.get === 1)
    }

    "get MLS Event Stats by Id" in {
      for {
        mlsEventStat <- createMLSEventStatForClient()
        stats <- mlsEventStatService.getById(mlsEventStat.id.get)
      } yield assert(stats.get.id.get === mlsEventStat.id.get)
    }

    "get MLS Event Stats by Client Id" in {
      for {
        createdClient <- r.client()
        createdEvent <- r.event(clientId = createdClient.id.get)
        mlsEventStat <- mlsEventStatService.create(createdEvent.id.get, "NYC", 1)
        stats <- mlsEventStatService.getByClientId(createdClient.id.get)
      } yield assert(stats(0).id.get === mlsEventStat.id.get)
    }

    "get MLS Event Stats by Event Id" in {
      for {
        createdClient <- r.client()
        createdEvent <- r.event(clientId = createdClient.id.get)
        mlsEventStat <- mlsEventStatService.create(createdEvent.id.get, "NYC", 1)
        stats <- mlsEventStatService.getByEventId(createdEvent.id.get)
      } yield {
        assert(stats.isDefined)
        assert(stats.get.id.get === mlsEventStat.id.get)
      }
    }

    "get MLS Event Stats by Event Ids" in {
      for {
        createdClient <- r.client()
        event1 <- r.event(clientId = createdClient.id.get)
        event2 <- r.event(clientId = createdClient.id.get)
        event3 <- r.event(clientId = createdClient.id.get)
        _ <- mlsEventStatService.create(event1.id.get, "NYC", 1)
        _ <- mlsEventStatService.create(event2.id.get, "NYC", 1)
        _ <- mlsEventStatService.create(event3.id.get, "DAL", 1)
        stats <- mlsEventStatService.getByEventIds(Seq(event1.id.get, event2.id.get))
      } yield {
        assert(stats.length === 2)
        assert(stats.forall(_.opponent === "NYC"))
      }
    }

    "update MLS Event Stats" in {
      for {
        mlsEventStat <- createMLSEventStatForClient()
        updatedMLSEventStat <- mlsEventStatService.upsert(mlsEventStat.copy(homeGameNumber = 2))
      } yield {
        assert(updatedMLSEventStat.id === mlsEventStat.id)
        assert(updatedMLSEventStat.homeGameNumber === 2)
      }
    }

    "throw an exception when trying to update non-existant MLS Event Stats" in {
      val now = DateHelper.now()

      val updateFuture = for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)
        createdMLSSeasonStat <- mlsEventStatService.upsert(
          MLSEventStat(Some(99999), now, now, event.id.get, "???", 1)
        )
      } yield createdMLSSeasonStat

      recoverToSucceededIf[Exception](updateFuture)
    }

    "choose to create MLS Event Stats" in {
      val now = DateHelper.now()
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)
        createdMLSEventStat <- mlsEventStatService.upsert(
          MLSEventStat(None, now, now, event.id.get, "???", 1)
        )
      } yield {
        assert(createdMLSEventStat.id.isDefined)
      }
    }
  }

}
