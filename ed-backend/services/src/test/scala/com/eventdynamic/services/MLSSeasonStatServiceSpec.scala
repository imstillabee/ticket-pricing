package com.eventdynamic.services

import com.eventdynamic.models.MLSSeasonStat
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest._

class MLSSeasonStatServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "MLSSeasonStatService" should {
    val r = new RandomUtil(ctx)
    val mlsSeasonStatService = r.services.mlsSeasonStat

    def createMLSSeasonStatForClient() = {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        mlsSeasonStat <- r.services.mlsSeasonStat.create(createdSeason.id.get, 7, 8, 1, 16)
      } yield mlsSeasonStat
    }

    "create MLS Season Stats" in {
      for {
        mlsSeasonStat <- createMLSSeasonStatForClient()
      } yield assert(mlsSeasonStat.id.get === 1)
    }

    "get MLS Season Stats by Id" in {
      for {
        mlsSeasonStat <- createMLSSeasonStatForClient()
        stats <- mlsSeasonStatService.getById(mlsSeasonStat.id.get)
      } yield assert(stats.get.id.get === mlsSeasonStat.id.get)
    }

    "get MLS Season Stats by Client Id" in {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        mlsSeasonStat <- mlsSeasonStatService.create(createdSeason.id.get, 7, 8, 1, 16)
        stats <- mlsSeasonStatService.getByClientId(createdClient.id.get)
      } yield assert(stats(0).id.get === mlsSeasonStat.id.get)
    }

    "get MLS Season Stats by Season Ids" in {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        mlsSeasonStat <- mlsSeasonStatService.create(createdSeason.id.get, 7, 8, 1, 16)
        stats <- mlsSeasonStatService.getBySeasonId(createdSeason.id.get)
      } yield {
        assert(stats.isDefined)
        assert(stats.get.id.get === mlsSeasonStat.id.get)
      }
    }

    "get MLS Season Stats by Season Id" in {
      for {
        createdClient <- r.client()
        createdSeason1 <- r.season(clientId = createdClient.id.get)
        createdSeason2 <- r.season(clientId = createdClient.id.get)
        createdSeason3 <- r.season(clientId = createdClient.id.get)
        _ <- mlsSeasonStatService.create(createdSeason1.id.get, 7, 8, 1, 16)
        _ <- mlsSeasonStatService.create(createdSeason2.id.get, 7, 8, 1, 16)
        _ <- mlsSeasonStatService.create(createdSeason3.id.get, 5, 4, 1, 10)
        stats <- mlsSeasonStatService.getBySeasonIds(
          Seq(createdSeason1.id.get, createdSeason2.id.get)
        )
      } yield {
        assert(stats.length === 2)
        assert(stats.forall(_.wins === 7))
      }
    }

    "update MLS Season Stats" in {
      for {
        mlsSeasonStat <- createMLSSeasonStatForClient()
        updatedMLSSeasonStat <- mlsSeasonStatService.createOrUpdate(
          mlsSeasonStat.copy(wins = 10, losses = 3, ties = 1)
        )
      } yield {
        assert(updatedMLSSeasonStat.id === mlsSeasonStat.id)
      }
    }

    "throw an exception when trying to update non-existant MLS Season Stats" in {
      val now = DateHelper.now()

      val updateFuture = for {
        client <- r.client()
        season <- r.season(clientId = client.id.get)

        createdMLSSeasonStat <- mlsSeasonStatService.createOrUpdate(
          MLSSeasonStat(Some(99999), now, now, season.id.get, 10, 3, 1, 16)
        )
      } yield createdMLSSeasonStat

      recoverToSucceededIf[Exception](updateFuture)
    }

    "choose to create MLS Season Stats" in {
      val now = DateHelper.now()
      for {
        client <- r.client()
        season <- r.season(clientId = client.id.get)

        createdMLSSeasonStat <- mlsSeasonStatService.createOrUpdate(
          MLSSeasonStat(None, now, now, season.id.get, 10, 3, 1, 16)
        )
      } yield {
        assert(createdMLSSeasonStat.wins === 10)
        assert(createdMLSSeasonStat.losses === 3)
        assert(createdMLSSeasonStat.ties === 1)
        assert(createdMLSSeasonStat.id.isDefined)
      }
    }
  }

}
