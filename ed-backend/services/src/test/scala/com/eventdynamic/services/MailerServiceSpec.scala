package com.eventdynamic.services

import com.amazonaws.services.simpleemail.model.{Content, Destination}
import com.eventdynamic.models.MailerConfig
import com.eventdynamic.test.DatabaseSuite
import org.scalatest.{AsyncWordSpec, PrivateMethodTester}

class MailerServiceSpec extends AsyncWordSpec with DatabaseSuite with PrivateMethodTester {

  "MailerService#getRecipientAddress" should {
    val getRecipientAddress =
      PrivateMethod[Option[Destination]]('getRecipientAddress)
    val data = Map(
      "userId" -> "1",
      "toEmail" -> "test@eventdynamic.com",
      "toName" -> "Test User",
      "tempPass" -> "asdf1234"
    )
    "return Destination if email is in whitelist" in {
      val mailerConfig: MailerConfig = MailerConfig(
        fromEmail = "someEmail",
        supportEmail = "someSupportEmail",
        edSiteLink = "edSiteLink",
        mailEnabled = true,
        whitelist = Array("eventdynamic.com"),
        userTempPasswordExpiration = "5760",
        userResetPasswordExpiration = "1440"
      )
      val mailerService = new MailerService(mailerConfig)

      val result = mailerService invokePrivate getRecipientAddress(data)

      assert(result.isDefined)
    }

    "return Destination whitelist is empty" in {
      val mailerConfig: MailerConfig = MailerConfig(
        fromEmail = "someEmail",
        supportEmail = "someSupportEmail",
        edSiteLink = "edSiteLink",
        mailEnabled = true,
        whitelist = Array.empty[String],
        userTempPasswordExpiration = "5760",
        userResetPasswordExpiration = "1440"
      )
      val mailerService = new MailerService(mailerConfig)

      val result = mailerService invokePrivate getRecipientAddress(data)

      assert(result.isDefined)
    }

    "return None if email is not in whitelist" in {
      val mailerConfig: MailerConfig = MailerConfig(
        fromEmail = "someEmail",
        supportEmail = "someSupportEmail",
        edSiteLink = "edSiteLink",
        mailEnabled = true,
        whitelist = Array("dialexa.com"),
        userTempPasswordExpiration = "5760",
        userResetPasswordExpiration = "1440"
      )
      val mailerService = new MailerService(mailerConfig)

      val result = mailerService invokePrivate getRecipientAddress(data)

      assert(result.isEmpty)
    }
  }

  "MailerService#getSubject" should {
    val getSubject = PrivateMethod[Content]('getSubject)
    val mailerConfig: MailerConfig = MailerConfig(
      fromEmail = "someEmail",
      supportEmail = "someSupportEmail",
      edSiteLink = "edSiteLink",
      mailEnabled = true,
      whitelist = Array.empty[String],
      userTempPasswordExpiration = "5760",
      userResetPasswordExpiration = "1440"
    )
    val mailerService = new MailerService(mailerConfig)
    "return 'Welcome to Event Dynamic' in content when given new_user code" in {
      val result = mailerService invokePrivate getSubject(TCode.new_user)
      assert(result.getData === "Welcome to Event Dynamic")
    }

    "return 'Requested Event Dynamic Password Reset' in content when given reset_pass code" in {
      val result = mailerService invokePrivate getSubject(TCode.reset_pass)
      assert(result.getData === "Requested Event Dynamic Password Reset")
    }
  }
}
