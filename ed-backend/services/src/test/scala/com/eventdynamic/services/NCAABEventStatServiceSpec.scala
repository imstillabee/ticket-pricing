package com.eventdynamic.services

import com.eventdynamic.models.NCAABEventStat
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest._

class NCAABEventStatServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "NCAABEventStatService" should {
    val r = new RandomUtil(ctx)
    val ncaabEventStatService = r.services.ncaabEventStat

    def createNCAABEventStatForClient() = {
      for {
        createdClient <- r.client()
        createdEvent <- r.event(clientId = createdClient.id.get)
        ncaabEventStat: NCAABEventStat <- r.services.ncaabEventStat.create(
          createdEvent.id.get,
          "ATL",
          false,
          false
        )
      } yield ncaabEventStat
    }

    "create NCAAB Event Stats" in {
      for {
        ncaabEventStat <- createNCAABEventStatForClient()
      } yield assert(ncaabEventStat.id.get === 1)
    }

    "get NCAAB Event Stats by Id" in {
      for {
        ncaabEventStat <- createNCAABEventStatForClient()
        stats <- ncaabEventStatService.getById(ncaabEventStat.id.get)
      } yield assert(stats.get.id.get === ncaabEventStat.id.get)
    }

    "get NCAAB Event Stats by Client Id" in {
      for {
        createdClient <- r.client()
        createdEvent <- r.event(clientId = createdClient.id.get)
        ncaabEventStat <- ncaabEventStatService.create(createdEvent.id.get, "ATL", false, false)
        stats <- ncaabEventStatService.getByClientId(createdClient.id.get)
      } yield assert(stats(0).id.get === ncaabEventStat.id.get)
    }

    "get NCAAB Event Stats by Event Id" in {
      for {
        ncaabEventStat <- createNCAABEventStatForClient()
        stats <- ncaabEventStatService.getByEventId(ncaabEventStat.eventId)
      } yield {
        assert(stats.isDefined)
        assert(stats.get.id.get === ncaabEventStat.id.get)
      }
    }

    "get NCAAB Event Stats by Event Ids" in {
      for {
        event1 <- r.event()
        event2 <- r.event()
        event3 <- r.event()
        _ <- ncaabEventStatService.create(event1.id.get, "ATL", false, false)
        _ <- ncaabEventStatService.create(event2.id.get, "ATL", false, false)
        _ <- ncaabEventStatService.create(event3.id.get, "DAL", false, false)
        stats <- ncaabEventStatService.getByEventIds(Seq(event1.id.get, event2.id.get))
      } yield {
        assert(stats.length === 2)
        assert(stats.forall(_.opponent === "ATL"))
      }
    }

    "update NCAAB Event Stats" in {
      for {
        ncaabEventStat <- createNCAABEventStatForClient()
        updatedId <- ncaabEventStatService.upsert(
          ncaabEventStat.copy(isHomeOpener = true, isPreSeason = true)
        )
        updatedNCAABEventStat <- ncaabEventStatService.getById(updatedId)
      } yield {
        assert(updatedNCAABEventStat.get.isHomeOpener)
        assert(updatedNCAABEventStat.get.isPreSeason)
        assert(updatedNCAABEventStat.get.id === ncaabEventStat.id)
      }
    }

    "throw an exception when trying to update non-existant NCAAB Event Stats" in {
      val now = DateHelper.now()

      val updateFuture = for {
        createdEventStat <- createNCAABEventStatForClient()

        createdNCAABEventStat <- ncaabEventStatService.upsert(
          NCAABEventStat(
            Some(createdEventStat.id.get + 1),
            now,
            now,
            createdEventStat.eventId,
            "???",
            false,
            false
          )
        )
      } yield createdNCAABEventStat

      recoverToSucceededIf[Exception](updateFuture)
    }

    "choose to create NCAAB Event Stats" in {
      val now = DateHelper.now()
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)

        createdId <- ncaabEventStatService.upsert(
          NCAABEventStat(None, now, now, event.id.get, "???", false, false)
        )
        createdNCAABEventStat <- ncaabEventStatService.getById(createdId)
      } yield {
        assert(createdNCAABEventStat.get.opponent === "???")
        assert(createdNCAABEventStat.get.isHomeOpener === false)
        assert(createdNCAABEventStat.get.isPreSeason === false)
        assert(createdNCAABEventStat.get.id.isDefined)
      }
    }
  }

}
