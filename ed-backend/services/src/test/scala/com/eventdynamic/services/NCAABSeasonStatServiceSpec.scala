package com.eventdynamic.services

import com.eventdynamic.models.NCAABSeasonStat
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest._

class NCAABSeasonStatServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "NCAABSeasonStatService" should {
    val r = new RandomUtil(ctx)
    val ncaabSeasonStatService = r.services.ncaabSeasonStat

    def createNCAABSeasonStatForClient() = {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        ncaabSeasonStat <- r.services.ncaabSeasonStat.create(createdSeason.id.get, 7, 8, 16)
      } yield ncaabSeasonStat
    }

    "create NCAAB Season Stats" in {
      for {
        ncaabSeasonStat <- createNCAABSeasonStatForClient()
      } yield assert(ncaabSeasonStat.id.get === 1)
    }

    "get NCAAB Season Stats by Id" in {
      for {
        ncaabSeasonStat <- createNCAABSeasonStatForClient()
        stats <- ncaabSeasonStatService.getById(ncaabSeasonStat.id.get)
      } yield assert(stats.get.id.get === ncaabSeasonStat.id.get)
    }

    "get NCAAB Season Stats by Client Id" in {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        ncaabSeasonStat <- ncaabSeasonStatService.create(createdSeason.id.get, 7, 8, 16)
        stats <- ncaabSeasonStatService.getByClientId(createdClient.id.get)
      } yield assert(stats(0).id.get === ncaabSeasonStat.id.get)
    }

    "get NCAAB Season Stats by Season Ids" in {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        ncaabSeasonStat <- ncaabSeasonStatService.create(createdSeason.id.get, 7, 8, 16)
        stats <- ncaabSeasonStatService.getBySeasonId(createdSeason.id.get)
      } yield {
        assert(stats.isDefined)
        assert(stats.get.id.get === ncaabSeasonStat.id.get)
      }
    }

    "get NCAAB Season Stats by Season Id" in {
      for {
        createdClient <- r.client()
        createdSeason1 <- r.season(clientId = createdClient.id.get)
        createdSeason2 <- r.season(clientId = createdClient.id.get)
        createdSeason3 <- r.season(clientId = createdClient.id.get)
        _ <- ncaabSeasonStatService.create(createdSeason1.id.get, 7, 8, 16)
        _ <- ncaabSeasonStatService.create(createdSeason2.id.get, 7, 8, 16)
        _ <- ncaabSeasonStatService.create(createdSeason3.id.get, 5, 4, 10)
        stats <- ncaabSeasonStatService.getBySeasonIds(
          Seq(createdSeason1.id.get, createdSeason2.id.get)
        )
      } yield {
        assert(stats.length === 2)
        assert(stats.forall(_.wins === 7))
      }
    }

    "update NCAAB Season Stats" in {
      for {
        ncaabSeasonStat <- createNCAABSeasonStatForClient()
        updatedId <- ncaabSeasonStatService.upsert(ncaabSeasonStat.copy(wins = 10, losses = 3))
        updatedNCAABSeasonStat <- ncaabSeasonStatService.getById(updatedId)
      } yield {
        assert(updatedNCAABSeasonStat.get.wins === 10)
        assert(updatedNCAABSeasonStat.get.losses === 3)
        assert(updatedNCAABSeasonStat.get.id === ncaabSeasonStat.id)
      }
    }

    "throw an exception when trying to update non-existant NCAAB Season Stats" in {
      val now = DateHelper.now()

      val updateFuture = for {
        ncaabSeasonStat <- createNCAABSeasonStatForClient()
        createdNCAABSeasonStat <- ncaabSeasonStatService.upsert(
          NCAABSeasonStat(
            Some(ncaabSeasonStat.id.get + 1),
            now,
            now,
            ncaabSeasonStat.seasonId,
            10,
            3,
            16
          )
        )
      } yield createdNCAABSeasonStat

      recoverToSucceededIf[Exception](updateFuture)
    }

    "choose to create NCAAB Season Stats" in {
      val now = DateHelper.now()
      for {
        client <- r.client()
        season <- r.season(clientId = client.id.get)

        createdId <- ncaabSeasonStatService.upsert(
          ncaabSeasonStat = NCAABSeasonStat(None, now, now, season.id.get, 10, 16, 8)
        )
        createdNCAABSeasonStat <- ncaabSeasonStatService.getById(createdId)
      } yield {
        assert(createdNCAABSeasonStat.get.wins === 10)
        assert(createdNCAABSeasonStat.get.losses === 16)
        assert(createdNCAABSeasonStat.get.gamesTotal === 8)
        assert(createdNCAABSeasonStat.get.id.isDefined)
      }
    }
  }

}
