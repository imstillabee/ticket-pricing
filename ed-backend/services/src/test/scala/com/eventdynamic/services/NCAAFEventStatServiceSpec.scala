package com.eventdynamic.services

import com.eventdynamic.models.NCAAFEventStat
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest._

class NCAAFEventStatServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "NCAAFEventStatService" should {
    val r = new RandomUtil(ctx)
    val ncaafEventStatService = r.services.ncaafEventStat

    def createNCAAFEventStatForClient() = {
      for {
        createdClient <- r.client()
        createdEvent <- r.event(clientId = createdClient.id.get)
        ncaafEventStat: NCAAFEventStat <- ncaafEventStatService.create(
          createdEvent.id.get,
          "ATL",
          false,
          false
        )
      } yield ncaafEventStat
    }

    "create NCAAF Event Stats" in {
      for {
        ncaafEventStat <- createNCAAFEventStatForClient()
      } yield assert(ncaafEventStat.id.get === 1)
    }

    "get NCAAF Event Stats by Id" in {
      for {
        ncaafEventStat <- createNCAAFEventStatForClient()
        stats <- ncaafEventStatService.getById(ncaafEventStat.id.get)
      } yield assert(stats.get.id.get === ncaafEventStat.id.get)
    }

    "get NCAAF Event Stats by Client Id" in {
      for {
        createdClient <- r.client()
        createdEvent <- r.event(clientId = createdClient.id.get)
        ncaafEventStat <- ncaafEventStatService.create(createdEvent.id.get, "ATL", false, false)
        stats <- ncaafEventStatService.getByClientId(createdClient.id.get)
      } yield assert(stats(0).id.get === ncaafEventStat.id.get)
    }

    "get NCAAF Event Stats by Event Id" in {
      for {
        createdEvent <- r.event()
        ncaafEventStat <- ncaafEventStatService.create(createdEvent.id.get, "ATL", false, false)
        stats <- ncaafEventStatService.getByEventId(createdEvent.id.get)
      } yield {
        assert(stats.isDefined)
        assert(stats.get.id.get === ncaafEventStat.id.get)
      }
    }

    "get NCAAF Event Stats by Event Ids" in {
      for {
        event1 <- r.event()
        event2 <- r.event()
        event3 <- r.event()
        _ <- ncaafEventStatService.create(event1.id.get, "ATL", false, false)
        _ <- ncaafEventStatService.create(event2.id.get, "ATL", false, false)
        _ <- ncaafEventStatService.create(event3.id.get, "DAL", false, false)
        stats <- ncaafEventStatService.getByEventIds(Seq(event1.id.get, event2.id.get))
      } yield {
        assert(stats.length === 2)
        assert(stats.forall(_.opponent === "ATL"))
      }
    }

    "update NCAAF Event Stats" in {
      for {
        ncaafEventStat <- createNCAAFEventStatForClient()
        updatedId <- ncaafEventStatService.upsert(
          ncaafEventStat.copy(isHomeOpener = true, isPreSeason = true)
        )
        updatedNCAAFEventStat <- ncaafEventStatService.getById(updatedId)
      } yield {
        assert(updatedNCAAFEventStat.get.isHomeOpener)
        assert(updatedNCAAFEventStat.get.isPreSeason)
        assert(updatedNCAAFEventStat.get.id === ncaafEventStat.id)
      }
    }

    "throw an exception when trying to update non-existant NCAAF Event Stats" in {
      val now = DateHelper.now()

      val updateFuture = for {
        ncaafEventStat <- createNCAAFEventStatForClient()

        createdNCAAFEventStat <- ncaafEventStatService.upsert(
          NCAAFEventStat(
            Some(ncaafEventStat.id.get + 1),
            now,
            now,
            ncaafEventStat.id.get,
            "???",
            false,
            false
          )
        )
      } yield createdNCAAFEventStat

      recoverToSucceededIf[Exception](updateFuture)
    }

    "choose to create NCAAF Event Stats" in {
      val now = DateHelper.now()
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)

        createdId <- ncaafEventStatService.upsert(
          NCAAFEventStat(None, now, now, event.id.get, "???", false, false)
        )
        createdNCAAFEventStat <- ncaafEventStatService.getById(createdId)
      } yield {
        assert(createdNCAAFEventStat.get.id.isDefined)
        assert(createdNCAAFEventStat.get.opponent === "???")
        assert(createdNCAAFEventStat.get.isHomeOpener === false)
        assert(createdNCAAFEventStat.get.isPreSeason === false)
      }
    }
  }

}
