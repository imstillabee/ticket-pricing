package com.eventdynamic.services

import com.eventdynamic.models.NCAAFSeasonStat
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest._

class NCAAFSeasonStatServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "NCAAFSeasonStatService" should {
    val r = new RandomUtil(ctx)
    val ncaafSeasonStatService = r.services.ncaafSeasonStat

    def createNCAAFSeasonStatForClient() = {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        ncaafSeasonStat <- r.services.ncaafSeasonStat.create(createdSeason.id.get, 7, 8, 16)
      } yield ncaafSeasonStat
    }

    "create NCAAF Season Stats" in {
      for {
        ncaafSeasonStat <- createNCAAFSeasonStatForClient()
      } yield assert(ncaafSeasonStat.id.get === 1)
    }

    "get NCAAF Season Stats by Id" in {
      for {
        ncaafSeasonStat <- createNCAAFSeasonStatForClient()
        stats <- ncaafSeasonStatService.getById(ncaafSeasonStat.id.get)
      } yield assert(stats.get.id.get === ncaafSeasonStat.id.get)
    }

    "get NCAAF Season Stats by Client Id" in {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        ncaafSeasonStat <- ncaafSeasonStatService.create(createdSeason.id.get, 7, 8, 16)
        stats <- ncaafSeasonStatService.getByClientId(createdClient.id.get)
      } yield assert(stats(0).id.get === ncaafSeasonStat.id.get)
    }

    "get NCAAF Season Stats by Season Ids" in {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        ncaafSeasonStat <- ncaafSeasonStatService.create(createdSeason.id.get, 7, 8, 16)
        stats <- ncaafSeasonStatService.getBySeasonId(createdSeason.id.get)
      } yield {
        assert(stats.isDefined)
        assert(stats.get.id.get === ncaafSeasonStat.id.get)
      }
    }

    "get NCAAF Season Stats by Season Id" in {
      for {
        createdClient <- r.client()
        createdSeason1 <- r.season(clientId = createdClient.id.get)
        createdSeason2 <- r.season(clientId = createdClient.id.get)
        createdSeason3 <- r.season(clientId = createdClient.id.get)
        _ <- ncaafSeasonStatService.create(createdSeason1.id.get, 7, 8, 16)
        _ <- ncaafSeasonStatService.create(createdSeason2.id.get, 7, 8, 16)
        _ <- ncaafSeasonStatService.create(createdSeason3.id.get, 5, 4, 10)
        stats <- ncaafSeasonStatService.getBySeasonIds(
          Seq(createdSeason1.id.get, createdSeason2.id.get)
        )
      } yield {
        assert(stats.length === 2)
        assert(stats.forall(_.wins === 7))
      }
    }

    "update NCAAF Season Stats" in {
      for {
        ncaafSeasonStat <- createNCAAFSeasonStatForClient()
        updatedId <- ncaafSeasonStatService.upsert(ncaafSeasonStat.copy(wins = 10, losses = 3))
        updatedNCAAFSeasonStat <- ncaafSeasonStatService.getById(updatedId)
      } yield {
        assert(updatedNCAAFSeasonStat.get.wins === 10)
        assert(updatedNCAAFSeasonStat.get.losses === 3)
        assert(updatedNCAAFSeasonStat.get.id === ncaafSeasonStat.id)
      }
    }

    "throw an exception when trying to update non-existant NCAAF Season Stats" in {
      val now = DateHelper.now()

      val updateFuture = for {
        ncaafSeasonStat <- createNCAAFSeasonStatForClient()

        createdNCAAFSeasonStat <- ncaafSeasonStatService.upsert(
          NCAAFSeasonStat(
            Some(ncaafSeasonStat.id.get + 1),
            now,
            now,
            ncaafSeasonStat.seasonId,
            10,
            3,
            16
          )
        )
      } yield createdNCAAFSeasonStat

      recoverToSucceededIf[Exception](updateFuture)
    }

    "choose to create NCAAF Season Stats" in {
      val now = DateHelper.now()
      for {
        client <- r.client()
        season <- r.season(clientId = client.id.get)

        createdId <- ncaafSeasonStatService.upsert(
          ncaafSeasonStat = NCAAFSeasonStat(None, now, now, season.id.get, 10, 16, 8)
        )
        createdNCAAFSeasonStat <- ncaafSeasonStatService.getById(createdId)
      } yield {
        assert(createdNCAAFSeasonStat.get.wins === 10)
        assert(createdNCAAFSeasonStat.get.losses === 16)
        assert(createdNCAAFSeasonStat.get.gamesTotal === 8)
        assert(createdNCAAFSeasonStat.get.id.isDefined)
      }
    }
  }

}
