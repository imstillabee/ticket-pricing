package com.eventdynamic.services

import com.eventdynamic.models.NFLEventStat
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest._

class NFLEventStatServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "NFLEventStatService" should {
    val r = new RandomUtil(ctx)
    val nflEventStatService = r.services.nflEventStat

    def createNFLEventStatForClient() = {
      for {
        createdClient <- r.client()
        createdEvent <- r.event()
        nflEventStat: NFLEventStat <- r.services.nflEventStat.create(
          createdClient.id.get,
          createdEvent.id.get,
          "ATL",
          false,
          false
        )
      } yield nflEventStat
    }

    "create NFL Event Stats" in {
      for {
        nflEventStat <- createNFLEventStatForClient()
      } yield assert(nflEventStat.id.get === 1)
    }

    "get NFL Event Stats by Id" in {
      for {
        nflEventStat <- createNFLEventStatForClient()
        stats <- nflEventStatService.getById(nflEventStat.id.get)
      } yield assert(stats.get.id.get === nflEventStat.id.get)
    }

    "get NFL Event Stats by Client Id" in {
      for {
        createdClient <- r.client()
        createdEvent <- r.event()
        nflEventStat <- nflEventStatService.create(
          createdClient.id.get,
          createdEvent.id.get,
          "ATL",
          false,
          false
        )
        stats <- nflEventStatService.getByClientId(createdClient.id.get)
      } yield assert(stats(0).id.get === nflEventStat.id.get)
    }

    "get NFL Event Stats by Event Id" in {
      for {
        createdClient <- r.client()
        createdEvent <- r.event()
        nflEventStat <- nflEventStatService.create(
          createdClient.id.get,
          createdEvent.id.get,
          "ATL",
          false,
          false
        )
        stats <- nflEventStatService.getByEventId(createdEvent.id.get)
      } yield {
        assert(stats.isDefined)
        assert(stats.get.id.get === nflEventStat.id.get)
      }
    }

    "get NFL Event Stats by Event Ids" in {
      for {
        createdClient <- r.client()
        event1 <- r.event()
        event2 <- r.event()
        event3 <- r.event()
        _ <- nflEventStatService.create(createdClient.id.get, event1.id.get, "ATL", false, false)
        _ <- nflEventStatService.create(createdClient.id.get, event2.id.get, "ATL", false, false)
        _ <- nflEventStatService.create(createdClient.id.get, event3.id.get, "DAL", false, false)
        stats <- nflEventStatService.getByEventIds(Seq(event1.id.get, event2.id.get))
      } yield {
        assert(stats.length === 2)
        assert(stats.forall(_.opponent === "ATL"))
      }
    }

    "update NFL Event Stats" in {
      for {
        nflEventStat <- createNFLEventStatForClient()
        updatedNFLEventStat <- nflEventStatService.upsert(
          nflEventStat.copy(isHomeOpener = true, isPreSeason = true)
        )
      } yield {
        assert(updatedNFLEventStat.isHomeOpener)
        assert(updatedNFLEventStat.isPreSeason)
        assert(updatedNFLEventStat.id === nflEventStat.id)
      }
    }

    "throw an exception when trying to update non-existant NFL Event Stats" in {
      val now = DateHelper.now()

      val updateFuture = for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)

        createdNFLSeasonStat <- nflEventStatService.upsert(
          NFLEventStat(Some(99999), now, now, client.id.get, event.id.get, "???", false, false)
        )
      } yield createdNFLSeasonStat

      recoverToSucceededIf[Exception](updateFuture)
    }

    "choose to create NFL Event Stats" in {
      val now = DateHelper.now()
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)

        createdNFLEventStat <- nflEventStatService.upsert(
          NFLEventStat(None, now, now, client.id.get, event.id.get, "???", false, false)
        )
      } yield {
        assert(createdNFLEventStat.opponent === "???")
        assert(createdNFLEventStat.isHomeOpener === false)
        assert(createdNFLEventStat.isPreSeason === false)
        assert(createdNFLEventStat.id.isDefined)
      }
    }
  }

}
