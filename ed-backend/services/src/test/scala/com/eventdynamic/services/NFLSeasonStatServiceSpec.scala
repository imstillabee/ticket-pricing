package com.eventdynamic.services

import com.eventdynamic.models.NFLSeasonStat
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest._

class NFLSeasonStatServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "NFLSeasonStatService" should {
    val r = new RandomUtil(ctx)
    val nflSeasonStatService = r.services.nflSeasonStat

    def createNFLSeasonStatForClient() = {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        nflSeasonStat <- r.services.nflSeasonStat.create(
          createdClient.id.get,
          createdSeason.id.get,
          7,
          8,
          1,
          16
        )
      } yield nflSeasonStat
    }

    "create NFL Season Stats" in {
      for {
        nflSeasonStat <- createNFLSeasonStatForClient()
      } yield assert(nflSeasonStat.id.get === 1)
    }

    "get NFL Season Stats by Id" in {
      for {
        nflSeasonStat <- createNFLSeasonStatForClient()
        stats <- nflSeasonStatService.getById(nflSeasonStat.id.get)
      } yield assert(stats.get.id.get === nflSeasonStat.id.get)
    }

    "get NFL Season Stats by Client Id" in {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        nflSeasonStat <- nflSeasonStatService.create(
          createdClient.id.get,
          createdSeason.id.get,
          7,
          8,
          1,
          16
        )
        stats <- nflSeasonStatService.getByClientId(createdClient.id.get)
      } yield assert(stats(0).id.get === nflSeasonStat.id.get)
    }

    "get NFL Season Stats by Season Ids" in {
      for {
        createdClient <- r.client()
        createdSeason <- r.season(clientId = createdClient.id.get)
        nflSeasonStat <- nflSeasonStatService.create(
          createdClient.id.get,
          createdSeason.id.get,
          7,
          8,
          1,
          16
        )
        stats <- nflSeasonStatService.getBySeasonId(createdSeason.id.get)
      } yield {
        assert(stats.isDefined)
        assert(stats.get.id.get === nflSeasonStat.id.get)
      }
    }

    "get NFL Season Stats by Season Id" in {
      for {
        createdClient <- r.client()
        createdSeason1 <- r.season(clientId = createdClient.id.get)
        createdSeason2 <- r.season(clientId = createdClient.id.get)
        createdSeason3 <- r.season(clientId = createdClient.id.get)
        _ <- nflSeasonStatService.create(createdClient.id.get, createdSeason1.id.get, 7, 8, 1, 16)
        _ <- nflSeasonStatService.create(createdClient.id.get, createdSeason2.id.get, 7, 8, 1, 16)
        _ <- nflSeasonStatService.create(createdClient.id.get, createdSeason3.id.get, 5, 4, 1, 10)
        stats <- nflSeasonStatService.getBySeasonIds(
          Seq(createdSeason1.id.get, createdSeason2.id.get)
        )
      } yield {
        assert(stats.length === 2)
        assert(stats.forall(_.wins === 7))
      }
    }

    "update NFL Season Stats" in {
      for {
        nflSeasonStat <- createNFLSeasonStatForClient()
        updatedNFLSeasonStat <- nflSeasonStatService.createOrUpdate(
          nflSeasonStat.copy(wins = 10, losses = 3, ties = 1)
        )
      } yield {
        assert(updatedNFLSeasonStat.wins === 10)
        assert(updatedNFLSeasonStat.losses === 3)
        assert(updatedNFLSeasonStat.ties === 1)
        assert(updatedNFLSeasonStat.id === nflSeasonStat.id)
      }
    }

    "throw an exception when trying to update non-existant NFL Season Stats" in {
      val now = DateHelper.now()

      val updateFuture = for {
        client <- r.client()
        season <- r.season(clientId = client.id.get)

        createdNFLSeasonStat <- nflSeasonStatService.createOrUpdate(
          NFLSeasonStat(Some(99999), now, now, client.id.get, season.id.get, 10, 3, 1, 16)
        )
      } yield createdNFLSeasonStat

      recoverToSucceededIf[Exception](updateFuture)
    }

    "choose to create NFL Season Stats" in {
      val now = DateHelper.now()
      for {
        client <- r.client()
        season <- r.season(clientId = client.id.get)

        createdNFLSeasonStat <- nflSeasonStatService.createOrUpdate(
          NFLSeasonStat(None, now, now, client.id.get, season.id.get, 10, 3, 1, 16)
        )
      } yield {
        assert(createdNFLSeasonStat.wins === 10)
        assert(createdNFLSeasonStat.losses === 3)
        assert(createdNFLSeasonStat.ties === 1)
        assert(createdNFLSeasonStat.id.isDefined)
      }
    }
  }

}
