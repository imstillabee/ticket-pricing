package com.eventdynamic.services

import java.sql.Timestamp
import java.util.Calendar

import com.eventdynamic.models.TransactionType
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest.{AsyncWordSpec, Matchers}

class ObservationServiceSpec extends AsyncWordSpec with DatabaseSuite with Matchers {
  val r = new RandomUtil(ctx)
  val service = new ObservationService(ctx)

  "ObservationService#getAllInRange" should {
    val now = DateHelper.now()
    // Greater than a year out
    val far = {
      val c = Calendar.getInstance()
      c.setTime(now)
      c.add(Calendar.YEAR, 1)
      c.add(Calendar.HOUR, 24)
      new Timestamp(c.getTimeInMillis)
    }
    // In the past
    val past = {
      val c = Calendar.getInstance()
      c.setTime(now)
      c.add(Calendar.HOUR, -24)
      new Timestamp(c.getTimeInMillis)
    }
    // Within a year
    val soon = {
      val c = Calendar.getInstance()
      c.setTime(now)
      c.add(Calendar.HOUR, 24)
      new Timestamp(c.getTimeInMillis)
    }

    "exclude all observations for events where isBroadcast is false" in {
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get, isBroadcast = true, timestamp = Some(soon))
        disabledEvent <- r.event(
          clientId = client.id.get,
          isBroadcast = false,
          timestamp = Some(soon)
        )
        seats <- r.some(_ => r.seat(), 20)
        priceScale <- r.priceScaleId()
        eventSeats <- r.eventSeats(seats, Seq(event, disabledEvent), priceScale)
        observations <- service.getAllForEvents(
          Seq(event.id.get, disabledEvent.id.get),
          client.id.get
        )
      } yield {
        observations should have length 20
        all(observations.map(_.eventId)) shouldBe event.id.get
      }
    }

    "exclude all observations for event seats where isBroadcast (aka isListed) is false" in {
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get, isBroadcast = true, timestamp = Some(soon))
        seats <- r.some(_ => r.seat(), 20)
        priceScale <- r.priceScaleId()
        eventSeats <- r.eventSeats(seats.take(10), Seq(event), priceScale, isListed = true)
        disabledEventSeats <- r.eventSeats(
          seats.takeRight(10),
          Seq(event),
          priceScale,
          isListed = false
        )
        observations <- service.getAllForEvents(Seq(event.id.get), client.id.get)
      } yield {
        observations should have length 10
        all(observations.map(_.eventId)) shouldEqual event.id.get
        observations.map(_.seatId).toSet shouldEqual seats.splitAt(10)._1.map(_.id.get).toSet
      }
    }

    "filter by clientId" in {
      for {
        client <- r.client()
        otherClient <- r.client()
        event <- r.event(clientId = client.id.get, timestamp = Some(soon))
        otherEvent <- r.event(clientId = otherClient.id.get, timestamp = Some(soon))
        seats <- r.some(_ => r.seat(), 20)
        priceScale <- r.priceScaleId()
        _ <- r.eventSeats(seats, Seq(event, otherEvent), priceScale)
        observations <- service.getAllForEvents(Seq(event.id.get), client.id.get)
      } yield {
        observations should have length 20
        all(observations.map(_.eventId)) shouldBe event.id.get
      }
    }

    "get observations with a single transaction" in {
      for {
        event <- r.event(timestamp = Some(soon))
        seats <- r.some(_ => r.seat(), 20)
        priceScaleId <- r.priceScaleId()
        eventSeats <- r.eventSeats(seats, Seq(event), priceScaleId)
        transaction <- r.transaction(eventSeatId = eventSeats.head.id.get)
        observations <- service.getAllForEvents(Seq(event.id.get), event.clientId)
      } yield {
        observations.length shouldBe 20
        observations.filter(_.soldPrice.isEmpty) should have length 19
        observations.map(_.soldPrice) should contain(Some(transaction.price))
      }
    }

    "Use the 'purchase' transaction for observations with multiple transactions for the same timestamp" in {
      val timestamp = DateHelper.now()
      for {
        event <- r.event(timestamp = Some(soon))
        seats <- r.some(_ => r.seat(), 20)
        priceScaleId <- r.priceScaleId()
        eventSeats <- r.eventSeats(seats, Seq(event), priceScaleId)
        tPurchase <- r.transaction(
          eventSeatId = eventSeats.head.id.get,
          transactionType = TransactionType.Purchase,
          time = timestamp
        )
        tReturn <- r.transaction(
          eventSeatId = eventSeats.head.id.get,
          transactionType = TransactionType.Return,
          time = timestamp
        )
        observations <- service.getAllForEvents(Seq(event.id.get), event.clientId)
      } yield {
        val observation = observations.find(_.eventSeatId == eventSeats.head.id.get).get
        observation.soldPrice shouldBe Some(tPurchase.price)
        observation.soldPrice should not be Some(tReturn.price)
      }
    }
  }
}
