package com.eventdynamic.services

import com.eventdynamic.models.{Client, ClientPerformanceType, PriceGuard}
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.{DateHelper, SuccessResponse}
import org.scalatest.AsyncWordSpec

import scala.concurrent.Future

class PriceGuardServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "PriceGuardService" should {
    val r = new RandomUtil(ctx)

    val venueService = new VenueService(ctx)
    val clientService = new ClientService(ctx)
    val priceScaleService = new PriceScaleService(ctx)
    val eventService = new EventService(ctx)
    val eventCategoryService = new EventCategoryService(ctx)
    val priceGuardService = new PriceGuardService(ctx)

    def createEventCategory(clientId: Int): Future[Int] = {
      return eventCategoryService.create("Marquee", clientId)
    }

    def createEvent(eventCategoryId: Int, eventId: Int = 1, venueId: Int = 1): Future[Int] = {
      for {
        _ <- eventService.create(
          Option(eventId),
          "event",
          None,
          1,
          venueId,
          eventCategoryId,
          None,
          false,
          0,
          None,
          0,
          None,
          1
        )
      } yield {
        eventId
      }
    }

    def createVenue(): Future[Int] = {
      venueService.create("venue", 50000, "02134", Some("https://dialexa.com/venueMap"))
    }

    def createClient(): Future[Int] = {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          name = "SOS",
          pricingInterval = 1500,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
      } yield client.id.get
    }

    def createPriceScale(name: String, venueId: Int): Future[Int] = {
      priceScaleService.create(name, venueId, integrationId = 9000)
    }

    def createPriceGuardForSectionAndRow(
      eventId: Int,
      section: String,
      row: String,
      minimumPrice: Int
    ) = {
      for {
        priceGuardId <- priceGuardService.createForSectionAndRow(
          eventId,
          section,
          row,
          minimumPrice,
          None
        )
      } yield priceGuardId
    }

    "create a priceGuard (by section & row)" in {
      for {
        venueId <- createVenue()
        clientId <- createClient()
        eventCategoryId <- createEventCategory(clientId)
        eventId <- createEvent(eventCategoryId, venueId = venueId)

        priceGuardId <- priceGuardService.createForSectionAndRow(eventId, "1", "AAA", 125, None)
        priceGuard <- priceGuardService.getById(priceGuardId)
      } yield {
        assert(priceGuard.isDefined)
        assert(priceGuard.get.id.get == 1)
        assert(priceGuard.get.row == "AAA")
        assert(priceGuard.get.section == "1")
      }
    }

    "get priceGuard by id" in {
      for {
        venueId <- createVenue()
        clientId <- createClient()
        eventCategoryId <- createEventCategory(clientId)
        createdEventId <- createEvent(eventCategoryId, venueId = venueId)
        createdPriceGuardId <- createPriceGuardForSectionAndRow(createdEventId, "1", "AAA", 130)

        priceGuard <- priceGuardService.getById(createdPriceGuardId)
      } yield {
        priceGuard match {
          case Some(priceGuard) =>
            assert(priceGuard.id == Some(createdPriceGuardId))
            assert(priceGuard.minimumPrice == 130)
          case None => fail()
        }
      }
    }

    "get all priceGuards for an event" in {
      for {
        venueId <- createVenue()
        clientId <- createClient()

        eventCategoryId <- createEventCategory(clientId)
        createdEventId <- createEvent(eventCategoryId, 1, venueId = venueId)
        createdEvent2Id <- createEvent(eventCategoryId, 2, venueId = venueId)
        _ <- createPriceGuardForSectionAndRow(createdEventId, "1", "AA", 130)
        _ <- createPriceGuardForSectionAndRow(createdEventId, "1", "AB", 150)
        _ <- createPriceGuardForSectionAndRow(createdEvent2Id, "2", "AA", 150)
        _ <- createPriceGuardForSectionAndRow(createdEvent2Id, "2", "AB", 130)

        priceGuards <- priceGuardService.getAllForEvents(Set(createdEventId))
      } yield {
        priceGuards match {
          case Seq() => fail()
          case priceGuards =>
            priceGuards.foreach(priceGuard => assert(priceGuard.eventId != createdEvent2Id))
            assert(priceGuards.length == 2)
        }
      }
    }

    "get all priceGuards for an event, section, and row" in {
      for {
        venueId <- createVenue()
        clientId <- createClient()

        eventCategoryId <- createEventCategory(clientId)
        createdEventId <- createEvent(eventCategoryId, 1, venueId = venueId)
        createdEvent2Id <- createEvent(eventCategoryId, 2, venueId = venueId)

        _ <- createPriceGuardForSectionAndRow(createdEventId, "1", "AA", 130)
        _ <- createPriceGuardForSectionAndRow(createdEventId, "1", "AB", 150)
        _ <- createPriceGuardForSectionAndRow(createdEventId, "2", "AA", 150)
        _ <- createPriceGuardForSectionAndRow(createdEventId, "2", "AB", 130)

        _ <- createPriceGuardForSectionAndRow(createdEvent2Id, "1", "AA", 130)
        _ <- createPriceGuardForSectionAndRow(createdEvent2Id, "1", "AB", 150)
        _ <- createPriceGuardForSectionAndRow(createdEvent2Id, "2", "AA", 150)
        _ <- createPriceGuardForSectionAndRow(createdEvent2Id, "2", "AB", 130)

        priceGuards <- priceGuardService.getAllForEventsSectionsAndRows(
          eventIds = Seq(createdEventId, createdEvent2Id).toSet,
          sections = Seq("1").toSet,
          rows = Seq("AA").toSet
        )
      } yield {
        assert(priceGuards.size == 2)
        assert(priceGuards.filter(_.row != "AA").size == 0)
        assert(priceGuards.filter(_.section != "1").size == 0)
      }
    }

    "PriceGuardService#bulkUpsert" should {
      "return sequence of ids corresponding to the upserted price guard records" in {
        val row = "8"
        val section = "AAA"
        val now = DateHelper.now()

        for {
          venueId <- createVenue()
          clientId <- createClient()
          eventCategoryId <- createEventCategory(clientId)
          eventId <- createEvent(eventCategoryId, venueId = venueId)
          existingPriceGuard <- r.priceGuard(eventId = eventId, row = row, section = section)
          pg = PriceGuard(
            id = None,
            eventId = eventId,
            section = section,
            row = row,
            minimumPrice = 5,
            maximumPrice = None,
            createdAt = now,
            modifiedAt = now
          )
          res <- priceGuardService.bulkUpsert(Seq(pg.copy(), pg.copy(row = "9")))
        } yield {
          assert(res.size == 2)
          assert(res.contains(existingPriceGuard.id.get))
        }
      }
    }
  }
}
