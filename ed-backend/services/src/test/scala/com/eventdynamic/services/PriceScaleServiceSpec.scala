package com.eventdynamic.services

import com.eventdynamic.models.PriceScale
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest.AsyncWordSpec

class PriceScaleServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)

  "PriceScaleService" should {

    val venueService = new VenueService(ctx)

    def createVenue() = {
      venueService.create("venue", 50000, "02134", Some("https://dialexa.com/venueMap"))
    }

    val priceScaleService = new PriceScaleService(ctx)

    def createPriceScale(venueId: Int) = {
      for {
        createdPriceScaleId <- priceScaleService.create("Premium Suites", venueId, 15)
      } yield createdPriceScaleId
    }

    "create a priceScale" in {
      for {
        venueId <- createVenue()
        priceScaleId <- createPriceScale(venueId)
      } yield {
        assert(priceScaleId == 1)
      }
    }

    "get priceScale by id" in {
      for {
        venueId <- createVenue()
        createdId <- createPriceScale(venueId)
        createdPriceScale <- priceScaleService.getById(createdId)
      } yield {
        createdPriceScale match {
          case Some(priceScale) => {
            assert(priceScale.id == Some(createdId))
            assert(priceScale.name == "Premium Suites")
            assert(priceScale.integrationId == 15)
          }
          case None => fail()
        }
      }
    }

    "get priceScale by integration id" in {
      for {
        venueId <- createVenue()
        createdId <- createPriceScale(venueId)
        createdPriceScale <- priceScaleService.getByIntegrationId(15)
      } yield {
        createdPriceScale match {
          case Some(priceScale) => {
            assert(priceScale.id.contains(createdId))
            assert(priceScale.name == "Premium Suites")
            assert(priceScale.integrationId == 15)
          }
          case None => fail()
        }
      }
    }

    "get all priceScales for a venue" in {
      for {
        venueId <- createVenue()
        venue2Id <- createVenue()
        _ <- createPriceScale(venueId)
        _ <- createPriceScale(venueId)
        _ <- createPriceScale(venue2Id)
        venue1PriceScales <- priceScaleService.getAllForVenue(venueId)
      } yield {
        venue1PriceScales match {
          case Seq() => fail()
          case priceScales => {
            assert(priceScales.length == 2)
          }
        }
      }
    }

    "get all priceScales for a client" in {
      for {
        venue <- r.venue()
        otherVenue <- r.venue()
        priceScaleIds <- r.some(_ => r.priceScaleId(venueId = venue), 7)
        _ <- r.some(_ => r.priceScaleId(venueId = otherVenue))
        event <- r.event(venueId = venue)
        _ <- r.event(venueId = otherVenue)
        priceScales <- priceScaleService.getAllForClient(clientId = event.clientId)
      } yield {
        assert(priceScales.length == 7)
        assert(priceScaleIds.contains(priceScales.head.id.get))
      }
    }

    "bulk insert priceScales" in {
      val now = DateHelper.now()
      val ps1 = PriceScale(None, "AA Balcony", 1, 1, now, now)
      val ps2 = PriceScale(None, "AA Veranda", 1, 2, now, now)
      val ps3 = PriceScale(None, "Delta Dugout", 2, 3, now, now)
      val somePriceScales = Seq(ps1, ps2, ps3)
      for {
        _ <- createVenue()
        _ <- createVenue()
        createdPriceScales <- priceScaleService.bulkInsert(somePriceScales)
        venue1priceScales <- priceScaleService.getAllForVenue(1)
        venue2priceScales <- priceScaleService.getAllForVenue(2)
      } yield {
        assert(somePriceScales.length === 3)
        assert(createdPriceScales.get === 3)
        assert(venue1priceScales.length === 2)
        assert(venue2priceScales.length === 1)
      }
    }
  }
}
