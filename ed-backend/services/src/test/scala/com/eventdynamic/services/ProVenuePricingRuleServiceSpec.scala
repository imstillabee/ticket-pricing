package com.eventdynamic.services

import com.eventdynamic.models.RoundingType
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.{NotFoundResponse, SuccessResponse}
import org.scalatest.AsyncWordSpec
import org.scalatest.Matchers._

class ProVenuePricingRuleServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val proVenuePricingRuleService = new ProVenuePricingRuleService(ctx)

  "ProVenuePricingRuleService#create" should {
    "create a rule with no optional args" in {
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)
        priceScaleId <- r.priceScaleId()
        createResult <- proVenuePricingRuleService.create(
          client.id.get,
          Seq(event.id.get),
          Seq(priceScaleId),
          Seq[String]("1", "2", "3'")
        )
      } yield {
        createResult shouldBe SuccessResponse(1)
      }
    }

    "create a rule with all optional args" in {
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)
        priceScaleId <- r.priceScaleId()
        mirrorPriceScaleId <- r.priceScaleId()
        createResult <- proVenuePricingRuleService.create(
          client.id.get,
          Seq(event.id.get),
          Seq(priceScaleId),
          Seq[String]("1", "2", "3'"),
          isActive = true,
          Some(mirrorPriceScaleId),
          Some(-10),
          Some(10.20),
          Some("My rule"),
          RoundingType.Floor
        )
      } yield {
        createResult shouldBe SuccessResponse(1)
      }
    }

    "return the created rule id" in {
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)
        priceScaleId <- r.priceScaleId()
        SuccessResponse(aRuleId: Int) <- proVenuePricingRuleService.create(
          client.id.get,
          Seq(event.id.get),
          Seq(priceScaleId),
          Seq("1")
        )
        SuccessResponse(bRuleId: Int) <- proVenuePricingRuleService.create(
          client.id.get,
          Seq(event.id.get),
          Seq(priceScaleId),
          Seq("2")
        )
      } yield {
        assert(aRuleId != bRuleId)
      }
    }
  }

  "ProVenuePricingRuleService#getAll" should {
    "get all pricing rules for a client" in {
      for {
        client <- r.client()
        matchingRuleId <- r.proVenuePricingRule(clientId = client.id.get)
        _ <- r.proVenuePricingRule()
        response <- proVenuePricingRuleService.getAll(client.id.get)
      } yield {
        response.length shouldBe 1
        response.forall(_.proVenuePricingRule.clientId == client.id.get) shouldBe true
        response.forall(_.proVenuePricingRule.id.contains(matchingRuleId)) shouldBe true
      }
    }

    "get active pricing rules" in {
      for {
        client <- r.client()
        matchingRuleId <- r.proVenuePricingRule(clientId = client.id.get, isActive = true)
        _ <- r.proVenuePricingRule(clientId = client.id.get)
        response <- proVenuePricingRuleService.getAll(client.id.get, true)
      } yield {
        response.length shouldBe 1
        response.forall(_.proVenuePricingRule.clientId == client.id.get) shouldBe true
        response.forall(_.proVenuePricingRule.isActive) shouldBe true
        response.forall(_.proVenuePricingRule.id.contains(matchingRuleId)) shouldBe true
      }
    }
  }

  "ProVenuePricingServiceSpec#getConflicting" should {
    "return any rule that matches the event / price scale / buyer type combination" in {
      for {
        client <- r.client()
        aRuleId <- r.proVenuePricingRule(clientId = client.id.get, isActive = true)
        bRuleId <- r.proVenuePricingRule(clientId = client.id.get, isActive = true)
        Some(aRule) <- proVenuePricingRuleService.getOne(aRuleId, client.id.get)
        Some(bRule) <- proVenuePricingRuleService.getOne(bRuleId, client.id.get)
        result <- proVenuePricingRuleService.getConflicting(
          None,
          client.id.get,
          Seq(aRule.eventIds.head, bRule.eventIds.head),
          Seq(aRule.priceScaleIds.head, bRule.priceScaleIds.head),
          Seq(aRule.externalBuyerTypeIds.head, bRule.externalBuyerTypeIds.head)
        )
      } yield {
        result.length shouldBe 2
        assert(result.contains(aRuleId))
        assert(result.contains(bRuleId))
      }
    }

    "ignore rules that are not active" in {
      for {
        client <- r.client()
        aRuleId <- r.proVenuePricingRule(clientId = client.id.get, isActive = true)
        bRuleId <- r.proVenuePricingRule(clientId = client.id.get, isActive = false)
        Some(aRule) <- proVenuePricingRuleService.getOne(aRuleId, client.id.get)
        Some(bRule) <- proVenuePricingRuleService.getOne(bRuleId, client.id.get)
        result <- proVenuePricingRuleService.getConflicting(
          None,
          client.id.get,
          Seq(aRule.eventIds.head, bRule.eventIds.head),
          Seq(aRule.priceScaleIds.head, bRule.priceScaleIds.head),
          Seq(aRule.externalBuyerTypeIds.head, bRule.externalBuyerTypeIds.head)
        )
      } yield {
        result.length shouldBe 1
        assert(result.contains(aRuleId))
      }
    }

    "ignore rules that don't match all the criteria" in {
      for {
        client <- r.client()
        aRuleId <- r.proVenuePricingRule(clientId = client.id.get, isActive = true)
        bRuleId <- r.proVenuePricingRule(clientId = client.id.get, isActive = true)
        Some(aRule) <- proVenuePricingRuleService.getOne(aRuleId, client.id.get)
        Some(bRule) <- proVenuePricingRuleService.getOne(bRuleId, client.id.get)
        result <- proVenuePricingRuleService.getConflicting(
          None,
          client.id.get,
          Seq(aRule.eventIds.head),
          Seq(aRule.priceScaleIds.head, bRule.priceScaleIds.head),
          Seq(bRule.externalBuyerTypeIds.head)
        )
      } yield {
        result.length shouldBe 0
      }
    }

    "get active rules that match a list of external buyer type ids" in {
      for {
        client <- r.client()
        otherClient <- r.client()
        a <- r.proVenuePricingRule(
          clientId = client.id.get,
          isActive = true,
          externalBuyerTypeIds = Seq("1", "2")
        )
        b <- r.proVenuePricingRule(
          clientId = client.id.get,
          isActive = true,
          externalBuyerTypeIds = Seq("3", "4")
        )
        _ <- r.proVenuePricingRule(
          clientId = client.id.get,
          isActive = true,
          externalBuyerTypeIds = Seq("5", "6")
        )
        _ <- r.proVenuePricingRule(
          clientId = otherClient.id.get,
          isActive = false,
          externalBuyerTypeIds = Seq("1", "2")
        )
        _ <- r.proVenuePricingRule(
          clientId = otherClient.id.get,
          isActive = true,
          externalBuyerTypeIds = Seq("1", "2")
        )
        conflicts <- proVenuePricingRuleService.getConflicting(client.id.get, Seq("1", "3"))
      } yield {
        conflicts should have length 2
        conflicts should contain(a)
        conflicts should contain(b)
      }
    }
  }

  "ProVenuePricingService#getOne" should {
    "fetch a rule by its id" in {
      for {
        client <- r.client()
        _ <- r.some(_ => r.proVenuePricingRule())
        ruleId <- r.proVenuePricingRule(clientId = client.id.get)
        result <- proVenuePricingRuleService.getOne(ruleId, client.id.get)
      } yield {
        assert(result.isDefined)
        assert(result.get.proVenuePricingRule.id.get == ruleId)
        assert(result.get.proVenuePricingRule.clientId == client.id.get)
      }
    }

    "return none if no match for id" in {
      for {
        client <- r.client()
        _ <- r.some(_ => r.proVenuePricingRule())
        _ <- r.proVenuePricingRule(clientId = client.id.get)
        result <- proVenuePricingRuleService.getOne(123, client.id.get)
      } yield {
        assert(result.isEmpty)
      }
    }

    "return none if no match for clientId" in {
      for {
        client <- r.client()
        _ <- r.some(_ => r.proVenuePricingRule())
        ruleId <- r.proVenuePricingRule()
        result <- proVenuePricingRuleService.getOne(ruleId, client.id.get)
      } yield {
        assert(result.isEmpty)
      }
    }
  }

  "ProVenuePricingRuleService#delete" should {
    "return true if rule is deleted" in {
      for {
        client <- r.client()
        ruleId <- r.proVenuePricingRule(clientId = client.id.get)
        result <- proVenuePricingRuleService.delete(ruleId, client.id.get)
      } yield {
        assert(result)
      }
    }
    "return false if rule cannot be found" in {
      for {
        client <- r.client()
        result <- proVenuePricingRuleService.delete(1, client.id.get)
      } yield {
        assert(!result)
      }
    }
    "return false if user has incorrect clientId" in {
      for {
        goodClient <- r.client()
        badClient <- r.client()
        ruleId <- r.proVenuePricingRule(clientId = goodClient.id.get)
        result <- proVenuePricingRuleService.delete(ruleId, badClient.id.get)
      } yield {
        assert(!result)
      }
    }
  }

  "ProVenuePricingRuleService#update" should {
    val name = Some("updated name")
    val percent = Some(8)
    val constant: Option[BigDecimal] = Some(10.01)
    val rounding = RoundingType.Round
    val isActive = true
    val buyerTypeIds = Seq("buyerType")

    "update a rule by id" in {
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)
        priceScaleId <- r.priceScaleId()
        ruleId <- r.proVenuePricingRule(clientId = client.id.get)
        before <- proVenuePricingRuleService.getOne(ruleId, client.id.get)
        updateResult <- proVenuePricingRuleService.update(
          ruleId,
          client.id.get,
          Seq(event.id.get),
          Seq(priceScaleId),
          buyerTypeIds,
          isActive,
          Some(priceScaleId),
          percent,
          constant,
          name,
          rounding
        )
        after <- proVenuePricingRuleService.getOne(ruleId, client.id.get)
      } yield {
        assert(before != after)
        assert(after.isDefined)

        assert(after.get.proVenuePricingRule.name == name)
        assert(after.get.proVenuePricingRule.percent == percent)
        assert(after.get.proVenuePricingRule.constant == constant)
        assert(after.get.proVenuePricingRule.isActive == isActive)
        assert(after.get.proVenuePricingRule.mirrorPriceScaleId.contains(priceScaleId))
        assert(after.get.proVenuePricingRule.round == rounding)
        assert(after.get.eventIds == Seq(event.id.get))
        assert(after.get.priceScaleIds == Seq(priceScaleId))
        assert(after.get.externalBuyerTypeIds == buyerTypeIds)
      }
    }

    "update nothing if part of the update fails" in {
      val invalidEventId = 123
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)
        priceScaleId <- r.priceScaleId()
        ruleId <- r.proVenuePricingRule(clientId = client.id.get)
        before <- proVenuePricingRuleService.getOne(ruleId, client.id.get)
        updateResult <- proVenuePricingRuleService.update(
          ruleId,
          client.id.get,
          Seq(invalidEventId),
          Seq(priceScaleId),
          buyerTypeIds,
          isActive,
          Some(priceScaleId),
          percent,
          constant,
          name,
          rounding
        )
        after <- proVenuePricingRuleService.getOne(ruleId, client.id.get)
      } yield {
        assert(updateResult == NotFoundResponse)
        assert(before == after)
      }
    }

    "return NotFound if the rule does not exist" in {
      val invalidRuleId = 123
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)
        priceScaleId <- r.priceScaleId()
        updateResult <- proVenuePricingRuleService.update(
          invalidRuleId,
          client.id.get,
          Seq(event.id.get),
          Seq(priceScaleId),
          buyerTypeIds,
          isActive,
          Some(priceScaleId),
          percent,
          constant,
          name,
          rounding
        )
      } yield {
        assert(updateResult == NotFoundResponse)
      }
    }

    "return NotFound if the rule does not have that clientId" in {
      val invalidClientId = 123
      for {
        client <- r.client()
        event <- r.event(clientId = client.id.get)
        priceScaleId <- r.priceScaleId()
        ruleId <- r.proVenuePricingRule(clientId = client.id.get)
        before <- proVenuePricingRuleService.getOne(ruleId, client.id.get)
        updateResult <- proVenuePricingRuleService.update(
          ruleId,
          invalidClientId,
          Seq(event.id.get),
          Seq(priceScaleId),
          buyerTypeIds,
          isActive,
          Some(priceScaleId),
          percent,
          constant,
          name,
          rounding
        )
        after <- proVenuePricingRuleService.getOne(ruleId, client.id.get)
      } yield {
        assert(updateResult == NotFoundResponse)
        assert(before == after)
      }
    }
  }
}
