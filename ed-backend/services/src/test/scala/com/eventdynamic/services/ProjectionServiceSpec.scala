package com.eventdynamic.services

import java.time.Instant
import java.time.temporal.ChronoUnit

import com.eventdynamic.models.Projection
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.transactions.SeasonTransaction
import com.eventdynamic.utils._
import org.scalatest._

import scala.concurrent.Future

class ProjectionServiceSpec extends AsyncWordSpec with DatabaseSuite with ProjectionUtil {
  val r = new RandomUtil(ctx)
  val eventService = new EventService(ctx)
  val eventSeatService = new EventSeatService(ctx)
  val transactionService = new TransactionService(ctx)

  val seasonTransaction =
    new SeasonTransaction(ctx, eventService, eventSeatService, transactionService)
  val projectionsService = new ProjectionService(ctx)

  "ProjectionService#bulkInsert" should {
    "bulk insert projections" in {
      val now = Instant.now()
      for {
        event <- r.event()
        projections <- Future.successful {
          generateNProjections(
            3,
            false,
            Projection(
              eventId = event.id.get,
              timestamp = now,
              inventory = r.int(),
              inventoryLowerBound = r.int(),
              inventoryUpperBound = r.int(),
              revenue = r.int(),
              revenueLowerBound = r.int(),
              revenueUpperBound = r.int()
            )
          )
        }
        result <- r.services.projection.bulkInsert(projections)
      } yield {
        assert(result === 3)
      }
    }
  }

  "ProjectionService#deleteForEvents" should {
    "delete old projections" in {
      val now = Instant.now()
      for {
        event <- r.event()
        projections <- Future.successful {
          generateNProjections(
            3,
            false,
            Projection(
              eventId = event.id.get,
              timestamp = now,
              inventory = r.int(),
              inventoryLowerBound = r.int(),
              inventoryUpperBound = r.int(),
              revenue = r.int(),
              revenueLowerBound = r.int(),
              revenueUpperBound = r.int()
            )
          )
        }
        _ <- projectionsService.bulkInsert(projections)
        result <- projectionsService.deleteForEvents(projections)
      } yield {
        assert(result === 3)
      }
    }

    "not delete old projections" in {
      val now = Instant.now()
      for {
        event <- r.event()
        projections <- Future.successful {
          generateNProjections(
            3,
            false,
            Projection(
              eventId = event.id.get,
              timestamp = now,
              inventory = r.int(),
              inventoryLowerBound = r.int(),
              inventoryUpperBound = r.int(),
              revenue = r.int(),
              revenueLowerBound = r.int(),
              revenueUpperBound = r.int()
            )
          )
        }
        _ <- projectionsService.bulkInsert(projections)
        result <- projectionsService.deleteForEvents(
          projections.map(_.copy(eventId = event.id.get + 1))
        )
      } yield {
        assert(result === 0)
      }
    }
  }

  "ProjectionService#bulkCleanAndInsert" should {
    "remove old projections and upsert" in {
      val now = Instant.now()
      val pastTime = now.minus(1, ChronoUnit.HOURS)
      val futureTime = now.plus(1, ChronoUnit.HOURS)
      for {
        event <- r.event()
        oldProjections <- Future.successful {
          generateNProjections(
            3,
            false,
            Projection(
              eventId = event.id.get,
              timestamp = pastTime,
              inventory = r.int(),
              inventoryLowerBound = r.int(),
              inventoryUpperBound = r.int(),
              revenue = r.int(),
              revenueLowerBound = r.int(),
              revenueUpperBound = r.int()
            )
          )
        }
        futureProjections <- Future.successful {
          generateNProjections(
            3,
            true,
            Projection(
              eventId = event.id.get,
              timestamp = futureTime,
              inventory = r.int(),
              inventoryLowerBound = r.int(),
              inventoryUpperBound = r.int(),
              revenue = r.int(),
              revenueLowerBound = r.int(),
              revenueUpperBound = r.int()
            )
          )
        }
        existingProjections <- Future.successful { oldProjections ++ futureProjections }
        futureProjectionsUpdated <- Future {
          futureProjections.map(p => p.copy(inventory = p.inventory + 1))
        }
        _ <- projectionsService.bulkInsert(existingProjections)
        result <- projectionsService.bulkCleanAndInsert(futureProjectionsUpdated)
      } yield {
        assert(result === 3)
      }
    }

    "insert only" in {
      val now = Instant.now()
      val futureTime = now.plus(1, ChronoUnit.HOURS)
      for {
        event1 <- r.event()
        event2 <- r.event()
        futureProjections <- Future.successful {
          generateNProjections(
            3,
            true,
            Projection(
              eventId = event1.id.get,
              timestamp = futureTime,
              inventory = r.int(),
              inventoryLowerBound = r.int(),
              inventoryUpperBound = r.int(),
              revenue = r.int(),
              revenueLowerBound = r.int(),
              revenueUpperBound = r.int()
            )
          )
        }
        newProjections <- Future.successful {
          generateNProjections(
            3,
            true,
            Projection(
              eventId = event2.id.get,
              timestamp = futureTime,
              inventory = r.int(),
              inventoryLowerBound = r.int(),
              inventoryUpperBound = r.int(),
              revenue = r.int(),
              revenueLowerBound = r.int(),
              revenueUpperBound = r.int()
            )
          )
        }
        futureProjectionsUpdated <- Future {
          futureProjections.map(p => p.copy(inventory = p.inventory + 1))
        }
        projectionsToUpsert <- Future { newProjections ++ futureProjectionsUpdated }
        _ <- projectionsService.bulkInsert(futureProjections)
        result <- projectionsService.bulkCleanAndInsert(projectionsToUpsert)
      } yield {
        assert(result === 6)
      }
    }
  }

  "ProjectionService#getProjectedTimeStats" should {
    "Only fetch projections in the specified time range" in {
      val today = Instant.now().truncatedTo(ChronoUnit.DAYS)
      val nRecords = 10

      for {
        event <- r.event()

        data = (1 to nRecords).map(idx => {
          RandomModelUtil
            .projection(eventId = event.id.get, timestamp = today.plus(idx, ChronoUnit.DAYS))
        })

        _ <- projectionsService.bulkInsert(data)

        firstDay <- projectionsService.getProjectedTimeStats(
          today.plus(1, ChronoUnit.DAYS),
          today.plus(1, ChronoUnit.DAYS),
          eventId = event.id
        )
        lastDay <- projectionsService.getProjectedTimeStats(
          today.plus(nRecords, ChronoUnit.DAYS),
          today.plus(nRecords, ChronoUnit.DAYS),
          eventId = event.id
        )
        fourDays <- projectionsService.getProjectedTimeStats(
          today.plus(1, ChronoUnit.DAYS),
          today.plus(5, ChronoUnit.DAYS),
          eventId = event.id
        )
      } yield {
        assert(firstDay.size == 1)
        assert(lastDay.size == 1)
        assert(fourDays.size == 5)
      }
    }

    "Properly calculate periodic inventory from cumulative inventory" in {
      val today = Instant.now().truncatedTo(ChronoUnit.DAYS)
      val nRecords = 10

      val startingInventory = RandomModelUtil.int()
      val periodicInventories = (0 until nRecords).map(_ => RandomModelUtil.int())

      var cumulativeInventory = startingInventory

      for {
        event <- r.event()

        data = periodicInventories.zipWithIndex.map {
          case (periodicInventory, idx) =>
            cumulativeInventory += periodicInventory
            RandomModelUtil
              .projection(
                eventId = event.id.get,
                timestamp = today.plus(idx + 1, ChronoUnit.DAYS),
                inventory = cumulativeInventory
              )
        }

        _ <- projectionsService.bulkInsert(data)

        results <- projectionsService.getProjectedTimeStats(
          today,
          today.plus(nRecords, ChronoUnit.DAYS),
          eventId = event.id
        )
      } yield {
        assert(results.size == nRecords)
        assert(results.head.inventory.isEmpty)
        assert(
          periodicInventories.slice(1, nRecords) == results.slice(1, nRecords).map(_.inventory.get)
        )
      }
    }

    "Properly calculate periodic revenue from cumulative inventory" in {
      val today = Instant.now().truncatedTo(ChronoUnit.DAYS)
      val nRecords = 10

      val startingRevenue = BigDecimal(RandomModelUtil.double())
      val periodicInventories = (0 until nRecords).map(_ => BigDecimal(RandomModelUtil.double()))

      var cumulativeRevenue = startingRevenue

      for {
        event <- r.event()

        data = periodicInventories.zipWithIndex.map {
          case (periodicRevenue, idx) =>
            cumulativeRevenue += periodicRevenue
            RandomModelUtil
              .projection(
                eventId = event.id.get,
                timestamp = today.plus(idx + 1, ChronoUnit.DAYS),
                revenue = cumulativeRevenue
              )
        }

        _ <- projectionsService.bulkInsert(data)

        results <- projectionsService.getProjectedTimeStats(
          today,
          today.plus(nRecords, ChronoUnit.DAYS),
          eventId = event.id
        )
      } yield {
        assert(results.size == nRecords)
        assert(results.head.revenue.isEmpty)
        assert(
          periodicInventories.slice(1, nRecords) == results.slice(1, nRecords).map(_.revenue.get)
        )
      }
    }

    "Fetch projections for a single event" in {
      val today = Instant.now().truncatedTo(ChronoUnit.DAYS)
      val nRecords = 10

      for {
        event <- r.event()
        otherEvent <- r.event()

        data = (0 until nRecords).map(idx => {
          RandomModelUtil
            .projection(eventId = event.id.get, timestamp = today.plus(idx + 1, ChronoUnit.DAYS))
        })

        otherData = (0 until nRecords).map(idx => {
          RandomModelUtil
            .projection(
              eventId = otherEvent.id.get,
              timestamp = today.plus(idx + 1, ChronoUnit.DAYS)
            )
        })

        _ <- projectionsService.bulkInsert(data ++ otherData)

        results <- projectionsService.getProjectedTimeStats(
          today,
          today.plus(nRecords, ChronoUnit.DAYS),
          eventId = event.id
        )
      } yield {
        assert(results.length == nRecords)
        assert(results.map(_.cumulativeInventory.get) == data.map(_.inventory))
        assert(results.map(_.cumulativeRevenue.get) == data.map(_.revenue))
        assert(results.map(_.intervalEnd.toInstant) == data.map(_.timestamp))
      }
    }

    "Fetch projections for a single season by summing projections for all the season events" in {
      val today = Instant.now().truncatedTo(ChronoUnit.DAYS)
      val nRecords = 10

      for {
        season <- r.season()
        seasonEvent1 <- r.event(seasonId = season.id)
        seasonEvent2 <- r.event(seasonId = season.id)

        otherEvent <- r.event()

        seasonEvent1Data = (0 until nRecords).map(idx => {
          RandomModelUtil
            .projection(
              eventId = seasonEvent1.id.get,
              timestamp = today.plus(idx + 1, ChronoUnit.DAYS)
            )
        })
        seasonEvent2Data = (0 until nRecords).map(idx => {
          RandomModelUtil
            .projection(
              eventId = seasonEvent2.id.get,
              timestamp = today.plus(idx + 1, ChronoUnit.DAYS)
            )
        })

        otherEventData = (0 until nRecords).map(idx => {
          RandomModelUtil
            .projection(
              eventId = otherEvent.id.get,
              timestamp = today.plus(idx + 1, ChronoUnit.DAYS)
            )
        })

        _ <- projectionsService.bulkInsert(seasonEvent1Data ++ seasonEvent2Data ++ otherEventData)

        results <- projectionsService.getProjectedTimeStats(
          today,
          today.plus(nRecords, ChronoUnit.DAYS),
          seasonId = season.id
        )
      } yield {
        assert(results.length == nRecords)
        val zippedData = seasonEvent1Data.zip(seasonEvent2Data)
        assert(results.map(_.cumulativeInventory.get) == zippedData.map {
          case (d1, d2) => d1.inventory + d2.inventory
        })
        assert(results.map(_.cumulativeRevenue.get) == zippedData.map {
          case (d1, d2) => d1.revenue + d2.revenue
        })
        assert(results.map(_.intervalEnd.toInstant) == seasonEvent1Data.map(_.timestamp))
        assert(results.map(_.intervalEnd.toInstant) == seasonEvent2Data.map(_.timestamp))
      }
    }
  }
}
