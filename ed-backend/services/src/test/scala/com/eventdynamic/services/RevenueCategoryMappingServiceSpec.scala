package com.eventdynamic.services

import com.eventdynamic.models.{Client, ClientPerformanceType}
import com.eventdynamic.test.DatabaseSuite
import com.eventdynamic.transactions.SeasonTransaction
import com.eventdynamic.utils.SuccessResponse
import org.scalatest.AsyncWordSpec

import scala.concurrent.Future

class RevenueCategoryMappingServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val clientService = new ClientService(ctx)
  val eventService = new EventService(ctx)
  val eventSeatService = new EventSeatService(ctx)
  val transactionService = new TransactionService(ctx)

  var seasonTransaction =
    new SeasonTransaction(ctx, eventService, eventSeatService, transactionService)
  val seasonService = new SeasonService(ctx, seasonTransaction)
  val revenueCategoryService = new RevenueCategoryService(ctx)

  val revenueCategoryMappingService =
    new RevenueCategoryMappingService(ctx)

  def createRevenueCategoryMapping(displayName: String, regex: String, order: Int) = {
    for {
      SuccessResponse(client: Client) <- clientService.create(
        "Dialexa",
        15,
        2,
        None,
        1.125,
        None,
        ClientPerformanceType.MLB
      )
      revenueCategory <- revenueCategoryService.create(displayName)
      revenueCategoryMapping <- revenueCategoryMappingService.create(
        client.id.get,
        revenueCategory.id.get,
        regex,
        order
      )
    } yield revenueCategoryMapping
  }

  def createRevenueCategoryMappingForClient(
    client: Client,
    displayName: String,
    regex: String,
    order: Int
  ) = {
    for {
      revenueCategory <- revenueCategoryService.create(displayName)
      revenueCategoryMapping <- revenueCategoryMappingService.create(
        client.id.get,
        revenueCategory.id.get,
        regex,
        order
      )
    } yield revenueCategoryMapping
  }

  def checkBuyerTypeCodes(codes: Seq[String], clientId: Int) = {

    val futures = for (code <- codes) yield {
      revenueCategoryMappingService.regexMatch(clientId, code).map {
        case resp => resp
      }
    }

    Future.sequence(futures)
  }

  "RevenueCategoryMappingService" should {
    "create a new revenue category mapping for a client" in {
      val displayName = "GROUP"
      val regex = "^G.*"
      val order = 1

      for {
        revenueCategoryMapping <- createRevenueCategoryMapping(displayName, regex, order)
      } yield {
        assert(revenueCategoryMapping.order === order)
        assert(revenueCategoryMapping.regex === regex)
      }
    }

    "gets all revenue category mappings for a client in correct order" in {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          "Dialexa",
          15,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        mappingOne <- createRevenueCategoryMappingForClient(client, "Group Ticket", "^G.*", 1)
        mappingTwo <- createRevenueCategoryMappingForClient(
          client,
          "Package Ticket",
          "^[NPRE].*",
          2
        )
        mappingThree <- createRevenueCategoryMappingForClient(
          client,
          "Single Game Ticket",
          """[\\s\\S]*""",
          3
        )
        revenueCategoryMappings <- revenueCategoryMappingService.getByClientId(client.id.get)
      } yield {
        assert(revenueCategoryMappings.seq.length === 3)
        assert(revenueCategoryMappings.seq.head === mappingOne)
        assert(revenueCategoryMappings.seq.apply(1) === mappingTwo)
        assert(revenueCategoryMappings.seq.last === mappingThree)
      }
    }

    "map a buyer type code to an ED revenue category id" in {
      val btc_1 = "GRP"
      val btc_2 = "NPRK"
      val btc_3 = "ADULT"

      for {
        SuccessResponse(client: Client) <- clientService.create(
          "Dialexa",
          15,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        _ <- createRevenueCategoryMappingForClient(client, "Group Ticket", "^G.*", 1)
        _ <- createRevenueCategoryMappingForClient(client, "Package Ticket", "^[NPRE].*", 2)
        _ <- createRevenueCategoryMappingForClient(client, "Single Game Ticket", """[\\s\\S]*""", 3)
        r1 <- revenueCategoryMappingService.regexMatch(client.id.get, btc_1)
        r2 <- revenueCategoryMappingService.regexMatch(client.id.get, btc_2)
        r3 <- revenueCategoryMappingService.regexMatch(client.id.get, btc_3)
      } yield {
        assert(r1 === Some(1))
        assert(r2 === Some(2))
        assert(r3 === Some(3))
      }
    }

    "map a buyer type code to correct ED revenue category id" in {
      val buyerTypes =
        Seq("R", "ADULT", "N", "G10", "P20N", "P20R", "BMCD50", "P40R1", "P40ZR", "PAP6")

      for {
        SuccessResponse(client: Client) <- clientService.create(
          "Dialexa",
          15,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        r1 <- createRevenueCategoryMappingForClient(client, "Group Ticket", "^G.*", 1)
        r2 <- createRevenueCategoryMappingForClient(client, "Package 20 Ticket", "^[NPRE].*20.*", 2)
        r3 <- createRevenueCategoryMappingForClient(client, "Package 40 Ticket", "^[NPRE].*40.*", 3)
        r4 <- createRevenueCategoryMappingForClient(client, "Package Ticket", "^[NPRE].*", 4)
        r5 <- createRevenueCategoryMappingForClient(client, "Single Game Ticket", ".*", 5)
        responses <- Future.successful(
          Seq(
            Some(r4.revenueCategoryId),
            Some(r5.revenueCategoryId),
            Some(r4.revenueCategoryId),
            Some(r1.revenueCategoryId),
            Some(r2.revenueCategoryId),
            Some(r2.revenueCategoryId),
            Some(r5.revenueCategoryId),
            Some(r3.revenueCategoryId),
            Some(r3.revenueCategoryId),
            Some(r4.revenueCategoryId)
          )
        )
        check <- checkBuyerTypeCodes(buyerTypes, client.id.get)
      } yield assert(check === responses)
    }
  }
}
