package com.eventdynamic.services

import com.eventdynamic.models._
import com.eventdynamic.test.DatabaseSuite
import com.eventdynamic.utils.{ResponseCode, SuccessResponse}
import org.scalatest.AsyncWordSpec

import scala.concurrent.Future

class RevenueCategoryServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val revenueCategoryService = new RevenueCategoryService(ctx)

  def createRevenueCategory(name: String): Future[RevenueCategory] = {
    for {
      revenueCategory <- revenueCategoryService.create(name)
    } yield revenueCategory
  }

  "RevenueCategoryService" should {
    "return the created revenueCategory" in {
      for {
        revenueCategory <- createRevenueCategory("ADULT")
      } yield assert(revenueCategory.name === "ADULT")
    }

    "gets a revenueCategory by Id" in {
      for {
        createdCategory <- createRevenueCategory("ADULT")
        Some(revenueCategory) <- revenueCategoryService.getById(createdCategory.id.get)
      } yield {
        assert(revenueCategory.name === "ADULT")
        assert(revenueCategory.id.get === createdCategory.id.get)
      }
    }
  }
}
