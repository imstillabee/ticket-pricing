package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.models.{Client, ClientPerformanceType, Event, Season}
import com.eventdynamic.utils._
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.transactions.SeasonTransaction
import org.scalatest._

class SeasonServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val clientService = new ClientService(ctx)
  val eventService = new EventService(ctx)
  val eventSeatService = new EventSeatService(ctx)
  val transactionService = new TransactionService(ctx)

  var seasonTransaction =
    new SeasonTransaction(ctx, eventService, eventSeatService, transactionService)
  val seasonService = new SeasonService(ctx, seasonTransaction)

  "SeasonService" should {

    "create season without name" in {
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "test",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        SuccessResponse(createdSeason: Season) <- seasonService.create(None, createdClient.id.get)
      } yield {
        assert(createdSeason.id.get == 1)
      }
    }

    "create season with name" in {
      val name = "Season"
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "test",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        SuccessResponse(createdSeason: Season) <- seasonService.create(
          Some(name),
          createdClient.id.get
        )
      } yield {
        assert(createdSeason.id.get == 1 && createdSeason.name.get == name)
      }
    }

    "get season with id" in {
      val name = "Season"
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "test",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        SuccessResponse(createdSeason: Season) <- seasonService.create(
          Some(name),
          createdClient.id.get
        )
        fetchedSeason <- seasonService.getById(1, createdClient.id, false)
      } yield {
        assert(fetchedSeason.isDefined)
        assert(fetchedSeason.get == createdSeason)
      }
    }

    "get all seasons" in {
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "test",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        _ <- seasonService.create(None, createdClient.id.get)
        fetchedSeasons <- seasonService.getAll(Some(createdClient.id.get), false)
      } yield {
        assert(fetchedSeasons.length == 1)
      }
    }

    "get current season" in {
      val lastSeasonStart = DateHelper.earlier(60 * 24 * 545)
      val lastSeasonEnd = DateHelper.earlier(60 * 24 * 180)
      val thisSeasonStart = DateHelper.earlier(60 * 24 * 30)
      val thisSeasonEnd = DateHelper.later(60 * 24 * 180)
      val nextSeasonEnd = DateHelper.later(60 * 24 * 545)
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "test",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        // this season has no startDate, and cannot be the current season
        _ <- seasonService.create(
          Some("Next Season"),
          createdClient.id.get,
          None,
          Some(nextSeasonEnd)
        )
        // this season ended already, and cannot be the current season
        _ <- seasonService.create(
          Some("Last Season"),
          createdClient.id.get,
          Some(lastSeasonStart),
          Some(lastSeasonEnd)
        )
        _ <- seasonService.create(
          Some("This Season"),
          createdClient.id.get,
          Some(thisSeasonStart),
          Some(thisSeasonEnd)
        )
        currentSeason <- seasonService.getCurrent(Some(createdClient.id.get), false)
      } yield {
        assert(currentSeason.get.name.get == "This Season")
      }
    }

    "get or create a season will return season if it already exists" in {
      val name = "Season"
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "test",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        SuccessResponse(createdSeason: Season) <- seasonService.create(
          Some(name),
          createdClient.id.get
        )
        fetchedSeason <- seasonService.getOrCreate(Some(name), 1)
      } yield {
        assert(fetchedSeason.isDefined)
        assert(fetchedSeason.get == createdSeason.id.get)
      }
    }

    "get or create a season will return None if season name is empty" in {
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "test",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        fetchedSeason <- seasonService.getOrCreate(None, createdClient.id.get)
      } yield {
        assert(fetchedSeason === None)
      }
    }

    "get or create a season will create a season if it doesn't exist" in {
      val name = "Season"
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "test",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        fetchedSeason <- seasonService.getOrCreate(Some(name), createdClient.id.get)
        createdSeason <- seasonService.getById(fetchedSeason.get, createdClient.id, false)
      } yield {
        assert(fetchedSeason.isDefined)
        assert(createdSeason.get.name.get === name)
      }
    }

    "update name of season" in {
      val name = "Season"
      val newName = "Season1"
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "test",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        _ <- seasonService.create(Some(name), createdClient.id.get)
        season <- seasonService.getById(1, createdClient.id, false)
        ret <- seasonService.update(season.get.copy(name = Some(newName)))
        updatedSeason <- seasonService.getById(1, createdClient.id, false)
      } yield {
        assert(ret == SuccessResponse(updatedSeason.get))
        assert(
          updatedSeason.get.name.get == newName && updatedSeason.get.id.get == season.get.id.get
        )
      }
    }

    "not update name of undefined season" in {
      val newName = "Season1"
      for {
        ret <- seasonService.update(Season(Some(1), Some(newName), 1, None, None))
      } yield {
        assert(ret == NotFoundResponse)
      }
    }

    "delete a season by its id" in {
      for {
        SuccessResponse(createdClient: Client) <- clientService.create(
          "test",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        SuccessResponse(createdSeason: Season) <- seasonService.create(None, createdClient.id.get)
        ret <- seasonService.delete(createdSeason.id.get)
        allSeasons <- seasonService.getAll(Some(createdClient.id.get), false)
      } yield {
        assert(allSeasons.length === 0)
        assert(ret === true)
      }
    }

    "reset a season's date" in {
      for {
        client <- r.client()
        season <- r.season(clientId = client.id.get)

        e1 <- r.event(
          timestamp = Some(Timestamp.valueOf("2018-02-01 10:55:00")),
          clientId = client.id.get,
          seasonId = season.id
        )
        e2 <- r.event(
          timestamp = Some(Timestamp.valueOf("2018-02-02 10:55:00")),
          clientId = client.id.get,
          seasonId = season.id
        )
        e3 <- r.event(
          timestamp = Some(Timestamp.valueOf("2018-02-03 10:55:00")),
          clientId = client.id.get,
          seasonId = season.id
        )

        seat <- r.seat()

        es1 <- r.eventSeat(seatId = seat.id.get, eventId = e1.id.get)
        es2 <- r.eventSeat(seatId = seat.id.get, eventId = e2.id.get)
        es3 <- r.eventSeat(seatId = seat.id.get, eventId = e3.id.get)

        _ <- r.transaction(
          time = Timestamp.valueOf("2018-01-03 10:55:00"),
          eventSeatId = es1.id.get
        )
        _ <- r.transaction(
          time = Timestamp.valueOf("2018-01-02 10:55:00"),
          eventSeatId = es3.id.get
        )
        _ <- r.transaction(
          time = Timestamp.valueOf("2018-01-04 10:55:00"),
          eventSeatId = es3.id.get
        )

        _ <- seasonService.resetDates(season)
        seasonPostReset <- seasonService.getById(season.id.get, client.id, true)
      } yield {
        assert(seasonPostReset.get.startDate.get === Timestamp.valueOf("2018-01-02 10:55:00"))
        assert(seasonPostReset.get.endDate.get === Timestamp.valueOf("2018-02-03 10:55:00"))
      }
    }
  }
}
