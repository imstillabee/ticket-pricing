package com.eventdynamic.services

import com.eventdynamic.models.Seat
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.{DateHelper, SuccessResponse}
import org.scalatest.AsyncWordSpec

class SeatServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val seatService = new SeatService(ctx)

  "SeatService#create" should {
    "return the created seat" in {
      for {
        venue <- r.venue()
        seat <- seatService.create(1, "seat", "row", "section", venue)
      } yield {
        seat match {
          case SuccessResponse(seat: Seat) =>
            assert(seat.isInstanceOf[Seat])
            assert(seat.primarySeatId.contains(1))
            assert(seat.seat == "seat")
            assert(seat.row == "row")
            assert(seat.section == "section")
            assert(seat.venueId == venue)
          case _ => fail()
        }
      }
    }
  }

  "SeatService#getByIntegrationId" should {
    "not find a seat and return None" in {
      seatService
        .getByIntegrationId(1, 1)
        .map(res => {
          assert(res.isEmpty)
        })
    }

    "find a seat and return it" in {
      for {
        createdSeat <- r.seat()
        fetchedSeat <- seatService.getByIntegrationId(
          createdSeat.primarySeatId.get,
          createdSeat.venueId
        )
      } yield {
        assert(fetchedSeat.isDefined)
        assert(fetchedSeat.get.primarySeatId == createdSeat.primarySeatId)
      }
    }
  }

  "SeatService#get" should {
    "get all seats" in {
      val nSeats = 10
      for {
        _ <- r.some(_ => r.seat(), nSeats)
        seats <- seatService.get()
      } yield {
        assert(seats.length == nSeats)
      }
    }

    "get all seats matching 'seat'" in {
      val seat = "my seat"
      val nSeats = 2
      for {
        _ <- r.some(_ => r.seat(), nSeats)
        _ <- r.some(_ => r.seat(seat = seat), nSeats)
        seats <- seatService.get(seat = Some(seat))
      } yield {
        assert(seats.length == nSeats)
        assert(seats.forall(_.seat == seat))
      }
    }

    "get all seats matching 'row" in {
      val row = "my row"
      val nSeats = 2
      for {
        _ <- r.some(_ => r.seat(), nSeats)
        _ <- r.some(_ => r.seat(row = row), nSeats)
        seats <- seatService.get(row = Some(row))
      } yield {
        assert(seats.length == nSeats)
        assert(seats.forall(_.row == row))
      }
    }

    "get all seats matching 'section" in {
      val section = "my section"
      val nSeats = 2
      for {
        _ <- r.some(_ => r.seat(), nSeats)
        _ <- r.some(_ => r.seat(section = section), nSeats)
        seats <- seatService.get(section = Some(section))
      } yield {
        assert(seats.length == nSeats)
        assert(seats.forall(_.section == section))
      }
    }

    "get all seats matching venueId" in {
      val nSeats = 2
      for {
        venueId <- r.venue()
        _ <- r.some(_ => r.seat(), nSeats)
        _ <- r.some(_ => r.seat(venueId = venueId), nSeats)
        seats <- seatService.get(venueId = Some(venueId))
      } yield {
        assert(seats.length == nSeats)
        assert(seats.forall(_.venueId == venueId))
      }
    }
  }

  "SeatService#upsert" should {
    "update an existing seat" in {
      val seat = "12"
      val row = "8"
      val section = "AAA"

      for {
        venueId <- r.venue()
        original <- r.seat(seat = seat, row = row, section = section, venueId = venueId)
        _ <- r.some(_ => r.seat(venueId = venueId))
        beforeCount <- seatService.get().map(_.length)
        res <- seatService.upsert(
          Seat(
            id = None,
            primarySeatId = Some(456),
            createdAt = DateHelper.now(),
            modifiedAt = DateHelper.now(),
            seat = seat,
            row = row,
            section = section,
            venueId = venueId
          )
        )
        afterCount <- seatService.get().map(_.length)
        updated <- seatService.getById(res)
      } yield {
        assert(afterCount == beforeCount)

        assert(updated.isDefined)
        assert(updated.get.seat == seat)
        assert(updated.get.row == row)
        assert(updated.get.section == section)
        assert(updated.get.venueId == venueId)
        assert(updated.get.primarySeatId.contains(456))

        assert(original.id.get == res)
        assert(original.seat == seat)
        assert(original.row == row)
        assert(original.section == section)
        assert(original.venueId == venueId)
        assert(!original.primarySeatId.contains(456))
      }
    }

    "insert a new seat" in {
      val seat = "12"
      val row = "8"
      val section = "AAA"

      for {
        venueId <- r.venue()
        _ <- r.some(_ => r.seat(venueId = venueId))
        beforeCount <- seatService.get().map(_.length)
        res <- seatService.upsert(
          Seat(
            id = None,
            primarySeatId = Some(456),
            createdAt = DateHelper.now(),
            modifiedAt = DateHelper.now(),
            seat = seat,
            row = row,
            section = section,
            venueId = venueId
          )
        )
        afterCount <- seatService.get().map(_.length)
        updated <- seatService.getById(res)
      } yield {
        assert(afterCount - beforeCount == 1)

        assert(updated.isDefined)
        assert(updated.get.seat == seat)
        assert(updated.get.row == row)
        assert(updated.get.section == section)
        assert(updated.get.venueId == venueId)
        assert(updated.get.primarySeatId.contains(456))
      }
    }
  }

  "SeatService#upsert" should {
    "return sequence of ids corresponding to the upserted seat records" in {
      val seatNumber = 12
      val row = "8"
      val section = "AAA"

      val s = Seat(
        id = None,
        primarySeatId = Some(456),
        createdAt = DateHelper.now(),
        modifiedAt = DateHelper.now(),
        seat = seatNumber.toString,
        row = row,
        section = section,
        venueId = -1
      )

      for {
        venueId <- r.venue()
        existingSeat <- r.seat(
          venueId = venueId,
          seat = seatNumber.toString,
          row = row,
          section = section
        )
        res <- seatService.bulkUpsert(
          Seq(
            s.copy(venueId = venueId, seat = seatNumber.toString),
            s.copy(venueId = venueId, seat = (seatNumber + 1).toString, primarySeatId = Some(567))
          )
        )
      } yield {
        assert(res.size == 2)
        assert(res.contains(existingSeat.id.get))
      }
    }

    "return seatIds in the same order as how they are sent" in {
      val row = "8"
      val section = "AAA"

      val s1 = Seat(
        id = None,
        primarySeatId = Some(456),
        createdAt = DateHelper.now(),
        modifiedAt = DateHelper.now(),
        seat = "10",
        row = row,
        section = section,
        venueId = -1
      )

      val s2 = Seat(
        id = None,
        primarySeatId = Some(457),
        createdAt = DateHelper.now(),
        modifiedAt = DateHelper.now(),
        seat = "11",
        row = row,
        section = section,
        venueId = -1
      )

      val s3 = Seat(
        id = None,
        primarySeatId = Some(458),
        createdAt = DateHelper.now(),
        modifiedAt = DateHelper.now(),
        seat = "12",
        row = row,
        section = section,
        venueId = -1
      )

      for {
        venueId <- r.venue()
        existingSeat1 <- r.seat(venueId = venueId, seat = "10", row = row, section = section)
        existingSeat2 <- r.seat(venueId = venueId, seat = "11", row = row, section = section)
        existingSeat3 <- r.seat(venueId = venueId, seat = "12", row = row, section = section)
        res <- seatService.bulkUpsert(
          Seq(s1.copy(venueId = venueId), s2.copy(venueId = venueId), s3.copy(venueId = venueId))
        )
      } yield {
        assert(res.size == 3)
        val existingSeatIds = Seq(existingSeat1, existingSeat2, existingSeat3).map(_.id.get)
        assert(res === existingSeatIds)
      }
    }
  }
}
