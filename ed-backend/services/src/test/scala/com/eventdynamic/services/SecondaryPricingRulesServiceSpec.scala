package com.eventdynamic.services

import com.eventdynamic.models.{ClientIntegrationComposite, Inventory}
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import org.scalatest.{AsyncWordSpec, PrivateMethodTester}

import scala.math.BigDecimal.RoundingMode

class SecondaryPricingRulesServiceSpec
    extends AsyncWordSpec
    with DatabaseSuite
    with PrivateMethodTester {
  val r = new RandomUtil(ctx)
  val secondaryPricingRulesService = new SecondaryPricingRulesService(ctx)

  "SecondaryPricingRulesService" should {
    "return created pricing rule" in {
      for {
        ci <- r.clientIntegration()
        spr <- secondaryPricingRulesService.create(ci.id.get, Some(5), None)
      } yield {
        assert(spr.percent.isDefined)
        assert(spr.constant.isEmpty)
        assert(spr.percent.get === 5)
        assert(spr.clientIntegrationId === ci.id.get)
      }
    }

    "return secondary pricing rule given valid id" in {
      for {
        insertedSpr <- r.secondaryPricingRule()
        spr <- secondaryPricingRulesService.getById(insertedSpr.id.get)
      } yield {
        assert(spr.isDefined)
        assert(spr.get.id === insertedSpr.id)
      }
    }

    "return None given invalid id" in {
      for {
        _ <- r.secondaryPricingRule()
        spr <- secondaryPricingRulesService.getById(-1)
      } yield {
        assert(spr.isEmpty)
      }
    }

    "return secondary pricing rule given valid clientIntegrationId" in {
      for {
        insertedSpr <- r.secondaryPricingRule()
        spr <- secondaryPricingRulesService.getByClientIntegrationId(
          insertedSpr.clientIntegrationId
        )
      } yield {
        assert(spr.isDefined)
        assert(spr.get === insertedSpr)
      }
    }

    "return None given invalid clientIntegrationId" in {
      for {
        _ <- r.secondaryPricingRule()
        spr <- secondaryPricingRulesService.getByClientIntegrationId(-1)
      } yield {
        assert(spr.isEmpty)
      }
    }

    "return 1 when inserting new pricing rule" in {
      for {
        ci <- r.clientIntegration()
        res <- secondaryPricingRulesService.saveSettings(ci.id.get, Some(5), None)
        spr <- secondaryPricingRulesService.getByClientIntegrationId(ci.id.get)
      } yield {
        assert(res == 1)
        assert(spr.isDefined)
        assert(spr.get.constant.isEmpty)
        assert(spr.get.percent.get === 5)
      }
    }

    "return 1 when updating existing pricing rule" in {
      for {
        ci <- r.clientIntegration()
        _ <- secondaryPricingRulesService.saveSettings(ci.id.get, Some(5), None)
        res <- secondaryPricingRulesService.saveSettings(ci.id.get, None, Some(5))
        spr <- secondaryPricingRulesService.getByClientIntegrationId(ci.id.get)
      } yield {
        assert(res == 1)
        assert(spr.isDefined)
        assert(spr.get.percent.isEmpty)
        assert(spr.get.constant.get === 5)
      }
    }

    "return inventory with modified percentage values" in {
      val ciComposite = ClientIntegrationComposite(
        id = Some(1),
        createdAt = r.timestamp(),
        modifiedAt = r.timestamp(),
        clientId = 1,
        integrationId = 1,
        name = "ciComposite",
        version = None,
        json = None,
        isActive = true,
        isPrimary = false,
        logoUrl = None,
        percent = Some(5),
        constant = None
      )

      val originalPrice = BigDecimal(10.00)

      val modifiedPrice =
        secondaryPricingRulesService.applySecondaryPricingRule(ciComposite, originalPrice)
      val expectedPrice = originalPrice * BigDecimal(1.00 + ciComposite.percent.get / 100.00)
      assert(modifiedPrice === expectedPrice.setScale(0, RoundingMode.CEILING))
    }

    "return inventory with modified constant values" in {
      val ciComposite = ClientIntegrationComposite(
        id = Some(1),
        createdAt = r.timestamp(),
        modifiedAt = r.timestamp(),
        clientId = 1,
        integrationId = 1,
        name = "ciComposite",
        version = None,
        json = None,
        isActive = true,
        isPrimary = false,
        logoUrl = None,
        percent = None,
        constant = Some(BigDecimal(5.00))
      )

      val originalPrice = BigDecimal(10.00)

      val modifiedPrice =
        secondaryPricingRulesService.applySecondaryPricingRule(ciComposite, originalPrice)
      val expectedPrice = originalPrice + ciComposite.constant.get
      assert(modifiedPrice === expectedPrice.setScale(0, RoundingMode.CEILING))
    }

    "throws exception if there is a constant and percent value" in {
      val ciComposite = ClientIntegrationComposite(
        id = Some(1),
        createdAt = r.timestamp(),
        modifiedAt = r.timestamp(),
        clientId = 1,
        integrationId = 1,
        name = "ciComposite",
        version = None,
        json = None,
        isActive = true,
        isPrimary = false,
        logoUrl = None,
        percent = Some(5),
        constant = Some(5)
      )

      val originalPrice = BigDecimal(10.00)
      assertThrows[Exception] {
        secondaryPricingRulesService.applySecondaryPricingRule(ciComposite, originalPrice)
      }
    }

    "getRoundingMode" should {
      val getRoundingMode = PrivateMethod[RoundingMode.Value]('getRoundingMode)
      "return Half-Up if there is no pricing rule" in {
        val ciComposite = ClientIntegrationComposite(
          id = Some(1),
          createdAt = r.timestamp(),
          modifiedAt = r.timestamp(),
          clientId = 1,
          integrationId = 1,
          name = "ciComposite",
          version = None,
          json = None,
          isActive = true,
          isPrimary = false,
          logoUrl = None,
          percent = None,
          constant = None
        )
        val result = secondaryPricingRulesService invokePrivate getRoundingMode(ciComposite)
        assert(result === RoundingMode.CEILING)
      }

      "return Half-Up if there is a pricing rule" in {
        val ciComposite = ClientIntegrationComposite(
          id = Some(1),
          createdAt = r.timestamp(),
          modifiedAt = r.timestamp(),
          clientId = 1,
          integrationId = 1,
          name = "ciComposite",
          version = None,
          json = None,
          isActive = true,
          isPrimary = false,
          logoUrl = None,
          percent = Some(5),
          constant = None
        )
        val result = secondaryPricingRulesService invokePrivate getRoundingMode(ciComposite)
        assert(result === RoundingMode.CEILING)
      }

      "return Half-Down if there is a pricing rule that decreases list price" in {
        val ciComposite = ClientIntegrationComposite(
          id = Some(1),
          createdAt = r.timestamp(),
          modifiedAt = r.timestamp(),
          clientId = 1,
          integrationId = 1,
          name = "ciComposite",
          version = None,
          json = None,
          isActive = true,
          isPrimary = false,
          logoUrl = None,
          percent = Some(-5),
          constant = None
        )
        val result = secondaryPricingRulesService invokePrivate getRoundingMode(ciComposite)
        assert(result === RoundingMode.DOWN)
      }
    }
  }
}
