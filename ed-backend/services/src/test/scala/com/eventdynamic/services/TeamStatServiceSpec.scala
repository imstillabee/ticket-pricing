package com.eventdynamic.services

import com.eventdynamic.models.TeamStat
import com.eventdynamic.test.RandomUtil
import com.eventdynamic.test.DatabaseSuite
import com.eventdynamic.utils.SuccessResponse
import org.scalatest._

class TeamStatServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "TeamStatService" should {
    val r = new RandomUtil(ctx)
    val teamStatService = r.services.teamStat

    def createTeamStatForClient() = {
      for {
        createdClient <- r.client()
        createdSeason <- r.season()
        SuccessResponse(teamStat: TeamStat) <- r.services.teamStat.create(
          createdClient.id.get,
          createdSeason.id.get,
          20,
          10,
          45
        )
      } yield teamStat
    }

    "create Team Stats" in {
      for {
        teamStat <- createTeamStatForClient()
      } yield assert(teamStat.id.get === 1)
    }

    "get Team Stats by Id" in {
      for {
        teamStat <- createTeamStatForClient()
        stats <- teamStatService.getById(teamStat.id.get)
      } yield assert(stats.get.id.get === teamStat.id.get)
    }

    "get Team Stats by Client Id" in {
      for {
        createdClient <- r.client()
        createdSeason <- r.season()
        SuccessResponse(teamStat: TeamStat) <- teamStatService.create(
          createdClient.id.get,
          createdSeason.id.get,
          20,
          10,
          45
        )
        stats <- teamStatService.getByClientId(createdClient.id.get)
      } yield assert(stats(0).id.get === teamStat.id.get)
    }

    "update Team Stats" in {
      for {
        teamStat <- createTeamStatForClient()
        SuccessResponse(updatedTeamStat: TeamStat) <- teamStatService.update(
          teamStat.copy(wins = 10, losses = 10)
        )
      } yield {
        assert(updatedTeamStat.wins === 10)
        assert(updatedTeamStat.losses === 10)
        assert(updatedTeamStat.id === teamStat.id)
      }
    }
  }

}
