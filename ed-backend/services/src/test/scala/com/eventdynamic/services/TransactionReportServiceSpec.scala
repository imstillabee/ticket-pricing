package com.eventdynamic.services

import java.sql.Timestamp
import java.time.Instant

import akka.actor.{ActorRefFactory, ActorSystem}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import akka.util.ByteString
import com.eventdynamic.models.TransactionType
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import org.scalatest.AsyncWordSpec

class TransactionReportServiceSpec extends AsyncWordSpec with DatabaseSuite {
  implicit val actorSystem: ActorRefFactory = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val r = new RandomUtil(ctx)
  val transactionReportService = new TransactionReportService(ctx)

  private def assertHeaders(rowByteString: ByteString) = {
    val row = rowByteString.decodeString("UTF-8")
    assert(row.contains("Transaction Type"))
    assert(row.contains("Event Name"))
    assert(row.contains("Event Timestamp"))
    assert(row.contains("Transaction Timestamp"))
    assert(row.contains("Section"))
    assert(row.contains("Row"))
    assert(row.contains("Seats"))
    assert(row.contains("Average Unit Price"))
    assert(row.contains("Sales Total"))
    assert(row.contains("Market Place"))
    assert(row.contains("Seat Created At"))
    assert(row.contains("Seat Modified At"))
  }

  "TransactionReportService#getTransactionData" should {
    "get all transaction report rows by season ID" in {
      val instant = Instant.parse("2019-01-01T00:00:00.00Z")
      val timestamp = Timestamp.from(instant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)
        anotherSeason <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        event1 <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        event2 <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        anotherEvent <- r.event(
          venueId = venueId,
          seasonId = anotherSeason.id,
          clientId = client.id.get
        )

        seat <- r.seat(venueId = venueId)

        eventSeat1 <- r.eventSeat(seatId = seat.id.get, eventId = event1.id.get)
        eventSeat2 <- r.eventSeat(seatId = seat.id.get, eventId = event2.id.get)
        anotherEventSeat <- r.eventSeat(seatId = seat.id.get, eventId = anotherEvent.id.get)

        // expected rows
        _ <- r.transaction(eventSeatId = eventSeat1.id.get)
        _ <- r.transaction(eventSeatId = eventSeat2.id.get)
        // don't expect this row
        _ <- r.transaction(eventSeatId = anotherEventSeat.id.get)

        rows <- transactionReportService
          .getTransactionData(seasonId = season.id, clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length == 2)
      }
    }

    "get all transaction report rows by event ID" in {
      val instant = Instant.parse("2019-01-01T00:00:00.00Z")
      val timestamp = Timestamp.from(instant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)
        anotherSeason <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        event <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        anotherEvent <- r.event(
          venueId = venueId,
          seasonId = anotherSeason.id,
          clientId = client.id.get
        )

        seat <- r.seat(venueId = venueId)

        eventSeat <- r.eventSeat(seatId = seat.id.get, eventId = event.id.get)
        anotherEventSeat <- r.eventSeat(seatId = seat.id.get, eventId = anotherEvent.id.get)

        // expected rows
        _ <- r.transaction(eventSeatId = eventSeat.id.get)
        _ <- r.transaction(eventSeatId = eventSeat.id.get)
        // don't expect this row
        _ <- r.transaction(eventSeatId = anotherEventSeat.id.get)

        rows <- transactionReportService
          .getTransactionData(eventId = event.id, clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length == 2)
      }
    }

    "get all transaction report rows by start timestamp" in {
      val pastInstant = Instant.parse("2017-01-01T00:00:00.00Z")
      val insertedInstant = Instant.parse("2019-01-01T00:00:00.00Z")
      val filterInstant = Instant.parse("2018-01-01T00:00:00.00Z")
      val pastTimestamp = Timestamp.from(pastInstant)
      val insertedTimestamp = Timestamp.from(insertedInstant)
      val filterTimestamp = Timestamp.from(filterInstant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        pastEvent <- r.event(
          timestamp = Some(pastTimestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        event <- r.event(
          timestamp = Some(insertedTimestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )

        seat <- r.seat(venueId = venueId)

        eventSeat <- r.eventSeat(seatId = seat.id.get, eventId = event.id.get)
        anotherEventSeat <- r.eventSeat(seatId = seat.id.get, eventId = pastEvent.id.get)

        // expected rows
        _ <- r.transaction(time = insertedTimestamp, eventSeatId = eventSeat.id.get)
        _ <- r.transaction(time = insertedTimestamp, eventSeatId = eventSeat.id.get)
        // don't expect this row
        _ <- r.transaction(time = pastTimestamp, eventSeatId = anotherEventSeat.id.get)
        rows <- transactionReportService
          .getTransactionData(start = Some(filterTimestamp), clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length == 2)
        // verifying all transactions are of events AFTER start timestamp
        val timestamps = rows.collect { case x if x.transactionTime.after(filterTimestamp) => x }
        assert(timestamps.length == 2)
      }
    }

    "get all transaction report rows by end timestamp" in {
      val pastInstant = Instant.parse("2017-01-01T00:00:00.00Z")
      val insertedInstant = Instant.parse("2019-01-01T00:00:00.00Z")
      val filterInstant = Instant.parse("2018-01-01T00:00:00.00Z")
      val pastTimestamp = Timestamp.from(pastInstant)
      val insertedTimestamp = Timestamp.from(insertedInstant)
      val filterTimestamp = Timestamp.from(filterInstant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        pastEvent <- r.event(
          timestamp = Some(pastTimestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        event <- r.event(
          timestamp = Some(insertedTimestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )

        seat <- r.seat(venueId = venueId)

        eventSeat <- r.eventSeat(seatId = seat.id.get, eventId = pastEvent.id.get)
        anotherEventSeat <- r.eventSeat(seatId = seat.id.get, eventId = event.id.get)

        // expected rows
        _ <- r.transaction(time = pastTimestamp, eventSeatId = eventSeat.id.get)
        _ <- r.transaction(time = pastTimestamp, eventSeatId = eventSeat.id.get)
        // don't expect this row
        _ <- r.transaction(time = insertedTimestamp, eventSeatId = anotherEventSeat.id.get)
        rows <- transactionReportService
          .getTransactionData(end = Some(filterTimestamp), clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length == 2)
        // verifying all transactions are of events BEFORE end timestamp
        val timestamps = rows.collect { case x if x.transactionTime.before(filterTimestamp) => x }
        assert(timestamps.length == 2)
      }
    }

    "should handle multiple transactions with the same primary transactionId" in {
      val instant = Instant.parse("2019-01-01T00:00:00.00Z")
      val timestamp = Timestamp.from(instant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        event1 <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        event2 <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )

        seat1 <- r.seat(section = "1", row = "1", seat = "1", venueId = venueId)
        seat2 <- r.seat(section = "1", row = "1", seat = "2", venueId = venueId)
        seat3 <- r.seat(section = "1", row = "1", seat = "3", venueId = venueId)

        eventSeat1 <- r.eventSeat(seatId = seat1.id.get, eventId = event1.id.get)
        eventSeat2 <- r.eventSeat(seatId = seat2.id.get, eventId = event1.id.get)
        eventSeat3 <- r.eventSeat(seatId = seat3.id.get, eventId = event1.id.get)

        eventSeat4 <- r.eventSeat(seatId = seat1.id.get, eventId = event2.id.get)
        eventSeat5 <- r.eventSeat(seatId = seat2.id.get, eventId = event2.id.get)
        eventSeat6 <- r.eventSeat(seatId = seat3.id.get, eventId = event2.id.get)

        // should be one row
        _ <- r.transaction(eventSeatId = eventSeat1.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat2.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat3.id.get, primaryTransactionId = Some("1"))

        // should be another row
        _ <- r.transaction(eventSeatId = eventSeat4.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat5.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat6.id.get, primaryTransactionId = Some("1"))

        rows <- transactionReportService
          .getTransactionData(seasonId = season.id, clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length === 2)
      }
    }

    "should handle multiple transactions with different section/row combos" in {
      val instant = Instant.parse("2019-01-01T00:00:00.00Z")
      val timestamp = Timestamp.from(instant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        event <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )

        seat1 <- r.seat(section = "1", row = "1", seat = "1", venueId = venueId)
        seat2 <- r.seat(section = "1", row = "1", seat = "2", venueId = venueId)
        seat3 <- r.seat(section = "1", row = "1", seat = "3", venueId = venueId)

        seat4 <- r.seat(section = "1", row = "2", seat = "1", venueId = venueId)
        seat5 <- r.seat(section = "1", row = "2", seat = "2", venueId = venueId)
        seat6 <- r.seat(section = "1", row = "2", seat = "3", venueId = venueId)

        eventSeat1 <- r.eventSeat(seatId = seat1.id.get, eventId = event.id.get)
        eventSeat2 <- r.eventSeat(seatId = seat2.id.get, eventId = event.id.get)
        eventSeat3 <- r.eventSeat(seatId = seat3.id.get, eventId = event.id.get)
        eventSeat4 <- r.eventSeat(seatId = seat4.id.get, eventId = event.id.get)
        eventSeat5 <- r.eventSeat(seatId = seat5.id.get, eventId = event.id.get)
        eventSeat6 <- r.eventSeat(seatId = seat6.id.get, eventId = event.id.get)

        // should be one row
        _ <- r.transaction(eventSeatId = eventSeat1.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat2.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat3.id.get, primaryTransactionId = Some("1"))

        // should be another row
        _ <- r.transaction(eventSeatId = eventSeat4.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat5.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat6.id.get, primaryTransactionId = Some("1"))

        // should be a third row
        _ <- r.transaction(
          eventSeatId = eventSeat1.id.get,
          primaryTransactionId = Some("1"),
          transactionType = TransactionType.Return
        )
        _ <- r.transaction(
          eventSeatId = eventSeat2.id.get,
          primaryTransactionId = Some("1"),
          transactionType = TransactionType.Return
        )
        _ <- r.transaction(
          eventSeatId = eventSeat3.id.get,
          primaryTransactionId = Some("1"),
          transactionType = TransactionType.Return
        )

        rows <- transactionReportService
          .getTransactionData(seasonId = season.id, clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length === 3)
      }
    }
  }

  "TransactionReportService#generateReport" should {
    "get all transaction report rows by season ID" in {
      val instant = Instant.parse("2019-01-01T00:00:00.00Z")
      val timestamp = Timestamp.from(instant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)
        anotherSeason <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        event1 <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        event2 <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        anotherEvent <- r.event(
          venueId = venueId,
          seasonId = anotherSeason.id,
          clientId = client.id.get
        )

        seat <- r.seat(venueId = venueId)

        eventSeat1 <- r.eventSeat(seatId = seat.id.get, eventId = event1.id.get)
        eventSeat2 <- r.eventSeat(seatId = seat.id.get, eventId = event2.id.get)
        anotherEventSeat <- r.eventSeat(seatId = seat.id.get, eventId = anotherEvent.id.get)

        // expected rows
        _ <- r.transaction(eventSeatId = eventSeat1.id.get)
        _ <- r.transaction(eventSeatId = eventSeat2.id.get)
        // don't expect this row
        _ <- r.transaction(eventSeatId = anotherEventSeat.id.get)

        rows <- transactionReportService
          .generateReport(seasonId = season.id, clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length == 3)
        assertHeaders(rows.head)
      }
    }

    "get all transaction report rows by event ID" in {
      val instant = Instant.parse("2019-01-01T00:00:00.00Z")
      val timestamp = Timestamp.from(instant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)
        anotherSeason <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        event <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        anotherEvent <- r.event(
          venueId = venueId,
          seasonId = anotherSeason.id,
          clientId = client.id.get
        )

        seat <- r.seat(venueId = venueId)

        eventSeat <- r.eventSeat(seatId = seat.id.get, eventId = event.id.get)
        anotherEventSeat <- r.eventSeat(seatId = seat.id.get, eventId = anotherEvent.id.get)

        // expected rows
        _ <- r.transaction(eventSeatId = eventSeat.id.get)
        _ <- r.transaction(eventSeatId = eventSeat.id.get)
        // don't expect this row
        _ <- r.transaction(eventSeatId = anotherEventSeat.id.get)

        rows <- transactionReportService
          .generateReport(eventId = event.id, clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length == 3)
        assertHeaders(rows.head)
      }
    }

    "get all transaction report rows by start timestamp" in {
      val pastInstant = Instant.parse("2017-01-01T00:00:00.00Z")
      val insertedInstant = Instant.parse("2019-01-01T00:00:00.00Z")
      val filterInstant = Instant.parse("2018-01-01T00:00:00.00Z")
      val pastTimestamp = Timestamp.from(pastInstant)
      val insertedTimestamp = Timestamp.from(insertedInstant)
      val filterTimestamp = Timestamp.from(filterInstant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        pastEvent <- r.event(
          timestamp = Some(pastTimestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        event <- r.event(
          timestamp = Some(insertedTimestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )

        seat <- r.seat(venueId = venueId)

        eventSeat <- r.eventSeat(seatId = seat.id.get, eventId = event.id.get)
        anotherEventSeat <- r.eventSeat(seatId = seat.id.get, eventId = pastEvent.id.get)

        // expected rows
        _ <- r.transaction(time = insertedTimestamp, eventSeatId = eventSeat.id.get)
        _ <- r.transaction(time = insertedTimestamp, eventSeatId = eventSeat.id.get)
        // don't expect this row
        _ <- r.transaction(time = pastTimestamp, eventSeatId = anotherEventSeat.id.get)
        rows <- transactionReportService
          .generateReport(start = Some(filterTimestamp), clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length == 3)
        assertHeaders(rows.head)
      }
    }

    "get all transaction report rows by end timestamp" in {
      val pastInstant = Instant.parse("2017-01-01T00:00:00.00Z")
      val insertedInstant = Instant.parse("2019-01-01T00:00:00.00Z")
      val filterInstant = Instant.parse("2018-01-01T00:00:00.00Z")
      val pastTimestamp = Timestamp.from(pastInstant)
      val insertedTimestamp = Timestamp.from(insertedInstant)
      val filterTimestamp = Timestamp.from(filterInstant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        pastEvent <- r.event(
          timestamp = Some(pastTimestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        event <- r.event(
          timestamp = Some(insertedTimestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )

        seat <- r.seat(venueId = venueId)

        eventSeat <- r.eventSeat(seatId = seat.id.get, eventId = pastEvent.id.get)
        anotherEventSeat <- r.eventSeat(seatId = seat.id.get, eventId = event.id.get)

        // expected rows
        _ <- r.transaction(time = pastTimestamp, eventSeatId = eventSeat.id.get)
        _ <- r.transaction(time = pastTimestamp, eventSeatId = eventSeat.id.get)
        // don't expect this row
        _ <- r.transaction(time = insertedTimestamp, eventSeatId = anotherEventSeat.id.get)
        rows <- transactionReportService
          .generateReport(end = Some(filterTimestamp), clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length == 3)
        assertHeaders(rows.head)
      }
    }

    "should handle multiple transactions with the same primary transactionId" in {
      val instant = Instant.parse("2019-01-01T00:00:00.00Z")
      val timestamp = Timestamp.from(instant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        event1 <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )
        event2 <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )

        seat1 <- r.seat(section = "1", row = "1", seat = "1", venueId = venueId)
        seat2 <- r.seat(section = "1", row = "1", seat = "2", venueId = venueId)
        seat3 <- r.seat(section = "1", row = "1", seat = "3", venueId = venueId)

        eventSeat1 <- r.eventSeat(seatId = seat1.id.get, eventId = event1.id.get)
        eventSeat2 <- r.eventSeat(seatId = seat2.id.get, eventId = event1.id.get)
        eventSeat3 <- r.eventSeat(seatId = seat3.id.get, eventId = event1.id.get)

        eventSeat4 <- r.eventSeat(seatId = seat1.id.get, eventId = event2.id.get)
        eventSeat5 <- r.eventSeat(seatId = seat2.id.get, eventId = event2.id.get)
        eventSeat6 <- r.eventSeat(seatId = seat3.id.get, eventId = event2.id.get)

        // should be one row
        _ <- r.transaction(eventSeatId = eventSeat1.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat2.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat3.id.get, primaryTransactionId = Some("1"))

        // should be another row
        _ <- r.transaction(eventSeatId = eventSeat4.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat5.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat6.id.get, primaryTransactionId = Some("1"))

        rows <- transactionReportService
          .generateReport(seasonId = season.id, clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length === 3)
        assertHeaders(rows.head)
      }
    }

    "should handle multiple transactions with different section/row combos" in {
      val instant = Instant.parse("2019-01-01T00:00:00.00Z")
      val timestamp = Timestamp.from(instant)
      for {
        client <- r.client()

        season <- r.season(clientId = client.id.get)

        venueId <- r.venue()

        event <- r.event(
          timestamp = Some(timestamp),
          venueId = venueId,
          seasonId = season.id,
          clientId = client.id.get
        )

        seat1 <- r.seat(section = "1", row = "1", seat = "1", venueId = venueId)
        seat2 <- r.seat(section = "1", row = "1", seat = "2", venueId = venueId)
        seat3 <- r.seat(section = "1", row = "1", seat = "3", venueId = venueId)

        seat4 <- r.seat(section = "1", row = "2", seat = "1", venueId = venueId)
        seat5 <- r.seat(section = "1", row = "2", seat = "2", venueId = venueId)
        seat6 <- r.seat(section = "1", row = "2", seat = "3", venueId = venueId)

        eventSeat1 <- r.eventSeat(seatId = seat1.id.get, eventId = event.id.get)
        eventSeat2 <- r.eventSeat(seatId = seat2.id.get, eventId = event.id.get)
        eventSeat3 <- r.eventSeat(seatId = seat3.id.get, eventId = event.id.get)
        eventSeat4 <- r.eventSeat(seatId = seat4.id.get, eventId = event.id.get)
        eventSeat5 <- r.eventSeat(seatId = seat5.id.get, eventId = event.id.get)
        eventSeat6 <- r.eventSeat(seatId = seat6.id.get, eventId = event.id.get)

        // should be one row
        _ <- r.transaction(eventSeatId = eventSeat1.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat2.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat3.id.get, primaryTransactionId = Some("1"))

        // should be another row
        _ <- r.transaction(eventSeatId = eventSeat4.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat5.id.get, primaryTransactionId = Some("1"))
        _ <- r.transaction(eventSeatId = eventSeat6.id.get, primaryTransactionId = Some("1"))

        rows <- transactionReportService
          .generateReport(seasonId = season.id, clientId = client.id.get)
          .runWith(Sink.seq)
      } yield {
        assert(rows.length === 3)
        assertHeaders(rows.head)
      }
    }
  }
}
