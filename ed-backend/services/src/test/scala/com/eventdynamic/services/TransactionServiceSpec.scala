package com.eventdynamic.services

import java.sql.Timestamp

import com.eventdynamic.models.{Transaction, TransactionType}
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest.AsyncWordSpec

import scala.concurrent.Future

class TransactionServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val transactionService = new TransactionService(ctx)
  val integrationService = new IntegrationService(ctx)

  "TransactionService#create" should {
    "create a transaction from parameters" in {
      val price = 10.0
      val integrationId = 1
      val buyerTypeCode = "asdf"
      val timestamp = DateHelper.now()

      for {
        eventSeat <- r.eventSeat()
        transaction <- transactionService.create(
          timestamp,
          price,
          eventSeat.id.get,
          integrationId,
          buyerTypeCode,
          TransactionType.Purchase,
          None,
          Some("1")
        )
      } yield {
        assert(transaction.id.isDefined)
        assert(transaction.timestamp == timestamp)
        assert(transaction.price == price)
        assert(transaction.integrationId == integrationId)
        assert(transaction.buyerTypeCode == buyerTypeCode)
      }
    }

    "fail if neither primaryTransactionId or secondaryTransactionId is specified" in {
      val price = 10.0
      val integrationId = 1
      val buyerTypeCode = "asdf"
      val timestamp = DateHelper.now()
      val transactionType = TransactionType.Purchase

      (for {
        eventSeat <- r.eventSeat()
        _ <- transactionService.create(
          timestamp,
          price,
          eventSeatId = eventSeat.id.get,
          integrationId,
          buyerTypeCode,
          transactionType,
          revenueCategoryId = None
        )
      } yield {
        fail()
      }).recover {
        case err => assert(err.isInstanceOf[Throwable])
      }
    }
  }

  "TransactionService#getOne" should {
    "get one transaction by externalSeatId and eventSeatId" in {
      for {
        created <- r.transaction(primaryTransactionId = Some("1"))
        matching <- transactionService.getOne("1", created.eventSeatId)
        notMatching <- transactionService.getOne("2", created.eventSeatId)
      } yield {
        assert(matching.contains(created))
        assert(notMatching.isEmpty)
      }
    }
  }

  "TransactionService#bulkUpdate" should {
    "update specific fields (timestamp, buyerTypeCode, price, and modifiedAt)" in {
      val price = 20.01
      val buyerTypeCode = "btc"
      val timestamp = DateHelper.now()

      for {
        rc <- r.revenueCategory()
        transactions <- r.some(i => r.transaction(primaryTransactionId = Some(i.toString)))
        updateResult <- transactionService.bulkUpdate(
          transactions.map(
            _.copy(
              price = price,
              buyerTypeCode = buyerTypeCode,
              timestamp = timestamp,
              revenueCategoryId = rc.id
            )
          )
        )
        updatedTransactions <- Future.sequence(
          transactions
            .map(t => transactionService.getOne(t.primaryTransactionId.get, t.eventSeatId))
        )
      } yield {
        assert(updateResult == transactions.length)
        assert(updatedTransactions.forall(transaction => {
          val t = transaction.get

          t.modifiedAt.after(transactions.head.modifiedAt) &&
          t.price == price &&
          t.buyerTypeCode == "btc" &&
          t.timestamp == timestamp
        }))
      }
    }

    "filter out transactions without an id" in {
      for {
        transactions <- r.some(_ => r.transaction())
        updateResult <- transactionService.bulkUpdate(transactions.map(_.copy(id = None)))
      } yield {
        assert(updateResult == 0)
      }
    }
  }

  "TransactionService#bulkUpdateIntegrationIds" should {
    "bulk update integrationID" in {
      for {
        integration <- integrationService.getById(1).map(_.get)
        created <- r.some(i => r.transaction(primaryTransactionId = Some(i.toString)))
        updateResult <- transactionService.bulkUpdateIntegrationIds(
          created.map(t => (t.id.get, integration.id.get))
        )
        updated <- Future.sequence(
          created.map(t => transactionService.getOne(t.primaryTransactionId.get, t.eventSeatId))
        )
      } yield {
        assert(updateResult == created.length)
        assert(created.length == updated.length)
        assert(updated.forall(_.exists(_.integrationId == integration.id.get)))
      }
    }
  }

  "TransactionService#bulkInsert" should {
    "bulk insert new transactions" in {
      for {
        transactions <- r.some(_ => r.transaction())
        otherEventSeat <- r.eventSeat()
        createdResult <- transactionService.bulkInsert(
          transactions.map(_.copy(eventSeatId = otherEventSeat.id.get))
        )
      } yield {
        assert(createdResult.get == transactions.length)
      }
    }
  }

  "TransactionService#getAllCreatedAfter" should {
    "get all created after" in {
      val timestamp = DateHelper.now()

      val nTransactions = 3

      for {
        integration <- integrationService.getById(1).map(_.get)
        _ <- r.some(_ => r.transaction(integrationId = integration.id.get), nTransactions)
        transactions <- transactionService.getAllCreatedAfter(
          timestamp,
          integrationId = integration.id.get
        )
      } yield {
        assert(transactions.length == nTransactions)
      }
    }
  }

  "TransactionService#upsert" should {
    "update an existing transaction" in {
      val primaryTransactionId = "456"
      val timestamp = new Timestamp(12312)
      val price = 19.99
      val buyerType = "asdfas"

      for {
        integration <- integrationService.getById(1).map(_.get)
        eventSeat <- r.eventSeat()
        _ <- r.transaction(
          eventSeatId = eventSeat.id.get,
          primaryTransactionId = Some(primaryTransactionId),
          integrationId = integration.id.get
        )
        _ <- r.some(_ => r.transaction(integrationId = integration.id.get))
        beforeCount <- transactionService
          .getAllCreatedAfter(new Timestamp(0), integration.id.get)
          .map(_.size)
        _ <- transactionService.upsert(
          Transaction(
            id = None,
            createdAt = new Timestamp(0),
            modifiedAt = new Timestamp(0),
            timestamp = timestamp,
            price = price,
            eventSeatId = eventSeat.id.get,
            integrationId = integration.id.get,
            buyerTypeCode = buyerType,
            transactionType = TransactionType.Purchase,
            revenueCategoryId = None,
            primaryTransactionId = Some(primaryTransactionId)
          )
        )
        afterCount <- transactionService
          .getAllCreatedAfter(new Timestamp(0), integration.id.get)
          .map(_.size)
        updated <- transactionService.getOne(primaryTransactionId, eventSeat.id.get)
      } yield {
        assert(afterCount == beforeCount)

        assert(updated.isDefined)
        assert(updated.get.timestamp == timestamp)
        assert(updated.get.price == price)
        assert(updated.get.eventSeatId == eventSeat.id.get)
        assert(updated.get.integrationId == integration.id.get)
        assert(updated.get.buyerTypeCode == buyerType)
        assert(updated.get.primaryTransactionId.get == primaryTransactionId)
      }
    }

    "insert a new transaction" in {
      val primaryTransactionId = "456"
      val timestamp = new Timestamp(12312)
      val price = 19.99
      val buyerType = "asdfas"

      for {
        integration <- integrationService.getById(1).map(_.get)
        eventSeat <- r.eventSeat()
        _ <- r.some(_ => r.transaction(integrationId = integration.id.get))
        beforeCount <- transactionService
          .getAllCreatedAfter(new Timestamp(0), integration.id.get)
          .map(_.size)
        _ <- transactionService.upsert(
          Transaction(
            id = None,
            createdAt = new Timestamp(0),
            modifiedAt = new Timestamp(0),
            timestamp = timestamp,
            price = price,
            eventSeatId = eventSeat.id.get,
            integrationId = integration.id.get,
            buyerTypeCode = buyerType,
            transactionType = TransactionType.Purchase,
            revenueCategoryId = None,
            primaryTransactionId = Some(primaryTransactionId)
          )
        )
        afterCount <- transactionService
          .getAllCreatedAfter(new Timestamp(0), integration.id.get)
          .map(_.size)
        updated <- transactionService.getOne(primaryTransactionId, eventSeat.id.get)
      } yield {
        assert(afterCount - beforeCount == 1)

        assert(updated.isDefined)
        assert(updated.get.timestamp == timestamp)
        assert(updated.get.price == price)
        assert(updated.get.eventSeatId == eventSeat.id.get)
        assert(updated.get.integrationId == integration.id.get)
        assert(updated.get.buyerTypeCode == buyerType)
        assert(updated.get.primaryTransactionId.get == primaryTransactionId)
      }
    }

    "update primaryTransactionId when uniquely identifying transaction by it's secondaryTransactionId" in {
      val primaryTransactionId = "123"
      val secondaryTransactionId = "345"
      val price = 19.99
      val buyerType = "asdfs"
      val timestamp = new Timestamp(0)

      for {
        integration <- integrationService.getById(1).map(_.get)
        eventSeat <- r.eventSeat()
        _ <- r.transaction(
          eventSeatId = eventSeat.id.get,
          secondaryTransactionId = Some(secondaryTransactionId),
          integrationId = integration.id.get
        )
        _ <- r.some(_ => r.transaction(integrationId = integration.id.get))
        beforeCount <- transactionService
          .getAllCreatedAfter(new Timestamp(0), integration.id.get)
          .map(_.size)
        _ <- transactionService.upsert(
          Transaction(
            timestamp = timestamp,
            price = price,
            eventSeatId = eventSeat.id.get,
            integrationId = integration.id.get,
            buyerTypeCode = buyerType,
            transactionType = TransactionType.Purchase,
            revenueCategoryId = None,
            primaryTransactionId = Some(primaryTransactionId),
            secondaryTransactionId = Some(secondaryTransactionId)
          )
        )
        afterCount <- transactionService
          .getAllCreatedAfter(new Timestamp(0), integration.id.get)
          .map(_.size)
        updated <- transactionService.getOne(primaryTransactionId, eventSeat.id.get)
      } yield {
        assert(afterCount == beforeCount)

        assert(updated.isDefined)
        assert(updated.get.timestamp == timestamp)
        assert(updated.get.price == price)
        assert(updated.get.eventSeatId == eventSeat.id.get)
        assert(updated.get.integrationId == integration.id.get)
        assert(updated.get.buyerTypeCode == buyerType)
        assert(updated.get.primaryTransactionId.get == primaryTransactionId)
        assert(updated.get.secondaryTransactionId.get == secondaryTransactionId)
      }
    }
  }
}
