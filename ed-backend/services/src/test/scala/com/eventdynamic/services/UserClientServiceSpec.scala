package com.eventdynamic.services

import com.eventdynamic.models.{Client, ClientPerformanceType, UserClient}
import com.eventdynamic.test.DatabaseSuite
import com.eventdynamic.utils._
import org.scalatest.AsyncWordSpec

class UserClientServiceSpec extends AsyncWordSpec with DatabaseSuite {

  val userClientService = new UserClientService(ctx)
  val clientService = new ClientService(ctx)
  val userService = new UserService(ctx)

  "UserClientService#create" should {
    "create a User Client mapping when user is createdfor one mapping" in {
      for {
        SuccessResponse(client: Client) <- clientService.create(
          "Client 1",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        (newUser, _) <- userService.create(
          "test@dialexa.com",
          "First",
          "Last",
          None,
          client.id.get,
          None,
          true
        )
        userClient <- userClientService.get(newUser.id.get, client.id.get)
      } yield {
        assert(newUser.activeClientId === client.id.get)
        assert(userClient.isDefined)
        assert(userClient.get.isDefault)
      }
    }

    "create a User Client mapping when there are multiple mappings" in {
      for {
        SuccessResponse(client1: Client) <- clientService.create(
          "Client 1",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        SuccessResponse(client2: Client) <- clientService.create(
          "Client 2",
          60,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        (newUser, _) <- userService.create(
          "test@dialexa.com",
          "First",
          "Last",
          None,
          client1.id.get,
          None,
          true
        )
        SuccessResponse(userClient2: UserClient) <- userClientService.create(
          newUser.id.get,
          client2.id.get
        )
        userClient1 <- userClientService.get(newUser.id.get, client1.id.get)
        userClient2 <- userClientService.get(newUser.id.get, client2.id.get)
      } yield {
        assert(userClient1.get.isDefault)

        assert(userClient2.get.userId === newUser.id.get)
        assert(userClient2.get.clientId === client2.id.get)
        assert(!userClient2.get.isDefault)
      }
    }
  }

  "UserClientService#getDefaultClientIdForUserById" should {
    "get default client id for a user when there is one mapping" in {
      for {
        SuccessResponse(client1: Client) <- clientService.create(
          "Client 1",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        (newUser, _) <- userService.create(
          "test@dialexa.com",
          "First",
          "Last",
          None,
          client1.id.get,
          None,
          true
        )
        defaultClientId <- userClientService.getDefaultClientIdForUserById(newUser.id)

      } yield {
        assert(defaultClientId === client1.id)
      }
    }

    "get default client id for a user when there are multiple mappings" in {
      for {
        SuccessResponse(client1: Client) <- clientService.create(
          "Client 1",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        SuccessResponse(client2: Client) <- clientService.create(
          "Client 2",
          60,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        (newUser, _) <- userService.create(
          "test@dialexa.com",
          "First",
          "Last",
          None,
          client1.id.get,
          None,
          true
        )
        _ <- userClientService.create(newUser.id.get, client2.id.get)
        defaultClientId <- userClientService.getDefaultClientIdForUserById(newUser.id)

      } yield {
        assert(defaultClientId === client1.id)
      }
    }
  }

  "UserClientService#getAllClientIdsForUser" should {
    "get all client ids for a user with one associated client" in {
      for {
        SuccessResponse(client1: Client) <- clientService.create(
          "Client 1",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        SuccessResponse(client2: Client) <- clientService.create(
          "Client 2",
          60,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        (newUser, _) <- userService.create(
          "test@dialexa.com",
          "First",
          "Last",
          None,
          client1.id.get,
          None,
          true
        )
        allClientIds <- userClientService.getAllClientIdsForUser(newUser.id)
      } yield {
        assert(allClientIds.contains(client1.id.get))
        assert(!allClientIds.contains(client2.id.get))
      }
    }

    "get all client ids for a user with multiple associated clients" in {
      for {
        SuccessResponse(client1: Client) <- clientService.create(
          "Client 1",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        SuccessResponse(client2: Client) <- clientService.create(
          "Client 2",
          60,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        (newUser, _) <- userService.create(
          "test@dialexa.com",
          "First",
          "Last",
          None,
          client1.id.get,
          None,
          true
        )
        SuccessResponse(userClient2: UserClient) <- userClientService.create(
          newUser.id.get,
          client2.id.get
        )
        allClientIds <- userClientService.getAllClientIdsForUser(newUser.id)
      } yield {
        assert(allClientIds.contains(client1.id.get))
        assert(allClientIds.contains(client2.id.get))
      }
    }
  }
}
