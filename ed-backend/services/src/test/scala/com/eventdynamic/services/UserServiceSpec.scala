package com.eventdynamic.services

import com.eventdynamic.models.{Client, ClientPerformanceType, User}
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.{DateHelper, SuccessResponse}
import org.mindrot.jbcrypt.BCrypt
import org.scalatest.{AsyncWordSpec, Matchers}
import org.scalatest.mockito.MockitoSugar

import scala.util.Random

class UserServiceSpec extends AsyncWordSpec with Matchers with DatabaseSuite {

  val r = new RandomUtil(ctx)
  val random = new Random()

  "UserService" should {

    // Client service to share in tests
    val clientService = new ClientService(ctx)

    // User service to share in tests
    val userService = new UserService(ctx)

    // UserClient service to share in tests
    val userClientService = new UserClientService(ctx)

    def createUser() = {
      val pass = "password"
      val email = "greg.smith+" + random.nextString(8) + "@dialexa.com"

      for {
        SuccessResponse(client1: Client) <- clientService.create(
          "Client 1",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        (user: User, tPass: String) <- userService
          .create(email, "test", "user", Some(pass), client1.id.get, None, true)
      } yield {
        user
      }
    }

    def createTestUser(email: String, password: String) = {
      for {
        SuccessResponse(client1: Client) <- clientService.create(
          "Client 1",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        (user: User, tPass: String) <- userService
          .create(email, "test", "user", Some(password), client1.id.get, None, true)
      } yield {
        user
      }
    }

    def createUserWithEmail(email: String) = {
      for {
        SuccessResponse(client1: Client) <- clientService.create(
          "Client 1",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        (user: User, tPass: String) <- userService
          .create(email, "test", "user", Some("password"), client1.id.get, None, true)
      } yield {
        user
      }
    }

    "create user" in {
      createUser()
        .map { user =>
          assert(user.id.get == 2)
        }
    }

    "get user by id" in {
      for {
        createdUser <- createUser()
        createdId = createdUser.id.get
        foundUser <- userService.getById(createdId)
      } yield {
        foundUser match {
          case Some(user) => assert(user.id == Some(createdId))
          case None       => fail()
        }
      }
    }

    "get user by email" in {
      for {
        createdUser <- createUserWithEmail("testById@dialexa.com")
        createdUserId = createdUser.id.get
        foundUser <- userService.getByEmail("testById@dialexa.com")
      } yield {
        foundUser match {
          case Some(user) => assert(user.id == Some(createdUserId))
          case None       => fail()
        }
      }
    }

    "get all users with same clientId when user is admin" in {
      for {
        client <- r.client()
        user <- r.user(client.id, true)
        user1 <- r.user(client.id)
        user2 <- r.user(client.id)
        user3 <- r.user(client.id)
        result <- userService.getAll(client.id.get, true)
      } yield {
        val idList: Seq[Int] = Seq(user, user1, user2, user3)
        result.map(user => assert(idList.contains(user.id.get)))
        assert(result.length === 4)
      }
    }

    "get all users with same clientId that are not admin if user is not admin" in {
      for {
        client <- r.client()
        user <- r.user(client.id, false)
        user1 <- r.user(client.id, false)
        user2 <- r.user(client.id, true)
        user3 <- r.user(client.id, true)
        result <- userService.getAll(client.id.get, false)
      } yield {
        val validIdList: Seq[Int] = Seq(user, user1)
        val invalidIdList: Seq[Int] = Seq(user2, user3)
        result.map(user => {
          assert(validIdList.contains(user.id.get))
          assert(!invalidIdList.contains(user.id.get))
        })
        assert(result.length === 2)
      }
    }

    "authenticate user" in {
      for {
        _ <- createTestUser("test@dialexa.com", "password")
        authenticated <- userService.authenticate("test@dialexa.com", "password")
      } yield assert(authenticated)
    }

    "not authenticate user" in {
      for {
        _ <- createTestUser("test2@dialexa.com", "password2")
        authenticated <- userService.authenticate("test2@dialexa.com", "Password2")
      } yield assert(!authenticated)
    }

    "not authenticate a user that doesn't exist" in {
      for {
        _ <- createTestUser("test3@dialexa.com", "password3")
        authenticated <- userService.authenticate("askdfasdf@dialexa.com", "password3")
      } yield assert(!authenticated)
    }

    "delete user by id" in {
      for {
        createdUser <- createUser()
        createdId = createdUser.id.get
        _ <- userService.delete(createdId)
        deletedUser <- userService.getById(createdId)
      } yield assert(deletedUser.isEmpty)
    }

    "delete user by email" in {
      for {
        createdUser <- createUserWithEmail("test4@dialexa.com")
        createdId = createdUser.id.get
        _ <- userService.deleteByEmail("test4@dialexa.com")
        deletedUser <- userService.getById(createdId)
      } yield assert(deletedUser.isEmpty)
    }

    "update user" in {
      for {
        createdUser <- createUser()
        createdId = createdUser.id.get
        optUser <- userService.getById(createdId)
        _ <- userService.update(optUser.get.copy(email = "test5@dialexa.com"))
        updatedOptUser <- userService.getById(createdId)
      } yield {
        assert(updatedOptUser.get.email == "test5@dialexa.com", "updated user email did not match")
        assert(updatedOptUser.get.id == Some(createdId), "updated user id did not match")
      }
    }

    "update user contact info" in {
      for {
        createdUser <- createUser()
        createdId = createdUser.id.get
        _ <- userService.updateUserInfo(createdId, "New", "TestUser", Some("1234567890"))
        updatedOptUser <- userService.getById(createdId)
      } yield {
        assert(updatedOptUser.get.firstName == "New", "updated User firstName did not match")
        assert(updatedOptUser.get.lastName == "TestUser", "updated User lastName did not match")
        assert(
          updatedOptUser.get.phoneNumber.get == "1234567890",
          "updated User phone did not match"
        )
      }
    }

    "update user email" in {
      for {
        createdUser <- createUser()
        createdId = createdUser.id.get
        _ <- userService.updateEmail(createdId, "newEmail@dialexa.com")
        updatedOptUser <- userService.getById(createdId)
      } yield
        assert(updatedOptUser.get.email == "newEmail@dialexa.com", "updated password did not match")
    }

    "add temporary password and expiration timestamp" in {
      val now = DateHelper.now()
      val almost = DateHelper.later(55) // almost is based on localDateTime plus interval
      for {
        createdUser <- createUser()
        createdId = createdUser.id.get
        _ <- userService.addTempPass(createdId, "password1", 60)
        updatedOptUser <- userService.getById(createdId)
      } yield {
        assert(
          BCrypt.checkpw("password1", updatedOptUser.get.tempPass.getOrElse("")),
          "tempPass did not match"
        )
        assert(
          updatedOptUser.get.tempExpire.getOrElse(now).getTime() > almost
            .getTime(),
          "tempExpire was not set"
        )
      }
    }

    "change user password" in {
      for {
        createdUser <- createUser()
        createdId = createdUser.id.get
        _ <- userService.changePassword(createdId, "newPass")
        updatedUser <- userService.getById(createdId)
      } yield assert(BCrypt.checkpw("newPass", updatedUser.get.password.getOrElse("")))
    }

    // Verify that changing a user's password when he/she has a tempPass will
    // result in clearing tempPass to None and resetting tempExpire to now
    "change user password, reset tempPass to Null, and set tempExpire to now" in {
      val now = DateHelper.now()
      val soon = DateHelper.later(5) // soon is based on localDateTime plus interval
      for {
        createdUser <- createUser()
        createdId = createdUser.id.get
        // this next operation will add a tempPass, and a tempExpire 60 minutes in the future
        _ <- userService.addTempPass(createdId, "fakeHashedPass", 60)
        // now we are changing the password, which should set tempPass to None & tempExpire to now
        _ <- userService.changePassword(createdId, "newPass")
        updatedUser <- userService.getById(createdId)
      } yield {
        assert(
          BCrypt.checkpw("newPass", updatedUser.get.password.getOrElse("")),
          "newPass did not match"
        )
        assert(updatedUser.get.tempPass == None, "tempPass was not cleared")
        assert(
          updatedUser.get.tempExpire.getOrElse(now).getTime() < soon.getTime(),
          "tempExpire was not reset"
        )
      }
    }

    "set active client to same client" in {
      for {
        createdUser <- createUser()
        clientId = createdUser.activeClientId
        updatedUser <- userService.setActiveClient(createdUser.id.get, clientId)
      } yield {
        assert(updatedUser.isDefined)
        assert(updatedUser.get.activeClientId === clientId)
      }
    }

    "set active client to different client" in {
      for {
        createdUser <- createUser()
        SuccessResponse(client2: Client) <- clientService.create(
          "Client 2",
          30,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        _ <- userClientService.create(createdUser.id.get, client2.id.get)
        updatedUser <- userService.setActiveClient(createdUser.id.get, client2.id.get)
      } yield {
        assert(updatedUser.isDefined)
        assert(updatedUser.get.activeClientId === client2.id.get)
      }
    }

    "not set active client if new client id does not exist" in {
      for {
        createdUser <- createUser()
        invalidClientId = createdUser.activeClientId + 1
        updatedUser <- userService.setActiveClient(createdUser.id.get, invalidClientId)
        client <- clientService.getById(invalidClientId)
        retrievedUser <- userService.getById(createdUser.id.get)
      } yield {
        assert(!updatedUser.isDefined)
        assert(!client.isDefined)
        assert(createdUser.activeClientId === retrievedUser.get.activeClientId)
      }
    }

    "not set active client to unassociated client" in {
      for {
        createdUser <- createUser()
        SuccessResponse(newClient: Client) <- clientService.create(
          "Client 2",
          50,
          2,
          None,
          1.125,
          None,
          ClientPerformanceType.MLB
        )
        updatedUser <- userService.setActiveClient(createdUser.id.get, newClient.id.get)
        retrievedUser <- userService.getById(createdUser.id.get)
      } yield {
        assert(!updatedUser.isDefined)
        assert(newClient.id.isDefined)
        assert(createdUser.activeClientId === retrievedUser.get.activeClientId)
      }
    }

    "throw on duplicate case insensitive emails" in {
      createUserWithEmail("test@dialexa.com") map { user =>
        assert(user.id.isDefined)
      }

      recoverToExceptionIf[Exception] {
        createUserWithEmail("TEST@dialexa.com")
      } map { ex =>
        assert(ex.getMessage.contains("duplicate key value violates unique constraint"))
      }
    }
  }
}
