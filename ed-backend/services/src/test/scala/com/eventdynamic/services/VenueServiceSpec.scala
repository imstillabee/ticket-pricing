package com.eventdynamic.services

import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import org.scalatest.AsyncWordSpec

import scala.concurrent.Future

class VenueServiceSpec extends AsyncWordSpec with DatabaseSuite {
  val venueService = new VenueService(ctx)

  val r = new RandomUtil(ctx)

  "VenueService#create" should {
    "create a venue returning the venue's ID" in {
      for {
        v1 <- venueService.create(
          "venue",
          50000,
          "02134",
          Some("https:eventdynamic.com/testVenueSvg")
        )
        v2 <- venueService.create(
          "venue2",
          50000,
          "02134",
          Some("https:eventdynamic.com/testVenueSvg")
        )
      } yield {
        assert(v1 == 1)
        assert(v2 == 2)
      }
    }
  }

  "VenueService#getById" should {
    "return Some(venue) when one exists" in {
      for {
        venueId <- r.venue()
        result <- venueService.getById(venueId)
      } yield {
        assert(result.isDefined)
        assert(result.get.id.contains(venueId))
      }
    }

    "return None when one does not exist" in {
      for {
        _ <- r.venue()
        result <- venueService.getById(123)
      } yield {
        assert(result.isEmpty)
      }
    }
  }

  "VenueService#getForEvent" should {
    "return Some(venue) when one exists" in {
      for {
        venueId <- r.venue()
        event <- r.event(venueId = venueId)
        venue <- venueService.getForEvent(eventId = event.id.get)
      } yield {
        assert(venue.isDefined)
        assert(venue.get.id.contains(venueId))
      }
    }

    "return None when one does not exist" in {
      for {
        venueId <- r.venue()
        _ <- r.event(venueId = venueId)
        venue <- venueService.getForEvent(eventId = 176)
      } yield {
        assert(venue.isEmpty)
      }
    }
  }

  "VenueService#getForSeason" should {
    "return a list of venues used in the season" in {
      val nCase = 3

      for {
        season <- r.season()
        otherSeason <- r.season()
        venueId <- r.venue()
        otherVenueId <- r.venue()

        // These should match our query
        _ <- r.some(_ => r.event(seasonId = Some(season.id.get), venueId = venueId), nCase)
        _ <- r.some(_ => r.event(seasonId = Some(season.id.get), venueId = otherVenueId), nCase)

        // These should not match the filter
        _ <- r.some(_ => r.event(seasonId = Some(otherSeason.id.get), venueId = venueId), nCase)
        _ <- r.some(_ => r.event(seasonId = None, venueId = venueId), nCase)

        // Query
        venues <- venueService.getForSeason(season.id.get)
      } yield {
        assert(venues.length == 2)
        assert(venues.head.id.get == venueId)
      }
    }
  }
}
