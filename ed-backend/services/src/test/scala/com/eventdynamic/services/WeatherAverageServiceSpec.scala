package com.eventdynamic.services

import com.eventdynamic.test.DatabaseSuite
import com.eventdynamic.utils.{ResponseCode, SuccessResponse}
import org.scalatest.AsyncWordSpec

import scala.concurrent.Future

class WeatherAverageServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "WeatherService" should {
    val weatherService = new WeatherAverageService(ctx)
    val venueService = new VenueService(ctx)

    def createSomeWeatherAverages(venueId: Int): Future[Seq[ResponseCode]] = {
      val futures = for {
        month <- 1 to 1
        day <- 1 to 30
        hour <- 1 to 24
      } yield weatherService.create(venueId, month, day, hour, 75.5f)

      Future.sequence(futures)
    }

    "create a weatherAverage record" in {
      for {
        venueId <- venueService.create("Venue", 1, "12345", None)
        SuccessResponse(weatherAverageId: Int) <- weatherService.create(venueId, 1, 1, 1, 75.5f)
      } yield {
        assert(weatherAverageId === 1)
      }
    }

    "get a weatherAverage record" in {
      val month = 1
      val day = 30
      val hour = 2

      for {
        firstVenueId <- venueService.create("Venue1", 1, "12345", None)
        secondVenueId <- venueService.create("Venue2", 1, "12345", None)
        _ <- createSomeWeatherAverages(firstVenueId)
        _ <- createSomeWeatherAverages(secondVenueId)
        res <- weatherService.get(firstVenueId, month, day, hour)
      } yield {
        assert(res.isDefined)
        assert(res.get.month == month)
        assert(res.get.day == day)
        assert(res.get.hour == hour)
        assert(res.get.venueId == firstVenueId)
      }
    }
  }
}
