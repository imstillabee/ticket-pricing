package com.eventdynamic.services.mlb

import com.eventdynamic.models.mlb.MySportsFeedInfo
import com.eventdynamic.test.RandomUtil
import com.eventdynamic.test.DatabaseSuite
import com.eventdynamic.utils.SuccessResponse
import org.scalatest.AsyncWordSpec

class MySportsFeedInfoServiceSpec extends AsyncWordSpec with DatabaseSuite {
  "MySportsFeedInfoServiceSpec" should {
    val r = new RandomUtil(ctx)
    val teamAbbreviationService = new MySportsFeedInfoService(ctx)

    def createTeamAbbreviation() = {
      for {
        client <- r.client()
        SuccessResponse(teamAbbr: MySportsFeedInfo) <- teamAbbreviationService
          .create("NYM", client.id.get)
      } yield {
        teamAbbr
      }
    }

    "create abbreviation for client" in {
      for {
        teamAbbr <- createTeamAbbreviation()
      } yield {
        assert(teamAbbr.id.get == 1)
        assert(teamAbbr.abbreviation == "NYM")
      }
    }

    "get abbreviation from client id" in {
      for {
        teamAbbr <- createTeamAbbreviation()
        abbreviation <- teamAbbreviationService.getByClientId(teamAbbr.clientId)
      } yield {
        assert(abbreviation.get === teamAbbr)
      }
    }

    "get abbreviation from client ids" in {
      for {
        teamAbbr1 <- createTeamAbbreviation()
        teamAbbr2 <- createTeamAbbreviation()
        _ <- createTeamAbbreviation()
        abbreviations <- teamAbbreviationService.getByClientIds(
          Seq(teamAbbr1.clientId, teamAbbr2.clientId)
        )
      } yield {
        assert(abbreviations.length === 2)
      }
    }
  }
}
