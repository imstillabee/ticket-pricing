package com.eventdynamic.test

import com.eventdynamic.db.EDContext
import org.flywaydb.core.Flyway
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, Suite}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

trait DatabaseSuite extends Suite with BeforeAndAfterAll with BeforeAndAfterEach {
  private val logger = LoggerFactory.getLogger(this.getClass)

  logger.trace("Creating context")
  val ctx = new EDContext()
  import ctx.profile.api._

  logger.trace("Loading config settings...")
  private val url = ctx.config.getString("db.url")
  private val driver = ctx.config.getString("db.driver")
  logger.trace(s"   url: $url")
  logger.trace(s"driver: $driver")

  val flyway = new Flyway()
  flyway.setDataSource(url, "", "")
  flyway.setLocations("filesystem:../flyway/sql")

  override def beforeAll() = {
    logger.trace("Running migrations")
    flyway.clean()
    flyway.migrate()
  }

  override def beforeEach() = {
    logger.trace("Clearing tables")

    val clearFuture = clearTables().andThen {
      case Success(_) => logger.trace("Cleared tables")
      case Failure(e) => throw new Exception("Failed to clear database", e)
    }

    Await.result(clearFuture, Duration.Inf)
  }

  override def afterAll() = {
    logger.trace("Done with DatabaseSuiteTests, closing connection")
    ctx.db.close()
  }

  private val whitelist = Seq("Integrations", "Users", "JobTriggers", "JobPriorities", "Jobs")

  private def clearTables(): Future[Unit] = {
    for {
      // All tables
      names <- ctx.db
        .run(sql"""SELECT tablename FROM pg_tables WHERE schemaname='public'""".as[String])
        .map(_.diff(whitelist).map("\"" + _ + "\""))
      _ <- ctx.db.run(sqlu"""TRUNCATE #${names.mkString(",")} RESTART IDENTITY CASCADE""")

      // Users table
      _ <- ctx.db.run(sql"""DELETE FROM "Users" WHERE "id" > 1""".as[Int])
      _ <- ctx.db.run(sqlu"""ALTER SEQUENCE "Users_id_seq" RESTART WITH 2""")
    } yield Unit
  }
}
