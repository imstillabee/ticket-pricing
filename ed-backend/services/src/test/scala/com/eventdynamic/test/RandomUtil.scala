package com.eventdynamic.test

import java.sql.Timestamp

import com.eventdynamic.db.EDContext
import com.eventdynamic.models.RoundingType.RoundingType
import com.eventdynamic.models.TransactionType.TransactionType
import com.eventdynamic.models._
import com.eventdynamic.utils.{DateHelper, SuccessResponse}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

class RandomUtil(ctx: EDContext)(implicit ec: ExecutionContext) {
  val services = new ServiceHolder(ctx)

  object ID {
    private var n = 1

    def next(): Int = {
      synchronized {
        n = n + 1
        n
      }
    }
  }

  def string(len: Int = 20): String = Random.nextString(len)
  def int(): Int = Random.nextInt()
  def boolean(): Boolean = Random.nextBoolean()
  def double(): Double = Random.nextDouble()
  def float(): Float = Random.nextFloat()
  def timestamp(): Timestamp = DateHelper.now()

  def client(
    name: String = string(),
    pricingInterval: Int = int(),
    eventScoreFloor: Double = double(),
    eventScoreCeiling: Option[Double] = None,
    springFloor: Double = double(),
    springCeiling: Option[Double] = None
  ): Future[Client] = {
    services.client.create(
      name,
      pricingInterval,
      eventScoreFloor,
      eventScoreCeiling,
      springFloor,
      springCeiling,
      ClientPerformanceType.MLB
    ) map {
      case SuccessResponse(client: Client) => client
    }
  }

  def user(
    clientId: Option[Int],
    isAdmin: Boolean = boolean(),
    email: String = string(8) + "@dialexa.com",
    firstName: String = string(5),
    lastName: String = string(5),
    password: String = string(10),
    phoneNumber: Option[String] = Some(string(10))
  ): Future[Int] = {
    for {
      (user, _) <- services.user.create(
        email,
        firstName,
        lastName,
        Some(password),
        clientId.get,
        phoneNumber,
        isAdmin
      )
    } yield user.id.get
  }

  def clientIntegration(
    clientId: Int = 0,
    integrationId: Int = 0,
    isActive: Boolean = boolean(),
    isPrimary: Boolean = boolean(),
  ): Future[ClientIntegration] = {
    for {
      cId <- if (clientId == 0) client().map(_.id.get)
      else Future.successful(clientId)

      iId <- if (integrationId == 0) services.integration.getById(1).map(_.get.id.get)
      else Future.successful(integrationId)

      clientIntegration <- services.clientIntegration.create(cId, iId, isActive, isPrimary)
    } yield clientIntegration
  }

  def secondaryPricingRule(
    clientIntegrationId: Int = 0,
    percent: Option[Int] = Some(int()),
    constant: Option[BigDecimal] = Some(double())
  ): Future[SecondaryPricingRule] = {
    for {
      ciId <- if (clientIntegrationId == 0) clientIntegration().map(_.id.get)
      else Future.successful(clientIntegrationId)

      secondaryPricingRule <- services.secondaryPricingRule
        .create(ciId, percent, constant)
    } yield secondaryPricingRule
  }

  def event(
    primaryEventId: Option[Int] = None,
    name: String = string(),
    timestamp: Option[Timestamp] = None,
    clientId: Int = 0,
    venueId: Int = 0,
    eventCategoryId: Int = 0,
    seasonId: Option[Int] = None,
    isBroadcast: Boolean = true,
    percentPriceModifier: Int = 0,
    eventScore: Option[Double] = None,
    eventScoreModifier: Double = 0
  ): Future[Event] = {

    for {
      cId <- if (clientId == 0) client().map(_.id.get)
      else Future.successful(clientId)
      ecId <- if (eventCategoryId == 0) this.eventCategoryId(string(), cId)
      else Future.successful(eventCategoryId)
      vId <- if (venueId == 0) venue() else Future.successful(venueId)
      SuccessResponse(event: Event) <- services.event.create(
        primaryEventId,
        name,
        timestamp,
        cId,
        vId,
        ecId,
        seasonId,
        isBroadcast,
        percentPriceModifier,
        eventScore,
        eventScoreModifier
      )
    } yield event
  }

  def eventSeat(
    seatId: Int = 0,
    eventId: Int = 0,
    priceScaleId: Int = 0,
    listedPrice: Option[BigDecimal] = None,
    overridePrice: Option[BigDecimal] = None,
    isListed: Boolean = true,
    isHeld: Boolean = false
  ): Future[EventSeat] = {
    val now = timestamp()
    for {
      sId <- if (seatId == 0) seat().map(_.id.get)
      else Future.successful(seatId)

      eId <- if (eventId == 0) event().map(_.id.get)
      else Future.successful(eventId)

      vId <- venue()
      integration <- services.integration.getById(1).map(_.get)

      psId <- if (priceScaleId == 0)
        services.priceScales.create(name = string(), vId, integration.id.get)
      else Future.successful(priceScaleId)

      SuccessResponse(eventSeat: EventSeat) <- services.eventSeat.create(
        EventSeat(None, sId, eId, psId, listedPrice, None, isListed, now, now, isHeld)
      )
    } yield eventSeat
  }

  def eventSeats(
    seats: Seq[Seat],
    events: Seq[Event],
    priceScaleId: Int,
    listedPrice: Option[BigDecimal] = None,
    isListed: Boolean = true,
  ): Future[Seq[EventSeat]] = {
    Future.sequence(for {
      s <- seats
      e <- events
    } yield {
      eventSeat(s.id.get, e.id.get, priceScaleId, listedPrice, None, isListed)
    })
  }

  def eventCategoryId(name: String = string(), clientId: Int): Future[Int] = {
    for {
      eventCategoryId <- services.eventCategory.create(name, clientId)
    } yield eventCategoryId
  }

  def priceGuard(
    eventId: Int,
    section: String,
    row: String,
    minimumPrice: Int = 15,
    maximumPrice: Option[Int] = None
  ): Future[PriceGuard] = {
    for {
      priceGuardId <- services.priceGuard.createForSectionAndRow(
        eventId,
        section,
        row,
        minimumPrice,
        maximumPrice
      )
      priceGuard <- services.priceGuard.getById(priceGuardId)
    } yield {
      priceGuard.get
    }
  }

  def priceScaleId(
    name: String = string(),
    venueId: Int = 0,
    integrationId: Int = int()
  ): Future[Int] = {
    for {
      vId <- if (venueId == 0) venue()
      else Future.successful(venueId)
      priceScaleId <- services.priceScales.create(name, vId, integrationId)
    } yield priceScaleId
  }

  def revenueCategory(name: String = string()): Future[RevenueCategory] = {
    services.revenueCategory.create(name)
  }

  def revenueCategoryMapping(
    clientId: Int = 0,
    revenueCategoryId: Int = 0,
    regex: String,
    order: Int = int()
  ): Future[RevenueCategoryMapping] = {
    for {
      cId <- if (clientId == 0) client().map(_.id.get)
      else Future.successful(clientId)
      rcId <- if (revenueCategoryId == 0) client().map(_.id.get)
      else Future.successful(revenueCategoryId)
      revenueCategoryMapping <- services.revenueCategoryMapping.create(cId, rcId, regex, order)
    } yield revenueCategoryMapping
  }

  def season(name: Option[String] = None, clientId: Int = 0): Future[Season] = {
    for {
      cId <- if (clientId == 0) client().map(_.id.get) else Future.successful(clientId)
      seasonResult <- services.season.create(name, cId).map {
        case SuccessResponse(season: Season) => season
      }
    } yield seasonResult
  }

  def seat(
    primarySeatId: Int = ID.next,
    seat: String = string(),
    row: String = string(),
    section: String = string(),
    venueId: Int = 0
  ): Future[Seat] = {
    for {
      vId <- if (venueId == 0) venue()
      else Future.successful(venueId)
      seat <- services.seat.create(primarySeatId, seat, row, section, vId) map {
        case SuccessResponse(seat: Seat) => seat
      }
    } yield seat
  }

  def transaction(
    time: Timestamp = timestamp(),
    price: BigDecimal = BigDecimal(double()),
    eventSeatId: Int = 0,
    integrationId: Int = 0,
    buyerTypeCode: String = string(),
    transactionType: TransactionType = TransactionType.Purchase,
    revenueCategoryId: Option[Int] = None,
    primaryTransactionId: Option[String] = Some(string()),
    secondaryTransactionId: Option[String] = None
  ): Future[Transaction] = {
    for {
      esId <- if (eventSeatId == 0) eventSeat().map(_.id.get)
      else Future.successful(eventSeatId)
      iId <- if (integrationId == 0) services.integration.getById(1).map(_.get.id.get)
      else Future.successful(integrationId)
      transaction <- services.transaction.create(
        time,
        price,
        esId,
        iId,
        buyerTypeCode,
        transactionType,
        revenueCategoryId,
        primaryTransactionId,
        secondaryTransactionId
      )
    } yield transaction
  }

  def venue(
    name: String = string(),
    capacity: Int = int(),
    zip: String = string(len = 5),
    svgUrl: String = string(len = 12),
    timeZone: String = "America/New_York"
  ): Future[Int] = {
    services.venue.create(name, capacity, zip, Some(svgUrl), timeZone)
  }

  def proVenuePricingRule(
    clientId: Int = 0,
    name: Option[String] = None,
    isActive: Boolean = false,
    mirrorPriceScaleId: Option[Int] = None,
    percent: Option[Int] = None,
    constant: Option[BigDecimal] = None,
    round: RoundingType = RoundingType.Ceil,
    eventIds: Seq[Int] = Seq(),
    priceScaleIds: Seq[Int] = Seq(),
    externalBuyerTypeIds: Seq[String] = Seq()
  ): Future[Int] = {
    for {
      cId <- if (clientId == 0) client().map(_.id.get) else Future.successful(clientId)
      eIds <- if (eventIds.isEmpty) event(clientId = cId).map(e => Seq(e.id.get))
      else Future.successful(eventIds)
      psIds <- if (priceScaleIds.isEmpty) priceScaleId().map(Seq(_))
      else Future.successful(priceScaleIds)
      SuccessResponse(id: Int) <- services.proVenuePricingRuleService.create(
        cId,
        eIds,
        psIds,
        if (externalBuyerTypeIds.nonEmpty) externalBuyerTypeIds else Seq(string()),
        isActive,
        mirrorPriceScaleId,
        percent,
        constant,
        name,
        round
      )
    } yield id
  }

  def teamToClientMap(teamId: Int = int(), clientId: Int = int()): Future[MLBAMTeamToClient] = {
    for {
      mapping <- services.mlbMappingService.createTeamClientMap(teamId, clientId) map {
        case SuccessResponse(teamClientMap: MLBAMTeamToClient) => teamClientMap
      }
    } yield mapping
  }

  def sectionToPriceScaleMap(
    sectionId: Int = int(),
    priceScaleId: Int = int()
  ): Future[MLBAMSectionToPriceScale] = {
    for {
      mapping <- services.mlbMappingService
        .createSectionPriceScaleMap(sectionId, priceScaleId) map {
        case SuccessResponse(sectionPriceScaleMap: MLBAMSectionToPriceScale) => sectionPriceScaleMap
      }
    } yield mapping
  }

  def scheduleEventMap(
    scheduleId: Int = int(),
    eventId: Int = int()
  ): Future[MLBAMScheduleToEvent] = {
    for {
      mapping <- services.mlbMappingService.createScheduleEventMap(scheduleId, eventId) map {
        case SuccessResponse(scheduleEventMap: MLBAMScheduleToEvent) => scheduleEventMap
      }
    } yield mapping
  }

  def some[A](creator: Int => Future[A], n: Int = 10): Future[Seq[A]] = {
    Future.sequence((1 to n).map(creator))
  }
}
