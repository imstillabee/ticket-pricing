package com.eventdynamic.test

import com.eventdynamic.db.EDContext
import com.eventdynamic.services._
import com.eventdynamic.transactions.SeasonTransaction

import scala.concurrent.ExecutionContext

class ServiceHolder(ctx: EDContext)(implicit ec: ExecutionContext) {
  lazy val client = new ClientService(ctx)
  lazy val eventSeat = new EventSeatService(ctx)
  lazy val eventCategory = new EventCategoryService(ctx)
  lazy val integration = new IntegrationService(ctx)
  lazy val secondaryPricingRule = new SecondaryPricingRulesService(ctx)
  lazy val mailerService = new MailerService
  lazy val venue = new VenueService(ctx)
  lazy val event = new EventService(ctx)
  lazy val transaction = new TransactionService(ctx)
  lazy val seasonTransaction = new SeasonTransaction(ctx, event, eventSeat, transaction)
  lazy val season = new SeasonService(ctx, seasonTransaction)
  lazy val seat = new SeatService(ctx)
  lazy val teamStat = new TeamStatService(ctx)
  lazy val nflSeasonStat = new NFLSeasonStatService(ctx)
  lazy val nflEventStat = new NFLEventStatService(ctx)
  lazy val ncaafSeasonStat = new NCAAFSeasonStatService(ctx)
  lazy val ncaafEventStat = new NCAAFEventStatService(ctx)
  lazy val ncaabSeasonStat = new NCAABSeasonStatService(ctx)
  lazy val ncaabEventStat = new NCAABEventStatService(ctx)
  lazy val mlsSeasonStat = new MLSSeasonStatService(ctx)
  lazy val mlsEventStat = new MLSEventStatService(ctx)
  lazy val clientIntegration = new ClientIntegrationService(ctx)
  lazy val revenueCategory = new RevenueCategoryService(ctx)
  lazy val projection = new ProjectionService(ctx)
  lazy val user = new UserService(ctx)
  lazy val priceScales = new PriceScaleService(ctx)
  lazy val priceGuard = new PriceGuardService(ctx)
  lazy val revenueCategoryMapping =
    new RevenueCategoryMappingService(ctx)
  lazy val materializedView = new MaterializedViewService(ctx)
  lazy val proVenuePricingRuleService = new ProVenuePricingRuleService(ctx)
  lazy val mlbMappingService = new MLBAMMappingService(ctx)
}
