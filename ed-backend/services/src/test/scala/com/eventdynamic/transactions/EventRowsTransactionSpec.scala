package com.eventdynamic.transactions

import com.eventdynamic.models.TransactionType
import com.eventdynamic.services._
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest.AsyncWordSpec
import play.api.libs.json.Json

class EventRowsTransactionSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val eventService = new EventService(ctx)
  val seatService = new SeatService(ctx)
  val eventSeatService = new EventSeatService(ctx)
  val priceGuardService = new PriceGuardService(ctx)
  val transactionService = new TransactionService(ctx)
  val eventCategoryService = new EventCategoryService(ctx)

  val eventRowsTransaction =
    new EventRowsTransaction(
      ctx,
      eventService,
      seatService,
      eventSeatService,
      priceGuardService,
      transactionService
    )

  // what do we want to test for the getAllByEvent method?
  // - that we retrieve all unsold seats for a given event, properly distributed in their rows
  // - that we do not return any seats for other events
  // - that we do not return any already-sold seats, unless they have since been returned
  // - that the open-seats-per-row totals are correct

  "EventRowsTransaction" should {
    "get all eventRows for an event" in {
      val now = DateHelper.now
      val later = DateHelper.later(10)

      for {
        venueId <- r.venue()
        _ <- r.client()
        priceScaleId <- r.priceScaleId("First Data Platinum", venueId)

        _ <- r.event(venueId = venueId)
        event <- r.event(venueId = venueId)
        otherEvent <- r.event(venueId = venueId)

        row1seat1 <- r.seat(seat = "seat1", row = "row1", section = "section1", venueId = venueId)
        row1seat2 <- r.seat(seat = "seat2", row = "row1", section = "section1", venueId = venueId)
        row1seat3 <- r.seat(seat = "seat3", row = "row1", section = "section1", venueId = venueId)
        row1seat4 <- r.seat(seat = "seat4", row = "row1", section = "section1", venueId = venueId)
        row1seat5 <- r.seat(seat = "seat5", row = "row1", section = "section1", venueId = venueId)
        row1seat6 <- r.seat(seat = "seat6", row = "row1", section = "section1", venueId = venueId)
        row3seat3 <- r.seat(seat = "seat3", row = "row3", section = "section2", venueId = venueId)
        row3seat4 <- r.seat(seat = "seat4", row = "row3", section = "section2", venueId = venueId)
        row3seat5 <- r.seat(seat = "seat5", row = "row3", section = "section2", venueId = venueId)
        row5seat6 <- r.seat(seat = "seat6", row = "row5", section = "section3", venueId = venueId)

        row1eSeat1 <- r.eventSeat(
          seatId = row1seat1.id.get,
          eventId = event.id.get,
          priceScaleId = priceScaleId
        )
        row1eSeat2 <- r.eventSeat(
          seatId = row1seat2.id.get,
          eventId = event.id.get,
          priceScaleId = priceScaleId
        ) // sold and returned
        row1eSeat3 <- r.eventSeat(
          seatId = row1seat3.id.get,
          eventId = event.id.get,
          priceScaleId = priceScaleId
        ) // sold, then returned and sold in one transaction

        _ <- r.eventSeat(
          seatId = row3seat3.id.get,
          eventId = otherEvent.id.get,
          priceScaleId = priceScaleId
        ) // wrong event
        _ <- r.eventSeat(
          seatId = row3seat4.id.get,
          eventId = otherEvent.id.get,
          priceScaleId = priceScaleId
        ) // wrong event
        _ <- r.eventSeat(
          seatId = row3seat5.id.get,
          eventId = otherEvent.id.get,
          priceScaleId = priceScaleId
        ) // wrong event

        row5eSeat6 <- r.eventSeat(
          seatId = row5seat6.id.get,
          eventId = event.id.get,
          priceScaleId = priceScaleId
        ) // will be sold

        _ <- r.eventSeat(
          seatId = row1seat4.id.get,
          eventId = event.id.get,
          priceScaleId = priceScaleId,
          isHeld = true
        ) // held seat
        _ <- r.eventSeat(
          seatId = row1seat5.id.get,
          eventId = event.id.get,
          priceScaleId = priceScaleId,
          isHeld = true
        ) // held seat
        _ <- r.eventSeat(
          seatId = row1seat6.id.get,
          eventId = event.id.get,
          priceScaleId = priceScaleId,
          isHeld = true
        ) // held seat

        _ <- r.transaction(eventSeatId = row5eSeat6.id.get) // sold
        _ <- r.transaction(eventSeatId = row1eSeat2.id.get, time = now) // sold
        _ <- r.transaction(
          eventSeatId = row1eSeat2.id.get,
          time = later,
          price = -50.00,
          transactionType = TransactionType.Return
        ) // returned
        _ <- r.transaction(eventSeatId = row1eSeat3.id.get, time = now) // sold
        _ <- r.transaction(
          eventSeatId = row1eSeat3.id.get,
          time = later,
          price = -50.00,
          transactionType = TransactionType.Return
        ) // returned
        _ <- r.transaction(eventSeatId = row1eSeat3.id.get, time = later, price = 50.00) // sold

        eventRows <- eventRowsTransaction.getAllByEvent(event.id.get)
      } yield {
        assert(eventRows.length == 1)
        assert(eventRows.head.eventId == event.id.get)
        assert(eventRows.head.section == "section1")
        assert(eventRows.head.row == "row1")
        val seatIdsArr = Json.parse(eventRows.head.seats).as[Array[Int]]
        assert(seatIdsArr.length == 2)
        assert(seatIdsArr(0) == row1eSeat1.id.get)
        assert(seatIdsArr(1) == row1eSeat2.id.get)
      }
    }
  }
}
