package com.eventdynamic.transactions

import com.eventdynamic.models.TransactionType
import com.eventdynamic.test.{DatabaseSuite, RandomUtil, ServiceHolder}
import org.scalatest.AsyncWordSpec

class IntegrationStatsTransactionSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val services = new ServiceHolder(ctx)

  val integrationStatsTransaction =
    new IntegrationStatsTransaction(ctx, services.event, services.eventSeat)

  "IntegrationStatsTransaction" should {
    "calculate integration stats for a season" in {
      val COUNT = 20

      for {
        integration <- services.integration.getById(1).map(_.get)
        integration2 <- services.integration.getById(2).map(_.get)
        client <- r.client()
        anotherClient <- r.client()
        _ <- services.clientIntegration.create(client.id.get, integration.id.get, true, true)
        _ <- services.clientIntegration.create(client.id.get, integration2.id.get, true, true)
        season <- r.season()
        season2 <- r.season()
        event <- r.event(seasonId = season.id, clientId = client.id.get)
        event2 <- r.event(seasonId = season.id, clientId = client.id.get)
        event3 <- r.event(seasonId = season2.id, clientId = anotherClient.id.get)
        es <- r.some(_ => r.eventSeat(eventId = event.id.get), COUNT)
        es2 <- r.some(_ => r.eventSeat(eventId = event2.id.get), COUNT)
        es3 <- r.some(_ => r.eventSeat(eventId = event3.id.get), COUNT)
        _ <- r.some(
          _ =>
            r.transaction(
              price = 5,
              integrationId = integration.id.get,
              eventSeatId = es.head.id.get
          )
        )
        _ <- r.some(
          _ =>
            r.transaction(
              price = -5,
              transactionType = TransactionType.Return,
              integrationId = integration.id.get,
              eventSeatId = es2.head.id.get
          ),
          5
        )
        _ <- r.some(
          _ =>
            r.transaction(
              price = 5,
              integrationId = integration.id.get,
              eventSeatId = es3.head.id.get
          )
        )
        _ <- services.materializedView.refreshEventIntegrationGroups()
        totals <- integrationStatsTransaction.getIntegrationTotals(client.id.get, season.id, None)
      } yield {
        assert(totals.length === 2)
        assert(totals.find(_.id === integration.id.get).get.sold === 5)
        assert(totals.find(_.id === integration.id.get).get.total === 5)
        assert(totals.find(_.id === integration2.id.get).get.sold === 0)
        assert(totals.find(_.id === integration2.id.get).get.total === 5)
      }
    }

    "calculate integration stats for an event" in {
      val COUNT = 20

      for {
        integration <- services.integration.getById(1).map(_.get)
        client <- r.client()
        anotherClient <- r.client()
        _ <- services.clientIntegration.create(client.id.get, integration.id.get, true, true)
        season <- r.season()
        event <- r.event(seasonId = season.id, clientId = client.id.get)
        event2 <- r.event(seasonId = season.id, clientId = anotherClient.id.get)
        es <- r.some(_ => r.eventSeat(eventId = event.id.get), COUNT)
        es2 <- r.some(_ => r.eventSeat(eventId = event2.id.get), COUNT)
        _ <- r.some(
          _ =>
            r.transaction(
              price = 5,
              integrationId = integration.id.get,
              eventSeatId = es.head.id.get
          )
        )
        _ <- r.some(
          _ =>
            r.transaction(
              price = 5,
              integrationId = integration.id.get,
              eventSeatId = es2.head.id.get
          )
        )
        _ <- services.materializedView.refreshEventIntegrationGroups()
        totals <- integrationStatsTransaction.getIntegrationTotals(
          client.id.get,
          season.id,
          event.id
        )
      } yield {
        assert(totals.head.sold == 10)
        assert(totals.head.total == 10)
      }
    }
  }
}
