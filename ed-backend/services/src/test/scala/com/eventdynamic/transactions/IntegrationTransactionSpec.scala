package com.eventdynamic.transactions

import com.eventdynamic.services.{
  ClientIntegrationEventSeatsService,
  ClientIntegrationEventsService,
  ClientIntegrationListingsService
}
import com.eventdynamic.test.{DatabaseSuite, RandomUtil, ServiceHolder}
import org.scalatest.AsyncWordSpec

class IntegrationTransactionSpec extends AsyncWordSpec with DatabaseSuite {
  "IntegrationTransaction" should {
    val serviceHolder = new ServiceHolder(ctx)
    val r = new RandomUtil(ctx)
    val clientIntegrationEventsService = new ClientIntegrationEventsService(ctx)
    val clientIntegrationListingsService = new ClientIntegrationListingsService(ctx)
    val clientIntegrationEventSeatsService = new ClientIntegrationEventSeatsService(ctx)
    val integrationTransactions =
      new IntegrationTransactions(
        ctx,
        serviceHolder.clientIntegration,
        clientIntegrationEventsService,
        clientIntegrationEventSeatsService,
        clientIntegrationListingsService,
        serviceHolder.event,
        serviceHolder.eventSeat,
        serviceHolder.integration,
        serviceHolder.seat
      )

    def create() = {
      for {
        client <- r.client()
        integration <- serviceHolder.integration.getById(1).map(_.get)
        clientIntegration <- serviceHolder.clientIntegration.create(
          client.id.get,
          integration.id.get,
          true,
          true
        )
        season <- r.season(clientId = client.id.get)
        event <- r.event(seasonId = season.id)
        eventSeat <- r.eventSeat()
        clientIntegrationEvent <- clientIntegrationEventsService.create(
          clientIntegration.id.get,
          event.id.get,
          r.string()
        )
        clientIntegrationListing <- clientIntegrationListingsService.create(
          clientIntegrationEvent.id.get,
          r.string()
        )
        clientIntegrationEventSeat <- clientIntegrationEventSeatsService.create(
          clientIntegrationListing.id.get,
          eventSeat.id.get
        )
      } yield {
        (clientIntegrationEventSeat, clientIntegration, event.seasonId.get)
      }
    }

    def createListing() = {
      for {
        client <- r.client()
        integration <- serviceHolder.integration.getById(1).map(_.get)
        clientIntegration <- serviceHolder.clientIntegration.create(
          client.id.get,
          integration.id.get,
          true,
          true
        )
        season <- r.season(clientId = client.id.get)
        event <- r.event(seasonId = season.id)
        eventSeat <- r.eventSeat()
        clientIntegrationEvent <- clientIntegrationEventsService.create(
          clientIntegration.id.get,
          event.id.get,
          r.string()
        )
        clientIntegrationListing <- clientIntegrationListingsService.create(
          clientIntegrationEvent.id.get,
          r.string()
        )
      } yield {
        (clientIntegrationListing, clientIntegration, event.seasonId.get)
      }
    }

    "get Integration Event Id Mapping" in {
      for {
        list <- create()
      } yield {
        val eventIds = integrationTransactions.getIntegrationEventIds(list._2.id.get, Some(list._3))
        assert(eventIds.isInstanceOf[Map[Int, String]])
      }
    }

    "get Client Integration Event Id Mapping" in {
      for {
        list <- create()
      } yield {
        val eventIds =
          integrationTransactions.getClientIntegrationEventIds(list._2.id.get, Some(list._3))
        assert(eventIds.isInstanceOf[Map[Int, Int]])
      }
    }

    "get Client Event Seat Listings from client integration from ticket ids" in {
      for {
        list <- create()
        clientEventSeatListings <- integrationTransactions.getClientEventSeatListing(
          Set(list._1.eventSeatId),
          list._2.id.get
        )
      } yield {
        assert(clientEventSeatListings.isInstanceOf[Seq[ClientIntegrationTicket]])
        assert(clientEventSeatListings.length === 1)
      }
    }

    "remove client integration event seats by id" in {
      for {
        eventSeat1 <- create()
        eventSeat2 <- create()
        numDeleted <- integrationTransactions.deleteClientIntegrationEventSeats(
          Seq(eventSeat1._1.id.get, eventSeat2._1.id.get)
        )
      } yield {
        assert(numDeleted === 2)
      }
    }

    "remove listings from client integration listings" in {
      for {
        listing <- createListing()
        numDeleted <- integrationTransactions.deleteListing(listing._1.id.get)
      } yield {
        assert(numDeleted === 1)
      }
    }

  }
}
