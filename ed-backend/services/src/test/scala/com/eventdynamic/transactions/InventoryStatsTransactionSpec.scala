package com.eventdynamic.transactions

import java.sql.Timestamp

import com.eventdynamic.test.{DatabaseSuite, RandomUtil, ServiceHolder}
import org.scalatest.AsyncWordSpec

import scala.concurrent.Future

class InventoryStatsTransactionSpec extends AsyncWordSpec with DatabaseSuite {
  "InventoryStatsTransaction" should {
    val serviceHolder = new ServiceHolder(ctx)
    val materializedView = serviceHolder.materializedView
    val inventoryStats = new InventoryStatsTransaction(ctx)
    val r = new RandomUtil(ctx)

    "get by event id" in {
      val start = Timestamp.valueOf("2018-01-01 00:00:00")

      val price: BigDecimal = 19.99
      val nTransactions = 5

      for {
        event <- r.event()
        revenueCategorySingleGameId <- r.revenueCategory("Single Game").map(_.id)
        revenueCategoryGroupId <- r.revenueCategory("Group").map(_.id)
        eventSeats <- r.some(_ => r.eventSeat(eventId = event.id.get), nTransactions)
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = start,
                  revenueCategoryId = revenueCategorySingleGameId
              )
            )
        )
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = start,
                  revenueCategoryId = revenueCategoryGroupId
              )
            )
        )
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = start,
                  revenueCategoryId = None // Should not be counted
              )
            )
        )
        otherEvent <- r.event()
        otherEventSeats <- r.some(_ => r.eventSeat(eventId = otherEvent.id.get), nTransactions)
        _ <- Future.sequence(
          otherEventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = start,
                  revenueCategoryId = revenueCategorySingleGameId
              )
            )
        )
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        stats <- inventoryStats.getInventory(event.clientId, event.id)
      } yield {
        assert(stats.isDefined)
        assert(stats.get.total === 5)
        assert(stats.get.soldIndividual === 15)
        assert(stats.get.soldOther === 5)
        assert(stats.get.revenue === 199.90)
      }
    }

    "get by list of event ids" in {
      val start = Timestamp.valueOf("2018-01-01 00:00:00")

      val price: BigDecimal = 19.99
      val nTransactions = 5

      for {
        event <- r.event()
        revenueCategorySingleGameId <- r.revenueCategory("Single Game").map(_.id)
        revenueCategoryGroupId <- r.revenueCategory("Group").map(_.id)
        eventSeats <- r.some(_ => r.eventSeat(eventId = event.id.get), nTransactions)
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = start,
                  revenueCategoryId = revenueCategorySingleGameId
              )
            )
        )
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = start,
                  revenueCategoryId = revenueCategoryGroupId
              )
            )
        )
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = start,
                  revenueCategoryId = None // Should not be counted
              )
            )
        )
        otherEvent <- r.event()
        otherEventSeats <- r.some(_ => r.eventSeat(eventId = otherEvent.id.get), nTransactions)
        _ <- Future.sequence(
          otherEventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = start,
                  revenueCategoryId = revenueCategorySingleGameId
              )
            )
        )
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        statsSeq <- inventoryStats.getInventoryForEvents(event.clientId, Seq(event.id.get), None)
      } yield {
        assert(statsSeq.size === 1)
        val stats = statsSeq.headOption
        assert(stats.isDefined)
        assert(stats.get.total === 5)
        assert(stats.get.soldIndividual === 15)
        assert(stats.get.soldOther === 5)
        assert(stats.get.revenue === 199.90)
      }
    }

    "get by season id" in {
      val start = Timestamp.valueOf("2018-01-01 00:00:00")

      val price: BigDecimal = 19.99
      val nTransactions = 5

      for {
        season <- r.season()
        event <- r.event(seasonId = season.id)
        revenueCategorySingleGameId <- r.revenueCategory("Single Game").map(_.id)
        revenueCategoryGroupId <- r.revenueCategory("Group").map(_.id)
        eventSeats <- r.some(_ => r.eventSeat(eventId = event.id.get), nTransactions)
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = start,
                  revenueCategoryId = revenueCategorySingleGameId
              )
            )
        )
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = start,
                  revenueCategoryId = revenueCategoryGroupId
              )
            )
        )
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = start,
                  revenueCategoryId = None // Should not be counted
              )
            )
        )
        otherEvent <- r.event(clientId = event.clientId, seasonId = season.id)
        _ <- r.some(_ => r.eventSeat(eventId = otherEvent.id.get), nTransactions)
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        stats <- inventoryStats.getInventory(event.clientId, None, season.id)
      } yield {
        assert(stats.isDefined)
        assert(stats.get.total === 10)
        assert(stats.get.soldIndividual === 15)
        assert(stats.get.soldOther === 5)
        assert(stats.get.revenue === 199.90)
      }
    }
  }
}
