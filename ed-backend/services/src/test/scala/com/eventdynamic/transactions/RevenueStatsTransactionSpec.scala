package com.eventdynamic.transactions

import com.eventdynamic.models.TransactionType
import com.eventdynamic.test.{DatabaseSuite, RandomUtil, ServiceHolder}
import org.scalatest.AsyncWordSpec

import scala.concurrent.Future

class RevenueStatsTransactionSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val services = new ServiceHolder(ctx)

  val revenueStatsTransaction = new RevenueStatsTransaction(
    ctx,
    services.event,
    services.eventSeat,
    services.revenueCategory,
    services.transaction
  )

  "RevenueStatsTransaction" should {
    "get revenue by category matching an event ID" in {
      val nSeats = 12
      val soldPrice: BigDecimal = 19.99

      for {
        event <- r.event()
        eventSeats <- r.some(_ => r.eventSeat(eventId = event.id.get), nSeats)
        rc1 <- r.revenueCategory(name = "ADULT")
        rc2 <- r.revenueCategory(name = "PACKAGE")
        rc3 <- r.revenueCategory(name = "SEASON")
        _ <- r.revenueCategoryMapping(event.clientId, rc1.id.get, "adult")
        _ <- r.revenueCategoryMapping(event.clientId, rc2.id.get, "package")
        _ <- r.revenueCategoryMapping(event.clientId, rc3.id.get, "season")
        _ <- Future.sequence(
          eventSeats
            .slice(0, 4)
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = soldPrice,
                  revenueCategoryId = rc1.id
              )
            )
        )
        _ <- Future.sequence(
          eventSeats
            .slice(4, 8)
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = soldPrice,
                  revenueCategoryId = rc2.id
              )
            )
        )
        _ <- Future.sequence(
          eventSeats
            .slice(8, 12)
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = soldPrice,
                  revenueCategoryId = rc3.id
              )
            )
        )
        _ <- services.materializedView.refreshEventRevenueGroups()
        rev <- revenueStatsTransaction.getRevenueByCategory(event.clientId, eventId = event.id)
      } yield {
        assert(eventSeats.length == nSeats)
        assert(rev.length == 3)
        assert(rev.map(_.name).sorted == Seq("ADULT", "PACKAGE", "SEASON"))
        assert(rev.forall(_.revenue == 4 * soldPrice))
        assert(rev.forall(_.ticketsSold == 4))
      }
    }

    "get revenue by category where some categories have not sold any tickets matching an eventId" in {
      val nSeats = 4
      val soldPrice: BigDecimal = 19.99

      for {
        event <- r.event()
        eventSeats <- r.some(_ => r.eventSeat(eventId = event.id.get), nSeats)
        rc1 <- r.revenueCategory(name = "ADULT")
        _ <- r.revenueCategoryMapping(event.clientId, rc1.id.get, "adult")
        rc2 <- r.revenueCategory(name = "CHILD")
        _ <- r.revenueCategoryMapping(event.clientId, rc2.id.get, "child")
        _ <- Future.sequence(
          eventSeats.map(
            es =>
              r.transaction(eventSeatId = es.id.get, price = soldPrice, revenueCategoryId = rc1.id)
          )
        )
        _ <- services.materializedView.refreshEventRevenueGroups()
        rev <- revenueStatsTransaction.getRevenueByCategory(event.clientId, eventId = event.id)
      } yield {
        val adult = rev.find(_.name == "ADULT")
        assert(adult.exists(_.revenue == nSeats * soldPrice))
        assert(adult.exists(_.ticketsSold == nSeats))

        val child = rev.find(_.name == "CHILD")
        assert(child.exists(_.revenue == 0))
        assert(child.exists(_.ticketsSold == 0))

        assert(rev.length == 2)
      }
    }

    "get revenue by category matching a client ID" in {
      val nSeats = 4
      val soldPrice: BigDecimal = 19.99

      for {
        event1 <- r.event()
        event2 <- r.event()
        eventSeats <- r.some(_ => r.eventSeat(eventId = event1.id.get), nSeats)
        rc1 <- r.revenueCategory(name = "ADULT")
        _ <- r.revenueCategoryMapping(event1.clientId, rc1.id.get, "adult")
        rc2 <- r.revenueCategory(name = "CHILD")
        _ <- r.revenueCategoryMapping(event2.clientId, rc2.id.get, "child")
        _ <- Future.sequence(
          eventSeats.map(
            es1 =>
              r.transaction(eventSeatId = es1.id.get, price = soldPrice, revenueCategoryId = rc1.id)
          )
        )
        _ <- services.materializedView.refreshEventRevenueGroups()
        rev <- revenueStatsTransaction.getRevenueByCategory(event1.clientId, eventId = event1.id)
      } yield {
        val adult = rev.find(_.name == "ADULT")
        assert(adult.exists(_.revenue == nSeats * soldPrice))
        assert(adult.exists(_.ticketsSold == nSeats))

        assert(rev.length == 1)
      }
    }

    "get revenue by category matching a season ID" in {
      val nSeats = 4
      val nEvents = 2
      val soldPrice: BigDecimal = 19.99

      for {
        client <- r.client()
        season <- r.season(name = Some("mySeason"))
        seats <- r.some(_ => r.seat(), nSeats)
        seasonEvents <- r.some(
          _ => r.event(seasonId = season.id, clientId = client.id.get),
          nEvents
        )
        noSeasonEvents <- r.some(_ => r.event(clientId = client.id.get), nEvents)
        adultCategory <- r.revenueCategory(name = "ADULT")
        _ <- r.revenueCategoryMapping(client.id.get, adultCategory.id.get, "adult")
        otherCategory <- r.revenueCategory(name = "OTHER")
        _ <- r.revenueCategoryMapping(client.id.get, otherCategory.id.get, "other")

        venueId <- r.venue()
        priceScaleId <- services.priceScales.create(name = r.string(), venueId, r.int())

        seasonEventSeats <- r.eventSeats(seats, seasonEvents, priceScaleId)
        noSeasonEventSeats <- r.eventSeats(seats, noSeasonEvents, priceScaleId)
        _ <- Future.sequence(
          seasonEventSeats.map(
            es =>
              r.transaction(
                eventSeatId = es.id.get,
                price = soldPrice,
                revenueCategoryId = adultCategory.id
            )
          )
        )
        _ <- Future.sequence(
          noSeasonEventSeats.map(
            es =>
              r.transaction(
                eventSeatId = es.id.get,
                price = soldPrice,
                revenueCategoryId = otherCategory.id
            )
          )
        )
        _ <- services.materializedView.refreshEventRevenueGroups()
        rev <- revenueStatsTransaction.getRevenueByCategory(client.id.get, seasonId = season.id)
      } yield {
        val adult = rev.find(_.name == "ADULT")
        assert(adult.exists(_.revenue == nSeats * nEvents * soldPrice))
        assert(adult.exists(_.ticketsSold == nSeats * nEvents))

        val other = rev.find(_.name == "OTHER")
        assert(other.exists(_.revenue == 0))
        assert(other.exists(_.ticketsSold == 0))

        assert(rev.length == 2)
      }
    }

    "get revenue negate return orders" in {
      val nSeats = 12
      val soldPrice: BigDecimal = 19.99
      val returnSeats = 4

      for {
        event <- r.event()
        eventSeats <- r.some(_ => r.eventSeat(eventId = event.id.get), nSeats)
        category <- r.revenueCategory("ADULT")
        _ <- r.revenueCategoryMapping(event.clientId, category.id.get, "adult")
        _ <- Future.sequence(
          eventSeats.map(
            es =>
              r.transaction(
                eventSeatId = es.id.get,
                price = soldPrice,
                revenueCategoryId = category.id
            )
          )
        )
        _ <- Future.sequence(
          eventSeats
            .slice(0, returnSeats)
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = soldPrice * -1,
                  transactionType = TransactionType.Return,
                  revenueCategoryId = category.id
              )
            )
        )
        _ <- services.materializedView.refreshEventRevenueGroups()
        rev <- revenueStatsTransaction.getRevenueByCategory(event.clientId, eventId = event.id)
      } yield {
        assert(rev.length == 1)

        val adult = rev.find(_.name == "ADULT")
        assert(adult.exists(_.ticketsSold == 8))
        assert(adult.exists(_.revenue == 159.92))
      }
    }

    "handle eventseats with multiple transactions" in {
      val initialPrice: BigDecimal = 19.99
      val finalPrice: BigDecimal = 25.5

      for {
        e <- r.event()
        es <- r.eventSeat(eventId = e.id.get)
        _ <- r.eventSeat(eventId = e.id.get)
        category <- r.revenueCategory(name = "ADULT")
        _ <- r.revenueCategoryMapping(e.clientId, category.id.get, "adult")
        _ <- r.transaction(
          price = initialPrice,
          eventSeatId = es.id.get,
          revenueCategoryId = category.id
        )
        _ <- r.transaction(
          price = initialPrice * -1,
          transactionType = TransactionType.Return,
          eventSeatId = es.id.get,
          revenueCategoryId = category.id
        )
        _ <- r.transaction(
          price = finalPrice,
          eventSeatId = es.id.get,
          revenueCategoryId = category.id
        )
        _ <- services.materializedView.refreshEventRevenueGroups()
        rev <- revenueStatsTransaction.getRevenueByCategory(clientId = e.clientId, eventId = e.id)
      } yield {
        val adult = rev.find(_.name == "ADULT")
        assert(adult.exists(_.revenue == finalPrice))
        assert(adult.exists(_.ticketsSold == 1))
      }
    }
  }
}
