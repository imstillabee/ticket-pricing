package com.eventdynamic.transactions

import java.sql.Timestamp

import com.eventdynamic.services.{EventSeatService, EventService, TransactionService}
import com.eventdynamic.test.{DatabaseSuite, RandomUtil}
import com.eventdynamic.utils.DateHelper
import org.scalatest.AsyncWordSpec

class SeasonTransactionSpec extends AsyncWordSpec with DatabaseSuite {
  val r = new RandomUtil(ctx)
  val eventService = new EventService(ctx)
  val eventSeatService = new EventSeatService(ctx)
  val transactionService = new TransactionService(ctx)

  val seasonTransaction =
    new SeasonTransaction(ctx, eventService, eventSeatService, transactionService)

  "SeasonTransaction" should {
    "determine startDate and endDate for a season" in {
      val nTransactions = 3
      val early: Timestamp = DateHelper.now()
      val later: Timestamp = DateHelper.later(1440)
      val latest: Timestamp = DateHelper.later(2880)
      val price: BigDecimal = 50.0
      for {
        // create a season
        season <- r.season()
        // need multiple events, so we can use the timestamp of the latest one for endDate
        event1 <- r.event(seasonId = Some(season.id.get), timestamp = Some(later))
        event2 <- r.event(seasonId = Some(season.id.get), timestamp = Some(later))
        event3 <- r.event(seasonId = Some(season.id.get), timestamp = Some(latest))
        // we can make do with one seat, and sell it multiple times
        seat <- r.seat()
        // we'll create three eventSeats, one for each of our three events
        eventSeat1 <- r.eventSeat(seat.id.get, event1.id.get)
        eventSeat2 <- r.eventSeat(seat.id.get, event2.id.get)
        eventSeat3 <- r.eventSeat(seat.id.get, event3.id.get)
        // we'll make at least 2 transactions for season1
        trans1 <- r.transaction(early, price, eventSeat1.id.get, 1)
        trans2 <- r.transaction(later, price, eventSeat1.id.get, 1)
        trans3 <- r.transaction(later, price, eventSeat1.id.get, 1)

        startDate <- seasonTransaction.getEarliestTransactionDateBySeason(season.id.get)
        endDate <- seasonTransaction.getLastEventBySeason(season.id.get)
      } yield {
        assert(startDate.get === early)
        assert(endDate.get === latest)
      }
    }
  }
}
