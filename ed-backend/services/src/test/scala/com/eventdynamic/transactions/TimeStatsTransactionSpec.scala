package com.eventdynamic.transactions

import java.sql.Timestamp
import java.time._
import java.time.temporal.ChronoUnit
import java.util.concurrent.TimeUnit

import com.eventdynamic.models.TransactionType
import com.eventdynamic.services.{
  EventService,
  EventTimeGroupsService,
  ProjectionService,
  VenueService
}
import com.eventdynamic.test.{DatabaseSuite, RandomUtil, ServiceHolder}
import com.eventdynamic.utils.RandomModelUtil
import org.scalatest.{AsyncWordSpec, PrivateMethodTester}

import scala.concurrent.Future

class TimeStatsTransactionSpec extends AsyncWordSpec with DatabaseSuite with PrivateMethodTester {
  val r = new RandomUtil(ctx)
  val services = new ServiceHolder(ctx)
  val inventoryStatsTransaction = new InventoryStatsTransaction(ctx)
  val eventService = new EventService(ctx)
  val eventTimeGroupsService = new EventTimeGroupsService(ctx)
  val projectionService = new ProjectionService(ctx)
  val venueService = new VenueService(ctx)

  val timeStatsTransaction =
    new TimeStatsTransaction(
      ctx,
      inventoryStatsTransaction,
      eventService,
      eventTimeGroupsService,
      projectionService,
      venueService
    )
  private val materializedView = services.materializedView
  private val tz = ZoneId.of("America/Los_Angeles")

  "TimeStatTransactionSpec#get" should {
    "use an interval of 1 day when start and end date are at least 15 days apart" in {
      val start = LocalDate.parse("2018-02-01")
      val end = LocalDate.parse("2018-02-15")

      for {
        venueId <- r.venue(timeZone = tz.toString)
        event <- r.event(venueId = venueId)
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        stats <- timeStatsTransaction.get(start, end, clientId = event.clientId, eventId = event.id)
      } yield {
        assert(stats.meta.interval == "Days")
        assert(stats.data.length == 15)
        assert(stats.data.head.intervalStart.toInstant == start.atStartOfDay(tz).toInstant)
        assert(
          stats.data.forall(
            stat =>
              ChronoUnit.DAYS
                .between(stat.intervalStart.toInstant, stat.intervalEnd.toInstant) == 1
          )
        )
      }
    }

    "use an interval of 1 hour when start and end date are at most 14 days apart" in {
      val start = LocalDate.parse("2018-01-01")
      val end = LocalDate.parse("2018-01-14")
      val expectedInterval = TimeUnit.HOURS.toMillis(1)

      for {
        venueId <- r.venue(timeZone = tz.toString)
        event <- r.event(venueId = venueId)
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        stats <- timeStatsTransaction.get(start, end, event.clientId, event.id, None)
      } yield {
        assert(stats.data.head.intervalStart.toInstant == start.atStartOfDay(tz).toInstant)
        assert(stats.meta.interval == "Hours")
        assert(stats.data.length == 14 * 24)
        assert(
          stats.data.forall(
            stat => stat.intervalEnd.getTime - stat.intervalStart.getTime == expectedInterval
          )
        )
      }
    }

    "only return projections if the interval is Days" in {
      val start = LocalDate.parse("2018-01-01")
      val now = LocalDate.parse("2018-01-08").atStartOfDay(tz).toInstant

      for {
        venueId <- r.venue(timeZone = tz.toString)
        event <- r.event(venueId = venueId)
        hourlyStats <- timeStatsTransaction.get(
          start,
          end = LocalDate.parse("2018-01-14"),
          clientId = event.clientId,
          eventId = event.id,
          now = now
        )
        dailyStats <- timeStatsTransaction.get(
          start,
          end = LocalDate.parse("2018-01-15"),
          clientId = event.clientId,
          eventId = event.id,
          now = now
        )
      } yield {
        assert(hourlyStats.meta.interval == "Hours")
        assert(hourlyStats.data.length == 14 * 24)
        assert(hourlyStats.data.forall(!_.isProjected))

        assert(dailyStats.meta.interval == "Days")
        assert(dailyStats.data.length == 15)
        assert(dailyStats.data.exists(_.isProjected))
      }
    }

    "return all actual data when the date range is in the past" in {
      val start = LocalDate.parse("2018-01-01")
      val end = start.plus(27, ChronoUnit.DAYS)
      val now = end.plus(2, ChronoUnit.DAYS).atStartOfDay(tz).toInstant

      for {
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        event <- r.event()
        stats <- timeStatsTransaction.get(
          start,
          end,
          clientId = event.clientId,
          eventId = event.id,
          now = now
        )
      } yield {
        assert(stats.data.size == 28)
        assert(stats.data.forall(_.isProjected == false))
      }
    }

    "return all projected data when the date range is in the future" in {
      val start = LocalDate.parse("2018-01-01")
      val end = start.plus(27, ChronoUnit.DAYS)
      val now = start.minus(2, ChronoUnit.DAYS).atStartOfDay(tz).toInstant

      for {
        venueId <- r.venue(timeZone = tz.toString)
        event <- r.event(venueId = venueId)
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        stats <- timeStatsTransaction.get(
          start,
          end,
          clientId = event.clientId,
          eventId = event.id,
          now = now
        )
      } yield {
        assert(stats.data.size == 28)
        assert(stats.data.forall(_.isProjected))
      }
    }

    "use a mix of actual and projected data when the date range starts in the past and ends in the future" in {
      val start = LocalDate.parse("2018-01-01")
      val end = LocalDate.parse("2018-01-28")
      val now = LocalDate.parse("2018-01-08").atStartOfDay(tz).toInstant

      for {
        venueId <- r.venue(timeZone = tz.toString)
        event <- r.event(venueId = venueId)
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        stats <- timeStatsTransaction.get(
          start,
          end,
          clientId = event.clientId,
          eventId = event.id,
          now = now
        )
      } yield {
        assert(stats.data.size == 28)
        assert(stats.data.exists(_.isProjected))
        assert(stats.data.exists(!_.isProjected))

        val (projected, actual) = stats.data.partition(_.isProjected)

        // If an interval is partially in the future, use the actual value
        val timeStatForNow = stats.data.find(
          s => s.intervalStart.toInstant.compareTo(now) <= 0 && s.intervalEnd.toInstant.isAfter(now)
        )
        assert(timeStatForNow.isDefined && !timeStatForNow.get.isProjected)

        assert(actual.size == 8)
        assert(projected.size == 20)
      }
    }

    "return stats for the event when passed an event ID" in {
      val start = LocalDate.parse("2018-01-01")
      val end = LocalDate.parse("2018-01-01")

      val price: BigDecimal = 19.99
      val nTransactions = 5

      for {
        priceScaleId <- r.priceScaleId()
        venueId <- r.venue(timeZone = tz.toString)
        event <- r.event(venueId = venueId)
        revenueCategoryId <- r.revenueCategory("Single Game").map(_.id)
        eventSeats <- r.some(
          _ => r.eventSeat(eventId = event.id.get, priceScaleId = priceScaleId),
          nTransactions
        )
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = Timestamp.from(start.atStartOfDay(tz).toInstant),
                  revenueCategoryId = revenueCategoryId
              )
            )
        )
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = Timestamp.from(start.atStartOfDay(tz).toInstant),
                  revenueCategoryId = None // Should not be counted
              )
            )
        )
        otherEvent <- r.event()
        otherEventSeats <- r.some(_ => r.eventSeat(eventId = otherEvent.id.get), nTransactions)
        _ <- Future.sequence(
          otherEventSeats
            .map(
              es =>
                r.transaction(
                  eventSeatId = es.id.get,
                  price = price,
                  time = Timestamp.from(start.atStartOfDay(tz).toInstant),
                  revenueCategoryId = revenueCategoryId
              )
            )
        )
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        stats <- timeStatsTransaction.get(start, end, event.clientId, event.id, None)
      } yield {
        val head = stats.data.head
        assert(head.revenue.get == nTransactions * price)
        assert(head.inventory.get == -nTransactions)
        assert(head.cumulativeRevenue.get == nTransactions * price)
        assert(head.cumulativeInventory.get == 0)

        val rest = stats.data.slice(1, stats.data.length)
        assert(rest.forall(_.revenue.get == 0))
        assert(rest.forall(_.inventory.get == 0))
        assert(rest.forall(_.cumulativeRevenue.get == nTransactions * price))
        assert(rest.forall(_.cumulativeInventory.get == 0))
      }
    }

    "return stats for the season when passed a season ID" in {
      val start = LocalDate.parse("2018-01-01")
      val end = LocalDate.parse("2018-01-01")

      val price: BigDecimal = 19.99
      val nEventsPerSeason = 2

      for {
        priceScaleId <- r.priceScaleId()
        client <- r.client()
        revenueCategoryId <- r.revenueCategory("Single Game").map(_.id)
        season <- r.season()
        venueId <- r.venue(timeZone = tz.toString)
        events <- r.some(
          _ => r.event(clientId = client.id.get, seasonId = season.id, venueId = venueId),
          nEventsPerSeason
        )
        eventSeats <- Future.sequence(
          events.map(e => r.eventSeat(eventId = e.id.get, priceScaleId = priceScaleId))
        )
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  time = Timestamp.from(start.atStartOfDay(tz).toInstant),
                  price = price,
                  eventSeatId = es.id.get,
                  revenueCategoryId = revenueCategoryId
              )
            )
        )
        _ <- Future.sequence(
          eventSeats
            .map(
              es =>
                r.transaction(
                  time = Timestamp.from(start.atStartOfDay(tz).toInstant),
                  price = price,
                  eventSeatId = es.id.get,
                  revenueCategoryId = None // Should not be counted
              )
            )
        )

        otherSeason <- r.season()
        otherEvents <- r.some(
          _ => r.event(clientId = client.id.get, seasonId = otherSeason.id),
          nEventsPerSeason
        )
        otherEventSeats <- Future.sequence(otherEvents.map(e => r.eventSeat(eventId = e.id.get)))
        _ <- Future.sequence(
          otherEventSeats
            .map(
              es =>
                r.transaction(
                  time = Timestamp.from(start.atStartOfDay(tz).toInstant),
                  price = price,
                  eventSeatId = es.id.get,
                  revenueCategoryId = revenueCategoryId
              )
            )
        )

        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        stats <- timeStatsTransaction.get(start, end, client.id.get, None, season.id)
      } yield {
        val head = stats.data.head
        assert(head.revenue.get == nEventsPerSeason * price)
        assert(head.inventory.get == -nEventsPerSeason)
        assert(head.cumulativeRevenue.get == nEventsPerSeason * price)
        assert(head.cumulativeInventory.get == 0)

        val rest = stats.data.slice(1, stats.data.length)
        assert(rest.forall(_.revenue.get == 0))
        assert(rest.forall(_.inventory.get == 0))
        assert(rest.forall(_.cumulativeRevenue.get == nEventsPerSeason * price))
        assert(rest.forall(_.cumulativeInventory.get == 0))
      }
    }

    "handle returns" in {
      val start = LocalDate.parse("2018-01-01")
      val end = LocalDate.parse("2018-01-01")

      val nEventSeats = 10
      val nReturned = 5
      val price: BigDecimal = 19.99
      val tz = "America/New_York"

      for {
        priceScaleId <- r.priceScaleId()
        venueId <- r.venue(timeZone = tz)
        event <- r.event(venueId = venueId)
        revenueCategoryId <- r.revenueCategory("Single Game").map(_.id)
        eventSeats <- r.some(
          _ => r.eventSeat(eventId = event.id.get, priceScaleId = priceScaleId),
          nEventSeats
        )

        // All tickets are bought
        _ <- Future.sequence(
          eventSeats.map(
            es =>
              r.transaction(
                price = price,
                eventSeatId = es.id.get,
                time = Timestamp
                  .from(LocalDateTime.parse("2018-01-01T01:30:00").atZone(ZoneId.of(tz)).toInstant),
                revenueCategoryId = revenueCategoryId
            )
          )
        )

        // Some tickets are returned
        _ <- Future.sequence(
          eventSeats
            .slice(0, nReturned)
            .map(
              es =>
                r.transaction(
                  price = -price,
                  transactionType = TransactionType.Return,
                  eventSeatId = es.id.get,
                  time = Timestamp.from(
                    LocalDateTime.parse("2018-01-01T02:30:00").atZone(ZoneId.of(tz)).toInstant
                  ),
                  revenueCategoryId = revenueCategoryId
              )
            )
        )

        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        stats <- timeStatsTransaction.get(start, end, event.clientId, event.id, None)
      } yield {
        assert(stats.data.length == 24)
        assert(stats.data.forall(_.isProjected == false))

        // First interval, nothing has sold
        val first = stats.data.head
        assert(first.cumulativeInventory.contains(nEventSeats))
        assert(first.cumulativeRevenue.contains(0))
        assert(first.inventory.contains(0))
        assert(first.revenue.contains(0))

        // Second interval, all tickets have sold
        val second = stats.data(1)
        assert(second.cumulativeInventory.contains(0))
        assert(second.cumulativeRevenue.contains(nEventSeats * price))
        assert(second.inventory.contains(-nEventSeats))
        assert(second.revenue.contains(nEventSeats * price))

        // Third interval, some tickets have been returned
        val third = stats.data(2)
        assert(third.cumulativeInventory.contains(nEventSeats - nReturned))
        assert(third.cumulativeRevenue.contains((nEventSeats - nReturned) * price))
        assert(third.inventory.contains(nReturned))
        assert(third.revenue.contains(-nReturned * price))

        // Fourth interval, no new sales or returns
        val fourth = stats.data(3)
        assert(fourth.cumulativeInventory.contains(nEventSeats - nReturned))
        assert(fourth.cumulativeRevenue.contains((nEventSeats - nReturned) * price))
        assert(fourth.inventory.contains(0))
        assert(fourth.revenue.contains(0))
      }
    }

    "handle missing data actual and projected revenue and inventory" in {
      val start = LocalDate.parse("2018-01-01")
      val end = start.plus(27, ChronoUnit.DAYS)
      val now = start.plus(7, ChronoUnit.DAYS).atStartOfDay(tz).toInstant

      for {
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()
        venueId <- r.venue(timeZone = tz.toString)
        event <- r.event(venueId = venueId)
        stats <- timeStatsTransaction.get(
          start,
          end,
          clientId = event.clientId,
          eventId = event.id,
          now = now
        )
      } yield {
        assert(stats.data.size == 28)

        val projections = stats.data.filter(_.isProjected)
        assert(projections.nonEmpty)
        assert(projections.forall(_.inventory.isEmpty))
        assert(projections.forall(_.revenue.isEmpty))
        assert(projections.forall(_.cumulativeInventory.isEmpty))
        assert(projections.forall(_.cumulativeRevenue.isEmpty))

        val actual = stats.data.filter(!_.isProjected)
        assert(actual.nonEmpty)
        assert(actual.forall(_.inventory.contains(0)))
        assert(actual.forall(_.revenue.contains(BigDecimal(0))))
        assert(actual.forall(_.cumulativeInventory.contains(0)))
        assert(actual.forall(_.cumulativeRevenue.contains(BigDecimal(0))))
      }
    }

    "correctly calculate time stats for the time window" in {
      val start = LocalDate.parse("2018-02-01")
      val end = LocalDate.parse("2018-02-28")
      val now = end.plus(7, ChronoUnit.DAYS).atStartOfDay(tz).toInstant

      for {
        // Seed some transaction data
        venueId <- r.venue(timeZone = tz.toString)
        event <- r.event(venueId = venueId)
        revenueCategoryId <- r.revenueCategory("Single Game").map(_.id)
        _ <- r
          .some(
            _ => {
              for {
                es <- r.eventSeat(eventId = event.id.get)
                t <- r.transaction(
                  eventSeatId = es.id.get,
                  revenueCategoryId = revenueCategoryId,
                  time = Timestamp.from(
                    RandomModelUtil.instant(
                      start.atStartOfDay(tz).toInstant,
                      end.atStartOfDay(tz).plus(1, ChronoUnit.DAYS).toInstant
                    )
                  )
                )
              } yield t
            },
            100
          )

        // Refresh materialized views
        _ <- materializedView.refreshEventSeatCounts()
        _ <- materializedView.refreshEventTimeGroups()

        allStats <- timeStatsTransaction.get(
          start,
          end,
          clientId = event.clientId,
          eventId = event.id,
          now = now
        )
        someStats <- timeStatsTransaction.get(
          start.plus(7, ChronoUnit.DAYS),
          end,
          clientId = event.clientId,
          eventId = event.id,
          now = now
        )
      } yield {
        assert(someStats.data.size == 21)
        assert(allStats.data.size == 28)

        assert((someStats.data ++ allStats.data).forall(!_.isProjected))
        assert(someStats.data == allStats.data.takeRight(someStats.data.size))
      }
    }
  }
}
