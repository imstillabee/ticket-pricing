package com.eventdynamic.utils

import java.sql.Timestamp
import java.time.Instant
import java.util.{Calendar, TimeZone}

import org.scalatest.FlatSpec

class DateHelperSpec extends FlatSpec {
  "startOfHour" should "round down to the nearest hour" in {
    // Wed, 13 Mar 2019 14:47:43 GMT
    val ts = new Timestamp(1552488463592L)
    val result = DateHelper.startOfHour(ts)

    val calendar = Calendar.getInstance
    calendar.setTimeZone(TimeZone.getTimeZone("UTC"))
    calendar.setTime(result)

    assert(calendar.get(Calendar.HOUR_OF_DAY) == 14)
    assert(calendar.get(Calendar.MINUTE) == 0)
    assert(calendar.get(Calendar.SECOND) == 0)
    assert(calendar.get(Calendar.MILLISECOND) == 0)
  }

  "startOfNextHour" should "round up to the next hour" in {
    // Wed, 13 Mar 2019 14:47:43 GMT
    val ts = new Timestamp(1552488463592L)
    val result = DateHelper.endOfHour(ts)

    val calendar = Calendar.getInstance
    calendar.setTimeZone(TimeZone.getTimeZone("UTC"))
    calendar.setTime(result)

    assert(calendar.get(Calendar.HOUR_OF_DAY) == 14)
    assert(calendar.get(Calendar.MINUTE) == 59)
    assert(calendar.get(Calendar.SECOND) == 59)
    assert(calendar.get(Calendar.MILLISECOND) == 999)
  }

  "roundUpToEndOfDay" should "round down to the nearest hour" in {
    // Wed, 13 Mar 2019 14:47:43 GMT
    val ts = new Timestamp(1552488463592L)
    val result = DateHelper.endOfDay(ts)

    val calendar = Calendar.getInstance
    calendar.setTimeZone(TimeZone.getTimeZone("UTC"))
    calendar.setTime(result)

    assert(calendar.get(Calendar.HOUR_OF_DAY) == 23)
    assert(calendar.get(Calendar.MINUTE) == 59)
    assert(calendar.get(Calendar.SECOND) == 59)
    assert(calendar.get(Calendar.MILLISECOND) == 999)
  }
}
