package com.eventdynamic.utils

import org.scalatest.FlatSpec

class HoldCodeHelperSpec extends FlatSpec {
  "checkHoldStatus" should "return false if holdCodeType is 'O'" in {
    val result = HoldCodeHelper.checkIsHeld("O")
    assert(!result)
  }

  "checkHoldStatus" should "return true if holdCodeType is not 'O'" in {
    val result = HoldCodeHelper.checkIsHeld("H")
    assert(result)
  }
}
