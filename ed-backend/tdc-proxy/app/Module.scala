import java.time.Instant

import com.google.inject.AbstractModule
import com.tdcproxy.repositories.{BuyerTypeRepository, InventoryRepository, TransactionRepository}
import com.tdcproxy.util.random.Record
import org.mockito.Matchers._
import org.mockito.Mockito._
import play.api.{Configuration, Environment}

import scala.concurrent.Future

class Module(environment: Environment, configuration: Configuration) extends AbstractModule {
  override def configure(): Unit = {
    val fakeData = configuration.get[Boolean]("app.fakeData")

    if (fakeData) {
      doFakeBindings()
    }
  }

  def doFakeBindings(): Unit = {
    // Fake buyer type repository
    val buyerTypeRepository = mock(classOf[BuyerTypeRepository])
    when(buyerTypeRepository.getForEvents(any[Seq[Long]]))
      .thenAnswer(_ => Future.successful((0 to 100).map(_ => Record.buyerType()).toVector))
    bind(classOf[BuyerTypeRepository])
      .toInstance(buyerTypeRepository)

    // Fake transaction repository
    val transactionRepository = mock(classOf[TransactionRepository])
    when(
      transactionRepository
        .getTransactionsForEvents(any[Instant], any[Instant], any[Seq[Long]], any[Option[Int]])
    ).thenReturn(Future.successful(Vector()))
    bind(classOf[TransactionRepository])
      .toInstance(transactionRepository)

    val inventoryRepository = mock(classOf[InventoryRepository])
    when(
      inventoryRepository
        .getInventoryForEvents(any[Instant], any[Instant], any[Seq[Long]], any[Option[Int]])
    ).thenReturn(Future.successful(Vector()))
    bind(classOf[InventoryRepository])
      .toInstance(inventoryRepository)
  }
}
