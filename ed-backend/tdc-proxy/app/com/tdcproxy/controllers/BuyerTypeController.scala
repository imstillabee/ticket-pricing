package com.tdcproxy.controllers

import com.google.inject.{Inject, Singleton}
import com.tdcproxy.models.BuyerType
import com.tdcproxy.repositories.BuyerTypeRepository
import com.typesafe.config.Config
import play.api.cache._
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

@Singleton
class BuyerTypeController @Inject()(
  cc: ControllerComponents,
  cache: AsyncCacheApi,
  buyerTypeRepo: BuyerTypeRepository,
  config: Config
)(implicit ec: ExecutionContext)
    extends AbstractController(cc) {
  private val cacheLifetime = config.getInt("app.cacheLifetime")

  def list(eventId: Seq[Long]): Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      cache
        .getOrElseUpdate[Vector[BuyerType]](request.uri, cacheLifetime.seconds) {
          buyerTypeRepo.getForEvents(eventId)
        }
        .map(res => Ok(Json.toJson(res)))
  }
}
