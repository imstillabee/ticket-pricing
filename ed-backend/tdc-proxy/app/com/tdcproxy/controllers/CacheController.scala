package com.tdcproxy.controllers

import com.google.inject.{Inject, Singleton}
import play.api.cache.AsyncCacheApi
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext

@Singleton
class CacheController @Inject()(cc: ControllerComponents, cache: AsyncCacheApi)(
  implicit ec: ExecutionContext
) extends AbstractController(cc) {

  def delete(): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    cache
      .removeAll()
      .map(res => {
        Ok(Json.toJson(Map("message" -> res.toString)))
      })
  }
}
