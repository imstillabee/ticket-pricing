package com.tdcproxy.controllers

import com.google.inject.{Inject, Singleton}
import com.tdcproxy.models.DateRange
import com.tdcproxy.repositories.InventoryRepository
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext

@Singleton
class InventoryController @Inject()(cc: ControllerComponents, inventoryRepo: InventoryRepository)(
  implicit ec: ExecutionContext
) extends AbstractController(cc) {

  def list(dateRange: DateRange, eventId: Seq[Long], limit: Option[Int]): Action[AnyContent] =
    Action.async { implicit request: Request[AnyContent] =>
      inventoryRepo
        .getInventoryForEvents(
          afterDate = dateRange.after,
          beforeDate = dateRange.before,
          eventIds = eventId,
          limit = limit
        )
        .map(res => Ok(Json.toJson(res)))
    }
}
