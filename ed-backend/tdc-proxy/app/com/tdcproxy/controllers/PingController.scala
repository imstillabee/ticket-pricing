package com.tdcproxy.controllers

import com.google.inject.Singleton
import javax.inject.Inject
import org.slf4j.LoggerFactory
import play.api.mvc._

@Singleton
class PingController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
  private val logger = LoggerFactory.getLogger(this.getClass.toString)

  def index() = Action { implicit request: Request[AnyContent] =>
    logger.debug("ping")
    Ok("ok")
  }
}
