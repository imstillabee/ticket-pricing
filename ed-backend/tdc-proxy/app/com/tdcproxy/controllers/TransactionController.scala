package com.tdcproxy.controllers

import com.google.inject.{Inject, Singleton}
import com.tdcproxy.models.DateRange
import com.tdcproxy.repositories.TransactionRepository
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext

@Singleton
class TransactionController @Inject()(
  cc: ControllerComponents,
  transactionsRepo: TransactionRepository
)(implicit ec: ExecutionContext)
    extends AbstractController(cc) {

  def list(dateRange: DateRange, eventId: Seq[Long], limit: Option[Int]): Action[AnyContent] =
    Action.async { implicit request: Request[AnyContent] =>
      transactionsRepo
        .getTransactionsForEvents(
          afterDate = dateRange.after,
          beforeDate = dateRange.before,
          eventIds = eventId,
          limit = limit
        )
        .map(res => Ok(Json.toJson(res)))
    }
}
