package com.tdcproxy.db

import com.typesafe.config.Config
import org.slf4j.LoggerFactory
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

class TDCContext {
  private val databaseConfig =
    DatabaseConfig.forConfig[JdbcProfile]("tdc.database")

  private val logger = LoggerFactory.getLogger(this.getClass)

  lazy val profile = databaseConfig.profile

  lazy val db = databaseConfig.db

  lazy val config: Config = databaseConfig.config

  def dispose(): Unit = {
    logger.trace("Closing DB connection for TDCContext")
    this.db.close()
  }
}
