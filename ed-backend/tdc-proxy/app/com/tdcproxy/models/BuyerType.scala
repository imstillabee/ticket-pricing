package com.tdcproxy.models

import java.sql.Timestamp

import play.api.libs.json.{Json, OFormat}

case class BuyerType(
  id: Long,
  createdDate: Timestamp,
  active: Boolean,
  code: String,
  description: String,
  publicDescription: String,
  isInPriceStructure: Boolean
)

object BuyerType extends TimestampISOFormat {
  implicit lazy val buyerTypeFormat: OFormat[BuyerType] = Json.format[BuyerType]
}
