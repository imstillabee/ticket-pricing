package com.tdcproxy.models

import java.time.Instant

import play.api.mvc.QueryStringBindable

case class DateRange(after: Instant, before: Instant)

object DateRange {
  implicit def queryStringBindable(
    implicit stringBinder: QueryStringBindable[String]
  ): QueryStringBindable[DateRange] = new QueryStringBindable[DateRange] {
    override def bind(
      key: String,
      params: Map[String, Seq[String]]
    ): Option[Either[String, DateRange]] = {
      for {
        after <- stringBinder.bind("after", params)
        before <- stringBinder.bind("before", params)
      } yield {
        (before, after) match {
          case (Right(b), Right(a)) => Right(DateRange(Instant.parse(a), Instant.parse(b)))
          case _                    => Left("Unable to parse date range")
        }
      }
    }

    override def unbind(key: String, dateRange: DateRange): String = {
      stringBinder.unbind("before", dateRange.before.toString) + "&" + stringBinder.unbind(
        "after",
        dateRange.after.toString
      )
    }
  }
}
