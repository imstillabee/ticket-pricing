package com.tdcproxy.models

import java.sql.Timestamp

import play.api.libs.json.{Json, OFormat}

case class Inventory(
  eventId: Long,
  seatId: Long,
  priceScaleId: Long,
  createdAt: Timestamp,
  modifiedAt: Timestamp,
  listedPrice: Option[BigDecimal],
  section: String,
  row: String,
  seatNumber: String,
  holdCodeType: String
)

object Inventory extends TimestampISOFormat {
  implicit lazy val inventoryFormat: OFormat[Inventory] = Json.format[Inventory]
}
