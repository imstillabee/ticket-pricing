package com.tdcproxy.models

import java.sql.Timestamp
import java.time.Instant

import play.api.libs.json.Json.toJson
import play.api.libs.json.{Format, JsResult, JsSuccess, JsValue}

trait TimestampISOFormat {
  implicit val timestampFormat: Format[Timestamp] = new Format[Timestamp] {
    def writes(t: Timestamp): JsValue = toJson(t.toInstant.toString)

    def reads(json: JsValue): JsResult[Timestamp] = JsSuccess(
      Timestamp.from(Instant.parse(json.as[String]))
    )
  }
}
