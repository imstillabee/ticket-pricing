package com.tdcproxy.models

import java.sql.Timestamp

import play.api.libs.json.{Json, OFormat}

case class Transaction(
  id: Long,
  createdAt: Timestamp,
  timestamp: Timestamp,
  eventId: Long,
  buyerTypeCode: String,
  seatId: Long,
  price: BigDecimal,
  section: String,
  row: String,
  seatNumber: String,
  transactionTypeCode: String,
  priceScaleId: Long,
  holdCodeType: String
)

object Transaction extends TimestampISOFormat {
  implicit lazy val transactionFormat: OFormat[Transaction] = Json.format[Transaction]
}
