package com.tdcproxy.repositories

import com.google.inject.{Inject, Singleton}
import com.tdcproxy.db.TDCContext
import com.tdcproxy.models.BuyerType
import slick.jdbc.GetResult

import scala.concurrent.Future

@Singleton
class BuyerTypeRepository @Inject()(ctx: TDCContext) {
  import ctx.profile.api._

  implicit private lazy val getBuyerTypeResult: AnyRef with GetResult[BuyerType] = GetResult(
    r =>
      BuyerType(
        id = r.nextInt(),
        createdDate = r.nextTimestamp(),
        active = r.nextBoolean(),
        code = r.nextString(),
        description = r.nextString(),
        publicDescription = r.nextString(),
        isInPriceStructure = r.nextBoolean()
    )
  )

  def getForEvents(eventIds: Seq[Long]): Future[Vector[BuyerType]] = {
    val idString = eventIds.mkString(",")

    val q = sql"""
        WITH EVENT_BUYER_TYPE_IDS AS (
          SELECT
            bt.BUYER_TYPE_ID
          FROM BUYER_TYPE bt
          WHERE
            bt.BUYER_TYPE_ID in (
              SELECT
                psbt.BUYER_TYPE_ID
              FROM PRICE_STRUCT_BUYER_TYPE psbt
                INNER JOIN PRICE_STRUCTURE ps ON ps.PRICE_STRUCTURE_ID = psbt.PRICE_STRUCTURE_ID
                INNER JOIN EVENT e ON e.PRICE_STRUCTURE_ID = ps.PRICE_STRUCTURE_ID
              WHERE
                e.EVENT_ID IN (#$idString)
            )
            OR bt.BUYER_TYPE_ID in (
              SELECT
                psbt.BUYER_TYPE_ID
              FROM PRICE_STRUCT_BUYER_TYPE psbt
                INNER JOIN PRICE_STRUCTURE ps ON ps.PRICE_STRUCTURE_ID = psbt.PRICE_STRUCTURE_ID
                INNER JOIN EVENT e ON e.GROUP_PRICE_STRUCTURE_ID = ps.PRICE_STRUCTURE_ID
              WHERE
                e.EVENT_ID IN (#$idString)
            )
        )
        SELECT 
          bt.BUYER_TYPE_ID,
          bt.CREATED_DATE,
          bt.ACTIVE,
          bt.BUYER_TYPE_CODE,
          bt.DESCRIPTION,
          bt.PUBLIC_DESCRIPTION,
          CASE WHEN ebti.BUYER_TYPE_ID IS NOT NULL THEN 1 ELSE 0 END AS IS_IN_PRICE_STRUCTURE
        FROM BUYER_TYPE bt
        LEFT JOIN EVENT_BUYER_TYPE_IDS ebti ON bt.BUYER_TYPE_ID = ebti.BUYER_TYPE_ID
        """.as[BuyerType]

    ctx.db.run(q)
  }
}
