package com.tdcproxy.repositories

import java.sql.Timestamp
import java.time.{Instant, LocalDateTime, ZoneId, ZonedDateTime}

import com.google.inject.{Inject, Singleton}
import com.tdcproxy.db.TDCContext
import com.tdcproxy.models.Inventory
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}
import com.typesafe.config.Config

@Singleton
class InventoryRepository @Inject()(config: Config, val ctx: TDCContext)(
  implicit ec: ExecutionContext
) {
  private val timezone = ZoneId.of(config.getString("app.timezone"))

  import ctx.profile.api._

  implicit private lazy val getInventoryResult: AnyRef with GetResult[Inventory] = GetResult(
    r =>
      Inventory(
        eventId = r.nextInt,
        seatId = r.nextInt,
        priceScaleId = r.nextInt,
        createdAt = Timestamp
          .from(
            ZonedDateTime
              .of(r.nextTimestamp.toLocalDateTime, timezone)
              .toInstant
          ),
        modifiedAt = Timestamp
          .from(
            ZonedDateTime
              .of(r.nextTimestamp.toLocalDateTime, timezone)
              .toInstant
          ),
        listedPrice = r.nextBigDecimalOption,
        section = r.nextString,
        row = r.nextString,
        seatNumber = r.nextString,
        holdCodeType = r.nextString
    )
  )

  def getInventoryForEvents(
    afterDate: Instant,
    beforeDate: Instant,
    eventIds: Seq[Long],
    limit: Option[Int] = None
  ): Future[Vector[Inventory]] = {
    val idString = eventIds.mkString(",")
    val limitVal = limit.getOrElse(-1)

    ctx.db.run(sql"""
                SELECT
                  es.EVENT_ID "eventId",
                  es.SEAT_ID "seatId",
                  es.PRICE_SCALE_ID "priceScaleId",
                  es.CREATED_DATE "createdAt",
                  es.LAST_UPDATED_DATE "modifiedAt",
                  es.PRICE "listedPrice",
                  sec.SECTION_CODE "section",
                  s.ROW_ "row",
                  s.SEAT_NUMBER "seatNumber",
                  hc.HOLD_CODE_TYPE_CODE "holdCodeType"
                FROM
                  EVENT_SEAT es
                  JOIN HOLD_CODE hc ON es.HOLD_CODE_ID = hc.HOLD_CODE_ID
                  JOIN SEAT s ON es.SEAT_ID = s.SEAT_ID
                  JOIN SECTION sec ON s.SECTION_ID = sec.SECTION_ID
                WHERE
                  es.LAST_UPDATED_DATE
                    BETWEEN ${Timestamp.valueOf(LocalDateTime.ofInstant(afterDate, timezone))}
                    AND ${Timestamp.valueOf(LocalDateTime.ofInstant(beforeDate, timezone))}
                  AND es.EVENT_ID IN (#$idString)
                  AND ($limitVal <= -1 OR ROWNUM <= $limitVal)
         """.as[Inventory])
  }
}
