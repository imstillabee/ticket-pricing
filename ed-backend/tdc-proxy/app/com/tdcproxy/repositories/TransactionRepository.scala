package com.tdcproxy.repositories

import java.sql.Timestamp
import java.time.{Instant, LocalDateTime, ZoneId, ZonedDateTime}

import com.google.inject.{Inject, Singleton}
import com.tdcproxy.db.TDCContext
import com.tdcproxy.models.Transaction
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}
import com.typesafe.config.Config

@Singleton
class TransactionRepository @Inject()(config: Config, val ctx: TDCContext)(
  implicit ec: ExecutionContext
) {
  private val timezone = ZoneId.of(config.getString("app.timezone"))

  import ctx.profile.api._

  implicit private lazy val getTransactionResult: AnyRef with GetResult[Transaction] = GetResult(
    r =>
      Transaction(
        id = r.nextLong,
        createdAt = Timestamp
          .from(
            ZonedDateTime
              .of(r.nextTimestamp.toLocalDateTime, timezone)
              .toInstant
          ),
        timestamp = Timestamp
          .from(
            ZonedDateTime
              .of(r.nextTimestamp.toLocalDateTime, timezone)
              .toInstant
          ),
        eventId = r.nextLong,
        buyerTypeCode = r.nextString,
        seatId = r.nextLong,
        price = r.nextBigDecimal,
        section = r.nextString,
        row = r.nextString,
        seatNumber = r.nextString,
        transactionTypeCode = r.nextString,
        priceScaleId = r.nextLong,
        holdCodeType = r.nextString
    )
  )

  /**
    * Get all transactions for events that happened after the last recorded transaction in Event Dynamic.
    *
    * @param afterDate last transaction date in Event Dynamic for this client
    * @param eventIds  event ids on the integration (not in Event Dynamic)
    * @return transactions
    */
  def getTransactionsForEvents(
    afterDate: Instant,
    beforeDate: Instant,
    eventIds: Seq[Long],
    limit: Option[Int] = None
  ): Future[Seq[Transaction]] = {
    val idString = eventIds.mkString(",")
    val limitVal = limit.getOrElse(-1)
    ctx.db.run(sql"""
            SELECT
              trx.transaction_id "transactionId",
              trx.created_date "createdAt",
              trx.transaction_date "timestamp",
              e.event_id "eventId",
              bt.buyer_type_code "buyerTypeCode",
              s.seat_id "seatId",
              coalesce(t.PRICE, - trmv.PRICE) "price",
              sec.section_code "section",
              s.row_ "row",
              s.seat_number "seatNumber",
              oli.transaction_type_code "transactionTypeCode",
              es.price_scale_id "priceScaleId",
              hc.hold_code_type_code "holdCodeType"
            FROM
              TRANSACTION trx
              JOIN order_line_item oli ON trx.transaction_id = oli.transaction_id
              LEFT OUTER JOIN ticket t ON oli.order_line_item_id = t.order_line_item_id
              LEFT OUTER JOIN ticket trmv ON oli.order_line_item_id = trmv.remove_order_line_item_id
              JOIN seat s ON s.seat_id = coalesce(t.seat_id, trmv.seat_id)
              JOIN section sec ON s.section_id = sec.section_id
              JOIN event e ON oli.event_id = e.event_id
              JOIN buyer_type bt ON coalesce(t.buyer_type_id, trmv.buyer_type_id) = bt.buyer_type_id
              LEFT OUTER JOIN event_seat es ON e.event_id = es.event_id
              		AND s.seat_id = es.seat_id
              LEFT OUTER JOIN hold_code hc ON es.hold_code_id = hc.hold_code_id
            WHERE
              oli.transaction_type_code IN('SA', 'CS', 'ES', 'ER', 'RT')
              AND oli.market_type_code = 'P'
              AND trx.created_date
                BETWEEN ${Timestamp.valueOf(LocalDateTime.ofInstant(afterDate, timezone))}
                AND ${Timestamp.valueOf(LocalDateTime.ofInstant(beforeDate, timezone))}
              AND e.event_id IN (#$idString)
              AND ($limitVal <= -1 OR ROWNUM <= $limitVal)
            ORDER BY
              trx.transaction_date
      """.as[Transaction])
  }
}
