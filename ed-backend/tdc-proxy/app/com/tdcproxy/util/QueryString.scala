package com.tdcproxy.util

object QueryString {

  def stringify(params: Map[String, Any]): String = {
    params.toSeq
      .flatMap {
        case (pKey, pVal: Seq[Any]) => pVal.map(pKey -> _)
        case other                  => Seq(other)
      }
      .map {
        case (pKey, pVal) => s"$pKey=${pVal.toString}"
      }
      .mkString("&")
  }
}
