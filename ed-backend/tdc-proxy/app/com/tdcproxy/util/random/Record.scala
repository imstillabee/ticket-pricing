package com.tdcproxy.util.random

import java.sql.Timestamp
import java.time.Instant

import com.tdcproxy.models.{BuyerType, Inventory, Transaction}

import scala.util.Random

object Record {
  def int(): Int = Random.nextInt()
  def long(): Long = Random.nextLong()
  def unsignedLong(): Long = Math.abs(Random.nextLong())
  def string(n: Int = 20): String = Random.alphanumeric.take(n).mkString
  def instant(): Instant = Instant.EPOCH
  def decimal(): BigDecimal = BigDecimal(Random.nextFloat())
  def boolean(): Boolean = Random.nextBoolean()

  def buyerType(
    id: Long = unsignedLong(),
    createdDate: Timestamp = Timestamp.from(instant()),
    active: Boolean = boolean(),
    code: String = string(6),
    description: String = string(),
    publicDescription: String = string(),
    isInPriceStructure: Boolean = boolean()
  ): BuyerType = {
    BuyerType(id, createdDate, active, code, description, publicDescription, isInPriceStructure)
  }

  private val transactionTypeCodes = List("SA", "CS", "ES", "ER", "RT")

  def transaction(
    id: Long = unsignedLong(),
    createdAt: Timestamp = Timestamp.from(instant()),
    timestamp: Timestamp = Timestamp.from(instant()),
    eventId: Long = unsignedLong(),
    seatId: Long = unsignedLong(),
    buyerTypeCode: String = string(5),
    price: BigDecimal = decimal(),
    section: String = string(),
    row: String = string(),
    seatNumber: String = string(),
    transactionTypeCode: String = transactionTypeCodes(Random.nextInt(transactionTypeCodes.length)),
    priceScaleId: Long = unsignedLong(),
    holdCodeType: String = string(1)
  ): Transaction = Transaction(
    id,
    createdAt,
    timestamp,
    eventId,
    buyerTypeCode,
    seatId,
    price,
    section,
    row,
    seatNumber,
    transactionTypeCode,
    priceScaleId,
    holdCodeType
  )

  def inventory(
    eventId: Long = unsignedLong(),
    seatId: Long = unsignedLong(),
    priceScaleId: Long = unsignedLong(),
    createdAt: Timestamp = Timestamp.from(instant()),
    modifiedAt: Timestamp = Timestamp.from(instant()),
    listedPrice: Option[BigDecimal] = Some(decimal()),
    section: String = string(),
    row: String = string(),
    seatNumber: String = string(),
    holdCodeType: String = string(1)
  ): Inventory = Inventory(
    eventId,
    seatId,
    priceScaleId,
    createdAt,
    modifiedAt,
    listedPrice,
    section,
    row,
    seatNumber,
    holdCodeType
  )
}
