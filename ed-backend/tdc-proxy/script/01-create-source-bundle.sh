#! /bin/bash
set -e

DOCKER_TAG=$1

# Prefix of file name is the tag.
DOCKERRUN_FILE=Dockerrun.aws.json
ZIP_FILE=$DOCKER_TAG-Dockerrun.zip

# Replacing tags in the file and creating a file.
sed -e "s@<TAG>@$DOCKER_TAG@" < Dockerrun.aws.json.template > $DOCKERRUN_FILE

zip $ZIP_FILE $DOCKERRUN_FILE -r .ebextensions
S3_PATH="s3://$DEPLOYMENT_BUCKET/$BUCKET_DIRECTORY/$ZIP_FILE"
# Uploading json file to $S3_PATH
$AWS_CLI_LOCATION s3 cp $ZIP_FILE $S3_PATH

cat $DOCKERRUN_FILE
