#! /bin/bash

set -e

DOCKER_TAG=$1
APPLICATION_NAME=$2
DESCRIPTION=$(git log -1 --pretty='%s - %cN [%h]')
DOCKERRUN_FILE=$DOCKER_TAG-Dockerrun.zip

# Check if the version has already been created
LATEST_VERSION=$($AWS_CLI_LOCATION elasticbeanstalk describe-application-versions --region=$REGION --application-name "$APPLICATION_NAME"  \
    --max-records 1 --version-labels $DOCKER_TAG)

VERSION_EXISTS=$(echo $LATEST_VERSION | jq 'if (.ApplicationVersions | length) > 0 then true else false end')

if [[ $VERSION_EXISTS == true ]]; then
  printf "\n> version $DOCKER_TAG already exists. Moving to deploy phase.\n\n"
  exit 0
else
  printf "\n> version $DOCKER_TAG does not exist. Creating version now.\n\n"
  # Run aws command to create a new EB application with label
  $AWS_CLI_LOCATION elasticbeanstalk create-application-version --region=$REGION --application-name "$APPLICATION_NAME" \
    --version-label $DOCKER_TAG --source-bundle S3Bucket=$DEPLOYMENT_BUCKET,S3Key=$BUCKET_DIRECTORY/$DOCKERRUN_FILE \
    --description "$DESCRIPTION"
fi

