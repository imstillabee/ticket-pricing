#! /bin/bash

set -e

DOCKER_TAG=$1
APPLICATION_NAME=$2
DESCRIPTION=$(git log -1 --pretty='%s - %cN [%h]')
ENVIRONMENT_NAME=$3

# Deploy
$AWS_CLI_LOCATION elasticbeanstalk update-environment --region=$REGION --application-name "$APPLICATION_NAME" \
  --version-label $DOCKER_TAG --environment-name $ENVIRONMENT_NAME
