package com.tdcproxy.controllers

import com.tdcproxy.repositories.BuyerTypeRepository
import com.tdcproxy.util.QueryString
import com.tdcproxy.util.random.Record
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.Future

class BuyerTypeControllerSpec extends PlaySpec with GuiceOneAppPerTest with MockitoSugar {
  private val buyerTypes = (1 to 10).map(_ => Record.buyerType()).toVector
  private var buyerTypeRepository = mock[BuyerTypeRepository]

  override def fakeApplication(): Application = {
    buyerTypeRepository = mock[BuyerTypeRepository]
    GuiceApplicationBuilder()
      .overrides(bind[BuyerTypeRepository].to(buyerTypeRepository))
      .build()
  }

  "BuyerTypeController GET" should {
    val eventIds = List[Long](1, 2, 3)

    "return 200" should {
      "when passed query params" in {
        when(buyerTypeRepository.getForEvents(any[Seq[Long]]))
          .thenReturn(Future.successful(buyerTypes))

        val params = Map("eventId" -> eventIds)

        val request = FakeRequest(GET, s"/buyerTypes?${QueryString.stringify(params)}")
        val result = route(app, request).get

        status(result) mustBe OK
        contentType(result) mustBe Some("application/json")
        contentAsJson(result) mustBe Json.toJson(buyerTypes)

        verify(buyerTypeRepository, times(1)).getForEvents(eventIds)
      }

      "with a cached response" in {
        when(buyerTypeRepository.getForEvents(any[Seq[Long]]))
          .thenReturn(Future.successful(buyerTypes))

        val params = Map("eventId" -> eventIds)

        val request = FakeRequest(GET, s"/buyerTypes?${QueryString.stringify(params)}")

        (1 to 4).foreach(_ => {
          val result = route(app, request).get
          status(result) mustBe OK
          contentType(result) mustBe Some("application/json")
          contentAsJson(result) mustBe Json.toJson(buyerTypes)
        })

        verify(buyerTypeRepository, times(1)).getForEvents(eventIds)
      }
    }
  }
}
