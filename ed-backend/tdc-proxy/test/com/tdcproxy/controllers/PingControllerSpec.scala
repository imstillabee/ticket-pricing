package com.tdcproxy.controllers

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._

/**
  * Add your spec here.
  * You can mock out a whole application including requests, plugins etc.
  *
  * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
  */
class PingControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "PingController GET" should {

    "return ok from a new instance of controller" in {
      val controller = new PingController(stubControllerComponents())
      val ping = controller.index().apply(FakeRequest(GET, "/"))

      status(ping) mustBe OK
      contentType(ping) mustBe Some("text/plain")
      contentAsString(ping) must include("ok")
    }

    "return ok from the application" in {
      val controller = inject[PingController]
      val ping = controller.index().apply(FakeRequest(GET, "/"))

      status(ping) mustBe OK
      contentType(ping) mustBe Some("text/plain")
      contentAsString(ping) must include("ok")
    }

    "render the index page from the router" in {
      val request = FakeRequest(GET, "/ping")
      val ping = route(app, request).get

      status(ping) mustBe OK
      contentType(ping) mustBe Some("text/plain")
      contentAsString(ping) must include("ok")
    }
  }
}
