package com.tdcproxy.controllers

import java.time.Instant
import java.time.temporal.ChronoUnit

import com.tdcproxy.repositories.TransactionRepository
import com.tdcproxy.util.QueryString
import com.tdcproxy.util.random.Record
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.Future

class TransactionControllerSpec extends PlaySpec with GuiceOneAppPerTest with MockitoSugar {

  private val transactions = (1 to 10).map(_ => Record.transaction()).toVector
  private var transactionRepository: TransactionRepository = mock[TransactionRepository]

  override def fakeApplication(): Application = {
    transactionRepository = mock[TransactionRepository]
    when(
      transactionRepository
        .getTransactionsForEvents(any[Instant], any[Instant], any[Seq[Long]], any[Option[Int]])
    ).thenReturn(Future.successful(transactions))
    new GuiceApplicationBuilder()
      .overrides(bind[TransactionRepository].to(transactionRepository))
      .build()
  }

  "TransactionController GET" should {
    val before = Instant.now()
    val after = before.minus(1, ChronoUnit.HOURS)
    val eventIds = List[Long](1, 2, 3)
    val limit = 100

    "return 200" should {
      "when passed query params" in {
        val params =
          Map("before" -> before, "after" -> after, "eventId" -> eventIds, "limit" -> limit)

        val request = FakeRequest(GET, s"/transactions?${QueryString.stringify(params)}")
        val result = route(app, request).get

        status(result) mustBe OK
        contentType(result) mustBe Some("application/json")
        contentAsJson(result) mustBe Json.toJson(transactions)

        verify(transactionRepository, times(1)).getTransactionsForEvents(
          afterDate = after,
          beforeDate = before,
          eventIds = eventIds,
          limit = Some(limit)
        )
      }
    }

    "return 400" should {
      "when limit is not a number" in {
        val params =
          Map("before" -> before, "after" -> after, "eventId" -> eventIds, "limit" -> "NaN")

        val request = FakeRequest(GET, s"/transactions?${QueryString.stringify(params)}")
        val result = route(app, request).get

        status(result) mustBe BAD_REQUEST

        verify(transactionRepository, times(0))
          .getTransactionsForEvents(any[Instant], any[Instant], any[List[Long]], any[Option[Int]])
      }

      "when after is missing" in {
        val params = Map("before" -> before, "eventId" -> eventIds)

        val request = FakeRequest(GET, s"/transactions?${QueryString.stringify(params)}")
        val result = route(app, request).get

        status(result) mustBe BAD_REQUEST

        verify(transactionRepository, times(0))
          .getTransactionsForEvents(any[Instant], any[Instant], any[List[Long]], any[Option[Int]])
      }

      "when before is missing" in {
        val params = Map("before" -> before, "eventId" -> eventIds)

        val request = FakeRequest(GET, s"/transactions?${QueryString.stringify(params)}")
        val result = route(app, request).get

        status(result) mustBe BAD_REQUEST

        verify(transactionRepository, times(0))
          .getTransactionsForEvents(any[Instant], any[Instant], any[List[Long]], any[Option[Int]])
      }
    }
  }
}
