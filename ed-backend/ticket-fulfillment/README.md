# Ticket Fulfillment Pipeline

The ticket fulfillment pipeline fulfills orders and synchronizes inventory across integrations.

Fulfillment steps:

1. __Listen__ for ticket orders
1. __Reserve__ the ticket(s) on the primary integration, going through the checkout flow and getting ticket barcodes
1. __Fulfill__ the order on the secondary integration, delivering the barcodes for the order
1. __Delist__ the sold tickets from all the secondary integrations

## Running the pipeline

1. Onboard data for the Zephyrs client, a fake team that exists in the TDC sandbox
1. Make sure the Zephyrs client has an `EDTickets` integration (in the `ClientIntegrations` table)
	```text
	clientId: <Zephyrs id from Clients table>
	createdAt: now
	id: default
	integrationId: <EDTickets id from Integrations table>
	isActive: TRUE
	isPrimary: FALSE
	modifiedAt: now
	```
1. Make sure the Zephyrs client has a `ticket-fulfillment` job config (in the `JobConfigs` table)
	```text
	createdAt: now
	id: default
	modifiedAt: now
	clientId: <Zephyrs id from Clients table>
	name: ticket-fulfillment
	json: <see example job config below>
	```
1. Set `.env` accordingly:
	1. __ED_DB_URI__ to correct URL
	1. Add __ED_FULFILLMENT_CLIENT_ID__ with Zephyrs id
1. Run `TICKET FULFILLMENT` 
1. Run the [ed-tickets](https://github.com/dialexa/ed-tickets) project
	1. Local: Make sure the __webhook_url__ in `ed-tickets/functions/.runtimeconfig.json` is set to __http://localhost:2552/edtickets__
	1. QA: Set the __webhook_url__ to __http://qafulfillment.eventdynamic.com__
1. Initiate an order from `ed-tickets`

### Example Job Config for Tickets.com (TDC)
```json
{
	"tdc": {
		"paymentMethodId":1821,
		"deliveryMethodId":2001,
		"patronAccountId":123528,
		"defaultBuyerTypeId":3161,
		"lastTransactionPoll":"2019-02-21T09:51:19.644"
	}   
}
```
### Example Job Config for Skybox
```json
{
	"skybox": {
		"lastTransactionPoll":"2019-02-21T09:51:19.644"
	}
}
```

## Organization

This project uses [akka](https://akka.io/) actors.
 
### Actors

| Actor | Usage | Primary | Secondary |
| :--- | :--- | :---: | :---: |
| COORDINATOR | the master actor, manages interactions between the other actors | n/a | n/a 
| LISTENER | listen (or poll) for new orders | x | x |
| RESERVER | purchase the tickets and get barcode(s) | x |
| FULFILLER | fulfill the order with the ticket barcode | | x | 
| DELISTER | remove tickets from sale from secondary integrations | | x |

