package com.eventdynamic.ticketfulfillment

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.eventdynamic.IntegrationServiceBuilder
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.JobType
import com.eventdynamic.models.jobconfigs.{TicketFulfillmentConfig, TicketFulfillmentConfigFormat}
import com.eventdynamic.services.{JobConfigService, NotificationService}
import com.eventdynamic.ticketfulfillment.pipeline.FulfillmentPipeline
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.typesafe.config.{Config, ConfigFactory}
import org.slf4j.LoggerFactory

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

object TicketFulfillment extends TicketFulfillmentConfigFormat {
  private val logger = LoggerFactory.getLogger(this.getClass)

  def main(args: Array[String]): Unit = {
    implicit val actorSystem: ActorSystem = ActorSystem("system")
    implicit val actorMaterializer: ActorMaterializer = ActorMaterializer()
    implicit val executionContext: ExecutionContext = actorSystem.dispatcher
    implicit val edContext: EDContext = new EDContext
    edContext.registerShutdownHook()

    val serviceHolder = ServiceHolder()
    val serviceBuilder = new IntegrationServiceBuilder()

    val conf: Config = ConfigFactory.defaultApplication().resolve()
    val clientId = conf.getInt("fulfillment.clientId")
    val jobConfig = getConfig(serviceHolder.jobConfigService, clientId)

    // Dynamically build pipeline for client
    val pipeline =
      FulfillmentPipeline.buildPipelineForClient(
        clientId,
        serviceHolder,
        actorSystem,
        jobConfig,
        serviceBuilder
      )

    // Reduce routes into a single definition
    val routes =
      if (pipeline.routes.isEmpty)
        healthRoute
      else healthRoute ~ pipeline.routes.values.reduce(_ ~ _)

    // Create and run server
    val interface = conf.getString("fulfillment.interface")
    val port = conf.getInt("fulfillment.port")
    Http().bindAndHandle(routes, interface, port)
    logger.info(s"Running online at http://$interface:$port/")
  }

  val healthRoute: Route = path("health") {
    get {
      logger.debug("Healthy!")
      complete(StatusCodes.OK)
    }
  }

  private def getConfig(
    jobConfigService: JobConfigService,
    clientId: Int
  ): TicketFulfillmentConfig = {
    val c = Await.result(
      jobConfigService.getConfig[TicketFulfillmentConfig](clientId, JobType.TicketFulfillment),
      5.seconds
    )

    if (c.isEmpty) {
      val notificationService = new NotificationService(clientId, Option.empty)
      notificationService.sendAlert(s"No ${JobType.TicketFulfillment} config for client $clientId.")
      System.exit(1)
    }

    c.get
  }
}
