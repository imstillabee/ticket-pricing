package com.eventdynamic.ticketfulfillment.coordinators
import akka.actor.{Actor, ActorRef, Props}
import com.eventdynamic.ticketfulfillment.delisters.DelistTickets
import com.eventdynamic.ticketfulfillment.fulfillers.{FulfillOrder, RejectOrder}
import com.eventdynamic.ticketfulfillment.models.Order
import com.eventdynamic.ticketfulfillment.persistor.UpsertOrder
import com.eventdynamic.ticketfulfillment.reservers.{PrintTickets, ReserveTickets}
import org.slf4j.LoggerFactory

class Coordinator(
  persistor: ActorRef,
  reserver: ActorRef,
  fulfillers: Map[Int, ActorRef],
  delisters: Map[Int, ActorRef]
) extends Actor {

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case ProcessOrder(order: Order) if order.isPrimary =>
      logger.info("New primary order", order)

      // Save the order in ED
      persistor ! UpsertOrder(order)

      // Delist the tickets from secondaries
      delisters.foreach(_._2 ! DelistTickets(order))

    case ProcessOrder(order: Order) if !order.isPrimary =>
      logger.info("New secondary order", order)

      // Reserve tickets on the primary
      reserver ! ReserveTickets(order)

    case ReservationSuccessful(order, deliveryInfo) =>
      logger.info("Processing successful reservation")

      // Save the order in ED
      persistor ! UpsertOrder(order)

      // Print the tickets, attaching the barcode to the order
      reserver ! PrintTickets(order, deliveryInfo)

      // Delist the tickets on all the other secondary integrations
      delisters
        .filter(_._1 != order.integrationId)
        .foreach(delister => delister._2 ! DelistTickets(order))

    case ReservationFailed(order) =>
      logger.info("Processing fail reservation")

      // Reject the order on the secondary it originated on
      fulfillers(order.integrationId) ! RejectOrder(order)

    case PrintSuccessful(order) =>
      logger.info("Successfully printed order")

      // Fulfill the order on the integration it was bought on
      fulfillers(order.integrationId) ! FulfillOrder(order)

    case PrintFailed(order) =>
      logger.warn(s"Failed to print order $order")

    case msg =>
      logger.error(s"Unknown message $msg")
  }
}

object Coordinator {

  /**
    *
    * @param reserver
    * @param fulfillers
    * @param delisters
    * @return
    */
  def props(
    persistor: ActorRef,
    reserver: ActorRef,
    fulfillers: Map[Int, ActorRef],
    delisters: Map[Int, ActorRef]
  ): Props =
    Props(new Coordinator(persistor, reserver, fulfillers, delisters))
}
