package com.eventdynamic.ticketfulfillment

import com.eventdynamic.ticketfulfillment.models.Order

package object coordinators {
  // Processing orders/returns from listeners
  case class ProcessOrder(order: Order)
  case class ProcessReturn()

  // Processing reservation responses from reservers
  case class ReservationSuccessful[T](order: Order, deliveryInfo: T)
  case class ReservationFailed(order: Order)

  // Processing printing responses from reservers
  case class PrintSuccessful(order: Order)
  case class PrintFailed(order: Order)
}
