package com.eventdynamic.ticketfulfillment.delisters

import akka.actor.{Actor, Props}
import com.eventdynamic.SttpHelper
import com.eventdynamic.edtickets.EDTicketsService
import com.eventdynamic.edtickets.models.EDListing
import com.eventdynamic.models.{Event, EventSeatBySeatLocator}
import com.eventdynamic.services.NotificationService
import com.eventdynamic.ticketfulfillment.models.Order
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.transactions.ClientIntegrationTicket
import com.eventdynamic.utils.FutureUtil
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.util.{Failure, Success}

class EDTicketsDelister(
  serviceHolder: ServiceHolder,
  apiService: EDTicketsService,
  clientIntegrationId: Int,
  notificationService: NotificationService
) extends Actor {
  import context.dispatcher

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case DelistTickets(order: Order) =>
      delistTickets(order)
        .andThen {
          case Success(_) => logger.info(s"Successfully delisted order on EDTickets: $order")
          case Failure(err) =>
            notificationService.sendAlert(s"Error delisting order on EDTickets", err)
        }
  }

  /**
    * Delist tickets on EDTickets
    *
    * @param order EventDynamic Order
    * @return list of delisted ED Listings
    */
  private def delistTickets(order: Order): Future[List[EDListing]] = {
    for {
      _ <- apiService.authenticate.map(SttpHelper.handleResponse)

      eventId <- getEventForOrder(order).map {
        case Some(event) => event.id.get
        case None        => throw new Exception("Event does not exist")
      }

      eventSeatIds <- FutureUtil.serializeFutures(order.orderItems)(orderItem => {
        val identifier =
          EventSeatBySeatLocator(eventId, order.section, order.row, orderItem.seatNumber.toString)
        serviceHolder.eventSeatService
          .getOne(identifier)
          .map {
            case Some(eventSeat) => eventSeat.id.get
            case None =>
              throw new Exception(s"Event seat does not exist for identifier $identifier")
          }
      })

      clientIntegrationListings <- serviceHolder.integrationTransactions
        .getClientEventSeatListing(eventSeatIds.toSet, clientIntegrationId)

      listingGroups = clientIntegrationListings.groupBy(_.clientIntegrationListingExternalId)

      res <- FutureUtil.serializeFutures(listingGroups)(group => {
        val (listingId, clientIntegrationListings) = group

        updateEdListing(listingId)(edListing => {
          val updatedSeats = edListing.seats.map(
            seat =>
              seat.copy(
                available = clientIntegrationListings.exists(_.seat == seat.seat) || seat.available
            )
          )

          val status = if (updatedSeats.exists(_.available)) "AVAILABLE" else "DELETED"

          edListing.copy(seats = updatedSeats, status = status)
        })
      })
    } yield res
  }

  /**
    * Utility method to fetch and update an ED Listing
    *
    * @param updater mapper function to update the order
    * @return updated EDOrder
    */
  private def updateEdListing(id: String)(updater: EDListing => EDListing): Future[EDListing] = {
    for {
      edListing <- apiService
        .getListing(id)
        .map(SttpHelper.handleResponse)

      updatedEdListing = updater(edListing)

      updateResult <- apiService
        .updateListing(updatedEdListing)
        .map(SttpHelper.handleResponse)
    } yield updateResult
  }

  // TODO: don't duplicate this logic between delisters...
  private def getEventForOrder(order: Order): Future[Option[Event]] = {
    if (order.isPrimary) {
      serviceHolder.eventService
        .getByIntegrationId(order.externalEventId.toInt, order.clientId)
    } else {
      serviceHolder.eventService.getBySecondaryEventId(
        clientId = order.clientId,
        integrationId = order.integrationId,
        secondaryEventId = order.externalEventId
      )
    }
  }
}

object EDTicketsDelister {

  def props(
    serviceHolder: ServiceHolder,
    apiService: EDTicketsService,
    clientIntegrationId: Int,
    notificationService: NotificationService
  ): Props = Props(
    new EDTicketsDelister(serviceHolder, apiService, clientIntegrationId, notificationService)
  )

  case class LoginSuccess(tickets: Seq[ClientIntegrationTicket]) extends Message
  case class EDTicketListing(tickets: Seq[ClientIntegrationTicket], listing: EDListing)
  case class DeleteEventSeats(tickets: Seq[ClientIntegrationTicket], listing: EDListing)
      extends Message
  case class DeleteListing(listingId: Int) extends Message
}
