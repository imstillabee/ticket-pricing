package com.eventdynamic.ticketfulfillment.delisters

import akka.actor.{Actor, Props}
import com.eventdynamic.models.{Event, EventSeatBySeatLocator}
import com.eventdynamic.services.NotificationService
import com.eventdynamic.stubhub.StubHubService
import com.eventdynamic.stubhub.models._
import com.eventdynamic.ticketfulfillment.models.Order
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.utils.FutureUtil
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.util.{Failure, Success}

class StubHubDelister(
  serviceHolder: ServiceHolder,
  apiService: StubHubService,
  clientIntegrationId: Int,
  notificationService: NotificationService
) extends Actor {
  import context.dispatcher

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case DelistTickets(order) =>
      delistTickets(order)
        .andThen {
          case Success(_) => logger.info(s"Successfully delisted order on StubHub: $order")
          case Failure(err) =>
            notificationService.sendAlert(s"Error delisting order on StubHub", err)
        }

    case unknown =>
      logger.error(s"Unknown message $unknown")

  }

  /**
    * Delist the tickets in the order so that they cannot be bought on StubHub
    *
    * @param order EventDynamic Order
    * @return list of results of removing listings
    */
  def delistTickets(order: Order): Future[List[StubHubListingResponse]] = {
    for {
      eventId <- getEventForOrder(order).map {
        case Some(event) => event.id.get
        case None        => throw new Exception("Event does not exist")
      }

      eventSeatIds <- FutureUtil.serializeFutures(order.orderItems)(orderItem => {
        val identifier =
          EventSeatBySeatLocator(eventId, order.section, order.row, orderItem.seatNumber.toString)
        serviceHolder.eventSeatService
          .getOne(identifier)
          .map {
            case Some(eventSeat) => eventSeat.id.get
            case None =>
              throw new Exception(s"Event seat does not exist for identifier $identifier")
          }
      })

      clientIntegrationListings <- serviceHolder.integrationTransactions
        .getClientEventSeatListing(eventSeatIds.toSet, clientIntegrationId)

      // Group the inventory by listingId because the order could be across several StubHub listings
      listingGroups = clientIntegrationListings.groupBy(_.clientIntegrationListingExternalId)

      delistResults <- FutureUtil.serializeFutures(listingGroups)(group => {
        val (listingId, clientIntegrationListings) = group
        val tickets = clientIntegrationListings.map(
          clientIntegrationListing =>
            StubHubTicket(clientIntegrationListing.row, clientIntegrationListing.seat)
        )
        apiService.delist(tickets, listingId.toInt).map {
          case ApiSuccess(_, body: StubHubListingResponse) => body
          case ApiError(code, body)                        => throw new Exception(s"Skybox error: $code -- $body")
        }
      })
    } yield delistResults
  }

  // TODO: don't duplicate this logic between delisters...
  private def getEventForOrder(order: Order): Future[Option[Event]] = {
    if (order.isPrimary) {
      serviceHolder.eventService
        .getByIntegrationId(primaryEventId = order.externalEventId.toInt, clientId = order.clientId)
    } else {
      serviceHolder.eventService
        .getBySecondaryEventId(
          clientId = order.clientId,
          integrationId = order.integrationId,
          secondaryEventId = order.externalEventId
        )
    }
  }
}

object StubHubDelister {

  def props(
    serviceHolder: ServiceHolder,
    apiService: StubHubService,
    clientIntegrationId: Int,
    notificationService: NotificationService
  ): Props =
    Props(new StubHubDelister(serviceHolder, apiService, clientIntegrationId, notificationService))
}
