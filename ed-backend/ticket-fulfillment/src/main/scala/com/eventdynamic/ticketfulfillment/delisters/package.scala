package com.eventdynamic.ticketfulfillment

import com.eventdynamic.ticketfulfillment.models.Order

package object delisters {
  abstract class Message

  // payloads
  case class AccessTokenExpired[T](payload: T) extends Message
  case class DelistTickets(order: Order) extends Message

  // success/error handling
  case class DelistSuccess[T](payload: T) extends Message
  case class DelistError(msg: String) extends Message
  case class DelistException(throwable: Throwable) extends Message
}
