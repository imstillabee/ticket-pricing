package com.eventdynamic.ticketfulfillment.fulfillers

import akka.actor.{Actor, Props}
import com.eventdynamic.SttpHelper
import com.eventdynamic.edtickets.EDTicketsService
import com.eventdynamic.edtickets.models.EDOrder
import com.eventdynamic.services.NotificationService
import com.eventdynamic.ticketfulfillment.models.Order
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.util.{Failure, Success}

class EDTicketsFulfiller(
  serviceHolder: ServiceHolder,
  apiService: EDTicketsService,
  notificationService: NotificationService
) extends Actor {

  import context.dispatcher

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case FulfillOrder(order) =>
      fulfillOrder(order)
        .andThen {
          case Success(_) => logger.info(s"Successfully fulfilled order: $order")
          case Failure(err) =>
            notificationService.sendAlert(
              s"An exception occurred fulfilling EDTickets order: $order",
              err
            )
        }

    case RejectOrder(order) =>
      rejectOrder(order)
        .andThen {
          case Success(_) => logger.info(s"Successfully rejected order: $order")
          case Failure(err) =>
            notificationService.sendAlert(
              s"An exception occurred rejecting EDTickets order: $order",
              err
            )
        }

    case unknown =>
      logger.error(s"Unknown message $unknown")
  }

  /**
    * Fulfill an EDOrder by updating the listing with the barcode
    *
    * @param order EventDynamic Order
    * @return updated EDOrder
    */
  private def fulfillOrder(order: Order): Future[EDOrder] = {
    for {
      _ <- apiService.authenticate.map(SttpHelper.handleResponse)

      edOrder <- updateEdOrder(order.sourceExternalTransactionId.toString) { edOrder =>
        edOrder.copy(
          status = "FULFILLED",
          seats = edOrder.seats.map(
            seat =>
              seat.copy(barcode = order.orderItems.find(_.seatNumber == seat.seat).get.barcode)
          )
        )
      }
    } yield edOrder
  }

  /**
    * Reject an EDOrder by updating the listing to remove the barcode
    *
    * @param order EventDynamic Order
    * @return updated EDOrder
    */
  private def rejectOrder(order: Order): Future[EDOrder] = {
    for {
      _ <- apiService.authenticate.map(SttpHelper.handleResponse)

      edOrder <- updateEdOrder(order.sourceExternalTransactionId.toString) { edOrder =>
        edOrder.copy(
          status = "REJECTED",
          seats = edOrder.seats.map(seat => seat.copy(barcode = None))
        )
      }
    } yield edOrder
  }

  /**
    * Utility method to fetch and update an EDOrder
    *
    * @param updater mapper function to update the order
    * @return updated EDOrder
    */
  private def updateEdOrder(id: String)(updater: EDOrder => EDOrder): Future[EDOrder] = {
    for {
      edOrder <- apiService
        .getOrder(id)
        .map(SttpHelper.handleResponse)

      updatedEdOrder = updater(edOrder)

      updateResult <- apiService
        .updateOrder(updatedEdOrder)
        .map(SttpHelper.handleResponse)
    } yield updateResult
  }
}

object EDTicketsFulfiller {

  def props(
    serviceHolder: ServiceHolder,
    apiService: EDTicketsService,
    notificationService: NotificationService
  ): Props = Props(new EDTicketsFulfiller(serviceHolder, apiService, notificationService))
}
