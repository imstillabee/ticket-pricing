package com.eventdynamic.ticketfulfillment.fulfillers

import akka.actor.{Actor, Props}
import com.eventdynamic.services.NotificationService
import com.eventdynamic.stubhub.StubHubService
import com.eventdynamic.stubhub.models._
import com.eventdynamic.ticketfulfillment.models.Order
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.util.{Failure, Success}

class StubHubFulfiller(
  serviceHolder: ServiceHolder,
  stubHubService: StubHubService,
  notificationService: NotificationService
) extends Actor {
  import context.dispatcher

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case FulfillOrder(order) =>
      fulfillOrder(order)
        .andThen {
          case Success(_) => logger.info(s"Successfully fulfilled order: $order")
          case Failure(err) =>
            notificationService.sendAlert(
              s"An exception occurred fulfilling StubHub order: $order",
              err
            )
        }

    case RejectOrder(order) =>
      // StubHub's API does not support rejecting an order
      notificationService.sendAlert(s"Rejected StubHub order: $order")

    case unknown =>
      logger.error(s"Unknown message $unknown")
  }

  def fulfillOrder(order: Order): Future[StubHubFulfillmentResponse] = {
    val stubHubOrderId = order.sourceExternalTransactionId.toInt
    val tickets = order.orderItems.map(
      item => StubHubTicketWithBarcode(order.row, item.seatNumber, item.barcode.get)
    )

    stubHubService
      .fulfill(orderId = stubHubOrderId, ticketsWithBarcodes = tickets)
      .map {
        case ApiSuccess(_, body: StubHubFulfillmentResponse) => body
        case ApiError(code, body) =>
          throw new Exception(s"Skybox error: $code -- $body")
      }
  }
}

object StubHubFulfiller {

  def props(
    serviceHolder: ServiceHolder,
    stubHubService: StubHubService,
    notificationService: NotificationService
  ): Props = Props(new StubHubFulfiller(serviceHolder, stubHubService, notificationService))
}
