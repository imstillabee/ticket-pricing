package com.eventdynamic.ticketfulfillment

import com.eventdynamic.ticketfulfillment.models.Order

package object fulfillers {
  abstract class Message

  // messages
  case class FulfillOrder(order: Order)
  case class RejectOrder(order: Order)
}
