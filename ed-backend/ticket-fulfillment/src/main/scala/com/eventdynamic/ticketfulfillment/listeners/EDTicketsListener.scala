package com.eventdynamic.ticketfulfillment.listeners

import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{as, complete, entity, path, post}
import akka.http.scaladsl.server.Route
import com.eventdynamic.edtickets.models.EDPendingOrder
import com.eventdynamic.ticketfulfillment.coordinators.ProcessOrder
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import org.slf4j.LoggerFactory
import play.api.libs.json.Json

class EDTicketsListener(
  val coord: ActorRef,
  val serviceHolder: ServiceHolder,
  val clientId: Int,
  val integrationId: Int
) extends EDPendingOrder.format {

  private val logger = LoggerFactory.getLogger(this.getClass)

  /** Webhook for EDTickets incoming orders
    *
    * @return
    */
  def route: Route = {
    path("edtickets") {
      post {
        entity(as[String]) { edOrderString =>
          val edOrder = Json.parse(edOrderString).as[EDPendingOrder]
          logger.info("EDTickets listener received an order", edOrder)

          // Map edtickets order and send message to coordinator
          val order = EDTicketsPoller.transformOrder(edOrder, clientId, integrationId)
          coord ! ProcessOrder(order)

          // Acknowledge that the request was received
          complete(StatusCodes.OK)
        }
      }
    }
  }
}
