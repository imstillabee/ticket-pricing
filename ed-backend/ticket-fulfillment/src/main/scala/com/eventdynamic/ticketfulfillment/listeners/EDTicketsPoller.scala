package com.eventdynamic.ticketfulfillment.listeners

import java.sql.Timestamp
import java.time.Instant

import akka.actor.{Actor, ActorRef, Props, Timers}
import akka.pattern.pipe
import com.eventdynamic.SttpHelper
import com.eventdynamic.edtickets.EDTicketsService
import com.eventdynamic.edtickets.models.EDPendingOrder
import com.eventdynamic.models.TransactionType
import com.eventdynamic.services.NotificationService
import com.eventdynamic.ticketfulfillment.coordinators.ProcessOrder
import com.eventdynamic.ticketfulfillment.listeners.EDTicketsPoller.{PollException, PollSuccess}
import com.eventdynamic.ticketfulfillment.models._
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

case class EDTicketsPollerConfig(clientId: Int, integrationId: Int, pollFrequency: FiniteDuration)

class EDTicketsPoller(
  val conf: EDTicketsPollerConfig,
  val serviceHolder: ServiceHolder,
  val apiService: EDTicketsService,
  val coord: ActorRef,
  val notificationService: NotificationService
) extends Actor
    with Timers {
  timers.startPeriodicTimer(Poll, Poll, conf.pollFrequency)
  import context.dispatcher

  private val logger = LoggerFactory.getLogger(this.getClass)

  private var cache = List[(Timestamp, String)]()
  private val cacheExpireMillis = 1000 * 60 * 30 // 30 minutes

  override def receive: Receive = {
    case Poll =>
      logger.info("Polling EDTickets...")
      poll()
        .map(PollSuccess)
        .recover {
          case err => PollException(err)
        }
        .pipeTo(self)(sender())

    case PollSuccess(orders) if orders.isEmpty =>
      logger.info("EDTickets up to date")

    case PollSuccess(orders) =>
      logger.info(s"New EDTickets orders: ${orders.length}")
      orders.foreach(coord ! ProcessOrder(_))

    case PollException(err: Throwable) =>
      notificationService.sendAlert("An error occurred while polling for EDTickets orders", err)

    case unknown =>
      logger.error(s"Unknown message $unknown")
  }

  private def poll(): Future[Seq[Order]] = {
    apiService.getPendingOrders
      .map(SttpHelper.handleResponse)
      .map(res => {
        val edPendingOrders = res.orders
          .getOrElse(Seq.empty[EDPendingOrder])
          .filterNot(orderIsInCache)

        edPendingOrders.map(edPendingOrder => {
          EDTicketsPoller.transformOrder(
            edPendingOrder = edPendingOrder,
            clientId = conf.clientId,
            integrationId = conf.integrationId
          )
        })
      })
  }

  /**
    * Check the cache if we processed an order recently.  Clears outdated entries when they expire.
    *
    * @param order
    * @return true if the order is in the cache
    */
  private def orderIsInCache(order: EDPendingOrder): Boolean = {
    // Filter out expired records
    val now = Timestamp.from(Instant.now)
    cache = cache.filter(_._1.getTime + cacheExpireMillis > now.getTime)

    // Check to see if the cache contains this listing
    val inCache = cache.map(_._2).contains(order.id)

    // Add listing to the cache
    cache = cache :+ (now, order.id)

    inCache
  }

}

object EDTicketsPoller {
  private case class PollSuccess(orders: Seq[Order])
  private case class PollException(err: Throwable)

  def transformOrder(edPendingOrder: EDPendingOrder, clientId: Int, integrationId: Int): Order = {
    Order(
      clientId = clientId,
      integrationId = integrationId,
      sourceExternalTransactionId = edPendingOrder.id,
      externalEventId = edPendingOrder.eventId,
      timestamp = edPendingOrder.timestamp,
      isPrimary = false,
      section = edPendingOrder.section,
      row = edPendingOrder.row,
      transactionType = TransactionType.Purchase,
      orderItems = edPendingOrder.seats.map(
        seat => OrderItem(seatNumber = seat.seat.toString, price = edPendingOrder.price)
      )
    )
  }

  def props(
    eDTicketsPollerConfig: EDTicketsPollerConfig,
    serviceHolder: ServiceHolder,
    apiService: EDTicketsService,
    coord: ActorRef,
    notificationService: NotificationService
  ): Props = Props(
    new EDTicketsPoller(
      eDTicketsPollerConfig,
      serviceHolder,
      apiService,
      coord,
      notificationService
    )
  )
}
