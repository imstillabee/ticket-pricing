package com.eventdynamic.ticketfulfillment.listeners

import java.time.Instant

import akka.actor.{Actor, ActorRef, Props, Timers}
import akka.pattern.pipe
import com.eventdynamic.SttpHelper
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models._
import com.eventdynamic.models.jobconfigs.ticketfulfillment.SkyboxFulfillmentConfig
import com.eventdynamic.models.jobconfigs.{TicketFulfillmentConfig, TicketFulfillmentConfigFormat}
import com.eventdynamic.services.NotificationService
import com.eventdynamic.skybox.SkyboxService
import com.eventdynamic.skybox.models.SkyboxSoldInventory
import com.eventdynamic.ticketfulfillment.coordinators.ProcessOrder
import com.eventdynamic.ticketfulfillment.models.{Order, OrderItem}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

case class SkyboxPollerConfig(
  clientId: Int,
  jobType: JobType,
  integrationId: Int,
  pollFrequency: FiniteDuration
)

class SkyboxPoller(
  val conf: SkyboxPollerConfig,
  val serviceHolder: ServiceHolder,
  val apiService: SkyboxService,
  val coord: ActorRef,
  val notificationService: NotificationService,
  val skyboxClientIntegrationConfig: SkyboxClientIntegrationConfig
) extends Actor
    with Timers
    with TicketFulfillmentConfigFormat
    with SkyboxClientIntegrationConfig.format {
  timers.startPeriodicTimer(Poll, Poll, conf.pollFrequency)

  import SkyboxPoller._
  import context.dispatcher

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case Poll =>
      logger.info("Polling Skybox...")
      poll()
        .map(PollSuccess)
        .recover {
          case e => PollException(e)
        } pipeTo self

    case PollSuccess(orders) if orders.isEmpty =>
      logger.info("Skybox up to date")

    case PollSuccess(orders) =>
      logger.info(s"New Skybox orders: ${orders.length}")
      orders.foreach(coord ! ProcessOrder(_))

    case PollException(err: Throwable) =>
      notificationService.sendAlert("An error occurred while polling for orders", err)

    case unknown =>
      logger.error(s"Unknown message $unknown")
  }

  private def poll(): Future[Seq[Order]] = {
    val now = Instant.now()

    for {
      // Grab job config for last poll time
      jobConfig <- serviceHolder.jobConfigService
        .getConfig[TicketFulfillmentConfig](conf.clientId, JobType.TicketFulfillment)
        .map(_.get)

      // Get the events that we want to check inventory for
      season <- serviceHolder.seasonService.getCurrent(Some(conf.clientId), isAdmin = false).map {
        case None    => throw new Exception(s"Client ${conf.clientId} does not have a current season")
        case Some(s) => s
      }
      events <- serviceHolder.eventService.getAllBySeason(
        season.id.get,
        conf.clientId,
        isAdmin = false
      )

      // Send API request
      invoices <- apiService
        .getInvoices(
          events.flatMap(_.primaryEventId),
          jobConfig.skybox
            .map(_.lastTransactionPoll)
            .getOrElse(Instant.now())
        )
        .map(SttpHelper.handleResponse)
        .map(_.rows)

      // Save last transaction Poll
      _ <- serviceHolder.jobConfigService.updateConfig(
        conf.clientId,
        JobType.TicketFulfillment,
        jobConfig.copy(skybox = Some(SkyboxFulfillmentConfig(lastTransactionPoll = now)))
      )

    } yield {
      mapSkyboxInvoicesToOrders(invoices)
    }
  }

  private def mapSkyboxInvoicesToOrders(invoices: Seq[SkyboxSoldInventory]): Seq[Order] = {
    invoices
    // Filter out invoices for customer ids that we want to ignore based on the clientIntegarationConfig
      .filter(
        invoice =>
          !skyboxClientIntegrationConfig.customersToIgnore.contains(invoice.customerId.toString)
      )
      .map(invoice => {
        // Map the customer id for secondary markets like StubHub to the integration set up for them
        // like "Skybox - Stubhub"
        val integrationId = skyboxClientIntegrationConfig.customers
          .getOrElse(invoice.customerId.toString, conf.integrationId)
        Order(
          clientId = conf.clientId,
          integrationId = integrationId,
          sourceExternalTransactionId = invoice.id.toString,
          externalEventId = invoice.eventId.toString,
          timestamp = invoice.invoiceDate,
          isPrimary = true,
          section = invoice.section,
          row = invoice.row,
          transactionType = TransactionType.Purchase,
          orderItems = invoice.seatNumbersList
            .map(
              seatNumber =>
                OrderItem(seatNumber = seatNumber.toString, price = invoice.unitTicketSales)
            )
        )
      })
  }
}

object SkyboxPoller {
  sealed trait PollMessage

  case class PollSuccess(orders: Seq[Order]) extends PollMessage
  case class PollException(err: Throwable) extends PollMessage

  def props(
    skyboxPollerConfig: SkyboxPollerConfig,
    serviceHolder: ServiceHolder,
    apiService: SkyboxService,
    coord: ActorRef,
    notificationService: NotificationService,
    skyboxClientIntegrationConfig: SkyboxClientIntegrationConfig
  ): Props = Props(
    new SkyboxPoller(
      skyboxPollerConfig,
      serviceHolder,
      apiService,
      coord,
      notificationService,
      skyboxClientIntegrationConfig
    )
  )
}
