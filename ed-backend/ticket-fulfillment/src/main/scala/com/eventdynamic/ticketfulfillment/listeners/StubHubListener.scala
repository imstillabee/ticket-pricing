package com.eventdynamic.ticketfulfillment.listeners

import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.eventdynamic.stubhub.models.StubHubTransaction
import com.eventdynamic.ticketfulfillment.coordinators.ProcessOrder
import org.slf4j.LoggerFactory
import play.api.libs.json.Json

class StubHubListener(clientId: Int, integrationId: Int) extends StubHubTransaction.format {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Creates the routes necessary for the StubHub transaction listener
    */
  def route(coord: ActorRef): Route = {
    path("stubhub") {
      post {
        entity(as[String]) { body =>
          val stubHubTransaction = Json.parse(body).as[StubHubTransaction]
          logger.info("StubHub listener received an order", stubHubTransaction)

          val order =
            StubHubPoller.mapStubHubTransactionToOrder(stubHubTransaction, clientId, integrationId)
          coord ! ProcessOrder(order)

          complete(StatusCodes.OK)
        }
      }
    }
  }
}
