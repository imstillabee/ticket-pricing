package com.eventdynamic.ticketfulfillment.listeners

import java.sql.Timestamp
import java.time.Instant

import akka.actor.{Actor, ActorRef, Props, Timers}
import akka.pattern.pipe
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models.jobconfigs.{TicketFulfillmentConfig, TicketFulfillmentConfigFormat}
import com.eventdynamic.models.{JobType, TransactionType}
import com.eventdynamic.services.NotificationService
import com.eventdynamic.stubhub.StubHubService
import com.eventdynamic.stubhub.models.StubHubTransaction
import com.eventdynamic.ticketfulfillment.coordinators.ProcessOrder
import com.eventdynamic.ticketfulfillment.models.{Order, OrderItem}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

case class StubHubPollerConfig(
  clientId: Int,
  jobType: JobType,
  integrationId: Int,
  pollFrequency: FiniteDuration
)

class StubHubPoller(
  conf: StubHubPollerConfig,
  serviceHolder: ServiceHolder,
  apiService: StubHubService,
  coord: ActorRef,
  notificationService: NotificationService
) extends Actor
    with Timers
    with TicketFulfillmentConfigFormat {
  timers.startPeriodicTimer(Poll, Poll, conf.pollFrequency)
  import StubHubPoller._
  import context.dispatcher

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case Poll =>
      logger.info("Polling StubHub...")
      poll()
        .map(PollSuccess)
        .recover {
          case err => PollException(err)
        }
        .pipeTo(self)(sender())

    case PollSuccess(orders) if orders.isEmpty =>
      logger.info("Stubhub up to date")

    case PollSuccess(orders) =>
      logger.info(s"New StubHub orders: ${orders.length}")
      orders.foreach(coord ! ProcessOrder(_))

    case PollException(err: Throwable) =>
      notificationService.sendAlert("An error occurred while polling for StubHub orders", err)
      throw err

    case unknown =>
      logger.error(s"Unknown message $unknown")
  }

  private def poll(): Future[Seq[Order]] = {
    val now = Instant.now

    for {
      // Grab job config for last poll time
      jobConfig <- serviceHolder.jobConfigService
        .getConfig[TicketFulfillmentConfig](conf.clientId, JobType.TicketFulfillment)
        .map(_.get)

      // Send API request
      stubHubTransactions <- apiService
        .getStubHubTransactions(
          Timestamp.from(jobConfig.stubhub.map(_.lastTransactionPoll).getOrElse(Instant.EPOCH))
        )

      // Update job config with last poll time
      _ <- serviceHolder.jobConfigService.updateConfig(
        conf.clientId,
        JobType.TicketFulfillment,
        jobConfig.copy(stubhub = jobConfig.stubhub.map(_.copy(lastTransactionPoll = now)))
      )
    } yield {
      // Transform StubHub transactions to Orders
      stubHubTransactions.map(
        StubHubPoller
          .mapStubHubTransactionToOrder(_, conf.clientId, conf.integrationId)
      )
    }
  }
}

object StubHubPoller {
  case class PollSuccess(orders: Seq[Order])
  case class PollException(err: Throwable)

  def props(
    stubHubPollerConfig: StubHubPollerConfig,
    serviceHolder: ServiceHolder,
    apiService: StubHubService,
    coord: ActorRef,
    notificationService: NotificationService
  ): Props = Props(
    new StubHubPoller(stubHubPollerConfig, serviceHolder, apiService, coord, notificationService)
  )

  def mapStubHubTransactionToOrder(
    stubHubTransaction: StubHubTransaction,
    clientId: Int,
    integrationId: Int
  ): Order = {
    // TODO: there are a couple fields we should check up on
    // 1. "stubHubTransaction.saleDate" -- What format is this date and why is it a string coming out of the external service
    // 2. "stubHubTransaction.price" -- Is this the unit price or the total price?
    // 3. TransactionType -- We're assuming that these transactions are all purchases, is that valid?
    Order(
      clientId = clientId,
      integrationId = integrationId,
      sourceExternalTransactionId = stubHubTransaction.saleId,
      externalEventId = stubHubTransaction.eventId,
      timestamp = Instant.parse(stubHubTransaction.saleDate),
      isPrimary = false,
      section = stubHubTransaction.section,
      row = stubHubTransaction.row,
      transactionType = TransactionType.Purchase,
      orderItems = stubHubTransaction.seats
        .split(",")
        .map(seatNumber => {
          OrderItem(seatNumber = seatNumber, price = stubHubTransaction.price)
        })
    )
  }
}
