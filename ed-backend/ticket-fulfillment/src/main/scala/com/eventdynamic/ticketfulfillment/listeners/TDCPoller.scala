package com.eventdynamic.ticketfulfillment.listeners

import java.time.Instant

import akka.actor.{Actor, ActorRef, Props, Timers}
import akka.pattern.pipe
import com.eventdynamic.SttpHelper
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models._
import com.eventdynamic.models.jobconfigs.{TicketFulfillmentConfig, TicketFulfillmentConfigFormat}
import com.eventdynamic.services.NotificationService
import com.eventdynamic.tdcreplicatedproxy.TdcReplicatedProxyService
import com.eventdynamic.tdcreplicatedproxy.models.TdcTransaction
import com.eventdynamic.ticketfulfillment.coordinators.ProcessOrder
import com.eventdynamic.ticketfulfillment.listeners.TDCPoller.{PollException, PollSuccess}
import com.eventdynamic.ticketfulfillment.models.{Order, OrderItem}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.utils.HoldCodeHelper
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.concurrent.duration._

case class TDCPollerConfig(
  clientId: Int,
  jobType: JobType,
  integrationId: Int,
  pollFrequency: FiniteDuration
)

class TDCPoller(
  conf: TDCPollerConfig,
  serviceHolder: ServiceHolder,
  tdcReplicatedProxyService: TdcReplicatedProxyService,
  coord: ActorRef,
  notificationService: NotificationService
) extends Actor
    with Timers
    with TicketFulfillmentConfigFormat {
  timers.startPeriodicTimer(Poll, Poll, conf.pollFrequency)
  import context.dispatcher

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case Poll =>
      logger.info("Polling Tickets.com...")

      poll()
        .map(PollSuccess)
        .recover {
          case err => PollException(err)
        } pipeTo self

    case PollSuccess(orders) if orders.isEmpty =>
      logger.info("Tickets.com up to date")

    case PollSuccess(orders) =>
      logger.info(s"New Tickets.com orders: ${orders.length}")
      orders.foreach(coord ! ProcessOrder(_))

    case PollException(err) =>
      notificationService.sendAlert("An error occurred while polling for orders", err)

    case msg =>
      logger.error(s"Unknown message $msg")
  }

  private def poll(): Future[Seq[Order]] = {
    for {
      // Grab job config for last poll time
      jobConfig <- serviceHolder.jobConfigService
        .getConfig[TicketFulfillmentConfig](conf.clientId, JobType.TicketFulfillment)
        .map(_.get)

      // Get the events that we want to check inventory for
      // Only look at events in the current season
      season <- serviceHolder.seasonService.getCurrent(Some(conf.clientId), isAdmin = false).map {
        case None    => throw new Exception(s"No current season for client ${conf.clientId}")
        case Some(s) => s
      }
      events <- serviceHolder.eventService.getAllBySeason(
        season.id.get,
        conf.clientId,
        isAdmin = false
      )

      // Fetch transactions from the TDC replicated DB
      transactions <- tdcReplicatedProxyService
        .getTransactions(
          after = jobConfig.tdc.get.lastTransactionPoll,
          before = Instant.now(),
          eventIds = events.flatMap(_.primaryEventId)
        )
        .map(SttpHelper.handleResponse)

      // Get the new poll time.  If we don't pick up transactions, don't change the time because we might
      // have to wait for the replicated database to refresh
      newLastPollTime = if (transactions.isEmpty) jobConfig.tdc.get.lastTransactionPoll
      else transactions.map(_.createdAt).max

      // Update config with latest poll time
      _ <- serviceHolder.jobConfigService.updateConfig(
        conf.clientId,
        JobType.TicketFulfillment,
        jobConfig.copy(tdc = jobConfig.tdc.map(_.copy(lastTransactionPoll = newLastPollTime)))
      )
    } yield {
      mapTdcTransactionsToOrders(transactions)
    }
  }

  private def mapTdcTransactionsToOrders(transactions: Seq[TdcTransaction]): Seq[Order] = {
    // TDC transaction come across split up by seat. Group them by transaction ID to get all the
    // line items for the order. Group by eventId, section, row for safety.
    val orders =
      transactions.groupBy(
        t => (t.id, t.eventId, t.transactionTypeCode, t.section, t.row, t.priceScaleId)
      )
    orders
      .map(group => {
        val ((id, eventId, transactionTypeCode, section, row, priceScaleId), orderTransactions) =
          group
        Order(
          clientId = conf.clientId,
          integrationId = conf.integrationId,
          sourceExternalTransactionId = id.toString,
          externalEventId = eventId.toString,
          timestamp = orderTransactions.head.timestamp,
          isPrimary = true,
          section = section,
          row = row,
          transactionType = tdcTransactionTypeCodeToTransactionType(transactionTypeCode),
          externalPriceScaleId = Some(priceScaleId.toInt),
          orderItems = orderTransactions.map(
            transaction =>
              OrderItem(
                seatNumber = transaction.seatNumber,
                externalSeatId = Some(transaction.seatId.toInt),
                buyerTypeCode = Some(transaction.buyerTypeCode),
                price = transaction.price,
                isHeld = HoldCodeHelper.checkIsHeld(transaction.holdCodeType)
            )
          )
        )
      })
      .toSeq
  }

  private def tdcTransactionTypeCodeToTransactionType(code: String): TransactionType.Value = {
    val purchases = Set("SA", "CS", "ES")
    val returns = Set("ER", "RT")

    if (purchases.contains(code)) {
      TransactionType.Purchase
    } else if (returns.contains(code)) {
      TransactionType.Return
    } else {
      throw new Exception(s"Unknown transaction type code $code")
    }
  }
}

object TDCPoller {
  case class PollSuccess(orders: Seq[Order])
  case class PollException(err: Throwable)

  def props(
    conf: TDCPollerConfig,
    serviceHolder: ServiceHolder,
    tdcReplicatedProxyService: TdcReplicatedProxyService,
    coord: ActorRef,
    notificationService: NotificationService
  ): Props = Props(
    new TDCPoller(conf, serviceHolder, tdcReplicatedProxyService, coord, notificationService)
  )
}
