package com.eventdynamic.ticketfulfillment.models

import java.time.Instant

import com.eventdynamic.models.TransactionType.TransactionType

/**
  * Event Dynamic structure to generically track an order through the fulfillment pipeline
  *
  * @param clientId client the order is associated with, references the Clients table
  * @param integrationId source integration for the order, references the Integrations table
  * @param sourceExternalTransactionId order identifier where the order originated (secondary or primary)
  * @param externalEventId event identifier in the source integration
  * @param timestamp order timestamp
  * @param isPrimary whether the order originated on a primary integration
  * @param section venue section
  * @param row venue row
  * @param transactionType type of transaction (Purchase or Return)
  * @param orderItems items (i.e. seats or tickets) in the order
  * @param copyExternalTransactionId this will be filled with the primary transaction ID for secondary orders and it
  *                                  will be blank for primary orders
  */
case class Order(
  clientId: Int,
  integrationId: Int,
  sourceExternalTransactionId: String,
  externalEventId: String,
  timestamp: Instant,
  isPrimary: Boolean,
  section: String,
  row: String,
  transactionType: TransactionType,
  externalPriceScaleId: Option[Int] = None,
  orderItems: Seq[OrderItem],
  copyExternalTransactionId: Option[String] = None
)

/**
  * Item (i.e. seat or ticket) of an order
  *
  * @param seatNumber seat number value, e.g. "12"
  * @param price unit price for the ticket
  * @param externalSeatId seat identifier in the source integration
  * @param barcode barcode value (blank until reservation)
  * @param buyerTypeCode buyer type code value for Tickets.com orders, e.g. "ADULT" or "GROUP"
  * @param isHeld boolean for if seat is held via hold code
  */
case class OrderItem(
  seatNumber: String,
  price: BigDecimal,
  externalSeatId: Option[Int] = None,
  barcode: Option[String] = None,
  buyerTypeCode: Option[String] = None,
  isHeld: Boolean = false
)
