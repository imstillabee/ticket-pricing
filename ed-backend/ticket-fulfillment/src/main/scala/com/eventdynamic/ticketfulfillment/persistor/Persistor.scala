package com.eventdynamic.ticketfulfillment.persistor

import java.sql.Timestamp

import akka.actor.{Actor, Props}
import com.eventdynamic.models._
import com.eventdynamic.services._
import com.eventdynamic.ticketfulfillment.models.Order
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.utils.FutureUtil
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class Persistor(serviceHolder: ServiceHolder) extends Actor {
  private val logger = LoggerFactory.getLogger(this.getClass)

  val p = new PersistorHelper(
    serviceHolder.eventService,
    serviceHolder.eventSeatService,
    serviceHolder.priceScaleService,
    serviceHolder.revenueCategoryMappingService,
    serviceHolder.seatService,
    serviceHolder.transactionService
  )

  override def receive(): Receive = {
    case UpsertOrder(order: Order) =>
      p.upsertOrder(order).andThen {
        case Success(_) =>
          logger.debug(s"Successfully upserted order: $order")
        case Failure(err) =>
          throw err
          logger.error(s"Failed to upsert order: $order", err)
      }

    case unknown =>
      logger.error(s"Unsupported persistor message: $unknown")
  }
}

object Persistor {

  def props(serviceHolder: ServiceHolder): Props = Props(new Persistor(serviceHolder))
}

class PersistorHelper(
  eventService: EventService,
  eventSeatService: EventSeatService,
  priceScaleService: PriceScaleService,
  revenueCategoryMappingService: RevenueCategoryMappingService,
  seatService: SeatService,
  transactionService: TransactionService
) {
  case class UpsertResult(seatId: Int, eventSeatId: Int, transactionId: Int)

  /**
    * Upsert appropriate records based on whether the order is primary or secondary
    *
    * @param order ticket order
    * @return upsert result with created record ids
    */
  def upsertOrder(order: Order): Future[Seq[UpsertResult]] = {
    if (order.isPrimary) {
      upsertPrimaryOrder(order)
    } else {
      upsertSecondaryOrder(order)
    }
  }

  /**
    * Upsert Seat, Event Seat, and Transaction records for the order.
    *
    * @param order ticket order from primary integration
    * @return upsert result with created record ids
    */
  private def upsertPrimaryOrder(order: Order): Future[Seq[UpsertResult]] = {
    case class SeatIdentifier(seatNumber: String, row: String, section: String, venueId: Int)
    for {
      event <- eventService
        .getByIntegrationId(primaryEventId = order.externalEventId.toInt, clientId = order.clientId)
        .map {
          case Some(event) => event
          case None        => throw new Exception("Event not found")
        }

      priceScales <- priceScaleService.getAllForClient(clientId = order.clientId)
      priceScaleLookup = priceScales
        .map(priceScale => priceScale.integrationId -> priceScale)
        .toMap

      foundPriceScale = order.externalPriceScaleId match {
        case None                       => guessPriceScale(priceScales, order)
        case Some(externalPriceScaleId) => priceScaleLookup(externalPriceScaleId)
      }

      seatIds <- seatService.bulkUpsert(
        order.orderItems.map(
          orderItem =>
            Seat(
              primarySeatId = if (order.isPrimary) orderItem.externalSeatId else None,
              seat = orderItem.seatNumber.toString,
              row = order.row,
              section = order.section,
              venueId = event.venueId
          )
        )
      )

      seatIdsWithIsHeld = seatIds.zip(order.orderItems.map(_.isHeld))

      eventSeatIds <- eventSeatService.bulkUpsert(seatIdsWithIsHeld.map(seatIdWithIsHeld => {
        val (seatId, isHeld) = seatIdWithIsHeld
        EventSeat(
          seatId = seatId,
          eventId = event.id.get,
          priceScaleId = foundPriceScale.id.get,
          listedPrice = None,
          overridePrice = None,
          isListed = true,
          isHeld = isHeld
        )
      }))

      revenueCategoryMap <- FutureUtil
        .serializeFutures(order.orderItems.flatMap(_.buyerTypeCode).toSet)(buyerTypeCode => {
          revenueCategoryMappingService
            .regexMatch(order.clientId, buyerTypeCode)
            .map(buyerTypeCode -> _)
        })
        .map(_.collect {
          case (key, Some(value)) => key -> value
        }.toMap)

      transactionIds <- transactionService.bulkUpsert(
        (order.orderItems, eventSeatIds).zipped.map((orderItem, eventSeatId) => {
          Transaction(
            timestamp = Timestamp.from(order.timestamp),
            price = orderItem.price,
            eventSeatId = eventSeatId,
            integrationId = order.integrationId,
            buyerTypeCode = orderItem.buyerTypeCode.getOrElse("n/a"), // TODO: how do we want to handle integrations that don't have buyer types? Make this optional or keep defaulting to "ADULT"?
            transactionType = order.transactionType,
            revenueCategoryId =
              orderItem.buyerTypeCode.flatMap(code => revenueCategoryMap.get(code)),
            primaryTransactionId = Some(order.sourceExternalTransactionId)
          )
        })
      )
    } yield (seatIds, eventSeatIds, transactionIds).zipped.map(UpsertResult)
  }

  /**
    * Upsert Transaction for the order
    *
    * @param order ticket order from secondary integration
    * @return upsert result with created record ids
    */
  private def upsertSecondaryOrder(order: Order): Future[List[UpsertResult]] = {
    for {
      event <- eventService
        .getBySecondaryEventId(
          clientId = order.clientId,
          integrationId = order.integrationId,
          secondaryEventId = order.externalEventId
        )
        .map {
          case Some(e) => e
          case None    => throw new Exception("Event does not exist")
        }

      res <- FutureUtil.serializeFutures(order.orderItems)(orderItem => {
        for {
          eventSeat <- eventSeatService
            .getOne(
              EventSeatBySeatLocator(
                eventId = event.id.get,
                section = order.section,
                row = order.row,
                seat = orderItem.seatNumber
              )
            )
            .map {
              case Some(eventSeat) => eventSeat
              case None            => throw new Exception(s"Event Seat does not exist")
            }

          revenueCategoryId <- orderItem.buyerTypeCode match {
            case Some(buyerTypeCode) =>
              revenueCategoryMappingService
                .regexMatch(order.clientId, buyerTypeCode)
            case None => Future.successful(None)
          }

          transactionId <- transactionService.upsert(
            Transaction(
              timestamp = Timestamp.from(order.timestamp),
              price = orderItem.price,
              eventSeatId = eventSeat.id.get,
              integrationId = order.integrationId,
              buyerTypeCode = orderItem.buyerTypeCode.getOrElse("n/a"),
              transactionType = order.transactionType,
              revenueCategoryId = revenueCategoryId,
              secondaryTransactionId = Some(order.sourceExternalTransactionId),
              primaryTransactionId = order.copyExternalTransactionId
            )
          )
        } yield
          UpsertResult(
            seatId = eventSeat.seatId,
            eventSeatId = eventSeat.id.get,
            transactionId = transactionId
          )
      })
    } yield res
  }

  private def guessPriceScale(priceScales: Seq[PriceScale], order: Order): PriceScale = {
    if (priceScales.isEmpty) {
      throw new Exception(s"Cannot guess price scale because there are no price scales")
    }

    priceScales.head
  }
}
