package com.eventdynamic.ticketfulfillment

import com.eventdynamic.ticketfulfillment.models.Order

package object persistor {
  case class UpsertOrder(order: Order)
}
