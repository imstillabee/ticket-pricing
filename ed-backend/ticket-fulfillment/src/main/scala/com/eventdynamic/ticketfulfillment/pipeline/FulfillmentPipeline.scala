package com.eventdynamic.ticketfulfillment.pipeline

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.server.Route
import com.eventdynamic.IntegrationServiceBuilder
import com.eventdynamic.models._
import com.eventdynamic.models.jobconfigs.TicketFulfillmentConfig
import com.eventdynamic.services.NotificationService
import com.eventdynamic.ticketfulfillment.coordinators.Coordinator
import com.eventdynamic.ticketfulfillment.delisters.{EDTicketsDelister, StubHubDelister}
import com.eventdynamic.ticketfulfillment.fulfillers.{EDTicketsFulfiller, StubHubFulfiller}
import com.eventdynamic.ticketfulfillment.listeners._
import com.eventdynamic.ticketfulfillment.persistor.Persistor
import com.eventdynamic.ticketfulfillment.reservers.{SkyboxReserver, TDCReserver, TDCReserverConf}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import org.slf4j.LoggerFactory

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
  * Contains all actors and routes for a clients fulfillment pipeline.
  *
  * @param reserver
  * @param coordinator
  * @param fulfillers
  * @param delisters
  * @param pollers
  * @param routes
  */
case class FulfillmentPipeline(
  persistor: ActorRef,
  reserver: ActorRef,
  coordinator: ActorRef,
  fulfillers: Map[Int, ActorRef],
  delisters: Map[Int, ActorRef],
  pollers: Map[Int, ActorRef],
  routes: Map[Int, Route]
)

object FulfillmentPipeline
    extends SkyboxClientIntegrationConfig.format
    with TDCClientIntegrationConfig.format
    with StubHubClientIntegrationConfig.format
    with EDTicketsClientIntegrationConfig.format {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Builds a pipeline for a client.
    *
    * @param clientId
    * @param serviceHolder
    * @param actorSystem
    * @param jobConfig
    * @param serviceBuilder
    * @return
    */
  def buildPipelineForClient(
    clientId: Int,
    serviceHolder: ServiceHolder,
    actorSystem: ActorSystem,
    jobConfig: TicketFulfillmentConfig,
    serviceBuilder: IntegrationServiceBuilder
  ): FulfillmentPipeline = {
    logger.info(s"Building pipeline for client with id $clientId")

    // Exclude Skybox subsidiary integrations
    val skyboxSecondaries = Set("Skybox - Vivid Seats", "Skybox - Stubhub")

    val pipelineFuture = for {
      integrations <- serviceHolder.clientIntegrationService
      // TODO: EVNT-928, consider the use case we disable an integration, but want to continue syncing existing records
        .getByClientId(clientId, true)
        .map(_.filter(i => !skyboxSecondaries.contains(i.name)))
    } yield {
      // Get the primary integration
      val primaryIntegration = integrations
        .find(_.isPrimary)
        .getOrElse({
          val notificationService = new NotificationService(clientId, Option.empty)
          val exception =
            new Exception(s"No primary integration found for client with id $clientId")
          notificationService.sendAlert(exception.getMessage)

          throw exception;
        })

      // Get the names of the secondaries
      val secondaryIntegrations =
        integrations.filter(!_.isPrimary)
      logger.info(s"Primary integration: ${primaryIntegration.name}")
      logger.info(s"Secondary integrations: ${secondaryIntegrations.map(_.name).mkString(", ")}")

      // Build the persistor
      val persistor = actorSystem.actorOf(Persistor.props(serviceHolder))

      // Build the pipeline units
      val reserver =
        buildReserver(
          clientId,
          primaryIntegration,
          serviceHolder,
          actorSystem,
          jobConfig,
          serviceBuilder
        )
      val fulfillers =
        buildFulfillers(clientId, secondaryIntegrations, serviceHolder, actorSystem, serviceBuilder)
      val delisters =
        buildDelisters(clientId, secondaryIntegrations, serviceHolder, actorSystem, serviceBuilder)
      val coordinator =
        actorSystem.actorOf(Coordinator.props(persistor, reserver, fulfillers, delisters))
      val (listeners, routes) =
        buildListeners(
          clientId,
          integrations,
          serviceHolder,
          actorSystem,
          coordinator,
          serviceBuilder
        )

      //logger.error(s"Got to end of pipeline build $secondaryClientIntegrations")

      // Return the pipeline
      FulfillmentPipeline(
        persistor,
        reserver,
        coordinator,
        fulfillers,
        delisters,
        listeners,
        routes
      )
    }

    Await.result(pipelineFuture, Duration.Inf)
  }

  /**
    * Builds a reserver specific to a primary integration.
    *
    * @param clientId
    * @param integration
    * @param serviceHolder
    * @param actorSystem
    * @param jobConfig
    * @param serviceBuilder
    * @return
    */
  def buildReserver(
    clientId: Int,
    integration: ClientIntegrationComposite,
    serviceHolder: ServiceHolder,
    actorSystem: ActorSystem,
    jobConfig: TicketFulfillmentConfig,
    serviceBuilder: IntegrationServiceBuilder
  ): ActorRef = {
    integration.name match {
      case "Tickets.com" =>
        logger.info("Building Tickets.com reserver")

        // Build config from job config
        if (jobConfig.tdc.isEmpty)
          throw new Exception("Missing TDC configuration, unable to build TDC reserver.")
        val tdcConf = TDCReserverConf(
          jobConfig.tdc.get.paymentMethodId,
          jobConfig.tdc.get.deliveryMethodId,
          jobConfig.tdc.get.patronAccountId,
          jobConfig.tdc.get.defaultBuyerTypeId
        )
        val proVenueService =
          serviceBuilder.buildProVenueService(integration.config[TDCClientIntegrationConfig])
        val notificationService = new NotificationService(clientId, Option("Tickets.com"))
        actorSystem.actorOf(
          TDCReserver
            .props(
              serviceHolder = serviceHolder,
              proVenueService = proVenueService,
              conf = tdcConf,
              reissueDelay = 10.seconds,
              notificationService = notificationService,
              clientIntegrationComposite = integration
            )
        )
      case "Skybox" =>
        // Create the Skybox reserver
        val clientIntegrationConfig = integration.config[SkyboxClientIntegrationConfig]
        val notificationService = new NotificationService(clientId, Option("Skybox"))
        val skyboxService = serviceBuilder.buildSkyboxService(clientIntegrationConfig)
        actorSystem.actorOf(
          SkyboxReserver
            .props(
              clientIntegrationConfig,
              serviceHolder,
              notificationService,
              skyboxService,
              integration
            )
        )
      case s =>
        throw new UnsupportedOperationException(s"Reserver for $s not supported")
    }
  }

  /**
    * Builds fulfillers specific to secondary integrations.
    *
    * @param clientId
    * @param integrations
    * @param serviceHolder
    * @param actorSystem
    * @param serviceBuilder
    * @return
    */
  def buildFulfillers(
    clientId: Int,
    integrations: Seq[ClientIntegrationComposite],
    serviceHolder: ServiceHolder,
    actorSystem: ActorSystem,
    serviceBuilder: IntegrationServiceBuilder
  ): Map[Int, ActorRef] = {
    integrations
      .map(integration => {
        // Create the fulfiller actor for this integration
        val actor = integration.name match {
          case "EDTickets" =>
            logger.info("Building EDTickets fulfiller.")
            val edTicketsService = serviceBuilder
              .buildEDTicketsService(integration.config[EDTicketsClientIntegrationConfig])
            val notificationService = new NotificationService(clientId, Option("EDTickets"))
            actorSystem.actorOf(
              EDTicketsFulfiller.props(serviceHolder, edTicketsService, notificationService)
            )
          case "StubHub" =>
            logger.info("Build StubHub fulfiller.")
            val notificationService = new NotificationService(clientId, Option("StubHub"))
            val stubHubService =
              serviceBuilder.buildStubHubService(integration.config[StubHubClientIntegrationConfig])
            actorSystem
              .actorOf(
                StubHubFulfiller
                  .props(serviceHolder, stubHubService, notificationService)
              )
          case name =>
            throw new UnsupportedOperationException(s"Fulfiller for $name not supported")
        }

        // Return tuple with integration and actor
        integration.integrationId -> actor
      })
      .toMap
  }

  /**
    * Builds delisters specific to secondary integrations.
    *
    * @param clientId
    * @param integrations
    * @param serviceHolder
    * @param actorSystem
    * @param serviceBuilder
    * @return
    */
  def buildDelisters(
    clientId: Int,
    integrations: Seq[ClientIntegrationComposite],
    serviceHolder: ServiceHolder,
    actorSystem: ActorSystem,
    serviceBuilder: IntegrationServiceBuilder
  ): Map[Int, ActorRef] = {
    integrations
      .map(integration => {
        val actor = integration.name match {
          case "EDTickets" =>
            logger.info("Building EDTickets delister.")
            val edTicketsService = serviceBuilder
              .buildEDTicketsService(integration.config[EDTicketsClientIntegrationConfig])
            val notificationService = new NotificationService(clientId, Option("EDTickets"))
            actorSystem.actorOf(
              EDTicketsDelister
                .props(serviceHolder, edTicketsService, integration.id.get, notificationService)
            )
          case "StubHub" =>
            logger.info("Building StubHub delister.")
            val stubHubService =
              serviceBuilder.buildStubHubService(integration.config[StubHubClientIntegrationConfig])
            val notificationService = new NotificationService(clientId, Option("StubHub"))
            actorSystem.actorOf(
              StubHubDelister
                .props(serviceHolder, stubHubService, integration.id.get, notificationService)
            )
          case name =>
            throw new UnsupportedOperationException(s"Delister for $name not supported.")
        }

        // Return actor with integration id as tuple
        integration.integrationId -> actor
      })
      .toMap
  }

  /**
    * Builds listeners and routes specific to secondary integrations.
    *
    * @param clientId
    * @param integrations
    * @param serviceHolder
    * @param actorSystem
    * @param coordinator
    * @param serviceBuilder
    * @return
    */
  def buildListeners(
    clientId: Int,
    integrations: Seq[ClientIntegrationComposite],
    serviceHolder: ServiceHolder,
    actorSystem: ActorSystem,
    coordinator: ActorRef,
    serviceBuilder: IntegrationServiceBuilder
  ): (Map[Int, ActorRef], Map[Int, Route]) = {
    // Build the pollers for each integration
    val pollers = integrations
      .map(integration => {
        val actor = integration.name match {
          case "EDTickets" =>
            logger.info("Building EDTickets poller.")
            val eDTicketsListenerConfig =
              EDTicketsPollerConfig(
                clientId = integration.clientId,
                integrationId = integration.integrationId,
                pollFrequency = 60.seconds
              )
            val edTicketsService = serviceBuilder
              .buildEDTicketsService(integration.config[EDTicketsClientIntegrationConfig])
            val notificationService = new NotificationService(clientId, Option("EDTickets"))
            actorSystem.actorOf(
              EDTicketsPoller
                .props(
                  eDTicketsListenerConfig,
                  serviceHolder,
                  edTicketsService,
                  coordinator,
                  notificationService
                )
            )

          case "Tickets.com" =>
            logger.info("Building Tickets.com poller.")
            val tdcListenerConfig = TDCPollerConfig(
              integration.clientId,
              JobType.TicketFulfillment,
              integration.integrationId,
              60.seconds // TODO replace with config
            )
            val tdcReplicatedProxyService = serviceBuilder
              .buildTdcReplicatedProxyService(integration.config[TDCClientIntegrationConfig])
            val notificationService = new NotificationService(clientId, Option("Tickets.com"))
            actorSystem.actorOf(
              TDCPoller
                .props(
                  tdcListenerConfig,
                  serviceHolder,
                  tdcReplicatedProxyService,
                  coordinator,
                  notificationService
                )
            )

          case "Skybox" =>
            logger.info("Building Skybox poller.")

            val skyboxPollerConfig = SkyboxPollerConfig(
              integration.clientId,
              JobType.TicketFulfillment,
              integration.integrationId,
              60.seconds
            )
            val skyboxService =
              serviceBuilder.buildSkyboxService(integration.config[SkyboxClientIntegrationConfig])
            val notificationService = new NotificationService(clientId, Option("SkyBox"))
            actorSystem
              .actorOf(
                SkyboxPoller.props(
                  skyboxPollerConfig,
                  serviceHolder,
                  apiService = skyboxService,
                  coord = coordinator,
                  notificationService = notificationService,
                  skyboxClientIntegrationConfig = integration.config[SkyboxClientIntegrationConfig]
                )
              )

          case "StubHub" =>
            logger.info("Building StubHub poller.")

            var stubHubPollerConfig = StubHubPollerConfig(
              integration.clientId,
              JobType.TicketFulfillment,
              integration.integrationId,
              60.seconds
            )
            var stubhubService =
              serviceBuilder.buildStubHubService(integration.config[StubHubClientIntegrationConfig])
            val notificationService = new NotificationService(clientId, Option("StubHub"))
            actorSystem
              .actorOf(
                StubHubPoller.props(
                  stubHubPollerConfig,
                  serviceHolder,
                  stubhubService,
                  coordinator,
                  notificationService
                )
              )

          case name =>
            throw new UnsupportedOperationException(s"Poller for $name not supported.")
        }

        // Return actor with integration id as tuple
        integration.integrationId -> actor
      })
      .toMap

    // Build the routes for each integration
    val routes = integrations
      .flatMap(integration => {
        val route = integration.name match {
          case "EDTickets" =>
            logger.info("Building EDTickets route.")
            Some(
              new EDTicketsListener(
                coord = coordinator,
                serviceHolder = serviceHolder,
                clientId = integration.clientId,
                integrationId = integration.integrationId
              ).route
            )

          case name =>
            logger.info(s"Route for $name not supported.")
            None
        }

        // Return route tupled with integration id
        if (route.isDefined)
          Some(integration.integrationId -> route.get)
        else
          None
      })
      .toMap

    (pollers, routes)
  }
}
