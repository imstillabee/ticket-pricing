package com.eventdynamic.ticketfulfillment.reservers

import akka.actor.{Actor, Props}
import akka.pattern.pipe
import com.eventdynamic.SttpHelper
import com.eventdynamic.models.{ClientIntegrationComposite, SkyboxClientIntegrationConfig}
import com.eventdynamic.services.NotificationService
import com.eventdynamic.skybox.SkyboxService
import com.eventdynamic.skybox.models.SkyboxInvoice
import com.eventdynamic.ticketfulfillment.coordinators._
import com.eventdynamic.ticketfulfillment.models.Order
import com.eventdynamic.ticketfulfillment.reservers.SkyboxReserver._
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import org.slf4j.LoggerFactory

import scala.concurrent.Future

class SkyboxReserver(
  skyboxClientIntegrationConfig: SkyboxClientIntegrationConfig,
  serviceHolder: ServiceHolder,
  notificationService: NotificationService,
  skyboxService: SkyboxService,
  clientIntegrationComposite: ClientIntegrationComposite
) extends Actor {
  import context.dispatcher

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case ReserveTickets(order: Order) =>
      reserveTickets(order)
        .map(invoice => {
          val soldInventoryId = invoice.lines.head.inventoryIds.head
          val updatedOrder = order.copy(copyExternalTransactionId = Some(soldInventoryId.toString))
          ReserveTicketsResponseSuccessful(updatedOrder, invoice)
        })
        .recover {
          case err => ReserveTicketsResponseFailed(order, err)
        }
        .pipeTo(self)(sender())

    case ReserveTicketsResponseSuccessful(order, invoice) =>
      sender() ! ReservationSuccessful(order, deliveryInfo = invoice)

    case ReserveTicketsResponseFailed(order, err) =>
      val message = s"Error reserving tickets on Skybox for order $order"
      notificationService.sendAlert(message, err)

      sender() ! ReservationFailed(order)

    case PrintTickets(order: Order, invoice: SkyboxInvoice) =>
      printTickets(order, invoice)
        .map(barcodeOrder => PrintTicketsResponseSuccessful(barcodeOrder))
        .recover {
          case err =>
            PrintTicketsResponseFailed(order, err)
        }
        .pipeTo(self)(sender())

    case PrintTicketsResponseSuccessful(order) =>
      sender() ! PrintSuccessful(order)

    case PrintTicketsResponseFailed(order, err) =>
      val message = s"Error getting tickets from Skybox for order $order"
      notificationService.sendAlert(message, err)

      // No reissue for now because the barcodes are either uploaded or not, and
      // attempting to get them again won't help
      sender() ! PrintFailed(order)

    case m =>
      logger.warn(s"Unsupported Skybox reserver message: $m")
  }

  /**
    * Remove tickets from sale by reserving them
    *
    * @param order Order information for the reservation
    * @return Created Skybox invoice
    */
  private def reserveTickets(order: Order): Future[SkyboxInvoice] = {
    for {
      // Lookup Event
      event <- serviceHolder.eventService
        .getBySecondaryEventId(
          clientId = order.clientId,
          integrationId = order.integrationId,
          secondaryEventId = order.externalEventId
        )
        .map {
          case Some(e) => e
          case None    => throw new Exception("Event does not exist")
        }

      tickets <- skyboxService
        .getTickets(
          eventId = event.primaryEventId.get,
          section = Some(order.section),
          row = Some(order.row),
          seatNumbers = order.orderItems.map(_.seatNumber),
          None,
          None
        )
        .map(SttpHelper.handleResponse)

      customerId = getCustomerForIntegration(order.integrationId)
      invoice <- skyboxService
        .createInvoice(
          customerId = customerId,
          amount = order.orderItems.map(_.price).sum,
          ticketIds = tickets.map(_.id)
        )
        .map(SttpHelper.handleResponse)
    } yield invoice
  }

  /**
    * Look up the Skybox customer associated with the order's originating integration
    * using the customers map in the Skybox client integration config.
    *
    * @param integrationId originating integration ID
    * @return Skybox customer ID
    */
  private def getCustomerForIntegration(integrationId: Int): Int = {
    val customerId = skyboxClientIntegrationConfig.getCustomerIdForIntegrationId(integrationId)
    if (customerId.isEmpty) {
      throw new Exception(s"No Skybox customerId mapping for integration $integrationId")
    }
    customerId.get.toInt
  }

  /**
    * Get barcodes for an order
    *
    * @param order Original order
    * @param invoice Skybox invoice ID (from reservation)
    * @return Order with barcodes
    */
  private def printTickets(order: Order, invoice: SkyboxInvoice): Future[Order] = {
    for {
      // Lookup event
      event <- serviceHolder.eventService
        .getBySecondaryEventId(
          clientId = order.clientId,
          integrationId = order.integrationId,
          secondaryEventId = order.externalEventId
        )
        .map {
          case Some(e) => e
          case None    => throw new Exception("Event does not exist")
        }

      tickets <- skyboxService
        .getTickets(
          eventId = event.primaryEventId.get,
          section = Some(order.section),
          row = Some(order.row),
          seatNumbers = order.orderItems.map(_.seatNumber),
          None,
          None
        )
        .map(SttpHelper.handleResponse)
    } yield {
      val orderItems = order.orderItems.map(item => {
        val ticket = tickets.find(_.seatNumber == item.seatNumber.toInt).get
        if (ticket.barCode.isEmpty) {
          throw new Exception("No barcode")
        }
        item.copy(barcode = ticket.barCode)
      })

      order.copy(orderItems = orderItems)
    }
  }
}

object SkyboxReserver {

  def props(
    skyboxClientIntegrationConfig: SkyboxClientIntegrationConfig,
    serviceHolder: ServiceHolder,
    notificationService: NotificationService,
    skyboxService: SkyboxService,
    clientIntegrationComposite: ClientIntegrationComposite
  ): Props = Props(
    new SkyboxReserver(
      skyboxClientIntegrationConfig,
      serviceHolder,
      notificationService,
      skyboxService,
      clientIntegrationComposite
    )
  )

  private case class ReserveTicketsResponseSuccessful(order: Order, invoice: SkyboxInvoice)
  private case class ReserveTicketsResponseFailed(order: Order, err: Throwable)

  private case class GetBarcodes(order: Order, invoice: SkyboxInvoice)
  private case class PrintTicketsResponseSuccessful(order: Order)
  private case class PrintTicketsResponseFailed(order: Order, err: Throwable)
}
