package com.eventdynamic.ticketfulfillment.reservers

import akka.actor.{Actor, ActorRef, Props, Timers}
import akka.pattern.pipe
import com.eventdynamic.SttpHelper
import com.eventdynamic.models.ClientIntegrationComposite
import com.eventdynamic.provenue.ProVenueService
import com.eventdynamic.provenue.models._
import com.eventdynamic.services.NotificationService
import com.eventdynamic.ticketfulfillment.coordinators._
import com.eventdynamic.ticketfulfillment.models.Order
import com.eventdynamic.ticketfulfillment.reservers.TDCReserver._
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.utils.FutureUtil
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.Success

case class TDCReserverConf(
  paymentMethodId: Int,
  deliveryMethodId: Int,
  patronAccountId: Int,
  defaultBuyerTypeId: Int
)

class TDCReserver(
  s: ServiceHolder,
  proVenueService: ProVenueService,
  conf: TDCReserverConf,
  reissueDelay: FiniteDuration,
  notificationService: NotificationService,
  clientIntegrationComposite: ClientIntegrationComposite
) extends Actor
    with Timers {
  import context.dispatcher

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    /**
      * External message handling
      */
    case ReserveTickets(order: Order) =>
      reserveTickets(order, conf.defaultBuyerTypeId)
        .map(checkoutResponse => {
          val updatedOrder =
            order.copy(copyExternalTransactionId = Some(checkoutResponse.transactionId.toString))
          ReserveTicketsResponseSuccess(updatedOrder, checkoutResponse)
        })
        .recover {
          case err => ReserveTicketsResponseFail(order, err)
        }
        .pipeTo(self)(sender())

    case PrintTickets(order, deliveryInfo: Int) =>
      printTickets(deliveryInfo, order, reissue = false)
        .map(PrintTicketsResponseSuccess)
        .recover {
          case err =>
            notificationService.sendAlert("Error printing tickets (first attempt)", err)
            PrintTicketsResponseFail(order, deliveryInfo)
        }
        .pipeTo(self)(sender())

    /**
      * Internal message handling
      */
    case ReserveTicketsResponseSuccess(order, checkoutResponseCart) =>
      logger.info("Reservation successful")

      sender() ! ReservationSuccessful(
        order,
        deliveryInfo = checkoutResponseCart.deliveries.head.id
      )

    case ReserveTicketsResponseFail(order, err) =>
      val message = s"Error reserving tickets on Tickets.com for order $order"
      notificationService.sendAlert(message, err)
      sender() ! ReservationFailed(order)

    case PrintTicketsResponseSuccess(order) =>
      sender() ! PrintSuccessful(order)

    case PrintTicketsResponseFail(order, deliveryId) =>
      logger.info("Queueing reissue print.")
      timers.startSingleTimer(
        key = ReissuePrintTickets,
        msg = ReissuePrintTickets(order, deliveryId, sender()),
        timeout = reissueDelay
      )

    case ReissuePrintTickets(order, deliveryId, sender) =>
      logger.info(s"Reissue printing order with id ${order.sourceExternalTransactionId}")
      printTickets(deliveryId, order, reissue = true)
        .map(PrintSuccessful)
        .recover {
          case err =>
            notificationService.sendAlert("Error printing tickets (second attempt)", err)
            PrintFailed(order)
        }
        .pipeTo(sender)

    case unknown =>
      logger.error(s"Unknown message $unknown")
  }

  private def reserveTickets(
    order: Order,
    defaultBuyerTypeId: Int
  ): Future[ProVenueCheckoutResponseCart] = {
    for {
      event <- s.eventService
        .getBySecondaryEventId(
          clientId = order.clientId,
          integrationId = order.integrationId,
          secondaryEventId = order.externalEventId
        )
        .map {
          case Some(e) => e
          case None    => throw new Exception("Event does not exist")
        }

      // Lookup seats
      seats <- FutureUtil.serializeFutures(order.orderItems, 10)(orderItem => {
        s.seatService
          .get(
            section = Some(order.section),
            row = Some(order.row),
            seat = Some(orderItem.seatNumber),
            venueId = Some(event.venueId)
          )
          .map(_.head)
      })

      // Lock seats in TDC to put the tickets in a cart
      lockSeatsResponse <- lockSeats(
        integrationEventId = event.primaryEventId.get,
        buyerTypeId = defaultBuyerTypeId,
        integrationSeatIds = seats.map(_.primarySeatId.get)
      )

      // Checkout cart in TDC
      checkoutResponse <- checkout(
        cartId = lockSeatsResponse.id,
        amount = lockSeatsResponse.totalSalesRevenue,
        deliveryMethodId = conf.deliveryMethodId,
        paymentMethodId = conf.paymentMethodId,
        transactionPatronAccountId = conf.patronAccountId
      )
    } yield checkoutResponse
  }

  private def lockSeats(
    integrationEventId: Int,
    buyerTypeId: Int,
    integrationSeatIds: Seq[Int]
  ): Future[ProVenueLockSeatsResponse] = {
    logger.debug(
      s"Locking seats: ${Map("integrationEventId" -> integrationEventId, "buyerTypeId" -> buyerTypeId, "integrationSeatIds" -> integrationSeatIds)}"
    )
    proVenueService
      .lockSeats(integrationEventId, buyerTypeId, integrationSeatIds)
      .andThen {
        case Success(response) if !response.isSuccess || response.body.isLeft =>
          notificationService.sendAlert(s"ProVenue error locking seats: ${response.body}")
      }
      .map(SttpHelper.handleResponse)
  }

  private def checkout(
    cartId: Int,
    amount: ProVenueMoney,
    deliveryMethodId: Int,
    paymentMethodId: Int,
    transactionPatronAccountId: Int
  ): Future[ProVenueCheckoutResponseCart] = {
    logger.debug(s"Checking out cart: ${Map(
      "cartId" -> cartId,
      "amount" -> amount.value,
      "deliveryMethodId" -> deliveryMethodId,
      "paymentMethodId" -> paymentMethodId,
      "patronAccountId" -> transactionPatronAccountId
    )}")

    proVenueService
      .checkout(cartId, amount, deliveryMethodId, paymentMethodId, transactionPatronAccountId)
      .andThen {
        case Success(response) if !response.isSuccess =>
          notificationService.sendAlert(
            s"ProVenue error checking out: ${response.code} - ${response.body.left.getOrElse("")}"
          )
      }
      .map(SttpHelper.handleResponse)
      .map(_.cart)
  }

  private def printTickets(deliveryId: Int, order: Order, reissue: Boolean): Future[Order] = {
    for {
      // Lookup event
      event <- s.eventService
        .getBySecondaryEventId(
          clientId = order.clientId,
          integrationId = order.integrationId,
          secondaryEventId = order.externalEventId
        )
        .map {
          case Some(e) => e
          case None    => throw new Exception("Event does not exist")
        }

      // Lookup seats
      seats <- FutureUtil.serializeFutures(order.orderItems, 10)(orderItem => {
        s.seatService
          .get(
            section = Some(order.section),
            row = Some(order.row),
            seat = Some(orderItem.seatNumber),
            venueId = Some(event.venueId)
          )
          .map(_.head)
      })

      printResult <- proVenueService.print(deliveryId, reissue).map(SttpHelper.handleResponse)
    } yield {
      // Assign barcodes from the TDC Print Result to individual Order Items
      val orderItems =
        order.orderItems.map(orderItem => {
          // TODO: Handle failed lookup? i.e. handle the gets?
          val seat = seats.find(seat => seat.seat == orderItem.seatNumber).get
          val ticket = printResult.printItems
            .map(_.ticket)
            .find(_.seatId == seat.primarySeatId.get)
          val barcode = ticket.get.barcode
          orderItem.copy(barcode = Some(barcode))
        })

      order.copy(orderItems = orderItems)
    }
  }
}

object TDCReserver {

  def props(
    serviceHolder: ServiceHolder,
    proVenueService: ProVenueService,
    conf: TDCReserverConf,
    reissueDelay: FiniteDuration = 10.seconds,
    notificationService: NotificationService,
    clientIntegrationComposite: ClientIntegrationComposite
  ): Props = Props(
    new TDCReserver(
      serviceHolder,
      proVenueService,
      conf,
      reissueDelay,
      notificationService,
      clientIntegrationComposite
    )
  )

  // Private messages
  private case class ReserveTicketsResponseSuccess(
    order: Order,
    checkoutResponse: ProVenueCheckoutResponseCart
  )
  private case class ReserveTicketsResponseFail(order: Order, err: Throwable)
  private case class PrintTicketsResponseSuccess(order: Order)
  private case class PrintTicketsResponseFail(order: Order, deliveryId: Int)
  private case class ReissuePrintTickets(order: Order, deliveryId: Int, sender: ActorRef)
}
