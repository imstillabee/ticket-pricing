package com.eventdynamic.ticketfulfillment

import com.eventdynamic.ticketfulfillment.models.Order

package object reservers {
  case class ReserveTickets(order: Order)

  /**
    * Request to print tickets after they have been reserved.
    *
    * @param order order without barcodes
    * @param deliveryInfo info to identify the delivery, passed back from coordinators.ReservationSuccessful
    */
  case class PrintTickets[T](order: Order, deliveryInfo: T)
}
