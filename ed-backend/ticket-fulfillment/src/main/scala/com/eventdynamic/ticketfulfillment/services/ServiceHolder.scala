package com.eventdynamic.ticketfulfillment.services

import com.eventdynamic.db.EDContext
import com.eventdynamic.services._
import com.eventdynamic.transactions._

import scala.concurrent.ExecutionContext

case class ServiceHolder(
  clientIntegrationService: ClientIntegrationService,
  transactionService: TransactionService,
  integrationTransactions: IntegrationTransactions,
  jobConfigService: JobConfigService,
  eventService: EventService,
  seatService: SeatService,
  eventSeatService: EventSeatService,
  clientIntegrationEventsService: ClientIntegrationEventsService,
  priceScaleService: PriceScaleService,
  revenueCategoryMappingService: RevenueCategoryMappingService,
  seasonService: SeasonService
)

object ServiceHolder {

  def apply()(implicit ed: EDContext, executionContext: ExecutionContext): ServiceHolder = {
    val integrationService = new IntegrationService(ed)
    val clientIntegrationService = new ClientIntegrationService(ed)
    val clientIntegrationEventsService = new ClientIntegrationEventsService(ed)
    val clientIntegrationEventSeatsService = new ClientIntegrationEventSeatsService(ed)
    val clientIntegrationListingsService = new ClientIntegrationListingsService(ed)
    val eventSeatService = new EventSeatService(ed)
    val eventService = new EventService(ed)
    val seatService = new SeatService(ed)
    val transactionService = new TransactionService(ed)
    val revenueCategoryMappingService =
      new RevenueCategoryMappingService(ed)
    val jobConfigService = new JobConfigService(ed)
    val priceScaleService = new PriceScaleService(ed)
    val seasonTransaction =
      new SeasonTransaction(ed, eventService, eventSeatService, transactionService)
    val seasonService = new SeasonService(ed, seasonTransaction)

    val integrationTransactions =
      new IntegrationTransactions(
        ed,
        clientIntegrationService,
        clientIntegrationEventsService,
        clientIntegrationEventSeatsService,
        clientIntegrationListingsService,
        eventService,
        eventSeatService,
        integrationService,
        seatService
      )

    ServiceHolder(
      clientIntegrationService,
      transactionService,
      integrationTransactions,
      jobConfigService,
      eventService,
      seatService,
      eventSeatService,
      clientIntegrationEventsService,
      priceScaleService,
      revenueCategoryMappingService,
      seasonService
    )
  }
}
