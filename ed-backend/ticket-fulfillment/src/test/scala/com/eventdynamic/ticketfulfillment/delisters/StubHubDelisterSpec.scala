package com.eventdynamic.ticketfulfillment.delisters

import java.time.Instant

import akka.actor.ActorSystem
import akka.testkit._
import com.eventdynamic.models.{EventSeatBySeatLocator, TransactionType}
import com.eventdynamic.services.{NotificationService, _}
import com.eventdynamic.stubhub.StubHubService
import com.eventdynamic.stubhub.models._
import com.eventdynamic.ticketfulfillment.models.{Order, OrderItem}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.transactions.{ClientIntegrationTicket, IntegrationTransactions}
import com.eventdynamic.utils.RandomModelUtil
import com.typesafe.config.ConfigFactory
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future
import scala.concurrent.duration._

class StubHubDelisterSpec
    extends TestKit(ActorSystem("TestActor", ConfigFactory.empty()))
    with WordSpecLike
    with MockitoSugar
    with BeforeAndAfterAll {

  private var eventService = mock[EventService]
  private var eventSeatService = mock[EventSeatService]
  private var integrationTransactions = mock[IntegrationTransactions]
  private var apiService = mock[StubHubService]
  private var serviceHolder = mock[ServiceHolder]

  def setup() = {
    eventService = mock[EventService]
    when(eventService.getBySecondaryEventId(any[Int], any[Int], any[String]))
      .thenReturn(Future.successful(Some(RandomModelUtil.event())))
    when(eventService.getByIntegrationId(any[Int], any[Int]))
      .thenReturn(Future.successful(Some(RandomModelUtil.event())))

    eventSeatService = mock[EventSeatService]
    when(eventSeatService.getOne(any[EventSeatBySeatLocator]))
      .thenReturn(Future.successful(Some(RandomModelUtil.eventSeat())))

    integrationTransactions = mock[IntegrationTransactions]
    when(integrationTransactions.getClientEventSeatListing(any[Set[Int]], any[Int]))
      .thenReturn(
        Future.successful(
          Seq(
            ClientIntegrationTicket(
              clientIntegrationEventSeatId = 1,
              clientIntegrationListingId = 1,
              clientIntegrationListingExternalId = "1235",
              eventSeatId = 1,
              row = "row",
              seat = "seat"
            )
          )
        )
      )

    apiService = mock[StubHubService]
    when(apiService.delist(any[Seq[StubHubTicket]], any[Int]))
      .thenReturn(Future.successful(ApiSuccess(200, mock[StubHubListingResponse])))

    serviceHolder = mock[ServiceHolder]
    when(serviceHolder.eventService).thenReturn(eventService)
    when(serviceHolder.eventSeatService).thenReturn(eventSeatService)
    when(serviceHolder.integrationTransactions).thenReturn(integrationTransactions)

    val clientIntegrationId = 1

    val notificationService = mock[NotificationService]

    system.actorOf(
      StubHubDelister.props(serviceHolder, apiService, clientIntegrationId, notificationService)
    )
  }

  val eventSeatIds = Seq(1, 2, 3, 4)

  "StubHubDelister" should {
    "delist an order from a primary integration" in {
      val order = Order(
        clientId = 1,
        integrationId = 3,
        sourceExternalTransactionId = "1",
        externalEventId = "1",
        timestamp = Instant.ofEpochMilli(150),
        isPrimary = true,
        section = "123",
        row = "1",
        transactionType = TransactionType.Purchase,
        orderItems = Seq(
          OrderItem(seatNumber = "1", price = 19.99),
          OrderItem(seatNumber = "2", price = 19.99),
          OrderItem(seatNumber = "3", price = 19.99)
        )
      )

      val stubHubDelister = setup()
      stubHubDelister ! DelistTickets(order)

      expectNoMessage(1.seconds)

      verify(serviceHolder.eventService, times(1)).getByIntegrationId(any[Int], any[Int])
      verify(serviceHolder.integrationTransactions, times(1))
        .getClientEventSeatListing(any[Set[Int]], any[Int])
      verify(apiService, times(1)).delist(any[Seq[StubHubTicket]], any[Int])
    }

    "delist an order from a secondary integration" in {
      val order = Order(
        clientId = 1,
        integrationId = 3,
        sourceExternalTransactionId = "1",
        externalEventId = "1",
        timestamp = Instant.ofEpochMilli(150),
        isPrimary = false,
        section = "123",
        row = "1",
        transactionType = TransactionType.Purchase,
        orderItems = Seq(
          OrderItem(seatNumber = "1", price = 19.99),
          OrderItem(seatNumber = "2", price = 19.99),
          OrderItem(seatNumber = "3", price = 19.99)
        )
      )

      val stubHubDelister = setup()
      stubHubDelister ! DelistTickets(order)

      expectNoMessage(1.seconds)

      verify(serviceHolder.eventService, times(1))
        .getBySecondaryEventId(any[Int], any[Int], any[String])
      verify(serviceHolder.integrationTransactions, times(1))
        .getClientEventSeatListing(any[Set[Int]], any[Int])
      verify(apiService, times(1)).delist(any[Seq[StubHubTicket]], any[Int])
    }
  }
}
