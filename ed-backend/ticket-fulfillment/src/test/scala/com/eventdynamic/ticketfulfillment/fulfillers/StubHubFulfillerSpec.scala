package com.eventdynamic.ticketfulfillment.fulfillers

import java.time.Instant

import akka.actor.ActorSystem
import akka.testkit._
import com.eventdynamic.models.TransactionType
import com.eventdynamic.services.NotificationService
import com.eventdynamic.stubhub.StubHubService
import com.eventdynamic.stubhub.models._
import com.eventdynamic.ticketfulfillment.models.{Order, OrderItem}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.typesafe.config.ConfigFactory
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.WordSpecLike
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future
import scala.concurrent.duration._

class StubHubFulfillerSpec
    extends TestKit(ActorSystem("TestActor", ConfigFactory.empty()))
    with WordSpecLike
    with MockitoSugar {

  // Constant test data
  val integrationId = 12
  val secondaryEventId = "123654"
  val section = "123"
  val row = "174"
  val price = 19.95
  val clientIntegrationId = 1

  val order = Order(
    clientId = 1,
    integrationId = integrationId,
    sourceExternalTransactionId = "1",
    externalEventId = secondaryEventId,
    timestamp = Instant.ofEpochMilli(150),
    isPrimary = false,
    section = section,
    row = row,
    transactionType = TransactionType.Purchase,
    orderItems = (1 to 3).map(
      idx => OrderItem(seatNumber = idx.toString, price = price, barcode = Some(s"barcode-$idx"))
    )
  )

  // Mock services
  val notificationService = mock[NotificationService]
  val serviceHolder = mock[ServiceHolder]

  val stubHubService = mock[StubHubService]
  when(stubHubService.fulfill(any[Int], any[Seq[StubHubTicketWithBarcode]])).thenReturn(
    Future
      .successful(ApiSuccess(200, StubHubFulfillmentResponse(orderId = "123", status = "GOOD?")))
  )

  val stubHubFulfiller =
    system.actorOf(
      StubHubFulfiller
        .props(serviceHolder, stubHubService, notificationService)
    )

  "StubHubFulfiller" should {
    "fulfill the order" in {
      stubHubFulfiller ! FulfillOrder(order)

      expectNoMessage(1.seconds)

      verify(stubHubService, times(1)).fulfill(any[Int], any[Seq[StubHubTicketWithBarcode]])
    }
  }

}
