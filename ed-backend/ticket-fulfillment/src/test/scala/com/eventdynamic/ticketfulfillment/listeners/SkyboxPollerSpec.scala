package com.eventdynamic.ticketfulfillment.listeners

import java.sql.Timestamp
import java.time.Instant

import akka.actor.ActorSystem
import akka.testkit._
import com.eventdynamic.SttpHelper
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models._
import com.eventdynamic.models.jobconfigs.ticketfulfillment.SkyboxFulfillmentConfig
import com.eventdynamic.models.jobconfigs.{TicketFulfillmentConfig, TicketFulfillmentConfigFormat}
import com.eventdynamic.services.{NotificationService, _}
import com.eventdynamic.skybox.SkyboxService
import com.eventdynamic.skybox.models.{SkyboxSoldInventory, SkyboxSoldInventoryGetResponse}
import com.eventdynamic.ticketfulfillment.coordinators.ProcessOrder
import com.eventdynamic.ticketfulfillment.models.{Order, OrderItem}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.transactions.IntegrationTransactions
import com.softwaremill.sttp.Response
import com.typesafe.config.ConfigFactory
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, WordSpecLike}
import org.specs2.mock.Mockito
import play.api.libs.json.{Reads, Writes}

import scala.concurrent.Future
import scala.concurrent.duration._

class SkyboxPollerSpec
    extends TestKit(ActorSystem("TestActor", ConfigFactory.empty()))
    with WordSpecLike
    with Mockito
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with TicketFulfillmentConfigFormat
    with SkyboxClientIntegrationConfig.format {

  // Set up constants
  val clientId = 1
  val integrationId = 5
  val primaryEventId = 1
  val eventSeatId = 101
  val lastPoll = Instant.ofEpochMilli(150)
  val skyboxConfig = SkyboxFulfillmentConfig(lastPoll)
  val customerId = 222
  val customerIdToIgnore = 712638

  val fulfillmentConfig =
    TicketFulfillmentConfig(tdc = None, skybox = Some(skyboxConfig), stubhub = None)

  val skyboxClientIntegrationConfig =
    SkyboxClientIntegrationConfig(
      "",
      "",
      0,
      "",
      0,
      customers = Map("222" -> 3),
      customersToIgnore = Seq(customerIdToIgnore.toString)
    )

  val jobType = JobType.TicketFulfillment
  val seat = Seat(Some(1), Some(999), new Timestamp(0), new Timestamp(0), "1", "1", "123", 1)
  val secondSeat = Seat(Some(1), Some(999), new Timestamp(0), new Timestamp(0), "2", "1", "123", 1)

  val seasonId = 1234

  val season =
    Season(id = Some(seasonId), name = None, clientId = clientId, startDate = None, endDate = None)

  val events = Seq[Event](
    Event(
      id = Some(1),
      primaryEventId = Some(primaryEventId),
      createdAt = new Timestamp(0),
      modifiedAt = new Timestamp(0),
      name = "Event 1",
      timestamp = Some(new Timestamp(200)),
      clientId = clientId,
      venueId = 1,
      eventCategoryId = 1,
      seasonId = None,
      isBroadcast = true,
      percentPriceModifier = 0,
      eventScore = None,
      eventScoreModifier = 0,
      spring = None,
      springModifier = 0
    )
  )

  val priceScales = Seq(
    PriceScale(
      id = Some(1),
      createdAt = new Timestamp(0),
      modifiedAt = new Timestamp(0),
      name = "name",
      venueId = 1,
      integrationId = 1
    )
  )

  val invoice =
    SkyboxSoldInventory(
      id = 1,
      invoiceDate = Instant.ofEpochMilli(150),
      unitTicketSales = 17.95,
      section = seat.section,
      row = seat.row,
      seatNumbers = seat.seat,
      eventId = primaryEventId,
      customerId = customerId
    )

  val secondInvoice =
    SkyboxSoldInventory(
      id = 2,
      invoiceDate = Instant.ofEpochMilli(100),
      unitTicketSales = 17.95,
      section = secondSeat.section,
      row = secondSeat.row,
      seatNumbers = secondSeat.seat,
      eventId = primaryEventId,
      customerId = customerId
    )

  val transaction = Transaction(
    timestamp = new Timestamp(150),
    price = invoice.unitTicketSales,
    eventSeatId = eventSeatId,
    integrationId = integrationId,
    buyerTypeCode = "ADULT",
    revenueCategoryId = None,
    transactionType = TransactionType.Purchase,
    primaryTransactionId = Some(invoice.id.toString)
  )

  val order = Order(
    clientId = 1,
    integrationId = 3,
    sourceExternalTransactionId = "1",
    externalEventId = "1",
    timestamp = Instant.ofEpochMilli(150),
    isPrimary = true,
    section = "123",
    row = "1",
    transactionType = TransactionType.Purchase,
    orderItems = Seq(OrderItem(seatNumber = "1", price = 17.95))
  )

  val successResponse = Response(
    Right(SkyboxSoldInventoryGetResponse(Seq(invoice))),
    200,
    "success",
    scala.collection.immutable.Seq(),
    List()
  )

  val failedResponse =
    Response[SkyboxSoldInventoryGetResponse](
      Left("asdf"),
      400,
      "failed",
      scala.collection.immutable.Seq(),
      List()
    )

  def successResponse[T](body: T) =
    Response(Right(body), 200, "success", scala.collection.immutable.Seq(), List())

  // Set up mock services
  val serviceHolder = ServiceHolder(
    mock[ClientIntegrationService],
    mock[TransactionService],
    mock[IntegrationTransactions],
    mock[JobConfigService],
    mock[EventService],
    mock[SeatService],
    mock[EventSeatService],
    mock[ClientIntegrationEventsService],
    mock[PriceScaleService],
    mock[RevenueCategoryMappingService],
    mock[SeasonService]
  )

  when(serviceHolder.seasonService.getCurrent(any[Option[Int]], any[Boolean]))
    .thenReturn(Future.successful(Some(season)))

  when(
    serviceHolder.jobConfigService
      .getConfig[TicketFulfillmentConfig](any[Int], any[JobType])(
        any[Reads[TicketFulfillmentConfig]]
      )
  ).thenReturn(Future.successful(Some(fulfillmentConfig)))

  when(
    serviceHolder.jobConfigService
      .updateConfig[TicketFulfillmentConfig](any[Int], any[JobType], any[TicketFulfillmentConfig])(
        any[Writes[TicketFulfillmentConfig]]
      )
  ).thenReturn(Future.successful(1))

  when(serviceHolder.eventService.getAllBySeason(seasonId, clientId, isAdmin = false))
    .thenReturn(Future.successful(events))

  when(serviceHolder.seatService.upsert(any[Seat])).thenReturn(Future.successful(1))

  when(serviceHolder.eventSeatService.upsert(any[EventSeat]))
    .thenReturn(Future.successful(eventSeatId))

  when(serviceHolder.transactionService.upsert(any[Transaction])).thenReturn(Future.successful(1))

  when(serviceHolder.priceScaleService.getAllForClient(any[Int]))
    .thenReturn(Future.successful(priceScales))

  when(serviceHolder.revenueCategoryMappingService.regexMatch(any[Int], any[String]))
    .thenReturn(Future.successful(None))

  val apiService =
    mock[SkyboxService]

  val skyboxPollerConfig =
    SkyboxPollerConfig(clientId, jobType, integrationId, 600.seconds)

  val notificationService = mock[NotificationService]

  // Create poller to test off of
  val skyboxPoller =
    system.actorOf(
      SkyboxPoller
        .props(
          skyboxPollerConfig,
          serviceHolder,
          apiService,
          testActor,
          notificationService,
          skyboxClientIntegrationConfig
        )
    )

  override def beforeEach(): Unit = {
    // Resetting here to clear the invocations
    reset(serviceHolder.transactionService)
    reset(serviceHolder.seatService)
    reset(serviceHolder.eventSeatService)
    when(serviceHolder.seatService.upsert(any[Seat])).thenReturn(Future.successful(1))
    when(serviceHolder.eventSeatService.upsert(any[EventSeat]))
      .thenReturn(Future.successful(eventSeatId))
    when(serviceHolder.transactionService.upsert(any[Transaction])).thenReturn(Future.successful(1))
  }

  override def afterAll(): Unit = {
    super.afterAll()

    TestKit.shutdownActorSystem(system)
  }

  "SkyboxPoller" should {
    "send ProcessOrder to coordinator" in {
      skyboxPoller ! SkyboxPoller.PollSuccess(Seq(order))
      expectMsg(ProcessOrder(order))
    }

    "not send ProcessOrder to coordinator when empty" in {
      val orders = Seq[Order]()
      skyboxPoller ! SkyboxPoller.PollSuccess(orders)
      expectNoMessage(100.milli)
    }

    "not send messages to coordinator when exception" in {
      skyboxPoller ! SkyboxPoller.PollException(new Exception())
      expectNoMessage(100.milli)
    }

    "not send messages to coordinator when unknown message" in {
      skyboxPoller ! "wowowowowowow"
      expectNoMessage(100.milli)
    }

    "poll for new transactions" in {
      when(
        apiService
          .getInvoices(events.flatMap(_.primaryEventId), lastPoll)
      ).thenReturn(Future.successful(successResponse))

      skyboxPoller ! Poll
      expectMsg(ProcessOrder(order))
    }

    "ignore transactions where the customer id is ignored" in {
      when(
        apiService
          .getInvoices(events.flatMap(_.primaryEventId), lastPoll)
      ).thenReturn(
        Future.successful(
          SttpHelper.successResponse(
            SkyboxSoldInventoryGetResponse(Seq(invoice.copy(customerId = customerIdToIgnore)))
          )
        )
      )

      skyboxPoller ! Poll
      expectNoMessage(100.millis)
    }

    "not fail poll on an empty response" in {
      when(
        apiService
          .getInvoices(events.flatMap(_.primaryEventId), lastPoll)
      ).thenReturn(Future.successful(failedResponse))

      skyboxPoller ! Poll

      expectNoMessage(100.millis)

      verify(serviceHolder.seatService, never()).upsert(any[Seat])
      verify(serviceHolder.eventSeatService, never()).upsert(any[EventSeat])
      verify(serviceHolder.transactionService, never()).upsert(any[Transaction])
    }
  }
}
