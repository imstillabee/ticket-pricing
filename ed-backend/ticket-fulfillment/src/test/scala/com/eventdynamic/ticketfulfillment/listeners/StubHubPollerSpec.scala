package com.eventdynamic.ticketfulfillment.listeners

import java.sql.Timestamp
import java.time.Instant

import akka.actor.ActorSystem
import akka.testkit._
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models._
import com.eventdynamic.models.jobconfigs.ticketfulfillment.StubHubFulfillmentConfig
import com.eventdynamic.models.jobconfigs.{TicketFulfillmentConfig, TicketFulfillmentConfigFormat}
import com.eventdynamic.services._
import com.eventdynamic.stubhub.StubHubService
import com.eventdynamic.stubhub.models.StubHubTransaction
import com.eventdynamic.ticketfulfillment.coordinators.ProcessOrder
import com.eventdynamic.ticketfulfillment.models.{Order, OrderItem}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.transactions.IntegrationTransactions
import com.typesafe.config.ConfigFactory
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, WordSpecLike}
import org.specs2.mock.Mockito
import play.api.libs.json.{Reads, Writes}

import scala.concurrent.Future
import scala.concurrent.duration._

class StubHubPollerSpec
    extends TestKit(ActorSystem("TestActor", ConfigFactory.empty()))
    with WordSpecLike
    with Mockito
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with TicketFulfillmentConfigFormat {

  // Set up constants
  val clientId = 1
  val integrationId = 5
  val primaryEventId = 1
  val secondaryEventId = "12345"
  val eventSeatId = 101
  val invoiceId = 1
  val section = "1"
  val row = "A"
  val seatId = "2"
  val unitTicketSales = 6.00
  val invoicePrice = 6.00
  val lastPoll = Instant.ofEpochMilli(100)
  val stubHubConfig = StubHubFulfillmentConfig(lastPoll)
  val saleDate = Instant.parse("2019-09-10T19:10:00Z")

  val fulfillmentConfig =
    TicketFulfillmentConfig(tdc = None, skybox = None, stubhub = Some(stubHubConfig))
  val jobType = JobType.TicketFulfillment
  val seat = Seat(Some(1), Some(999), new Timestamp(0), new Timestamp(0), section, row, seatId, 1)

  val event = Event(
    id = Some(1),
    primaryEventId = Some(primaryEventId),
    createdAt = new Timestamp(0),
    modifiedAt = new Timestamp(0),
    name = "Event 1",
    timestamp = Some(new Timestamp(200)),
    clientId = clientId,
    venueId = 1,
    eventCategoryId = 1,
    seasonId = None,
    isBroadcast = true,
    percentPriceModifier = 0,
    eventScore = None,
    eventScoreModifier = 0,
    spring = None,
    springModifier = 0
  )

  val stubHubTransaction = StubHubTransaction(
    saleId = "55555",
    listingId = "77777",
    status = "COMPLETED",
    eventId = secondaryEventId,
    section = section,
    row = row,
    seats = seatId,
    price = unitTicketSales,
    saleDate = saleDate.toString
  )

  val stubHubTransactions = Seq[StubHubTransaction](stubHubTransaction)

  // Set up mock services
  val serviceHolder = ServiceHolder(
    mock[ClientIntegrationService],
    mock[TransactionService],
    mock[IntegrationTransactions],
    mock[JobConfigService],
    mock[EventService],
    mock[SeatService],
    mock[EventSeatService],
    mock[ClientIntegrationEventsService],
    mock[PriceScaleService],
    mock[RevenueCategoryMappingService],
    mock[SeasonService]
  )

  val order = Order(
    integrationId = integrationId,
    clientId = clientId,
    sourceExternalTransactionId = stubHubTransaction.saleId,
    externalEventId = stubHubTransaction.eventId,
    timestamp = saleDate,
    isPrimary = false,
    section = section,
    row = row,
    transactionType = TransactionType.Purchase,
    orderItems = Seq(OrderItem(seatNumber = seatId, price = unitTicketSales))
  )

  when(
    serviceHolder.jobConfigService
      .getConfig[TicketFulfillmentConfig](any[Int], any[JobType])(
        any[Reads[TicketFulfillmentConfig]]
      )
  ).thenReturn(Future.successful(Some(fulfillmentConfig)))

  when(
    serviceHolder.jobConfigService
      .updateConfig[TicketFulfillmentConfig](any[Int], any[JobType], any[TicketFulfillmentConfig])(
        any[Writes[TicketFulfillmentConfig]]
      )
  ).thenReturn(Future.successful(1))

  val apiService =
    mock[StubHubService]

  val stubHubPollerConfig =
    StubHubPollerConfig(clientId, jobType, integrationId, 600.seconds)

  val notificationService = mock[NotificationService]

  // Create poller to test off of
  val stubHubPoller =
    system.actorOf(
      StubHubPoller
        .props(stubHubPollerConfig, serviceHolder, apiService, testActor, notificationService)
    )

  override def afterAll(): Unit = {
    super.afterAll()
    TestKit.shutdownActorSystem(system)
  }

  "StubHubPoller" should {
    "send ProcessOrder to coordinator" in {
      stubHubPoller ! StubHubPoller.PollSuccess(Seq(order))
      expectMsg(1.seconds, ProcessOrder(order))
    }

    "not send ProcessOrder to coordinator when empty" in {
      stubHubPoller ! StubHubPoller.PollSuccess(Seq.empty[Order])
      expectNoMessage(100.milli)
    }

    "not send messages to coordinator when exception" in {
      stubHubPoller ! StubHubPoller.PollException(new Exception())
      expectNoMessage(100.milli)
    }

    "not send messages to coordinator when unknown message" in {
      stubHubPoller ! "wowowowowowow"
      expectNoMessage(100.milli)
    }

    "poll for new transactions" in {
      when(apiService.getStubHubTransactions(Timestamp.from(lastPoll)))
        .thenReturn(Future.successful(stubHubTransactions))

      stubHubPoller ! Poll

      expectMsg(1.seconds, ProcessOrder(order))
    }

    "poll not fail when empty response" in {
      when(apiService.getStubHubTransactions(Timestamp.from(lastPoll)))
        .thenReturn(Future.successful(Seq[StubHubTransaction]()))

      stubHubPoller ! Poll

      expectNoMessage(100.millis)
    }
  }

}
