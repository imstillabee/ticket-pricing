package com.eventdynamic.ticketfulfillment.listeners

import java.sql.Timestamp
import java.time.Instant

import akka.actor.ActorSystem
import akka.testkit._
import com.eventdynamic.SttpHelper
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models._
import com.eventdynamic.models.jobconfigs.ticketfulfillment.TDCFulfillmentConfig
import com.eventdynamic.models.jobconfigs.{TicketFulfillmentConfig, TicketFulfillmentConfigFormat}
import com.eventdynamic.services._
import com.eventdynamic.tdcreplicatedproxy.TdcReplicatedProxyService
import com.eventdynamic.tdcreplicatedproxy.models.TdcTransaction
import com.eventdynamic.ticketfulfillment.coordinators.ProcessOrder
import com.eventdynamic.ticketfulfillment.models.{Order, OrderItem}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.transactions.IntegrationTransactions
import com.typesafe.config.ConfigFactory
import org.mockito.ArgumentCaptor
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, WordSpecLike}
import org.specs2.mock.Mockito
import play.api.libs.json.{Reads, Writes}

import scala.concurrent.Future
import scala.concurrent.duration._

class TDCPollerSpec
    extends TestKit(ActorSystem("TestActor", ConfigFactory.empty()))
    with WordSpecLike
    with Mockito
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with TicketFulfillmentConfigFormat {

  // Set up constants
  val messageWaitTime = 100.milli
  val eventSeatId = 1
  val integrationId = 1
  val seasonId = 1234
  val clientId = 1
  val jobType = JobType.TicketFulfillment
  val primaryEventId = 23678

  val events = Seq[Event](
    Event(
      id = Some(1),
      primaryEventId = Some(primaryEventId),
      createdAt = new Timestamp(0),
      modifiedAt = new Timestamp(0),
      name = "Event 1",
      timestamp = Some(new Timestamp(200)),
      clientId = clientId,
      venueId = 1,
      eventCategoryId = 1,
      seasonId = None,
      isBroadcast = true,
      percentPriceModifier = 0,
      eventScore = None,
      eventScoreModifier = 0,
      spring = None,
      springModifier = 0
    )
  )

  val season =
    Season(id = Some(seasonId), name = None, clientId = clientId, startDate = None, endDate = None)

  val eventSeat = EventSeat(
    id = Some(eventSeatId),
    createdAt = new Timestamp(0),
    modifiedAt = new Timestamp(0),
    seatId = 1,
    eventId = 1,
    priceScaleId = 1,
    listedPrice = None,
    overridePrice = None,
    isListed = true,
    isHeld = false
  )

  val tdcTransaction = TdcTransaction(
    id = 1,
    createdAt = Instant.EPOCH,
    timestamp = Instant.EPOCH,
    eventId = 1,
    buyerTypeCode = "ADULT",
    seatId = 2,
    price = 9.95,
    section = "section",
    row = "row",
    seatNumber = "123",
    transactionTypeCode = "SA",
    priceScaleId = 1123,
    holdCodeType = "O"
  )

  val order = Order(
    integrationId = 1,
    clientId = 1,
    sourceExternalTransactionId = "1",
    externalEventId = "1",
    timestamp = Instant.EPOCH,
    isPrimary = true,
    section = "section",
    row = "row",
    transactionType = TransactionType.Purchase,
    externalPriceScaleId = Some(1123),
    orderItems = Seq(
      OrderItem(
        seatNumber = "123",
        externalSeatId = Some(tdcTransaction.seatId.toInt),
        price = 9.95,
        buyerTypeCode = Some("ADULT")
      )
    )
  )

  val lastPoll = Instant.ofEpochMilli(100)

  val tdcFulfillmentConfig =
    TDCFulfillmentConfig(
      paymentMethodId = 1,
      deliveryMethodId = 1,
      patronAccountId = 1,
      defaultBuyerTypeId = 1,
      lastPoll
    )

  val ticketFulfillmentConfig =
    TicketFulfillmentConfig(tdc = Some(tdcFulfillmentConfig), skybox = None, stubhub = None)

  val tdcPollerConfig =
    TDCPollerConfig(clientId, jobType, integrationId, pollFrequency = 600.seconds)

  val tdcReplicatedProxyService = mock[TdcReplicatedProxyService]
  val notificationService = mock[NotificationService]

  val serviceHolder = ServiceHolder(
    mock[ClientIntegrationService],
    mock[TransactionService],
    mock[IntegrationTransactions],
    mock[JobConfigService],
    mock[EventService],
    mock[SeatService],
    mock[EventSeatService],
    mock[ClientIntegrationEventsService],
    mock[PriceScaleService],
    mock[RevenueCategoryMappingService],
    mock[SeasonService]
  )

  // Create poller to test off of
  val tdcPoller =
    system.actorOf(
      TDCPoller
        .props(
          tdcPollerConfig,
          serviceHolder,
          tdcReplicatedProxyService,
          testActor,
          notificationService
        )
    )

  when(
    serviceHolder.jobConfigService
      .getConfig[TicketFulfillmentConfig](any[Int], any[JobType])(
        any[Reads[TicketFulfillmentConfig]]
      )
  ).thenReturn(Future.successful(Some(ticketFulfillmentConfig)))

  when(
    serviceHolder.jobConfigService
      .updateConfig[TicketFulfillmentConfig](any[Int], any[JobType], any[TicketFulfillmentConfig])(
        any[Writes[TicketFulfillmentConfig]]
      )
  ).thenReturn(Future.successful(1))

  when(tdcReplicatedProxyService.getTransactions(any[Instant], any[Instant], any[Seq[Int]]))
    .thenReturn(Future.successful(SttpHelper.successResponse(Seq(tdcTransaction))))

  when(serviceHolder.eventSeatService.getOne(any[EventSeatUniqueIdentifier]))
    .thenReturn(Future.successful(Some(eventSeat)))

  when(serviceHolder.transactionService.upsert(any[Transaction]))
    .thenReturn(Future.successful(1))

  when(serviceHolder.seasonService.getCurrent(any[Option[Int]], any[Boolean]))
    .thenReturn(Future.successful(Some(season)))

  when(serviceHolder.eventService.getAllBySeason(seasonId, clientId, isAdmin = false))
    .thenReturn(Future.successful(events))

  when(serviceHolder.revenueCategoryMappingService.regexMatch(any[Int], any[String]))
    .thenReturn(Future.successful(None))

  override def afterAll(): Unit = {
    super.afterAll()
    TestKit.shutdownActorSystem(system)
  }

  "TDCPoller" should {
    "send ProcessOrder to coordinator" in {
      tdcPoller ! TDCPoller.PollSuccess(Seq(order))
      expectMsg(ProcessOrder(order))
    }

    "not send ProcessOrder to coordinator when empty" in {
      tdcPoller ! TDCPoller.PollSuccess(Seq[Order]())
      expectNoMessage(messageWaitTime)
    }

    "not send messages to coordinator when exception" in {
      tdcPoller ! TDCPoller.PollException(new Exception())
      expectNoMessage(messageWaitTime)
    }

    "not send messages to coordinator when unknown message" in {
      tdcPoller ! "unknown message"
      expectNoMessage(messageWaitTime)
    }

    "poll for new transactions" in {
      val configCaptor = ArgumentCaptor.forClass(classOf[TicketFulfillmentConfig])
      when(
        serviceHolder.jobConfigService
          .updateConfig[TicketFulfillmentConfig](any[Int], any[JobType], configCaptor.capture())(
            any[Writes[TicketFulfillmentConfig]]
          )
      ).thenReturn(Future.successful(1))

      tdcPoller ! Poll

      expectMsg(ProcessOrder(order))

      // Make sure we change the timestamp to the latest transaction time
      assert(configCaptor.getValue.tdc.get.lastTransactionPoll == tdcTransaction.createdAt)
    }

    "poll not fail when empty response" in {
      when(serviceHolder.jobConfigService.getConfig[TicketFulfillmentConfig](clientId, jobType))
        .thenReturn(Future.successful(Some(ticketFulfillmentConfig)))

      val configCaptor = ArgumentCaptor.forClass(classOf[TicketFulfillmentConfig])
      when(
        serviceHolder.jobConfigService
          .updateConfig[TicketFulfillmentConfig](any[Int], any[JobType], configCaptor.capture())(
            any[Writes[TicketFulfillmentConfig]]
          )
      ).thenReturn(Future.successful(1))

      when(tdcReplicatedProxyService.getTransactions(any[Instant], any[Instant], any[Seq[Int]]))
        .thenReturn(Future.successful(SttpHelper.successResponse(Seq.empty[TdcTransaction])))

      tdcPoller ! Poll

      expectNoMessage(messageWaitTime)

      // Assert that we haven't updated the poll time (only updates based on pulled transactions)
      assert(
        configCaptor.getValue.tdc.get.lastTransactionPoll == ticketFulfillmentConfig.tdc.get.lastTransactionPoll
      )
    }
  }
}
