package com.eventdynamic.ticketfulfillment.persistor

import java.time.Instant

import akka.actor.ActorSystem
import akka.testkit._
import com.eventdynamic.models._
import com.eventdynamic.services._
import com.eventdynamic.ticketfulfillment.models.{Order, OrderItem}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.utils.RandomModelUtil
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._

import scala.concurrent.Future
import scala.concurrent.duration._

class PersistorSpec
    extends TestKit(ActorSystem("TestActor", ConfigFactory.empty()))
    with WordSpecLike
    with MockitoSugar
    with BeforeAndAfterAll {

  override def afterAll() = {
    super.afterAll()

    TestKit.shutdownActorSystem(system)
  }

  "Persistor" should {
    "upsert seat, event seat, and transaction for primary order" in {
      val order = Order(
        clientId = 1,
        integrationId = 2,
        sourceExternalTransactionId = "3",
        externalEventId = "4",
        timestamp = Instant.ofEpochMilli(150),
        isPrimary = true,
        section = "section",
        row = "row",
        transactionType = TransactionType.Purchase,
        externalPriceScaleId = Some(4),
        orderItems =
          (10 to 12).map(seatNumber => OrderItem(seatNumber = seatNumber.toString, price = 19.99))
      )
      val eventService = mock[EventService]
      when(eventService.getByIntegrationId(any[Int], any[Int])).thenAnswer(req => {
        val primaryEventId = req.getArgument[Int](0)
        val clientId = req.getArgument[Int](1)

        Future.successful(
          Some(RandomModelUtil.event(primaryEventId = Some(primaryEventId), clientId = clientId))
        )
      })

      val priceScaleService = mock[PriceScaleService]
      when(priceScaleService.getAllForClient(any[Int])).thenAnswer(_ => {
        Future.successful(
          (1 to 3).map(idx => RandomModelUtil.priceScale(id = Some(idx), integrationId = idx * 2))
        )
      })

      val seatService = mock[SeatService]
      when(seatService.bulkUpsert(any[Seq[Seat]])).thenAnswer(req => {
        val records = req.getArgument[Seq[Any]](0)
        val baseId = 100
        Future.successful(baseId to baseId + records.length)
      })

      val eventSeatService = mock[EventSeatService]
      when(eventSeatService.bulkUpsert(any[Seq[EventSeat]])).thenAnswer(req => {
        val records = req.getArgument[Seq[Any]](0)
        val baseId = 200
        Future.successful(baseId to baseId + records.length)
      })

      val revenueCategoryMappingService = mock[RevenueCategoryMappingService]
      when(revenueCategoryMappingService.regexMatch(any[Int], any[String]))
        .thenReturn(Future.successful(Some(666)))

      val transactionService = mock[TransactionService]
      when(transactionService.bulkUpsert(any[Seq[Transaction]])).thenAnswer(req => {
        val records = req.getArgument[Seq[Any]](0)
        val baseId = 300
        Future.successful(baseId to baseId + records.length)
      })

      val serviceHolder = mock[ServiceHolder]
      when(serviceHolder.seatService).thenReturn(seatService)
      when(serviceHolder.eventSeatService).thenReturn(eventSeatService)
      when(serviceHolder.transactionService).thenReturn(transactionService)
      when(serviceHolder.priceScaleService).thenReturn(priceScaleService)
      when(serviceHolder.eventService).thenReturn(eventService)

      val persistor = system.actorOf(Persistor.props(serviceHolder))

      persistor ! UpsertOrder(order)

      expectNoMessage(1000.millis)

      verify(serviceHolder.seatService, times(1)).bulkUpsert(any[Seq[Seat]])
      verify(serviceHolder.eventSeatService, times(1)).bulkUpsert(any[Seq[EventSeat]])
      verify(serviceHolder.transactionService, times(1)).bulkUpsert(any[Seq[Transaction]])
    }

    "Upsert transaction for secondary order" in {
      val order = Order(
        clientId = 1,
        integrationId = 2,
        sourceExternalTransactionId = "3",
        externalEventId = "4",
        timestamp = Instant.ofEpochMilli(150),
        isPrimary = false,
        section = "section",
        row = "row",
        transactionType = TransactionType.Purchase,
        orderItems =
          (10 to 12).map(seatNumber => OrderItem(seatNumber = "seatNumber", price = 19.99))
      )

      val eventService = mock[EventService]
      when(eventService.getBySecondaryEventId(any[Int], any[Int], any[String])).thenAnswer(req => {
        val primaryEventId = req.getArgument[Int](0)
        val clientId = req.getArgument[Int](1)

        Future.successful(
          Some(RandomModelUtil.event(primaryEventId = Some(primaryEventId), clientId = clientId))
        )
      })

      val eventSeatService = mock[EventSeatService]
      when(eventSeatService.getOne(any[EventSeatBySeatLocator]))
        .thenAnswer(_ => Future.successful(Some(RandomModelUtil.eventSeat())))

      val revenueCategoryMappingService = mock[RevenueCategoryMappingService]
      when(revenueCategoryMappingService.regexMatch(any[Int], any[String]))
        .thenReturn(Future.successful(Some(666)))

      val transactionService = mock[TransactionService]
      when(transactionService.upsert(any[Transaction])).thenAnswer(req => {
        Future.successful(101)
      })

      val serviceHolder = mock[ServiceHolder]
      when(serviceHolder.eventSeatService).thenReturn(eventSeatService)
      when(serviceHolder.transactionService).thenReturn(transactionService)
      when(serviceHolder.eventService).thenReturn(eventService)

      val persistor = system.actorOf(Persistor.props(serviceHolder))

      persistor ! UpsertOrder(order)

      expectNoMessage(1000.millis)

      verify(serviceHolder.transactionService, times(order.orderItems.length))
        .upsert(any[Transaction])
    }
  }
}
