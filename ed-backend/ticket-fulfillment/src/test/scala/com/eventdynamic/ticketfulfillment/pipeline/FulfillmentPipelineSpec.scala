package com.eventdynamic.ticketfulfillment.pipeline

import java.sql.Timestamp
import java.time.Instant

import akka.actor.{ActorSystem, PoisonPill}
import akka.testkit.TestKit
import com.eventdynamic.IntegrationServiceBuilder
import com.eventdynamic.edtickets.EDTicketsService
import com.eventdynamic.models.jobconfigs.TicketFulfillmentConfig
import com.eventdynamic.models.{
  ClientIntegrationComposite,
  EDTicketsClientIntegrationConfig,
  SkyboxClientIntegrationConfig,
  TDCClientIntegrationConfig
}
import com.eventdynamic.provenue.ProVenueService
import com.eventdynamic.services._
import com.eventdynamic.skybox.SkyboxService
import com.eventdynamic.models.jobconfigs.ticketfulfillment.{
  SkyboxFulfillmentConfig,
  StubHubFulfillmentConfig,
  TDCFulfillmentConfig
}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.transactions.IntegrationTransactions
import com.typesafe.config.ConfigFactory
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import org.specs2.mock.Mockito

import scala.concurrent.Future

class FulfillmentPipelineSpec
    extends TestKit(ActorSystem("TestActor", ConfigFactory.empty()))
    with Mockito
    with WordSpecLike
    with BeforeAndAfterAll
    with SkyboxClientIntegrationConfig.format
    with TDCClientIntegrationConfig.format
    with EDTicketsClientIntegrationConfig.format {

  val clientId = 1
  val tdcConfig = Some(TDCFulfillmentConfig(0, 0, 0, 0, Instant.EPOCH))
  val skyboxConfig = Some(SkyboxFulfillmentConfig(Instant.EPOCH))
  val stubhubConfig = Some(StubHubFulfillmentConfig(Instant.EPOCH))

  val serviceHolder = ServiceHolder(
    mock[ClientIntegrationService],
    mock[TransactionService],
    mock[IntegrationTransactions],
    mock[JobConfigService],
    mock[EventService],
    mock[SeatService],
    mock[EventSeatService],
    mock[ClientIntegrationEventsService],
    mock[PriceScaleService],
    mock[RevenueCategoryMappingService],
    mock[SeasonService]
  )

  def buildCIC(id: Int, name: String, primary: Boolean, json: String = "") =
    ClientIntegrationComposite(
      Some(id),
      new Timestamp(0),
      new Timestamp(0),
      clientId,
      id,
      name,
      None,
      Some(json),
      true,
      primary,
      None,
      Some(0),
      None
    )

  val tdc = buildCIC(1, "Tickets.com", true, s"""{
        |"agent":"agent",
        |"apiKey":"appKey",
        |"appId":"appId",
        |"username":"username",
        |"password":"password",
        |"baseUrl":"https://baseUrl",
        |"supplierId":1,
        |"tdcProxyBaseUrl": "proxyBaseUrl",
        |"mlbamGridConfigs":[]}
        |""".stripMargin)
  val seatgeek = buildCIC(2, "SeatGeek", false)
  val stubhub = buildCIC(3, "StubHub", false, s"""{
        |"mode": "mode",
        |"username": "username",
        |"password": "password",
        |"consumerKey": "key",
        |"consumerSecret": "secret",
        |"baseUrl": "https://baseurl",
        |"applicationToken": "token",
        |"sellerGUID": "70214d9b-a4c3-4a58-8d12-af4d0d60eed2"
        |}
     """.stripMargin)
  val edtickets = buildCIC(4, "EDTickets", false, s"""{
       |"apiKey": "apikey",
       |"appId": "appid",
       |"baseUrl": "https://baseurl",
       |"appDatabase": "database",
       |"email": "email@email.com",
       |"password": "password",
       |"functionsUrl": "https://functionalurl"
       |}
     """.stripMargin)
  val skybox = buildCIC(5, "Skybox", true, s"""{
       |"appKey": "appKey",
       |"baseUrl": "baseUrl",
       |"accountId": 1,
       |"apiKey": "appKey",
       |"defaultVendorId": 1,
       |"customers": {"12345": 7},
       |"customersToIgnore": ["11"]
       |}
       |""".stripMargin)

  val fake = buildCIC(1000, "Fake", false)

  val serviceBuilder = mock[IntegrationServiceBuilder]
  when(serviceBuilder.buildProVenueService(tdc.config[TDCClientIntegrationConfig]))
    .thenReturn(mock[ProVenueService])
  when(serviceBuilder.buildEDTicketsService(edtickets.config[EDTicketsClientIntegrationConfig]))
    .thenReturn(mock[EDTicketsService])
  when(serviceBuilder.buildSkyboxService(skybox.config[SkyboxClientIntegrationConfig]))
    .thenReturn(mock[SkyboxService])

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "FulfillmentPipeline" should {
    "create basic pipeline" in {
      when(serviceHolder.clientIntegrationService.getByClientId(clientId, true))
        .thenReturn(Future.successful(Seq[ClientIntegrationComposite](skybox, edtickets)))

      val jobConfig =
        TicketFulfillmentConfig(tdcConfig, None, None)
      val pipeline =
        FulfillmentPipeline.buildPipelineForClient(
          clientId,
          serviceHolder,
          system,
          jobConfig,
          serviceBuilder
        )

      assert(pipeline.delisters.size == 1)
      assert(pipeline.pollers.size == 2)
      assert(pipeline.routes.size == 1)
      assert(pipeline.fulfillers.size == 1)

      pipeline.pollers.foreach(_._2 ! PoisonPill)
    }

    "error when building unsupported reserver" in {
      assertThrows[UnsupportedOperationException] {
        FulfillmentPipeline.buildReserver(
          clientId,
          fake,
          serviceHolder,
          system,
          TicketFulfillmentConfig(None, None, None),
          serviceBuilder
        )
      }
    }

    "error when building unsupported fulfiller" in {
      assertThrows[UnsupportedOperationException] {
        FulfillmentPipeline.buildFulfillers(
          clientId,
          Seq(fake),
          serviceHolder,
          system,
          serviceBuilder
        )
      }
    }

    "error when building unsupported delister" in {
      assertThrows[UnsupportedOperationException] {
        FulfillmentPipeline.buildDelisters(
          clientId,
          Seq(fake),
          serviceHolder,
          system,
          serviceBuilder
        )
      }
    }

    "error when building unsupported listener" in {
      assertThrows[UnsupportedOperationException] {
        FulfillmentPipeline.buildListeners(
          clientId,
          Seq(fake),
          serviceHolder,
          system,
          testActor,
          serviceBuilder
        )
      }
    }
  }
}
