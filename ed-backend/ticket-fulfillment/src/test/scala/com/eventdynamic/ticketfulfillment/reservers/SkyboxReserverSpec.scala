package com.eventdynamic.ticketfulfillment.reservers

import java.time.Instant

import akka.actor.ActorSystem
import akka.testkit._
import com.eventdynamic.SttpHelper
import com.eventdynamic.models.{SkyboxClientIntegrationConfig, TransactionType}
import com.eventdynamic.services._
import com.eventdynamic.skybox.SkyboxService
import com.eventdynamic.skybox.models.{SkyboxInvoice, SkyboxInvoiceLine, SkyboxTicket}
import com.eventdynamic.ticketfulfillment.coordinators.{
  PrintFailed,
  PrintSuccessful,
  ReservationFailed,
  ReservationSuccessful
}
import com.eventdynamic.ticketfulfillment.models.{Order, OrderItem}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.utils.RandomModelUtil
import com.typesafe.config.ConfigFactory
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterEach, WordSpecLike}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future
import scala.concurrent.duration._

class SkyboxReserverSpec
    extends TestKit(ActorSystem("TestActor", ConfigFactory.empty()))
    with ImplicitSender
    with WordSpecLike
    with MockitoSugar
    with BeforeAndAfterEach {

  private val skyboxClientIntegrationConfig = mock[SkyboxClientIntegrationConfig]
  private val eventService = mock[EventService]
  private val seatService = mock[SeatService]
  private val serviceHolder = mock[ServiceHolder]
  private val notificationService = mock[NotificationService]
  private val skyboxService = mock[SkyboxService]

  val amount = BigDecimal(25.49)
  val section = "section"
  val row = "row"
  val skyboxSoldInventoryId = 12345
  val eventId = 28736
  val primaryEventId = 9873
  val customerId = 7625376
  val seatNumbers = Seq(1, 2, 3)
  val secondaryEventId = "76235"
  val integrationId = 2
  val clientIntegrationComposite = RandomModelUtil.clientIntegrationComposite()

  val invoice = SkyboxInvoice(
    id = 123,
    lines = Seq(
      SkyboxInvoiceLine(
        id = 123,
        accountId = None,
        quantity = None,
        amount = 19.99,
        lineItemType = "INVENTORY",
        inventoryIds = Seq(skyboxSoldInventoryId)
      )
    )
  )

  // Create reserver to test off of
  private var skyboxReserver = system.actorOf(
    SkyboxReserver
      .props(
        skyboxClientIntegrationConfig,
        serviceHolder,
        notificationService,
        skyboxService,
        clientIntegrationComposite
      )
  )

  val skyboxTickets: Seq[SkyboxTicket] = seatNumbers.map(seatNumber => {
    SkyboxTicket(
      id = seatNumber,
      createdDate = Instant.now(),
      lastUpdate = Instant.now(),
      inventoryId = 1,
      invoiceLineId = 1,
      purchaseLineId = 1,
      status = "status",
      eventId = eventId,
      section = section,
      row = row,
      seatNumber = seatNumber,
      cost = 19.99,
      faceValue = 19.99,
      sellPrice = 19.99,
      barCode = Some(s"barcode-$seatNumber")
    )
  })

  override def beforeEach(): Unit = {
    super.beforeEach()

    reset(
      skyboxClientIntegrationConfig,
      eventService,
      seatService,
      serviceHolder,
      notificationService,
      skyboxService
    )

    when(skyboxClientIntegrationConfig.getCustomerIdForIntegrationId(any[Int]))
      .thenReturn(Some(customerId.toString))

    when(eventService.getBySecondaryEventId(any[Int], any[Int], any[String]))
      .thenAnswer(
        _ =>
          Future.successful(
            Some(RandomModelUtil.event(id = Some(eventId), primaryEventId = Some(primaryEventId)))
        )
      )

    when(
      seatService
        .get(any[Option[String]], any[Option[String]], any[Option[String]], any[Option[Int]])
    ).thenAnswer(_ => {
      Future.successful(Some(RandomModelUtil.seat(section = section, row = row)))
    })

    when(serviceHolder.eventService).thenReturn(eventService)
    when(serviceHolder.seatService).thenReturn(seatService)

    when(
      skyboxService
        .getTickets(
          any[Int],
          any[Option[String]],
          any[Option[String]],
          any[Seq[String]],
          any[Option[Instant]],
          any[Option[Instant]]
        )
    ).thenReturn(Future.successful(SttpHelper.successResponse(skyboxTickets)))

    when(skyboxService.createInvoice(any[Int], any[BigDecimal], any[Seq[Int]]))
      .thenReturn(Future.successful(SttpHelper.successResponse(invoice)))

    skyboxReserver = system.actorOf(
      SkyboxReserver
        .props(
          skyboxClientIntegrationConfig,
          serviceHolder,
          notificationService,
          skyboxService,
          clientIntegrationComposite
        )
    )
  }

  "SkyboxReserver" should {
    val order = Order(
      clientId = 1,
      integrationId = integrationId,
      sourceExternalTransactionId = "1",
      externalEventId = secondaryEventId,
      timestamp = Instant.ofEpochMilli(150),
      isPrimary = false,
      section = section,
      row = row,
      transactionType = TransactionType.Purchase,
      orderItems = (1 to 3).map(sn => OrderItem(seatNumber = sn.toString, price = amount))
    )
    val barcodeOrder = order.copy(orderItems = order.orderItems.zipWithIndex.map {
      case (orderItem, index) => orderItem.copy(barcode = Some(s"barcode-${index + 1}"))
    })

    "respond with ReservationSuccessful message when reservation succeeds" in {
      skyboxReserver ! ReserveTickets(order)

      val reservationOrder =
        order.copy(copyExternalTransactionId = Some(skyboxSoldInventoryId.toString))
      expectMsg(1.seconds, ReservationSuccessful[SkyboxInvoice](reservationOrder, invoice))

      verify(skyboxService, times(1))
        .getTickets(
          primaryEventId,
          Some(section),
          Some(row),
          seatNumbers = Seq("1", "2", "3"),
          None,
          None
        )

      verify(skyboxService, times(1))
        .createInvoice(customerId, amount * order.orderItems.length, ticketIds = Seq(1, 2, 3))
    }

    "respond with ReservationFailed message when reservation fails" in {
      when(skyboxService.createInvoice(any[Int], any[BigDecimal], any[Seq[Int]]))
        .thenReturn(Future.successful(SttpHelper.failedResponse[SkyboxInvoice]()))

      skyboxReserver ! ReserveTickets(order)
      expectMsg(1.seconds, ReservationFailed(order))

      verify(skyboxService, times(1))
        .getTickets(
          primaryEventId,
          Some(section),
          Some(row),
          seatNumbers = Seq("1", "2", "3"),
          None,
          None
        )

      verify(skyboxService, times(1))
        .createInvoice(customerId, amount * order.orderItems.length, ticketIds = Seq(1, 2, 3))

      verify(notificationService, times(1))
        .sendAlert(any[String], any[Throwable])
    }

    "respond with PrintSuccessful message when print succeeds" in {
      skyboxReserver ! PrintTickets(order, invoice)
      expectMsg(1.seconds, PrintSuccessful(barcodeOrder))

      verify(skyboxService, times(1))
        .getTickets(
          primaryEventId,
          Some(section),
          Some(row),
          seatNumbers = Seq("1", "2", "3"),
          None,
          None
        )
    }

    "respond with PrintFailed message when print fails" in {
      when(
        skyboxService
          .getTickets(
            any[Int],
            any[Option[String]],
            any[Option[String]],
            any[Seq[String]],
            any[Option[Instant]],
            any[Option[Instant]]
          )
      ).thenReturn(Future.successful(SttpHelper.failedResponse[Seq[SkyboxTicket]]()))

      skyboxReserver ! PrintTickets(order, invoice)
      expectMsg(1.seconds, PrintFailed(order))

      verify(skyboxService, times(1))
        .getTickets(
          primaryEventId,
          Some(section),
          Some(row),
          seatNumbers = Seq("1", "2", "3"),
          None,
          None
        )

      verify(notificationService, times(1))
        .sendAlert(any[String], any[Throwable])
    }
  }
}
