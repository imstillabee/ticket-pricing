package com.eventdynamic.ticketfulfillment.reservers

import java.sql.Timestamp
import java.time.Instant

import akka.actor.{ActorRef, ActorSystem, PoisonPill}
import akka.testkit.{ImplicitSender, TestKit}
import com.eventdynamic.models._
import com.eventdynamic.provenue.ProVenueService
import com.eventdynamic.provenue.models._
import com.eventdynamic.services.{NotificationService, _}
import com.eventdynamic.ticketfulfillment.coordinators.{
  PrintFailed,
  PrintSuccessful,
  ReservationFailed,
  ReservationSuccessful
}
import com.eventdynamic.ticketfulfillment.models.{Order, OrderItem}
import com.eventdynamic.ticketfulfillment.services.ServiceHolder
import com.eventdynamic.transactions.IntegrationTransactions
import com.eventdynamic.utils.RandomModelUtil
import com.softwaremill.sttp.Response
import com.typesafe.config.ConfigFactory
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterEach, WordSpecLike}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future
import scala.concurrent.duration._

class TDCReserverSpec
    extends TestKit(ActorSystem("TestActor", ConfigFactory.empty()))
    with ImplicitSender
    with WordSpecLike
    with MockitoSugar
    with BeforeAndAfterEach {

  val clientId = 1

  val serviceHolder = ServiceHolder(
    mock[ClientIntegrationService],
    mock[TransactionService],
    mock[IntegrationTransactions],
    mock[JobConfigService],
    mock[EventService],
    mock[SeatService],
    mock[EventSeatService],
    mock[ClientIntegrationEventsService],
    mock[PriceScaleService],
    mock[RevenueCategoryMappingService],
    mock[SeasonService]
  )

  val eventId = 28736
  val primaryEventId = 5123
  val secondaryEventId = "8276"

  val event = RandomModelUtil.event(
    id = Some(eventId),
    primaryEventId = Some(primaryEventId),
    clientId = clientId,
  )
  when(serviceHolder.eventService.getBySecondaryEventId(any[Int], any[Int], any[String]))
    .thenReturn(Future.successful(Some(event)))

  when(
    serviceHolder.seatService
      .get(any[Option[String]], any[Option[String]], any[Option[String]], any[Option[Int]])
  ).thenReturn(
    Future.successful(
      Seq(Seat(Some(1), Some(101), new Timestamp(0), new Timestamp(0), "1", "1", "123", 1))
    )
  )

  val notificationService = mock[NotificationService]

  val tdcReserverConf = TDCReserverConf(1, 1, 1, 1)
  var pvService: ProVenueService = mock[ProVenueService]

  val clientIntegrationComposite = RandomModelUtil.clientIntegrationComposite()

  var tdcReserver: ActorRef =
    system.actorOf(
      TDCReserver.props(
        serviceHolder,
        pvService,
        tdcReserverConf,
        0.5.seconds,
        notificationService,
        clientIntegrationComposite
      )
    )

  override def beforeEach(): Unit = {
    super.beforeEach()

    pvService = mock[ProVenueService]
    tdcReserver = system.actorOf(
      TDCReserver.props(
        serviceHolder,
        pvService,
        tdcReserverConf,
        0.05.seconds,
        notificationService,
        clientIntegrationComposite
      )
    )

    when(pvService.lockSeats(any[Int], any[Int], any[Seq[Int]]))
      .thenReturn(Future.successful(successResponse(TDCReserverSpec.lockSeatsResponse)))

    when(pvService.checkout(any[Int], any[ProVenueMoney], any[Int], any[Int], any[Int]))
      .thenReturn(Future.successful(successResponse(TDCReserverSpec.checkoutResponse)))

    when(pvService.print(any[Int], any[Boolean]))
      .thenReturn(Future.successful(successResponse(TDCReserverSpec.printResponse)))
  }

  override def afterEach(): Unit = {
    tdcReserver ! PoisonPill
  }

  def successResponse[T](body: T): Response[T] = {
    Response(Right(body), 200, "success", scala.collection.immutable.Seq(), List())
  }

  def failedResponse[T](): Response[T] = {
    Response[T](Left("asdf"), 500, "failed", scala.collection.immutable.Seq(), List())
  }

  "TDCReserver" should {
    val order = Order(
      clientId = 1,
      integrationId = 3,
      sourceExternalTransactionId = "1",
      externalEventId = secondaryEventId,
      timestamp = Instant.ofEpochMilli(150),
      isPrimary = false,
      section = "123",
      row = "1",
      transactionType = TransactionType.Purchase,
      orderItems = Seq(OrderItem(seatNumber = "1", price = 17.95))
    )
    val barcodeOrder = order.copy(orderItems = order.orderItems.zipWithIndex.map {
      case (orderItem, index) => orderItem.copy(barcode = Some(s"barcode-${index + 1}"))
    })

    "reserve tickets successfully" in {
      tdcReserver ! ReserveTickets(order)
      val reservationOrder =
        order.copy(
          copyExternalTransactionId =
            Some(TDCReserverSpec.checkoutResponse.cart.transactionId.toString)
        )
      expectMsg(5.seconds, ReservationSuccessful(reservationOrder, 1))

      verify(pvService, times(1))
        .lockSeats(primaryEventId, tdcReserverConf.defaultBuyerTypeId, Seq(101))

      val lockSeatsCart = TDCReserverSpec.lockSeatsResponse
      verify(pvService, times(1))
        .checkout(
          lockSeatsCart.id,
          lockSeatsCart.totalSalesRevenue,
          tdcReserverConf.deliveryMethodId,
          tdcReserverConf.paymentMethodId,
          tdcReserverConf.patronAccountId
        )
    }

    "print tickets successfully" in {
      tdcReserver ! PrintTickets(order, TDCReserverSpec.checkoutResponse.cart.deliveries.head.id)

      expectMsg(5.seconds, PrintSuccessful(barcodeOrder))

      verify(pvService, times(1))
        .print(TDCReserverSpec.checkoutResponse.cart.deliveries.head.id, false)
    }

    "call reissue print when initial print fails" in {
      when(pvService.print(TDCReserverSpec.checkoutResponse.cart.deliveries.head.id, false))
        .thenReturn(Future.successful(failedResponse[ProVenuePrintResponse]()))
      when(pvService.print(TDCReserverSpec.checkoutResponse.cart.deliveries.head.id, true))
        .thenReturn(Future.successful(successResponse(TDCReserverSpec.printResponse)))

      tdcReserver ! PrintTickets(order, TDCReserverSpec.checkoutResponse.cart.deliveries.head.id)

      expectMsg(5.second, PrintSuccessful(barcodeOrder))

      verify(pvService, times(1))
        .print(TDCReserverSpec.checkoutResponse.cart.deliveries.head.id, false)
      verify(pvService, times(1))
        .print(TDCReserverSpec.checkoutResponse.cart.deliveries.head.id, true)
    }

    "send error message if reprint fails" in {
      when(pvService.print(TDCReserverSpec.checkoutResponse.cart.deliveries.head.id, false))
        .thenReturn(Future.successful(failedResponse[ProVenuePrintResponse]()))
      when(pvService.print(TDCReserverSpec.checkoutResponse.cart.deliveries.head.id, true))
        .thenReturn(Future.successful(failedResponse[ProVenuePrintResponse]()))

      tdcReserver ! PrintTickets(order, TDCReserverSpec.checkoutResponse.cart.deliveries.head.id)

      expectMsg(5.second, PrintFailed(order))

      verify(pvService, times(1))
        .print(TDCReserverSpec.checkoutResponse.cart.deliveries.head.id, false)
      verify(pvService, times(1))
        .print(TDCReserverSpec.checkoutResponse.cart.deliveries.head.id, true)
    }

    "send error message if tickets are not available" in {
      val response: Response[ProVenueLockSeatsResponse] =
        Response(Left("error"), 422, "error", scala.collection.immutable.Seq(), List())

      when(pvService.lockSeats(any[Int], any[Int], any[Seq[Int]]))
        .thenReturn(Future.successful(response))

      tdcReserver ! ReserveTickets(order)
      expectMsgType[ReservationFailed]

      verify(pvService, times(1))
        .lockSeats(primaryEventId, tdcReserverConf.defaultBuyerTypeId, Seq(101))

      val lockSeatsCart = TDCReserverSpec.lockSeatsResponse
      verify(pvService, times(0))
        .checkout(
          lockSeatsCart.id,
          lockSeatsCart.totalSalesRevenue,
          tdcReserverConf.deliveryMethodId,
          tdcReserverConf.paymentMethodId,
          tdcReserverConf.patronAccountId
        )

      verify(pvService, times(0))
        .print(TDCReserverSpec.checkoutResponse.cart.deliveries.head.id)
    }
  }
}

object TDCReserverSpec {

  val lockSeatsResponse = ProVenueLockSeatsResponse(
    1,
    ProVenueMoney("USD", 19.99),
    Seq(
      ProVenueLockSeatsResponseOffer(
        1,
        Seq(
          ProVenueLockSeatsResponseLineItem(
            Seq(ProVenueLockSeatsResponseTicket(1, 7, ProVenueMoney("USD", 19.99), 0)),
            "10"
          )
        )
      )
    ),
    Seq(ProVenueLockSeatsResponseBuyerType(1, "ADULT"))
  )

  val checkoutResponse = ProVenueCheckoutResponse(
    ProVenueCheckoutResponseCart(2, Seq(ProVenueCheckoutResponseDelivery(1, "SUCCESS")))
  )

  val printResponse = ProVenuePrintResponse(
    3,
    4,
    2,
    "PRINT",
    Seq(
      ProVenuePrintItem(
        0,
        7,
        ProVenueTicket(1, ProVenueMoney("USD", 19.99), 9, 0, 101, "barcode-1")
      ),
      ProVenuePrintItem(
        0,
        7,
        ProVenueTicket(2, ProVenueMoney("USD", 19.99), 13, 0, 102, "barcode-2")
      ),
      ProVenuePrintItem(
        0,
        7,
        ProVenueTicket(3, ProVenueMoney("USD", 19.99), 13, 0, 103, "barcode-3")
      )
    ),
    Seq(ProVenuePrintResponseBuyerType(1, "ADULT")),
    Seq(ProVenuePrintResponseDelivery(1, 1, "STATUS"))
  )
}
