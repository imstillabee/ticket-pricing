# Ticket Pricing Batch

## Setup

1. Setup AWS credentials
	
	Create an `~/.aws/credentials` file
	Example content:
	```sh
	[default]
	aws_access_key_id=
	aws_secret_access_key=
	```

## Running Locally

1. Make sure you have the right bucket and `ED_CLIENT_ID` set in `application.conf`.
1. In sbt shell `ticketPricingBatch/run`.

## Docker building instructions

1. Build JAR using `sbt ticketPricingBatch/assembly`
1. Run build docker file (which pulls the JAR file created by assembly command): `docker build -t tpb .`

## Other instructions

- If running from IntelliJ, ensure that the VM Parameters are set to `-Xms1024M -Xmx2048M -XX:+CMSClassUnloadingEnabled` (Memory needs to be 2048 to have room for all observations)

## 3rd Party Vendors

### MySportsFeeds

MySportsFeed is an the api used for pulling MLB Statistics into EventDynamic.

Read the [documentation](https://www.mysportsfeeds.com/data-feeds/api-docs/#) for more information.

#### Authorization
MySportsFeed requires an Authentication Header in the request sent. The auth header is defined as: 

`Authorization: Basic {encrypted_credentials}`
- `{encryped_credentials} ` => base_64_encode(username + ":" + password)

#### Primary Endpoints
The primary api endpoints used in ED are outlined below:

1. Get MLB Team Division Stats `https://api.mysportsfeeds.com/v1.2/pull/mlb/{season-name}/division_team_standings.{format}`
	- `{season-name}` => set to `current`, will receive the current season. Free version is set to `2017-regular`
	- {format} => `json`
	- Optional Params
		- `team={list-of-teams}` => Example: `NYM`
		- `teamstats={list-of-stats}` => Example: `W,L,GB` (For ED we are only wanting to return Wins[W] Losses[L] and Games Back[GB]) 

2. Get MLB Team Game Stats `https://api.mysportsfeeds.com/v1.2/pull/mlb/{season-name}/team_gamelogs.{format}`
	- `{season-name}` => set to `current`, will receive the current season. Free version is set to `2017-regular`
	- {format} => `json`
	- Optional Params
		- `team={list-of-teams}` => Example: `NYM`
		- `teamstats={list-of-stats}` => Example: `W` (For ED we are only wanting to return Wins[W]), all other statistics are calculated i.e. streak, home_opener, etc.
		- `date={date-range}` => Example: `until-today` will return all game logs for the season up to now
		- `sort={sort-by}` => currently sorting by `game.starttime`

3. Get MLB Team Game Schedule `https://api.mysportsfeeds.com/v1.2/pull/mlb/{season-name}/full_game_schedule.{format}`
	- `{season-name}` => set to `current`, will receive the current season. Free version is set to `2017-regular`
	- {format} => `json`
	- Optional Params
		- `team={list-of-teams}` => Example: `NYM`


#### Endpoint responsibilities
This section outlines the mlb statistics that each endpoints data is used to calculate in ED

1. `/division_team_standings`
	- Current Rank
	- \# Wins
	- \# Losses
	- Games Behind Division Leader (0 if 1st in division)

2. `/team_gamelogs`
	- streak

3. `/full_game_schedule`
	- Game Opponent
	- Game Number
	- Home Opener
	- Home Series Opener