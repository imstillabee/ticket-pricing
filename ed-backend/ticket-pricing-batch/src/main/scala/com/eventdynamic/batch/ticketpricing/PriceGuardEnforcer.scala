package com.eventdynamic.batch.ticketpricing

import com.eventdynamic.batch.ticketpricing.models.PipelinePrice
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.models._
import com.eventdynamic.utils.JsonUtil
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class PriceGuardEnforcer(serviceHolder: ServiceHolder, globalPriceFloor: BigDecimal) {
  private val logger = LoggerFactory.getLogger(this.getClass)

  def enforce(pipelinePrices: Seq[PipelinePrice]): Future[Seq[PipelinePrice]] =
    calculatePipelinePrices(pipelinePrices)

  private def calculatePipelinePrices(
    pipelinePrices: Seq[PipelinePrice]
  ): Future[Seq[PipelinePrice]] = {
    for {
      priceGuards <- serviceHolder.priceGuardService.getAllForEvents(
        pipelinePrices.map(p => p.eventId).toSet
      )
    } yield {
      // Create a map of price guards for fast look ups
      val rowGuards = priceGuards
        .map(pg => (pg.eventId, pg.section, pg.row) -> pg)
        .toMap

      // Check if we need to adjust the price
      pipelinePrices.toList.map(pipelinePrice => {
        if (pipelinePrice.overridden || pipelinePrice.price.isEmpty)
          pipelinePrice
        else {
          val rowGuard =
            rowGuards.get((pipelinePrice.eventId, pipelinePrice.section, pipelinePrice.row))

          // Check row guard if exists
          if (rowGuard.isDefined)
            guardPipelinePrice(pipelinePrice, rowGuard.get)
          else guardGlobalPriceFloor(pipelinePrice, globalPriceFloor)
        }
      })
    }
  }

  private def guardPipelinePrice(
    pipelinePrice: PipelinePrice,
    priceGuard: PriceGuard
  ): PipelinePrice =
    // We can safely call get here because we check for empty prices earlier in the pipeline
    if (priceGuard.minimumPrice > pipelinePrice.price.get) {
      logEnforcement(pipelinePrice, priceGuard, "minimum")
      pipelinePrice.copy(price = Some(priceGuard.minimumPrice))
    } else if (priceGuard.maximumPrice.isDefined && priceGuard.maximumPrice.get < pipelinePrice.price.get) {
      logEnforcement(pipelinePrice, priceGuard, "maximum")
      pipelinePrice.copy(price = priceGuard.maximumPrice.map(BigDecimal(_)))
    } else pipelinePrice

  private def logEnforcement(
    pipelinePrice: PipelinePrice,
    priceGuard: PriceGuard,
    minOrMax: String
  ): Unit = {
    val message = s"Price guard enforced a $minOrMax price"
    val context =
      Map(("message", message), ("predictedPrice", pipelinePrice), ("priceGuard", priceGuard))
    logger.info(JsonUtil.stringify(context))
  }

  private def guardGlobalPriceFloor(pipelinePrice: PipelinePrice, globalPriceFloor: BigDecimal) = {
    if (globalPriceFloor > pipelinePrice.price.get) {
      val message = s"Global price floor enforced a $globalPriceFloor price"
      val context = Map(
        "message" -> message,
        "predictedPrice" -> pipelinePrice,
        "globalPriceFloor" -> globalPriceFloor
      )
      logger.info(JsonUtil.stringify(context))

      pipelinePrice.copy(price = Some(globalPriceFloor))
    } else pipelinePrice
  }
}
