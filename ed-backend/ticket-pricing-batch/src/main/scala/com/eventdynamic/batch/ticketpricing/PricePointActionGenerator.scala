package com.eventdynamic.batch.ticketpricing

import com.eventdynamic.batch.ticketpricing.models.PipelinePriceByScale
import com.eventdynamic.mlbam.models.BuyerTypePrice
import com.eventdynamic.provenue.models.{
  ProVenueMoney,
  ProVenuePricePointAction,
  ProVenuePriceStructure
}
import com.eventdynamic.services.ProVenuePricingRuleService
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global

class PricePointActionGenerator(proVenuePricingRuleService: ProVenuePricingRuleService) {
  private val logger = LoggerFactory.getLogger(this.getClass)

  private case class AdjustmentRuleLookupKey(priceScaleId: Int, externalBuyerTypeId: String)

  def generatePricePointActions(
    clientId: Int,
    eventId: Int,
    priceStructure: ProVenuePriceStructure,
    priceMap: Map[PipelinePriceByScale, Seq[BuyerTypePrice]]
  ): Seq[ProVenuePricePointAction] = {
    assert(priceMap.keys.forall(_.eventId == eventId), "All prices must be for the supplied event")

    val externalPriceScaleToPrice =
      priceMap.keys.groupBy(_.externalPriceScaleId).mapValues(_.head)

    val actions = mapPriceStructureToActions(priceStructure)

    actions
      .filter(hasGeneratedPrice(_, externalPriceScaleToPrice))
      .map(assignGeneratedPrice(_, externalPriceScaleToPrice))
      .flatMap(applyAdjustmentRule(_, priceMap, externalPriceScaleToPrice))
      .filter(isChanged(_, actions))
  }

  private def hasGeneratedPrice(
    action: ProVenuePricePointAction,
    externalPriceScaleToScale: Map[Int, PipelinePriceByScale]
  ): Boolean = {
    val priceByScale = externalPriceScaleToScale.get(action.priceScaleId)
    val generatedPrice = priceByScale.flatMap(_.price)

    if (generatedPrice.isEmpty) {
      logger.warn(s"No generated price for proVenue price scale: ${action.priceScaleId}")
    }

    generatedPrice.isDefined
  }

  private def assignGeneratedPrice(
    action: ProVenuePricePointAction,
    externalPriceScaleToScale: Map[Int, PipelinePriceByScale]
  ): ProVenuePricePointAction = {
    val generated = externalPriceScaleToScale(action.priceScaleId)

    // We can safely `get` here as `hasGeneratedPrice` checks for `None`
    action.copy(price = ProVenueMoney("USD", generated.price.get))
  }

  private def applyAdjustmentRule(
    action: ProVenuePricePointAction,
    priceMap: Map[PipelinePriceByScale, Seq[BuyerTypePrice]],
    generatedPriceByExternalPriceScale: Map[Int, PipelinePriceByScale],
  ): Option[ProVenuePricePointAction] = {
    val generated = generatedPriceByExternalPriceScale(action.priceScaleId)
    val price = for {
      rules <- priceMap.get(generated)
      rule <- rules.find(_.externalBuyerTypeId == action.buyerTypeId.toString)
      price <- rule.price
    } yield price

    price.map(p => action.copy(price = ProVenueMoney("USD", p)))
  }

  private def isChanged(
    action: ProVenuePricePointAction,
    originalActions: Seq[ProVenuePricePointAction]
  ): Boolean = {
    !originalActions.contains(action)
  }

  private def mapPriceStructureToActions(
    priceStructure: ProVenuePriceStructure
  ): Seq[ProVenuePricePointAction] = {
    priceStructure.pricePoints
      .map(pricePoint => {
        val externalPriceScaleId = priceStructure.priceScaleRefs(pricePoint.priceScaleRefIndex).id
        val externalBuyerTypeId = priceStructure.buyerTypeRefs(pricePoint.buyerTypeRefIndex).id
        ProVenuePricePointAction(
          pricePoint.id,
          "UPDATE",
          externalBuyerTypeId,
          externalPriceScaleId,
          pricePoint.price
        )
      })
  }
}
