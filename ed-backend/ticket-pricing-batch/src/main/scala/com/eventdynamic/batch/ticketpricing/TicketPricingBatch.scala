package com.eventdynamic.batch.ticketpricing

import com.eventdynamic.IntegrationServiceBuilder
import com.eventdynamic.batch.ticketpricing.configs.{AWSConfig, PricingBatchConfig}
import com.eventdynamic.batch.ticketpricing.pipeline._
import com.eventdynamic.batch.ticketpricing.pipeline.composers.{FactorComposer, _}
import com.eventdynamic.batch.ticketpricing.pipeline.dispatchers.DispatcherFactory
import com.eventdynamic.batch.ticketpricing.pipeline.evaluators.ModelServerEvaluator
import com.eventdynamic.batch.ticketpricing.pipeline.predictors.{Predictor, RowPredictor}
import com.eventdynamic.batch.ticketpricing.pipeline.statscomposers.StatsComposerFactory
import com.eventdynamic.batch.ticketpricing.pipeline.strategies.pricing.{
  DemoPricingStrategy,
  EvaluatorPricingStrategy,
  PricingStrategy
}
import com.eventdynamic.batch.ticketpricing.services._
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.{JobType, ScheduledJob}
import com.eventdynamic.modelserver.ModelServerService
import com.eventdynamic.queues.ScheduledJobQueue
import com.eventdynamic.services.JobConfigService
import com.typesafe.config.{Config, ConfigFactory}
import org.slf4j.LoggerFactory
import resource._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object TicketPricingBatch {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Main function
    *
    * @param args command line args
    */
  def main(args: Array[String]): Unit = {
    logger.info(s"Starting ${this.getClass.getName}")

    // Load static config
    val appConfig = ConfigFactory.load()
    val pricingBatchConfig = PricingBatchConfig.forConfig(appConfig)
    val clientId = pricingBatchConfig.clientId

    logger.info(s"Starting for client id: $clientId")

    for (ed <- managed(new EDContext())) {
      val serviceHolder = ServiceHolder(ed)
      val scheduledJobQueue = new ScheduledJobQueue(ed)

      // Fetch pricing config
      val scheduledJob = Await.result(
        pollScheduledJob(clientId, serviceHolder.jobConfigService, scheduledJobQueue),
        30.seconds
      )

      // Exit early if there are no jobs in the queue
      if (scheduledJob.isEmpty) {
        logger.info(s"No jobs for client $clientId")
        return
      }
      val jobId = scheduledJob.flatMap(_.id).get

      try {
        // Build pipeline
        val pipeline = Await.result(
          buildPipeline(appConfig, pricingBatchConfig, scheduledJob.get, serviceHolder),
          30.seconds
        )

        // Execute pipeline
        Await.result(pipeline.execute(), 8.minutes)

        // Save job as success
        Await.result(scheduledJobQueue.complete(jobId), 30.seconds)
      } catch {
        case e: Throwable =>
          logger.error("Error executing pipeline", e)
          // Save job as failure
          Await.result(scheduledJobQueue.fail(jobId, e), 30.seconds)
      }
    }
  }

  /**
    * Builds a pipeline with dependencies
    */
  private def buildPipeline(
    appConfig: Config,
    pricingBatchConfig: PricingBatchConfig,
    scheduledJob: ScheduledJob,
    serviceHolder: ServiceHolder
  ): Future[Pipeline] = {
    val integrationServiceBuilder = new IntegrationServiceBuilder()
    val modelServerUrl = appConfig.getString("services.modelServer.baseUrl")
    val modelServerService = new ModelServerService(modelServerUrl)
    val clientId = pricingBatchConfig.clientId

    for {
      dispatchers <- DispatcherFactory
        .getDispatchersForClient(pricingBatchConfig, serviceHolder, integrationServiceBuilder)
      statsComposer <- StatsComposerFactory.getStatsComposerByClientId(clientId, serviceHolder)
    } yield {
      val weatherServices = WeatherServicesFactory.get(serviceHolder)
      val weatherComposer =
        if (pricingBatchConfig.enableWeatherData) Some(new WeatherComposer(weatherServices))
        else None

      new Pipeline(
        pricingBatchConfig = pricingBatchConfig,
        scheduledJob = scheduledJob,
        serviceHolder = serviceHolder,
        statsComposer = statsComposer,
        weatherComposer = weatherComposer,
        factorComposer = new FactorComposer(modelServerService, serviceHolder.eventService),
        dispatchers = dispatchers,
        fs = FileStore(AWSConfig.forConfig().s3Config),
        predictor = getPredictor(clientId, modelServerService)
      )
    }
  }

  /**
    * Polls the scheduled job queue for a event to process.
    */
  private def pollScheduledJob(
    clientId: Int,
    jobConfigService: JobConfigService,
    scheduledJobQueue: ScheduledJobQueue
  ): Future[Option[ScheduledJob]] =
    for {
      jobConfig <- jobConfigService.getRawConfig(clientId, JobType.Pricing).map {
        case Some(config) => config
        case None         => throw new Exception("Unable to get config")
      }
      scheduledJob <- scheduledJobQueue.poll(jobConfig.id.get)
    } yield scheduledJob

  private def getPredictor(clientId: Int, modelServerService: ModelServerService): Predictor = {
    // TODO: build the strategy dynamically from config
    val pricingStrategy = createActualStrategy(clientId, modelServerService)
    new RowPredictor(pricingStrategy)
  }

  /**
    * Create strategy for using the latest models.
    */
  private def createActualStrategy(
    clientId: Int,
    modelServerService: ModelServerService
  ): PricingStrategy = {
    val modelServerEvaluator =
      new ModelServerEvaluator(clientId.toString, modelServerService)
    new EvaluatorPricingStrategy(modelServerEvaluator)
  }

  /**
    * Create dummy strategies for demoing.
    */
  private def createDummyStrategy(): PricingStrategy = {
    new DemoPricingStrategy(true)
  }
}
