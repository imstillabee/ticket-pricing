package com.eventdynamic.batch.ticketpricing.configs

import awscala._
import com.typesafe.config.{Config, ConfigFactory}

case class S3Config(bucket: String, region: Region)

case class AWSConfig(s3Config: S3Config)

object AWSConfig {

  def forConfig(conf: Config = ConfigFactory.load(), path: String = "aws"): AWSConfig = {
    // S3 config
    val regionStr = conf.getString(s"$path.s3.region")
    val region = Region0.apply(regionStr)
    val bucket = conf.getString(s"$path.s3.bucket")
    val s3Config = S3Config(bucket, region)

    AWSConfig(s3Config)
  }
}
