package com.eventdynamic.batch.ticketpricing.configs

import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath}

case class ModelConfig(pricingModel: String, projectionModel: String)

object ModelConfig {
  implicit val pricingConfigFormat: Format[ModelConfig] = (
    (JsPath \ "pricingModel").format[String] and
      (JsPath \ "projectionModel").format[String]
  )(ModelConfig.apply, unlift(ModelConfig.unapply))

  def template(): ModelConfig = ModelConfig("pricing-model-path.pmml", "projection-model-path.pmml")
}
