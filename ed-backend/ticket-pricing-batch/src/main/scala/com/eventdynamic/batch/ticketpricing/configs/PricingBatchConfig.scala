package com.eventdynamic.batch.ticketpricing.configs

import com.typesafe.config.{Config, ConfigFactory}

case class PricingBatchConfig(
  clientId: Int,
  saveChunkSize: Int,
  skyboxUpdateChunkSize: Int,
  globalPriceFloor: BigDecimal,
  enableWeatherData: Boolean
)

object PricingBatchConfig {

  def forConfig(
    conf: Config = ConfigFactory.load(),
    path: String = "pricingBatch"
  ): PricingBatchConfig = {
    PricingBatchConfig(
      clientId = conf.getInt(s"$path.clientId"),
      saveChunkSize = conf.getInt(s"$path.saveChunkSize"),
      skyboxUpdateChunkSize = conf.getInt(s"$path.skyboxUpdateChunkSize"),
      globalPriceFloor = BigDecimal(conf.getDouble(s"$path.globalPriceFloor")),
      enableWeatherData = conf.getBoolean(s"$path.enableWeatherData ")
    )
  }
}
