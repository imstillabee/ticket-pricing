package com.eventdynamic.batch.ticketpricing.configs

import play.api.libs.json._

case class PricingConfig(models: ModelConfig, outputPath: String)

object PricingConfig {

  trait format {
    implicit lazy val format: OFormat[PricingConfig] = Json.format[PricingConfig]
  }

  def template(): PricingConfig =
    PricingConfig(ModelConfig.template, "output-path")
}
