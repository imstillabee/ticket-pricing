package com.eventdynamic.batch.ticketpricing.models

import java.sql.Timestamp
import java.util.Calendar

/**
  * Generic representation of an hour of the day with no year
  *
  * @param month month of the year, January = 1
  * @param day day of the month
  * @param hour hour of the day, midnight = 0 (rounded to the nearest hour)
  */
case class DateParts(month: Int, day: Int, hour: Int)

object DateParts {

  def from(time: Timestamp): DateParts = {
    val cal = Calendar.getInstance()
    cal.setTime(time)
    val month = cal.get(Calendar.MONTH) + 1
    val day = cal.get(Calendar.DAY_OF_MONTH)
    val hour = Math.round(cal.get(Calendar.HOUR_OF_DAY) + (cal.get(Calendar.MINUTE) / 60.0)).toInt

    DateParts(month, day, hour)
  }
}
