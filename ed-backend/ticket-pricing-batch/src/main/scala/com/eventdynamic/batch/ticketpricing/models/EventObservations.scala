package com.eventdynamic.batch.ticketpricing.models

/**
  * Holds all observation groups for an event.
  *
  * @param eventId
  * @param observationGroups
  */
case class EventObservations(eventId: Int, observationGroups: Seq[ObservationGroup]) {
  def first: Observation = observationGroups.head.first
  def timestamp = observationGroups.head.features.timestamp
}
