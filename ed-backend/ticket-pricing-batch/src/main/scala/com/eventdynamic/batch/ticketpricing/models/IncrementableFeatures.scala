package com.eventdynamic.batch.ticketpricing.models

import java.sql.Timestamp

/**
  * Class that encapsulates features that change over time and are consistent among all observations within an
  * ObservationGroup. Having these fields here removes the need to update individual observations.
  *
  * @param timestamp current timestamp of the reading
  * @param inventory remaining inventory for the group (can be fractional)
  * @param listPrice the listed price for all seats within the group
  * @param revenue total revenue
  * @param velocity recent velocity
  */
case class IncrementableFeatures(
  var timestamp: Timestamp,
  var inventory: Double,
  var listPrice: Option[BigDecimal],
  var revenue: BigDecimal,
  var velocity: BigDecimal
)
