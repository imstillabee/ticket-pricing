package com.eventdynamic.batch.ticketpricing.models

import java.sql.Timestamp
import java.time.{ZoneId, ZonedDateTime}

import com.eventdynamic.batch.ticketpricing.models.weather.ObservationWeather
import com.eventdynamic.models.ObservationBase
import com.eventdynamic.models.TransactionType.TransactionType

case class Observation(
  timestamp: Timestamp,
  eventSeatId: Int,
  seatId: Int,
  eventId: Int,
  eventName: String,
  integrationSeatId: Int,
  integrationEventId: Int,
  eventDate: ZonedDateTime,
  seat: String,
  row: String,
  section: String,
  listPrice: Option[BigDecimal],
  overridePrice: Option[BigDecimal],
  priceScaleId: Int,
  externalPriceScaleId: Int,
  priceScale: String,
  soldPrice: Option[BigDecimal],
  revenueCategoryId: Option[Int],
  buyerTypeCode: Option[String],
  integrationId: Option[Int],
  transactionType: Option[TransactionType],
  isListed: Boolean,
  isEventListed: Boolean,
  weather: Option[ObservationWeather],
  eventScore: Double,
  spring: Double,
  isHeld: Boolean
) {

  def toEventSeatInfo: EventSeatInfo = {
    EventSeatInfo(
      this.eventId,
      Timestamp.from(this.eventDate.toInstant),
      this.eventName,
      this.section,
      this.row,
      this.priceScale,
      this.listPrice,
      this.isHeld
    )
  }

  /**
    * Converts case class to seq for csv use. Should be kept in sync with Observation.header
    *
    * @return
    */
  def toSeq: Seq[Any] = {
    Seq(
      this.timestamp,
      this.seatId,
      this.eventId,
      this.eventName,
      this.integrationSeatId,
      this.integrationEventId,
      this.eventDate,
      this.seat,
      this.row,
      this.section,
      this.listPrice,
      this.overridePrice,
      this.externalPriceScaleId,
      this.priceScale,
      this.soldPrice.getOrElse(""),
      this.revenueCategoryId.getOrElse(""),
      this.buyerTypeCode.getOrElse(""),
      this.integrationId.getOrElse(""),
      this.transactionType.getOrElse("").toString,
      this.isListed,
      this.isEventListed,
      // weather
      this.weather.flatMap(_.actual_temp).getOrElse(""),
      // factors
      this.eventScore,
      this.spring
    )
  }

  def shouldPrice: Boolean = isListed && isEventListed
}

case class EventSeatInfo(
  id: Int,
  date: Timestamp,
  name: String,
  section: String,
  row: String,
  priceScale: String,
  listingPrice: Option[BigDecimal],
  isHeld: Boolean
)

object Observation {

  /**
    * Merge data sources together to form an observation.
    *
    * @param timestamp
    * @param base
    * @param weather
    * @return
    */
  def from(
    timestamp: Timestamp,
    base: ObservationBase,
    weather: Option[ObservationWeather],
    eventScore: Double,
    spring: Double
  ): Observation = {
    Observation(
      timestamp,
      base.eventSeatId,
      base.seatId,
      base.eventId,
      base.eventName,
      base.integrationSeatId,
      base.integrationEventId,
      base.eventDate.toInstant.atZone(ZoneId.of(base.venueTimeZone)),
      base.seat,
      base.row,
      base.section,
      base.listPrice,
      base.overridePrice,
      base.priceScaleId,
      base.externalPriceScaleId,
      base.priceScale,
      base.soldPrice,
      base.revenueCategoryId,
      base.buyerTypeCode,
      base.integrationId,
      base.transactionType,
      base.isListed,
      base.isEventListed,
      weather,
      eventScore,
      spring,
      base.isHeld
    )
  }

  /**
    * Header sequence used for CSVs.  This should be kept in sync with toSeq.
    *
    * @return
    */
  def header: Seq[String] = {
    Seq(
      "timestamp",
      "seatId",
      "eventId",
      "eventName",
      "integrationSeatId",
      "integrationEventId",
      "eventDate",
      "seat",
      "row",
      "section",
      "listPrice",
      "overridePrice",
      "priceScaleId",
      "priceScale",
      "soldPrice",
      "revenueCategoryId",
      "integrationBuyerTypeCode",
      "integrationId",
      "transactionType",
      "isListed",
      "isEventListed",
      "temp",
      "eventScore",
      "spring"
    )
  }
}
