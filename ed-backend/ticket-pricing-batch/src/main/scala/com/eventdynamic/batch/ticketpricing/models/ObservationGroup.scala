package com.eventdynamic.batch.ticketpricing.models

/**
  * The ObservationGroup encapsulates a group of observations grouped by some arbitrary key.  Typically this group
  * would be by scale or row.
  *
  * @param observations sequence of observations in the group
  * @param features the features that change over time
  */
case class ObservationGroup(observations: Seq[Observation], features: IncrementableFeatures) {
  def first: Observation = observations.head
}

object ObservationGroup {

  /**
    * Converts observations to an observation group.  Calculates inventory based on passed in observation data.
    * Pulls other mutable data from first observation.
    *
    * @param observations
    * @return
    */
  def fromObservations(observations: Seq[Observation]): ObservationGroup = {
    val features = IncrementableFeatures(
      observations.head.timestamp,
      observations.count(_.soldPrice.getOrElse(BigDecimal(-1)) < 0),
      observations.head.listPrice,
      BigDecimal(0.0),
      BigDecimal(0.0)
    )

    ObservationGroup(observations, features)
  }
}
