package com.eventdynamic.batch.ticketpricing.models

/**
  * Price data sent through the pipeline, used to dispatch prices.
  *
  * @param eventSeatId internal id of the event seat
  * @param eventId internal id of the event
  * @param seatId internal id of the seat
  * @param integrationEventId external id of the event
  * @param integrationSeatId external id of the seat
  * @param externalPriceScaleId external id of the price scale
  * @param price new price
  * @param shouldPrice are both the seat and event toggled on for this seat?
  * @param overridden is this pipeline price an overidden price?
  * @param available is this seat for this pipeline price available?
  */
case class PipelinePrice(
  eventSeatId: Int,
  eventId: Int,
  seatId: Int,
  integrationEventId: Int,
  integrationSeatId: Int,
  priceScaleId: Int,
  externalPriceScaleId: Int,
  price: Option[BigDecimal],
  shouldPrice: Boolean,
  overridden: Boolean,
  section: String,
  row: String,
  available: Boolean
) {

  def toPipelinePriceByScale: PipelinePriceByScale = {
    PipelinePriceByScale(
      this.eventId,
      this.integrationEventId,
      this.priceScaleId,
      this.externalPriceScaleId,
      this.price
    )
  }
}

case class PipelinePriceByScale(
  eventId: Int,
  integrationEventId: Int,
  priceScaleId: Int,
  externalPriceScaleId: Int,
  price: Option[BigDecimal]
)

case class PipelinePriceByRow(
  eventId: Int,
  integrationEventId: Int,
  price: Option[BigDecimal],
  seatIds: Seq[Int],
  integrationSeatIds: Seq[Int],
  section: String,
  row: String
)
