package com.eventdynamic.batch.ticketpricing.models

case class Price(eventId: Int, priceScaleId: Int, price: BigDecimal)
