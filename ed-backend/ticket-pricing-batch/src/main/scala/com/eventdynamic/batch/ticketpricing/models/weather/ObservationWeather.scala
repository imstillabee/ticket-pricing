package com.eventdynamic.batch.ticketpricing.models.weather

import com.eventdynamic.batch.ticketpricing.models.weather.WeatherCondition.WeatherCondition
import com.eventdynamic.modelserver.models.ModelServerWeather

object WeatherCondition extends Enumeration {
  type WeatherCondition = Value

  val None, LightRain, MediumRain, HeavyRain, Clear, Clouds, HeavyClouds, LightSnow, MediumSnow,
  HeavySnow = Value
}

case class ObservationWeather(
  actual_temp: Option[Float],
  average_temp: Float,
  condition: WeatherCondition.Value
)

object ObservationWeather {

  /**
    * Map a OpenWeatherMap weather code to our weather condition
    * @see https://openweathermap.org/weather-conditions
    *
    * @param code OpenWeatherMap weather code
    */
  def conditionForCode(code: Int): WeatherCondition.Value = {
    val codeAsString = code.toString

    // The first number in the code represents the weather category, e.g. rain, snow, etc.
    val categoryCode = codeAsString(0)
    val categoryMap = Map(
      '2' -> "rain", // thunderstorm
      '3' -> "rain", // drizzle
      '5' -> "rain", // rain
      '6' -> "snow",
      '7' -> "atmosphere", // mist, smoke, tornadoes...
      '8' -> "clouds"
    )

    // The last number in the code represents the weather intensity, e.g. light, medium, heavy, very heavy
    val intensityCode = codeAsString(2)

    // The middle number in the code represents a sub-category, eg. drizzle vs drizzle rain. Ignore it

    // Map category and intensity to a WeatherCondition
    (categoryMap.getOrElse(categoryCode, ""), intensityCode) match {
      case ("rain", '0')   => WeatherCondition.LightRain
      case ("rain", '1')   => WeatherCondition.MediumRain
      case ("rain", _)     => WeatherCondition.HeavyRain
      case ("snow", '0')   => WeatherCondition.LightSnow
      case ("snow", '1')   => WeatherCondition.MediumSnow
      case ("snow", _)     => WeatherCondition.HeavySnow
      case ("clouds", '0') => WeatherCondition.Clear
      case ("clouds", '1') => WeatherCondition.Clouds
      case ("clouds", _)   => WeatherCondition.HeavyClouds
      case _               => WeatherCondition.None
    }

  }

  def convertWeatherCondition(condition: WeatherCondition): String = {
    condition match {
      case WeatherCondition.Clear       => "CLEAR"
      case WeatherCondition.None        => "CLEAR"
      case WeatherCondition.Clouds      => "CLOUD"
      case WeatherCondition.HeavyClouds => "CLOUD"
      case WeatherCondition.HeavyRain   => "RAIN"
      case WeatherCondition.LightRain   => "RAIN"
      case WeatherCondition.MediumRain  => "RAIN"
      case WeatherCondition.LightSnow   => "SNOW"
      case WeatherCondition.MediumSnow  => "SNOW"
      case WeatherCondition.HeavySnow   => "SNOW"
    }
  }

  def createModelServerWeather(
    observationWeather: Option[ObservationWeather]
  ): Option[ModelServerWeather] = {
    observationWeather.map(
      weather =>
        ModelServerWeather(
          temp_average = weather.average_temp,
          temp = weather.actual_temp.getOrElse(weather.average_temp),
          weather_condition = ObservationWeather.convertWeatherCondition(weather.condition)
      )
    )
  }

  /**
    * Create ObservationWeather from OpenWeatherMapCode
    *
    * @param temp temperature
    * @param code open weather map code
    */
  def apply(temp: Option[Float], average_temp: Float, code: Int): ObservationWeather =
    ObservationWeather(temp, average_temp, conditionForCode(code))
}
