package com.eventdynamic.batch.ticketpricing.models.weather

import java.sql.Timestamp
import java.time.Instant

import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Failure, Success, Try}

case class OpenWeatherForecastCondition(id: Int, main: String, description: String)
case class OpenWeatherForecast(
  time: Timestamp,
  temp: Double,
  minTemp: Double,
  maxTemp: Double,
  conditions: Seq[OpenWeatherForecastCondition]
)
case class OpenWeatherForecastResponse(code: String, list: Seq[OpenWeatherForecast])

object OpenWeatherForecast {

  def fromResponse(str: String): Option[Seq[OpenWeatherForecast]] = {
    Try(Json.parse(str)) match {
      case Failure(_) => None
      case Success(res) =>
        val validation = res.validateOpt[OpenWeatherForecastResponse]
        if (validation.isSuccess) validation.get.map(_.list) else None
    }
  }

  implicit val weatherForecastConditionReads: Reads[OpenWeatherForecastCondition] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "main").read[String] and
      (JsPath \ "description").read[String]
  )(OpenWeatherForecastCondition.apply _)

  implicit val weatherForecastReads: Reads[OpenWeatherForecast] = (
    (JsPath \ "dt").read[Long].map(n => Timestamp.from(Instant.ofEpochMilli(n * 1000))) and
      (JsPath \ "main" \ "temp").read[Double] and
      (JsPath \ "main" \ "temp_min").read[Double] and
      (JsPath \ "main" \ "temp_max").read[Double] and
      (JsPath \ "weather").read[Seq[OpenWeatherForecastCondition]]
  )(OpenWeatherForecast.apply _)

  implicit val weatherForecastResponseReads: Reads[OpenWeatherForecastResponse] = (
    (JsPath \ "cod").read[String] and
      (JsPath \ "list").read[Seq[OpenWeatherForecast]]
  )(OpenWeatherForecastResponse.apply _)
}
