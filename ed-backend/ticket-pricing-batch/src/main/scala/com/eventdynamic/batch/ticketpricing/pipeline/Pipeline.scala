package com.eventdynamic.batch.ticketpricing.pipeline

import java.sql.Timestamp
import java.time.Instant

import com.eventdynamic.batch.ticketpricing.PriceGuardEnforcer
import com.eventdynamic.batch.ticketpricing.configs.PricingBatchConfig
import com.eventdynamic.batch.ticketpricing.pipeline.composers.{FactorComposer, WeatherComposer}
import com.eventdynamic.batch.ticketpricing.pipeline.dispatchers.Dispatcher
import com.eventdynamic.batch.ticketpricing.pipeline.phases._
import com.eventdynamic.batch.ticketpricing.pipeline.predictors.Predictor
import com.eventdynamic.batch.ticketpricing.pipeline.statscomposers.StatsComposer
import com.eventdynamic.batch.ticketpricing.services.{FileStore, ServiceHolder}
import com.eventdynamic.models.{ClientIntegrationComposite, ScheduledJob}
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class Pipeline(
  pricingBatchConfig: PricingBatchConfig,
  scheduledJob: ScheduledJob,
  serviceHolder: ServiceHolder,
  weatherComposer: Option[WeatherComposer],
  factorComposer: FactorComposer,
  statsComposer: StatsComposer,
  dispatchers: Map[ClientIntegrationComposite, Dispatcher],
  fs: FileStore,
  predictor: Predictor
)(implicit ec: ExecutionContext) {
  private val logger = LoggerFactory.getLogger(this.getClass)

  private val clientId = pricingBatchConfig.clientId

  def execute(): Future[Unit] = {
    logger.info("Processing Pipeline...")

    val jobStartTs = Instant.now()

    for {
      // Ingestion phase
      observations <- phase("Ingestion") {
        new IngestionPhase(
          clientId,
          statsComposer,
          weatherComposer,
          serviceHolder.observationService,
          factorComposer,
          fs,
          eventIds = Seq(scheduledJob.eventId),
          jobStartTs = Timestamp.from(jobStartTs)
        ).execute()
      }

      // Analysis phase
      predictedPipelinePrices <- phase("Analysis") {
        new AnalysisPhase(clientId, observations, serviceHolder, predictor).execute()
      }

      // Apply price modifications
      modifiedPipelinePrices <- phase("Event Price Modifiers") {
        new EventPriceModificationPhase(serviceHolder).apply(predictedPipelinePrices)
      }

      // Apply price floors
      pipelinePrices <- phase("Price Floors") {
        new PriceGuardEnforcer(serviceHolder, pricingBatchConfig.globalPriceFloor)
          .enforce(modifiedPipelinePrices)
      }

      // Publish prices
      _ <- phase("Publish") {
        new PublishPhase(pricingBatchConfig, serviceHolder, pipelinePrices, dispatchers).execute()
      }

      // Reporting Phase (Output to CSV)
      _ <- phase("Reporting") {
        // TODO NOTE: Commenting out since we don't need this in production. Need to discuss how to handle this
        Future {
          logger.info("Skipping reporting phase")
        }
//        new ReportingPhase(
//          eventInfo = observations.map(_.toEventSeatInfo),
//          predictedPipelinePrices = predictedPipelinePrices,
//          priceGuards = pipelinePrices,
//          clientIntegrations = dispatchers.keys.toSeq
//        ).execute()
      }
    } yield ()
  }

  private def phase[A](name: String)(phase: Future[A]): Future[A] = {
    logger.info(s"$name -- starting phase")

    phase.andThen {
      case Success(_) => logger.info(s"$name -- phase finished successfully")
      case Failure(e) => logger.error(s"$name -- phase failed", e)
    }
  }
}
