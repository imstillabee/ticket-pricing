package com.eventdynamic.batch.ticketpricing.pipeline.composers

import java.sql.Timestamp
import java.time.ZoneId

import com.eventdynamic.batch.ticketpricing.models.weather.ObservationWeather
import com.eventdynamic.models.ObservationBase
import com.eventdynamic.modelserver.ModelServerService
import com.eventdynamic.modelserver.models._
import com.eventdynamic.services.EventService
import com.eventdynamic.utils.DateHelper.timestampOrdering
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class FactorComposer(modelServerService: ModelServerService, eventService: EventService) {

  import FactorComposer._

  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Gets a mapping from eventId -> event score from the model server.
    *
    * @param clientId
    * @param timestamp
    * @param observations
    * @param weatherMap
    * @param clientStats
    * @param eventStatsMap
    * @return
    */
  def getFactorMap(
    clientId: Int,
    timestamp: Timestamp,
    observations: Seq[ObservationBase],
    weatherMap: Option[Map[WeatherKey, ObservationWeather]],
    clientStats: ModelServerClientStats,
    eventStatsMap: Map[Timestamp, ModelServerEventStats]
  ): Future[Map[Int, Factors]] = {
    logger.info("Creating event score map")
    val payload = getPayload(observations, weatherMap, timestamp, clientStats, eventStatsMap)

    modelServerService
      .getFactors(clientId.toString, payload)
      .map(
        resp =>
          resp.body match {
            case Left(str) =>
              throw new Exception(s"Unable to parse event scores for client $clientId: $str")
            case Right(body) =>
              // Get the sorted dates (examples sent sorted by date)
              // Zip the dates with the response event scores
              logger.info(s"Received ${body.eventScores.size} scores from model server")
              val eventData =
                observations
                  .map(
                    o => (o.eventDate, o.eventId, o.eventScoreModifier, o.springModifier / 100.0)
                  )
                  .distinct
                  .sortBy(_._1)
                  .map(t => (t._2, t._3, t._4))

              val eventIds = eventData.map(_._1)
              val eventScoreModifiers = eventData.map(_._2)
              val springModifiers = eventData.map(_._3)

              if (eventIds.size != body.eventScores.size)
                throw new Exception(
                  s"Number of events (${eventIds.size}) does not match number of event scores (${body.eventScores.size})"
                )

              if (eventIds.size != body.springValues.size)
                throw new Exception(
                  s"Number of events (${eventIds.size}) does not match number of spring values (${body.springValues.size})"
                )

              // Extract the factors into our value object
              // NOTE: multiplying spring by 100 to convert to our format!!
              val factors = for (i <- body.eventScores.indices)
                yield
                  Factors(
                    eventScore = body.eventScores(i),
                    eventScoreModifier = eventScoreModifiers(i),
                    spring = body.springValues(i) * 100,
                    springModifier = springModifiers(i) * 100
                  )

              eventIds.zip(factors).toMap
        }
      )
      .andThen {
        case Success(map) => saveEventScores(map)
        case Failure(e)   => logger.error("Unable to save event scores as request failed", e)
      }
  }

  def getPayload(
    observations: Seq[ObservationBase],
    weatherMap: Option[Map[WeatherKey, ObservationWeather]],
    timestamp: Timestamp,
    clientStats: ModelServerClientStats,
    eventStatsMap: Map[Timestamp, ModelServerEventStats]
  ): ModelServerFactorPayload = {
    logger.info(s"Building Factors Payload")

    // Create model server context for factors
    val context =
      ModelServerFactorContext(timestamp = timestamp.toInstant.toString, clientStats = clientStats)

    // Create model server examples for factors
    val examples = observations
      .groupBy(_.eventDate)
      .map(group => {
        val (eventDate, obs) = group

        // Get event stats
        val eventStats = eventStatsMap(eventDate)

        // Get weather data based on time and venue
        val weather = weatherMap
          .map(_(WeatherKey(eventDate, obs.head.venueId, obs.head.venueZip)))

        // Combine data into factor example
        ModelServerFactorExample(
          event_date =
            eventDate.toInstant.atZone(ZoneId.of(obs.head.venueTimeZone)).toOffsetDateTime.toString,
          event_score_modifier = obs.head.eventScoreModifier,
          spring_modifier = obs.head.springModifier / 100.0,
          weather = ObservationWeather.createModelServerWeather(weather),
          eventStats
        )
      })
      .toSeq
      .sortBy(_.event_date) // Ensure earliest events come first in examples

    // Create model server payload with context and examples
    ModelServerFactorPayload(context, examples)
  }

  /**
    * Updates the event scores based on new data.
    *
    * @param eventScoreMap
    * @return
    */
  private def saveEventScores(eventScoreMap: Map[Int, Factors]): Future[Int] = {
    logger.info(s"Updating ${eventScoreMap.size} event scores")
    logger.debug(s"Event score map: $eventScoreMap")

    Future
      .sequence(
        eventScoreMap
          .map(g => {
            logger.info(
              s"Updating factors: (${g._2.eventScore} + ${g._2.eventScoreModifier}), (${g._2.spring} + ${g._2.springModifier})"
            )
            eventService.updateFactors(
              g._1,
              Some(g._2.eventScore - g._2.eventScoreModifier),
              Some(g._2.spring - g._2.springModifier)
            )
          })
      )
      .map(_.sum)
      .andThen {
        case Success(n) => logger.info(s"Done updating $n event scores")
        case Failure(e) => logger.error(s"Failed to save event scores", e)
      }
  }
}

object FactorComposer {
  case class Factors(
    eventScore: Double,
    eventScoreModifier: Double,
    spring: Double,
    springModifier: Double
  )
}
