package com.eventdynamic.batch.ticketpricing.pipeline.composers

import java.sql.Timestamp
import java.util.concurrent.TimeUnit

import com.eventdynamic.batch.ticketpricing.models.weather.{ObservationWeather, OpenWeatherForecast}
import com.eventdynamic.batch.ticketpricing.models.DateParts
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.batch.ticketpricing.services.weather.OpenWeatherForecastService
import com.eventdynamic.models.ObservationBase
import com.eventdynamic.services.WeatherAverageService
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class WeatherServices(
  forecastService: OpenWeatherForecastService,
  averageService: WeatherAverageService
)

object WeatherServicesFactory {

  def get(serviceHolder: ServiceHolder): WeatherServices = {
    new WeatherServices(
      serviceHolder.openWeatherForecastService,
      serviceHolder.weatherAverageService
    )
  }
}

case class WeatherKey(eventDate: Timestamp, venueId: Int, venueZip: String)

class WeatherComposer(val services: WeatherServices) {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Get's a mapping from event/venue info to weather data
    *
    * @param observations
    * @return
    */
  def getWeatherMap(
    observations: Seq[ObservationBase]
  ): Future[Map[WeatherKey, ObservationWeather]] = {
    // Get elements of map in a list
    // Each value will be a Future, so we have to Future.sequence them first
    // After that we can convert to a map
    val weatherSeq = observations
      .map(o => WeatherKey(o.eventDate, o.venueId, o.venueZip))
      .distinct
      .map(key => getWeather(key).map(key -> _))

    Future.sequence(weatherSeq).map(_.toMap)
  }

  /**
    * Get weather data for a date/zip combination
    *
    * @param key
    * @return Forecast data if it exists, otherwise a historical average
    */
  private def getWeather(key: WeatherKey): Future[ObservationWeather] = {
    // Parse generic (non-year) date pieces from event date for grabbing average weather
    val dateParts = DateParts.from(key.eventDate)
    val forecastsFuture = services.forecastService
      .getForecast(key.venueZip)
      .recover {
        case e =>
          logger.error(s"Error getting weather forecast data", e)
          None
      }

    val averageFuture =
      services.averageService.get(key.venueId, dateParts.month, dateParts.day, dateParts.hour)

    for {
      forecasts <- forecastsFuture
      average <- averageFuture
    } yield {
      // The forecast is an optional field, warn if we don't have it
      if (forecasts.isEmpty)
        logger.warn(s"Failed to get weather forecast for ${key.venueZip}")

      // Average weather is a required field, throw an exception if we don't have it
      if (average.isEmpty)
        throw new Exception(
          s"Failed to get average weather for venue (${key.venueId}) at ${key.eventDate}"
        )

      val averageTemp = average.get.temp.toFloat
      val closestForecast = forecasts.flatMap(findCloseWeatherForecast(key.eventDate, _))
      val actualTemp = closestForecast.map(_.temp.toFloat)
      val condition =
        closestForecast
          .flatMap(_.conditions.headOption.map(_.id))
          .getOrElse(800) // TODO grab this value somewhere 800 is clear

      ObservationWeather(actualTemp, averageTemp, condition)
    }
  }

  /**
    * Find the closest weather forecast
    *
    * @param time
    * @param forecasts
    * @return Some(OpenWeatherForecast)
    */
  def findCloseWeatherForecast(
    time: Timestamp,
    forecasts: Seq[OpenWeatherForecast]
  ): Option[OpenWeatherForecast] = {
    // The forecast has values for every 3 hours, so this value should be above 90 minutes
    val MIN_DIFFERENCE = TimeUnit.HOURS.toMillis(2)

    val closest = forecasts.reduce((a, b) => {
      val aDiff = Math.abs(time.getTime - a.time.getTime)
      val bDiff = Math.abs(time.getTime - b.time.getTime)
      if (aDiff < bDiff) {
        a
      } else {
        b
      }
    })

    if (Math.abs(time.getTime - closest.time.getTime) < MIN_DIFFERENCE) {
      Some(closest)
    } else {
      None
    }
  }

}
