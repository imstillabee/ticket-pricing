package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.batch.ticketpricing.models.PipelinePrice
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

// Dispatch result can hold information from the pipeline like how many tickets were sold
case class DispatchResult(updates: Int)

/**
  * Base Dispatcher pushes prices at the seat level
  */
abstract class Dispatcher {
  private val logger = LoggerFactory.getLogger(this.getClass)

  def run(prices: Seq[PipelinePrice], clientId: Int): Future[DispatchResult] = {
    logger.info(s"Updating ${prices.length} prices")

    for {
      filtered <- filterByPolicy(prices)
      updates <- pushPrices(filtered, clientId)
    } yield {
      DispatchResult(updates)
    }
  }

  /**
    * Go to the ED database and filter prices based on price pushing policy
    *
    * @param prices all prices
    * @return filtered prices
    */
  protected def filterByPolicy(prices: Seq[PipelinePrice]): Future[Seq[PipelinePrice]] = {
    // TODO: this will need a clientID and integrationID
    // TODO: this will need whatever service this is stored in
    // TODO: might want some logging around this stage

    // Just do a pass through for now
    Future.successful(prices)
  }

  /**
    * Push prices to the integration
    *
    * @param prices prices by seat
    * @return number of seats successfully updated
    */
  protected def pushPrices(prices: Seq[PipelinePrice], clientId: Int): Future[Int]
}
