package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.IntegrationServiceBuilder
import com.eventdynamic.batch.ticketpricing.PricePointActionGenerator
import com.eventdynamic.batch.ticketpricing.configs.PricingBatchConfig
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.models.{
  ClientIntegrationComposite,
  EDTicketsClientIntegrationConfig,
  SkyboxClientIntegrationConfig,
  StubHubClientIntegrationConfig,
  TDCClientIntegrationConfig
}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object DispatcherFactory
    extends SkyboxClientIntegrationConfig.format
    with TDCClientIntegrationConfig.format
    with StubHubClientIntegrationConfig.format
    with EDTicketsClientIntegrationConfig.format {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Dynamically builds client integrations
    */
  def getDispatchersForClient(
    pricingBatchConfig: PricingBatchConfig,
    serviceHolder: ServiceHolder,
    integrationServiceBuilder: IntegrationServiceBuilder
  ): Future[Map[ClientIntegrationComposite, Dispatcher]] = {
    val clientId = pricingBatchConfig.clientId

    logger.info(s"Creating dispatchers for client $clientId")

    // exclude skybox subsidiary client integrations
    val skyboxClientIntegrations = Set("Skybox - Vivid Seats", "Skybox - Stubhub")

    serviceHolder.clientIntegrationService
      .getByClientId(clientId, true)
      .map(
        _.filter(ci => !skyboxClientIntegrations.contains(ci.name))
          .map(
            clientIntegration =>
              (
                clientIntegration,
                buildDispatcher(
                  pricingBatchConfig,
                  clientIntegration,
                  serviceHolder,
                  integrationServiceBuilder
                )
            )
          )
          .toMap
      )
  }

  /**
    * Builds a single dispatcher for the client integration.
    */
  def buildDispatcher(
    pricingBatchConfig: PricingBatchConfig,
    clientIntegration: ClientIntegrationComposite,
    serviceHolder: ServiceHolder,
    integrationServiceBuilder: IntegrationServiceBuilder
  ): Dispatcher = {
    logger.info(s"Creating ${clientIntegration.name} dispatcher")
    clientIntegration.name match {
      case "Tickets.com" =>
        val pvService = integrationServiceBuilder.buildProVenueService(
          clientIntegration.config[TDCClientIntegrationConfig]
        )
        val pgService = integrationServiceBuilder.buildPricingGridService()
        val actionGenerator = new PricePointActionGenerator(
          serviceHolder.proVenuePricingRuleService
        )
        val pricingRuleEnforcer = new ProVenuePricingRuleEnforcer(
          serviceHolder.proVenuePricingRuleService
        )
        new TDCDispatcher(
          pvService,
          actionGenerator,
          pricingRuleEnforcer,
          serviceHolder,
          clientIntegration,
          pgService
        )

      case "Skybox" =>
        val skyboxService = integrationServiceBuilder.buildSkyboxService(
          clientIntegration.config[SkyboxClientIntegrationConfig]
        )
        new SkyboxDispatcher(skyboxService, pricingBatchConfig.skyboxUpdateChunkSize)

      case "EDTickets" =>
        val service = integrationServiceBuilder.buildEDTicketsService(
          clientIntegration.config[EDTicketsClientIntegrationConfig]
        )
        new EDTicketsDispatcher(serviceHolder, service, clientIntegration.id.get)

      case "StubHub" =>
        val stubHubService = integrationServiceBuilder.buildStubHubService(
          clientIntegration.config[StubHubClientIntegrationConfig]
        )
        new StubHubDispatcher(serviceHolder, stubHubService, clientIntegration.id.get)

      case name =>
        throw new UnsupportedOperationException(s"Dispatcher for $name is not supported.")
    }
  }
}
