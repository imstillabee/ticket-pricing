package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.batch.ticketpricing.models.PipelinePrice
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.edtickets.EDTicketsService
import com.eventdynamic.edtickets.models.EDPrice
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class EDTicketsDispatcher(
  val serviceHolder: ServiceHolder,
  val apiService: EDTicketsService,
  val clientIntegrationId: Int
) extends Dispatcher {

  final private val updateBatchSize: Int = 25

  private val logger = LoggerFactory.getLogger(this.getClass)

  /** Push Prices by seat
    *
    * @param prices prices by seat
    * @return number of seats successfully updated
    */
  override def pushPrices(prices: Seq[PipelinePrice], clientId: Int): Future[Int] = {
    for {
      listingsMap <- getMappingsForListings(clientIntegrationId)
      numUpdated <- pushPricesByRow(prices, listingsMap)
    } yield numUpdated
  }

  /** Authenticate with EDTickets then push updated prices
    *
    * @param prices
    * @param listingsMap
    * @return
    */
  def pushPricesByRow(prices: Seq[PipelinePrice], listingsMap: Map[Int, String]): Future[Int] = {
    val edPrices: Seq[EDPrice] = mapToEDPrices(prices, listingsMap)

    for {
      _ <- apiService.authenticate.map(_.body.toOption match {
        case Some(_) => logger.info("Successfully authenticated with EDTickets")
        case _       => logger.error("Error authenticating with EDTickets")
      })
      updatedPrices <- updatePrices(edPrices)
    } yield {
      updatedPrices
    }

  }

  /** Filter out prices and map to EDPrice
    *
    * @param prices
    * @param listingsMap
    * @return
    */
  def mapToEDPrices(prices: Seq[PipelinePrice], listingsMap: Map[Int, String]): Seq[EDPrice] = {
    prices
      .groupBy(t => listingsMap.get(t.eventSeatId))
      .filter(_._2.forall(_.shouldPrice))
      .flatMap {
        case (Some(listingId), group) =>
          val prices =
            if (group.exists(_.overridden))
              group.filter(_.overridden)
            else group
          // Max by will return the highest value or None if there are no values
          val price = prices.maxBy(_.price).price

          // Returns None if the price is empty
          price.map(EDPrice(listingId, _))
        case _ => None
      }
      .toSeq
  }

  /** Update prices in batches to EDTickets
    *
    * @param edPrices
    * @return
    */
  def updatePrices(edPrices: Seq[EDPrice]): Future[Int] = {
    val futures = edPrices
      .grouped(updateBatchSize)
      .map(prices => {
        apiService
          .updatePrices(prices)
          .map(_.body.toOption match {
            case Some(_) =>
              logger.info(s"Done Updating ${prices.length} prices in EDTickets")
              prices.length
            case _ =>
              logger.error(s"Error Updating prices in EDTickets for $prices")
              0
          })
          .recover {
            case err =>
              logger.error(err.getMessage)
              0
          }
      })

    Future.sequence(futures).map(_.sum)
  }

  /** Get the mappings of eventSeatId -> listingId for pushing prices
    *
    * @param clientIntegrationId
    * @return
    */
  def getMappingsForListings(clientIntegrationId: Int): Future[Map[Int, String]] = {
    serviceHolder.integrationTransactions.getListingsMap(clientIntegrationId)
  }
}
