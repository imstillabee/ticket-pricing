package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.batch.ticketpricing.models.PipelinePriceByScale
import com.eventdynamic.mlbam.models.BuyerTypePrice
import com.eventdynamic.models.{ProVenuePricingRule, RoundingType}
import com.eventdynamic.services.ProVenuePricingRuleService
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.math.BigDecimal.RoundingMode

class ProVenuePricingRuleEnforcer(proVenuePricingRuleService: ProVenuePricingRuleService) {
  import ProVenuePricingRuleEnforcer._

  private val logger = LoggerFactory.getLogger(this.getClass)

  def enforcePricingRules(
    clientId: Int,
    pipelinePrices: Seq[PipelinePriceByScale]
  ): Future[Map[PipelinePriceByScale, Seq[BuyerTypePrice]]] = {
    for {
      adjustmentRules <- fetchAdjustmentRules(clientId, pipelinePrices)
    } yield {
      val priceScaleToPrice =
        pipelinePrices.groupBy(pp => EventScale(pp.eventId, pp.priceScaleId)).mapValues(_.head)
      applyAdjustmentRules(adjustmentRules, priceScaleToPrice)
    }
  }

  /**
    * apply all adjustment rules to pipeline price for each buyer type
    *
    * @param adjustmentRules
    * @param priceScaleToPrice
    * @return PipelinePriceByScale -> Seq[BuyerTypePrice]
    */
  private def applyAdjustmentRules(
    adjustmentRules: ScaleToRuleMap,
    priceScaleToPrice: Map[EventScale, PipelinePriceByScale]
  ): Map[PipelinePriceByScale, Seq[BuyerTypePrice]] = {
    adjustmentRules.transform((pipelinePrice, rules) => {
      rules.flatMap(rule => {
        rule.externalBuyerTypeIds.map(buyerTypeId => {
          val adjustedPrice =
            calculateAdjustment(pipelinePrice, rule.proVenuePricingRule, priceScaleToPrice)
          BuyerTypePrice(buyerTypeId, adjustedPrice)
        })
      })
    })
  }

  /**
    * Fetch pricing rules
    *
    * @param clientId
    * @param prices
    * @return PipelinePriceByScale -> Seq[ProVenuePricingRuleService.CompoundProVenuePricingRule]
    */
  private def fetchAdjustmentRules(
    clientId: Int,
    prices: Seq[PipelinePriceByScale]
  ): Future[ScaleToRuleMap] = {
    for {
      rules <- proVenuePricingRuleService.getAll(clientId, true)
    } yield {
      prices
        .map(
          price =>
            price -> rules
              .filter(_.priceScaleIds.contains(price.priceScaleId))
              .filter(_.eventIds.contains(price.eventId))
        )
        .toMap
    }
  }

  def calculateAdjustment(
    price: PipelinePriceByScale,
    rule: ProVenuePricingRule,
    generatedPriceByPriceScale: Map[EventScale, PipelinePriceByScale]
  ): Option[BigDecimal] = {
    var adjPrice = price.price

    // Apply mirror change
    if (rule.mirrorPriceScaleId.isDefined) {
      val eventScale = EventScale(price.eventId, rule.mirrorPriceScaleId.get)
      val pricesScale = generatedPriceByPriceScale.get(eventScale)

      // If the mirror scale is missing or has a missing price, we return an empty price so
      // this seat doesn't get pushed.
      if (pricesScale.isEmpty) {
        logger.warn("Pricing rule specified for a price scale that doesn't exist")
        return None
      } else if (pricesScale.get.price.isEmpty) {
        logger.warn("Pricing rule specified for a price scale that doesn't have a price")
        return None
      }

      adjPrice = pricesScale.get.price
    }

    // Apply percent change
    if (rule.percent.isDefined) {
      adjPrice = for {
        ap <- adjPrice
        percent <- rule.percent
      } yield ap + ap * percent.toDouble / 100.0
    }

    // Apply constant change
    if (rule.constant.isDefined) {
      adjPrice = for {
        ap <- adjPrice
        constant <- rule.constant
      } yield ap + constant
    }

    // Round result to the nearest dollar or cent depending on the rule definition
    rule.round match {
      case RoundingType.Ceil  => adjPrice.map(_.setScale(0, RoundingMode.CEILING))
      case RoundingType.Floor => adjPrice.map(_.setScale(0, RoundingMode.FLOOR))
      case RoundingType.Round => adjPrice.map(_.setScale(0, RoundingMode.HALF_UP))
      case RoundingType.None  => adjPrice.map(_.setScale(2, RoundingMode.HALF_UP))
    }
  }
}

object ProVenuePricingRuleEnforcer {

  type ScaleToRuleMap =
    Map[PipelinePriceByScale, Seq[ProVenuePricingRuleService.CompoundProVenuePricingRule]]

  case class EventScale(eventId: Int, priceScaleId: Int)
}
