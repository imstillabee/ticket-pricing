package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.batch.ticketpricing.models.{PipelinePrice, PipelinePriceByRow}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

abstract class RowDispatcher extends Dispatcher {
  import RowDispatcher._

  /**
    * Push prices to the integration
    *
    * @param prices prices by seat
    * @return number of seats successfully updated
    */
  override protected def pushPrices(prices: Seq[PipelinePrice], clientId: Int): Future[Int] = {
    pushPricesByRow(groupPricesByRow(prices), clientId)
  }

  /**
    * On top of normal filtering rules, the row dispatcher should not push prices for rows that have a single
    * seat toggled off.
    *
    * @param prices all prices
    * @return filtered prices
    */
  override protected def filterByPolicy(prices: Seq[PipelinePrice]): Future[Seq[PipelinePrice]] = {
    val superPrices = super.filterByPolicy(prices)

    superPrices
      .map(_.groupBy(groupKey).filter(_._2.forall(_.shouldPrice)).values.flatten.toSeq)
  }

  /**
    * Push prices that have been grouped by row to the integration.
    *
    * @param prices
    * @param clientId
    * @return
    */
  protected def pushPricesByRow(prices: Seq[PipelinePriceByRow], clientId: Int): Future[Int]
}

object RowDispatcher {
  private val groupKey: PipelinePrice => Row = price =>
    Row(price.eventId, price.integrationEventId, price.section, price.row)

  /**
    * Groups prices into PipelinePriceByRow to pass to subclasses.
    *
    * @param prices
    * @return
    */
  def groupPricesByRow(prices: Seq[PipelinePrice]): Seq[PipelinePriceByRow] = {
    prices
      .groupBy(groupKey)
      .flatMap(g => {
        // Filter for available prices only
        val availablePrices = g._2.filter(_.available)

        // Prioritize overridden prices, else use all prices to get max
        val prices =
          if (availablePrices.exists(_.overridden)) availablePrices.filter(_.overridden)
          else availablePrices

        if (prices.isEmpty)
          None
        else
          Some(
            PipelinePriceByRow(
              g._1.eventId,
              g._1.integrationEventId,
              prices.map(_.price).max,
              g._2.map(_.seatId),
              g._2.map(_.integrationSeatId),
              g._1.section,
              g._1.row
            )
          )
      })
      .toSeq
  }

  /**
    * Used to group data into rows.
    *
    * @param eventId
    * @param integrationEventId
    * @param section
    * @param row
    */
  private case class Row(eventId: Int, integrationEventId: Int, section: String, row: String)
}
