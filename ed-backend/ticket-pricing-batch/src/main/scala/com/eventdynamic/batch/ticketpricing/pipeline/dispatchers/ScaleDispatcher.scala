package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers
import com.eventdynamic.batch.ticketpricing.models.{PipelinePrice, PipelinePriceByScale}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Scale dispatcher pushes price at the scale level
  */
abstract class ScaleDispatcher extends Dispatcher {
  import ScaleDispatcher._

  /**
    * Push prices to the integration
    *
    * @param prices prices by seat
    * @return number of seats successfully updated
    */
  protected def pushPrices(prices: Seq[PipelinePrice], clientId: Int): Future[Int] = {
    val grouped = groupPricesByScale(prices)
    pushPricesByScale(grouped, clientId)
  }

  /**
    * Push prices to the integration
    *
    * @param prices prices by scale
    * @return number of scale successfully updated
    */
  protected def pushPricesByScale(prices: Seq[PipelinePriceByScale], clientId: Int): Future[Int]

  /**
    * On top of normal filtering rules, the scale dispatcher should not push prices for scales that have a single
    * seat toggled off.
    *
    * @param prices all prices
    * @return filtered prices
    */
  override protected def filterByPolicy(prices: Seq[PipelinePrice]): Future[Seq[PipelinePrice]] = {
    val superPrices = super.filterByPolicy(prices)

    superPrices.map(_.groupBy(groupKey).filter(_._2.forall(_.shouldPrice)).values.flatten.toSeq)
  }
}

object ScaleDispatcher {
  protected val groupKey: PipelinePrice => (Int, Int) = price =>
    (price.integrationEventId, price.externalPriceScaleId)

  /**
    * Group prices by scale
    *
    * @param prices all prices
    * @return prices per scale
    */
  def groupPricesByScale(prices: Seq[PipelinePrice]): Seq[PipelinePriceByScale] = {
    prices
      .groupBy(groupKey)
      .mapValues(p => {
        // Filter out prices that are unavailable
        val availablePrices =
          p.filter(pipelinePrice => pipelinePrice.available)

        // If there are overridden prices, get the onesRowDispatcherSpec
        val prices =
          if (availablePrices.exists(_.overridden))
            availablePrices.filter(_.overridden)
          else availablePrices

        if (prices.isEmpty)
          None
        else
          Some(prices.maxBy(_.price))
      })
      .values
      .flatten
      .toSeq
      .map(_.toPipelinePriceByScale)
  }
}
