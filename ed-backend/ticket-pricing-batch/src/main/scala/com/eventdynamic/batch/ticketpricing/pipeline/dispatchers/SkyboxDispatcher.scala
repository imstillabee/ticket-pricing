package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import java.time.Instant

import com.eventdynamic.batch.ticketpricing.models.PipelinePriceByRow
import com.eventdynamic.skybox.SkyboxService
import com.eventdynamic.skybox.models.SkyboxBulkPriceItem
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class SkyboxDispatcher(val apiService: SkyboxService, updateChunkSize: Int) extends RowDispatcher {

  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Push prices that have been grouped by row to the integration.
    *
    * @param prices
    * @param clientId
    * @return
    */
  override protected def pushPricesByRow(
    prices: Seq[PipelinePriceByRow],
    clientId: Int
  ): Future[Int] = {
    // We should only push prices for rows that have a defined price
    val (definedPrices, emptyPrices) = prices.partition(_.price.isDefined)
    if (emptyPrices.nonEmpty) {
      logger.warn(s"Ignoring ${emptyPrices.length} rows of prices due to missing prices")
    }

    val primaryEventIds = definedPrices.map(_.integrationEventId).distinct.toList
    logger.info(s"Pushing prices to Skybox for ${primaryEventIds.length} events")

    val totalChunks = (definedPrices.length / updateChunkSize) + 1

    logger.info(
      s"Pushing ${definedPrices.length} price updates to Skybox in $totalChunks chunks with chunk size of $updateChunkSize"
    )

    for {
      mappings <- getMappings(primaryEventIds)
      _ <- Future.sequence(definedPrices.grouped(updateChunkSize).zipWithIndex.map {
        case (chunk, index) =>
          logger.info(s"(${index + 1} of $totalChunks) pushing to Skybox")
          apiService
            .bulkUpdatePrices(mapToSkyboxPrices(chunk, mappings))
            .andThen {
              case Success(_) =>
                logger.info(s"(${index + 1} of $totalChunks) Skybox push success")
              case Failure(e) =>
                logger.error(s"(${index + 1} of $totalChunks) Skybox push failed", e)
            }
            .recover {
              case e =>
                logger.error(s"Inside pushPricesByRow Recover (Index: $index)", e)
                false
            }
      })
    } yield definedPrices.length
  }

  /**
    * Converts from pipeline prices to skybox bulk update prices.
    *
    * @param prices prices by seat
    * @return prices formatted for skybox api
    */
  def mapToSkyboxPrices(
    prices: Seq[PipelinePriceByRow],
    mappings: Map[Row, Seq[Int]]
  ): Seq[SkyboxBulkPriceItem] = {
    logger.info(s"Mapping prices for Skybox: $prices", prices)
    prices
      .flatMap(p => {
        mappings.get(Row(p.integrationEventId, p.section, p.row)) match {
          case Some(listingIds) =>
            // We can safely call `get` here because we previously filtered None prices
            listingIds.map(SkyboxBulkPriceItem(_, apiService.accountId, p.price.get))
          case None =>
            logger.warn(
              s"Unable to find Skybox listing for event seat with event id ${p.eventId}, section ${p.section}, row ${p.row}"
            )
            Seq()
        }
      })
      .sortBy(_.id)
  }

  /**
    * Gets the mappings from row data -> listing ids to map prices.
    *
    * We need to call they Skybox API to get all of the listings first because we don't store the listing ids
    * inside of Event Dynamic.
    *
    * @param primaryEventIds
    * @return a map of row data to all listing ids associated with that row
    */
  def getMappings(primaryEventIds: Seq[Int]): Future[Map[Row, Seq[Int]]] = {
    val inventoryFuture = apiService.getInventory(primaryEventIds)
    val invoicesFuture = apiService.getInvoices(primaryEventIds, Instant.EPOCH)
    logger.info(s"Getting inventory data for ${primaryEventIds.length} events.")

    for {
      inventory <- inventoryFuture
      invoices <- invoicesFuture
    } yield
      (inventory.body, invoices.body) match {
        case (Right(inventoryBody), Right(invoicesBody)) =>
          // Map the sold and unsold inventory to their row information and their listing id
          val inventoryRows =
            inventoryBody.rows
              .map(
                inventory =>
                  Row(inventory.eventId, inventory.section, inventory.row) -> inventory.id
              )
          val invoiceRows =
            invoicesBody.rows
              .map(invoice => Row(invoice.eventId, invoice.section, invoice.row) -> invoice.id)

          // Combine invoice and inventory rows, aggregate listing id seq together
          (invoiceRows ++ inventoryRows)
            .groupBy(_._1)
            .mapValues(_.map(_._2))
        case _ =>
          throw new Exception("Unable to get Skybox inventory/invoices to create mappings.")
      }
  }

  /**
    * Used to group listings by row.  Case classes will compare based on values inside of the class
    * instead of by reference.
    *
    * @param primaryEventId
    * @param section
    * @param row
    */
  case class Row(primaryEventId: Int, section: String, row: String)
}
