package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.batch.ticketpricing.models.PipelinePrice
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.stubhub.StubHubService
import com.eventdynamic.stubhub.models._
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class StubHubDispatcher(
  val serviceHolder: ServiceHolder,
  val apiService: StubHubService,
  val clientIntegrationId: Int
) extends Dispatcher {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /** Push Prices by seat
    *
    * @param prices prices by seat
    * @return number of seats successfully updated
    */
  override def pushPrices(prices: Seq[PipelinePrice], clientId: Int): Future[Int] = {
    val listingsMap = getMappingsForListings(clientIntegrationId)

    pushPricesByRow(prices, listingsMap)
  }

  /** Login and Push Prices by row to StubHub
    *
    * @param prices
    * @return
    */
  def pushPricesByRow(prices: Seq[PipelinePrice], listingsMap: Map[Int, String]): Future[Int] = {
    apiService
      .login()
      .flatMap {
        case ApiSuccess(_, res: StubHubLoginResponse) =>
          updatePrices(prices, listingsMap)
        case err =>
          logger.error(s"Error connecting to StubHub in $err")
          Future.successful(0)
      }
      .recover {
        case err =>
          logger.error(s"Unknown error connecting to StubHub: $err")
          0
      }
  }

  /** Push updated prices to StubHub
    *
    * @param prices
    * @return
    */
  def updatePrices(prices: Seq[PipelinePrice], listingsMap: Map[Int, String]): Future[Int] = {
    val futures = prices
      .groupBy(t => listingsMap.get(t.eventSeatId))
      .map {
        case (Some(listingId), group) =>
          val price = group.maxBy(_.price).price

          if (price.isDefined) {
            logger.info(s"Updating Price for listing $listingId to $price")
            apiService.updateListing(listingId.toInt, price.get).map {
              case ApiSuccess(_, response: StubHubListingResponse) =>
                logger.info(response.toString)
                1
              case err =>
                logger.error(err.toString)
                0
            }
          } else {
            logger.info(s"No listed price for listing $listingId")
            Future.successful(0)
          }

        case (None, group) =>
          logger.error(
            s"Could not find listingIds for event seats: ${group.map(_.eventSeatId).splitAt(100)._1}"
          )
          Future.successful(0)
      }
      .toSeq

    Future.sequence(futures).map(_.sum)
  }

  /** Get the mappings of eventSeatId -> listingId for pushing prices
    *
    * @param clientIntegrationId
    * @return
    */
  def getMappingsForListings(clientIntegrationId: Int): Map[Int, String] = {
    val future =
      serviceHolder.integrationTransactions.getListingsMap(clientIntegrationId)

    Await.result(future, 480.seconds)
  }
}
