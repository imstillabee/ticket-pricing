package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.batch.ticketpricing.PricePointActionGenerator
import com.eventdynamic.batch.ticketpricing.models.PipelinePriceByScale
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.mlbam.PricingGridService
import com.eventdynamic.mlbam.models.{BuyerTypePrice, PricingGridBatch}
import com.eventdynamic.models.{
  ClientIntegrationComposite,
  PriceGridConfig,
  TDCClientIntegrationConfig
}
import com.eventdynamic.provenue.ProVenueService
import com.eventdynamic.provenue.models._
import com.eventdynamic.utils.FutureUtil
import org.slf4j.LoggerFactory
import play.api.libs.json.Json
import scalaz.Memo

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Failure

class TDCDispatcher(
  val pv: ProVenueService,
  val actionGenerator: PricePointActionGenerator,
  val pricingRuleEnforcer: ProVenuePricingRuleEnforcer,
  serviceHolder: ServiceHolder,
  val clientIntegration: ClientIntegrationComposite,
  val pricingGridService: PricingGridService,
  private val saleTypes: Seq[String] = Seq("SINGLE", "GROUP")
) extends ScaleDispatcher
    with TDCClientIntegrationConfig.format {

  private val logger = LoggerFactory.getLogger(this.getClass)

  private val getPriceStructureMemo = Memo.immutableHashMapMemo {
    (pv.getPriceStructure _).tupled
  }

  /**
    * Push prices to pro venue
    *
    * @param prices prices by scale
    * @return number of price structures successfully updated
    */
  override def pushPricesByScale(prices: Seq[PipelinePriceByScale], clientId: Int): Future[Int] = {
    logger.info("Push price by scale")
    val priceGroupDefs = getPriceGroupDefinitions(clientId)
    for {
      _ <- pingProVenue(clientId)
      priceMap <- pricingRuleEnforcer.enforcePricingRules(clientId, prices)
      updatedPrices <- pushPricesToTDC(clientId, priceMap)
      _ <- pushPricesToMLBAM(clientId, priceMap, priceGroupDefs)
    } yield {
      updatedPrices
    }
  }

  /**
    * Get definitions for buyer type prices pushed to single and group mlbam pricing grid
    *
    * @param clientId
    * @return Seq[MLBAMPriceGroupDef]
    */
  private def getPriceGroupDefinitions(clientId: Int): Seq[PriceGridConfig] = {
    clientIntegration.config[TDCClientIntegrationConfig].mlbamGridConfigs
  }

  private def pushPricesToTDC(
    clientId: Int,
    priceMap: Map[PipelinePriceByScale, Seq[BuyerTypePrice]]
  ): Future[Int] = {
    val pricesByEvent = for {
      priceGroup <- priceMap.groupBy(_._1.eventId)
      saleType <- saleTypes
    } yield (saleType, priceGroup._1, priceGroup._2)

    FutureUtil
      .serializeFutures(pricesByEvent)(group => {
        val (saleType, eventId, generatedPrices) = group
        val integrationEventId = generatedPrices.keys.head.integrationEventId

        logger.info(s"Pushing prices to ProVenue for eventId: $eventId and saleType: $saleType")

        for {
          ProVenuePriceStructureResponse(priceStructure) <- getPriceStructure(
            integrationEventId,
            saleType
          )
          pricePointActions = actionGenerator
            .generatePricePointActions(clientId, eventId, priceStructure, generatedPrices)
          nUpdates <- bulkUpdatePricePoints(priceStructure.id, pricePointActions)
        } yield nUpdates
      })
      .map(_.sum)
  }

  private def pushPricesToMLBAM(
    clientId: Int,
    priceMap: Map[PipelinePriceByScale, Seq[BuyerTypePrice]],
    priceGroupDefs: Seq[PriceGridConfig]
  ): Future[Unit] = {
    serviceHolder.mlbamMappingService.getTeamIdByClientId(clientId).flatMap {
      case Some(teamId) =>
        for {
          pushResult <- pushBatchPriceScales(teamId, priceMap, priceGroupDefs)
        } yield {
          printMLBAMResponse(pushResult)
        }
      case None => Future.successful(logger.error("No MLBAM Team Id"))
    }
  }

  private def printMLBAMResponse(pushResult: List[String]): Unit = {
    val attrs = pushResult.map(scala.xml.XML.loadString(_).attributes)
    attrs.foreach(attr => {
      logger.info(s"Updated records: ${attr.get("totalGoodRecords").get}")
      logger.info(s"Failed records: ${attr.get("totalBadRecords").get}")
      logger.info(s"Skipped records: ${attr.get("totalSkippedRecords").get}")
      logger.info(s"Unchanged records: ${attr.get("totalUnchangedRecords").get}")
    })
  }

  private def pushBatchPriceScales(
    teamId: Int,
    priceMap: Map[PipelinePriceByScale, Seq[BuyerTypePrice]],
    priceGroupDefs: Seq[PriceGridConfig]
  ): Future[List[String]] = {
    Future.sequence(
      priceGroupDefs
        .map(priceGroupDef => {
          for {
            mappings <- getMLBAMMappings(priceMap, priceGroupDef)
            response <- pricingGridService
              .bulkUpdatePrices(filterPricingGridData(mappings), teamId, priceGroupDef.priceGroupId)
          } yield {
            response.body match {
              case Right(message) => message
              case Left(_)        => throw new Exception("Bad response from MLBAM")
            }
          }
        })
        .toList
    )
  }

  def getMLBAMMappings(
    pipelinePrices: Map[PipelinePriceByScale, Seq[BuyerTypePrice]],
    priceGroupDef: PriceGridConfig
  ): Future[Seq[(Option[Int], Option[Int], Option[BigDecimal])]] = {
    Future.sequence(
      pipelinePrices
        .map(pipelinePrice => {

          val buyerTypePrice = pipelinePrice._2
            .find(btp => priceGroupDef.externalBuyerTypeIds.contains(btp.externalBuyerTypeId))
          val price = buyerTypePrice.map(_.price).getOrElse(pipelinePrice._1.price)
          for {
            scheduleId <- serviceHolder.mlbamMappingService
              .getScheduleIdByEventId(pipelinePrice._1.eventId)
            sectionId <- serviceHolder.mlbamMappingService
              .getSectionIdByPriceScaleId(pipelinePrice._1.priceScaleId)
          } yield { (scheduleId, sectionId, price) }
        })
        .toSeq
    )
  }

  def filterPricingGridData(
    unfiltered: Seq[(Option[Int], Option[Int], Option[BigDecimal])]
  ): Seq[PricingGridBatch] = {
    unfiltered
      .collect {
        case (Some(scheduleId), Some(sectionId), Some(price)) =>
          PricingGridBatch(scheduleId, sectionId, price)
        case (None, Some(_), _)       => logger.warn(s"No ED event mapping")
        case (Some(_), None, _)       => logger.warn(s"No ED price scale mapping")
        case (None, None, _)          => logger.warn(s"No mappings found")
        case (Some(_), Some(_), None) => logger.warn("No ED price generated")
      }
      .collect {
        case PricingGridBatch(scheduleId, sectionId, price) =>
          PricingGridBatch(scheduleId, sectionId, price)
      }
  }

  /**
    * Call the ProVenue ping api and save the version response in the DB
    */
  private def pingProVenue(clientId: Int): Future[ProVenuePingResponse] = {
    val pingFuture = pv
      .ping()
      .map {
        case res if !res.isSuccess  => throw new Exception("Error pinging ProVenue")
        case res if res.body.isLeft => throw new Exception("Error parsing ProVenue ping response")
        case res                    => res.body.right.get
      }

    val clientIntegrationFuture = serviceHolder.clientIntegrationService
      .getByName(clientId, "Tickets.com")
      .map(_.getOrElse(throw new Exception(s"No Tickets.com integration for client: $clientId")))

    for {
      pingResponse <- pingFuture
      clientIntegration <- clientIntegrationFuture
      _ <- serviceHolder.clientIntegrationService
        .updateVersion(clientIntegration.id.get, pingResponse.proVenueAppVersion)
    } yield pingResponse
  }

  /**
    * Get price structure with error handling and logging
    */
  private def getPriceStructure(
    integrationEventId: Int,
    saleType: String
  ): Future[ProVenuePriceStructureResponse] = {
    getPriceStructureMemo(integrationEventId, saleType)
      .map {
        case response if !response.isSuccess =>
          throw new Exception(
            s"Failed to get price structure for eventId $integrationEventId and saleType $saleType"
          )
        case response if response.body.isLeft =>
          throw new Exception("Error process ProVenue price structure response")
        case response => response.body.right.get
      }
      .andThen {
        case Failure(err) => logger.error(s"Unknown error getting price structure: $err")
      }
  }

  /**
    * Bulk update price points with error handling and logging
    */
  private def bulkUpdatePricePoints(
    priceStructureId: Int,
    updates: Seq[ProVenuePricePointAction]
  ): Future[Int] = {
    // ProVenue throws a 500 if you pass it an empty array of price point actions...
    if (updates.isEmpty) {
      Future.successful(0)
    } else {
      pv.bulkUpdatePricePoints(priceStructureId, updates)
        .map {
          case r if !r.isSuccess =>
            throw new Exception(s"API error updating prices in ProVenue, $r")
          case r if r.body.isLeft =>
            throw new Exception(s"Unexpected ProVenueResponse, $r")
          case r => r.body.right.get.updatedCount
        }
    }
  }
}
