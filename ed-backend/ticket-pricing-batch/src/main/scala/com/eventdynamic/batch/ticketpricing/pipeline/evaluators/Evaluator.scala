package com.eventdynamic.batch.ticketpricing.pipeline.evaluators
import com.eventdynamic.batch.ticketpricing.models.ObservationGroup

import scala.concurrent.Future

abstract class Evaluator {

  /**
    * Function that takes in observation groups and returns observation groups with the new price on listPrice
    *
    * @param observations
    * @return
    */
  def evaluate(observations: Seq[ObservationGroup]): Future[Seq[ObservationGroup]]
}
