package com.eventdynamic.batch.ticketpricing.pipeline.evaluators

import com.eventdynamic.batch.ticketpricing.models.ObservationGroup
import com.eventdynamic.batch.ticketpricing.models.weather.ObservationWeather
import com.eventdynamic.modelserver.ModelServerService
import com.eventdynamic.modelserver.models.{
  ModelServerPriceContext,
  ModelServerPriceExample,
  ModelServerPricePayload
}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ModelServerEvaluator(val clientId: String, val modelServerService: ModelServerService)
    extends Evaluator {
  import ModelServerEvaluator._

  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Function that takes in observation groups and returns prices asynchronously
    *
    * @param observations
    * @return
    */
  override def evaluate(observations: Seq[ObservationGroup]): Future[Seq[ObservationGroup]] = {
    // Pair each observation with each
    val futures = observations
      .groupBy(_.first.eventId)
      .mapValues(sendRequest)
      .values

    Future.sequence(futures).map(_.flatten.toSeq)
  }

  /**
    * Sends the observation groups to the model server for inference.  Gets the new prices and updates the observation
    * groups list price in place.
    *
    * @param observationGroups observation groups in the same event
    * @return
    */
  private def sendRequest(
    observationGroups: Seq[ObservationGroup]
  ): Future[Seq[ObservationGroup]] = {
    val payload = buildPricePayload(observationGroups)

    modelServerService
      .getPrices(clientId, payload)
      .map(
        resp =>
          resp.body match {
            case Right(body) =>
              // Zip the observations (rows/scales) with the corresponding prices from the server
              // Set the list price for that group with the zipped price
              // In the future if observationGroups no longer have vars, use copy() here instead
              observationGroups
                .zip(body.prices)
                .foreach(zip => {
                  val (group, price) = zip

                  // Set the price to whatever we got back from the model server
                  // If we get back an empty response, we set the price to empty (a no-op)
                  group.features.listPrice = price
                  if (price.isEmpty)
                    logger.warn(
                      s"Null price response from model server for section ${group.first.section} row ${group.first.row}"
                    )
                })

              observationGroups
            case Left(str) =>
              throw new Exception(s"Unable to parse model server price response $str")
        }
      )
  }
}

object ModelServerEvaluator {

  def buildPricePayload(observationGroups: Seq[ObservationGroup]): ModelServerPricePayload = {
    val context = getPriceContext(observationGroups)
    val examples = getPriceExamples(observationGroups)
    ModelServerPricePayload(context, examples)
  }

  private def getPriceContext(observationGroups: Seq[ObservationGroup]): ModelServerPriceContext = {
    val observation = observationGroups.head.first

    ModelServerPriceContext(
      event_date = observation.eventDate.toOffsetDateTime.toString,
      event_score = observation.eventScore,
      spring = observation.spring / 100.0,
      timestamp = observation.timestamp.toInstant.toString,
      weather = ObservationWeather.createModelServerWeather(observation.weather)
    )
  }

  private def getPriceExamples(
    observationGroups: Seq[ObservationGroup]
  ): Seq[ModelServerPriceExample] = {
    observationGroups.map(
      og =>
        ModelServerPriceExample(
          row = og.first.row,
          scale = og.first.priceScale,
          section = og.first.section,
          qty_left = og.features.inventory.toInt
      )
    )
  }
}
