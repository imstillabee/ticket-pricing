package com.eventdynamic.batch.ticketpricing.pipeline.evaluators

import awscala.s3.{Bucket, S3Object}
import com.eventdynamic.batch.ticketpricing.pipeline.predictors.FeatureMap
import com.eventdynamic.batch.ticketpricing.services.FileStore
import org.dmg.pmml.{FieldName, Model, PMML}
import org.jpmml.evaluator._
import org.jpmml.model.PMMLUtil

import scala.collection.JavaConverters

class PMMLEvaluator(fs: FileStore, path: String) {
  val evaluatorFactory: ModelEvaluatorFactory = ModelEvaluatorFactory.newInstance()
  val bucket: Option[Bucket] = fs.s3.bucket(fs.s3Config.bucket)
  val model: Option[S3Object] = fs.s3.get(bucket.get, path)
  val pmmModel: PMML = PMMLUtil.unmarshal(model.get.content)

  val evaluator: ModelEvaluator[_ <: Model] = evaluatorFactory.newModelEvaluator(pmmModel)

  val activeFields: Seq[InputField] =
    JavaConverters.asScalaIteratorConverter(evaluator.getActiveFields.iterator()).asScala.toSeq

  val targetFields: Seq[TargetField] =
    JavaConverters.asScalaIteratorConverter(evaluator.getTargetFields.iterator()).asScala.toSeq

  val outputFields: Seq[OutputField] =
    JavaConverters.asScalaIteratorConverter(evaluator.getOutputFields.iterator()).asScala.toSeq

  /**
    * Prepare inputs for passing to model
    *
    * @param inputs
    * @return
    */
  def prepare(inputs: Seq[FeatureMap]): Seq[Map[FieldName, FieldValue]] = {
    inputs.map(
      input =>
        activeFields
          .map(field => field.getName -> field.prepare(input(field.getName.toString)))
          .toMap
    )
  }

  /**
    * Evaluate Outputs
    *
    * @param inputs
    * @return
    */
  def evaluate(inputs: Seq[Map[FieldName, FieldValue]]): Seq[Seq[String]] = {
    inputs
      .map(input => JavaConverters.mapAsJavaMapConverter(input).asJava)
      .map(input => evaluator.evaluate(input))
      .map(result => targetFields.map(field => result.get(field.getName).toString))
  }
}
