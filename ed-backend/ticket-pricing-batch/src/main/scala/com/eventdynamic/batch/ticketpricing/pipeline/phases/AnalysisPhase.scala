package com.eventdynamic.batch.ticketpricing.pipeline.phases

import com.eventdynamic.batch.ticketpricing.models.{Observation, PipelinePrice}
import com.eventdynamic.batch.ticketpricing.pipeline.predictors.Predictor
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.models.EventSeatListedPriceUpdate
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class AnalysisPhase(
  clientId: Int,
  observations: Seq[Observation],
  serviceHolder: ServiceHolder,
  predictor: Predictor
) extends PipelinePhase[Seq[PipelinePrice]] {

  private val logger = LoggerFactory.getLogger(this.getClass)

  def execute(): Future[Seq[PipelinePrice]] = {
    Future {
      // TODO: predict should probably return a future
      predictor.predict(observations)
    }
  }
}
