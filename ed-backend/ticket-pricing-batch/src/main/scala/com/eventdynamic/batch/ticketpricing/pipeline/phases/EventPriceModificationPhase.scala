package com.eventdynamic.batch.ticketpricing.pipeline.phases

import com.eventdynamic.batch.ticketpricing.models.PipelinePrice
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.models._
import scala.math.BigDecimal.RoundingMode

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class PriceModifierForPipelinePrice(pipelinePrice: PipelinePrice, percentPriceModifier: Int)
case class PriceUpdatePriceModifier(
  updatedPrice: EventSeatListedPriceUpdate,
  percentPriceModifier: Int
)

class EventPriceModificationPhase(serviceHolder: ServiceHolder) {

  def apply(pipelinePrices: Seq[PipelinePrice]): Future[Seq[PipelinePrice]] =
    applyEventPriceModification(pipelinePrices)

  private def applyEventPriceModification(
    pipelinePrices: Seq[PipelinePrice]
  ): Future[Seq[PipelinePrice]] = {
    for {
      events <- serviceHolder.eventService.getByIdBulk(pipelinePrices.map(_.eventId).toSet)
    } yield {
      val eventPriceModifiers = mapEventPriceModifiers(events)

      val priceModifierForPipelinePrices = pipelinePrices.map(
        ppp => PriceModifierForPipelinePrice(ppp, eventPriceModifiers(ppp.eventId))
      )
      priceModifierForPipelinePrices.map(calculateModifiedPipelinePrice)
    }
  }

  private def calculateModifiedPipelinePrice(
    priceModifierForPipelinePrice: PriceModifierForPipelinePrice
  ): PipelinePrice = {
    if (priceModifierForPipelinePrice.pipelinePrice.overridden) {
      priceModifierForPipelinePrice.pipelinePrice
    } else {
      val newPrice = priceModifierForPipelinePrice.pipelinePrice.price
        .map(_ * calculateModifier(priceModifierForPipelinePrice.percentPriceModifier))
        .map(_.setScale(2, RoundingMode.HALF_UP))

      priceModifierForPipelinePrice.pipelinePrice.copy(price = newPrice)
    }
  }

  private def mapEventPriceModifiers(events: Seq[Event]): Map[Int, Int] = {
    events.map(e => (e.id.get, e.percentPriceModifier)).toMap
  }

  private def calculateModifier(percentPriceModifier: Int): BigDecimal = {
    1 + BigDecimal(percentPriceModifier / 100.00)
  }
}
