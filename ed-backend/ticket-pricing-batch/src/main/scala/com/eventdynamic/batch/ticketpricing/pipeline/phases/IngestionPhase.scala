package com.eventdynamic.batch.ticketpricing.pipeline.phases

import java.io.File
import java.sql.Timestamp

import awscala.s3.PutObjectResult
import com.eventdynamic.batch.ticketpricing.models.Observation
import com.eventdynamic.batch.ticketpricing.models.weather.ObservationWeather
import com.eventdynamic.batch.ticketpricing.pipeline.composers.{
  FactorComposer,
  WeatherComposer,
  WeatherKey
}
import com.eventdynamic.batch.ticketpricing.pipeline.statscomposers.StatsComposer
import com.eventdynamic.batch.ticketpricing.services.FileStore
import com.eventdynamic.models.ObservationBase
import com.eventdynamic.services.ObservationService
import com.eventdynamic.utils.{DateHelper, FutureUtil}
import com.github.tototoshi.csv.CSVWriter
import org.slf4j.LoggerFactory
import resource.managed

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class IngestionPhase(
  clientId: Int,
  statsComposer: StatsComposer,
  weatherComposer: Option[WeatherComposer],
  observationService: ObservationService,
  factorComposer: FactorComposer,
  fs: FileStore,
  eventIds: Seq[Int],
  jobStartTs: Timestamp
) extends PipelinePhase[Seq[Observation]] {

  private val logger = LoggerFactory.getLogger(this.getClass)

  def execute(): Future[Seq[Observation]] = {
    compose(clientId, eventIds, jobStartTs)
      .map(obs => {
        if (obs.isEmpty) {
          throw new Exception("No observations to ingest")
        }
        obs
      })
    //  logger.info("Writing Observations...")
    //  ingestion.write(outputPath, observations, jobStartTs)
    //  logger.info("Done Writing Observations")
  }

  /**
    * Compose observations result from baseObservation and 3rd party vendors
    *
    * @param clientId
    * @param eventIds
    * @param jobStart timestamp to start looking for events
    * @return Seq[Observation]
    */
  private def compose(
    clientId: Int,
    eventIds: Seq[Int],
    jobStart: Timestamp
  ): Future[Seq[Observation]] = {
    for {
      baseObservations <- observationService.getAllForEvents(eventIds, clientId)
      weatherMap <- getWeatherMap(baseObservations)

      clientStats <- statsComposer.getClientStats(baseObservations)
      eventStatsMap <- statsComposer.getEventStatsMap(baseObservations, jobStart)
      factorMap <- factorComposer.getFactorMap(
        clientId,
        jobStart,
        baseObservations,
        weatherMap,
        clientStats,
        eventStatsMap
      )
    } yield
      baseObservations.map(observation => {
        val factors = factorMap(observation.eventId)

        // Get weather data if weather is available
        val weatherKey =
          WeatherKey(observation.eventDate, observation.venueId, observation.venueZip)
        val weather = weatherMap.map(_(weatherKey))

        Observation
          .from(jobStart, observation, weather, factors.eventScore, factors.spring)
      })
  }

  private def getWeatherMap(
    baseObservations: Seq[ObservationBase]
  ): Future[Option[Map[WeatherKey, ObservationWeather]]] = {
    if (weatherComposer.isDefined)
      weatherComposer.get.getWeatherMap(baseObservations).map(Some(_))
    else
      Future.successful(None)
  }

  /**
    * write results from postgres to a file and upload to S3
    *
    * @param outputPath script config
    * @param observations observations to write
    * @return PutObjectResult
    */
  private def write(
    outputPath: String,
    observations: Seq[Observation],
    jobStart: Timestamp
  ): PutObjectResult = {
    val f = File.createTempFile("EventDynamic", "Observations")

    for (writer <- managed(CSVWriter.open(f))) {
      writer.writeRow(Observation.header)
      observations.foreach(observation => {
        writer.writeRow(observation.toSeq)
      })
    }

    val fileName = DateHelper.toISOString(jobStart)
    fs.upload(s"$outputPath/$fileName.csv", f)
  }
}
