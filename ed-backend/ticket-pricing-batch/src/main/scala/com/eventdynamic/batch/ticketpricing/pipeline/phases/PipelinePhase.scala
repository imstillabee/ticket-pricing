package com.eventdynamic.batch.ticketpricing.pipeline.phases

import scala.concurrent.Future

trait PipelinePhase[S] {
  def execute(): Future[S]
}
