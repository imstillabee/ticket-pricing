package com.eventdynamic.batch.ticketpricing.pipeline.phases

import com.eventdynamic.batch.ticketpricing.configs.PricingBatchConfig
import com.eventdynamic.batch.ticketpricing.models.PipelinePrice
import com.eventdynamic.batch.ticketpricing.pipeline.dispatchers.{DispatchResult, Dispatcher}
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.models.{
  ClientIntegrationComposite,
  EventSeatListedPriceUpdate,
  EventSeatListedPriceUpdateByRow,
  EventSeatListedPriceUpdateByScale
}
import com.eventdynamic.utils.FutureUtil
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class PublishPhase(
  pricingBatchConfig: PricingBatchConfig,
  serviceHolder: ServiceHolder,
  pipelinePrices: Seq[PipelinePrice],
  dispatchers: Map[ClientIntegrationComposite, Dispatcher]
) extends PipelinePhase[Unit] {
  import PublishPhase._

  private val SAVE_CHUNK_SIZE = pricingBatchConfig.saveChunkSize
  private val clientId = pricingBatchConfig.clientId

  def execute(): Future[Unit] = {
    // Dispatch prices then save them to the EventDynamic database
    for {
      _ <- dispatch()
      _ <- save()
    } yield ()
  }

  private def dispatch(): Future[Iterable[DispatchResult]] = {
    logger.info("Dispatching Prices...")

    Future
      .sequence(dispatchers.map(d => {
        val (clientIntegration, dispatcher) = d
        val modifiedPipelinePrices = pipelinePrices.map(pipelinePrice => {
          val modifiedPrice = pipelinePrice.price.map(
            price =>
              serviceHolder.secondaryPricingRulesService
                .applySecondaryPricingRule(clientIntegration, price)
          )
          pipelinePrice.copy(price = modifiedPrice)
        })
        dispatcher.run(modifiedPipelinePrices, clientId).andThen {
          case Success(DispatchResult(n)) =>
            logger.info(s"Done dispatching $n prices to ${clientIntegration.name}")
          case Failure(f) =>
            logger.error(s"Failed to dispatch prices to ${clientIntegration.name}: ${f.getMessage}")
        }
      }))
  }

  private def save(): Future[Int] = {
    val priceUpdates = pipelinePricesToUpdates(pipelinePrices)
    logger.info(s"Saving ${priceUpdates.size} price groups...")

    val groupedPriceUpdates = priceUpdates
      .grouped(SAVE_CHUNK_SIZE)
      .toIterable

    FutureUtil
      .serializeFutures(groupedPriceUpdates)(p => {
        serviceHolder.eventSeatService
          .updateListedPrices(p)
          .andThen {
            case Success(s) => logger.debug(s"Done saving chunk of $s prices")
            case Failure(f) =>
              logger.error(s"Failed to save prices: ${f.getMessage}")
          }
      })
      .map(_.sum)
      .andThen {
        case Success(sum) => logger.info(s"Done Saving $sum Prices")
        case Failure(f)   => logger.error(s"Failed to save prices: ${f.getMessage}")
      }
  }
}

object PublishPhase {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Converts pipeline prices to event seat listed price updates.
    *
    * Order of priority:
    *   - If all prices in each scale match, update by scale
    *   - If all prices for each row match, update by row
    *   - [error] not doing seat level prices yet
    *
    * @param pipelinePrices
    * @return
    */
  def pipelinePricesToUpdates(pipelinePrices: Seq[PipelinePrice]): Seq[EventSeatListedPriceUpdate] =
    pipelinePrices
      .groupBy(_.eventId)
      .flatMap(eventGroup => {
        // Group data by scale and by row
        val (eventId, eventPrices) = eventGroup
        val scalePrices = eventPrices.filter(_.price.isDefined).groupBy(_.priceScaleId)
        val rowPrices = eventPrices.filter(_.price.isDefined).groupBy(pp => (pp.section, pp.row))

        // First check if all scales have the same price, then rows
        if (scalePrices.values.forall(_.map(_.price).distinct.length == 1)) {
          logger.info(s"Grouping price updates for event $eventId by scale")
          scalePrices
            .map(
              scaleGroup =>
                EventSeatListedPriceUpdateByScale(
                  eventId = eventId,
                  priceScaleId = scaleGroup._1,
                  price = scaleGroup._2.head.price.get // we've filtered empty prices out
              )
            )
        } else if (rowPrices.values.forall(_.map(_.price).distinct.length == 1)) {
          logger.info(s"Grouping price updates for event $eventId by row")
          rowPrices.map(
            rowGroup =>
              EventSeatListedPriceUpdateByRow(
                eventId = eventId,
                section = rowGroup._1._1,
                row = rowGroup._1._2,
                price = rowGroup._2.head.price.get // we've filtered empty prices out
            )
          )
        } else
          throw new Exception("Unable to group pipeline prices to price updates.")
      })
      .toSeq
}
