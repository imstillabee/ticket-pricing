package com.eventdynamic.batch.ticketpricing.pipeline.phases

import java.io.File

import com.eventdynamic.batch.ticketpricing.models.{EventSeatInfo, PipelinePrice}
import com.eventdynamic.models.ClientIntegrationComposite
import com.github.tototoshi.csv._

import scala.concurrent.Future

case class EventSeatKey(eventId: Int, section: String, row: String)

class ReportingPhase(
  eventInfo: Seq[EventSeatInfo],
  predictedPipelinePrices: Seq[PipelinePrice],
  priceGuards: Seq[PipelinePrice],
  clientIntegrations: Seq[ClientIntegrationComposite]
) extends PipelinePhase[Unit] {

  def execute(): Future[Unit] = {
    Future.successful({
      val (predictedPriceMap, priceGuardMap) = createMaps(predictedPipelinePrices, priceGuards)
      val rows = createRows(eventInfo, predictedPriceMap, priceGuardMap, clientIntegrations)
      val header = createHeaders(clientIntegrations)
      generateReport(header, rows)
    })
  }

  private def generateReport(header: Seq[String], rows: Seq[Seq[String]]): Unit = {
    val outputFile = new File("report.csv")
    val writer = CSVWriter.open(outputFile)
    writer.writeAll(header +: rows)
    writer.close()
  }

  private def createMaps(
    predictedPipelinePrices: Seq[PipelinePrice],
    priceGuards: Seq[PipelinePrice]
  ): (Map[EventSeatKey, Option[BigDecimal]], Map[EventSeatKey, Option[BigDecimal]]) = {
    // TODO: May need to rethink how to do this if we ever need to price at the seat level
    val predictedPriceMap =
      predictedPipelinePrices
        .map(pp => (EventSeatKey(pp.eventId, pp.section, pp.row), pp.price))
        .toMap
    val priceGuardMap =
      priceGuards.map(pf => (EventSeatKey(pf.eventId, pf.section, pf.row), pf.price)).toMap
    (predictedPriceMap, priceGuardMap)
  }

  private def createRows(
    eventInfo: Seq[EventSeatInfo],
    predictedPriceMap: Map[EventSeatKey, Option[BigDecimal]],
    priceGuardMap: Map[EventSeatKey, Option[BigDecimal]],
    clientIntegrations: Seq[ClientIntegrationComposite]
  ): Seq[Seq[String]] = {
    // TODO: the Seq.fill line will need to change when we refactor the publish phase to return the prices pushed to client integrations especially when secondary pricing rules get implemented
    eventInfo
      .groupBy(event => (event.id, event.section, event.row))
      .values
      .map(group => {
        val ei = group.maxBy(_.listingPrice)
        val oldPrice = ei.listingPrice.getOrElse(BigDecimal(0)) // used for differences
        val (predictedEventPrice, eventPriceGuard) =
          getPredictedAndFlooredPrices(ei, predictedPriceMap, priceGuardMap)
        val absDifference = eventPriceGuard.map(p => (p - oldPrice).toString).getOrElse("")
        val row = Seq(
          ei.date.toString,
          ei.name,
          ei.section,
          ei.row,
          ei.priceScale,
          ei.listingPrice.map(_.toString).getOrElse(""),
          predictedEventPrice.getOrElse("").toString,
          eventPriceGuard.getOrElse("").toString,
          absDifference,
          getPercentageDifference(eventPriceGuard, oldPrice),
        ) ++ Seq.fill(clientIntegrations.length)(eventPriceGuard.toString)
        row
      })
      .toSeq
  }

  private def getPercentageDifference(
    eventPriceGuard: Option[BigDecimal],
    previousPrice: BigDecimal
  ): String = {
    if (previousPrice == BigDecimal(0) || eventPriceGuard.isEmpty) {
      ""
    } else {
      ((eventPriceGuard.get - previousPrice) / previousPrice * 100).toString
    }
  }

  private def getPredictedAndFlooredPrices(
    eventInfo: EventSeatInfo,
    predictedPriceMap: Map[EventSeatKey, Option[BigDecimal]],
    priceGuardMap: Map[EventSeatKey, Option[BigDecimal]]
  ): (Option[BigDecimal], Option[BigDecimal]) = {
    val predictedPrice = predictedPriceMap(
      EventSeatKey(eventInfo.id, eventInfo.section, eventInfo.row)
    )
    val priceGuard = priceGuardMap(EventSeatKey(eventInfo.id, eventInfo.section, eventInfo.row))
    (predictedPrice, priceGuard)
  }

  val defaultHeaders: Seq[String] =
    Seq(
      "Date",
      "Name",
      "Section",
      "Row",
      "Price Scale",
      "Previous Price",
      "Predicted Price",
      "Modified Price",
      "$ Price Difference",
      "% Price Difference"
    )

  private def createHeaders(clientIntegrations: Seq[ClientIntegrationComposite]): Seq[String] = {
    val primaryName = clientIntegrations.filter(_.isPrimary).head.name
    val secondaryNames = clientIntegrations.filter(_.isPrimary == false).map(_.name)
    defaultHeaders ++ Seq(primaryName) ++ secondaryNames
  }
}
