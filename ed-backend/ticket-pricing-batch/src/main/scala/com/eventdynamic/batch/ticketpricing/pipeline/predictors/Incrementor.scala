package com.eventdynamic.batch.ticketpricing.pipeline.predictors

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.models.{EventObservations, ObservationGroup}
import com.eventdynamic.utils.DateHelper

object Incrementor {

  /**
    * Increments multiple events worth of observations by 15 minutes.
    *
    * @param eventObservations
    * @return
    */
  def increment(eventObservations: Seq[EventObservations]): Seq[EventObservations] = {
    // Grab the current timestamp (assumes all are the same timestamp)
    val currentTimestamp = eventObservations.head.timestamp
    val nextTimestamp = DateHelper.later(15, currentTimestamp)

    // Filter events that have started
    val (startedEvents, upcomingEvents) = splitStartedEvents(nextTimestamp, eventObservations)

    val numberStartedEvents = startedEvents.size

    // Increment each event
    upcomingEvents.foreach(
      _.observationGroups.foreach(incrementObservationGroup(_, nextTimestamp, numberStartedEvents))
    )

    upcomingEvents
  }

  /**
    * Partitions events based on if they have started at a timestamp.
    *
    * @param timestamp
    * @param eventObservations
    * @return (startedEvents, upcomingEvents)
    */
  private def splitStartedEvents(
    timestamp: Timestamp,
    eventObservations: Seq[EventObservations]
  ): (Seq[EventObservations], Seq[EventObservations]) = {
    eventObservations.partition(
      _.observationGroups.head.first.eventDate.toInstant.isBefore(timestamp.toInstant)
    )
  }

  /**
    * Business logic application for moving into the next timestamp.
    *
    * @param observationGroup
    * @param timestamp
    * @param numEvents
    * @return
    */
  private def incrementObservationGroup(
    observationGroup: ObservationGroup,
    timestamp: Timestamp,
    numEvents: Int
  ): Unit = {
    observationGroup.features.timestamp = timestamp
    // TODO update win/loss based on numEvents passed
  }
}
