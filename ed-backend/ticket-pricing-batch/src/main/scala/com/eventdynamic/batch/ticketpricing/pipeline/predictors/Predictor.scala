package com.eventdynamic.batch.ticketpricing.pipeline.predictors

import com.eventdynamic.batch.ticketpricing.models.{Observation, PipelinePrice}
import com.eventdynamic.models.{EventSeatListedPriceUpdate, Projection}

abstract class Predictor {

  /**
    * Generate new prices for a set of observations.
    *
    * @param observations
    * @return listing price updates and the new observations
    */
  def predict(observations: Seq[Observation]): Seq[PipelinePrice]
}
