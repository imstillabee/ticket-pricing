package com.eventdynamic.batch.ticketpricing.pipeline.predictors

import com.eventdynamic.batch.ticketpricing.models.{Observation, ObservationGroup, PipelinePrice}
import com.eventdynamic.batch.ticketpricing.pipeline.strategies.pricing.PricingStrategy
import com.eventdynamic.models._
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext
import scala.math.BigDecimal.RoundingMode

class RowPredictor(val pricingStrategy: PricingStrategy)(implicit ec: ExecutionContext)
    extends Predictor {

  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Groups observationGroups into ObservationGroups with the same section/row.
    *
    * @param observations
    * @return
    */
  private def groupObservations(observations: Seq[Observation]): Seq[ObservationGroup] = {
    observations
      .groupBy(o => (o.eventId, o.section, o.row))
      .mapValues(ObservationGroup.fromObservations)
      .values
      .toSeq
  }

  /**
    * Generates new prices for the given observationGroups.
    *
    * @param observations
    * @return (Seq[EventSeatListedPriceUpdate], Seq[PipelinePrice])
    */
  def predict(observations: Seq[Observation]): Seq[PipelinePrice] = {
    // Group observations
    val observationGroups = groupObservations(observations)

    // Update observationGroups with new prices
    updateObservationPricesByGroup(observationGroups)

    // Create update objects from those prices
    observationGroupsToPipelinePrices(observationGroups)
  }

  /**
    * Converts observationGroups to pipeline prices
    *
    * @param observationGroups
    * @return
    */
  private def observationGroupsToPipelinePrices(
    observationGroups: Seq[ObservationGroup]
  ): Seq[PipelinePrice] = {
    observationGroups.flatMap(
      observationGroup =>
        observationGroup.observations.map(
          observation =>
            PipelinePrice(
              observation.eventSeatId,
              observation.eventId,
              observation.seatId,
              observation.integrationEventId,
              observation.integrationSeatId,
              observation.priceScaleId,
              observation.externalPriceScaleId,
              observationGroup.features.listPrice,
              observation.shouldPrice,
              observationGroup.observations.exists(_.overridePrice.isDefined),
              observation.section,
              observation.row,
              !observation.transactionType.contains(TransactionType.Purchase) && !observation.isHeld
          )
      )
    )
  }

  /**
    * Update observation prices on the group if all seats should be priced.  If all seats should be priced, if one
    * seat has an overridden value, then the group is priced at the highest overridden value.
    *
    * @param observationGroups
    * @return
    */
  private def updateObservationPricesByGroup(observationGroups: Seq[ObservationGroup]): Unit = {
    // Get all observation groups that should be priced.
    val toPrice = observationGroups.filter(_.observations.forall(_.shouldPrice))

    // Split into groups that should use the strategy and should be overridden
    val (usingStrategy, usingOverride) =
      toPrice.partition(_.observations.forall(_.overridePrice.isEmpty))

    // Price using strategy
    pricingStrategy.priceObservationGroups(usingStrategy)

    // Round prices up to the nearest whole dollar
    usingStrategy.foreach(
      og => og.features.listPrice = og.features.listPrice.map(_.setScale(0, RoundingMode.CEILING))
    )

    // Price using max overridden price
    usingOverride.foreach(og => og.features.listPrice = og.observations.map(_.overridePrice).max)
  }
}
