package com.eventdynamic.batch.ticketpricing.pipeline.statscomposers

import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.eventdynamic.batch.ticketpricing.models.DateParts
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.models.ObservationBase
import com.eventdynamic.modelserver.models.{
  MLBModelServerClientStats,
  MLBModelServerEventStats,
  ModelServerClientStats,
  ModelServerEventStats
}
import com.eventdynamic.mysportsfeed.models.mlb._
import com.eventdynamic.services.{SeasonService, TeamStatService}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class MLBStatsComposer(val serviceHolder: ServiceHolder, val clientId: Int) extends StatsComposer {
  private val logger = LoggerFactory.getLogger(this.getClass)

  def getClientStats(observationBase: Seq[ObservationBase]): Future[ModelServerClientStats] = {
    for {
      mlbStats <- MLBStatsComposer.getStats(serviceHolder, clientId)
    } yield {
      mlbStats.teamStats match {
        case Some(stats) => {
          logger.info(s"Found MLB Team stats for Client ${clientId}")
          MLBModelServerClientStats(
            wins = stats.wins.toInt,
            losses = stats.losses.toInt,
            rank = Some(stats.rank),
            games_ahead = stats.gamesAhead
          )
        }
        case None => {
          logger.info(s"Missing MLB Team stats for Client ${clientId}")
          // Setting default wins, losses, rank, and games ahead
          MLBModelServerClientStats(0, 0, None, 0)
        }
      }
    }
  }

  def getEventStatsMap(
    observations: Seq[ObservationBase],
    timestamp: Timestamp
  ): Future[Map[Timestamp, ModelServerEventStats]] = {
    for {
      mlbStats <- MLBStatsComposer.getStats(serviceHolder, clientId)
    } yield {
      val mlbGameStatsMap = getGameStatsMap(observations, mlbStats)

      // Group observations based on event date
      observations
        .groupBy(_.eventDate)
        .map(group => {
          val (eventDate, obs) = group

          // Retrieve MLB game stats
          val mlbGameStats = mlbGameStatsMap(eventDate)

          // Map event date to MLB event stats
          eventDate ->
            MLBModelServerEventStats(
              game_num = mlbGameStats.gameNum,
              home_opener = if (mlbGameStats.homeOpener) 1 else 0,
              home_series_start = if (mlbGameStats.homeSeriesStart) 1 else 0,
              opp_losses = 0, // TODO fix with EVNT-288
              opp_rank = None, // TODO fix with EVNT-288
              opp_wins = 0, // TODO fix with EVNT-288
              opponent = mlbGameStats.opponent
            )
        })
    }
  }

  def getGameStatsMap(
    observations: Seq[ObservationBase],
    mlbStats: MLBStats
  ): Map[Timestamp, MLBGameStats] =
    observations
      .map(_.eventDate)
      .distinct
      .map(date => date -> calculateGameStats(date, mlbStats))
      .toMap

  /** Calculates the MLB Game stats like streak and whether it is a home_opener / home_series_start
    *
    * @param eventDate
    * @param mlbStats
    * @return
    */
  private def calculateGameStats(eventDate: Timestamp, mlbStats: MLBStats): MLBGameStats = {
    val dateParts = DateParts.from(eventDate)

    val date =
      LocalDateTime.of(eventDate.toLocalDateTime.getYear, dateParts.month, dateParts.day, 0, 0)
    val formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd")
    val dateString = date.format(formatter)

    serviceHolder.mySportsFeedService.MLB.calculateGameStats(mlbStats, dateString)
  }
}

object MLBStatsComposer {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /** Makes MLB API Calls for stats
    *
    * @return
    */
  def getStats(serviceHolder: ServiceHolder, clientId: Int): Future[MLBStats] = {
    logger.info(s"Building MLBStats for client $clientId")
    for {
      teamAbbreviation <- serviceHolder.teamAbbreviationService
        .getByClientId(clientId)
        .map(_.get.abbreviation)
      gameLogs <- serviceHolder.mySportsFeedService.MLB.getTeamGameLogs(teamAbbreviation)
      teamStats <- getMLBTeamStats(serviceHolder, clientId, teamAbbreviation, gameLogs)
      teamSchedule <- serviceHolder.mySportsFeedService.MLB.getTeamFullSchedule(teamAbbreviation)
    } yield {
      MLBStats(teamAbbreviation, teamStats, gameLogs, teamSchedule)
    }
  }

  /** Get Teams divisional stats and win loss record from MySportsFeedsApi
    *
    * @param teamAbbreviation
    * @return
    */
  def getMLBTeamStats(
    serviceHolder: ServiceHolder,
    clientId: Int,
    teamAbbreviation: String,
    gameLogs: Option[Seq[GameLogs]]
  ): Future[Option[MLBTeamStats]] = {
    serviceHolder.mySportsFeedService.MLB
      .getTeamStandings(teamAbbreviation)
      .flatMap {
        case Some(mlbStats) =>
          val division =
            mlbStats.filter(_.teamStandings.exists(_.team == teamAbbreviation)).head
          val stats = division.teamStandings.filter(_.team == teamAbbreviation).head
          var gamesBack = -stats.gamesBack.toFloat

          // if leading division calculate games ahead
          if (stats.rank == "1") {
            gamesBack = division.teamStandings.apply(1).gamesBack.toFloat
          }

          // Calculate the win streak
          val streak = gameLogs
            .filter(_.nonEmpty)
            .map(games => {
              val completedGames = games.sortBy(_.date)
              val lastOutcome = completedGames.last.won
              val inARow = completedGames.reverse.takeWhile(_.won == lastOutcome).size

              if (lastOutcome == "0")
                -inARow
              else
                inARow
            })
            .getOrElse(0)

          // Update the team statistics and return the team stats
          updateTeamStatistics(
            clientId,
            serviceHolder.seasonService,
            serviceHolder.teamStatService,
            stats
          ).map(
            _ =>
              Some(
                MLBTeamStats(
                  stats.rank.toInt,
                  stats.wins.toInt,
                  stats.losses.toInt,
                  gamesBack,
                  streak
                )
            )
          )
        case _ =>
          logger.error(s"Could not get mlb stats")
          Future.successful(None)
      }
      .recover {
        case e =>
          logger.error(s"Exception getting mlb stats: $e")
          None
      }
  }

  /** Update Team Statistics in ED from MySportsFeedApi Data
    *
    * @param teamStatService
    * @param teamStandings
    * @param numGames the number of games in the season (away games included)
    * @return
    */
  def updateTeamStatistics(
    clientId: Int,
    seasonService: SeasonService,
    teamStatService: TeamStatService,
    teamStandings: TeamStandings,
    numGames: Int = 162 // TODO calculate this from something
  ): Future[Any] = {
    seasonService
      .getCurrent(Some(clientId), false)
      .flatMap {
        case Some(season) =>
          teamStatService.getByClientIdAndSeasonId(clientId, season.id.get).flatMap {
            case Some(teamStat) =>
              logger.info(
                s"Found team stats for Client $clientId and Current Season ${season.id.get}, updating with new statistics"
              )
              teamStatService.update(
                teamStat
                  .copy(wins = teamStandings.wins.toInt, losses = teamStandings.losses.toInt)
              )
            case None =>
              logger.info(
                s"No team stats for Client $clientId and Current Season ${season.id.get}, creating new team stats."
              )
              teamStatService.create(
                clientId,
                season.id.get,
                teamStandings.wins.toInt,
                teamStandings.losses.toInt,
                numGames
              )
          }
        case None =>
          logger.error("Couldn't get current season to save team stats.")
          Future.successful()
      }
      .recover {
        case e =>
          logger.error(s"Exception getting team statistics: $e")
      }
  }
}
