package com.eventdynamic.batch.ticketpricing.pipeline.statscomposers

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.models.ObservationBase
import com.eventdynamic.modelserver.models.{
  MLSModelServerClientStats,
  MLSModelServerEventStats,
  ModelServerClientStats,
  ModelServerEventStats
}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class MLSStatsComposer(val serviceHolder: ServiceHolder, val clientId: Int) extends StatsComposer {
  private val logger = LoggerFactory.getLogger(this.getClass)

  def getClientStats(observationBase: Seq[ObservationBase]): Future[ModelServerClientStats] = {
    // Find the corresponding season for a given event assuming all observations are for one season
    val eventId = observationBase.head.eventId
    for {
      seasonId <- serviceHolder.eventService.getById(eventId).map {
        case Some(event) => {
          logger.info(s"Found Event $eventId")
          event.seasonId
        }
        case None => {
          val msg = s"Event $eventId does not exist"
          logger.error(msg)
          throw new Exception(msg)
        }
      }
      mlsSeasonStat <- serviceHolder.mlsSeasonStatService.getBySeasonId(seasonId.get).map {
        case Some(seasonStat) => {
          logger.info(s"Found MLS Season Stats ${seasonStat.id} for Season ${seasonId.get}")
          seasonStat
        }
        case None => {
          val msg = s"Season ${seasonId.get} does not have MLS Season stats"
          logger.error(msg)
          throw new Exception(msg)
        }
      }
    } yield {
      // Return the season stats
      MLSModelServerClientStats(
        wins = mlsSeasonStat.wins,
        losses = mlsSeasonStat.losses,
        ties = mlsSeasonStat.ties
      )
    }
  }

  def getEventStatsMap(
    observations: Seq[ObservationBase],
    timestamp: Timestamp
  ): Future[Map[Timestamp, ModelServerEventStats]] = {
    // Group observations based on event
    val eventStatsSeq = observations
      .groupBy(_.eventId)
      .map(group => {
        val (eventId, obs) = group

        // Create a model server example for each event and map it from the event date
        for {
          eventStats <- serviceHolder.mlsEventStatService
            .getByEventId(eventId)
        } yield {
          eventStats match {
            case Some(stats) => {
              logger.info(s"Found MLS Event Stats ${stats.id} for Event $eventId")
              obs.head.eventDate -> MLSModelServerEventStats(
                opponent = stats.opponent,
                home_game_number = stats.homeGameNumber
              )
            }
            case _ => {
              val msg = s"Event $eventId does not have MLS Event Stats"
              logger.error(msg)
              throw new Exception(msg)
            }
          }
        }
      })
      .toSeq

    for {
      eventStatsFuture <- Future.sequence(eventStatsSeq)
    } yield {
      eventStatsFuture.toMap
    }
  }
}
