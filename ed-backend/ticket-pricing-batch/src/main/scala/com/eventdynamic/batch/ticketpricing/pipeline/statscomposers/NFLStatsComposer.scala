package com.eventdynamic.batch.ticketpricing.pipeline.statscomposers

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.models.ObservationBase
import com.eventdynamic.modelserver.models.{
  ModelServerClientStats,
  ModelServerEventStats,
  NFLModelServerClientStats,
  NFLModelServerEventStats
}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class NFLStatsComposer(val serviceHolder: ServiceHolder, val clientId: Int) extends StatsComposer {
  private val logger = LoggerFactory.getLogger(this.getClass)

  def getClientStats(observationBase: Seq[ObservationBase]): Future[ModelServerClientStats] = {
    // Find the corresponding season for a given event assuming all observations are for one season
    val eventId = observationBase.head.eventId
    for {
      seasonId <- serviceHolder.eventService.getById(eventId).map {
        case Some(event) => {
          logger.info(s"Found Event $eventId")
          event.seasonId
        }
        case None => {
          val msg = s"Event $eventId does not exist"
          logger.error(msg)
          throw new Exception(msg)
        }
      }
      nflSeasonStat <- serviceHolder.nflSeasonStatService.getBySeasonId(seasonId.get).map {
        case Some(seasonStat) => {
          logger.info(s"Found NFL Season Stats ${seasonStat.id} for Season ${seasonId.get}")
          seasonStat
        }
        case None => {
          val msg = s"Season ${seasonId.get} does not have NFL Season stats"
          logger.error(msg)
          throw new Exception(msg)
        }
      }
    } yield {
      // Return the season stats
      NFLModelServerClientStats(
        wins = nflSeasonStat.wins,
        losses = nflSeasonStat.losses,
        ties = nflSeasonStat.ties
      )
    }
  }

  def getEventStatsMap(
    observations: Seq[ObservationBase],
    timestamp: Timestamp
  ): Future[Map[Timestamp, ModelServerEventStats]] = {
    // Group observations based on event
    val eventStatsSeq = observations
      .groupBy(_.eventId)
      .map(group => {
        val (eventId, obs) = group

        // Create a model server example for each event and map it from the event date
        for {
          eventStats <- serviceHolder.nflEventStatService
            .getByEventId(eventId)
        } yield {
          eventStats match {
            case Some(stats) => {
              logger.info(s"Found NFL Event Stats ${stats.id} for Event $eventId")
              obs.head.eventDate -> NFLModelServerEventStats(
                opponent = stats.opponent,
                is_preseason =
                  if (stats.isPreSeason) 1
                  else 0
              )
            }
            case _ => {
              val msg = s"Event $eventId does not have NFL Event Stats"
              logger.error(msg)
              throw new Exception(msg)
            }
          }
        }
      })
      .toSeq

    for {
      eventStatsFuture <- Future.sequence(eventStatsSeq)
    } yield {
      eventStatsFuture.toMap
    }
  }
}
