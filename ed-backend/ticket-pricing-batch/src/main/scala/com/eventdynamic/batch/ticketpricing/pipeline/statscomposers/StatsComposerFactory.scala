package com.eventdynamic.batch.ticketpricing.pipeline.statscomposers

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.models.{ClientPerformanceType, ObservationBase}
import com.eventdynamic.modelserver.models.{ModelServerClientStats, ModelServerEventStats}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait StatsComposer {

  /***
    * Dynamically build client-specific stats
    *
    * @param observationBase
    * @return
    */
  def getClientStats(observationBase: Seq[ObservationBase]): Future[ModelServerClientStats]

  /***
    * Dynamically build a map of event times to event-specific stats
    * These stats are generated depending on the performance type of the client
    *
    * @param observations
    * @param timestamp
    * @return
    */
  def getEventStatsMap(
    observations: Seq[ObservationBase],
    timestamp: Timestamp
  ): Future[Map[Timestamp, ModelServerEventStats]]
}

object StatsComposerFactory {

  def getStatsComposerByClientId(
    clientId: Int,
    serviceHolder: ServiceHolder
  ): Future[StatsComposer] = {
    // Generate a StatsComposer based on the performance type of the client
    serviceHolder.clientService.getById(clientId).map {
      case Some(client) if client.performanceType == ClientPerformanceType.MLB =>
        new MLBStatsComposer(serviceHolder, clientId)

      case Some(client) if client.performanceType == ClientPerformanceType.NFL =>
        new NFLStatsComposer(serviceHolder, clientId)

      case Some(client) if client.performanceType == ClientPerformanceType.MLS =>
        new MLSStatsComposer(serviceHolder, clientId)

      case Some(client) if client.performanceType == ClientPerformanceType.NCAAF =>
        new NCAAFStatsComposer(serviceHolder, clientId)

      case Some(client) if client.performanceType == ClientPerformanceType.NCAAB =>
        new NCAABStatsComposer(serviceHolder, clientId)

      case Some(client) => throw new Exception(s"Client ${client.performanceType} is not supported")
      case None         => throw new Exception(s"Client $clientId does not exist")
    }
  }
}
