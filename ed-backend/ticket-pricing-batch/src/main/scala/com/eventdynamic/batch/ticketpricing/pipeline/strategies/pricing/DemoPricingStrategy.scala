package com.eventdynamic.batch.ticketpricing.pipeline.strategies.pricing

import com.eventdynamic.batch.ticketpricing.models.ObservationGroup

import scala.util.{Failure, Success, Try}

class DemoPricingStrategy(val setBaseline: Boolean) extends PricingStrategy {

  /**
    * Price all observation groups using a strategy only for demos.  First you set a baseline where every first row
    * seat is $50, then decrease price by shrinking factor as you go back in rows.  Next, rerun set prices with the
    * price increase strategy (pass false to setBaseline).
    *
    * @param observationGroups
    * @return observation group with new listed prices
    */
  override def priceObservationGroups(observationGroups: Seq[ObservationGroup]): Unit = {
    if (setBaseline) setBaselinePrices(observationGroups) else increasePrices(observationGroups)
  }

  /**
    * Increases prices by 10% across the board.
    *
    * @param observationGroups
    */
  def increasePrices(observationGroups: Seq[ObservationGroup]): Unit = {
    observationGroups.foreach(og => {
      og.features.listPrice = og.features.listPrice
        .map(_ * 1.1)
        .map(_.setScale(2, BigDecimal.RoundingMode.HALF_UP))
    })
  }

  /**
    * Sets prices to a baseline.  Row 1 (or null) = $50, decreasing the further back the row.
    *
    * Formula from experimentation = (row - 1) ^^ .25 * 8.5
    *
    * @param observationGroups
    */
  def setBaselinePrices(observationGroups: Seq[ObservationGroup]): Unit = {
    observationGroups.foreach(observationGroup => {
      val uniqueRows = observationGroup.observations.map(_.row).distinct
      if (uniqueRows.length != 1) {
        observationGroup.features.listPrice = Some(50)
      } else {
        observationGroup.features.listPrice = Try(uniqueRows.head.toInt) match {
          case Failure(_) => Some(75)
          case Success(num) =>
            Some(
              BigDecimal(100 - Math.pow(Math.max(1, Math.min(num, 50)) - 1, .25) * 8.5)
                .setScale(2, BigDecimal.RoundingMode.HALF_UP)
            )
        }
      }
    })
  }
}
