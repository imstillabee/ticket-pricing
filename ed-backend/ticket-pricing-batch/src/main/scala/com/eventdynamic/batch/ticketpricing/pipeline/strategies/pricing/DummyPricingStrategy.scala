package com.eventdynamic.batch.ticketpricing.pipeline.strategies.pricing
import com.eventdynamic.batch.ticketpricing.models.ObservationGroup

class DummyPricingStrategy(val basePrice: BigDecimal, val range: BigDecimal)
    extends PricingStrategy {

  /**
    * Price a single observation group with a randomly generated value.
    *
    * @param observationGroups
    * @return observation group with new listed prices
    */
  override def priceObservationGroups(observationGroups: Seq[ObservationGroup]): Unit = {
    observationGroups.foreach(observationGroup => {
      observationGroup.first.row
      val newListPrice = (basePrice + range * (Math.random() * 2 - 1))
        .setScale(2, BigDecimal.RoundingMode.HALF_UP)

      observationGroup.features.listPrice = Some(newListPrice)
    })
  }
}
