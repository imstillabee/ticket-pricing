package com.eventdynamic.batch.ticketpricing.pipeline.strategies.pricing

import com.eventdynamic.batch.ticketpricing.models.ObservationGroup
import com.eventdynamic.batch.ticketpricing.pipeline.evaluators.Evaluator

import scala.concurrent.Await
import scala.concurrent.duration._

class EvaluatorPricingStrategy(evaluator: Evaluator) extends PricingStrategy {
  /**
    * Calls the Evaluator to set prices on the observation groups.  The evaluator current updates the prices in place
    * so nothing more than calling the function is necessary.
    *
    * @param observationGroups
    * @return observation groups with new listed prices
    */
  override def priceObservationGroups(observationGroups: Seq[ObservationGroup]): Unit = {
    Await.result(evaluator.evaluate(observationGroups), 120.seconds)
  }
}
