package com.eventdynamic.batch.ticketpricing.pipeline.strategies.pricing

import com.eventdynamic.batch.ticketpricing.models.ObservationGroup

abstract class PricingStrategy {

  /**
    * Price a sequence of observation groups.
    *
    * @param observationGroups
    * @return observation groups with new listed prices
    */
  def priceObservationGroups(observationGroups: Seq[ObservationGroup]): Unit
}
