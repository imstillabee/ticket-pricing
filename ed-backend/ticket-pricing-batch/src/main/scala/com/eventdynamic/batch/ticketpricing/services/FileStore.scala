package com.eventdynamic.batch.ticketpricing.services

import java.io.File

import awscala.s3.{PutObjectResult, S3}
import com.eventdynamic.batch.ticketpricing.configs.S3Config

case class FileStore(s3Config: S3Config) {
  implicit val s3 = S3.at(s3Config.region)

  private val bucket = s3.bucket(s3Config.bucket)
  if (bucket.isEmpty) {
    throw new Error(s"Bucket '${s3Config.bucket}' does not exist")
  }

  def upload(key: String, file: File): PutObjectResult = {
    bucket.get.put(key, file)
  }
}
