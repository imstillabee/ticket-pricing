package com.eventdynamic.batch.ticketpricing.services

import com.eventdynamic.batch.ticketpricing.services.weather.OpenWeatherForecastService
import com.eventdynamic.db.EDContext
import com.eventdynamic.mysportsfeed.MySportsFeedService
import com.eventdynamic.services._
import com.eventdynamic.services.mlb.MySportsFeedInfoService
import com.eventdynamic.transactions.{IntegrationTransactions, SeasonTransaction}
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.ExecutionContext.Implicits.global

case class ServiceHolder(
  clientIntegrationService: ClientIntegrationService,
  clientService: ClientService,
  seasonService: SeasonService,
  eventService: EventService,
  seatService: SeatService,
  eventSeatService: EventSeatService,
  observationService: ObservationService,
  weatherAverageService: WeatherAverageService,
  openWeatherForecastService: OpenWeatherForecastService,
  integrationService: IntegrationService,
  revenueCategoryService: RevenueCategoryService,
  revenueCategoryMappingService: RevenueCategoryMappingService,
  mySportsFeedService: MySportsFeedService,
  teamAbbreviationService: MySportsFeedInfoService,
  teamStatService: TeamStatService,
  transactionService: TransactionService,
  integrationTransactions: IntegrationTransactions,
  jobConfigService: JobConfigService,
  mlbamMappingService: MLBAMMappingService,
  priceGuardService: PriceGuardService,
  eventCategoryService: EventCategoryService,
  proVenuePricingRuleService: ProVenuePricingRuleService,
  priceScaleService: PriceScaleService,
  secondaryPricingRulesService: SecondaryPricingRulesService,
  nflSeasonStatService: NFLSeasonStatService,
  nflEventStatService: NFLEventStatService,
  mlsSeasonStatService: MLSSeasonStatService,
  mlsEventStatService: MLSEventStatService,
  ncaafSeasonStatService: NCAAFSeasonStatService,
  ncaafEventStatService: NCAAFEventStatService,
  ncaabSeasonStatService: NCAABSeasonStatService,
  ncaabEventStatService: NCAABEventStatService
)

object ServiceHolder {

  def apply(ed: EDContext, conf: Config = ConfigFactory.load()): ServiceHolder = {
    val eventService = new EventService(ed)
    val clientService = new ClientService(ed)
    val seatService = new SeatService(ed)
    val eventSeatService = new EventSeatService(ed)
    val observationService = new ObservationService(ed)
    val weatherAverageService = new WeatherAverageService(ed)
    val openWeatherForecastService = OpenWeatherForecastService.fromConfig(conf)
    val integrationService = new IntegrationService(ed)
    val clientIntegrationService = new ClientIntegrationService(ed)
    val revenueCategoryService = new RevenueCategoryService(ed)
    val revenueCategoryMappingService =
      new RevenueCategoryMappingService(ed)
    val transactionService = new TransactionService(ed)
    val seasonTransaction =
      new SeasonTransaction(ed, eventService, eventSeatService, transactionService)
    val seasonService = new SeasonService(ed, seasonTransaction)
    val mlbStatsService = MySportsFeedService.fromConfig(conf)
    val teamAbbreviationService = new MySportsFeedInfoService(ed)
    val teamStatService = new TeamStatService(ed)
    val clientIntegrationEventsService = new ClientIntegrationEventsService(ed)
    val clientIntegrationEventSeatsService = new ClientIntegrationEventSeatsService(ed)
    val clientIntegrationListingsService = new ClientIntegrationListingsService(ed)
    val mlbamMappingService = new MLBAMMappingService(ed)
    val priceGuardService = new PriceGuardService(ed)
    val eventCategoryService = new EventCategoryService(ed)
    val proVenuePricingRuleService = new ProVenuePricingRuleService(ed)
    val priceScaleService = new PriceScaleService(ed)
    val secondaryPricingRulesService = new SecondaryPricingRulesService(ed)
    val nflSeasonStatService = new NFLSeasonStatService(ed)
    val nflEventStatService = new NFLEventStatService(ed)
    val mlsSeasonStatService = new MLSSeasonStatService(ed)
    val mlsEventStatService = new MLSEventStatService(ed)
    val ncaafSeasonStatService = new NCAAFSeasonStatService(ed)
    val ncaafEventStatService = new NCAAFEventStatService(ed)
    val ncaabSeasonStatService = new NCAABSeasonStatService(ed)
    val ncaabEventStatService = new NCAABEventStatService(ed)

    val integrationTransactions =
      new IntegrationTransactions(
        ed,
        clientIntegrationService,
        clientIntegrationEventsService,
        clientIntegrationEventSeatsService,
        clientIntegrationListingsService,
        eventService,
        eventSeatService,
        integrationService,
        seatService
      )
    val jobConfigService = new JobConfigService(ed)

    ServiceHolder(
      clientIntegrationService,
      clientService,
      seasonService,
      eventService,
      seatService,
      eventSeatService,
      observationService,
      weatherAverageService,
      openWeatherForecastService,
      integrationService,
      revenueCategoryService,
      revenueCategoryMappingService,
      mlbStatsService,
      teamAbbreviationService,
      teamStatService,
      transactionService,
      integrationTransactions,
      jobConfigService,
      mlbamMappingService,
      priceGuardService,
      eventCategoryService,
      proVenuePricingRuleService,
      priceScaleService,
      secondaryPricingRulesService,
      nflSeasonStatService,
      nflEventStatService,
      mlsSeasonStatService,
      mlsEventStatService,
      ncaafSeasonStatService,
      ncaafEventStatService,
      ncaabSeasonStatService,
      ncaabEventStatService
    )
  }
}
