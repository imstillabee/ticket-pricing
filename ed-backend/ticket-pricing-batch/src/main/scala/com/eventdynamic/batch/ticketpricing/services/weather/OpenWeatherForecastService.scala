package com.eventdynamic.batch.ticketpricing.services.weather

import com.eventdynamic.batch.ticketpricing.models.weather.OpenWeatherForecast
import com.softwaremill.sttp._
import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class OpenWeatherForecastService(
  private val appId: String,
  private val baseUrl: String,
  val sttpBackend: SttpBackend[Future, Nothing]
) {

  implicit val backend: SttpBackend[Future, Nothing] = sttpBackend

  def getForecast(zip: String): Future[Option[Seq[OpenWeatherForecast]]] = {
    val params = Map("appid" -> appId, "zip" -> zip, "units" -> "imperial")

    sttp
      .get(uri"$baseUrl/forecast?$params")
      .send()
      .map {
        case res if !res.isSuccess =>
          throw new Exception(s"Error getting open weather forecast: ${res.body}")
        case res if res.body.isLeft =>
          throw new Exception(
            s"Error parsing open weather forecast response body: ${res.body.left}"
          )
        case res => OpenWeatherForecast.fromResponse(res.body.right.get)
      }
  }
}

object OpenWeatherForecastService {
  val defaultBackend = OkHttpFutureBackend()

  def fromConfig(
    conf: Config = ConfigFactory.load(),
    backend: SttpBackend[Future, Nothing] = defaultBackend
  ): OpenWeatherForecastService = {
    val appId = conf.getString("openWeatherMap.appId")
    val baseUrl = conf.getString("openWeatherMap.baseUrl")

    new OpenWeatherForecastService(appId, baseUrl, backend)
  }
}
