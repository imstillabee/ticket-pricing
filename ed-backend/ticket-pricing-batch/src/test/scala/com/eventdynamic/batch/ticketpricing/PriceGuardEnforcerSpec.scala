package com.eventdynamic.batch.ticketpricing

import com.eventdynamic.batch.ticketpricing.models.PipelinePrice
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.models._
import com.eventdynamic.services.{EventService, PriceGuardService, PriceScaleService}
import org.scalatest._
import org.specs2.mock.Mockito
import org.mockito.Mockito._

import scala.concurrent.Future
import com.eventdynamic.utils.DateHelper.now

class PriceGuardEnforcerSpec extends AsyncWordSpec with Mockito {

  private def createEvent(eventId: Int = 1, eventCategoryId: Int = 1): Event = {
    Event(
      id = Option(eventId),
      primaryEventId = Option(0),
      createdAt = now(),
      modifiedAt = now(),
      name = "Event 1",
      timestamp = Option(now()),
      clientId = 1,
      venueId = 1,
      eventCategoryId = eventCategoryId,
      seasonId = Option(1),
      isBroadcast = true,
      percentPriceModifier = 0,
      eventScore = None,
      eventScoreModifier = 0,
      spring = None,
      springModifier = 0
    )
  }

  private def createPriceScale(
    priceScaleId: Int = 1,
    name: String = "First Data Platinum"
  ): PriceScale = {
    PriceScale(
      id = Option(priceScaleId),
      name = name,
      venueId = 1,
      integrationId = 1,
      createdAt = null,
      modifiedAt = null
    )
  }

  private def createPriceGuard(
    priceGuardId: Int = 1,
    eventId: Int = 1,
    row: String = "1",
    section: String = "AAA",
    priceScaleId: Option[Int] = None,
    minimumPrice: Int = 135,
    maximumPrice: Option[Int] = Option(200)
  ): PriceGuard = {
    PriceGuard(
      id = Option(priceGuardId),
      eventId = eventId,
      section = section,
      row = row,
      minimumPrice = minimumPrice,
      maximumPrice = maximumPrice,
      createdAt = null,
      modifiedAt = null
    )
  }

  private def createPipelinePrice(
    eventId: Int = 1,
    row: String = "1",
    section: String = "AAA",
    priceScaleId: Int = 1,
    price: Option[BigDecimal] = Some(BigDecimal(100)),
    overridden: Boolean = false,
    available: Boolean = true
  ): PipelinePrice = {
    PipelinePrice(
      eventSeatId = 0,
      eventId = eventId,
      seatId = 1,
      integrationEventId = 1,
      integrationSeatId = 1,
      priceScaleId = priceScaleId,
      externalPriceScaleId = 1,
      price = price,
      shouldPrice = true,
      overridden = overridden,
      section = section,
      row = row,
      available = available
    )
  }

  private def createScenario(): PriceGuardEnforcer = {
    val serviceHolder = mock[ServiceHolder]
    val priceGuardService = mock[PriceGuardService]
    val priceScaleService = mock[PriceScaleService]
    val eventService = mock[EventService]

    when(serviceHolder.priceGuardService).thenReturn(priceGuardService)
    when(serviceHolder.priceScaleService).thenReturn(priceScaleService)
    when(serviceHolder.eventService).thenReturn(eventService)

    val priceGuards = Seq(
      createPriceGuard(minimumPrice = 135),
      createPriceGuard(
        minimumPrice = 134,
        maximumPrice = Some(199),
        section = "",
        row = "",
        priceScaleId = Some(1)
      )
    )

    when(priceGuardService.getAllForEvents(any[Set[Int]]))
      .thenReturn(Future.successful(priceGuards))

    when(priceScaleService.getAllForClient(any[Int]))
      .thenReturn(Future.successful(Seq(createPriceScale())))

    when(eventService.getByIdBulk(any[Set[Int]]))
      .thenReturn(Future.successful(Seq(createEvent())))

    new PriceGuardEnforcer(serviceHolder, 0)
  }

  private def createNoGuardScenario(globalPriceFloor: BigDecimal = 0): PriceGuardEnforcer = {
    val serviceHolder = mock[ServiceHolder]
    val priceGuardService = mock[PriceGuardService]
    val priceScaleService = mock[PriceScaleService]
    val eventService = mock[EventService]

    when(serviceHolder.priceGuardService).thenReturn(priceGuardService)
    when(serviceHolder.priceScaleService).thenReturn(priceScaleService)
    when(serviceHolder.eventService).thenReturn(eventService)

    val priceGuards = Seq()

    when(priceGuardService.getAllForEvents(any[Set[Int]]))
      .thenReturn(Future.successful(priceGuards))

    when(eventService.getByIdBulk(any[Set[Int]]))
      .thenReturn(Future.successful(Seq(createEvent())))

    new PriceGuardEnforcer(serviceHolder, globalPriceFloor)
  }

  "PriceGuardEnforcer" should {
    "update the price of a pipeline price if it's less than the minimum price" in {
      val pipelinePrices =
        Seq(
          createPipelinePrice(price = Some(100)),
          createPipelinePrice(row = "12-abc", price = Some(100))
        )
      val enforcer = createScenario()

      for {
        p2 <- enforcer.enforce(pipelinePrices)
      } yield {
        assert(p2.head.price.get == 135)
        assert(p2(1).price.get == 100)
      }
    }

    "update the price of a pipeline price if it's larger than the maximum price" in {
      val pipelinePrices =
        Seq(
          createPipelinePrice(price = Some(300)),
          createPipelinePrice(row = "12-abc", price = Some(300))
        )
      val enforcer = createScenario()

      for {
        p2 <- enforcer.enforce(pipelinePrices)
      } yield {
        assert(p2.head.price.get == 200)
        assert(p2(1).price.get == 300)
      }
    }

    "not update a price of a pipeline price if a manual value (overridden) was given (minimum price)" in {
      val predictedPipelinePrices = Seq(
        createPipelinePrice(price = Some(100), overridden = true),
        createPipelinePrice(price = Some(100), overridden = false)
      )

      val enforcer = createScenario()

      for {
        pipelinePrices <- enforcer.enforce(predictedPipelinePrices)
      } yield {
        assert(pipelinePrices.head.price.get == 100)
        assert(pipelinePrices.last.price.get == 135)
      }
    }

    "not update a price of a pipeline price if a manual value (overridden) was given (maximum price)" in {
      val predictedPipelinePrices = Seq(
        createPipelinePrice(price = Some(300), overridden = true),
        createPipelinePrice(price = Some(300), overridden = false)
      )

      val enforcer = createScenario()

      for {
        pipelinePrices <- enforcer.enforce(predictedPipelinePrices)
      } yield {
        assert(pipelinePrices.head.price.get == 300)
        assert(pipelinePrices.last.price.get == 200)
      }
    }

    "respect a pipeline price if its larger than the minimum price, but less than the maximum price" in {
      val pipelinePrices = Seq(createPipelinePrice(price = Some(BigDecimal(150))))
      val enforcer = createScenario()

      for {
        p2 <- enforcer.enforce(pipelinePrices)
      } yield {
        assert(p2.head.price.get == 150)
      }
    }

    "respect a pipeline price if its equal to the minimum price" in {
      val pipelinePrices = Seq(createPipelinePrice(price = Some(BigDecimal(135))))
      val enforcer = createScenario()

      for {
        p2 <- enforcer.enforce(pipelinePrices)
      } yield {
        assert(p2.head.price.get == 135)
      }
    }

    "respect a pipeline price if its equal to the maximum price" in {
      val pipelinePrices = Seq(createPipelinePrice(price = Some(BigDecimal(200))))
      val enforcer = createScenario()

      for {
        p2 <- enforcer.enforce(pipelinePrices)
      } yield {
        assert(p2.head.price.get == 200)
      }
    }

    "update the price of a pipeline price if it's less than the global price floor" in {
      val pipelinePrices = Seq(createPipelinePrice(price = Some(BigDecimal(1))))
      val enforcer = createNoGuardScenario(BigDecimal(10))

      for {
        p2 <- enforcer.enforce(pipelinePrices)
      } yield {
        assert(p2.head.price.get == BigDecimal(10))
      }
    }

    "not update the price of a pipeline price if it's greater than the global price floor" in {
      val pipelinePrices = Seq(createPipelinePrice(price = Some(BigDecimal(12))))
      val enforcer = createNoGuardScenario(BigDecimal(10))

      for {
        p2 <- enforcer.enforce(pipelinePrices)
      } yield {
        assert(p2.head.price.get == 12)
      }
    }

    "not update the price of a pipeline price if the price is empty" in {
      val pipelinePrices = Seq(createPipelinePrice(price = None))
      val enforcer = createScenario()

      for {
        p2 <- enforcer.enforce(pipelinePrices)
      } yield {
        assert(p2.head.price.isEmpty)
      }
    }
  }
}
