package com.eventdynamic.batch.ticketpricing

import com.eventdynamic.batch.ticketpricing.models.PipelinePriceByScale
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.mlbam.models.BuyerTypePrice
import com.eventdynamic.models.RoundingType.RoundingType
import com.eventdynamic.models.{ProVenuePricingRule, RoundingType}
import com.eventdynamic.provenue.models._
import com.eventdynamic.services.ProVenuePricingRuleService.CompoundProVenuePricingRule
import com.eventdynamic.services.ProVenuePricingRuleService
import com.eventdynamic.utils.DateHelper
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.{AsyncWordSpec, Matchers, PrivateMethodTester}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future
import scala.util.Random

class PricePointActionGeneratorSpec
    extends AsyncWordSpec
    with Matchers
    with MockitoSugar
    with PrivateMethodTester {
  val t = new TestHelper

  object Rand {

    def pricingRule(
      mirrorPriceScaleId: Option[Int] = None,
      percent: Option[Int] = None,
      constant: Option[BigDecimal] = None,
      round: RoundingType = RoundingType.Ceil
    ): ProVenuePricingRule = {
      ProVenuePricingRule(
        Some(Random.nextInt()),
        DateHelper.now(),
        DateHelper.now(),
        Random.nextInt(),
        None,
        true,
        mirrorPriceScaleId,
        percent,
        constant,
        round
      )
    }

    def pipelinePriceByScale(
      eventId: Int = Random.nextInt(),
      integrationEventId: Int = Random.nextInt(),
      priceScale: Int = Random.nextInt(),
      externalPriceScaleId: Int = Random.nextInt(),
      price: Option[BigDecimal] = Some(Random.nextInt())
    ): PipelinePriceByScale = {
      PipelinePriceByScale(eventId, integrationEventId, priceScale, externalPriceScaleId, price)
    }
  }

  object InstanceFactory {
    private def defaultAdjuster = {
      val m = mock[ProVenuePricingRuleService]
      when(m.getAll(any[Int], any[Boolean])) thenReturn Future.successful(Seq())
      m
    }

    def get(adjuster: ProVenuePricingRuleService = defaultAdjuster): PricePointActionGenerator = {
      new PricePointActionGenerator(adjuster)
    }
  }

  "PricePointActionGenerator#generatePricePointActions" should {
    val buyerTypeRefs = Seq(
      ProVenueBuyerType(10, "Adult", true, "Adult"),
      ProVenueBuyerType(11, "Child", true, "Child"),
      ProVenueBuyerType(12, "Veteran", true, "Veteran"),
      ProVenueBuyerType(13, "Inactive", false, "Inactive")
    )
    val priceScaleRefs = Seq(
      ProVenuePriceScale(20, "Audi gold"),
      ProVenuePriceScale(21, "Audi silver"),
      ProVenuePriceScale(22, "Home dugout"),
      ProVenuePriceScale(23, "Outfield")
    )

    "not assign generated prices to price point actions that match the price structure without matching buyer type" in {
      val actionGenerator = InstanceFactory.get()

      val generatedPriceMap = Map(
        PipelinePriceByScale(1, 1, 2, 20, Some(12.99)) -> Seq(
          BuyerTypePrice("1", Some(10)),
          BuyerTypePrice("2", Some(11))
        ),
        PipelinePriceByScale(1, 1, 3, 21, Some(12.99)) -> Seq(
          BuyerTypePrice("1", Some(10)),
          BuyerTypePrice("2", Some(11))
        ),
        PipelinePriceByScale(1, 1, 4, 50, Some(14.99)) -> Seq(
          BuyerTypePrice("1", Some(10)),
          BuyerTypePrice("2", Some(11))
        ),
        PipelinePriceByScale(1, 1, 4, 23, None) -> Seq(
          BuyerTypePrice("1", Some(10)),
          BuyerTypePrice("2", Some(11))
        )
      )

      val priceStructure = ProVenuePriceStructure(
        1,
        "code",
        Seq(
          ProVenuePricePoint(1, ProVenueMoney("USD", 1.99), 0, 0),
          ProVenuePricePoint(2, ProVenueMoney("USD", 1.99), 1, 0),
          ProVenuePricePoint(3, ProVenueMoney("USD", 1.99), 1, 1)
        ),
        buyerTypeRefs,
        priceScaleRefs
      )

      val result =
        actionGenerator.generatePricePointActions(1, 1, priceStructure, generatedPriceMap)
      assert(result.length === 0)
    }

    "assign generated prices to price point actions that match the price structure with matching buyer type" in {
      val actionGenerator = InstanceFactory.get()

      val generatedPriceMap = Map(
        PipelinePriceByScale(1, 1, 2, 20, Some(12.99)) -> Seq(
          BuyerTypePrice("10", Some(10)),
          BuyerTypePrice("11", Some(11))
        ),
        PipelinePriceByScale(1, 1, 3, 21, Some(12.99)) -> Seq(
          BuyerTypePrice("10", Some(10)),
          BuyerTypePrice("11", Some(10))
        ),
        PipelinePriceByScale(1, 1, 4, 50, Some(14.99)) -> Seq(
          BuyerTypePrice("10", Some(10)),
          BuyerTypePrice("11", Some(11))
        ),
        PipelinePriceByScale(1, 1, 4, 23, None) -> Seq(
          BuyerTypePrice("10", Some(10)),
          BuyerTypePrice("11", Some(11))
        )
      )

      val priceStructure = ProVenuePriceStructure(
        1,
        "code",
        Seq(
          ProVenuePricePoint(1, ProVenueMoney("USD", 1.99), 0, 0),
          ProVenuePricePoint(2, ProVenueMoney("USD", 1.99), 1, 0),
          ProVenuePricePoint(3, ProVenueMoney("USD", 1.99), 1, 1)
        ),
        buyerTypeRefs,
        priceScaleRefs
      )

      val result =
        actionGenerator.generatePricePointActions(1, 1, priceStructure, generatedPriceMap)
      assert(result.length === 3)
      all(result.map(_.price.value)) shouldBe 10
    }

    "apply adjustment rules" in {
      val adjuster = mock[ProVenuePricingRuleService]
      val rule = CompoundProVenuePricingRule(
        ProVenuePricingRule(
          Some(1),
          DateHelper.now(),
          DateHelper.now(),
          1,
          None,
          true,
          None,
          None,
          Some(10),
          RoundingType.None
        ),
        Seq("10", "11"),
        Seq(2, 3),
        Seq(1)
      )

      when(adjuster.getAll(any[Int], any[Boolean])) thenReturn Future.successful(Seq(rule))

      val actionGenerator = InstanceFactory.get(adjuster = adjuster)

      val generatedPriceMap =
        Map(
          PipelinePriceByScale(1, 1, 2, 20, Some(12.99)) -> Seq(
            BuyerTypePrice("10", Some(22.99)),
            BuyerTypePrice("11", Some(22.99))
          ),
          PipelinePriceByScale(1, 1, 3, 21, Some(12.99)) -> Seq(
            BuyerTypePrice("10", Some(22.99)),
            BuyerTypePrice("11", Some(22.99))
          )
        )

      val priceStructure = ProVenuePriceStructure(
        1,
        "code",
        Seq(
          ProVenuePricePoint(1, ProVenueMoney("USD", 1.99), 0, 0),
          ProVenuePricePoint(2, ProVenueMoney("USD", 1.99), 1, 0),
          ProVenuePricePoint(3, ProVenueMoney("USD", 1.99), 0, 1),
          ProVenuePricePoint(3, ProVenueMoney("USD", 1.99), 1, 1)
        ),
        buyerTypeRefs,
        priceScaleRefs
      )

      val result =
        actionGenerator.generatePricePointActions(1, 1, priceStructure, generatedPriceMap)
      assert(result.length === 4)
      all(result.map(_.price.value)) shouldBe 22.99
    }

    "filter out prices that haven't changed" in {
      val actionGenerator = InstanceFactory.get()

      val generatedPriceMap =
        Map(
          PipelinePriceByScale(1, 1, 2, 20, Some(1.99)) -> Seq.empty[BuyerTypePrice],
          PipelinePriceByScale(1, 1, 3, 21, Some(1.99)) -> Seq.empty[BuyerTypePrice]
        )

      val priceStructure = ProVenuePriceStructure(
        1,
        "code",
        Seq(
          ProVenuePricePoint(1, ProVenueMoney("USD", 1.99), 0, 0),
          ProVenuePricePoint(2, ProVenueMoney("USD", 1.99), 1, 0),
          ProVenuePricePoint(3, ProVenueMoney("USD", 1.99), 0, 1),
          ProVenuePricePoint(3, ProVenueMoney("USD", 1.99), 1, 1)
        ),
        buyerTypeRefs,
        priceScaleRefs
      )

      val result =
        actionGenerator.generatePricePointActions(1, 1, priceStructure, generatedPriceMap)
      assert(result.length === 0)
    }

    "work if there's no generated price for a price point" in {
      val actionGenerator = InstanceFactory.get()

      val generatedPriceMap = Map.empty[PipelinePriceByScale, Seq[BuyerTypePrice]]

      val priceStructure = ProVenuePriceStructure(
        1,
        "code",
        Seq(
          ProVenuePricePoint(1, ProVenueMoney("USD", 1.99), 0, 0),
          ProVenuePricePoint(2, ProVenueMoney("USD", 1.99), 1, 0),
          ProVenuePricePoint(3, ProVenueMoney("USD", 1.99), 0, 1),
          ProVenuePricePoint(3, ProVenueMoney("USD", 1.99), 1, 1)
        ),
        buyerTypeRefs,
        priceScaleRefs
      )

      val result =
        actionGenerator.generatePricePointActions(1, 1, priceStructure, generatedPriceMap)
      assert(result.length === 0)
    }
  }
}
