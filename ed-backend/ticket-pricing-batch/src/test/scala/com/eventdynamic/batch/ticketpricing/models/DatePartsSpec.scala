package com.eventdynamic.batch.ticketpricing.models

import java.sql.Timestamp

import org.scalatest.WordSpec

class DatePartsSpec extends WordSpec {
  "DateParts" should {
    "be the same for different years" in {
      val t1 = Timestamp.valueOf("2018-05-24 19:00:00.0")
      val t2 = Timestamp.valueOf("1999-05-24 19:00:00.0")

      val dp1 = DateParts.from(t1)
      val dp2 = DateParts.from(t2)
      assert(dp1 == DateParts(5, 24, 19))
      assert(dp2 == DateParts(5, 24, 19))
    }

    "round up to the nearest hour" in {
      val t = Timestamp.valueOf("2018-05-24 19:30:00.0")
      val dp = DateParts.from(t)
      assert(dp == DateParts(5, 24, 20))
    }

    "round down to the nearest hour" in {
      val t = Timestamp.valueOf("2018-05-24 19:29:00.0")
      val dp = DateParts.from(t)
      assert(dp == DateParts(5, 24, 19))
    }

    "months are 1 indexed" in {
      val t = Timestamp.valueOf("2018-01-01 20:00:00.0")
      val dp = DateParts.from(t)
      assert(dp == DateParts(1, 1, 20))

    }
  }
}
