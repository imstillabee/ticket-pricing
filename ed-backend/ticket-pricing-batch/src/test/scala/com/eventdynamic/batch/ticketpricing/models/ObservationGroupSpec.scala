package com.eventdynamic.batch.ticketpricing.models
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import org.scalatest.WordSpec

class ObservationGroupSpec extends WordSpec {
  val t = new TestHelper

  "ObservationGroup#fromObservations" should {
    "calculate remaining inventory" in {
      val observations = Seq(
        t.observation(soldPrice = None),
        t.observation(soldPrice = None),
        t.observation(soldPrice = Some(-10)),
        t.observation(soldPrice = Some(10)),
        t.observation(soldPrice = Some(100)),
        t.observation(soldPrice = Some(0))
      )

      val group = ObservationGroup.fromObservations(observations)

      assert(group.observations.length == 6)
      assert(group.features.inventory == 3.0)
    }
  }
}
