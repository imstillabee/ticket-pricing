package com.eventdynamic.batch.ticketpricing.models

import com.eventdynamic.batch.ticketpricing.models.weather.ObservationWeather
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.utils.DateHelper
import org.scalatest.WordSpec

class ObservationSpec extends WordSpec {
  val t = new TestHelper

  "Observation" should {
    "have the same number of headers as sequence elements" in {
      val o = t.observation()

      val seq = o.toSeq
      val headers = Observation.header

      assert(seq.size === headers.size)
    }
  }

  "Observation#from" should {
    "add event score modifier to event score" in {
      val timestamp = DateHelper.now()
      val weather = Some(ObservationWeather(Some(75f), 75f, 800))
      val eventScore = 25.0
      val spring = 2.0

      val base1 = t.observationBase(eventScoreModifier = 0.0, springModifier = 0.0)
      val obs1 =
        Observation.from(timestamp, base1, weather, eventScore, spring)
      assert(obs1.eventScore === 25.0)

      val base2 = t.observationBase(eventScoreModifier = 4.0, springModifier = 0.5)
      val obs2 =
        Observation.from(timestamp, base2, weather, eventScore, spring)
      assert(obs2.eventScore === 25.0)

      val base3 = t.observationBase(eventScoreModifier = -4.0, springModifier = -0.5)
      val obs3 =
        Observation.from(timestamp, base3, weather, eventScore, spring)
      assert(obs3.eventScore === 25.0)

      val obs4 =
        Observation.from(timestamp, base3, None, eventScore, spring)
      assert(obs4.eventScore === 25.0)
    }
  }
}
