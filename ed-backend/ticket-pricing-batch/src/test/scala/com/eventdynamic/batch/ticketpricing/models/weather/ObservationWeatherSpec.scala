package com.eventdynamic.batch.ticketpricing.models.weather

import org.scalatest.WordSpec

class ObservationWeatherSpec extends WordSpec {
  "ObservationWeather" should {
    "initialize from OpenWeatherMap weather code" in {
      assert(ObservationWeather(Some(75f), 75f, 500).condition == WeatherCondition.LightRain)
      assert(ObservationWeather(Some(75f), 75f, 501).condition == WeatherCondition.MediumRain)
      assert(ObservationWeather(Some(75f), 75f, 502).condition == WeatherCondition.HeavyRain)
      assert(ObservationWeather(Some(75f), 75f, 503).condition == WeatherCondition.HeavyRain)
      assert(ObservationWeather(Some(75f), 75f, 610).condition == WeatherCondition.LightSnow)
      assert(ObservationWeather(Some(75f), 75f, 611).condition == WeatherCondition.MediumSnow)
      assert(ObservationWeather(Some(75f), 75f, 612).condition == WeatherCondition.HeavySnow)
      assert(ObservationWeather(Some(75f), 75f, 800).condition == WeatherCondition.Clear)
      assert(ObservationWeather(Some(75f), 75f, 801).condition == WeatherCondition.Clouds)
      assert(ObservationWeather(Some(75f), 75f, 802).condition == WeatherCondition.HeavyClouds)
      assert(ObservationWeather(Some(75f), 75f, 999).condition == WeatherCondition.None)
    }
  }
}
