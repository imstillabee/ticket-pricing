package com.eventdynamic.batch.ticketpricing.models.weather

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.models.weather
import org.scalatest.WordSpec

class OpenWeatherForecastSpec extends WordSpec {
  "OpenWeatherForecast" should {
    "initialize from string" in {
      val t = Timestamp.valueOf("2018-05-24 19:00:00.0")
      val input =
        s"""{ "cod": "100", "list": [ { "dt": ${t.getTime / 1000}, "main": { "temp": 1.2, "temp_min": 2.2, "temp_max": 3.2 }, "weather": [{ "id": 800, "main": "clouds", "description": "the sky is cloudy" }] } ] }"""

      val weatherForecastOption = OpenWeatherForecast.fromResponse(input)
      assert(weatherForecastOption.isDefined)

      val weatherForecast = weatherForecastOption.get
      assert(weatherForecast.length == 1)

      val expected =
        OpenWeatherForecast(
          t,
          1.2,
          2.2,
          3.2,
          Seq(OpenWeatherForecastCondition(800, "clouds", "the sky is cloudy"))
        )

      assert(weatherForecast.head == expected)
    }

    "initialize from string with no weather" in {
      val t = Timestamp.valueOf("2018-05-24 19:00:00.0")
      val input =
        s"""{ "cod": "200", "list": [ { "dt": ${t.getTime / 1000}, "main": { "temp": 1.2, "temp_min": 2.2, "temp_max": 3.2 }, "weather": [] } ] }"""

      val weatherForecastOption = OpenWeatherForecast.fromResponse(input)
      assert(weatherForecastOption.isDefined)

      val weatherForecast = weatherForecastOption.get
      assert(weatherForecast.length == 1)

      val expected =
        weather.OpenWeatherForecast(t, 1.2, 2.2, 3.2, Seq[OpenWeatherForecastCondition]())

      assert(weatherForecast.head == expected)
    }

    "return None if json could not be parsed into a forecast" in {
      val input = "{}"

      val weatherForecast = OpenWeatherForecast.fromResponse(input)
      assert(weatherForecast.isEmpty)
    }

    "return None if passed malformed json" in {
      val input = "no bueno"

      val weatherForecast = OpenWeatherForecast.fromResponse(input)
      assert(weatherForecast.isEmpty)
    }
  }
}
