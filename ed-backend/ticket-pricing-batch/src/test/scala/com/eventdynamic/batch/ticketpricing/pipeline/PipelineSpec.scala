package com.eventdynamic.batch.ticketpricing.pipeline

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.configs.{PricingBatchConfig, PricingConfig}
import com.eventdynamic.batch.ticketpricing.models.PipelinePrice
import com.eventdynamic.batch.ticketpricing.models.weather.ObservationWeather
import com.eventdynamic.batch.ticketpricing.pipeline.composers.FactorComposer.Factors
import com.eventdynamic.batch.ticketpricing.pipeline.composers._
import com.eventdynamic.batch.ticketpricing.pipeline.dispatchers.{DispatchResult, Dispatcher}
import com.eventdynamic.batch.ticketpricing.pipeline.predictors.RowPredictor
import com.eventdynamic.batch.ticketpricing.pipeline.statscomposers.StatsComposer
import com.eventdynamic.batch.ticketpricing.pipeline.strategies.pricing.DemoPricingStrategy
import com.eventdynamic.batch.ticketpricing.services.{FileStore, ServiceHolder}
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.models.JobType.JobType
import com.eventdynamic.models._
import com.eventdynamic.modelserver.models._
import com.eventdynamic.services._
import com.eventdynamic.utils.DateHelper
import com.eventdynamic.utils.DateHelper.now
import org.mockito.Mockito.when
import org.scalatest.AsyncWordSpec
import org.specs2.mock.Mockito
import play.api.libs.json.Writes

import scala.concurrent.Future

class PipelineSpec extends AsyncWordSpec with Mockito {
  import PipelineSpec._

  "PipelineSpec" should {
    val client = createClient()
    val serviceHolder = mock[ServiceHolder]
    val pricingBatchConfig =
      PricingBatchConfig(
        clientId = 1,
        saveChunkSize = 100,
        skyboxUpdateChunkSize = 1000,
        globalPriceFloor = 0,
        enableWeatherData = true
      )
    val pricingBatchConfigNoWeather =
      PricingBatchConfig(
        clientId = 1,
        saveChunkSize = 100,
        skyboxUpdateChunkSize = 1000,
        globalPriceFloor = 0,
        enableWeatherData = false
      )
    val dispatcher = mock[Dispatcher]
    val tdcClientIntegration = ClientIntegrationComposite(
      Some(1),
      DateHelper.now(),
      DateHelper.now(),
      client.id.get,
      1,
      "TDC",
      None,
      None,
      true,
      true,
      None,
      None,
      None
    )
    val dispatchers = Map(tdcClientIntegration -> dispatcher)
    val fs = mock[FileStore]
    val event1 = createMockEvent(1, "Event 1")
    val event2 = createMockEvent(2, name = "Event 2")
    val scheduledJob = ScheduledJob(
      Some(1),
      event1.id.get,
      1,
      ScheduledJobStatus.Running,
      0,
      Some(DateHelper.now()),
      None,
      None,
      DateHelper.now(),
      DateHelper.now()
    )

    // Create mocks for service calls
    // Mock event service calls
    val eventService = mock[EventService]
    when(serviceHolder.eventService).thenReturn(eventService)

    when(eventService.getByIntegrationId(1, client.id.get))
      .thenReturn(Future.successful(Some(event1)))

    when(eventService.getByIntegrationId(2, client.id.get))
      .thenReturn(Future.successful(Some(event2)))

    when(eventService.getByIdBulk(any[Set[Int]]))
      .thenReturn(Future.successful(Seq(event1, event2)))

    val payload = ModelServerFactorPayload(
      ModelServerFactorContext(
        DateHelper.now().toString,
        MLBModelServerClientStats(1, 3, Some(2), 1.5f)
      ),
      Seq(
        ModelServerFactorExample(
          DateHelper.now().toString,
          5.5,
          2.2,
          Some(ModelServerWeather(55.3f, 48.5f, "")),
          MLBModelServerEventStats(5, 0, 0, 3, Some(3), 2, "NYY")
        )
      )
    )

    val payloadNoWeather = ModelServerFactorPayload(
      ModelServerFactorContext(
        DateHelper.now().toString,
        MLBModelServerClientStats(1, 3, Some(2), 1.5f)
      ),
      Seq(
        ModelServerFactorExample(
          DateHelper.now().toString,
          5.5,
          2.2,
          None,
          MLBModelServerEventStats(5, 0, 0, 3, Some(3), 2, "NYY")
        )
      )
    )

    // Create mock weatherComposer
    val mockedWeatherComposer = mock[WeatherComposer]

    when(mockedWeatherComposer.getWeatherMap(any[Seq[ObservationBase]]))
      .thenReturn(
        Future.successful(
          Map(
            WeatherKey(event1.timestamp.get, 1, "12345") ->
              ObservationWeather(None, 70f, 800),
            WeatherKey(event2.timestamp.get, 1, "12345") -> ObservationWeather(None, 70f, 800)
          )
        )
      )

    // Mock event seat service calls
    val eventSeatService = mock[EventSeatService]
    when(serviceHolder.eventSeatService).thenReturn(eventSeatService)

    when(eventSeatService.updateListedPrices(any[Seq[EventSeatListedPriceUpdateByScale]]))
      .thenReturn(Future.successful(2))

    // Mock transaction service calls
    val transactionService = mock[TransactionService]
    when(serviceHolder.transactionService).thenReturn(transactionService)

    when(transactionService.bulkInsert(any[Seq[Transaction]]))
      .thenReturn(Future.successful(Some(2)))

    // Mock observation service calls
    val observationService = mock[ObservationService]
    when(serviceHolder.observationService).thenReturn(observationService)

    // Initialize a mock generator helper
    val t = new TestHelper

    val observations = Seq(
      t.observationBase(
        eventId = 1,
        row = "AAA",
        priceScaleId = 1,
        eventDate = event1.timestamp.get,
        venueId = 1,
        venueZip = "12345"
      ),
      t.observationBase(
        eventId = 2,
        row = "AAA",
        priceScaleId = 1,
        eventDate = event2.timestamp.get,
        venueId = 1,
        venueZip = "12345"
      )
    )

    when(observationService.getAllForEvents(any[Seq[Int]], any[Int]))
      .thenReturn(Future.successful(observations))

    // Mock price guard service calls
    val priceGuardService = mock[PriceGuardService]
    when(serviceHolder.priceGuardService).thenReturn(priceGuardService)

    val priceGuard = createMockPriceGuard(minimumPrice = 135)
    when(
      priceGuardService
        .getAllForEventsSectionsAndRows(any[Set[Int]], any[Set[String]], any[Set[String]])
    ).thenReturn(Future.successful(Seq(priceGuard)))

    when(priceGuardService.getAllForEvents(any[Set[Int]]))
      .thenReturn(Future.successful(Seq(priceGuard)))

    // Mock price scale services calls
    val priceScaleService = mock[PriceScaleService]
    when(serviceHolder.priceScaleService).thenReturn(priceScaleService)

    when(priceScaleService.getAllForClient(any[Int]))
      .thenReturn(Future.successful(Seq(createMockPriceScale())))

    // Mock job config service calls
    val jobConfigService = mock[JobConfigService]
    when(serviceHolder.jobConfigService).thenReturn(jobConfigService)

    when(
      jobConfigService.updateConfig[PricingConfig](any[Int], any[JobType], any[PricingConfig])(
        any[Writes[PricingConfig]]
      )
    ).thenReturn(Future.successful(t.int()))

    // Create mocks for dispatchers
    when(dispatcher.run(any[Seq[PipelinePrice]], any[Int]))
      .thenReturn(Future.successful(DispatchResult(t.int())))

    // It's easier to use dummy services here for now - they're essentially mocks
    val predictor =
      new RowPredictor(new DemoPricingStrategy(true))

    // Create mocks for factor composer
    val mockedFactorComposer = mock[FactorComposer]

    // Create mocks for stats composer
    val mockedStatsComposer = mock[StatsComposer]

    when(
      mockedFactorComposer.getFactorMap(
        any[Int],
        any[Timestamp],
        any[Seq[ObservationBase]],
        any[Option[Map[WeatherKey, ObservationWeather]]],
        any[ModelServerClientStats],
        any[Map[Timestamp, ModelServerEventStats]]
      )
    ).thenReturn(
      Future.successful(Map(1 -> Factors(10.0, 1.0, 2.0, 0.5), 2 -> Factors(11.0, 2.0, 1.9, 0.5)))
    )

    when(
      mockedFactorComposer.getPayload(
        any[Seq[ObservationBase]],
        any[Option[Map[WeatherKey, ObservationWeather]]],
        any[Timestamp],
        any[ModelServerClientStats],
        any[Map[Timestamp, ModelServerEventStats]]
      )
    ).thenReturn(payload)

    "process ticket pricing batch with weather data" in {
      // Initialize the pipeline
      val pipeline = new Pipeline(
        pricingBatchConfig = pricingBatchConfig,
        scheduledJob = scheduledJob,
        serviceHolder = serviceHolder,
        weatherComposer = Some(mockedWeatherComposer),
        factorComposer = mockedFactorComposer,
        statsComposer = mockedStatsComposer,
        dispatchers = dispatchers,
        fs = fs,
        predictor = predictor
      )

      pipeline.execute()
      assert(true)
    }

    "process ticket pricing batch without weather data" in {
      // Initialize the pipeline for no weather data
      val pipelineNoWeather = new Pipeline(
        pricingBatchConfig = pricingBatchConfigNoWeather,
        scheduledJob = scheduledJob,
        serviceHolder = serviceHolder,
        weatherComposer = None,
        factorComposer = mockedFactorComposer,
        statsComposer = mockedStatsComposer,
        dispatchers = dispatchers,
        fs = fs,
        predictor = predictor
      )

      pipelineNoWeather.execute()
      assert(true)
    }
  }
}

object PipelineSpec {
  private def createClient(
    clientId: Int = 1,
    name: String = "Dialexa",
    pricingInterval: Int = 1500,
    logoUrl: String = "https://dialexa.com/logoUrl",
    eventScoreFloor: Double = 2,
    eventScoreCeiling: Option[Double] = None,
    springFloor: Double = 1.125,
    springCeiling: Option[Double] = None
  ): Client = {
    Client(
      id = Option(clientId),
      createdAt = now(),
      modifiedAt = now(),
      pricingInterval = pricingInterval,
      name = name,
      logoUrl = Option(logoUrl),
      eventScoreFloor = eventScoreFloor,
      eventScoreCeiling = eventScoreCeiling,
      springFloor = springFloor,
      springCeiling = springCeiling,
      ClientPerformanceType.MLB
    )
  }

  def createMockEvent(id: Int, name: String): Event = {
    Event(
      id = Some(id),
      primaryEventId = Some(1),
      createdAt = now(),
      modifiedAt = now(),
      name = name,
      timestamp = Option(now()),
      clientId = 1,
      venueId = 1,
      eventCategoryId = 1,
      seasonId = None,
      isBroadcast = true,
      percentPriceModifier = 0,
      eventScore = None,
      eventScoreModifier = 0,
      spring = None,
      springModifier = 0
    )
  }

  private def createMockPriceScale(
    priceScaleId: Int = 1,
    name: String = "First Data Platinum"
  ): PriceScale = {
    PriceScale(
      id = Option(priceScaleId),
      name = name,
      venueId = 1,
      integrationId = 1,
      createdAt = null,
      modifiedAt = null
    )
  }

  private def createMockPriceGuard(
    priceGuardId: Int = 1,
    eventId: Int = 1,
    section: String = "1",
    row: String = "AAA",
    minimumPrice: Int = 135,
    maximumPrice: Option[Int] = Option(200)
  ): PriceGuard = {
    val timestamp = DateHelper.now()

    PriceGuard(
      id = Option(priceGuardId),
      eventId = eventId,
      section = section,
      row = row,
      minimumPrice = minimumPrice,
      maximumPrice = maximumPrice,
      createdAt = timestamp,
      modifiedAt = timestamp
    )
  }
}
