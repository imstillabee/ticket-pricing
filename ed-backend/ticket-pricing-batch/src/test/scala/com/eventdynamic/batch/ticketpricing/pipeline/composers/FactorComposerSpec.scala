package com.eventdynamic.batch.ticketpricing.pipeline.composers

import java.sql.Timestamp
import java.time.ZoneId

import com.eventdynamic.batch.ticketpricing.models.weather.ObservationWeather
import com.eventdynamic.batch.ticketpricing.pipeline.composers.FactorComposer.Factors
import com.eventdynamic.batch.ticketpricing.pipeline.statscomposers.MLBStatsComposer
import com.eventdynamic.models.ObservationBase
import com.eventdynamic.modelserver.ModelServerService
import com.eventdynamic.modelserver.models._
import com.eventdynamic.mysportsfeed.models.mlb.{MLBGameStats, MLBTeamStats}
import com.eventdynamic.services.EventService
import com.softwaremill.sttp.Response
import org.mockito.Mockito._
import org.scalatest.AsyncWordSpec
import org.specs2.mock.Mockito

import scala.concurrent.Future

class FactorComposerSpec extends AsyncWordSpec with Mockito {
  import FactorComposerSpec._

  val clientId = 1
  val timestamp = new Timestamp(0)
  val eventDate1 = new Timestamp(100)
  val eventDate2 = new Timestamp(200)

  val weatherMap = Some(
    Map(
      WeatherKey(eventDate1, 1, "12345") -> ObservationWeather(None, 75f, 800),
      WeatherKey(eventDate2, 1, "12345") -> ObservationWeather(None, 70f, 800)
    )
  )

  val weatherMapNoData = None

  val gameStatsMap = Map(
    eventDate1 -> MLBGameStats(1, "NYY", true, true),
    eventDate2 -> MLBGameStats(2, "NYY", false, false)
  )

  val eventStatsMap = Map(
    eventDate1 -> MLBModelServerEventStats(5, 0, 0, 3, Some(3), 2, "NYY"),
    eventDate2 -> MLBModelServerEventStats(3, 1, 1, 2, Some(1), 0, "BOS")
  )

  val teamStats = Option(MLBTeamStats(1, 1f, 1f, 1f, 1))

  val observations =
    Seq(
      mockObservation(2, eventDate2),
      mockObservation(1, eventDate1),
      mockObservation(1, eventDate1)
    )

  val successResponse = Response(
    Right(ModelServerFactorResponse(Seq(75.0, 50.0), Seq(.02, .019))),
    200,
    "",
    scala.collection.immutable.Seq.empty[(String, String)],
    List.empty[Response[Unit]]
  )

  val failedResponse = Response[ModelServerFactorResponse](
    Left("hi"),
    200,
    "",
    scala.collection.immutable.Seq.empty[(String, String)],
    List.empty[Response[Unit]]
  )

  val statsComposer = mock[MLBStatsComposer]
  val mockClientStats = MLBModelServerClientStats(1, 3, Some(2), 1.5f)

  when(statsComposer.getClientStats(any[Seq[ObservationBase]]))
    .thenReturn(Future.successful(mockClientStats))
  when(statsComposer.getEventStatsMap(any[Seq[ObservationBase]], any[Timestamp]))
    .thenReturn(Future.successful(eventStatsMap))

  "FactorComposer#getFactorMap" should {
    "get and save factors" in {
      val mockModelServerService = mock[ModelServerService]
      val mockEventService = mock[EventService]

      when(mockEventService.updateFactors(any[Int], any[Option[Double]], any[Option[Double]]))
        .thenReturn(Future.successful(1))
      when(mockModelServerService.getFactors(any[String], any[ModelServerFactorPayload]))
        .thenReturn(Future.successful(successResponse))

      val factorComposer = new FactorComposer(mockModelServerService, mockEventService)

      factorComposer
        .getFactorMap(clientId, timestamp, observations, weatherMap, mockClientStats, eventStatsMap)
        .map(eventScoreMap => {
          verify(mockEventService, times(2))
            .updateFactors(any[Int], any[Option[Double]], any[Option[Double]])

          assert(eventScoreMap.size == 2)
          assert(eventScoreMap(1) == Factors(75.0, 0.0, 2.0, 0.0))
          assert(eventScoreMap(2) == Factors(50.0, 0.0, 1.9, 0.0))
        })
    }

    "propagate factor request uneven" in {
      val mockModelServerService = mock[ModelServerService]
      val mockEventService = mock[EventService]

      val unevenSuccess = Response(
        Right(ModelServerFactorResponse(Seq(75.0, 50.0), Seq(.02))),
        200,
        "",
        scala.collection.immutable.Seq.empty[(String, String)],
        List.empty[Response[Unit]]
      )

      when(mockModelServerService.getFactors(any[String], any[ModelServerFactorPayload]))
        .thenReturn(Future.successful(unevenSuccess))

      val factorComposer = new FactorComposer(mockModelServerService, mockEventService)

      recoverToSucceededIf[Exception](
        factorComposer
          .getFactorMap(
            clientId,
            timestamp,
            observations,
            weatherMap,
            mockClientStats,
            eventStatsMap
          )
      )
    }

    "propagate factor request malformed" in {
      val mockModelServerService = mock[ModelServerService]
      val mockEventService = mock[EventService]

      when(mockModelServerService.getFactors(any[String], any[ModelServerFactorPayload]))
        .thenReturn(Future.successful(failedResponse))

      val factorComposer = new FactorComposer(mockModelServerService, mockEventService)

      recoverToSucceededIf[Exception](
        factorComposer
          .getFactorMap(
            clientId,
            timestamp,
            observations,
            weatherMap,
            mockClientStats,
            eventStatsMap
          )
      )
    }

    "propagate factor request error" in {
      val mockModelServerService = mock[ModelServerService]
      val mockEventService = mock[EventService]

      when(mockModelServerService.getFactors(any[String], any[ModelServerFactorPayload]))
        .thenReturn(Future.failed(new Exception("get scores failed")))

      val factorComposer = new FactorComposer(mockModelServerService, mockEventService)

      recoverToSucceededIf[Exception](
        factorComposer
          .getFactorMap(
            clientId,
            timestamp,
            observations,
            weatherMap,
            mockClientStats,
            eventStatsMap
          )
      )
    }

    "continue when saving event score locally fails" in {
      val mockModelServerService = mock[ModelServerService]
      val mockEventService = mock[EventService]

      when(mockEventService.updateFactors(any[Int], any[Option[Double]], any[Option[Double]]))
        .thenReturn(Future.failed(new Exception("save failed")))
      when(mockModelServerService.getFactors(any[String], any[ModelServerFactorPayload]))
        .thenReturn(Future.successful(successResponse))

      val factorComposer = new FactorComposer(mockModelServerService, mockEventService)

      factorComposer
        .getFactorMap(clientId, timestamp, observations, weatherMap, mockClientStats, eventStatsMap)
        .map(eventScoreMap => {
          verify(mockEventService, times(2))
            .updateFactors(any[Int], any[Option[Double]], any[Option[Double]])

          assert(eventScoreMap.size == 2)
          assert(eventScoreMap(1) == Factors(75.0, 0.0, 2.0, 0.0))
          assert(eventScoreMap(2) == Factors(50.0, 0.0, 1.9, 0.0))
        })
    }

    "build Model Server Payload" in {
      val mockModelServerService = mock[ModelServerService]
      val mockEventService = mock[EventService]

      val factorComposer = new FactorComposer(mockModelServerService, mockEventService)

      val payload = factorComposer.getPayload(
        observations,
        weatherMap,
        timestamp,
        mockClientStats,
        eventStatsMap
      )
      assert(
        payload.context == ModelServerFactorContext(
          timestamp.toInstant.toString,
          MLBModelServerClientStats(1, 3, Some(2), 1.5f)
        )
      )
      assert(payload.examples.length == 2)
      assert(
        payload.examples == Seq(
          ModelServerFactorExample(
            eventDate1.toInstant.atZone(ZoneId.of("America/New_York")).toOffsetDateTime.toString,
            0,
            0,
            Some(ModelServerWeather(75.0f, 75.0f, "CLEAR")),
            MLBModelServerEventStats(5, 0, 0, 3, Some(3), 2, "NYY")
          ),
          ModelServerFactorExample(
            eventDate2.toInstant.atZone(ZoneId.of("America/New_York")).toOffsetDateTime.toString,
            0.0,
            0.0,
            Some(ModelServerWeather(70.0f, 70.0f, "CLEAR")),
            MLBModelServerEventStats(3, 1, 1, 2, Some(1), 0, "BOS")
          )
        )
      )
    }

    "build Model Server Payload with no weather data" in {
      val mockModelServerService = mock[ModelServerService]
      val mockEventService = mock[EventService]

      val factorComposer = new FactorComposer(mockModelServerService, mockEventService)

      val payload = factorComposer.getPayload(
        observations,
        weatherMapNoData,
        timestamp,
        mockClientStats,
        eventStatsMap
      )
      assert(
        payload.context == ModelServerFactorContext(
          timestamp.toInstant.toString,
          MLBModelServerClientStats(1, 3, Some(2), 1.5f)
        )
      )
      assert(payload.examples.length == 2)
      assert(
        payload.examples == Seq(
          ModelServerFactorExample(
            eventDate1.toInstant.atZone(ZoneId.of("America/New_York")).toOffsetDateTime.toString,
            0,
            0,
            None,
            MLBModelServerEventStats(5, 0, 0, 3, Some(3), 2, "NYY")
          ),
          ModelServerFactorExample(
            eventDate2.toInstant.atZone(ZoneId.of("America/New_York")).toOffsetDateTime.toString,
            0.0,
            0.0,
            None,
            MLBModelServerEventStats(3, 1, 1, 2, Some(1), 0, "BOS")
          )
        )
      )
    }
  }
}

object FactorComposerSpec {

  def mockObservation(eventId: Int, eventDate: Timestamp): ObservationBase =
    ObservationBase(
      seatId = 1,
      eventId = eventId,
      eventName = "game1",
      integrationEventId = 1,
      integrationSeatId = 1,
      venueId = 1,
      venueZip = "12345",
      venueTimeZone = "America/New_York",
      eventDate = eventDate,
      seat = "seat",
      row = "row",
      section = "section",
      eventSeatId = 1,
      listPrice = Some(12.5),
      overridePrice = None,
      priceScaleId = 1,
      externalPriceScaleId = 1,
      priceScale = "scale",
      soldPrice = Some(12.25),
      revenueCategoryId = None,
      buyerTypeCode = None,
      integrationId = None,
      transactionType = None,
      isListed = true,
      isEventListed = true,
      eventScoreModifier = 0.0,
      springModifier = 0.0,
      isHeld = false
    )
}
