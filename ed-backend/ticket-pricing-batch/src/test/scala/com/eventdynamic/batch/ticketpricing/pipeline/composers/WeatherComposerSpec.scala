package com.eventdynamic.batch.ticketpricing.pipeline.composers

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.models.weather.{
  ObservationWeather,
  OpenWeatherForecast,
  WeatherCondition
}
import com.eventdynamic.batch.ticketpricing.services.weather.OpenWeatherForecastService
import com.eventdynamic.models.{ObservationBase, WeatherAverage}
import com.eventdynamic.services._
import com.eventdynamic.utils.DateHelper
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll}

import scala.concurrent.Future

class WeatherComposerSpec extends AsyncWordSpec with MockitoSugar with BeforeAndAfterAll {

  def mockObservation(
    zip: String = "12345",
    venueId: Int = 1,
    time: Timestamp = Timestamp.valueOf("2018-01-01 05:00:00.0")
  ): ObservationBase =
    ObservationBase(
      seatId = 1,
      eventId = 1,
      eventName = "event1",
      integrationEventId = 1,
      integrationSeatId = 1,
      venueId = venueId,
      venueZip = zip,
      venueTimeZone = "America/New_York",
      eventDate = time,
      seat = "seat",
      row = "row",
      section = "section",
      eventSeatId = 1,
      listPrice = Some(12.5),
      overridePrice = None,
      priceScaleId = 1,
      externalPriceScaleId = 1,
      priceScale = "scale",
      soldPrice = Some(12.25),
      revenueCategoryId = None,
      buyerTypeCode = None,
      integrationId = None,
      transactionType = None,
      isListed = true,
      isEventListed = true,
      eventScoreModifier = 0.0,
      springModifier = 0.0,
      isHeld = false
    )

  def mockForecast(
    time: Timestamp = Timestamp.valueOf("2018-01-01 01:00:00.0"),
    temp: Double = 75.0
  ): OpenWeatherForecast = {
    OpenWeatherForecast(time, temp, 0.0, 100.0, Seq())
  }

  def mockAverage(
    venueId: Int = 1,
    day: Int = 1,
    month: Int = 0,
    hour: Int = 1,
    temp: Double = 75.0
  ): WeatherAverage = {
    WeatherAverage(Some(1), DateHelper.now(), DateHelper.now(), venueId, month, day, hour, temp)
  }

  val mockedForecasts = Seq(
    mockForecast(time = Timestamp.valueOf("2018-01-01 01:00:00.0"), temp = 70.0),
    mockForecast(time = Timestamp.valueOf("2018-01-01 04:00:00.0"), temp = 75.0),
    mockForecast(time = Timestamp.valueOf("2018-01-01 07:00:00.0"), temp = 80.0)
  )

  val mockedServices =
    WeatherServices(mock[OpenWeatherForecastService], mock[WeatherAverageService])
  val weatherComposer = new WeatherComposer(mockedServices)

  override def beforeAll(): Unit = {
    super.beforeAll()

    when(mockedServices.forecastService.getForecast(mockObservation().venueZip))
      .thenReturn(Future.successful(Some(mockedForecasts)))
    when(mockedServices.forecastService.getForecast("invalid"))
      .thenReturn(Future.successful(None))

    when(mockedServices.averageService.get(anyInt, anyInt, anyInt, anyInt))
      .thenReturn(Future.successful(None))
  }

  "WeatherComposer.findCloseWeatherForecast" should {
    val mockedForecasts = Seq(
      mockForecast(time = Timestamp.valueOf("2018-01-01 04:00:00.0"), temp = 70.0),
      mockForecast(time = Timestamp.valueOf("2018-01-01 07:00:00.0"), temp = 75.0),
      mockForecast(time = Timestamp.valueOf("2018-01-01 10:00:00.0"), temp = 80.0)
    )

    "find forecast where time is an exact match" in {
      val t = Timestamp.valueOf("2018-01-01 07:00:00.0")
      val v = weatherComposer.findCloseWeatherForecast(t, mockedForecasts)
      assert(v.contains(mockedForecasts(1)))
    }

    "find forecast where time is in between two forecast times" in {
      val t = Timestamp.valueOf("2018-01-01 06:00:00.0")
      val v = weatherComposer.findCloseWeatherForecast(t, mockedForecasts)
      assert(v.contains(mockedForecasts(1)))
    }

    "find forecast where time is before the first forecast but within min difference" in {
      val t = Timestamp.valueOf("2018-01-01 04:00:00.0")
      val v = weatherComposer.findCloseWeatherForecast(t, mockedForecasts)
      assert(v.contains(mockedForecasts.head))
    }

    "find forecast where time is after the last forecast but within min difference" in {
      val t = Timestamp.valueOf("2018-01-01 11:00:00.0")
      val v = weatherComposer.findCloseWeatherForecast(t, mockedForecasts)
      assert(v.contains(mockedForecasts(2)))
    }

    "not find a forecast where time is beyond the min difference before the first forecast" in {
      val t = Timestamp.valueOf("2018-01-01 01:00:00.0")
      val v = weatherComposer.findCloseWeatherForecast(t, mockedForecasts)
      assert(v.isEmpty)
    }

    "not find a forecast where time is beyond the min difference after the last forecast" in {
      val t = Timestamp.valueOf("2018-01-01 13:00:00.0")
      val v = weatherComposer.findCloseWeatherForecast(t, mockedForecasts)
      assert(v.isEmpty)
    }
  }

  "WeatherComposer.getResult" should {
    "throw an exception if there is no average" in {
      val observation = mockObservation(zip = "invalid")

      recoverToSucceededIf[Exception](
        new WeatherComposer(mockedServices)
          .getWeatherMap(Seq(observation))
      )
    }

    "have both average and actual weather" in {
      val mockedAverage = mockAverage(temp = 90.0)
      val observation =
        mockObservation(time = Timestamp.valueOf("2018-01-01 05:00:00.0"))

      when(mockedServices.averageService.get(observation.venueId, 1, 1, 5))
        .thenReturn(Future.successful(Some(mockedAverage)))

      new WeatherComposer(mockedServices)
        .getWeatherMap(Seq(observation))
        .map(
          res =>
            assert(res.values.head == ObservationWeather(Some(75f), 90f, WeatherCondition.Clear))
        )
    }

    "not include forecast if it is too far in the future" in {
      val mockedAverage = mockAverage(temp = 90.0)
      val observation =
        mockObservation(time = Timestamp.valueOf("2018-01-01 11:00:00.0"))

      when(mockedServices.averageService.get(observation.venueId, 1, 1, 11))
        .thenReturn(Future.successful(Some(mockedAverage)))

      new WeatherComposer(mockedServices)
        .getWeatherMap(Seq(observation))
        .map(
          res => assert(res.values.head == ObservationWeather(None, 90f, WeatherCondition.Clear))
        )
    }
  }
}
