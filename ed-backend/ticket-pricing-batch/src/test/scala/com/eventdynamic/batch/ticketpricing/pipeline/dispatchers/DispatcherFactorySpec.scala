package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.IntegrationServiceBuilder
import com.eventdynamic.batch.ticketpricing.configs.{PricingBatchConfig, PricingConfig}
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.models.ClientIntegrationComposite
import com.eventdynamic.utils.DateHelper
import org.mockito.Mockito._
import org.scalatest.AsyncWordSpec
import org.specs2.mock.Mockito

import scala.concurrent.Future

class DispatcherFactorySpec extends AsyncWordSpec with Mockito {
  val t = new TestHelper
  val serviceHolder = t.mockedServices
  val integrationServiceBuilder = mock[IntegrationServiceBuilder]
  val pricingConfig = mock[PricingConfig]
  val pricingBatchConfig = mock[PricingBatchConfig]
  val skyboxJson = s"""{
                      |"appKey": "appKey",
                      |"baseUrl": "baseUrl",
                      |"accountId": 1,
                      |"apiKey": "appKey",
                      |"defaultVendorId": 1,
                      |"customers": {"123456": 5},
                      |"customersToIgnore":[ "11" ]
                      |}
                      |""".stripMargin
  val tdcJson = s"""{
                |"agent":"agent",
                |"apiKey":"appKey",
                |"appId":"appId",
                |"username":"username",
                |"password":"password",
                |"baseUrl":"baseUrl",
                |"supplierId":1,
                |"tdcProxyBaseUrl": "proxyBaseUrl",
                |"mlbamGridConfigs":[]}
                |""".stripMargin
  val stubhubJson = s"""{
                    |"mode": "mode",
                    |"username": "username",
                    |"password": "password",
                    |"consumerKey": "consumerKey",
                    |"consumerSecret": "consumerSecret",
                    |"baseUrl": "baseUrl",
                    |"applicationToken": "applicationToken",
                    |"sellerGUID": "sellerGUID"
                }""".stripMargin
  val edTicketsJson = s"""{
                     |"apiKey": "apiKey",
                     |"appId": "appId",
                     |"baseUrl": "baseUrl",
                     |"appDatabase": "appDatabase",
                     |"email": "email",
                     |"password": "password",
                     |"functionsUrl": "functionsUrl"
                     |}
                   """.stripMargin

  def buildClientIntegration(
    id: Int,
    name: String,
    primary: Boolean = true,
    active: Boolean = true,
    json: String = ""
  ) =
    ClientIntegrationComposite(
      Some(id),
      DateHelper.now(),
      DateHelper.now(),
      1,
      id,
      name,
      None,
      Some(json),
      active,
      primary,
      None,
      None,
      None
    )

  "DispatcherFactory#getDispatchersForClient" should {
    "build a basic dispatcher mapping" in {
      val clientIntegrations =
        Seq(
          buildClientIntegration(1, "Tickets.com", json = tdcJson),
          buildClientIntegration(3, "StubHub", false, json = stubhubJson)
        )

      when(
        serviceHolder.clientIntegrationService.getByClientId(any[Int], any[Boolean], any[Boolean])
      ).thenReturn(Future.successful(clientIntegrations))

      DispatcherFactory
        .getDispatchersForClient(pricingBatchConfig, serviceHolder, integrationServiceBuilder)
        .map(dispatchers => {
          assert(dispatchers.size == 2)

          val tdc = dispatchers.find(_._1.name == "Tickets.com")
          assert(tdc.isDefined)
          assert(tdc.get._2.getClass == classOf[TDCDispatcher])

          val edtickets = dispatchers.find(_._1.name == "EDTickets")
          assert(edtickets.isEmpty)

          val stubhub = dispatchers.find(_._1.name == "StubHub")
          assert(stubhub.isDefined)
          assert(stubhub.get._2.getClass == classOf[StubHubDispatcher])
        })
    }

    "propogate service error" in {
      when(
        serviceHolder.clientIntegrationService.getByClientId(any[Int], any[Boolean], any[Boolean])
      ).thenReturn(Future.failed(new Exception))

      recoverToSucceededIf[Exception](
        DispatcherFactory
          .getDispatchersForClient(pricingBatchConfig, serviceHolder, integrationServiceBuilder)
      )
    }
  }

  "DispatcherFactory#buildDispatcher" should {
    "build a Tickets.com dispatcher" in {
      val tdcIntegration = buildClientIntegration(1, "Tickets.com", json = tdcJson)
      val dispatcher = DispatcherFactory.buildDispatcher(
        pricingBatchConfig,
        tdcIntegration,
        serviceHolder,
        integrationServiceBuilder
      )

      assert(dispatcher.getClass == classOf[TDCDispatcher])
    }

    "build a Skybox dispatcher" in {
      val skyboxIntegration = buildClientIntegration(1, "Skybox", json = skyboxJson)
      val dispatcher = DispatcherFactory.buildDispatcher(
        pricingBatchConfig,
        skyboxIntegration,
        serviceHolder,
        integrationServiceBuilder
      )

      assert(dispatcher.getClass == classOf[SkyboxDispatcher])
    }

    "build a EDTickets dispatcher" in {
      val edTicketsIntegration = buildClientIntegration(1, "EDTickets", json = edTicketsJson)
      val dispatcher = DispatcherFactory.buildDispatcher(
        pricingBatchConfig,
        edTicketsIntegration,
        serviceHolder,
        integrationServiceBuilder
      )

      assert(dispatcher.getClass == classOf[EDTicketsDispatcher])
    }

    "build a StubHub dispatcher" in {
      val stubhubIntegration = buildClientIntegration(1, "StubHub", json = stubhubJson)
      val dispatcher = DispatcherFactory.buildDispatcher(
        pricingBatchConfig,
        stubhubIntegration,
        serviceHolder,
        integrationServiceBuilder
      )

      assert(dispatcher.getClass == classOf[StubHubDispatcher])
    }

    "throw an error building an unknown dispatcher" in {
      val tdcIntegration = buildClientIntegration(1, "Fake")

      assertThrows[UnsupportedOperationException](
        DispatcherFactory
          .buildDispatcher(
            pricingBatchConfig,
            tdcIntegration,
            serviceHolder,
            integrationServiceBuilder
          )
      )
    }
  }
}
