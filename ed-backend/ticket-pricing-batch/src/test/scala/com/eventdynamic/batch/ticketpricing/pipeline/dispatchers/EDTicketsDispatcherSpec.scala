package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.edtickets.EDTicketsService
import com.eventdynamic.edtickets.models.{EDLoginResponse, EDPrice}
import com.eventdynamic.models.ClientIntegrationComposite
import com.eventdynamic.utils.DateHelper
import com.softwaremill.sttp.Response
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.{AsyncWordSpec, BeforeAndAfterEach}

import scala.concurrent.Future

class EDTicketsDispatcherSpec extends AsyncWordSpec with MockitoSugar with BeforeAndAfterEach {
  val t = new TestHelper
  var edTicketsService: EDTicketsService = mock[EDTicketsService]
  val clientId = 1

  var edTicketsDispatcher: EDTicketsDispatcher =
    new EDTicketsDispatcher(t.mockedServices, edTicketsService, 2)

  val listingMappings: Map[Int, String] = (0 to 9).map(i => i -> "2").toMap

  def successResponse[T](body: T): Response[T] = {
    Response(Right(body), 200, "success", scala.collection.immutable.Seq(), List())
  }

  val clientIntegrationComposite: ClientIntegrationComposite = ClientIntegrationComposite(
    Some(1),
    DateHelper.now(),
    DateHelper.now(),
    clientId,
    1,
    "EDTickets",
    None,
    None,
    true,
    false,
    None,
    Some(0),
    None
  )

  override def beforeEach(): Unit = {
    super.beforeEach()

    edTicketsService = mock[EDTicketsService]
    edTicketsDispatcher = new EDTicketsDispatcher(t.mockedServices, edTicketsService, 2)

    when(edTicketsService.authenticate)
      .thenReturn(Future.successful(successResponse[EDLoginResponse](EDLoginResponse("", ""))))

    when(edTicketsService.updatePrices(any[Seq[EDPrice]]))
      .thenReturn(Future.successful(successResponse("success")))

    when(
      t.mockedServices.clientIntegrationService.getByClientId(any[Int], any[Boolean], any[Boolean])
    ).thenReturn(Future.successful(Seq(clientIntegrationComposite)))

    when(t.mockedServices.integrationTransactions.getListingsMap(any[Int]))
      .thenReturn(Future.successful(listingMappings))
  }

  "EDTicketsDispatcher#pushPrices" should {
    "push prices" in {
      val prices = (0 to 9).map(i => t.pipelinePrice(eventSeatId = i))

      edTicketsDispatcher
        .run(prices, clientId)
        .map(r => assert(r.updates == 1))
    }

    "push prices even if one seat doesn't have price" in {
      val emptyPrice = t.pipelinePrice(eventSeatId = 9, price = None)
      val prices = (0 to 9).map(i => t.pipelinePrice(eventSeatId = i)) :+ emptyPrice

      edTicketsDispatcher
        .run(prices, clientId)
        .map(r => {
          assert(r.updates == 1)

          // Verify we called the service with the correct price
          val captor = ArgumentCaptor.forClass(classOf[Seq[EDPrice]])
          verify(edTicketsService).updatePrices(captor.capture())
          assert(captor.getValue.forall(_.updatePrice === 18.99))
        })
    }

    "not update prices if one seat is not toggled" in {
      val prices = (0 to 9).map(i => t.pipelinePrice(eventSeatId = i, shouldPrice = false))

      edTicketsDispatcher
        .run(prices, clientId)
        .map(r => assert(r.updates == 0))
    }

    "push prices by row and listing" in {
      val prices = (0 to 9).map(i => t.pipelinePrice(eventSeatId = i))

      when(t.mockedServices.integrationTransactions.getListingsMap(any[Int]))
        .thenReturn(Future.successful((0 to 9).map(i => i -> s"$i").toMap))

      edTicketsDispatcher
        .run(prices, clientId)
        .map(r => assert(r.updates == 10))
    }

    "use overriden price" in {
      val prices = (0 to 9).map(_ => t.pipelinePrice())
      val overridePrice1 = prices.head.copy(price = Some(100), overridden = true)
      val overridePrice2 = prices.head.copy(price = Some(50), overridden = true)

      edTicketsDispatcher
        .run(prices :+ overridePrice1 :+ overridePrice2, clientId)
        .map(r => {
          assert(r.updates == 1)

          // Verify we called the service with the correct price
          val captor = ArgumentCaptor.forClass(classOf[Seq[EDPrice]])
          verify(edTicketsService).updatePrices(captor.capture())
          assert(captor.getValue.forall(_.updatePrice === 100))
        })

    }

  }
}
