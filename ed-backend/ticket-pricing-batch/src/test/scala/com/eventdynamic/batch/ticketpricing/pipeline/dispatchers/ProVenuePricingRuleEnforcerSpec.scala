package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.batch.ticketpricing.pipeline.dispatchers.ProVenuePricingRuleEnforcer.EventScale
import com.eventdynamic.batch.ticketpricing.models.PipelinePriceByScale
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.mlbam.models.BuyerTypePrice
import com.eventdynamic.models.RoundingType.RoundingType
import com.eventdynamic.models.{ProVenuePricingRule, RoundingType}
import com.eventdynamic.services.ProVenuePricingRuleService
import com.eventdynamic.services.ProVenuePricingRuleService.CompoundProVenuePricingRule
import com.eventdynamic.utils.DateHelper
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.{AsyncWordSpec, BeforeAndAfterEach, Matchers, PrivateMethodTester}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future
import scala.util.Random

class ProVenuePricingRuleEnforcerSpec
    extends AsyncWordSpec
    with Matchers
    with MockitoSugar
    with BeforeAndAfterEach
    with PrivateMethodTester {
  var proVenuePricingRuleService: ProVenuePricingRuleService = mock[ProVenuePricingRuleService]
  var t = new TestHelper
  var mockedServices: ServiceHolder = t.mockedServices

  var proVenuePricingRuleEnforcer = new ProVenuePricingRuleEnforcer(proVenuePricingRuleService)

  object Rand {

    def pricingRule(
      mirrorPriceScaleId: Option[Int] = None,
      percent: Option[Int] = None,
      constant: Option[BigDecimal] = None,
      round: RoundingType = RoundingType.Ceil
    ): ProVenuePricingRule = {
      ProVenuePricingRule(
        Some(Random.nextInt()),
        DateHelper.now(),
        DateHelper.now(),
        Random.nextInt(),
        None,
        true,
        mirrorPriceScaleId,
        percent,
        constant,
        round
      )
    }

    def pipelinePriceByScale(
      eventId: Int = Random.nextInt(),
      integrationEventId: Int = Random.nextInt(),
      priceScale: Int = Random.nextInt(),
      externalPriceScaleId: Int = Random.nextInt(),
      price: Option[BigDecimal] = Some(Random.nextInt())
    ): PipelinePriceByScale = {
      PipelinePriceByScale(eventId, integrationEventId, priceScale, externalPriceScaleId, price)
    }
  }

  "ProVenuePricingRuleEnforcer#applyAdjustmentRules" should {
    val applyAdjustmentRules =
      PrivateMethod[Map[PipelinePriceByScale, Seq[BuyerTypePrice]]]('applyAdjustmentRules)
    "should create map with rules applied for each buyer type" in {
      val rule = CompoundProVenuePricingRule(
        ProVenuePricingRule(
          Some(1),
          DateHelper.now(),
          DateHelper.now(),
          1,
          None,
          true,
          None,
          None,
          Some(10),
          RoundingType.None
        ),
        Seq("10", "11"),
        Seq(2, 3),
        Seq(1)
      )
      val adjustmentRules = Map(PipelinePriceByScale(1, 1, 2, 20, Some(12.99)) -> Seq(rule))
      val result = proVenuePricingRuleEnforcer invokePrivate applyAdjustmentRules(
        adjustmentRules,
        Map()
      )

      val expected = Map(
        PipelinePriceByScale(1, 1, 2, 20, Some(12.99)) -> List(
          BuyerTypePrice("10", Some(22.99)),
          BuyerTypePrice("11", Some(22.99))
        )
      )
      assert(result == expected)
    }

    "should create map with no adjusted prices if there are no rules applied" in {
      val adjustmentRules = Map(PipelinePriceByScale(1, 1, 2, 20, Some(12.99)) -> Seq())
      val result = proVenuePricingRuleEnforcer invokePrivate applyAdjustmentRules(
        adjustmentRules,
        Map()
      )

      val expected = Map(PipelinePriceByScale(1, 1, 2, 20, Some(12.99)) -> Seq())
      assert(result == expected)
    }
  }

  "ProVenuePricingRuleEnforcer#fetchAdjustmentRules" should {
    val fetchAdjustmentRules = PrivateMethod[Future[
      Map[PipelinePriceByScale, Seq[ProVenuePricingRuleService.CompoundProVenuePricingRule]]
    ]]('fetchAdjustmentRules)
    "should fetch adjustment rules and create map" in {
      val prices =
        Seq(
          PipelinePriceByScale(1, 1, 2, 20, Some(12.99)),
          PipelinePriceByScale(1, 1, 3, 21, Some(12.99))
        )
      val rule = CompoundProVenuePricingRule(
        ProVenuePricingRule(
          Some(1),
          DateHelper.now(),
          DateHelper.now(),
          1,
          None,
          true,
          None,
          None,
          Some(10),
          RoundingType.None
        ),
        Seq("10", "11"),
        Seq(2, 3),
        Seq(1)
      )
      when(proVenuePricingRuleService.getAll(any[Int], any[Boolean])) thenReturn Future
        .successful(Seq(rule))

      val futureResult = proVenuePricingRuleEnforcer invokePrivate fetchAdjustmentRules(1, prices)
      val expectedResult = prices.map(price => price -> Seq(rule)).toMap
      for {
        result <- futureResult
      } yield assert(result == expectedResult)
    }

    "should filter out rules not associated with an event in a pipeline price" in {
      val prices = Seq(PipelinePriceByScale(1, 1, 1, 20, Some(12.99)))
      val rule = CompoundProVenuePricingRule(
        ProVenuePricingRule(
          Some(1),
          DateHelper.now(),
          DateHelper.now(),
          1,
          None,
          true,
          None,
          None,
          Some(10),
          RoundingType.None
        ),
        Seq("10", "11"),
        Seq(2, 3),
        Seq(1)
      )
      when(proVenuePricingRuleService.getAll(any[Int], any[Boolean])) thenReturn Future
        .successful(Seq(rule))

      val futureResult = proVenuePricingRuleEnforcer invokePrivate fetchAdjustmentRules(1, prices)
      val expectedResult = prices.map(price => price -> Seq.empty).toMap
      for {
        result <- futureResult
      } yield assert(result == expectedResult)
    }
  }

  "ProVenuePricingRuleEnforcer#calculateAdjustment" should {
    "mirror another price scale" in {
      val price = t.pipelinePrice(price = Some[BigDecimal](12.99)).toPipelinePriceByScale
      val mirror = 1
      val rule = Rand.pricingRule(mirrorPriceScaleId = Some(mirror))
      val genPriceByScale =
        Map(EventScale(price.eventId, mirror) -> Rand.pipelinePriceByScale(price = Some(22.99)))

      val adjusted = proVenuePricingRuleEnforcer.calculateAdjustment(price, rule, genPriceByScale)
      adjusted shouldBe Some(23)
    }

    "mirror another price scale even if current price is empty" in {
      val price = t.pipelinePrice(price = None).toPipelinePriceByScale
      val mirror = 1
      val rule = Rand.pricingRule(mirrorPriceScaleId = Some(mirror))
      val genPriceByScale =
        Map(EventScale(price.eventId, mirror) -> Rand.pipelinePriceByScale(price = Some(22.99)))

      val adjusted = proVenuePricingRuleEnforcer.calculateAdjustment(price, rule, genPriceByScale)
      adjusted shouldBe Some(23)
    }

    "erase the price if the price structure doesn't have the mirrored price scale" in {
      val price = t.pipelinePrice(price = Some[BigDecimal](12.99)).toPipelinePriceByScale
      val mirror = 1
      val rule = Rand.pricingRule(mirrorPriceScaleId = Some(mirror), constant = Some(10.0))
      val genPriceByScale =
        Map(EventScale(price.eventId, 22) -> Rand.pipelinePriceByScale(price = Some(22.99)))

      val adjusted = proVenuePricingRuleEnforcer.calculateAdjustment(price, rule, genPriceByScale)
      adjusted shouldBe None
    }

    "alter by negative percentage" in {
      val price = t.pipelinePrice(price = Some[BigDecimal](20)).toPipelinePriceByScale
      val rule = Rand.pricingRule(percent = Some(-25))

      val adjusted = proVenuePricingRuleEnforcer.calculateAdjustment(price, rule, Map())
      adjusted shouldBe Some(15)
    }

    "ignore prices that are empty" in {
      val price = t.pipelinePrice(price = None).toPipelinePriceByScale
      val rule = Rand.pricingRule(percent = Some(25))

      val adjusted = proVenuePricingRuleEnforcer.calculateAdjustment(price, rule, Map())
      adjusted shouldBe None
    }

    "alter by positive percentage" in {
      val price = t.pipelinePrice(price = Some[BigDecimal](20)).toPipelinePriceByScale
      val rule = Rand.pricingRule(percent = Some(25))

      val adjusted = proVenuePricingRuleEnforcer.calculateAdjustment(price, rule, Map())
      adjusted shouldBe Some(25)
    }

    "alter by negative constant" in {
      val price = t.pipelinePrice(price = Some[BigDecimal](20)).toPipelinePriceByScale
      val rule = Rand.pricingRule(constant = Some(-5))

      val adjusted = proVenuePricingRuleEnforcer.calculateAdjustment(price, rule, Map())
      adjusted shouldBe Some(15)
    }

    "alter by positive constant" in {
      val price = t.pipelinePrice(price = Some[BigDecimal](20)).toPipelinePriceByScale
      val rule = Rand.pricingRule(constant = Some(5))

      val adjusted = proVenuePricingRuleEnforcer.calculateAdjustment(price, rule, Map())
      adjusted shouldBe Some(25)
    }

    "round down to the nearest dollar" in {
      val prices = Seq[BigDecimal](20.99, 20.5, 20.01)
        .map(p => t.pipelinePrice(price = Some(p)).toPipelinePriceByScale)
      val rule = Rand.pricingRule(round = RoundingType.Floor)

      val results = prices.map(proVenuePricingRuleEnforcer.calculateAdjustment(_, rule, Map()))

      all(results) shouldBe Some(20)
    }

    "round up to the nearest dollar" in {
      val prices = Seq[BigDecimal](20.99, 20.5, 20.01)
        .map(p => t.pipelinePrice(price = Some(p)).toPipelinePriceByScale)
      val rule = Rand.pricingRule(round = RoundingType.Ceil)

      val results = prices.map(proVenuePricingRuleEnforcer.calculateAdjustment(_, rule, Map()))

      all(results) shouldBe Some(21)
    }

    "round naturally up" in {
      val prices = Seq[BigDecimal](20.99, 20.5)
        .map(p => t.pipelinePrice(price = Some(p)).toPipelinePriceByScale)
      val rule = Rand.pricingRule(round = RoundingType.Round)

      val results = prices.map(proVenuePricingRuleEnforcer.calculateAdjustment(_, rule, Map()))

      all(results) shouldBe Some(21)
    }

    "round naturally down" in {
      val prices = Seq[BigDecimal](20.01, 20.49)
        .map(p => t.pipelinePrice(price = Some(p)).toPipelinePriceByScale)
      val rule = Rand.pricingRule(round = RoundingType.Round)

      val results = prices.map(proVenuePricingRuleEnforcer.calculateAdjustment(_, rule, Map()))

      all(results) shouldBe Some(20)
    }

    "round down to the nearest cent" in {
      // Would normally only happen due to a percent change
      val prices = Seq[BigDecimal](20.001, 20.0049)
        .map(p => t.pipelinePrice(price = Some(p)).toPipelinePriceByScale)
      val rule = Rand.pricingRule(round = RoundingType.None)

      val results = prices.map(proVenuePricingRuleEnforcer.calculateAdjustment(_, rule, Map()))
      all(results) shouldBe Some(20.00)
    }

    "round up to the nearest cent" in {
      // Would normally only happen due to a percent change
      val prices = Seq[BigDecimal](20.005, 20.0051, 20.0099)
        .map(p => t.pipelinePrice(price = Some(p)).toPipelinePriceByScale)
      val rule = Rand.pricingRule(round = RoundingType.None)

      val results = prices.map(proVenuePricingRuleEnforcer.calculateAdjustment(_, rule, Map()))
      all(results) shouldBe Some(20.01)
    }

    "combine all rule parameters in order (mirror, percent, constant, round)" in {
      val price = t.pipelinePrice(price = Some[BigDecimal](12.99)).toPipelinePriceByScale
      val mirror = 1
      val rule = Rand.pricingRule(
        mirrorPriceScaleId = Some(mirror),
        constant = Some(10),
        percent = Some(50),
        round = RoundingType.Ceil
      )
      val genPriceByScale =
        Map(EventScale(price.eventId, mirror) -> Rand.pipelinePriceByScale(price = Some(21.5)))

      val adjusted = proVenuePricingRuleEnforcer.calculateAdjustment(price, rule, genPriceByScale)
      adjusted shouldBe Some(43)
    }
  }

}
