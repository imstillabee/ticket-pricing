package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import org.scalatest.WordSpec

class RowDispatcherSpec extends WordSpec {
  val t = new TestHelper

  "RowDispatcher#groupPricesByRow" should {
    "ignore unavailable seats" in {
      val availablePrices = t.some(_ => t.pipelinePrice(price = Some(20)))
      val unavailablePrices = t.some(_ => t.pipelinePrice(price = Some(25), available = false))
      val overriddenPrices =
        t.some(_ => t.pipelinePrice(price = Some(15), overridden = true, available = false))

      val rowPrices =
        RowDispatcher.groupPricesByRow(availablePrices ++ unavailablePrices ++ overriddenPrices)

      assert(rowPrices.length === 1)
      assert(rowPrices.head.price.contains(20))
    }

    "prioritize overridden prices" in {
      val availablePrices = t.some(_ => t.pipelinePrice(price = Some(20)))
      val overriddenPrices = t.some(_ => t.pipelinePrice(price = Some(15), overridden = true))

      val rowPrices =
        RowDispatcher.groupPricesByRow(availablePrices ++ overriddenPrices)

      assert(rowPrices.length === 1)
      assert(rowPrices.head.price.contains(15))
    }

    "not generate prices for empty row" in {
      val unavailablePrices = t.some(_ => t.pipelinePrice(available = false))

      val rowPrices =
        RowDispatcher.groupPricesByRow(unavailablePrices)

      assert(rowPrices.isEmpty)
    }
  }
}
