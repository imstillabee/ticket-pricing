package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import org.scalatest.WordSpec

class ScaleDispatcherSpec extends WordSpec {
  val t = new TestHelper

  "ScaleDispatcher#groupPricesByScale" should {
    "ignore unavailable seats" in {
      val availablePrices = t.some(_ => t.pipelinePrice(price = Some(20)))
      val unavailablePrices = t.some(_ => t.pipelinePrice(price = Some(25), available = false))
      val overriddenPrices =
        t.some(_ => t.pipelinePrice(price = Some(15), overridden = true, available = false))

      val scalePrices =
        ScaleDispatcher.groupPricesByScale(availablePrices ++ unavailablePrices ++ overriddenPrices)

      assert(scalePrices.length === 1)
      assert(scalePrices.head.price.contains(20))
    }

    "prioritize overridden prices" in {
      val availablePrices = t.some(_ => t.pipelinePrice(price = Some(20)))
      val overriddenPrices = t.some(_ => t.pipelinePrice(price = Some(15), overridden = true))

      val scalePrices =
        ScaleDispatcher.groupPricesByScale(availablePrices ++ overriddenPrices)

      assert(scalePrices.length === 1)
      assert(scalePrices.head.price.contains(15))
    }

    "not generate prices for empty scales" in {
      val availablePrices = t.some(_ => t.pipelinePrice(available = false))

      val scalePrices =
        ScaleDispatcher.groupPricesByScale(availablePrices)

      assert(scalePrices.isEmpty)
    }
  }
}
