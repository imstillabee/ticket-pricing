package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import java.time.Instant

import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.skybox.SkyboxService
import com.eventdynamic.skybox.models._
import com.softwaremill.sttp.Response
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.{AsyncWordSpec, BeforeAndAfterEach}

import scala.concurrent.Future

class SkyboxDispatcherSpec extends AsyncWordSpec with MockitoSugar with BeforeAndAfterEach {
  val t = new TestHelper
  var skyboxService: SkyboxService = mock[SkyboxService]

  var skyboxDispatcher =
    new SkyboxDispatcher(skyboxService, 100)

  val clientId = 1
  val accountId = 3000

  val pipelinePrices = Seq(
    t.pipelinePrice(eventSeatId = 1, integrationEventId = 1, price = Some(21)),
    t.pipelinePrice(eventSeatId = 2, integrationEventId = 1, price = Some(21)),
    t.pipelinePrice(eventSeatId = 3, integrationEventId = 2, price = Some(22)),
    t.pipelinePrice(eventSeatId = 4, integrationEventId = 2, price = Some(22)),
    t.pipelinePrice(eventSeatId = 5, integrationEventId = 2, price = None, section = "101")
  )

  val inventoryBody = SkyboxInventoryGetResponse(
    Seq(
      SkyboxListing(100, 1, "100", "1", Some(15), "status", 1, 10, Some(10)),
      SkyboxListing(200, 2, "100", "1", Some(17), "status", 1, 10, Some(10)),
      SkyboxListing(300, 2, "101", "1", Some(20), "status", 1, 10, Some(10))
    )
  )

  val inventoryReponse = Response(
    Right(inventoryBody),
    200,
    "",
    scala.collection.immutable.Seq.empty[(String, String)],
    List.empty[Response[Unit]]
  )

  val inventoryFailedReponse = Response[SkyboxInventoryGetResponse](
    Left("asdf"),
    400,
    "",
    scala.collection.immutable.Seq.empty[(String, String)],
    List.empty[Response[Unit]]
  )

  val invoiceSuccess = SkyboxSoldInventoryGetResponse(
    Seq(SkyboxSoldInventory(201, Instant.EPOCH, 22, "100", "1", "2", 2, 2336))
  )

  val invoiceResponse = Response(
    Right(invoiceSuccess),
    200,
    "",
    scala.collection.immutable.Seq.empty[(String, String)],
    List.empty[Response[Unit]]
  )

  override def beforeEach(): Unit = {
    super.beforeEach()

    reset(skyboxService)
    when(skyboxService.accountId).thenReturn(accountId)
  }

  "SkyboxDispatcher#updatePrices" should {
    "push prices to skybox" in {
      when(skyboxService.getInventory(Seq(1, 2))).thenReturn(Future.successful(inventoryReponse))

      when(skyboxService.getInvoices(Seq(1, 2), Instant.EPOCH))
        .thenReturn(Future.successful(invoiceResponse))

      when(skyboxService.bulkUpdatePrices(any[Seq[SkyboxBulkPriceItem]]))
        .thenReturn(Future.successful(true))

      skyboxDispatcher
        .run(pipelinePrices, clientId)
        .map(result => {
          verify(skyboxService).bulkUpdatePrices(
            Seq(
              SkyboxBulkPriceItem(100, accountId, 21),
              SkyboxBulkPriceItem(200, accountId, 22),
              SkyboxBulkPriceItem(201, accountId, 22)
            )
          )

          assert(result.updates === 2)
        })
    }

    "throw error when exception to push prices to skybox" in {
      when(skyboxService.getInventory(Seq(1, 2))).thenReturn(Future.failed(new Exception))

      when(skyboxService.getInvoices(Seq(1, 2), Instant.EPOCH))
        .thenReturn(Future.successful(invoiceResponse))

      recoverToSucceededIf[Exception](skyboxDispatcher.run(pipelinePrices, clientId))
    }

    "throw error when missing body getting inventory" in {
      when(skyboxService.getInventory(Seq(1, 2)))
        .thenReturn(Future.successful(inventoryFailedReponse))

      when(skyboxService.getInvoices(Seq(1, 2), Instant.EPOCH))
        .thenReturn(Future.successful(invoiceResponse))

      recoverToSucceededIf[Exception](skyboxDispatcher.run(pipelinePrices, clientId))
    }
  }
}
