package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.models.ClientIntegrationComposite
import com.eventdynamic.stubhub.StubHubService
import com.eventdynamic.stubhub.models._
import com.eventdynamic.utils.DateHelper
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.{AsyncWordSpec, BeforeAndAfterEach}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future

class StubHubDispatcherSpec extends AsyncWordSpec with MockitoSugar with BeforeAndAfterEach {
  val t = new TestHelper
  var stubHubService: StubHubService = mock[StubHubService]

  var stubHubDispatcher: StubHubDispatcher =
    new StubHubDispatcher(t.mockedServices, stubHubService, 1)

  val listingMappings: Map[Int, String] = Map(1 -> "2")

  override def beforeEach(): Unit = {
    super.beforeEach()

    stubHubService = mock[StubHubService]

    stubHubDispatcher = new StubHubDispatcher(t.mockedServices, stubHubService, 1)

    when(
      t.mockedServices.clientIntegrationService.getByClientId(any[Int], any[Boolean], any[Boolean])
    ).thenReturn(
      Future.successful(
        Seq(
          ClientIntegrationComposite(
            Some(1),
            DateHelper.now(),
            DateHelper.now(),
            1,
            1,
            "StubHub",
            None,
            None,
            true,
            false,
            None,
            Some(0),
            None
          )
        )
      )
    )

    when(t.mockedServices.integrationTransactions.getListingsMap(any[Int]))
      .thenReturn(Future.successful(Map(1 -> "2")))

    when(stubHubService.login())
      .thenReturn(Future.successful(ApiSuccess(200, StubHubLoginResponse("", ""))))

    when(stubHubService.updateListing(any[Int], any[BigDecimal]))
      .thenReturn(Future.successful(ApiSuccess(200, StubHubListingResponse("1", "ACTIVE", None))))

  }

  "StubHubDispatcher#getMappingsForListings" should {
    "get correct mappings of eventSeatId to listingId" in {
      assert(stubHubDispatcher.getMappingsForListings(1).isInstanceOf[Map[Int, String]])
    }
  }

  "StubHubDispatcher#updatePrices" should {
    "push prices to stubHub with update listing call" in {
      val tickets = Seq(t.pipelinePrice())
      for {
        total <- stubHubDispatcher.updatePrices(tickets, Map(1 -> "2"))
      } yield {
        assert(total === 1)
      }
    }

    "return 0 if api error occurs" in {
      when(stubHubService.updateListing(any[Int], any[BigDecimal]))
        .thenReturn(Future.successful(ApiError(400, StubHubErrorResponse("", ""))))

      val tickets = Seq(t.pipelinePrice())
      for {
        total <- stubHubDispatcher.updatePrices(tickets, listingMappings)
      } yield {
        assert(total === 0)
      }
    }

    "return 0 if inventory has no mapped listing id" in {
      val tickets = Seq(t.pipelinePrice(eventSeatId = 2))
      for {
        total <- stubHubDispatcher.updatePrices(tickets, listingMappings)
      } yield {
        assert(total === 0)
      }
    }
  }

  "StubHubDispatcher#pushPricesByRow" should {
    "login to stubHub and Push prices" in {
      val tickets = Seq(t.pipelinePrice())

      for {
        total <- stubHubDispatcher.pushPricesByRow(tickets, listingMappings)
      } yield {
        assert(total === 1)
      }
    }

    "Not push prices if failed to login" in {
      when(stubHubService.login())
        .thenReturn(Future.successful(ApiError(400, StubHubErrorResponse("", ""))))

      val tickets = Seq(t.pipelinePrice())

      for {
        total <- stubHubDispatcher.pushPricesByRow(tickets, listingMappings)
      } yield {
        assert(total === 0)
      }
    }
  }
}
