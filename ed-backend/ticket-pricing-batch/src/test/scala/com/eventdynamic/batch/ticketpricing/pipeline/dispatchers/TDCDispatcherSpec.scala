package com.eventdynamic.batch.ticketpricing.pipeline.dispatchers

import com.eventdynamic.batch.ticketpricing.PricePointActionGenerator
import com.eventdynamic.batch.ticketpricing.models.PipelinePriceByScale
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.mlbam.PricingGridService
import com.eventdynamic.mlbam.models.{BuyerTypePrice, PricingGridBatch}
import com.eventdynamic.models.{
  ClientIntegration,
  ClientIntegrationComposite,
  PriceGridConfig,
  ProVenuePricingRule,
  RoundingType
}
import com.eventdynamic.provenue.ProVenueService
import com.eventdynamic.provenue.models._
import com.eventdynamic.services.ProVenuePricingRuleService
import com.eventdynamic.services.ProVenuePricingRuleService.CompoundProVenuePricingRule
import com.eventdynamic.utils.{DateHelper, SuccessResponse}
import com.softwaremill.sttp.Response
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.{AsyncWordSpec, BeforeAndAfterEach, PrivateMethodTester}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future
import scala.util.Random

class TDCDispatcherSpec
    extends AsyncWordSpec
    with MockitoSugar
    with BeforeAndAfterEach
    with PrivateMethodTester {
  var pvService: ProVenueService = mock[ProVenueService]
  var pgService: PricingGridService = mock[PricingGridService]
  var pvPricingRuleService: ProVenuePricingRuleService = mock[ProVenuePricingRuleService]

  var pvPricingRuleEnforcer: ProVenuePricingRuleEnforcer = new ProVenuePricingRuleEnforcer(
    pvPricingRuleService
  )
  val t = new TestHelper
  val mockedServices: ServiceHolder = t.mockedServices

  var actionGenerator = new PricePointActionGenerator(mockedServices.proVenuePricingRuleService)
  val json = s"""{
                   |"agent":"agent",
                   |"apiKey":"appKey",
                   |"appId":"appId",
                   |"username":"username",
                   |"password":"password",
                   |"baseUrl":"baseUrl",
                   |"supplierId":1,
                   |"tdcProxyBaseUrl": "proxyBaseUrl",
                   |"mlbamGridConfigs":[
                   |{"priceGroupId": 6046314,"externalBuyerTypeIds": ["3161"]},
                   |{"priceGroupId": 6048343,"externalBuyerTypeIds": ["3168","3169"]}]}
                   |""".stripMargin

  var mockClientIntegration = ClientIntegrationComposite(
    id = Some(1),
    createdAt = DateHelper.now(),
    modifiedAt = DateHelper.now(),
    clientId = 1,
    integrationId = 1,
    name = "Tickets.com",
    version = None,
    json = Some(json),
    isActive = true,
    isPrimary = true,
    logoUrl = None,
    percent = None,
    constant = None
  )

  var tdcDispatcher: TDCDispatcher =
    new TDCDispatcher(
      pvService,
      actionGenerator,
      pvPricingRuleEnforcer,
      mockedServices,
      mockClientIntegration,
      pgService,
      Seq("SINGLE")
    )

  def usPrice(p: BigDecimal) = ProVenueMoney("USD", p)

  def successResponse[T](body: T): Response[T] = {
    Response(Right(body), 200, "success", scala.collection.immutable.Seq(), List())
  }

  override def beforeEach(): Unit = {
    super.beforeEach()

    pvService = mock[ProVenueService]
    actionGenerator = new PricePointActionGenerator(mockedServices.proVenuePricingRuleService)
    tdcDispatcher = new TDCDispatcher(
      pvService,
      actionGenerator,
      pvPricingRuleEnforcer,
      mockedServices,
      mockClientIntegration,
      pgService,
      Seq("SINGLE")
    )

    // ping default mock
    when(pvService.ping()).thenReturn(
      Future.successful(
        successResponse[ProVenuePingResponse](
          ProVenuePingResponse(
            clientId = "clientId",
            proVenueAppVersion = "version",
            currentUTC = "utc",
            timeZoneCode = "timezone"
          )
        )
      )
    )

    when(
      mockedServices.clientIntegrationService.getByClientId(any[Int], any[Boolean], any[Boolean])
    ).thenReturn(Future.successful(Seq(mockClientIntegration)))

    when(pvPricingRuleService.getAll(any[Int], any[Boolean]))
      .thenReturn(
        Future.successful(
          Seq(
            CompoundProVenuePricingRule(
              proVenuePricingRule = ProVenuePricingRule(
                id = Some(1),
                createdAt = DateHelper.now(),
                modifiedAt = DateHelper.now(),
                clientId = 1,
                name = Some("Rule"),
                isActive = true,
                mirrorPriceScaleId = Some(1),
                percent = Some(0),
                constant = Some(BigDecimal(0)),
                round = RoundingType.Round
              ),
              externalBuyerTypeIds = Seq("1", "2", "3"),
              priceScaleIds = Seq(0, 1, 2, 3),
              eventIds = Seq(1, 2, 3)
            )
          )
        )
      )

    // bulkUpdatePricePoints default mock
    when(pvService.bulkUpdatePricePoints(any[Int], any[Seq[ProVenuePricePointAction]]))
      .thenAnswer(i => {
        val pricePointActions = i.getArguments()(1).asInstanceOf[Seq[ProVenuePricePointAction]]
        val proVenuePricePointBulkResponse = ProVenuePricePointBulkResponse(
          pricePointActions.length,
          0,
          0,
          0,
          pricePointActions.length,
          0,
          0,
          0
        )
        Future.successful(successResponse(proVenuePricePointBulkResponse))
      })

    // getPriceStructure default mock
    when(pvService.getPriceStructure(any[Int], any[String]))
      .thenReturn(
        Future.successful(
          successResponse(
            ProVenuePriceStructureResponse(
              ProVenuePriceStructure(
                1,
                "structure",
                Seq(ProVenuePricePoint(1, usPrice(19.99), 0, 0)),
                Seq(ProVenueBuyerType(1, "ADULT", true, "description")),
                Seq(ProVenuePriceScale(1, "scale"))
              )
            )
          )
        )
      )

    when(
      mockedServices.clientIntegrationService.getByName(any[Int], name = any[String], any[Boolean])
    ).thenReturn(
      Future.successful(
        Some(
          ClientIntegrationComposite(
            id = Some(Random.nextInt()),
            createdAt = DateHelper.now(),
            modifiedAt = DateHelper.now(),
            clientId = 1,
            integrationId = Random.nextInt(),
            name = "Tickets.com",
            version = Some("version"),
            json = None,
            isActive = true,
            isPrimary = true,
            logoUrl = None,
            percent = Some(0),
            constant = None
          )
        )
      )
    )

    when(mockedServices.clientIntegrationService.updateVersion(any[Int], any[String]))
      .thenReturn(
        Future.successful(
          SuccessResponse(
            ClientIntegration(
              id = Some(Random.nextInt()),
              createdAt = DateHelper.now(),
              modifiedAt = DateHelper.now(),
              clientId = Random.nextInt(),
              integrationId = Random.nextInt(),
              version = Some("version"),
              json = None,
              isActive = true,
              isPrimary = true
            )
          )
        )
      )

    when(
      mockedServices.proVenuePricingRuleService
        .getAll(any[Int], any[Boolean])
    ).thenReturn(Future.successful(Seq()))

    when(mockedServices.mlbamMappingService.getTeamIdByClientId(any[Int]))
      .thenReturn(Future.successful(Some(1)))

    when(mockedServices.mlbamMappingService.getSectionIdByPriceScaleId(any[Int]))
      .thenReturn(Future.successful(Some(1)))

    when(mockedServices.mlbamMappingService.getScheduleIdByEventId(any[Int]))
      .thenReturn(Future.successful(Some(1)))

    when(
      pgService
        .bulkUpdatePrices(Seq(any[PricingGridBatch]), any[Int], any[Int])
    ).thenReturn(
      Future.successful(
        successResponse(
          """<ns2:PriceFeedResponse xmlns:ns2="http://www.mlb.com/ticketing/price/dynamic" hasErrors="false" totalRecords="1" totalGoodRecords="1" totalBadRecords="0" totalSkippedRecords="0" totalUnchangedRecords="0" totalPriceUpdates="1" totalFeaturedUpdates="0"/>"""
        )
      )
    )
  }

  "TDCDispatcher#pushPricesByScale" should {
    "push one price structure" in {
      val prices = (0 to 9).map(_ => t.pipelinePrice())

      tdcDispatcher
        .run(prices, 1)
        .map(r => {
          assert(r.updates == 1)
        })
    }

    "not push price structure when seat is off" in {
      val prices1 = (0 to 9).map(_ => t.pipelinePrice())
      val prices2 = (0 to 9).map(_ => t.pipelinePrice(integrationEventId = 2, eventId = 2)) :+ t
        .pipelinePrice(integrationEventId = 2, eventId = 2, shouldPrice = false)

      tdcDispatcher
        .run(prices1 ++ prices2, 1)
        .map(r => {
          assert(r.updates == 1)
        })
    }

    "not push price structure when price is empty" in {
      val prices1 = (0 to 9).map(_ => t.pipelinePrice())
      val prices2 =
        (0 to 9).map(_ => t.pipelinePrice(integrationEventId = 2, eventId = 2, price = None))

      tdcDispatcher
        .run(prices1 ++ prices2, 1)
        .map(r => {
          assert(r.updates == 1)
        })
    }

    "push multiple price structures" in {
      val eventIdToPriceStructureId = Map(12 -> 13, 14 -> 15)

      val first =
        (0 to 9).map(
          idx => t.pipelinePrice(integrationEventId = 12, eventId = 2, externalPriceScaleId = idx)
        )
      val second =
        (0 to 9).map(
          idx => t.pipelinePrice(integrationEventId = 14, eventId = 3, externalPriceScaleId = idx)
        )

      when(pvService.getPriceStructure(any[Int], any[String])).thenAnswer(i => {
        val eventId = i.getArguments.head.asInstanceOf[Int]

        val response = eventIdToPriceStructureId.get(eventId) match {
          case Some(psId) =>
            successResponse(
              ProVenuePriceStructureResponse(
                ProVenuePriceStructure(
                  psId,
                  "structure",
                  Seq(
                    ProVenuePricePoint(1, usPrice(19.99), 0, 0),
                    ProVenuePricePoint(1, usPrice(19.99), 1, 0)
                  ),
                  Seq(ProVenueBuyerType(1, "ADULT", active = true, "description")),
                  Seq(ProVenuePriceScale(1, "scale"), ProVenuePriceScale(2, "scale"))
                )
              )
            )
          case None =>
            Response(Left("Not found"), 404, "not found", scala.collection.immutable.Seq(), List())
        }

        Future.successful(response)
      })

      tdcDispatcher
        .run(first ++ second, 1)
        .map(r => {
          assert(r.updates == 4)
        })
    }
  }

  "push prices for all buyer types" in {
    val prices =
      (0 to 9).map(idx => t.pipelinePrice(integrationEventId = 12, externalPriceScaleId = idx))

    when(pvService.getPriceStructure(any[Int], any[String]))
      .thenReturn(
        Future.successful(
          successResponse(
            ProVenuePriceStructureResponse(
              ProVenuePriceStructure(
                1,
                "structure",
                for {
                  buyerTypeIndex <- 0 to 2
                  priceScaleIndex <- 0 to 1
                } yield {
                  ProVenuePricePoint(1, usPrice(19.99), priceScaleIndex, buyerTypeIndex)
                },
                Seq(
                  ProVenueBuyerType(1, "ADULT", active = true, "description"),
                  ProVenueBuyerType(2, "OTHER", active = true, "description"),
                  ProVenueBuyerType(3, "CHILD", active = true, "description")
                ),
                Seq(ProVenuePriceScale(1, "scale"), ProVenuePriceScale(2, "scale"))
              )
            )
          )
        )
      )

    tdcDispatcher
      .run(prices, 1)
      .map(r => {
        assert(r.updates == 6)
      })
  }

  "push prices for multiple sale types" in {
    val prices =
      (0 to 9).map(idx => t.pipelinePrice(integrationEventId = 12, externalPriceScaleId = idx))

    when(pvService.getPriceStructure(any[Int], any[String]))
      .thenReturn(
        Future.successful(
          successResponse(
            ProVenuePriceStructureResponse(
              ProVenuePriceStructure(
                1,
                "structure",
                for {
                  buyerTypeIndex <- 0 to 2
                  priceScaleIndex <- 0 to 1
                } yield {
                  ProVenuePricePoint(1, usPrice(19.99), priceScaleIndex, buyerTypeIndex)
                },
                Seq(
                  ProVenueBuyerType(1, "ADULT", active = true, "description"),
                  ProVenueBuyerType(2, "OTHER", active = true, "description"),
                  ProVenueBuyerType(3, "CHILD", active = true, "description")
                ),
                Seq(ProVenuePriceScale(1, "scale"), ProVenuePriceScale(2, "scale"))
              )
            )
          )
        )
      )

    val tdcDispatcher =
      new TDCDispatcher(
        pvService,
        actionGenerator,
        pvPricingRuleEnforcer,
        mockedServices,
        mockClientIntegration,
        pgService,
        Seq("SINGLE", "GROUP")
      )
    tdcDispatcher
      .run(prices, 1)
      .map(r => {
        verify(pvService).getPriceStructure(12, "SINGLE")
        verify(pvService).getPriceStructure(12, "GROUP")
        verify(pvService, times(2))
          .bulkUpdatePricePoints(any[Int], any[Seq[ProVenuePricePointAction]])
        assert(r.updates == 12)
      })
  }

  "apply buyer type rules" in {
    val prices =
      (0 to 9).map(idx => t.pipelinePrice(priceScaleId = idx, externalPriceScaleId = idx))

    when(pvService.getPriceStructure(any[Int], any[String]))
      .thenReturn(
        Future.successful(
          successResponse(
            ProVenuePriceStructureResponse(
              ProVenuePriceStructure(
                1,
                "structure",
                for {
                  buyerTypeIndex <- 0 to 2
                  priceScaleIndex <- 0 to 1
                } yield {
                  ProVenuePricePoint(1, usPrice(19.99), priceScaleIndex, buyerTypeIndex)
                },
                Seq(
                  ProVenueBuyerType(1, "ADULT", active = true, "description"),
                  ProVenueBuyerType(2, "OTHER", active = true, "description"),
                  ProVenueBuyerType(3, "CHILD", active = true, "description")
                ),
                Seq(ProVenuePriceScale(1, "scale"), ProVenuePriceScale(2, "scale"))
              )
            )
          )
        )
      )

    tdcDispatcher
      .run(prices, 1)
      .map(r => {
        assert(r.updates == 6)
      })
  }

  "not push prices for prices that have not changed" in {
    val samePrice = 19.99
    val pricesThatChanged =
      (1 to 3).map(
        idx =>
          t.pipelinePrice(integrationEventId = 12, externalPriceScaleId = idx, priceScaleId = idx)
      )
    val pricesThatStayed =
      (4 to 6).map(
        idx =>
          t.pipelinePrice(
            integrationEventId = 12,
            externalPriceScaleId = idx,
            priceScaleId = idx,
            price = Some(samePrice)
        )
      )

    val allPrices = pricesThatChanged ++ pricesThatStayed

    val priceScales = allPrices.map(p => ProVenuePriceScale(p.externalPriceScaleId, "scale"))
    when(pvService.getPriceStructure(any[Int], any[String]))
      .thenReturn(
        Future.successful(
          successResponse(
            ProVenuePriceStructureResponse(ProVenuePriceStructure(1, "structure", for {
              priceScaleIndex <- allPrices.indices
            } yield {
              ProVenuePricePoint(priceScaleIndex + 20, usPrice(samePrice), priceScaleIndex, 0)
            }, Seq(ProVenueBuyerType(1, "ADULT", active = true, "description")), priceScales))
          )
        )
      )

    tdcDispatcher
      .run(pricesThatChanged ++ pricesThatStayed, 1)
      .map(r => {
        assert(pricesThatChanged.length == 3)
        assert(pricesThatStayed.length == 3)

        assert(r.updates == pricesThatChanged.length)
        assert(r.updates != allPrices.length)
      })
  }

  "use overridden price" in {
    val prices = (0 to 9).map(_ => t.pipelinePrice())
    val overridePrice1 = prices.head.copy(price = Some(100), overridden = true)
    val overridePrice2 = prices.head.copy(price = Some(50), overridden = true)

    tdcDispatcher
      .run(prices :+ overridePrice1 :+ overridePrice2, 1)
      .map(r => {
        assert(r.updates == 1)

        // Verify we called the service with the correct price
        val captor = ArgumentCaptor.forClass(classOf[Seq[ProVenuePricePointAction]])
        verify(pvService).bulkUpdatePricePoints(any[Int], captor.capture())
        assert(captor.getValue.forall(_.price.value === 100))
      })
  }

  "TDCDispatcher#filterPricingGridData" should {
    "return filtered pricingGridBatch sequence" in {
      val unfiltedData = Seq(
        (Some(1), Some(2), Some(BigDecimal(10.0))),
        (Some(2), Some(3), Some(BigDecimal(10.0))),
        (Some(1), None, Some(BigDecimal(10.0))),
        (None, Some(2), Some(BigDecimal(10.0))),
        (None, None, Some(BigDecimal(10.0)))
      )
      val filteredData = tdcDispatcher.filterPricingGridData(unfiltedData)
      assert(filteredData.length == 2)
      assert(filteredData.contains(PricingGridBatch(1, 2, BigDecimal(10.0))))
      assert(filteredData.contains(PricingGridBatch(2, 3, BigDecimal(10.0))))
    }
  }

  "TDCDispatcher#getMLBAMMappings" should {
    "return 4 mappings given 4 pipeline price by scale" in {
      when(mockedServices.mlbamMappingService.getScheduleIdByEventId(1))
        .thenReturn(Future.successful(Some(1)))
      when(mockedServices.mlbamMappingService.getScheduleIdByEventId(2))
        .thenReturn(Future.successful(Some(2)))
      when(mockedServices.mlbamMappingService.getScheduleIdByEventId(3))
        .thenReturn(Future.successful(Some(3)))
      when(mockedServices.mlbamMappingService.getScheduleIdByEventId(4))
        .thenReturn(Future.successful(None))

      when(mockedServices.mlbamMappingService.getSectionIdByPriceScaleId(1))
        .thenReturn(Future.successful(Some(1)))
      when(mockedServices.mlbamMappingService.getSectionIdByPriceScaleId(2))
        .thenReturn(Future.successful(Some(2)))
      when(mockedServices.mlbamMappingService.getSectionIdByPriceScaleId(3))
        .thenReturn(Future.successful(Some(3)))
      when(mockedServices.mlbamMappingService.getSectionIdByPriceScaleId(4))
        .thenReturn(Future.successful(None))

      val priceMap = Map(
        PipelinePriceByScale(1, 1, 1, 1, Some(10)) -> Seq(
          BuyerTypePrice("1", Some(10)),
          BuyerTypePrice("2", Some(11))
        ),
        PipelinePriceByScale(2, 2, 2, 2, Some(10)) -> Seq(
          BuyerTypePrice("1", Some(10)),
          BuyerTypePrice("2", Some(11))
        ),
        PipelinePriceByScale(3, 3, 3, 3, Some(10)) -> Seq(
          BuyerTypePrice("1", Some(10)),
          BuyerTypePrice("2", Some(11))
        ),
        PipelinePriceByScale(4, 4, 4, 4, Some(10)) -> Seq(
          BuyerTypePrice("1", Some(10)),
          BuyerTypePrice("2", Some(11))
        )
      )

      val priceGroupDefs: PriceGridConfig =
        PriceGridConfig(1, Seq("1"))

      tdcDispatcher
        .getMLBAMMappings(priceMap, priceGroupDefs)
        .map(res => {
          assert(res.length == 4)
          assert(res.contains((Some(1), Some(1), Some(BigDecimal(10)))))
          assert(res.contains((Some(2), Some(2), Some(BigDecimal(10)))))
          assert(res.contains((Some(2), Some(2), Some(BigDecimal(10)))))
          assert(res.contains((None, None, Some(BigDecimal(10)))))
        })
    }
  }
}
