package com.eventdynamic.batch.ticketpricing.pipeline.evaluators

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.models.{IncrementableFeatures, ObservationGroup}
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.modelserver.ModelServerService
import com.eventdynamic.modelserver.models.{ModelServerPricePayload, ModelServerPriceResponse}
import com.softwaremill.sttp.Response
import org.scalatest.AsyncWordSpec
import org.specs2.mock.Mockito
import org.mockito.Mockito._

import scala.concurrent.Future

class ModelServerEvaluatorSpec extends AsyncWordSpec with Mockito {
  val t = new TestHelper
  val clientId = "1"

  // Create two observation groups with the same data
  val observations = Seq(t.observation(), t.observation(), t.observation(), t.observation())

  val features = IncrementableFeatures(
    timestamp = new Timestamp(0),
    inventory = 0,
    listPrice = Some(100),
    revenue = 0,
    velocity = 0
  )

  val observationGroups =
    Seq(
      ObservationGroup(observations, features.copy()),
      ObservationGroup(observations, features.copy()),
      ObservationGroup(observations, features.copy()),
      ObservationGroup(observations, features.copy(listPrice = None))
    )

  "ModelServerEvaluator#evaluate" should {
    "update prices of observation groups" in {
      val priceResponse = ModelServerPriceResponse(Seq(Some(110), Some(111), None, Some(123)))
      val response = Response(
        Right(priceResponse),
        200,
        "good",
        scala.collection.immutable.Seq.empty[(String, String)],
        List.empty[Response[Unit]]
      )
      val modelServerService = mock[ModelServerService]
      when(modelServerService.getPrices(any[String], any[ModelServerPricePayload]))
        .thenReturn(Future.successful(response))
      val evaluator = new ModelServerEvaluator(clientId, modelServerService)

      evaluator
        .evaluate(observationGroups)
        .map(observationGroups => {
          assert(observationGroups.size == 4)
          assert(observationGroups.head.features.listPrice.contains(110))
          assert(observationGroups(1).features.listPrice.contains(111))
          assert(observationGroups(2).features.listPrice.isEmpty)
          assert(observationGroups(3).features.listPrice.contains(123))
        })
    }

    "return a failure when the request fails" in {
      val modelServerService = mock[ModelServerService]
      when(modelServerService.getPrices(any[String], any[ModelServerPricePayload])).thenReturn(
        Future.successful(
          Response[ModelServerPriceResponse](
            Left("error"),
            400,
            "error",
            scala.collection.immutable.Seq.empty[(String, String)],
            List.empty[Response[Unit]]
          )
        )
      )

      val evaluator = new ModelServerEvaluator(clientId, modelServerService)

      recoverToSucceededIf[Exception](evaluator.evaluate(observationGroups))
    }

    "return a failure when the request errors" in {
      val modelServerService = mock[ModelServerService]
      when(modelServerService.getPrices(any[String], any[ModelServerPricePayload]))
        .thenReturn(Future.failed(new Exception))

      val evaluator = new ModelServerEvaluator(clientId, modelServerService)

      recoverToSucceededIf[Exception](evaluator.evaluate(observationGroups))
    }
  }
}
