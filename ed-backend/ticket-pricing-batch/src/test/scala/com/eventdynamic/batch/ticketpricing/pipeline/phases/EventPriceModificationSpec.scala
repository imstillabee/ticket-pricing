package com.eventdynamic.batch.ticketpricing.pipeline.phases

import com.eventdynamic.batch.ticketpricing.models.PipelinePrice
import com.eventdynamic.batch.ticketpricing.services.ServiceHolder
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, PrivateMethodTester}
import org.specs2.mock.Mockito

class EventPriceModificationSpec
    extends AsyncWordSpec
    with Mockito
    with BeforeAndAfterAll
    with PrivateMethodTester {

  val t = new TestHelper
  val mockedServices: ServiceHolder = t.mockedServices

  val eventPriceModificationPhase = new EventPriceModificationPhase(mockedServices)

  "calculateModifiedPipelinePrice" should {
    val mockPipelinePrice = t.pipelinePrice()
    val originalPrice = mockPipelinePrice.price
    val calculateModifiedPipelinePrice =
      PrivateMethod[PipelinePrice]('calculateModifiedPipelinePrice)

    "return a pipelinePrice with a modified price parameter (positive modifier)" in {
      val mockPercentPriceModifier = 10
      val mockPriceModifiersForPipelinePrices =
        PriceModifierForPipelinePrice(mockPipelinePrice, mockPercentPriceModifier)
      val expectedPrice = t.expectedPrice(mockPipelinePrice.price.get, mockPercentPriceModifier)

      val moddedPipelinePrice = eventPriceModificationPhase invokePrivate calculateModifiedPipelinePrice(
        mockPriceModifiersForPipelinePrices
      )

      assert(moddedPipelinePrice.price.contains(expectedPrice))
      assert(moddedPipelinePrice.price > originalPrice)
    }

    "return a pipelinePrice with a modified price parameter (negative modifier)" in {
      val mockPercentPriceModifier = -10
      val mockPriceModifiersForPipelinePrices =
        PriceModifierForPipelinePrice(mockPipelinePrice, mockPercentPriceModifier)
      val expectedPrice = t.expectedPrice(mockPipelinePrice.price.get, mockPercentPriceModifier)

      val moddedPipelinePrice = eventPriceModificationPhase invokePrivate calculateModifiedPipelinePrice(
        mockPriceModifiersForPipelinePrices
      )

      assert(moddedPipelinePrice.price.contains(expectedPrice))
      assert(moddedPipelinePrice.price < originalPrice)
    }

    "return a pipelinePrice with a modified price parameter (zero modifier)" in {
      val mockPercentPriceModifier = 0
      val mockPriceModifiersForPipelinePrices =
        PriceModifierForPipelinePrice(mockPipelinePrice, mockPercentPriceModifier)
      val expectedPrice = t.expectedPrice(mockPipelinePrice.price.get, mockPercentPriceModifier)

      val moddedPipelinePrice = eventPriceModificationPhase invokePrivate calculateModifiedPipelinePrice(
        mockPriceModifiersForPipelinePrices
      )

      assert(moddedPipelinePrice.price.contains(expectedPrice))
      assert(moddedPipelinePrice.price == originalPrice)
    }

    "ignore a pipelinePrice that has been overridden" in {
      val mockOverriddenPipelinePrice = t.pipelinePrice(overridden = true)
      val mockPriceModifierForOverridenPipelinePrice =
        PriceModifierForPipelinePrice(mockOverriddenPipelinePrice, 10)
      val moddedPipelinePrice = eventPriceModificationPhase invokePrivate calculateModifiedPipelinePrice(
        mockPriceModifierForOverridenPipelinePrice
      )

      assert(moddedPipelinePrice == mockOverriddenPipelinePrice)
    }

    "ignore a pipelinePrice that has an empty price" in {
      val mockEmptyPipelinePrice = t.pipelinePrice(price = None)
      val mockPriceModifierForEmptyPipelinePrice =
        PriceModifierForPipelinePrice(mockEmptyPipelinePrice, 10)
      val moddedPipelinePrice = eventPriceModificationPhase invokePrivate calculateModifiedPipelinePrice(
        mockPriceModifierForEmptyPipelinePrice
      )

      assert(moddedPipelinePrice == mockEmptyPipelinePrice)
    }
  }

  "calculateModifier" should {
    val calculateModifier = PrivateMethod[BigDecimal]('calculateModifier)
    "return a value greater than 1 given positive percentPriceModifier" in {
      val modifier = eventPriceModificationPhase invokePrivate calculateModifier(10)
      assert(modifier > 1)
    }

    "return a value less than 1 given negative percentPriceModifier" in {
      val modifier = eventPriceModificationPhase invokePrivate calculateModifier(-10)
      assert(modifier < 1)
    }

    "return a value equal to 1 given 0 percentPriceModifier" in {
      val modifier = eventPriceModificationPhase invokePrivate calculateModifier(0)
      assert(modifier == 1)
    }
  }
}
