package com.eventdynamic.batch.ticketpricing.pipeline.phases

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.models.Price
import com.eventdynamic.batch.ticketpricing.models.weather.ObservationWeather
import com.eventdynamic.batch.ticketpricing.pipeline.composers.FactorComposer.Factors
import com.eventdynamic.batch.ticketpricing.pipeline.composers._
import com.eventdynamic.batch.ticketpricing.pipeline.statscomposers.StatsComposer
import com.eventdynamic.batch.ticketpricing.services._
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.models.{
  Event,
  EventSeatListedPriceUpdateByScale,
  ObservationBase,
  Transaction
}
import com.eventdynamic.modelserver.models._
import com.eventdynamic.utils.DateHelper
import com.eventdynamic.utils.DateHelper.now
import org.mockito.Mockito.when
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll}
import org.specs2.mock.Mockito

import scala.concurrent.Future

class IngestionPhaseSpec extends AsyncWordSpec with Mockito with BeforeAndAfterAll {

  def mockPrices: Vector[Price] = {
    Vector(Price(1, 1, 100), Price(2, 2, 150))
  }

  def mockedEvent(id: Int, eventDate: Timestamp): Option[Event] = {
    Some(
      Event(
        id = Some(id),
        primaryEventId = Some(1),
        createdAt = now(),
        modifiedAt = now(),
        name = "",
        timestamp = Some(eventDate),
        clientId = 1,
        venueId = 1,
        eventCategoryId = 1,
        seasonId = None,
        isBroadcast = true,
        percentPriceModifier = 0,
        eventScore = None,
        eventScoreModifier = 0,
        spring = None,
        springModifier = 0
      )
    )
  }

  "Ingestion Phase" should {
    val t = new TestHelper
    val mockedServices = t.mockedServices
    val eventDate = new Timestamp(0)
    val venueId = 1
    val zip = "12345"

    val payload = ModelServerFactorPayload(
      ModelServerFactorContext(
        DateHelper.now().toString,
        MLBModelServerClientStats(1, 3, Some(2), 1.5f)
      ),
      Seq(
        ModelServerFactorExample(
          DateHelper.now().toString,
          5.5,
          2.2,
          Some(ModelServerWeather(55.3f, 48.5f, "")),
          MLBModelServerEventStats(5, 0, 0, 3, Some(3), 2, "NYY")
        )
      )
    )

    when(mockedServices.eventService.getByIntegrationId(1, 1))
      .thenReturn(Future.successful(mockedEvent(1, eventDate)))

    when(mockedServices.eventService.getByIntegrationId(2, 1))
      .thenReturn(Future.successful(mockedEvent(2, eventDate)))

    when(
      mockedServices.eventSeatService
        .updateListedPrices(any[Seq[EventSeatListedPriceUpdateByScale]])
    ).thenReturn(Future.successful(2))

    when(mockedServices.transactionService.bulkInsert(any[Seq[Transaction]]))
      .thenReturn(Future.successful(Some(2)))

    when(mockedServices.observationService.getAllForEvents(any[Seq[Int]], any[Int]))
      .thenReturn(
        Future.successful(
          t.some(
            _ =>
              t.observationBase(
                eventId = 1,
                eventDate = eventDate,
                venueId = venueId,
                venueZip = zip
            )
          )
        )
      )

    val mockedWeatherComposer = mock[WeatherComposer]

    when(mockedWeatherComposer.getWeatherMap(any[Seq[ObservationBase]]))
      .thenReturn(
        Future.successful(
          Map(WeatherKey(eventDate, venueId, zip) -> ObservationWeather(None, 70f, 800))
        )
      )

    // Mock factor composer
    val mockedFactorComposer = mock[FactorComposer]

    // Mock stats composer
    val mockedStatsComposer = mock[StatsComposer]

    val clientStats = MLBModelServerClientStats(1, 2, Some(2), 3)
    when(mockedStatsComposer.getClientStats(any[Seq[ObservationBase]]))
      .thenReturn(Future.successful(clientStats))

    val EventStatsMap = Map(now() -> MLBModelServerEventStats(1, 1, 1, 0, None, 0, "TEX"))
    when(mockedStatsComposer.getEventStatsMap(any[Seq[ObservationBase]], any[Timestamp]))
      .thenReturn(Future.successful(EventStatsMap))

    when(
      mockedFactorComposer.getPayload(
        any[Seq[ObservationBase]],
        any[Option[Map[WeatherKey, ObservationWeather]]],
        any[Timestamp],
        any[ModelServerClientStats],
        any[Map[Timestamp, ModelServerEventStats]]
      )
    ).thenReturn(payload)

    when(
      mockedFactorComposer.getFactorMap(
        any[Int],
        any[Timestamp],
        any[Seq[ObservationBase]],
        any[Option[Map[WeatherKey, ObservationWeather]]],
        any[ModelServerClientStats],
        any[Map[Timestamp, ModelServerEventStats]]
      )
    ).thenReturn(
      Future.successful(Map(1 -> Factors(10.0, 2.0, 2.0, 0.5), 2 -> Factors(11.0, 5.0, 1.9, 0.5)))
    )

    "create observations" in {
      val ingestion = new IngestionPhase(
        1,
        mockedStatsComposer,
        Some(mockedWeatherComposer),
        mockedServices.observationService,
        mockedFactorComposer,
        mock[FileStore],
        Seq(1),
        now()
      )
      ingestion
        .execute()
        .map(observations => {
          assert(observations.length == 10)
        })
    }

    "create observations for no weather data" in {
      val ingestion = new IngestionPhase(
        1,
        mockedStatsComposer,
        None,
        mockedServices.observationService,
        mockedFactorComposer,
        mock[FileStore],
        Seq(1),
        now()
      )
      ingestion
        .execute()
        .map(observations => {
          assert(observations.length == 10)
        })
    }
  }
}
