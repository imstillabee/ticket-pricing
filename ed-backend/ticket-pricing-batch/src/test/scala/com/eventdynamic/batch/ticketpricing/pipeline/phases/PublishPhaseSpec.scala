package com.eventdynamic.batch.ticketpricing.pipeline.phases
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.models.{EventSeatListedPriceUpdateByRow, EventSeatListedPriceUpdateByScale}
import org.scalatest.AsyncWordSpec

class PublishPhaseSpec extends AsyncWordSpec {
  val t = new TestHelper

  "PublishPhase#pipelinePricesToUpdates" should {
    "convert pipeline prices to scale updates" in {
      val prices = Seq(
        t.pipelinePrice(priceScaleId = 1, row = "10", price = Some(100)),
        t.pipelinePrice(priceScaleId = 1, row = "11", price = Some(100)),
        t.pipelinePrice(priceScaleId = 1, row = "11", price = None),
        t.pipelinePrice(priceScaleId = 1, row = "11", price = Some(100)),
        t.pipelinePrice(priceScaleId = 2, row = "10", price = Some(100)),
        t.pipelinePrice(priceScaleId = 3, row = "10", price = None),
        t.pipelinePrice(priceScaleId = 3, row = "11", price = None)
      )

      val updates = PublishPhase.pipelinePricesToUpdates(prices)

      assert(updates.length == 2)
      assert(updates.forall(_.isInstanceOf[EventSeatListedPriceUpdateByScale]))
    }

    "convert pipeline prices to row updates" in {
      val prices = Seq(
        t.pipelinePrice(section = "100", row = "10", price = Some(100)),
        t.pipelinePrice(section = "100", row = "11", price = Some(90)),
        t.pipelinePrice(section = "100", row = "11", price = None),
        t.pipelinePrice(section = "100", row = "11", price = Some(90)),
        t.pipelinePrice(section = "200", row = "10", price = Some(100)),
        t.pipelinePrice(section = "101", row = "11", price = None),
        t.pipelinePrice(section = "101", row = "11", price = None)
      )

      val updates = PublishPhase.pipelinePricesToUpdates(prices)

      assert(updates.length == 3)
      assert(updates.forall(_.isInstanceOf[EventSeatListedPriceUpdateByRow]))
    }

    "throw an exception when seat-level prices" in {
      val prices = Seq(
        t.pipelinePrice(row = "10", price = Some(100)),
        t.pipelinePrice(row = "10", price = Some(110)),
        t.pipelinePrice(row = "20")
      )
      assertThrows[Exception](PublishPhase.pipelinePricesToUpdates(prices))
    }
  }
}
