package com.eventdynamic.batch.ticketpricing.pipeline.phases

import java.sql.Timestamp
import com.eventdynamic.batch.ticketpricing.models.{EventSeatInfo, PipelinePrice}
import com.eventdynamic.models._
import com.eventdynamic.utils.DateHelper
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, PrivateMethodTester}
import org.specs2.mock.Mockito

class ReportingPhaseSpec
    extends AsyncWordSpec
    with Mockito
    with BeforeAndAfterAll
    with PrivateMethodTester {
  val now: Timestamp = DateHelper.now()

  val mockEventInfo: Seq[EventSeatInfo] = {
    Seq(
      EventSeatInfo(
        id = 1,
        date = now,
        name = "event1",
        section = "123",
        row = "1",
        priceScale = "front row",
        listingPrice = Some(100.00),
        isHeld = false
      ),
      EventSeatInfo(
        id = 1,
        date = now,
        name = "event1",
        section = "123",
        row = "1",
        priceScale = "front row",
        listingPrice = Some(150.00),
        isHeld = false
      ),
      EventSeatInfo(
        id = 1,
        date = now,
        name = "event1",
        section = "123",
        row = "1",
        priceScale = "front row",
        listingPrice = Some(200.00),
        isHeld = false
      ),
      EventSeatInfo(
        id = 2,
        date = now,
        name = "event2",
        section = "123",
        row = "1",
        priceScale = "front row",
        listingPrice = Some(200.00),
        isHeld = false
      ),
      EventSeatInfo(
        id = 2,
        date = now,
        name = "event2",
        section = "123",
        row = "1",
        priceScale = "front row",
        listingPrice = Some(250.00),
        isHeld = false
      ),
      EventSeatInfo(
        id = 2,
        date = now,
        name = "event2",
        section = "123",
        row = "1",
        priceScale = "front row",
        listingPrice = Some(300.00),
        isHeld = false
      )
    )
  }

  val mockPredictedPipelinePrices: Seq[PipelinePrice] = {
    Seq(
      PipelinePrice(
        eventSeatId = 1,
        eventId = 1,
        seatId = 1,
        integrationEventId = 1,
        integrationSeatId = 1,
        priceScaleId = 1,
        externalPriceScaleId = 1,
        price = Some(89.99),
        shouldPrice = true,
        overridden = false,
        section = "123",
        row = "1",
        available = true
      ),
      PipelinePrice(
        eventSeatId = 2,
        eventId = 2,
        seatId = 2,
        integrationEventId = 2,
        integrationSeatId = 2,
        priceScaleId = 2,
        externalPriceScaleId = 2,
        price = Some(300.00),
        shouldPrice = true,
        overridden = false,
        section = "123",
        row = "1",
        available = true
      )
    )
  }

  val mockPriceGuards: Seq[PipelinePrice] = {
    Seq(
      PipelinePrice(
        eventSeatId = 1,
        eventId = 1,
        seatId = 1,
        integrationEventId = 1,
        integrationSeatId = 1,
        priceScaleId = 1,
        externalPriceScaleId = 1,
        price = Some(100.00),
        shouldPrice = true,
        overridden = false,
        section = "123",
        row = "1",
        available = true
      ),
      PipelinePrice(
        eventSeatId = 2,
        eventId = 2,
        seatId = 2,
        integrationEventId = 2,
        integrationSeatId = 2,
        priceScaleId = 2,
        externalPriceScaleId = 2,
        price = Some(100.00),
        shouldPrice = true,
        overridden = false,
        section = "123",
        row = "1",
        available = true
      )
    )
  }

  val mockClientIntegrations: Seq[ClientIntegrationComposite] = {
    Seq(
      ClientIntegrationComposite(
        id = Some(1),
        createdAt = now,
        modifiedAt = now,
        clientId = 1,
        integrationId = 1,
        name = "Client 1",
        version = None,
        json = None,
        isActive = true,
        isPrimary = true,
        logoUrl = None,
        percent = None,
        constant = None
      ),
      ClientIntegrationComposite(
        id = Some(2),
        createdAt = now,
        modifiedAt = now,
        clientId = 1,
        integrationId = 2,
        name = "Client 1",
        version = None,
        json = None,
        isActive = true,
        isPrimary = false,
        None,
        None,
        None
      ),
      ClientIntegrationComposite(
        id = Some(3),
        createdAt = now,
        modifiedAt = now,
        clientId = 1,
        integrationId = 3,
        name = "Client 1",
        version = None,
        json = None,
        isActive = true,
        isPrimary = false,
        logoUrl = None,
        percent = None,
        constant = None
      )
    )
  }

  val reportingPhase = new ReportingPhase(
    mockEventInfo,
    mockPredictedPipelinePrices,
    mockPriceGuards,
    mockClientIntegrations
  )

  "createMaps" should {
    val createMaps =
      PrivateMethod[(Map[EventSeatKey, Option[BigDecimal]], Map[EventSeatKey, Option[BigDecimal]])](
        'createMaps
      )
    "return tuple of Maps with (eventId, section, row) as a key" in {
      val (predictedPriceMap, priceGuardMap) = reportingPhase invokePrivate createMaps(
        mockPredictedPipelinePrices,
        mockPriceGuards
      )
      assert(
        predictedPriceMap == Map(
          EventSeatKey(eventId = 1, section = "123", row = "1") -> Some(BigDecimal(89.99)),
          EventSeatKey(eventId = 2, section = "123", row = "1") -> Some(BigDecimal(300.00))
        )
      )
      assert(
        priceGuardMap == Map(
          EventSeatKey(eventId = 1, section = "123", row = "1") -> Some(BigDecimal(100.00)),
          EventSeatKey(eventId = 2, section = "123", row = "1") -> Some(BigDecimal(100.00))
        )
      )
    }

    "update values for duplicate keys" in {
      val (predictedPriceMap, priceGuardMap) = reportingPhase invokePrivate createMaps(
        mockPredictedPipelinePrices ++ Seq(
          PipelinePrice(
            eventSeatId = 1,
            eventId = 1,
            seatId = 1,
            integrationEventId = 1,
            integrationSeatId = 1,
            priceScaleId = 1,
            externalPriceScaleId = 1,
            price = Some(1000.00),
            shouldPrice = true,
            overridden = false,
            section = "123",
            row = "1",
            available = true
          ),
          PipelinePrice(
            eventSeatId = 2,
            eventId = 2,
            seatId = 2,
            integrationEventId = 2,
            integrationSeatId = 2,
            priceScaleId = 2,
            externalPriceScaleId = 2,
            price = Some(3000.00),
            shouldPrice = true,
            overridden = false,
            section = "123",
            row = "1",
            available = true
          )
        ),
        mockPriceGuards ++ Seq(
          PipelinePrice(
            eventSeatId = 1,
            eventId = 1,
            seatId = 1,
            integrationEventId = 1,
            integrationSeatId = 1,
            priceScaleId = 1,
            externalPriceScaleId = 1,
            price = Some(150.00),
            shouldPrice = true,
            overridden = false,
            section = "123",
            row = "1",
            available = true
          ),
          PipelinePrice(
            eventSeatId = 2,
            eventId = 2,
            seatId = 2,
            integrationEventId = 2,
            integrationSeatId = 2,
            priceScaleId = 2,
            externalPriceScaleId = 2,
            price = Some(200.00),
            shouldPrice = true,
            overridden = false,
            section = "123",
            row = "1",
            available = true
          )
        )
      )
      assert(
        predictedPriceMap == Map(
          EventSeatKey(eventId = 1, section = "123", row = "1") -> Some(BigDecimal(1000.00)),
          EventSeatKey(eventId = 2, section = "123", row = "1") -> Some(BigDecimal(3000.00))
        )
      )
      assert(
        priceGuardMap == Map(
          EventSeatKey(eventId = 1, section = "123", row = "1") -> Some(BigDecimal(150.00)),
          EventSeatKey(eventId = 2, section = "123", row = "1") -> Some(BigDecimal(200.00))
        )
      )
    }
  }

  "createRows" should {
    val createRows = PrivateMethod[Seq[Seq[String]]]('createRows)

    val createMaps =
      PrivateMethod[(Map[EventSeatKey, BigDecimal], Map[EventSeatKey, BigDecimal])]('createMaps)
    val (predictedPriceMap, priceGuardMap) = reportingPhase invokePrivate createMaps(
      mockPredictedPipelinePrices,
      mockPriceGuards
    )
    "create only the same amount of rows as the number of unique events" in {
      val rows = reportingPhase invokePrivate createRows(
        mockEventInfo,
        predictedPriceMap,
        priceGuardMap,
        mockClientIntegrations
      )
      assert(rows.length == 2)
    }
  }

  "getPercentageDifference" should {
    val percentageDifference = PrivateMethod[String]('getPercentageDifference)
    "return a positive value if the enforced price is greater than predicted price" in {
      val pDiff = reportingPhase invokePrivate percentageDifference(
        Some(BigDecimal(100.00)),
        BigDecimal(80.00)
      )
      assert(pDiff.contains("25"))
    }

    "return a negative value if the enforced price is less than predicted price" in {
      val pDiff = reportingPhase invokePrivate percentageDifference(
        Some(BigDecimal(80.00)),
        BigDecimal(100.00)
      )
      assert(pDiff.contains("-20"))
    }

    "return 0 if the enforced price is equal to predicted price" in {
      val pDiff = reportingPhase invokePrivate percentageDifference(
        Some(BigDecimal(100.00)),
        BigDecimal(100.00)
      )
      assert(pDiff.contains("0"))
    }

    "return blank if predicted price is 0" in {
      val pDiff = reportingPhase invokePrivate percentageDifference(
        Some(BigDecimal(100.00)),
        BigDecimal(0)
      )
      assert(pDiff == "")
    }

    "return blank if predicted price is empty" in {
      val pDiff = reportingPhase invokePrivate percentageDifference(None, BigDecimal(123))
      assert(pDiff == "")
    }
  }

  "getPredictedAndFlooredPrices" should {
    val getPredictedAndFlooredPrices =
      PrivateMethod[(Option[BigDecimal], Option[BigDecimal])]('getPredictedAndFlooredPrices)
    val createMaps =
      PrivateMethod[(Map[EventSeatKey, Option[BigDecimal]], Map[EventSeatKey, Option[BigDecimal]])](
        'createMaps
      )
    val (predictedPriceMap, priceGuardMap) = reportingPhase invokePrivate createMaps(
      mockPredictedPipelinePrices,
      mockPriceGuards
    )
    "return a tuple of the predicted and enforced price if a key is found for an event" in {
      val (predictedPrice, flooredPrice) = reportingPhase invokePrivate getPredictedAndFlooredPrices(
        mockEventInfo.head,
        predictedPriceMap,
        priceGuardMap
      )
      assert(predictedPrice.contains(89.99)) //values contained in mocks
      assert(flooredPrice.contains(100.00))
    }
  }

  "createHeaders" should {
    val createHeaders = PrivateMethod[Seq[String]]('createHeaders)
    "return seq of headers equal to 10 plus the number of client integrations" in {
      val headers = reportingPhase invokePrivate createHeaders(mockClientIntegrations)
      // 10 is the default amount of headers for a report excluding client integrations
      assert(headers.length == mockClientIntegrations.length + reportingPhase.defaultHeaders.length)
    }
  }
}
