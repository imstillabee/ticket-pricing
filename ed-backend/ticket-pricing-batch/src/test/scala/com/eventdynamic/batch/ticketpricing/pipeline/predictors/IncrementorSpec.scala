package com.eventdynamic.batch.ticketpricing.pipeline.predictors

import java.sql.Timestamp
import java.time.{Instant, ZoneId}

import com.eventdynamic.batch.ticketpricing.models.{EventObservations, ObservationGroup}
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import org.scalatest.WordSpec

class IncrementorSpec extends WordSpec {
  val t = new TestHelper

  "Incrementor#increment" should {
    "increment time and remove past events" in {
      val jobStart = Timestamp.from(Instant.parse("2018-01-01T12:00:00Z"))
      val nextTime = Timestamp.from(Instant.parse("2018-01-01T12:15:00Z"))
      val t1 = Instant.parse("2018-01-01T12:10:00Z").atZone(ZoneId.systemDefault())
      val t2 = Instant.parse("2018-01-01T12:20:00Z").atZone(ZoneId.systemDefault())

      val og1 =
        ObservationGroup.fromObservations(
          t.some(_ => t.observation(eventDate = t1, jobStart = jobStart))
        )
      val og2 =
        ObservationGroup.fromObservations(
          t.some(_ => t.observation(eventDate = t2, jobStart = jobStart))
        )

      val eo1 = EventObservations(1, Seq(og1))
      val eo2 = EventObservations(2, Seq(og2))

      val eventObservations = Incrementor.increment(Seq(eo1, eo2))

      assert(eventObservations.size === 1)
      assert(eventObservations.head.eventId === 2)
      assert(eventObservations.head.observationGroups.head.features.timestamp === nextTime)
    }
  }
}
