package com.eventdynamic.batch.ticketpricing.pipeline.predictors

import com.eventdynamic.batch.ticketpricing.models.ObservationGroup
import com.eventdynamic.batch.ticketpricing.pipeline.strategies.pricing.PricingStrategy
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import org.scalatest.WordSpec

import scala.concurrent.ExecutionContext.Implicits.global

class RowPredictorSpec extends WordSpec {
  val t = new TestHelper

  // Use a pricing strategy with a constant price
  val PRICE = Option(BigDecimal(10))

  val pricingStrategy: PricingStrategy = (observationGroups: Seq[ObservationGroup]) =>
    observationGroups.foreach(_.features.listPrice = PRICE)

  val emptyPricingStrategy: PricingStrategy = (observationGroups: Seq[ObservationGroup]) =>
    observationGroups.foreach(_.features.listPrice = None)

  // Row predictor with constant strategies
  val rowPredictor = new RowPredictor(pricingStrategy)
  val emptyRowPredictor = new RowPredictor(emptyPricingStrategy)

  // Use a pricing strategy with a constant price
  val ROUNDING_PRICE = Option(BigDecimal(10.25))
  val EXPECTED_ROUNDING_PRICE = 11

  val roundedPricingStrategy: PricingStrategy = (observationGroups: Seq[ObservationGroup]) =>
    observationGroups.foreach(_.features.listPrice = ROUNDING_PRICE)

  // Row predictor with constant strategies
  val roundingRowPredictor = new RowPredictor(roundedPricingStrategy)

  "RowPredictor#predict" should {
    "generate new prices for a single row" in {
      val observations =
        t.some(_ => t.observation(listPrice = Some(5), section = "100", row = "1", eventId = 1))

      val pipelinePrices = rowPredictor.predict(observations)

      assert(pipelinePrices.size === 10)
      assert(pipelinePrices.forall(_.price === PRICE))
    }

    "generate new prices for a row in different events" in {
      val observations1 =
        t.some(_ => t.observation(listPrice = Some(5), section = "100", row = "1", eventId = 1))
      val observations2 =
        t.some(_ => t.observation(listPrice = Some(5), section = "100", row = "1", eventId = 2))

      val pipelinePrices = rowPredictor.predict(observations1 ++ observations2)

      assert(pipelinePrices.size === 20)
      assert(pipelinePrices.forall(_.price === PRICE))
    }

    "round up to the nearest dollar for a given amount" in {
      val observations =
        t.some(_ => t.observation(listPrice = Some(5), section = "100", row = "1", eventId = 1))

      val pipelinePrices = roundingRowPredictor.predict(observations)

      assert(pipelinePrices.size === 10)
      assert(pipelinePrices.forall(_.price.contains(EXPECTED_ROUNDING_PRICE)))
    }

    "not generate new prices when a single seat is toggled" in {
      val observations =
        t.some(_ => t.observation(listPrice = Some(5), section = "100", row = "1", eventId = 1))
      val notListed = observations.head.copy(isListed = false)

      val pipelinePrices = rowPredictor.predict(observations :+ notListed)

      assert(pipelinePrices.size === 11)
      assert(pipelinePrices.forall(_.price.contains(5)))
      assert(pipelinePrices.count(!_.shouldPrice) === 1)
    }

    "not generate new prices when an event is toggled" in {
      val observations1 =
        t.some(_ => t.observation(listPrice = Some(5), section = "100", row = "1", eventId = 1))
      val observations2 =
        t.some(
          _ =>
            t.observation(
              listPrice = Some(5),
              section = "100",
              row = "1",
              eventId = 2,
              isEventListed = false
          )
        )

      val pipelinePrices = rowPredictor.predict(observations1 ++ observations2)

      assert(pipelinePrices.size === 20)
      assert(pipelinePrices.filter(_.eventId == 1).forall(_.price === PRICE))
      assert(pipelinePrices.filter(_.eventId == 2).forall(_.price.contains(5)))
      assert(pipelinePrices.filter(_.eventId == 2).forall(!_.shouldPrice))
    }

    "use override prices" in {
      val observations =
        t.some(_ => t.observation(listPrice = Some(5), section = "100", row = "1", eventId = 1))
      val overrideObs1 = observations.head.copy(overridePrice = Some(100))
      val overrideObs2 = observations.head.copy(overridePrice = Some(50))

      val pipelinePrices =
        rowPredictor.predict(observations :+ overrideObs1 :+ overrideObs2)

      assert(pipelinePrices.size === 12)
      assert(pipelinePrices.forall(_.price.contains(100)))
    }

    "handle incoming empty listed prices" in {
      val observations =
        t.some(_ => t.observation(listPrice = None, section = "100", row = "1", eventId = 1))

      val pipelinePrices = rowPredictor.predict(observations)

      assert(pipelinePrices.size === 10)
      assert(pipelinePrices.forall(_.price == PRICE))
    }

    "handle empty prices from pricing strategy" in {
      val pricedObservations =
        t.some(_ => t.observation(listPrice = Some(10), section = "100", row = "1", eventId = 1))
      val emptyObservations =
        t.some(_ => t.observation(listPrice = None, section = "100", row = "1", eventId = 1))

      val pipelinePrices = emptyRowPredictor.predict(pricedObservations ++ emptyObservations)

      assert(pipelinePrices.size === 20)
      assert(pipelinePrices.forall(_.price.isEmpty))
    }

    "should mark held event seats in observations as unavailable" in {
      val heldObservations = t.some(_ => t.observation(isHeld = true))

      val pipelinePrices = rowPredictor.predict(heldObservations)
      assert(!pipelinePrices.head.available)
    }

    "should mark held event seats with no transactions in observations as available" in {
      val heldObservations = t.some(_ => t.observation(isHeld = false))

      val pipelinePrices = rowPredictor.predict(heldObservations)
      assert(pipelinePrices.head.available)
    }
  }
}
