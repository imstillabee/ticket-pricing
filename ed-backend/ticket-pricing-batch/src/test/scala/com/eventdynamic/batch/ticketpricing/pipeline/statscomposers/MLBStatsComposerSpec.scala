package com.eventdynamic.batch.ticketpricing.pipeline.statscomposers

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.models.weather.ObservationWeather
import com.eventdynamic.batch.ticketpricing.pipeline.composers.WeatherKey
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.models.mlb.MySportsFeedInfo
import com.eventdynamic.models.{ObservationBase, Season, TeamStat}
import com.eventdynamic.mysportsfeed.models.mlb._
import com.eventdynamic.utils.DateHelper
import org.mockito.Mockito.{verify, when}
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll}
import org.specs2.mock.Mockito

import scala.concurrent.Future

class MLBStatsComposerSpec extends AsyncWordSpec with Mockito with BeforeAndAfterAll {

  val t = new TestHelper
  val mockedServiceHolder = t.mockedServices

  val mockedMLB = mock[mockedServiceHolder.mySportsFeedService.MLB.type]

  val mockMLBStats = MLBStats("NYM", None, mockGameLogs, mockTeamSchedule)

  val mlbStatsComposer =
    new MLBStatsComposer(mockedServiceHolder, 1)

  def mockTeamInfo: Option[MySportsFeedInfo] = {
    return Some(MySportsFeedInfo(Some(1), DateHelper.now(), DateHelper.now(), 1, "NYM"))
  }

  def mockTeamStat: TeamStat = {
    return TeamStat(Some(1), DateHelper.now(), DateHelper.now(), 1, 1, 0, 0, 162)
  }

  def mockDivision(rank: String): Option[Seq[Division]] = {
    return Some(
      Seq(
        Division(
          "American League/East",
          Seq(
            TeamStandings("NYY", rank, "20", "10", "0"),
            TeamStandings("NYM", rank, "20", "10", "3.5")
          )
        )
      )
    )
  }

  def mockGameLogs: Option[Seq[GameLogs]] = {
    return Some(
      Seq(GameLogs("20180602", "07:00PM", "NYY", "1"), GameLogs("20180603", "07:00PM", "NYY", "1"))
    )
  }

  def mockTeamSchedule: Option[Seq[GameEntry]] = {
    return Some(
      Seq(
        GameEntry("20180602", "07:00PM", "NYY", "NYM"),
        GameEntry("20180602", "07:00PM", "NYY", "NYM")
      )
    )
  }

  def mockSeason: Season = {
    return Season(
      Some(1),
      Some("TeamName"),
      1,
      Some(DateHelper.now()),
      Some(DateHelper.later(100000))
    )
  }

  def mockObservation(
    zip: String = "12345",
    venueId: Int = 1,
    time: Timestamp = Timestamp.valueOf("2018-01-01 05:00:00.0")
  ): ObservationBase =
    ObservationBase(
      seatId = 1,
      eventId = 1,
      eventName = "game1",
      integrationEventId = 1,
      integrationSeatId = 1,
      venueId = venueId,
      venueZip = zip,
      venueTimeZone = "America/New_York",
      eventDate = time,
      seat = "seat",
      row = "row",
      section = "section",
      eventSeatId = 1,
      listPrice = Some(12.5),
      overridePrice = None,
      priceScaleId = 1,
      externalPriceScaleId = 1,
      priceScale = "scale",
      soldPrice = Some(12.25),
      revenueCategoryId = None,
      buyerTypeCode = None,
      integrationId = None,
      transactionType = None,
      isListed = true,
      isEventListed = true,
      eventScoreModifier = 0.0,
      springModifier = 0.0,
      isHeld = false
    )

  def mockWeatherMap(
    zip: String = "12345",
    venueId: Int = 1
  ): Map[WeatherKey, ObservationWeather] = {
    Map(
      WeatherKey(Timestamp.valueOf("2018-01-01 05:00:00.0"), venueId, zip) -> ObservationWeather(
        None,
        70f,
        800
      )
    )
  }

  override def beforeAll(): Unit = {
    super.beforeAll()

    when(mockedServiceHolder.teamAbbreviationService.getByClientId(any[Int]))
      .thenReturn(Future.successful(mockTeamInfo))
    when(mockedServiceHolder.seasonService.getCurrent(any[Option[Int]], any[Boolean]))
      .thenReturn(Future.successful(Some(mockSeason)))
    when(mockedServiceHolder.teamStatService.getByClientIdAndSeasonId(any[Int], any[Int]))
      .thenReturn(Future.successful(Some(mockTeamStat)))
    when(mockedServiceHolder.mySportsFeedService.MLB).thenReturn(mockedMLB)
    when(mockedMLB.getTeamStandings(any[String]))
      .thenReturn(Future { None })
    when(mockedMLB.getTeamGameLogs(any[String]))
      .thenReturn(Future.successful(mockGameLogs))
    when(mockedMLB.getTeamFullSchedule(any[String]))
      .thenReturn(Future.successful(mockTeamSchedule))
  }

  "MLBStatsComposer" should {
    "build MLB stats" in {
      for {
        mlbStats <- MLBStatsComposer.getStats(mockedServiceHolder, 1)
      } yield {
        assert(MLBStats("NYM", None, mockGameLogs, mockTeamSchedule) == mlbStats)
      }
    }

    "update team statistics" in {
      MLBStatsComposer
        .updateTeamStatistics(
          1,
          mockedServiceHolder.seasonService,
          mockedServiceHolder.teamStatService,
          TeamStandings("NYY", "2", "20", "10", "0")
        )
        .map(response => {
          verify(mockedServiceHolder.teamStatService).getByClientIdAndSeasonId(1, 1)
          verify(mockedServiceHolder.teamStatService).update(any[TeamStat])
          assert(response.isInstanceOf[Any])
        })
    }

    "get MLB Team Stats for a client by teamAbbreviation" in {
      when(mockedServiceHolder.mySportsFeedService.MLB.getTeamStandings(any[String]))
        .thenReturn(Future.successful(mockDivision("2")))

      for {
        mlbStats <- MLBStatsComposer.getMLBTeamStats(mockedServiceHolder, 1, "NYM", None)
      } yield {
        assert(mlbStats.get == MLBTeamStats(2, 20, 10, -3.5f, 0))
      }
    }

    "get MLB Team Stats for a client and calculate gamesAhead when in 1st" in {
      when(mockedServiceHolder.mySportsFeedService.MLB.getTeamStandings(any[String]))
        .thenReturn(Future.successful(mockDivision("1")))

      for {
        mlbStats <- MLBStatsComposer.getMLBTeamStats(mockedServiceHolder, 1, "NYY", None)
      } yield {
        assert(mlbStats.get == MLBTeamStats(1, 20, 10, 3.5f, 0))
      }
    }

    "get Game Stats map" in {
      val stat = MLBGameStats(1, "NYY", false, true)
      when(
        mockedServiceHolder.mySportsFeedService.MLB.calculateGameStats(any[MLBStats], any[String])
      ).thenReturn(stat)
      val res =
        mlbStatsComposer.getGameStatsMap(Seq(mockObservation()), mockMLBStats)

      assert(res.values.head == stat)
    }
  }
}
