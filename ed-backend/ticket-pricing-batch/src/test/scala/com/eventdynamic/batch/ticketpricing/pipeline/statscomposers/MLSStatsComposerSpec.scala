package com.eventdynamic.batch.ticketpricing.pipeline.statscomposers

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.models._
import com.eventdynamic.modelserver.models.{MLSModelServerClientStats, MLSModelServerEventStats}
import com.eventdynamic.utils.DateHelper.now
import org.mockito.Mockito.when
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll}
import org.specs2.mock.Mockito

import scala.concurrent.Future

class MLSStatsComposerSpec extends AsyncWordSpec with Mockito with BeforeAndAfterAll {
  import MLSStatsComposerSpec._

  val t = new TestHelper
  val mockedServiceHolder = t.mockedServices

  val clientId = 1
  val timestamp = new Timestamp(0)
  val eventDate1 = new Timestamp(100)
  val eventDate2 = new Timestamp(200)

  val observations =
    Seq(
      mockObservation(2, eventDate2),
      mockObservation(1, eventDate1),
      mockObservation(1, eventDate1)
    )

  val mockedEvent = mockEvent(1, "Event 1", Some(1))
  val mockedMLSSeasonStats = mockMLSSeasonStat(1, mockedEvent.seasonId.get)
  val mockedMLSEventStats = mockMLSEventStat(1, mockedEvent.id.get)

  override def beforeAll(): Unit = {
    super.beforeAll()
    when(mockedServiceHolder.eventService.getById(any[Int]))
      .thenReturn(Future.successful(Some(mockedEvent)))
    when(mockedServiceHolder.mlsSeasonStatService.getBySeasonId(any[Int]))
      .thenReturn(Future.successful(Some(mockedMLSSeasonStats)))
    when(
      mockedServiceHolder.mlsEventStatService
        .getByEventId(any[Int])
    ).thenReturn(Future.successful(Some(mockedMLSEventStats)))
  }

  "MLSStatsComposer" should {
    "return client stats" in {
      val mlsStatsComposer = new MLSStatsComposer(mockedServiceHolder, clientId)

      for {
        clientStats <- mlsStatsComposer.getClientStats(observations)
      } yield {
        assert(
          clientStats ==
            MLSModelServerClientStats(
              wins = mockedMLSSeasonStats.wins,
              losses = mockedMLSSeasonStats.losses,
              ties = mockedMLSSeasonStats.ties
            )
        )
      }
    }

    "throws exception if mls season stats are not found" in {
      when(mockedServiceHolder.mlsSeasonStatService.getBySeasonId(any[Int]))
        .thenReturn(Future.successful(None))
      val mlsStatsComposer = new MLSStatsComposer(mockedServiceHolder, clientId)

      mlsStatsComposer
        .getClientStats(observations)
        .map(_ => fail())
        .recover {
          case ex: Exception =>
            assert(
              ex.getMessage == s"Season ${mockedEvent.seasonId.get} does not have MLS Season stats"
            )
        }
    }

    "throws exception if event is not found" in {
      when(mockedServiceHolder.eventService.getById(any[Int]))
        .thenReturn(Future.successful(None))
      val mlsStatsComposer = new MLSStatsComposer(mockedServiceHolder, clientId)

      mlsStatsComposer
        .getClientStats(observations)
        .map(_ => fail())
        .recover {
          case ex: Exception =>
            assert(ex.getMessage == s"Event ${observations.head.eventId} does not exist")
        }
    }

    "return event stats map" in {
      val mlsStatsComposer = new MLSStatsComposer(mockedServiceHolder, clientId)

      for {
        eventStatsMap <- mlsStatsComposer.getEventStatsMap(observations, timestamp)
      } yield {
        assert(
          eventStatsMap == Map(
            eventDate1 -> MLSModelServerEventStats("NYM", 4),
            eventDate2 -> MLSModelServerEventStats("NYM", 4)
          )
        )
      }
    }

    "return empty event stats map if there are no mls event stats" in {
      when(
        mockedServiceHolder.mlsEventStatService
          .getByEventId(any[Int])
      ).thenReturn(Future.successful(None))
      val mlsStatsComposer = new MLSStatsComposer(mockedServiceHolder, clientId)

      mlsStatsComposer
        .getEventStatsMap(observations, timestamp)
        .map(_ => fail())
        .recover {
          case ex: Exception =>
            assert(
              ex.getMessage == s"Event ${observations.head.eventId} does not have MLS Event Stats"
            )
        }
    }
  }
}

object MLSStatsComposerSpec {

  def mockObservation(eventId: Int, eventDate: Timestamp): ObservationBase =
    ObservationBase(
      seatId = 1,
      eventId = eventId,
      eventName = "game1",
      integrationEventId = 1,
      integrationSeatId = 1,
      venueId = 1,
      venueZip = "12345",
      venueTimeZone = "America/New_York",
      eventDate = eventDate,
      seat = "seat",
      row = "row",
      section = "section",
      eventSeatId = 1,
      listPrice = Some(12.5),
      overridePrice = None,
      priceScaleId = 1,
      externalPriceScaleId = 1,
      priceScale = "scale",
      soldPrice = Some(12.25),
      revenueCategoryId = None,
      buyerTypeCode = None,
      integrationId = None,
      transactionType = None,
      isListed = true,
      isEventListed = true,
      eventScoreModifier = 0.0,
      springModifier = 0.0,
      isHeld = false
    )

  def mockEvent(id: Int, name: String, seasonId: Option[Int] = None): Event = {
    Event(
      id = Some(id),
      primaryEventId = Some(1),
      createdAt = now(),
      modifiedAt = now(),
      name = name,
      timestamp = Option(now()),
      clientId = 1,
      venueId = 1,
      eventCategoryId = 1,
      seasonId = seasonId,
      isBroadcast = true,
      percentPriceModifier = 0,
      eventScore = None,
      eventScoreModifier = 0,
      spring = None,
      springModifier = 0
    )
  }

  def mockMLSSeasonStat(id: Int, seasonId: Int): MLSSeasonStat = {
    MLSSeasonStat(
      id = Some(id),
      createdAt = now(),
      modifiedAt = now(),
      seasonId = seasonId,
      wins = 10,
      losses = 5,
      ties = 0,
      gamesTotal = 38
    )
  }

  def mockMLSEventStat(id: Int, eventId: Int) = {
    MLSEventStat(
      id = Some(id),
      createdAt = now(),
      modifiedAt = now(),
      eventId = eventId,
      opponent = "NYM",
      homeGameNumber = 4
    )
  }
}
