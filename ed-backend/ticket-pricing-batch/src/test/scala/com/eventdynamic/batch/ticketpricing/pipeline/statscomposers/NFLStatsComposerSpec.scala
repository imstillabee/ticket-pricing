package com.eventdynamic.batch.ticketpricing.pipeline.statscomposers

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.models.ClientPerformanceType.ClientPerformanceType
import com.eventdynamic.models._
import com.eventdynamic.modelserver.models.{NFLModelServerClientStats, NFLModelServerEventStats}
import com.eventdynamic.utils.DateHelper.now
import org.mockito.Mockito.when
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll}
import org.specs2.mock.Mockito

import scala.concurrent.Future

class NFLStatsComposerSpec extends AsyncWordSpec with Mockito with BeforeAndAfterAll {
  import NFLStatsComposerSpec._

  val t = new TestHelper
  val mockedServiceHolder = t.mockedServices

  val clientId = 1
  val timestamp = new Timestamp(0)
  val eventDate1 = new Timestamp(100)
  val eventDate2 = new Timestamp(200)

  val observations =
    Seq(
      mockObservation(2, eventDate2),
      mockObservation(1, eventDate1),
      mockObservation(1, eventDate1)
    )

  val mockedEvent = mockEvent(1, "Event 1", Some(1))
  val mockedNFLSeasonStats = mockNFLSeasonStat(1, mockedEvent.seasonId.get)
  val mockedNFLEventStats = mockNFLEventStat(1, mockedEvent.id.get)

  override def beforeAll(): Unit = {
    super.beforeAll()
    when(mockedServiceHolder.eventService.getById(any[Int]))
      .thenReturn(Future.successful(Some(mockedEvent)))
    when(mockedServiceHolder.nflSeasonStatService.getBySeasonId(any[Int]))
      .thenReturn(Future.successful(Some(mockedNFLSeasonStats)))
    when(
      mockedServiceHolder.nflEventStatService
        .getByEventId(any[Int])
    ).thenReturn(Future.successful(Some(mockedNFLEventStats)))
  }

  "NFLStatsComposer" should {
    "return client stats" in {
      val nflStatsComposer = new NFLStatsComposer(mockedServiceHolder, clientId)

      for {
        clientStats <- nflStatsComposer.getClientStats(observations)
      } yield {
        assert(
          clientStats ==
            NFLModelServerClientStats(
              wins = mockedNFLSeasonStats.wins,
              losses = mockedNFLSeasonStats.losses,
              ties = mockedNFLSeasonStats.ties
            )
        )
      }
    }

    "throws exception if nfl season stats are not found" in {
      when(mockedServiceHolder.nflSeasonStatService.getBySeasonId(any[Int]))
        .thenReturn(Future.successful(None))
      val nflStatsComposer = new NFLStatsComposer(mockedServiceHolder, clientId)

      nflStatsComposer
        .getClientStats(observations)
        .map(_ => fail())
        .recover {
          case ex: Exception =>
            assert(
              ex.getMessage == s"Season ${mockedEvent.seasonId.get} does not have NFL Season stats"
            )
        }
    }

    "throws exception if event is not found" in {
      when(mockedServiceHolder.eventService.getById(any[Int]))
        .thenReturn(Future.successful(None))
      val nflStatsComposer = new NFLStatsComposer(mockedServiceHolder, clientId)

      nflStatsComposer
        .getClientStats(observations)
        .map(_ => fail())
        .recover {
          case ex: Exception =>
            assert(ex.getMessage == s"Event ${observations.head.eventId} does not exist")
        }
    }

    "return event stats map" in {
      val nflStatsComposer = new NFLStatsComposer(mockedServiceHolder, clientId)

      for {
        eventStatsMap <- nflStatsComposer.getEventStatsMap(observations, timestamp)
      } yield {
        assert(
          eventStatsMap == Map(
            eventDate1 -> NFLModelServerEventStats("NYM", 0),
            eventDate2 -> NFLModelServerEventStats("NYM", 0)
          )
        )
      }
    }

    "return empty event stats map if there are no nfl event stats" in {
      when(
        mockedServiceHolder.nflEventStatService
          .getByEventId(any[Int])
      ).thenReturn(Future.successful(None))
      val nflStatsComposer = new NFLStatsComposer(mockedServiceHolder, clientId)

      nflStatsComposer
        .getEventStatsMap(observations, timestamp)
        .map(_ => fail())
        .recover {
          case ex: Exception =>
            assert(
              ex.getMessage == s"Event ${observations.head.eventId} does not have NFL Event Stats"
            )
        }
    }
  }
}

object NFLStatsComposerSpec {

  def mockObservation(eventId: Int, eventDate: Timestamp): ObservationBase =
    ObservationBase(
      seatId = 1,
      eventId = eventId,
      eventName = "game1",
      integrationEventId = 1,
      integrationSeatId = 1,
      venueId = 1,
      venueZip = "12345",
      venueTimeZone = "America/New_York",
      eventDate = eventDate,
      seat = "seat",
      row = "row",
      section = "section",
      eventSeatId = 1,
      listPrice = Some(12.5),
      overridePrice = None,
      priceScaleId = 1,
      externalPriceScaleId = 1,
      priceScale = "scale",
      soldPrice = Some(12.25),
      revenueCategoryId = None,
      buyerTypeCode = None,
      integrationId = None,
      transactionType = None,
      isListed = true,
      isEventListed = true,
      eventScoreModifier = 0.0,
      springModifier = 0.0,
      isHeld = false
    )

  def mockEvent(id: Int, name: String, seasonId: Option[Int] = None): Event = {
    Event(
      id = Some(id),
      primaryEventId = Some(1),
      createdAt = now(),
      modifiedAt = now(),
      name = name,
      timestamp = Option(now()),
      clientId = 1,
      venueId = 1,
      eventCategoryId = 1,
      seasonId = seasonId,
      isBroadcast = true,
      percentPriceModifier = 0,
      eventScore = None,
      eventScoreModifier = 0,
      spring = None,
      springModifier = 0
    )
  }

  def mockNFLSeasonStat(id: Int, seasonId: Int): NFLSeasonStat = {
    NFLSeasonStat(
      id = Some(id),
      createdAt = now(),
      modifiedAt = now(),
      clientId = 1,
      seasonId = seasonId,
      wins = 3,
      losses = 1,
      ties = 0,
      gamesTotal = 16
    )
  }

  def mockNFLEventStat(id: Int, eventId: Int) = {
    NFLEventStat(
      id = Some(id),
      createdAt = now(),
      modifiedAt = now(),
      clientId = 1,
      eventId = eventId,
      opponent = "NYM",
      isHomeOpener = false,
      isPreSeason = false
    )
  }
}
