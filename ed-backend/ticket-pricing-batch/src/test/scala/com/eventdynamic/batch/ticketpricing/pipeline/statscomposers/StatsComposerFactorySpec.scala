package com.eventdynamic.batch.ticketpricing.pipeline.statscomposers

import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import com.eventdynamic.models.ClientPerformanceType.ClientPerformanceType
import com.eventdynamic.models.{Client, ClientPerformanceType}
import com.eventdynamic.utils.DateHelper
import org.mockito.Mockito.when
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll}
import org.specs2.mock.Mockito

import scala.concurrent.Future

class StatsComposerFactorySpec extends AsyncWordSpec with Mockito with BeforeAndAfterAll {

  val t = new TestHelper
  val mockedServiceHolder = t.mockedServices

  val clientId = 1
  val timestamp = DateHelper.now()

  val mlbClient = mockClient(performanceType = ClientPerformanceType.MLB)
  val nflClient = mockClient(performanceType = ClientPerformanceType.NFL)
  val mlsClient = mockClient(performanceType = ClientPerformanceType.MLS)

  def mockClient(performanceType: ClientPerformanceType = ClientPerformanceType.MLB): Client = {
    new Client(
      Some(clientId),
      timestamp,
      timestamp,
      0,
      "MLBClient",
      None,
      5.0,
      None,
      1.2,
      None,
      performanceType
    )
  }

  override def beforeAll(): Unit = {
    super.beforeAll()
  }

  "StatsComposerFactory" should {
    "return MLBStatsComposer for an MLB client" in {
      when(mockedServiceHolder.clientService.getById(any[Int]))
        .thenReturn(Future.successful(Some(mlbClient)))

      StatsComposerFactory
        .getStatsComposerByClientId(clientId, mockedServiceHolder)
        .map(statsComposer => assert(statsComposer.isInstanceOf[MLBStatsComposer]))
    }

    "return NFLStatsComposer for an NFL client" in {
      when(mockedServiceHolder.clientService.getById(any[Int]))
        .thenReturn(Future.successful(Some(nflClient)))

      StatsComposerFactory
        .getStatsComposerByClientId(clientId, mockedServiceHolder)
        .map(statsComposer => assert(statsComposer.isInstanceOf[NFLStatsComposer]))
    }

    "return MLSStatsComposer for an MLS client" in {
      when(mockedServiceHolder.clientService.getById(any[Int]))
        .thenReturn(Future.successful(Some(mlsClient)))

      StatsComposerFactory
        .getStatsComposerByClientId(clientId, mockedServiceHolder)
        .map(statsComposer => assert(statsComposer.isInstanceOf[MLSStatsComposer]))
    }

    "throw Exception if client is not found" in {
      when(mockedServiceHolder.clientService.getById(any[Int]))
        .thenReturn(Future.successful(None))

      StatsComposerFactory
        .getStatsComposerByClientId(clientId, mockedServiceHolder)
        .map(_ => fail())
        .recover {
          case ex: Exception => assert(ex.getMessage == s"Client $clientId does not exist")
        }

    }
  }
}
