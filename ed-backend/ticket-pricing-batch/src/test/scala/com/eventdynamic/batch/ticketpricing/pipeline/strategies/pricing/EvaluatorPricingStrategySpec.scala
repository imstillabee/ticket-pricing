package com.eventdynamic.batch.ticketpricing.pipeline.strategies.pricing

import java.sql.Timestamp

import com.eventdynamic.batch.ticketpricing.models.{IncrementableFeatures, ObservationGroup}
import com.eventdynamic.batch.ticketpricing.pipeline.evaluators.Evaluator
import com.eventdynamic.batch.ticketpricing.utils.TestHelper
import org.scalatest.WordSpec
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future

class EvaluatorPricingStrategySpec extends WordSpec with MockitoSugar {
  val t = new TestHelper

  // Fake data to start with
  val fakeObservations = Seq(t.observation(), t.observation())
  val fakeFeatures = IncrementableFeatures(new Timestamp(0), 1000.0, Some(100.0), 10000.0, 10.0)
  val fakeObservationGroups = Seq(ObservationGroup(fakeObservations, fakeFeatures))

  "EvaluatorPricingStrategy#priceObservationGroups" should {
    "let the evaluator update the price on the observation groups" in {
      // Evaluator that adds $10 to list price
      val fakeEvaluator = new Evaluator() {
        override def evaluate(
          observations: Seq[ObservationGroup]
        ): Future[Seq[ObservationGroup]] = {
          observations.foreach(og => og.features.listPrice = og.features.listPrice.map(_ + 10))
          Future.successful(observations)
        }
      }

      new EvaluatorPricingStrategy(fakeEvaluator).priceObservationGroups(fakeObservationGroups)

      assert(fakeObservationGroups.head.features.listPrice.contains(BigDecimal(110)))
    }

    "throw an exception when the future fails" in {
      val fakeEvaluator = mock[Evaluator]
      when(fakeEvaluator.evaluate(any[Seq[ObservationGroup]]))
        .thenReturn(Future.failed(new Exception))

      val strat = new EvaluatorPricingStrategy(fakeEvaluator)
      assertThrows[Exception](strat.priceObservationGroups(fakeObservationGroups))
    }
  }
}
