package com.eventdynamic.batch.ticketpricing.services.weather

import java.sql.Timestamp
import java.time.LocalDateTime

import com.softwaremill.sttp.okhttp.OkHttpFutureBackend
import com.softwaremill.sttp.testing.SttpBackendStub
import org.scalatest.AsyncWordSpec

class OpenWeatherForecastServiceSpec extends AsyncWordSpec {
  val appId = "appId"
  val baseUrl = "baseUrl"

  "OpenWeatherForecastService" should {
    "get forecast" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("forecast"))
        .thenRespondWithCode(
          200,
          """{ "cod": "100", "list": [ { "dt": 1527206400, "main": { "temp": 1.2, "temp_min": 2.2, "temp_max": 3.2 }, "weather": [{ "id": 800, "main": "clouds", "description": "the sky is cloudy" }] } ] }"""
        )
      val weatherForecastService = new OpenWeatherForecastService(appId, baseUrl, backend)

      val zip = "12345"
      weatherForecastService
        .getForecast(zip)
        .map(res => {
          assert(res.isDefined)
          val weatherForecast = res.get

          assert(weatherForecast.length == 1)
          val wf = weatherForecast.head
          assert(wf.time.before(Timestamp.valueOf(LocalDateTime.now())))
        })
    }

    "throw an exception for server errors" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("forecast"))
        .thenRespondServerError()
      val weatherForecastService = new OpenWeatherForecastService(appId, baseUrl, backend)

      val zip = "12345"
      weatherForecastService
        .getForecast(zip)
        .map(_ => fail)
        .recover {
          case _ => succeed
        }
    }

    "throw an exception for client errors" in {
      val backend = SttpBackendStub(OkHttpFutureBackend())
        .whenRequestMatches(_.uri.path.contains("forecast"))
        .thenRespondNotFound()
      val weatherForecastService = new OpenWeatherForecastService(appId, baseUrl, backend)

      val zip = "12345"
      weatherForecastService
        .getForecast(zip)
        .map(_ => fail)
        .recover {
          case _ => succeed
        }
    }
  }
}
