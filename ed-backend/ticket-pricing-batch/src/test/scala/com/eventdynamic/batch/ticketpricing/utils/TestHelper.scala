package com.eventdynamic.batch.ticketpricing.utils

import java.sql.Timestamp
import java.time.ZonedDateTime

import com.eventdynamic.batch.ticketpricing.models.weather.ObservationWeather
import com.eventdynamic.batch.ticketpricing.models.{Observation, PipelinePrice}
import com.eventdynamic.batch.ticketpricing.services._
import com.eventdynamic.batch.ticketpricing.services.weather.OpenWeatherForecastService
import com.eventdynamic.models.TransactionType.TransactionType
import com.eventdynamic.models.{EventSeatListedPriceUpdateByScale, ObservationBase}
import com.eventdynamic.modelserver.models.{
  ModelServerPriceContext,
  ModelServerPriceExample,
  ModelServerWeather
}
import com.eventdynamic.mysportsfeed.MySportsFeedService
import com.eventdynamic.mysportsfeed.models.mlb.{MLBGameStats, MLBTeamStats}
import com.eventdynamic.services._
import com.eventdynamic.services.mlb.MySportsFeedInfoService
import com.eventdynamic.transactions.IntegrationTransactions
import com.eventdynamic.utils.DateHelper
import org.specs2.mock.Mockito

import scala.math.BigDecimal.RoundingMode
import scala.util.Random

class TestHelper extends Mockito {

  def string(len: Int = 20) = Random.nextString(len)
  def int() = Random.nextInt()
  def float() = Random.nextFloat()
  def timestamp() = DateHelper.now()

  def observation(
    id: Int = int(),
    seatId: Int = int(),
    eventId: Int = int(),
    eventName: String = string(),
    integrationSeatId: Int = int(),
    integrationEventId: Int = int(),
    eventDate: ZonedDateTime = ZonedDateTime.now(),
    listPrice: Option[BigDecimal] = Some(BigDecimal(float())),
    overridePrice: Option[BigDecimal] = None,
    priceScaleId: Int = int(),
    priceScale: String = string(),
    externalPriceScaleId: Int = int(),
    jobStart: Timestamp = timestamp(),
    section: String = string(),
    row: String = string(),
    seat: String = string(),
    soldPrice: Option[BigDecimal] = None,
    revenueCategoryId: Option[Int] = None,
    buyerTypeCode: Option[String] = None,
    integrationId: Option[Int] = None,
    transactionType: Option[TransactionType] = None,
    isEventListed: Boolean = true,
    weather: Option[ObservationWeather] = Some(ObservationWeather(None, 70f, 800)),
    mLBTeamStats: Option[MLBTeamStats] = None,
    mLBGameStats: MLBGameStats = MLBGameStats(0, "", false, false),
    eventScore: Double = 10,
    spring: Double = 2,
    isHeld: Boolean = false
  ): Observation = {
    Observation(
      jobStart,
      id,
      seatId,
      eventId,
      eventName,
      integrationSeatId,
      integrationEventId,
      eventDate,
      seat,
      row,
      section,
      listPrice,
      overridePrice,
      priceScaleId,
      externalPriceScaleId,
      priceScale,
      soldPrice,
      revenueCategoryId,
      buyerTypeCode,
      integrationId,
      transactionType,
      isListed = true,
      isEventListed = isEventListed,
      weather,
      eventScore,
      spring,
      isHeld
    )
  }

  def observationBase(
    seatId: Int = int(),
    eventId: Int = int(),
    eventName: String = string(),
    integrationEventId: Int = int(),
    integrationSeatId: Int = int(),
    venueId: Int = int(),
    venueZip: String = string(len = 6),
    eventDate: Timestamp = timestamp(),
    seat: String = string(),
    row: String = string(),
    section: String = string(),
    eventSeatId: Int = int(),
    listPrice: Option[BigDecimal] = Some(BigDecimal(float())),
    overridePrice: Option[BigDecimal] = None,
    priceScaleId: Int = int(),
    externalPriceScaleId: Int = int(),
    priceScale: String = string(),
    soldPrice: Option[BigDecimal] = None,
    revenueCategoryId: Option[Int] = None,
    buyerTypeCode: Option[String] = None,
    integrationId: Option[Int] = None,
    transactionType: Option[TransactionType] = None,
    isListed: Boolean = true,
    isEventListed: Boolean = true,
    eventScoreModifier: Double = 0.0,
    springModifier: Double = 0.0,
    isHeld: Boolean = false
  ): ObservationBase = {
    ObservationBase(
      seatId,
      eventId,
      eventName,
      integrationEventId,
      integrationSeatId,
      venueId,
      venueZip,
      venueTimeZone = "America/New_York",
      eventDate,
      seat,
      row = row,
      section,
      eventSeatId,
      listPrice,
      overridePrice,
      priceScaleId,
      externalPriceScaleId,
      priceScale,
      soldPrice,
      revenueCategoryId,
      buyerTypeCode,
      integrationId,
      transactionType,
      isListed,
      isEventListed,
      eventScoreModifier,
      springModifier,
      isHeld
    )
  }

  def listPriceChange(
    eventId: Int = int(),
    priceScaleId: Int = int(),
    price: BigDecimal = BigDecimal(float())
  ): EventSeatListedPriceUpdateByScale = {
    EventSeatListedPriceUpdateByScale(eventId, priceScaleId, price)
  }

  def some[A](creator: Int => A, n: Int = 10): Seq[A] = {
    (1 to n).map(creator)
  }

  val mockedServices = new ServiceHolder(
    mock[ClientIntegrationService],
    mock[ClientService],
    mock[SeasonService],
    mock[EventService],
    mock[SeatService],
    mock[EventSeatService],
    mock[ObservationService],
    mock[WeatherAverageService],
    mock[OpenWeatherForecastService],
    mock[IntegrationService],
    mock[RevenueCategoryService],
    mock[RevenueCategoryMappingService],
    mock[MySportsFeedService],
    mock[MySportsFeedInfoService],
    mock[TeamStatService],
    mock[TransactionService],
    mock[IntegrationTransactions],
    mock[JobConfigService],
    mock[MLBAMMappingService],
    mock[PriceGuardService],
    mock[EventCategoryService],
    mock[ProVenuePricingRuleService],
    mock[PriceScaleService],
    mock[SecondaryPricingRulesService],
    mock[NFLSeasonStatService],
    mock[NFLEventStatService],
    mock[MLSSeasonStatService],
    mock[MLSEventStatService],
    mock[NCAAFSeasonStatService],
    mock[NCAAFEventStatService],
    mock[NCAABSeasonStatService],
    mock[NCAABEventStatService]
  )

  def pipelinePrice(
    eventSeatId: Int = 1,
    eventId: Int = 1,
    seatId: Int = 1,
    integrationEventId: Int = 1,
    integrationSeatId: Int = 1,
    priceScaleId: Int = 1,
    externalPriceScaleId: Int = 1,
    price: Option[BigDecimal] = Some(18.99),
    shouldPrice: Boolean = true,
    overridden: Boolean = false,
    section: String = "100",
    row: String = "1",
    available: Boolean = true
  ): PipelinePrice = {
    PipelinePrice(
      eventSeatId,
      eventId,
      seatId,
      integrationEventId,
      integrationSeatId,
      priceScaleId,
      externalPriceScaleId,
      price,
      shouldPrice,
      overridden,
      section,
      row,
      available
    )
  }

  def expectedPrice(price: BigDecimal, percentPriceModification: Int): BigDecimal = {
    val result = price * (1 + BigDecimal(percentPriceModification / 100.00))
    result.setScale(2, RoundingMode.HALF_UP)
  }

  def modelServerPriceContext(): ModelServerPriceContext =
    ModelServerPriceContext(
      event_date = "2018-12-13 12:13:14.123",
      event_score = 10,
      spring = .02,
      timestamp = "2018-12-22 12:13:14.123",
      weather = Some(ModelServerWeather(temp_average = 0f, temp = 0, weather_condition = ""))
    )

  def modelServerPriceExample(): ModelServerPriceExample =
    ModelServerPriceExample("", "", "", 0)
}
