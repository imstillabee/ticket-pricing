# Ticket Sync (aka Primary Sync)

> Ticket sync (aka primary sync) is a job that pulls data from the primary integration and updates records in the
Event Dynamic database to match.

## Quick Start
### Setup
1. Onboard a client
1. Setup a primary _ClientIntegration_ record for that client by setting the appropriate _json_ configuration
1. Set the `ED_CLIENT_ID` environment variable for the client you would like to sync
1. Run the job once to create a _JobConfig_ record: `sbt ticketSync/run`

### Running
1. Update the _lastRun_ field in the _JobConfig_ record for the interval you want to test
1. __Optionally__ remove records from the _Seats_, _EventSeats_, and _Transactions_ tables
1. Run the script using sbt: `sbt ticketSync/run`
1. Check the _Seats_, _EventSeats_, and _Transactions_ tables for new / updated records
1. Repeat from step 1

## Configuration
### Environment Variables
| Name | Description |
| :--- | :--- |
| `ED_CLIENT_ID` | Client ID to run the job for |

### Job Config
Job Name: `primary-sync`

Example JSON Body:
```json
{
  "lastRun": "2019-01-01T01:01:01.000Z"
}
```

Field Descriptions:

| Field | Description |
| :--- | :--- |
| `lastRun` | ISO String representing the last time this job was run. The job uses this time to only pull in changed information from the primary integration |

## Service Dependencies
* Primary Integration (e.g. Skybox or Tickets.com)

## Troubleshooting
_Put FAQs here_

