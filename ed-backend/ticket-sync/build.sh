#!/bin/bash

set -e

SHOULD_PUSH=${SHOULD_PUSH:-false}
BUILD_DIR=$(dirname "$0")
ROOT_DIR=$(dirname "$PWD")

# Project-specific variables
DOCKER_REPO="ed-ticket-sync"
BUILD_COMMAND="sbt --warn -no-colors coverageOff clean ticketSync/assembly"

# Build project assets
printf "\n> Running: $BUILD_COMMAND\n\n"
$BUILD_COMMAND

$ROOT_DIR/scripts/dockerize.sh \
  $BUILD_DIR \
  $DOCKER_REPO \
  $SHOULD_PUSH
