package com.eventdynamic.batch.ticket.sync

import java.time.Instant

import com.eventdynamic.batch.ticket.sync.models.InventoryItem
import com.eventdynamic.batch.ticket.sync.services.ServiceHolder
import com.eventdynamic.models.{Event, EventSeat, PriceScale, Seat}
import com.eventdynamic.utils.{DateHelper, FutureUtil}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object InventorySync {

  private val logger = LoggerFactory.getLogger(this.getClass)
  private case class SeatIdentifier(venueId: Int, section: String, row: String, seat: String)

  private type PrimaryEventIdToEvent = Map[Int, Event]
  private type SeatIdentifierToSeatId = Map[SeatIdentifier, Int]
  private type InventoryItemToPriceScaleId = Map[InventoryItem, Int]

  /**
    * Sync inventory from primary integration to EventDynamic.
    *
    * Syncing includes...
    *  - Creating new inventory (seats and eventSeats) for new inventory
    *  - Updating prices for inventory that already exists
    *
    * Syncing does not include...
    *   - Removing EventDynamic inventory that has disappeared from the primary (for now)
    *
    * @param serviceHolder services
    * @param events events to sync
    * @return the number of updated event seats
    */
  def syncInventory(
    serviceHolder: ServiceHolder,
    events: Seq[Event],
    start: Instant,
    end: Instant
  ): Future[Int] = {
    logger.info(s"Syncing inventory changed between $start and $end")

    // Look up event by primaryEventId
    val eventLookup =
      events.filter(_.primaryEventId.isDefined).map(e => e.primaryEventId.get -> e).toMap

    for {
      inventory <- fetchInventory(serviceHolder, events, start, end).andThen {
        case Success(inventory) => logger.info(s"New inventory items: ${inventory.length}")
        case Failure(e)         => logger.error(s"Error fetching inventory", e)
      }
      upsertResult <- upsertInventory(serviceHolder, eventLookup, inventory)
        .andThen {
          case Success(inventoryCount) => {
            logger.info(s"Upserted inventory items: $inventoryCount")
          }
          case Failure(e) => logger.error(s"Error upserting inventory", e)
        }
    } yield upsertResult

  }

  /**
    * Fetch all price scales for the client
    */
  private def fetchPriceScales(
    serviceHolder: ServiceHolder,
    events: Iterable[Event]
  ): Future[Seq[PriceScale]] = {
    events.headOption.map(_.clientId) match {
      case None           => Future.successful(Seq())
      case Some(clientId) => serviceHolder.priceScaleService.getAllForClient(clientId)
    }
  }

  /**
    * Fetch inventory from the primary
    */
  private def fetchInventory(
    serviceHolder: ServiceHolder,
    events: Seq[Event],
    start: Instant,
    end: Instant
  ): Future[Seq[InventoryItem]] = {
    val primaryEventIds = events.flatMap(_.primaryEventId)
    serviceHolder.primaryService.getInventory(primaryEventIds, start, end)
  }

  /**
    * Upsert inventory into the EventDynamic database
    */
  private def upsertInventory(
    serviceHolder: ServiceHolder,
    eventLookup: PrimaryEventIdToEvent,
    inventory: Seq[InventoryItem]
  ): Future[Int] = {
    for {
      priceScaleLookup <- buildPriceScaleLookup(serviceHolder, eventLookup, inventory)
      seatLookup <- upsertSeatsForInventory(serviceHolder, eventLookup, inventory)
      eventSeatIds <- upsertEventSeatsForInventory(
        serviceHolder,
        eventLookup,
        seatLookup,
        priceScaleLookup,
        inventory
      )
    } yield {
      eventSeatIds.size
    }
  }

  private def upsertSeatsForInventory(
    serviceHolder: ServiceHolder,
    eventLookup: PrimaryEventIdToEvent,
    inventory: Seq[InventoryItem]
  ): Future[SeatIdentifierToSeatId] = {
    val now = DateHelper.now()

    // Extract seat record from inventory
    val seatRecords = inventory
      .map(item => {
        val venueId = eventLookup(item.primaryEventId).venueId
        Seat(
          id = None,
          primarySeatId = item.primarySeatId,
          createdAt = now,
          modifiedAt = now,
          seat = item.seat,
          row = item.row,
          section = item.section,
          venueId = venueId
        )
      })
      .distinct

    // Chunk upsert records to run in bulk request one at a time
    val CHUNK_SIZE = 1000
    val groups = seatRecords.grouped(CHUNK_SIZE).toList

    // Upsert each chunk of changes, then map to a lookup table of seatIdentifier -> seatId
    FutureUtil
      .serializeFutures(groups.zipWithIndex, 3)(group => {
        val (data, index) = group
        logger.info(s"Upserting Seats chunk ${index + 1} of ${groups.length}")
        serviceHolder.seatService.bulkUpsert(data)
      })
      .map(resultLists => {
        resultLists.flatten
          .zip(seatRecords)
          .map {
            case (seatId, s) => SeatIdentifier(s.venueId, s.section, s.row, s.seat) -> seatId
          }
          .toMap
      })
  }

  private def upsertEventSeatsForInventory(
    serviceHolder: ServiceHolder,
    eventLookup: PrimaryEventIdToEvent,
    seatLookup: SeatIdentifierToSeatId,
    priceScaleLookup: InventoryItemToPriceScaleId,
    inventory: Seq[InventoryItem]
  ): Future[List[Int]] = {
    val eventSeatRecords = inventory.map(item => {
      val event = eventLookup(item.primaryEventId)
      val now = DateHelper.now()

      EventSeat(
        id = None,
        seatId = seatLookup(SeatIdentifier(event.venueId, item.section, item.row, item.seat)),
        eventId = event.id.get,
        priceScaleId = priceScaleLookup(item),
        listedPrice = item.listedPrice,
        overridePrice = None,
        isListed = true,
        createdAt = now,
        modifiedAt = now,
        isHeld = item.isHeld
      )
    })

    // Chunk upsert records to run in bulk request one at a time
    val CHUNK_SIZE = 1000
    val groups = eventSeatRecords.grouped(CHUNK_SIZE).toList

    // Upsert each chunk of changes, then return the created ids
    FutureUtil
      .serializeFutures(groups.zipWithIndex, 3)(group => {
        val (data, index) = group
        logger.info(s"Upserting EventSeats chunk ${index + 1} of ${groups.length}")
        serviceHolder.eventSeatService.bulkUpsert(data)
      })
      .map(_.flatten)
  }

  private def buildPriceScaleLookup(
    serviceHolder: ServiceHolder,
    eventLookup: PrimaryEventIdToEvent,
    inventory: Seq[InventoryItem]
  ): Future[InventoryItemToPriceScaleId] = {
    fetchPriceScales(serviceHolder, eventLookup.values)
      .map(_.map(ps => ps.integrationId -> ps).toMap)

    for {
      priceScales <- fetchPriceScales(serviceHolder, eventLookup.values)
    } yield {
      val priceScaleLookup = priceScales
        .map(priceScale => priceScale.integrationId -> priceScale)
        .toMap

      inventory
        .map(item => {
          val priceScale = item.primaryPriceScaleId match {
            case None                      => guessPriceScale(priceScales, item)
            case Some(primaryPriceScaleId) => priceScaleLookup(primaryPriceScaleId)
          }

          item -> priceScale.id.get
        })
        .toMap
    }
  }

  /**
    * Guess a price scale naively by choosing the first one
    *
    * @return a price scale
    */
  private def guessPriceScale(
    priceScales: Seq[PriceScale],
    inventoryItem: InventoryItem
  ): PriceScale = {
    if (priceScales.isEmpty) {
      throw new Exception(s"Cannot guess price scale because there are no price scales")
    }

    // TODO: guess per integration
    priceScales.head
  }
}
