package com.eventdynamic.batch.ticket.sync

import java.time.Instant

import com.eventdynamic.batch.ticket.sync.models.{InventoryItem, PrimaryPriceGuard}
import com.eventdynamic.batch.ticket.sync.services.{PrimaryService, ServiceHolder}
import com.eventdynamic.models._
import com.eventdynamic.utils.{DateHelper, FutureUtil}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.math.BigDecimal.RoundingMode
import scala.util.{Failure, Success}

object PriceGuardSync {
  private val logger = LoggerFactory.getLogger(this.getClass)

  private type PrimaryEventIdToEvent = Map[Int, Event]

  /**
    * Sync inventory from primary integration to EventDynamic.
    *
    * Syncing includes...
    *  - Creating new inventory (seats and eventSeats) for new inventory
    *  - Updating prices for inventory that already exists
    *
    * Syncing does not include...
    *   - Removing EventDynamic inventory that has disappeared from the primary (for now)
    *
    * @param serviceHolder services
    * @param events events to sync
    * @return the number of updated event seats
    */
  def syncPriceGuards(
    serviceHolder: ServiceHolder,
    events: Seq[Event],
    start: Instant,
    end: Instant
  ): Future[Int] = {
    logger.info(s"Syncing price guards changed between $start and $end")

    // Look up event by primaryEventId
    val eventLookup =
      events.filter(_.primaryEventId.isDefined).map(e => e.primaryEventId.get -> e).toMap

    for {
      primaryPriceGuards <- fetchPrimaryPriceGuards(serviceHolder, events, start, end)
        .andThen {
          case Success(priceGuards) => logger.info(s"New price guards: ${priceGuards.length}")
          case Failure(e)           => logger.error(s"Error fetching tickets", e)
        }
      priceGuardIds <- upsertPriceGuardsForInventory(serviceHolder, eventLookup, primaryPriceGuards)
        .andThen {
          case Success(priceGuardIds) =>
            logger.info(s"Upserted price guards: ${priceGuardIds.size}")
          case Failure(e) => logger.error(s"Error upserting inventory", e)
        }
    } yield priceGuardIds.size
  }

  /**
    * Fetch individual tickets from the primary
    */
  private def fetchPrimaryPriceGuards(
    serviceHolder: ServiceHolder,
    events: Seq[Event],
    start: Instant,
    end: Instant
  ): Future[Seq[PrimaryPriceGuard]] = {
    val primaryEventIds = events.flatMap(_.primaryEventId)
    serviceHolder.primaryService.getPriceGuards(primaryEventIds, start, end)
  }

  private def upsertPriceGuardsForInventory(
    serviceHolder: ServiceHolder,
    eventLookup: PrimaryEventIdToEvent,
    primaryPriceGuards: Seq[PrimaryPriceGuard]
  ): Future[List[Int]] = {
    val now = DateHelper.now()

    // Get updated price guards for each event
    val priceGuards = primaryPriceGuards
      .map(
        priceGuard =>
          PriceGuard(
            id = None,
            eventId = eventLookup(priceGuard.primaryEventId).id.get,
            section = priceGuard.section,
            row = priceGuard.row,
            minimumPrice = priceGuard.minPrice.setScale(0, RoundingMode.CEILING).intValue(),
            maximumPrice = None,
            createdAt = now,
            modifiedAt = now
        )
      )

    // Chunk upsert records to run in bulk request one at a time
    val CHUNK_SIZE = 1000
    val groups = priceGuards.grouped(CHUNK_SIZE).toList

    for {
      // Upsert each chunk of changes, then return the created ids
      upsertResults <- FutureUtil
        .serializeFutures(groups.zipWithIndex, 3)(group => {
          val (data, index) = group
          logger.info(s"Upserting PriceGuards chunk ${index + 1} of ${groups.length}")
          serviceHolder.priceGuardService.bulkUpsert(data)
        })
        .map(_.flatten)
    } yield {
      upsertResults
    }
  }
}
