package com.eventdynamic.batch.ticket.sync

import java.time.{Duration, Instant}

import com.eventdynamic.IntegrationServiceBuilder
import com.eventdynamic.batch.ticket.sync.services.{
  PrimaryService,
  ServiceHolder,
  SkyboxPrimaryService,
  TDCPrimaryService
}
import com.eventdynamic.db.EDContext
import com.eventdynamic.models.jobconfigs.{TicketSyncConfig, TicketSyncFormat}
import com.eventdynamic.models._
import com.eventdynamic.services.{ClientIntegrationService, JobConfigService}
import com.eventdynamic.utils.{DateHelper, FutureUtil}
import com.typesafe.config.{Config, ConfigFactory}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.util.Success

object TicketSync
    extends SkyboxClientIntegrationConfig.format
    with TDCClientIntegrationConfig.format {
  private val logger = LoggerFactory.getLogger(this.getClass)

  /**
    * Main entry point for the Script
    *
    * @param args CLI arguments
    */
  def main(args: Array[String]): Unit = {
    logger.info("----------- START TicketSync.main -----------")

    val conf: Config = ConfigFactory.defaultApplication().resolve()
    val clientId = conf.getInt("ticketSync.clientId")
    logger.debug(s"> clientId: $clientId")

    // Create ED database context
    val ed = new EDContext()
    val integrationComposite = getPrimaryIntegration(clientId, ed)

    // Create integration service builder
    val serviceBuilder = new IntegrationServiceBuilder()

    // Create primary service
    val primaryService = integrationComposite.name match {
      case "Tickets.com" =>
        new TDCPrimaryService(
          serviceBuilder.buildTdcReplicatedProxyService(
            integrationComposite.config[TDCClientIntegrationConfig]
          ),
          integrationComposite
        )
      case "Skybox" =>
        new SkyboxPrimaryService(
          serviceBuilder.buildSkyboxService(
            integrationComposite.config[SkyboxClientIntegrationConfig]
          ),
          integrationComposite
        )
      case n => throw new Exception(s"Unknown primary integration $n")
    }

    try {
      val ticketSync = new TicketSync(ed, clientId, primaryService)
      Await.result(ticketSync.sync(clientId), concurrent.duration.Duration.Inf)
    } finally {
      // Dispose contexts
      // TDC context might not exist
      logger.info("Disposing database connections")
      ed.dispose()
    }
    logger.info("----------- END TicketSync.main -----------")
  }

  /**
    * Gets the primary integration so we can load the correct primary service
    */
  private def getPrimaryIntegration(clientId: Int, ctx: EDContext): ClientIntegrationComposite = {
    val clientIntegrationService = new ClientIntegrationService(ctx)

    val f = clientIntegrationService
      .getByClientId(clientId, onlyActive = true, onlyPrimary = true)
      .map(_.head)

    Await.result(f, concurrent.duration.Duration.Inf)
  }
}

class TicketSync(ctx: EDContext, clientId: Int, primaryService: PrimaryService)
    extends TicketSyncFormat {
  val serviceHolder = ServiceHolder(ctx, primaryService)
  val jobConfigService = new JobConfigService(ctx)

  private val logger = LoggerFactory.getLogger(this.getClass)

  private val jobType = JobType.PrimarySync

  def sync(clientId: Int): Future[Unit.type] = {
    val now = Instant.now()
    for {
      // Get job config
      jobConfig <- getJobConfig

      // Get the events we want to pull information for
      events <- getEventsForCurrentSeason

      // Sync inventory first so we have all the references for incoming transactions
      _ <- syncInventoryInChunks(events, jobConfig.lastRun, now, jobConfig.transactionSyncStep)
      _ <- syncPriceGuards(events, jobConfig.lastRun, now)
      _ <- syncTransactionsInChunks(events, jobConfig.lastRun, now, jobConfig.transactionSyncStep)

      // Update config
      _ <- saveJobConfig(jobConfig.copy(lastRun = now))
    } yield Unit
  }

  def syncPriceGuards(events: Seq[Event], start: Instant, end: Instant): Future[Int] = {
    PriceGuardSync.syncPriceGuards(serviceHolder, events, start, end).andThen {
      case Success(nUpsertedPriceGuards) =>
        logger.info(s"Upserted $nUpsertedPriceGuards price guards")
    }
  }

  def syncInventoryInChunks(
    events: Seq[Event],
    start: Instant,
    end: Instant,
    step: Option[Duration]
  ): Future[List[Int]] = {
    val range = step match {
      case Some(stepVal) => DateHelper.dateRange(start, end, stepVal)
      case None          => Iterator((start, end))
    }

    FutureUtil.serializeFutures(range.toIterable)(rangeItem => {
      val (rangeStart, rangeEnd) = rangeItem

      InventorySync
        .syncInventory(serviceHolder, events, rangeStart, rangeEnd)
        .andThen {
          case Success(nUpsertedInventory) =>
            logger
              .info(s"Upserted $nUpsertedInventory inventory for range $rangeStart to $rangeEnd")
        }
    })
  }

  def syncTransactionsInChunks(
    events: Seq[Event],
    start: Instant,
    end: Instant,
    step: Option[Duration]
  ): Future[List[Int]] = {
    val range = step match {
      case Some(stepVal) => DateHelper.dateRange(start, end, stepVal)
      case None          => Iterator((start, end))
    }

    FutureUtil.serializeFutures(range.toIterable)(rangeItem => {
      val (rangeStart, rangeEnd) = rangeItem

      TransactionSync
        .syncTransactions(serviceHolder, rangeStart, rangeEnd, events, clientId)
        .andThen {
          case Success(nUpsertedTransactions) =>
            logger.info(
              s"Upserted $nUpsertedTransactions transactions for range $rangeStart to $rangeEnd"
            )
        }
    })
  }

  private def getEventsForCurrentSeason: Future[Seq[Event]] = {
    for {
      season <- serviceHolder.seasonService.getCurrent(Some(clientId), isAdmin = false).map {
        case None    => throw new Exception("No current season")
        case Some(s) => s
      }

      events <- serviceHolder.eventService
        .getAllBySeason(season.id.get, clientId, isAdmin = false)
        .andThen {
          case Success(e) => logger.info(s"Got ${e.length} event ids.")
          case _          => logger.error("Failed to get event ids.")
        }
    } yield events
  }

  private def getJobConfig: Future[TicketSyncConfig] = {
    jobConfigService
      .getConfig[TicketSyncConfig](clientId, jobType)
      .flatMap {
        case Some(config) =>
          logger.info(s"Found job config: $config")
          Future.successful(config)
        case None =>
          logger.info(s"No job config, creating one.")
          val config =
            TicketSyncConfig(lastRun = Instant.EPOCH)
          jobConfigService
            .createConfig[TicketSyncConfig](clientId, jobType, config)
            .map(_ => config)
      }
  }

  private def saveJobConfig(ticketSyncConfig: TicketSyncConfig) = {
    jobConfigService.updateConfig(clientId, jobType, ticketSyncConfig)
  }
}
