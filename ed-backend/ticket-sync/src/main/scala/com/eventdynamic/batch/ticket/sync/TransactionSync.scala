package com.eventdynamic.batch.ticket.sync

import java.time.Instant

import com.eventdynamic.batch.ticket.sync.models.PrimaryTransaction
import com.eventdynamic.batch.ticket.sync.services.ServiceHolder
import com.eventdynamic.models.{Event, Transaction}
import com.eventdynamic.utils.{DateHelper, FutureUtil}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
  * Helper functions for syncing transactions.
  */
object TransactionSync {
  private val logger = LoggerFactory.getLogger(this.getClass)

  val defaultBuyerTypeCode = "ADULT"

  /**
    * Private function that syncs recent transactions from primary
    *
    * @param s service holder
    * @param start the time of the latest transaction being pulled
    * @param end the last time to query transactions for
    * @param events the list of events grab transactions for
    * @param clientId id of the client
    * @return Future[Int] total quantity of transactions sync'd
    */
  def syncTransactions(
    s: ServiceHolder,
    start: Instant,
    end: Instant,
    events: Seq[Event],
    clientId: Int
  ): Future[Int] = {
    val eventIds = events.flatMap(_.primaryEventId)

    for {
      primaryTransactions <- s.primaryService.getTransactionsForEvents(start, end, eventIds)
      transactions <- mapTransactions(s, clientId, primaryTransactions)
      _ <- upsertTransactions(s, transactions)
    } yield transactions.length
  }

  /**
    * Creates or updates transactions pending if they're already inside event dynamic.
    */
  private def upsertTransactions(
    s: ServiceHolder,
    transactions: Seq[Transaction]
  ): Future[List[Seq[Int]]] = {
    val CHUNK_SIZE = 1000
    val groups = transactions.grouped(CHUNK_SIZE).toList

    FutureUtil
      .serializeFutures(groups.zipWithIndex, 3)(group => {
        val (data, index) = group
        logger.info(s"Upserting Transaction chunk ${index + 1} of ${groups.length}")
        s.transactionService.bulkUpsert(data)
      })
  }

  /**
    * Converts transactions from primary to Event Dynamic transactions.  If the transaction already exists in Event
    * Dynamic, then it pulls the transaction from the db instead of creating a new model.  Note the new models are
    * not saved to the database.
    */
  private def mapTransactions(
    serviceHolder: ServiceHolder,
    clientId: Int,
    primaryTransactions: Seq[PrimaryTransaction]
  ): Future[Seq[Transaction]] = {
    FutureUtil
      .serializeFutures(primaryTransactions, 10)(primaryTransaction => {
        mapTransaction(serviceHolder, clientId, primaryTransaction)
          .recover {
            case e =>
              logger.error(s"Error mapping transaction $primaryTransactions", e)
              throw e
          }
      })
  }

  /**
    * Retrieve transaction for a given primary transaction
    */
  private def mapTransaction(
    serviceHolder: ServiceHolder,
    clientId: Int,
    primaryTransaction: PrimaryTransaction
  ): Future[Transaction] = {
    val buyerTypeCode = primaryTransaction.buyerTypeCode.getOrElse(defaultBuyerTypeCode)

    for {
      // Expect the event seat to exist, because we should be syncing inventory
      eventSeat <- serviceHolder.eventSeatService
        .getOne(primaryTransaction.eventSeatIdentifier)
        .map {
          case Some(es) => es
          case None =>
            throw new Exception(
              s"EventSeat not found for identifiers ${primaryTransaction.eventSeatIdentifier}"
            )
        }
      revenueCategoryId <- serviceHolder.revenueCategoryMappingService
        .regexMatch(clientId, buyerTypeCode)
    } yield {
      Transaction(
        id = None,
        createdAt = DateHelper.now(),
        modifiedAt = DateHelper.now(),
        timestamp = primaryTransaction.timestamp,
        price = primaryTransaction.price,
        eventSeatId = eventSeat.id.get,
        integrationId = primaryTransaction.integrationId,
        buyerTypeCode = buyerTypeCode,
        transactionType = primaryTransaction.transactionType,
        revenueCategoryId = revenueCategoryId,
        primaryTransactionId = Some(primaryTransaction.primaryTransactionId)
      )
    }
  }
}
