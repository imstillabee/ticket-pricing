package com.eventdynamic.batch.ticket.sync.models

case class InventoryItem(
  primaryEventId: Int,
  primarySeatId: Option[Int],
  primaryPriceScaleId: Option[Int],
  listedPrice: Option[BigDecimal],
  section: String,
  row: String,
  seat: String,
  priceFloor: Option[BigDecimal],
  isHeld: Boolean
)
