package com.eventdynamic.batch.ticket.sync.models

case class PrimaryPriceGuard(
  primaryEventId: Int,
  section: String,
  row: String,
  minPrice: BigDecimal
)
