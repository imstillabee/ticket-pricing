package com.eventdynamic.batch.ticket.sync.models

import java.sql.Timestamp

import com.eventdynamic.models.EventSeatUniqueIdentifier
import com.eventdynamic.models.TransactionType.TransactionType

case class PrimaryTransaction(
  primaryTransactionId: String,
  timestamp: Timestamp,
  price: BigDecimal,
  buyerTypeCode: Option[String],
  integrationId: Int,
  transactionType: TransactionType,
  eventSeatIdentifier: EventSeatUniqueIdentifier
)
