package com.eventdynamic.batch.ticket.sync.services

import java.time.Instant

import com.eventdynamic.batch.ticket.sync.models.{
  InventoryItem,
  PrimaryPriceGuard,
  PrimaryTransaction
}

import scala.concurrent.Future

/**
  * Defines the required functionality for primary integrations for syncing tickets.
  */
trait PrimaryService {

  /**
    * Get all inventory for the events.
    *
    * Include a time window so that the service can limit inventory to options that
    * have changed within the window, but it is not strictly required that the service
    * returns inventory within the window.
    *
    * @param eventIds event ids (in the primary) to fetch inventory for
    * @param start start of change window
    * @param end end of change window
    * @return inventory (should contain event and seat information so we can create
    *         those records)
    */
  def getInventory(eventIds: Seq[Int], start: Instant, end: Instant): Future[Seq[InventoryItem]]

  /**
    * Get all individual tickets for the events.
    *
    * Include a time window so that the service can limit price guards to options that
    * have changed within the window, but it is not strictly required that the service
    * returns price guards within the window.
    *
    * @param eventIds event ids (in the primary) to fetch price guards for
    * @param start start of change window
    * @param end end of change window
    * @return tickets (should contain event and seat information so we can map to
    *         those records)
    */
  def getPriceGuards(
    eventIds: Seq[Int],
    start: Instant,
    end: Instant
  ): Future[Seq[PrimaryPriceGuard]]

  /**
    * Get all transactions for events that happened after the last recorded transaction in Event Dynamic.
    *
    * @param afterDate last transaction date in Event Dynamic for this client
    * @param eventIds event ids on the integration (not in Event Dynamic)
    * @return transactions
    */
  def getTransactionsForEvents(
    afterDate: Instant,
    beforeDate: Instant,
    eventIds: Seq[Int]
  ): Future[Vector[PrimaryTransaction]]

  /**
    * Returns the corresponding integration name for this service.
    *
    * @return
    */
  def getIntegrationName(): String
}
