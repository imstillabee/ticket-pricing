package com.eventdynamic.batch.ticket.sync.services

import com.eventdynamic.db.EDContext
import com.eventdynamic.services._
import com.eventdynamic.transactions.SeasonTransaction

import scala.concurrent.ExecutionContext.Implicits.global

case class ServiceHolder(
  ctx: EDContext,
  clientIntegrationEventsService: ClientIntegrationEventsService,
  clientIntegrationService: ClientIntegrationService,
  eventSeatService: EventSeatService,
  eventService: EventService,
  integrationService: IntegrationService,
  revenueCategoryService: RevenueCategoryService,
  revenueCategoryMappingService: RevenueCategoryMappingService,
  seatService: SeatService,
  transactionService: TransactionService,
  primaryService: PrimaryService,
  venueService: VenueService,
  priceGuardService: PriceGuardService,
  priceScaleService: PriceScaleService,
  seasonService: SeasonService
)

object ServiceHolder {

  def apply(ed: EDContext, primary: PrimaryService): ServiceHolder = {
    val eventSeatService = new EventSeatService(ed)
    val eventService = new EventService(ed)
    val integrationService = new IntegrationService(ed)
    val clientIntegrationService = new ClientIntegrationService(ed)
    val clientIntegrationEventsService = new ClientIntegrationEventsService(ed)
    val revenueCategoryService = new RevenueCategoryService(ed)
    val revenueCategoryMappingService =
      new RevenueCategoryMappingService(ed)
    val seatService = new SeatService(ed)
    val transactionService = new TransactionService(ed)
    val primaryService = primary
    val venueService = new VenueService(ed)
    val priceGuardService = new PriceGuardService(ed)
    val priceScaleService = new PriceScaleService(ed)
    val seasonTransaction =
      new SeasonTransaction(ed, eventService, eventSeatService, transactionService)
    val seasonService = new SeasonService(ed, seasonTransaction)

    ServiceHolder(
      ed,
      clientIntegrationEventsService,
      clientIntegrationService,
      eventSeatService,
      eventService,
      integrationService,
      revenueCategoryService,
      revenueCategoryMappingService,
      seatService,
      transactionService,
      primaryService,
      venueService,
      priceGuardService,
      priceScaleService,
      seasonService
    )
  }
}
