package com.eventdynamic.batch.ticket.sync.services

import java.sql.Timestamp
import java.time.Instant

import com.eventdynamic.SttpHelper
import com.eventdynamic.batch.ticket.sync.models.{
  InventoryItem,
  PrimaryPriceGuard,
  PrimaryTransaction
}
import com.eventdynamic.models.{
  ClientIntegrationComposite,
  EventSeatByExternalSeatLocator,
  PriceGuard,
  SkyboxClientIntegrationConfig,
  TransactionType
}
import com.eventdynamic.skybox.SkyboxService
import com.eventdynamic.skybox.models.{SkyboxListing, SkyboxTicket}
import com.eventdynamic.utils.FutureUtil
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.math.BigDecimal.RoundingMode

class SkyboxPrimaryService(
  skyboxService: SkyboxService,
  clientIntegrationComposite: ClientIntegrationComposite
) extends PrimaryService
    with SkyboxClientIntegrationConfig.format {
  private val logger = LoggerFactory.getLogger(this.getClass)

  private val skyboxSubsidiaryCustomers = {
    if (clientIntegrationComposite.json.isEmpty) {
      throw new Exception(
        s"No configuration found for Skybox integration id: ${clientIntegrationComposite.integrationId}"
      )
    }
    clientIntegrationComposite.config[SkyboxClientIntegrationConfig].customers
  }

  /**
    * Get all inventory for the given events that have been changed within the given time window
    *
    * Return inventory results at the seat level
    *
    * @param eventIds event ids to fetch inventory for
    * @param start start of change window
    * @param end end of change window
    * @return seat inventory
    */
  override def getInventory(
    eventIds: Seq[Int],
    start: Instant,
    end: Instant
  ): Future[Seq[InventoryItem]] = {
    for {
      inventory <- skyboxService
        .getInventory(eventIds, start, end)
        .map(SttpHelper.handleResponse)
      soldInventory <- skyboxService
        .getSoldInventory(eventIds, start, end)
        .map(SttpHelper.handleResponse)
    } yield {

      (inventory.rows ++ soldInventory.rows).flatMap(skyboxInventoryToInventoryItems)
    }
  }

  /**
    * Get all price guards for the given events that have been changed within the given time window
    *
    * @param eventIds Skybox event ids to fetch price guards for
    * @param start start of change window
    * @param end end of change window
    * @return tickets
    */
  override def getPriceGuards(
    eventIds: Seq[Int],
    start: Instant,
    end: Instant
  ): Future[Seq[PrimaryPriceGuard]] = {
    FutureUtil
      .serializeFutures(eventIds)(eventId => {
        skyboxService
          .getTickets(eventId, None, None, Seq(), Some(start), Some(end))
          .map(SttpHelper.handleResponse)
          .map(skyboxTicketsToPrimaryPriceGuards)
      })
      .map(_.flatten)
  }

  /**
    * Get all transactions for events that happened after the last recorded transaction in Event Dynamic.
    *
    * @param afterDate last transaction date in Event Dynamic for this client
    * @param eventIds  event ids on the integration (not in Event Dynamic)
    * @return transactions
    */
  override def getTransactionsForEvents(
    afterDate: Instant,
    beforeDate: Instant,
    eventIds: Seq[Int]
  ): Future[Vector[PrimaryTransaction]] = {
    val config = clientIntegrationComposite.config[SkyboxClientIntegrationConfig]
    for {
      invoices <- skyboxService.getInvoices(eventIds, afterDate, beforeDate).map {
        case r if !r.isSuccess => throw new Exception(s"Error getting skybox invoices: $r")
        case r if r.body.isLeft =>
          throw new Exception(s"Error parsing skybox invoices response: $r")
        case r => r.body.right.get
      }
    } yield {
      val transactions = for {
        invoice <- invoices.rows.filter(
          inv => !config.customersToIgnore.contains(inv.customerId.toString)
        )
        seatNumber <- invoice.seatNumbers.split(",")
      } yield {

        val integrationId =
          integrationIdFromCustomerId(invoice.customerId)

        PrimaryTransaction(
          primaryTransactionId = invoice.id.toString,
          timestamp = Timestamp.from(invoice.invoiceDate),
          price = invoice.unitTicketSales,
          buyerTypeCode = None,
          integrationId = integrationId,
          transactionType = TransactionType.Purchase,
          eventSeatIdentifier = EventSeatByExternalSeatLocator(
            primaryEventId = invoice.eventId,
            section = invoice.section,
            row = invoice.row,
            seat = seatNumber
          )
        )
      }

      transactions.toVector
    }
  }

  private def skyboxInventoryToInventoryItems(
    skyboxInventory: SkyboxListing
  ): Seq[InventoryItem] = {
    (skyboxInventory.lowSeat to skyboxInventory.highSeat).map(seat => {
      InventoryItem(
        primaryEventId = skyboxInventory.eventId,
        primarySeatId = None,
        primaryPriceScaleId = None,
        section = skyboxInventory.section,
        row = skyboxInventory.row,
        seat = seat.toString,
        listedPrice = skyboxInventory.listPrice,
        priceFloor = skyboxInventory.unitCostAverage,
        isHeld = false
      )
    })
  }

  private def integrationIdFromCustomerId(customerId: Int): Int = {
    skyboxSubsidiaryCustomers.get(customerId.toString) match {
      case Some(ci) => ci
      case None =>
        logger.warn(
          s"Could not find integration id for Skybox customer id: $customerId (integration id: ${clientIntegrationComposite.integrationId})"
        )
        clientIntegrationComposite.integrationId
    }
  }

  private def skyboxTicketsToPrimaryPriceGuards(
    skyboxTickets: Seq[SkyboxTicket]
  ): Seq[PrimaryPriceGuard] = {
    skyboxTickets
      .groupBy(ticket => (ticket.eventId, ticket.section, ticket.row))
      .map {
        case ((primaryEventId, section, row), items) =>
          val minPrice = items.map(_.cost).max

          PrimaryPriceGuard(
            primaryEventId = primaryEventId,
            section = section,
            row = row,
            minPrice = minPrice.setScale(0, RoundingMode.CEILING).intValue()
          )
      }
      .toSeq
  }

  /**
    * Returns the corresponding integration name for this service.
    */
  override def getIntegrationName(): String = "Skybox"
}
