package com.eventdynamic.batch.ticket.sync.services
import java.sql.Timestamp
import java.time.Instant

import com.eventdynamic.SttpHelper
import com.eventdynamic.batch.ticket.sync.models.{
  InventoryItem,
  PrimaryPriceGuard,
  PrimaryTransaction
}
import com.eventdynamic.models.{
  ClientIntegrationComposite,
  EventSeatByPrimaryIdentifiers,
  TransactionType
}
import com.eventdynamic.tdcreplicatedproxy.TdcReplicatedProxyService
import com.eventdynamic.utils.HoldCodeHelper

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class TDCPrimaryService(
  val tdcService: TdcReplicatedProxyService,
  clientIntegrationComposite: ClientIntegrationComposite
) extends PrimaryService {

  /**
    * Get all inventory for the given events that have been changed within the given time window
    *
    * @param eventIds event ids to fetch inventory for
    * @param start start of change window
    * @param end end of change window
    * @return inventory items
    */
  override def getInventory(
    eventIds: Seq[Int],
    start: Instant,
    end: Instant
  ): Future[Seq[InventoryItem]] = {
    for {
      inventory <- tdcService.getInventory(eventIds, start, end).map(SttpHelper.handleResponse)
    } yield
      inventory.map(
        inventoryItem =>
          InventoryItem(
            primaryEventId = inventoryItem.eventId,
            primarySeatId = Some(inventoryItem.seatId),
            primaryPriceScaleId = Some(inventoryItem.priceScaleId),
            listedPrice = inventoryItem.listedPrice,
            section = inventoryItem.section,
            row = inventoryItem.row,
            seat = inventoryItem.seatNumber,
            priceFloor = None,
            isHeld = HoldCodeHelper.checkIsHeld(inventoryItem.holdCodeType)
        )
      )
  }

  /**
    * Get all price guards for the given events that have been changed within the given time window
    *
    * @param eventIds event ids (in the primary) to fetch price guards for
    * @param start start of change window
    * @param end end of change window
    * @return price guards
    */
  override def getPriceGuards(
    eventIds: Seq[Int],
    start: Instant,
    end: Instant
  ): Future[Seq[PrimaryPriceGuard]] = {
    Future.successful(Seq())
  }

  /**
    * Get all transactions for events that happened after the last recorded transaction in Event Dynamic.
    *
    * @param afterDate last transaction date in Event Dynamic for this client
    * @param eventIds  event ids on the integration (not in Event Dynamic)
    * @return transactions
    */
  override def getTransactionsForEvents(
    afterDate: Instant,
    beforeDate: Instant,
    eventIds: Seq[Int]
  ): Future[Vector[PrimaryTransaction]] = {
    for {
      tdcTransactions <- tdcService
        .getTransactions(afterDate, beforeDate, eventIds)
        .map(SttpHelper.handleResponse)
    } yield {
      tdcTransactions
        .map(tdcTransaction => {
          PrimaryTransaction(
            primaryTransactionId = tdcTransaction.id.toString,
            timestamp = Timestamp.from(tdcTransaction.timestamp),
            price = tdcTransaction.price,
            buyerTypeCode = Some(tdcTransaction.buyerTypeCode),
            integrationId = clientIntegrationComposite.integrationId,
            transactionType =
              tdcTransactionTypeCodeToTransactionType(tdcTransaction.transactionTypeCode),
            eventSeatIdentifier = EventSeatByPrimaryIdentifiers(
              tdcTransaction.eventId.toInt,
              tdcTransaction.seatId.toInt
            )
          )
        })
        .toVector
    }
  }

  private def tdcTransactionTypeCodeToTransactionType(code: String): TransactionType.Value = {
    val purchases = Set("SA", "CS", "ES")
    val returns = Set("ER", "RT")

    if (purchases.contains(code)) {
      TransactionType.Purchase
    } else if (returns.contains(code)) {
      TransactionType.Return
    } else {
      throw new Exception(s"Unknown transaction type code $code")
    }
  }

  /**
    * Returns the corresponding integration object for this service.
    *
    * @return
    */
  override def getIntegrationName(): String = "Tickets.com"
}
