package com.eventdynamic.batch.ticket.sync

import com.eventdynamic.batch.ticket.sync.models.{InventoryItem, PrimaryPriceGuard}
import com.eventdynamic.models.{Event, EventSeat, PriceScale}
import com.eventdynamic.utils.DateHelper

import scala.util.Random

object Generator {

  def event(
    id: Int = Random.nextInt(),
    primaryEventId: Int = Random.nextInt(),
    clientId: Int = Random.nextInt()
  ): Event = {
    Event(
      id = Some(id),
      primaryEventId = Some(primaryEventId),
      createdAt = DateHelper.now(),
      modifiedAt = DateHelper.now(),
      name = "Event Name",
      timestamp = Some(DateHelper.now()),
      clientId = clientId,
      venueId = 1,
      eventCategoryId = 1,
      seasonId = Some(1),
      isBroadcast = true,
      percentPriceModifier = 0,
      eventScore = None,
      eventScoreModifier = 0,
      spring = None,
      springModifier = 0
    )
  }

  def priceScale(id: Int = Random.nextInt(), integrationId: Int = Random.nextInt()): PriceScale = {
    PriceScale(
      id = Some(id),
      name = "Name",
      venueId = Random.nextInt(),
      integrationId,
      createdAt = DateHelper.now(),
      modifiedAt = DateHelper.now()
    )
  }

  def inventoryItem(
    primaryEventId: Int = Random.nextInt(),
    primaryPriceScaleId: Option[Int] = None,
    priceFloor: Option[BigDecimal] = Some(15.00),
    isHeld: Boolean = Random.nextBoolean()
  ): InventoryItem = {
    InventoryItem(
      primaryEventId,
      primarySeatId = Some(Random.nextInt()),
      primaryPriceScaleId,
      section = "sec",
      row = "row",
      seat = "seat",
      listedPrice = Some(19.99),
      priceFloor = priceFloor,
      isHeld = isHeld
    )
  }

  def eventSeat(
    priceScaleId: Int = Random.nextInt(),
    isHeld: Boolean = Random.nextBoolean()
  ): EventSeat = {
    val now = DateHelper.now()
    EventSeat(
      id = Some(Random.nextInt()),
      seatId = Random.nextInt(),
      eventId = Random.nextInt(),
      priceScaleId,
      listedPrice = Some(19.99),
      overridePrice = Some(29.99),
      isListed = true,
      createdAt = now,
      modifiedAt = now,
      isHeld = isHeld
    )
  }

  def primaryPriceGuard(
    primaryEventId: Int = Random.nextInt(),
    cost: BigDecimal = 15.00
  ): PrimaryPriceGuard = {
    PrimaryPriceGuard(primaryEventId, section = "sec", row = "row", minPrice = cost)
  }
}
