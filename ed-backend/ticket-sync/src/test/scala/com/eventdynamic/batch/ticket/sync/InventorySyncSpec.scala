package com.eventdynamic.batch.ticket.sync

import java.time.Instant

import com.eventdynamic.batch.ticket.sync.services.{PrimaryService, ServiceHolder}
import com.eventdynamic.models.{EventSeat, PriceScale, Seat}
import com.eventdynamic.services._
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.{AsyncWordSpec, Matchers, PrivateMethodTester}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future

class InventorySyncSpec
    extends AsyncWordSpec
    with Matchers
    with MockitoSugar
    with PrivateMethodTester {
  case class Mocks(
    serviceHolder: ServiceHolder,
    priceScaleService: PriceScaleService,
    primaryService: PrimaryService,
    seatService: SeatService,
    eventSeatService: EventSeatService
  )

  def defaultMocks: Mocks = {
    val serviceHolder = mock[ServiceHolder]

    val priceScaleService = mock[PriceScaleService]
    val primaryService = mock[PrimaryService]
    val seatService = mock[SeatService]
    val eventSeatService = mock[EventSeatService]

    val priceScales = (1 to 5).map(idx => Generator.priceScale(integrationId = idx))
    val inventoryItems = (1 to 5).map(
      idx => Generator.inventoryItem(primaryEventId = idx, primaryPriceScaleId = Some(idx))
    )

    when(serviceHolder.priceScaleService) thenReturn priceScaleService
    when(serviceHolder.primaryService) thenReturn primaryService
    when(serviceHolder.seatService) thenReturn seatService
    when(serviceHolder.eventSeatService) thenReturn eventSeatService

    when(priceScaleService.getAllForClient(any[Int])) thenReturn Future.successful(priceScales)

    when(primaryService.getInventory(any[Seq[Int]], any[Instant], any[Instant])) thenReturn Future
      .successful(inventoryItems)

    when(seatService.bulkUpsert(any[Seq[Seat]])) thenReturn Future.successful(Seq(1))

    when(eventSeatService.bulkUpsert(any[Seq[EventSeat]])) thenReturn Future.successful(Seq(1))

    Mocks(serviceHolder, priceScaleService, primaryService, seatService, eventSeatService)
  }

  "InventorySync#syncInventory" should {
    "return the number of upserts" in {
      val mocks = defaultMocks
      val serviceHolder = mocks.serviceHolder

      val events = (1 to 5).map(primaryEventId => Generator.event(primaryEventId = primaryEventId))
      val start = Instant.now()
      val end = Instant.now()

      InventorySync
        .syncInventory(serviceHolder, events, start, end)
        .map(res => {
          verify(mocks.eventSeatService, times(1)).bulkUpsert(any[Seq[EventSeat]])
          verify(mocks.seatService, times(1)).bulkUpsert(any[Seq[Seat]])
          res shouldBe 1
        })
    }

    "return the number of upserts if inventory does not have a price floor" in {
      val mocks = defaultMocks
      val serviceHolder = mocks.serviceHolder

      val events = (1 to 5).map(primaryEventId => Generator.event(primaryEventId = primaryEventId))
      val start = Instant.now()
      val end = Instant.now()

      val inventoryItems = (1 to 5).map(
        idx => Generator.inventoryItem(primaryEventId = idx, primaryPriceScaleId = Some(idx), None)
      )
      when(
        serviceHolder.primaryService
          .getInventory(any[Seq[Int]], any[Instant], any[Instant])
      ) thenReturn Future
        .successful(inventoryItems)

      InventorySync
        .syncInventory(serviceHolder, events, start, end)
        .map(res => {
          verify(mocks.eventSeatService, times(1)).bulkUpsert(any[Seq[EventSeat]])
          verify(mocks.seatService, times(1)).bulkUpsert(any[Seq[Seat]])
          res shouldBe 1
        })
    }

    "stop if fetching the inventory fails" in {
      val mocks = defaultMocks
      val serviceHolder = mocks.serviceHolder
      val primaryService = mocks.primaryService

      when(primaryService.getInventory(any[Seq[Int]], any[Instant], any[Instant])) thenReturn Future
        .failed(new Exception("Error getting inventory"))

      val events = (1 to 5).map(primaryEventId => Generator.event(primaryEventId = primaryEventId))
      val start = Instant.now()
      val end = Instant.now()

      InventorySync
        .syncInventory(serviceHolder, events, start, end)
        .map(_ => fail())
        .recover {
          case e =>
            verify(mocks.eventSeatService, times(0)).bulkUpsert(any[Seq[EventSeat]])
            verify(mocks.seatService, times(0)).bulkUpsert(any[Seq[Seat]])
            e shouldBe a[Throwable]
        }
    }

    "stop if an upsert seat call fails" in {
      val mocks = defaultMocks
      val serviceHolder = mocks.serviceHolder
      val seatService = mocks.seatService

      when(seatService.bulkUpsert(any[Seq[Seat]])) thenReturn Future
        .failed(new Exception("Error upserting seat"))

      val events = (1 to 5).map(primaryEventId => Generator.event(primaryEventId = primaryEventId))
      val start = Instant.now()
      val end = Instant.now()

      InventorySync
        .syncInventory(serviceHolder, events, start, end)
        .map(_ => fail())
        .recover {
          case e =>
            verify(mocks.eventSeatService, times(0)).bulkUpsert(any[Seq[EventSeat]])
            e shouldBe a[Throwable]
        }
    }

    "stop if an upsert event seat call fails" in {
      val mocks = defaultMocks
      val serviceHolder = mocks.serviceHolder
      val eventSeatService = mocks.eventSeatService

      when(eventSeatService.bulkUpsert(any[Seq[EventSeat]])) thenReturn Future
        .failed(new Exception("Error upserting event seat"))

      val events = (1 to 5).map(primaryEventId => Generator.event(primaryEventId = primaryEventId))
      val start = Instant.now()
      val end = Instant.now()

      InventorySync
        .syncInventory(serviceHolder, events, start, end)
        .map(_ => fail())
        .recover {
          case e =>
            verify(mocks.seatService, times(1)).bulkUpsert(any[Seq[Seat]])
            e shouldBe a[Throwable]
        }
    }

    "guess the price scale if the inventory from the primary service does not have a price scale" in {
      val mocks = defaultMocks

      val serviceHolder = mocks.serviceHolder
      val primaryService = mocks.primaryService
      val eventSeatService = mocks.eventSeatService
      val seatService = mocks.seatService

      val inventoryItems = (1 to 5).map(
        idx => Generator.inventoryItem(primaryEventId = idx, primaryPriceScaleId = None)
      )
      when(primaryService.getInventory(any[Seq[Int]], any[Instant], any[Instant])) thenReturn Future
        .successful(inventoryItems)

      val events = (1 to 5).map(primaryEventId => Generator.event(primaryEventId = primaryEventId))
      val start = Instant.now()
      val end = Instant.now()

      InventorySync
        .syncInventory(serviceHolder, events, start, end)
        .map(res => {
          verify(eventSeatService, times(1)).bulkUpsert(any[Seq[EventSeat]])
          verify(seatService, times(1)).bulkUpsert(any[Seq[Seat]])
          res shouldBe 1
        })
    }

    "throw an exception if a price scale cannot be guessed" in {
      val mocks = defaultMocks

      val serviceHolder = mocks.serviceHolder
      val primaryService = mocks.primaryService
      val priceScaleService = mocks.priceScaleService

      val inventoryItems = (1 to 5).map(
        idx => Generator.inventoryItem(primaryEventId = idx, primaryPriceScaleId = None)
      )
      when(primaryService.getInventory(any[Seq[Int]], any[Instant], any[Instant])) thenReturn Future
        .successful(inventoryItems)

      when(priceScaleService.getAllForClient(any[Int])) thenReturn Future.successful(Seq())

      val events = (1 to 5).map(primaryEventId => Generator.event(primaryEventId = primaryEventId))
      val start = Instant.now()
      val end = Instant.now()

      InventorySync
        .syncInventory(serviceHolder, events, start, end)
        .map(_ => fail())
        .recover {
          case e =>
            verify(mocks.eventSeatService, times(0)).bulkUpsert(any[Seq[EventSeat]])
            verify(mocks.seatService, times(0)).bulkUpsert(any[Seq[Seat]])
            e shouldBe a[Throwable]
        }
    }
  }

  "InventorySync#guessPriceScale" should {
    val guessPriceScale = PrivateMethod[PriceScale]('guessPriceScale)

    "choose the first price scale" in {
      val priceScales = Seq(Generator.priceScale(), Generator.priceScale())

      val inventoryItem = Generator.inventoryItem(primaryEventId = 1, primaryPriceScaleId = None)

      val res = InventorySync invokePrivate guessPriceScale(priceScales, inventoryItem)
      res shouldBe priceScales.head
    }

    "throw an exception if there are no price scales" in {
      val priceScales = Seq()

      val inventoryItem = Generator.inventoryItem(primaryEventId = 1, primaryPriceScaleId = None)

      try {
        InventorySync invokePrivate guessPriceScale(priceScales, inventoryItem)
        fail()
      } catch {
        case e: Exception => e shouldBe a[Throwable]
      }
    }
  }
}
