package com.eventdynamic.batch.ticket.sync

import java.time.Instant

import com.eventdynamic.batch.ticket.sync.models.InventoryItem
import com.eventdynamic.batch.ticket.sync.services.{PrimaryService, ServiceHolder}
import com.eventdynamic.models.{Event, EventSeat, PriceGuard, PriceScale, Seat}
import com.eventdynamic.services._
import com.eventdynamic.utils.DateHelper
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.{AsyncWordSpec, Matchers, PrivateMethodTester}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future
import scala.util.Random

class PriceGuardSyncSpec
    extends AsyncWordSpec
    with Matchers
    with MockitoSugar
    with PrivateMethodTester {
  case class Mocks(
    serviceHolder: ServiceHolder,
    priceGuardService: PriceGuardService,
    primaryService: PrimaryService
  )

  def defaultMocks: Mocks = {
    val serviceHolder = mock[ServiceHolder]

    val priceGuardService = mock[PriceGuardService]
    val primaryService = mock[PrimaryService]

    when(serviceHolder.priceGuardService) thenReturn priceGuardService
    when(serviceHolder.primaryService) thenReturn primaryService

    val tickets = (1 to 5).map(idx => Generator.primaryPriceGuard(primaryEventId = idx))

    when(primaryService.getPriceGuards(any[Seq[Int]], any[Instant], any[Instant])) thenReturn Future
      .successful(tickets)
    when(primaryService.getPriceGuards(any[Seq[Int]], any[Instant], any[Instant])) thenReturn Future
      .successful(tickets)

    when(priceGuardService.bulkUpsert(any[Seq[PriceGuard]])) thenReturn Future.successful(Seq(1))
    when(priceGuardService.bulkUpsert(Seq())) thenReturn Future.successful(Seq(0))

    Mocks(serviceHolder, priceGuardService, primaryService)
  }

  "PriceGuardSync#syncPriceGuard" should {
    "return the number of upserts" in {
      val mocks = defaultMocks
      val serviceHolder = mocks.serviceHolder

      val events = (1 to 5).map(primaryEventId => Generator.event(primaryEventId = primaryEventId))
      val start = Instant.now()
      val end = Instant.now()

      PriceGuardSync
        .syncPriceGuards(serviceHolder, events, start, end)
        .map(res => {
          verify(mocks.priceGuardService, times(1)).bulkUpsert(any[Seq[PriceGuard]])
          res shouldBe 1
        })
    }

    "stop if an upsert price guard call fails" in {
      val mocks = defaultMocks
      val serviceHolder = mocks.serviceHolder
      val priceGuardService = mocks.priceGuardService

      when(priceGuardService.bulkUpsert(any[Seq[PriceGuard]])) thenReturn Future
        .failed(new Exception("Error upserting price guard"))

      val events = (1 to 5).map(primaryEventId => Generator.event(primaryEventId = primaryEventId))
      val start = Instant.now()
      val end = Instant.now()

      PriceGuardSync
        .syncPriceGuards(serviceHolder, events, start, end)
        .map(_ => fail())
        .recover {
          case e =>
            verify(mocks.priceGuardService, times(1)).bulkUpsert(any[Seq[PriceGuard]])
            e shouldBe a[Throwable]
        }
    }
  }
}
