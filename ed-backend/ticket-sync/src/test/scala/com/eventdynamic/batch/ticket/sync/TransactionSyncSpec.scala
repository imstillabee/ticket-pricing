package com.eventdynamic.batch.ticket.sync

import java.sql.Timestamp
import java.time.Instant
import java.time.temporal.ChronoUnit

import com.eventdynamic.batch.ticket.sync.models.PrimaryTransaction
import com.eventdynamic.batch.ticket.sync.services.{ServiceHolder, SkyboxPrimaryService}
import com.eventdynamic.models.{EventSeatByExternalSeatLocator, Transaction, TransactionType}
import com.eventdynamic.services.{
  EventSeatService,
  RevenueCategoryMappingService,
  TransactionService
}
import com.eventdynamic.utils.RandomModelUtil
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.AsyncWordSpec
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future

class TransactionSyncSpec extends AsyncWordSpec with MockitoSugar {
  // Constants
  private val clientId = 1
  private val start = Instant.EPOCH
  private val end = start.plus(7, ChronoUnit.DAYS)
  private val events = (1 to 5).map(idx => RandomModelUtil.event(primaryEventId = Some(idx)))

  def primaryTransaction(
    primaryTransactionId: Int = RandomModelUtil.int(),
    seat: String = RandomModelUtil.string()
  ): PrimaryTransaction = {
    PrimaryTransaction(
      primaryTransactionId = primaryTransactionId.toString,
      timestamp = new Timestamp(0),
      price = 19.99,
      buyerTypeCode = None,
      integrationId = 12,
      transactionType = TransactionType.Purchase,
      eventSeatIdentifier = EventSeatByExternalSeatLocator(
        primaryEventId = 276,
        section = "section",
        row = "row",
        seat = seat
      )
    )
  }

  // Setup mocks
  private val primaryService = mock[SkyboxPrimaryService]
  private val primaryTransactions = (1 to 5).map(_ => primaryTransaction()).toVector
  when(primaryService.getTransactionsForEvents(any[Instant], any[Instant], any[Seq[Int]]))
    .thenReturn(Future.successful(primaryTransactions))

  private val eventSeatService = mock[EventSeatService]
  when(eventSeatService.getOne(any[EventSeatByExternalSeatLocator])).thenAnswer(req => {
    val arg = req.getArgumentAt(0, classOf[EventSeatByExternalSeatLocator])
    Future.successful(Some(RandomModelUtil.eventSeat(eventId = arg.primaryEventId)))
  })

  private val revenueCategoryMappingService = mock[RevenueCategoryMappingService]
  when(revenueCategoryMappingService.regexMatch(any[Int], any[String]))
    .thenReturn(Future.successful(None))

  private val transactionService = mock[TransactionService]
  when(transactionService.bulkUpsert(any[Seq[Transaction]])).thenAnswer(req => {
    val args = req.getArgumentAt(0, classOf[Seq[Transaction]])
    Future.successful(args.length)
  })

  private val serviceHolder = mock[ServiceHolder]
  when(serviceHolder.primaryService).thenReturn(primaryService)
  when(serviceHolder.eventSeatService).thenReturn(eventSeatService)
  when(serviceHolder.revenueCategoryMappingService).thenReturn(revenueCategoryMappingService)
  when(serviceHolder.transactionService).thenReturn(transactionService)

  "TransactionSync#syncTransactions" should {
    "upsert new transactions from the primary" in {

      TransactionSync
        .syncTransactions(
          s = serviceHolder,
          start = start,
          end = end,
          events = events,
          clientId = clientId
        )
        .map(res => {
          verify(primaryService, times(1))
            .getTransactionsForEvents(start, end, events.map(_.primaryEventId.get))

          verify(eventSeatService, times(primaryTransactions.length))
            .getOne(any[EventSeatByExternalSeatLocator])

          verify(revenueCategoryMappingService, times(primaryTransactions.length))
            .regexMatch(clientId, TransactionSync.defaultBuyerTypeCode)

          verify(transactionService, times(1)).bulkUpsert(any[Seq[Transaction]])

          assert(res == primaryTransactions.length)
        })
    }
  }
}
