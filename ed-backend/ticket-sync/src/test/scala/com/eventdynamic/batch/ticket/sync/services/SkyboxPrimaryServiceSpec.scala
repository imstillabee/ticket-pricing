package com.eventdynamic.batch.ticket.sync.services

import java.sql.Timestamp
import java.time.Instant
import java.time.temporal.ChronoUnit

import com.eventdynamic.batch.ticket.sync.models.PrimaryTransaction
import com.eventdynamic.models.{
  ClientIntegrationComposite,
  EventSeatByExternalSeatLocator,
  TransactionType
}
import com.eventdynamic.skybox.SkyboxService
import com.eventdynamic.skybox.models.{
  SkyboxInventoryGetResponse,
  SkyboxListing,
  SkyboxSoldInventory,
  SkyboxSoldInventoryGetResponse
}
import com.eventdynamic.utils.DateHelper
import com.softwaremill.sttp.Response
import org.mockito.Matchers._
import org.mockito.Mockito.when
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.{AsyncWordSpec, BeforeAndAfterEach, Matchers}
import com.eventdynamic.services.ClientIntegrationService

import scala.concurrent.Future

class SkyboxPrimaryServiceSpec
    extends AsyncWordSpec
    with MockitoSugar
    with BeforeAndAfterEach
    with ScalaFutures
    with Matchers {

  // constants
  val eventIds: Seq[Int] = 1 to 10
  val now: Timestamp = DateHelper.now()
  val beforeDate: Instant = Instant.now()
  val afterDate: Instant = beforeDate.minus(1, ChronoUnit.DAYS)

  // services
  var skyboxService: SkyboxService = mock[SkyboxService]
  var clientIntegrationService: ClientIntegrationService = mock[ClientIntegrationService];

  var customerId = 2345
  var subsidiaryIntegrationId = 7

  var json =
    s"""{
       |"appKey": "appKey",
       |"baseUrl": "baseUrl",
       |"accountId": 1,
       |"apiKey": "appKey",
       |"defaultVendorId": 1,
       |"customers": {"$customerId": $subsidiaryIntegrationId},
       |"customersToIgnore": ["11"]
       |}
       |""".stripMargin

  var clientIntegrationComposite = new ClientIntegrationComposite(
    id = Some(1),
    createdAt = now,
    modifiedAt = now,
    clientId = 10,
    integrationId = 5,
    name = "Skybox - Vivid Seats",
    version = None,
    json = Some(json),
    isActive = true,
    isPrimary = false,
    logoUrl = None,
    percent = None,
    constant = None
  )

  var skyboxPrimaryService: SkyboxPrimaryService =
    new SkyboxPrimaryService(skyboxService, clientIntegrationComposite)

  def successResponse[T](body: T): Response[T] = {
    Response(Right(body), 200, "success", scala.collection.immutable.Seq(), List())
  }

  def failResponse[T](): Response[T] = {
    Response(
      Left("Not authorized"),
      401,
      "not authorized",
      scala.collection.immutable.Seq(),
      List()
    )
  }

  // clearing each mocked service
  override def beforeEach(): Unit = {
    super.beforeEach()

    skyboxService = mock[SkyboxService]

    skyboxPrimaryService = new SkyboxPrimaryService(skyboxService, clientIntegrationComposite)
  }

  // tests
  "SkyboxPrimaryService#getTransactionsForEvents" should {
    "throw an exception for a skybox error" in {
      when(skyboxService.getInvoices(any[Seq[Int]], any[Instant], any[Instant]))
        .thenReturn(Future.successful(failResponse[SkyboxSoldInventoryGetResponse]()))

      skyboxPrimaryService
        .getTransactionsForEvents(afterDate, beforeDate, eventIds)
        .map(_ => fail())
        .recover {
          case e => e shouldBe a[Throwable]
        }
    }

    "should ignore customersToIgnore" in {
      val primaryEventId = 1
      val section = "123"
      val row = "10"
      val seat = "1"
      val price: BigDecimal = 100.0

      when(skyboxService.getInvoices(any[Seq[Int]], any[Instant], any[Instant]))
        .thenReturn({
          val res = successResponse[SkyboxSoldInventoryGetResponse](
            SkyboxSoldInventoryGetResponse(
              Seq(
                SkyboxSoldInventory(
                  id = 1,
                  invoiceDate = now.toInstant,
                  unitTicketSales = price,
                  section,
                  row,
                  seatNumbers = seat,
                  primaryEventId,
                  11
                )
              )
            )
          )
          Future.successful(res)

        })

      skyboxPrimaryService
        .getTransactionsForEvents(afterDate, beforeDate, eventIds)
        .map(result => {
          assert(result.isEmpty)
        })
    }

    "return a future primary transaction vector" in {
      val primaryEventId = 1
      val section = "123"
      val row = "10"
      val seat = "1"
      val price: BigDecimal = 100.0

      val expectedResult: PrimaryTransaction =
        PrimaryTransaction(
          primaryTransactionId = "1",
          timestamp = now,
          price,
          buyerTypeCode = None,
          integrationId = subsidiaryIntegrationId,
          transactionType = TransactionType.Purchase,
          eventSeatIdentifier = EventSeatByExternalSeatLocator(primaryEventId, section, row, seat)
        )

      when(skyboxService.getInvoices(any[Seq[Int]], any[Instant], any[Instant]))
        .thenReturn({
          val res = successResponse[SkyboxSoldInventoryGetResponse](
            SkyboxSoldInventoryGetResponse(
              Seq(
                SkyboxSoldInventory(
                  id = 1,
                  invoiceDate = now.toInstant,
                  unitTicketSales = price,
                  section,
                  row,
                  seatNumbers = seat,
                  primaryEventId,
                  customerId
                )
              )
            )
          )
          Future.successful(res)

        })

      skyboxPrimaryService
        .getTransactionsForEvents(afterDate, beforeDate, eventIds)
        .map(result => {
          assert(result.length == 1 && result.head == expectedResult)
        })
    }
  }

  "SkyboxPrimaryService#getInventory" should {
    "fetch inventory with list price and unit cost average" in {
      val start = Instant.EPOCH
      val end = Instant.ofEpochSecond(1000)
      val eventIds = Seq(123)

      val section = "AAA"
      val row = "12"
      val seats = Seq(1, 2, 3)

      val inventoryIds = Seq(12345, 23456, 34567)

      val listing = SkyboxListing(
        id = 123,
        eventId = eventIds.head,
        section,
        row,
        listPrice = Some(1000.0),
        status = "status",
        lowSeat = seats.head,
        highSeat = seats.last,
        unitCostAverage = Some(850.0)
      )

      when(skyboxService.getInventory(eventIds, start, end))
        .thenReturn(Future.successful(successResponse(SkyboxInventoryGetResponse(Seq(listing)))))

      when(skyboxService.getSoldInventory(eventIds, start, end))
        .thenReturn(Future.successful(successResponse(SkyboxInventoryGetResponse(Seq(listing)))))

      for {
        res <- skyboxPrimaryService.getInventory(eventIds, start, end)
      } yield {
        res.length shouldBe seats.length * 2
      }
    }

    "SkyboxPrimaryService#getInventory" should {
      "fetch inventory with list price and without unit cost average" in {
        val start = Instant.EPOCH
        val end = Instant.ofEpochSecond(1000)
        val eventIds = Seq(123)

        val section = "AAA"
        val row = "12"
        val seats = Seq(1, 2, 3)

        val listing = SkyboxListing(
          id = 123,
          eventId = eventIds.head,
          section,
          row,
          listPrice = Some(1000.0),
          status = "status",
          lowSeat = seats.head,
          highSeat = seats.last,
          unitCostAverage = None
        )

        when(skyboxService.getInventory(eventIds, start, end))
          .thenReturn(Future.successful(successResponse(SkyboxInventoryGetResponse(Seq(listing)))))

        when(skyboxService.getSoldInventory(eventIds, start, end))
          .thenReturn(Future.successful(successResponse(SkyboxInventoryGetResponse(Seq(listing)))))

        for {
          res <- skyboxPrimaryService.getInventory(eventIds, start, end)
        } yield {
          res.length shouldBe seats.length * 2
        }
      }
    }

    "SkyboxPrimaryService#getInventory" should {
      "fetch inventory without list price and with unit cost average" in {
        val start = Instant.EPOCH
        val end = Instant.ofEpochSecond(1000)
        val eventIds = Seq(123)

        val section = "AAA"
        val row = "12"
        val seats = Seq(1, 2, 3)

        val listing = SkyboxListing(
          id = 123,
          eventId = eventIds.head,
          section,
          row,
          listPrice = None,
          status = "status",
          lowSeat = seats.head,
          highSeat = seats.last,
          unitCostAverage = Some(850.0)
        )

        when(skyboxService.getInventory(eventIds, start, end))
          .thenReturn(Future.successful(successResponse(SkyboxInventoryGetResponse(Seq(listing)))))

        when(skyboxService.getSoldInventory(eventIds, start, end))
          .thenReturn(Future.successful(successResponse(SkyboxInventoryGetResponse(Seq(listing)))))

        for {
          res <- skyboxPrimaryService.getInventory(eventIds, start, end)
        } yield {
          res.length shouldBe seats.length * 2
        }
      }
    }

    "fetch inventory without list price and without unit cost average" in {
      val start = Instant.EPOCH
      val end = Instant.ofEpochSecond(1000)
      val eventIds = Seq(123)

      val section = "AAA"
      val row = "12"
      val seats = Seq(1, 2, 3)

      val listing = SkyboxListing(
        id = 123,
        eventId = eventIds.head,
        section,
        row,
        listPrice = None,
        status = "status",
        lowSeat = seats.head,
        highSeat = seats.last,
        unitCostAverage = None
      )

      when(skyboxService.getInventory(eventIds, start, end))
        .thenReturn(Future.successful(successResponse(SkyboxInventoryGetResponse(Seq(listing)))))

      when(skyboxService.getSoldInventory(eventIds, start, end))
        .thenReturn(Future.successful(successResponse(SkyboxInventoryGetResponse(Seq(listing)))))

      for {
        res <- skyboxPrimaryService.getInventory(eventIds, start, end)
      } yield {
        res.length shouldBe seats.length * 2
      }
    }
  }
}
