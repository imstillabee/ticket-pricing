package com.eventdynamic.batch.ticket.sync.services
import java.sql.Timestamp
import java.time.Instant
import java.time.temporal.ChronoUnit

import com.eventdynamic.batch.ticket.sync.models.PrimaryTransaction
import com.eventdynamic.models.{
  ClientIntegrationComposite,
  EventSeatByExternalSeatLocator,
  TransactionType
}
import com.eventdynamic.utils.DateHelper
import com.softwaremill.sttp.Response
import org.mockito.Matchers._
import org.mockito.Mockito.when
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.{AsyncWordSpec, BeforeAndAfterEach, Matchers}
import com.eventdynamic.services.ClientIntegrationService
import com.eventdynamic.tdcreplicatedproxy.TdcReplicatedProxyService
import com.eventdynamic.tdcreplicatedproxy.models.{TdcInventory, TdcTransaction}

import scala.concurrent.Future

class TDCPrimaryServiceSpec
    extends AsyncWordSpec
    with MockitoSugar
    with BeforeAndAfterEach
    with ScalaFutures
    with Matchers {

  // constants
  val eventIds: Seq[Int] = 1 to 10
  val now: Timestamp = DateHelper.now()
  val beforeDate: Instant = Instant.now()
  val afterDate: Instant = beforeDate.minus(1, ChronoUnit.DAYS)

  // services
  var tdcService: TdcReplicatedProxyService = mock[TdcReplicatedProxyService]
  var clientIntegrationService: ClientIntegrationService = mock[ClientIntegrationService]

  var json =
    s"""{
       |"agent": "agent",
       |"apiKey": "apiKey",
       |"appId": "appId",
       |"baseUrl": "",
       |"password": "",
       |"username": "",
       |"supplierId": 1121,
       |"tdcProxyBaseUrl": "baseUrl",
       |"mlbamGridConfigs": [
       |  {
       |    "priceGroupId": 4792111,
       |    "externalBuyerTypeIds": ["1"]
       |  },
       |  {
       |    "priceGroupId": 5597093,
       |    "externalBuyerTypeIds": ["1185"]
       |  }
       |]
       |}""".stripMargin

  var clientIntegrationComposite = new ClientIntegrationComposite(
    id = Some(1),
    createdAt = now,
    modifiedAt = now,
    clientId = 10,
    integrationId = 5,
    name = "Tickets.com",
    version = None,
    json = Some(json),
    isActive = true,
    isPrimary = true,
    logoUrl = None,
    percent = None,
    constant = None
  )

  var tdcPrimaryService: TDCPrimaryService =
    new TDCPrimaryService(tdcService, clientIntegrationComposite)

  def successResponse[T](body: T): Response[T] = {
    Response(Right(body), 200, "success", scala.collection.immutable.Seq(), List())
  }

  def failResponse[T](): Response[T] = {
    Response(
      Left("Bad server response"),
      400,
      "bad server response",
      scala.collection.immutable.Seq(),
      List()
    )
  }

  // clearing each mocked service
  override def beforeEach(): Unit = {
    super.beforeEach()

    tdcService = mock[TdcReplicatedProxyService]

    tdcPrimaryService = new TDCPrimaryService(tdcService, clientIntegrationComposite)
  }

  // tests
  "TDCPrimaryService#getInventory" should {
    "return inventory item with isHeld = false based on 'O' hold code type" in {
      val eventId = 123
      val start = Instant.EPOCH
      val end = Instant.ofEpochSecond(1000)
      val eventIds = Seq(eventId)

      val openInventory = TdcInventory(
        eventId = eventId,
        seatId = 1,
        priceScaleId = 1,
        createdAt = Instant.now(),
        modifiedAt = Instant.now().minus(1, ChronoUnit.DAYS),
        listedPrice = Some(100.50),
        section = "1",
        row = "1",
        seatNumber = "1",
        holdCodeType = "O"
      )

      when(
        tdcService
          .getInventory(eventIds, start, end)
      ).thenReturn(Future.successful(successResponse(Seq(openInventory))))

      for {
        result <- tdcPrimaryService.getInventory(eventIds, start, end)
      } yield {
        result.length shouldBe 1
        val inventoryItem = result.head
        inventoryItem.isHeld shouldBe false
      }
    }

    "return inventory item with isHeld = true based on 'H' hold code type" in {
      val eventId = 123
      val start = Instant.EPOCH
      val end = Instant.ofEpochSecond(1000)
      val eventIds = Seq(eventId)

      val heldInventory = TdcInventory(
        eventId = eventId,
        seatId = 2,
        priceScaleId = 1,
        createdAt = Instant.now(),
        modifiedAt = Instant.now().minus(1, ChronoUnit.DAYS),
        listedPrice = Some(50.50),
        section = "1",
        row = "1",
        seatNumber = "2",
        holdCodeType = "H"
      )

      when(
        tdcService
          .getInventory(eventIds, start, end)
      ).thenReturn(Future.successful(successResponse(Seq(heldInventory))))

      for {
        result <- tdcPrimaryService.getInventory(eventIds, start, end)
      } yield {
        result.length shouldBe 1
        val inventoryItem = result.head
        inventoryItem.isHeld shouldBe true
      }
    }

    "return inventory item with isHeld = true based on 'B' hold code type" in {
      val eventId = 123
      val start = Instant.EPOCH
      val end = Instant.ofEpochSecond(1000)
      val eventIds = Seq(eventId)

      val blackoutInventory = TdcInventory(
        eventId = eventId,
        seatId = 3,
        priceScaleId = 1,
        createdAt = Instant.now(),
        modifiedAt = Instant.now().minus(1, ChronoUnit.DAYS),
        listedPrice = Some(50.50),
        section = "1",
        row = "1",
        seatNumber = "3",
        holdCodeType = "B"
      )

      when(
        tdcService
          .getInventory(eventIds, start, end)
      ).thenReturn(Future.successful(successResponse(Seq(blackoutInventory))))

      for {
        result <- tdcPrimaryService.getInventory(eventIds, start, end)
      } yield {
        result.length shouldBe 1
        val inventoryItem = result.head
        inventoryItem.isHeld shouldBe true
      }
    }

    "throw an exception for TDC error" in {
      val eventId = 123
      val start = Instant.EPOCH
      val end = Instant.ofEpochSecond(1000)
      val eventIds = Seq(eventId)

      when(
        tdcService
          .getInventory(eventIds, start, end)
      ).thenReturn(Future.successful(failResponse[Seq[TdcInventory]]()))

      tdcPrimaryService
        .getInventory(eventIds, start, end)
        .map(_ => fail())
        .recover {
          case e => e shouldBe a[Throwable]
        }
    }

    "return empty sequence when getting no inventory back" in {
      val eventId = 123
      val start = Instant.EPOCH
      val end = Instant.ofEpochSecond(1000)
      val eventIds = Seq(eventId)

      when(
        tdcService
          .getInventory(eventIds, start, end)
      ).thenReturn(Future.successful(successResponse[Seq[TdcInventory]](Seq())))

      for {
        result <- tdcPrimaryService.getInventory(eventIds, start, end)
      } yield result.length shouldBe 0
    }
  }

  "TDCPrimaryService#getTransactionsForEvents" should {
    "purchase tdc transactions" in {
      val eventId = 123
      val start = Instant.EPOCH
      val end = Instant.ofEpochSecond(1000)
      val eventIds = Seq(eventId)

      val purchaseTransaction = TdcTransaction(
        id = 1,
        createdAt = Instant.now(),
        timestamp = Instant.now().minus(1, ChronoUnit.DAYS),
        eventId = 123,
        buyerTypeCode = "Adult",
        seatId = 1,
        price = 100.50,
        section = "1",
        row = "1",
        seatNumber = "1",
        transactionTypeCode = "SA",
        priceScaleId = 1123,
        holdCodeType = "O"
      )

      when(
        tdcService
          .getTransactions(start, end, eventIds)
      ).thenReturn(Future.successful(successResponse(Seq(purchaseTransaction))))

      for {
        result <- tdcPrimaryService.getTransactionsForEvents(start, end, eventIds)
      } yield {
        result.length shouldBe 1
        val primaryTransaction = result.head
        primaryTransaction.transactionType shouldBe TransactionType.Purchase
      }
    }

    "return tdc transactions" in {
      val eventId = 123
      val start = Instant.EPOCH
      val end = Instant.ofEpochSecond(1000)
      val eventIds = Seq(eventId)

      val purchaseTransaction = TdcTransaction(
        id = 1,
        createdAt = Instant.now(),
        timestamp = Instant.now().minus(1, ChronoUnit.DAYS),
        eventId = 123,
        buyerTypeCode = "Adult",
        seatId = 1,
        price = 100.50,
        section = "1",
        row = "1",
        seatNumber = "1",
        transactionTypeCode = "ER",
        priceScaleId = 1123,
        holdCodeType = "O"
      )

      when(
        tdcService
          .getTransactions(start, end, eventIds)
      ).thenReturn(Future.successful(successResponse(Seq(purchaseTransaction))))

      for {
        result <- tdcPrimaryService.getTransactionsForEvents(start, end, eventIds)
      } yield {
        result.length shouldBe 1
        val primaryTransaction = result.head
        primaryTransaction.transactionType shouldBe TransactionType.Return
      }
    }

    "handle empty response from tdc proxy" in {
      val eventId = 123
      val start = Instant.EPOCH
      val end = Instant.ofEpochSecond(1000)
      val eventIds = Seq(eventId)

      when(
        tdcService
          .getTransactions(start, end, eventIds)
      ).thenReturn(Future.successful(successResponse[Seq[TdcTransaction]](Seq())))

      for {
        result <- tdcPrimaryService.getTransactionsForEvents(start, end, eventIds)
      } yield {
        result.length shouldBe 0
      }
    }

    "throw exception for tdc error" in {
      val eventId = 123
      val start = Instant.EPOCH
      val end = Instant.ofEpochSecond(1000)
      val eventIds = Seq(eventId)

      when(
        tdcService
          .getTransactions(start, end, eventIds)
      ).thenReturn(Future.successful(failResponse[Seq[TdcTransaction]]()))

      tdcPrimaryService
        .getTransactionsForEvents(start, end, eventIds)
        .map(_ => fail())
        .recover {
          case e => e shouldBe a[Throwable]
        }
    }
  }
}
