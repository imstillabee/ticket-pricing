# Event Dynamic tests

## UI automated tests using Cypress

1. `cd e2e` && `yarn install` or `yarn` if you haven't already 

2. Create a `.env` file from the `.env.example` file -> simply run `yarn set:env:cypress` to get started.

3. To run UI automated tests interactively: `yarn test`

4. To run cypress tests headlessly (like we do in ci): `yarn test:ci`