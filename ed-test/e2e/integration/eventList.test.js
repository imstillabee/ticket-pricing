const { eventList } = require('../pages');
let searchTerm;

const pickOne = collection => {
  return collection[Math.floor(Math.random() * collection.length)];
};

describe('Event List', function () {

  beforeEach(function () {
    // Define Test Data Fixtures
    cy.fixture('user.json').as('user')
      .then(user => {
        cy.loginViaAPI(user.email, Cypress.env('ED_USER_PASS'));
      });
    cy.fixture('events.json').as('events');

    // Define Routes to wait on / stub
    cy.server()
      .route(`${Cypress.env('API_URL')}/events*`).as('getEvents');
    // Visit Event List
    cy.visit('/season');
  });

  it('will return events when they match the search term', function () {
    searchTerm = 'Braves';

    eventList.countEventCards()
      .then(initialCardCount => {
        eventList.search(searchTerm);
        eventList.cards
          .should('have.length.below', initialCardCount)
          .and('contain', searchTerm);
      });
  });

  it('will show no results when no events match', function () {
    searchTerm = 'afksfdfk';

    eventList.search(searchTerm);
    eventList.cards.should('have.length', 0);
    eventList.emptyStateMessage
      .should('be.visible')
      .and('have.text', 'No events matching query');
  });

  it('will allow fuzzy searching', function () {

    searchTerm = 'bravs';
    eventList.stubData(this.events);

    eventList.search(searchTerm);
    eventList.cards
      .should('have.length', 1)
      .contains('h4', 'Braves');
  });

  it('navigates to the event page when clicked', function () {

    const event = pickOne(this.events);
    cy.log(`Using event.id: ${event.id}`);
    eventList.stubData(this.events);

    eventList.clickCardWithText(event.name);
    cy.url().should('include', `/event/${event.id}`);
  });
});
