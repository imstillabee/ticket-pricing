const { loginPage, header } = require('../pages');

describe('Login', function () {

  beforeEach(function () {
    cy.fixture('user.json').as('user');
    cy.visit('/');
  });

  describe('A user with valid credentials', function () {

    it('is routed to the /season page after login', function () {
      cy.server()
        .route(`${Cypress.env('API_URL')}/**`, [])
        .route(`${Cypress.env('API_URL')}/me*`);

      loginPage.loginViaUI(this.user.email, Cypress.env('ED_USER_PASS'))
        .url().should('include', '/season');
    });

  });

  describe('A user with invalid credentials', function () {

    before(function () {
      if (Cypress.env('ID_COOKIE')) {
        cy.logoutViaApi();
      }
    });

    it('cannot access authenticated pages', function () {

      loginPage.loginViaUI(this.user.email, 'bad_password');
      loginPage.errorMessage.should('be.visible');
      cy.visit('/season')
        .url().should('not.include', '/season');
      header.settingsIcon.should('not.exist');
    });
  });
});
