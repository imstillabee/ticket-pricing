const { header } = require('../pages');

describe('Logout', function () {

  beforeEach(function () {
    cy.fixture('user.json').as('user');
  });

  describe('A user logged into the application', function () {

    it('is routed to the /login page after logging out', function () {
      cy.loginViaAPI(this.user.email, Cypress.env('ED_USER_PASS'));
      cy.visit('/');
      header.logout();

      cy.url().should('include', '/login');
    });
  });

  describe('A user logged out of the application', function () {

    it('cannot access authenticated pages', function () {

      cy.server()
        .route(`${Cypress.env('API_URL')}/**`, [])
        .route(`${Cypress.env('API_URL')}/me*`)
        .route('DELETE', `${Cypress.env('API_URL')}/auth*`).as('deleteAuth');

      cy.loginViaAPI(this.user.email, Cypress.env('ED_USER_PASS'))
        .visit('/');
      header.logout()
        .wait('@deleteAuth')
        .visit('/season')
        .url().should('include', '/login');
    });
  });

});
