const { header, settingsSideBar, userSettingsPage } = require('../pages');
const Chance = require('chance');
const chance = new Chance();

describe('User Settings', function() {

  beforeEach(function () {
    cy.fixture('user.json').as('user')
      .then(user => {
        cy.loginViaAPI(user.email, Cypress.env('ED_USER_PASS'));
      });
    cy.visit('/season');
  });

  it('will update the user`s name', function () {
    const firstName = chance.first();
    const lastName = chance.last();

    header.navToSettings();

    settingsSideBar.clickUserSettings();

    console.log('type: ', typeof userSettingsPage.fullNameText);

    userSettingsPage.fullNameText
      .should('not.contain', firstName)
      .and('not.contain', lastName);
    userSettingsPage.updateName(firstName, lastName);
    userSettingsPage.fullNameText
      .should('contain', firstName)
      .and('contain', lastName);
  });

  it('will update the user`s name in the header', function () {
    const firstName = chance.first().toLowerCase();
    const lastName = chance.last().toLowerCase();

    header.navToSettings();

    settingsSideBar.clickUserSettings();

    cy.window().then((window) => {
      const headerName = header.getHeadingName(window).toLowerCase();

      expect(headerName.includes(firstName)).to.be.false;
      expect(headerName.includes(lastName)).to.be.false;
    });

    userSettingsPage.updateName(firstName, lastName);

    cy.window().then((window) => {
      const headerName = header.getHeadingName(window).toLowerCase();

      expect(headerName.includes(firstName)).to.be.true;
      expect(headerName.includes(lastName)).to.be.true;
    });


  });

  it('will update the user`s phone number', function () {
    const phoneNumber = chance.phone({ formatted: false });

    header.navToSettings();

    settingsSideBar.clickUserSettings();

    userSettingsPage.phoneNumberText.should('not.contain', phoneNumber);
    userSettingsPage.updatePhoneNumber(phoneNumber);
    userSettingsPage.phoneNumberText.should('not.contain', phoneNumber);

  });
});
