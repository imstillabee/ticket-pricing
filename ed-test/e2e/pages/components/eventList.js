module.exports = {

  get cards() { return cy.get('[data-test-id="event-list-card"]'); },
  get searchField() { return cy.get('[data-test-id="event-list-search"]'); },
  get emptyStateMessage() { return cy.get('[data-test-id="no-events-message"]'); },

  isLoaded() {
    return cy.wait('@getEvents');
  },
  countEventCards() {
    return this.cards.its('length');
  },
  search(term) {
    return this.searchField.type(term);
  },
  clickCardWithText(text) {
    return this.cards.contains(text)
      .parent()
      .parent()
      .should('not.have.class', 'fMDvjj')
      .click();
  },
  stubData(data) {
    return cy.route(`${Cypress.env('API_URL')}/events*`, data).as('getEvents');

  }
};
