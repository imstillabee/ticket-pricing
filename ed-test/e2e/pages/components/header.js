module.exports = {

  get userNameText() { return cy.get('[data-test-id="user-name-text"]'); },
  get settingsIcon() { return cy.get('[data-test-id="settings-icon"]'); },
  get settingsButton() { return cy.get('[data-test-id="settings-button"]'); },
  get logoutButton() { return cy.get('[data-test-id="logout-button"]'); },

  clickLogout() {
    return this.logoutButton.click();
  },

  clickSettingsIcon() {
    return this.settingsIcon.click();
  },

  clickSettings() {
    return this.settingsButton.click();
  },

  navToSettings() {
    this.clickSettingsIcon();
    return this.clickSettings();
  },

  logout() {
    this.clickSettingsIcon();
    return this.clickLogout();
  },

  getHeadingName(cypressWindow) {
    return window.getComputedStyle(cypressWindow.document.querySelector('[data-test-id="user-name-text"]'), ':before').content;
  }

};
