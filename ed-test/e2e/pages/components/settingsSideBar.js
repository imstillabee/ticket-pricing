module.exports = {
  get userSettingsButton() { return cy.get('[data-test-id="user-settings-button"]'); },

  clickUserSettings() {
    return this.userSettingsButton.click();
  }
};
