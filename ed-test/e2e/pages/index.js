module.exports = {
  loginPage: require('./loginPage'),
  header: require('./components/header'),
  settingsSideBar: require('./components/settingsSideBar'),
  userSettingsPage: require('./userSettingsPage'),
  eventList: require('./components/eventList')
};
