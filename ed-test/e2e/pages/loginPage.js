module.exports = {

  get emailInput() { return cy.get('[data-test-id="email-input"]'); },
  get passwordInput() { return cy.get('[data-test-id="password-input"]'); },
  get loginButton() { return cy.get('[data-test-id="login-button"]'); },
  get errorMessage() { return cy.get('div').contains('Incorrect Email Address or Password.'); },

  loginViaUI(email, password) {
    cy.server()
      .route('POST', `${Cypress.env('API_URL')}/auth*`).as('postAuth');
    this.emailInput.type(email);
    this.passwordInput.type(password);
    this.loginButton.click();
    return cy.wait('@postAuth');
  }
};

