module.exports = {

  get fullNameText() { return cy.get('[data-test-id="full-name-text"]'); },
  get editNameButton() { return cy.get('[data-test-id="edit-name-button"]'); },
  get firstNameInput() { return cy.get('[data-test-id="first-name-input"]'); },
  get lastNameInput() { return cy.get('[data-test-id="last-name-input"]'); },
  get saveNameButton() { return cy.get('[data-test-id="save-name-button"]'); },

  get phoneNumberText() { return cy.get('[data-test-id="phone-number-text"]'); },
  get editPhoneNumberButton() { return cy.get('[data-test-id="edit-phone-number-button"]'); },
  get phoneNumberInput() { return cy.get('[data-test-id="phone-number-input"]'); },
  get savePhoneNumberButton() { return cy.get('[data-test-id="save-phone-number-button"]'); },

  clickEditNameButton() {
    return this.editNameButton.click();
  },

  getFullNameText() {
    return this.fullNameText;
  },

  updateName(firstName, lastName) {
    this.clickEditNameButton();
    this.firstNameInput.clear().type(firstName);
    this.lastNameInput.clear().type(lastName);
    return this.saveNameButton.click();
  },

  clickEditPhoneNumberButton() {
    return this.editPhoneNumberButton.click();
  },

  updatePhoneNumber(phoneNumber) {
    this.clickEditPhoneNumberButton();
    this.phoneNumberInput.clear().type(phoneNumber);
    return this.savePhoneNumberButton.click();
  }

};
