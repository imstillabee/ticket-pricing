require('dotenv').config();
const assert = require('assert');

module.exports = (on, baseConfig) => {

  const config = {
    baseUrl: process.env.BASE_URL || 'http://qa.eventdynamic.com',
    env: {
      ED_USER_PASS: process.env.ED_USER_PASS || assert(false, 'Set ED_USER_PASS in your .env file'),
      API_URL: process.env.API_URL || 'http://qaapi.eventdynamic.com:10011',
      BROWSER: (process.env.ENV === 'ci') ? 'electron' : 'chrome'
    }
  };

  return Object.assign(baseConfig, config);
};
