const headers = {
  'Accept-Language': (Cypress.env('BROWSER') === 'chrome') ? 'en-US,en;q=0.9' : 'en-US'
};

function loginRequest (email, password) {

  const options = {
    url: `${Cypress.env('API_URL')}/auth`,
    method: 'POST',
    body: { email, password },
    headers
  };

  cy.request(options).then(res => {
    if (res.status === 200) {
      Cypress.env('ID_COOKIE', res.headers['set-cookie'][0]);
    }
  });
}

Cypress.Commands.add('loginViaAPI', (email, password) => {

  if (Cypress.env('ID_COOKIE')) {
    cy.logoutViaApi()
      .then(res => {
        if (res.status !== 200) {
          cy.log('Unexpected error while logging out!');
        }
        return loginRequest(email, password);
      });
  } else {
    loginRequest(email, password);
  }
});

Cypress.Commands.add('logoutViaApi', () => {

  const options = {
    url: `${Cypress.env('API_URL')}/auth`,
    method: 'DELETE',
    headers: (Cypress.env('ID_COOKIE'))
      ? Object.assign({ Cookie: Cypress.env('ID_COOKIE') }, headers)
      : headers
  };
  cy.request(options).then(res => {
    if (res.status === 200) {
      Cypress.env('ID_COOKIE', null);
    }
  });
});
