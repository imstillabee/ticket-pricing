// @flow

import { Center, Loader } from '_components';
import React from 'react';

export function CenteredLoader() {
  return (
    <Center>
      <Loader />
    </Center>
  );
}
