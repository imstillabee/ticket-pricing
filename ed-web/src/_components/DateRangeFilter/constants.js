export const filterOptions = [
  {
    title: 'All Time',
    key: 0
  },
  {
    title: 'Today',
    key: 1
  },
  {
    title: 'Yesterday',
    key: 2
  },
  {
    title: 'Last 7 Days',
    key: 3
  },
  {
    title: 'Month to Date',
    key: 4
  },
  {
    title: 'Previous Month',
    key: 5
  },
  {
    title: 'Year to Date',
    key: 6
  },
  {
    title: 'Select Date Range',
    key: 7
  }
];
