import styled from 'styled-components';
import { cssConstants } from '_constants';

export const MailtoLink = styled.a`
  font-size: 10px;
  font-weight: bold;
  line-height: 130%;
  color: ${cssConstants.PRIMARY_LIGHT_BLACK};
  cursor: pointer;
  text-decoration: none;
`;
