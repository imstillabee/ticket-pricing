import styled from 'styled-components';
import { cssConstants } from '_constants';

export const ScreenTitleBlock = styled.div`
  width: 100%;
  height: 80px;
  margin: 0;
  margin-bottom: 30px;
  padding: 0;
  background: ${cssConstants.PRIMARY_LIGHTEST_GRAY};
  color: ${cssConstants.SECONDARY_BLUE};
`;
