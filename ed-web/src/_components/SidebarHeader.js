// @flow

import styled from 'styled-components';
import { cssConstants } from '_constants';

export const SidebarHeader = styled.div`
  height: 150px;
  width: auto;
  margin: 0;
  padding-left: 20px;
  color: ${cssConstants.PRIMARY_LIGHTEST_GRAY};
  background: ${cssConstants.SECONDARY_BLUE_ACCENT};
`;
