import { cssConstants } from '.';

export const chartLabelStyles = {
  fontSize: 12,
  color: cssConstants.PRIMARY_GRAY
};

export const GROUP_FILTERS = {
  periodic: 0,
  cumulative: 1
};

export const CHART_HEIGHT = 400;
