const BASE = 1000;
const OVERLAY = BASE + 100;

export const cssConstants = {
  PRIMARY_EVEN_LIGHTER_GRAY: '#F2F2F2',
  PRIMARY_BLUE: '#0A169A',
  PRIMARY_BLUE_HOVER: '#848ACC',
  PRIMARY_LIGHT_BLUE: '#38A9DB',
  PRIMARY_LIGHTEST_GRAY: '#F8F8F8',
  PRIMARY_WHITE: '#FFFFFF',
  PRIMARY_LIGHTER_GRAY: '#EEEEEE',
  PRIMARY_LIGHT_GRAY: '#D1D1D1',
  PRIMARY_GRAY: '#999999',
  PRIMARY_DARK_GRAY: '#767676',
  PRIMARY_DARKEST_GRAY: '#323232',
  PRIMARY_LIGHT_BLACK: '#070707',
  PRIMARY_RED: '#B80C09',
  SECONDARY_BLUE: '#1F49B6',
  SECONDARY_BLUE_ACCENT: '#040B52',
  SECONDARY_LIGHT_BLUE: '#4B98CF',
  SECONDARY_LIGHTEST_BLUE: '#7CBCDD',
  SECONDARY_BACKGROUND_LIGHTEST_BLUE: '#F0F4F7',
  SECONDARY_BACKGROUND_BLUE: '#2670AE',
  SECONDARY_BLUE_GRAY: '#C4D2E1',
  SECONDARY_PURPLE: '#5A288F',
  SECONDARY_LIGHT_PURPLE: '#817CB8',
  SECONDARY_LIGHTER_PURPLE: '#BDA3D1',
  SECONDARY_BURNT_ORANGE: '#C57330',
  SECONDARY_YELLOW: '#FDE74C',
  SECONDARY_RED: '#DC0A0A',
  SECONDARY_GREEN: '#2F6450',
  SECONDARY_LIGHT_GREEN: '#9BC53D',
  CARD_PURPLE: '#E3E4EF',
  TITLE_SIZE_H1: '36px',
  TITLE_WEIGHT_H1: 'normal',
  TITLE_SIZE_H2: '28px',
  TITLE_WEIGHT_H2: 'normal',
  TITLE_SIZE_H3: '22px',
  TITLE_WEIGHT_H3: 'normal',
  TITLE_SIZE_H4: '18px',
  TITLE_WEIGHT_H4: 'normal',
  TITLE_SIZE_H5: '14px',
  TITLE_WEIGHT_H5: '300',
  PARAGRAPH_SIZE_P1: '36px',
  PARAGRAPH_WEIGHT_P1: 'bold',
  PARAGRAPH_SIZE_P2: '28px',
  PARAGRAPH_WEIGHT_P2: 'normal',
  SUBHEADING_SIZE_S1: '12px',
  SUBHEADING_WEIGHT_S1: 'normal',
  LABEL_SIZE: '14px',
  LABEL_WEIGHT: 'normal'
};

export const shadows = {
  ACTIVE_BUTTON: '0 0 3px rgba(56, 169, 219, 100)',
  SMALL: '0 1px 5px rgba(0, 0, 0, 0.1)',
  MEDIUM: '0 1px 10px rgba(0, 0, 0, 0.15)',
  LARGE: '0 1px 15px rgba(0, 0, 0, 0.15)'
};

export const zIndexes = {
  BASE,
  DROPDOWN_FILTER: BASE + 1,
  POPOVER: BASE + 2,
  OVERLAY,
  DROPDOWN_CLIENT_HEADER: OVERLAY + 1
};

const getFontFamily = (fontName) =>
  `${fontName}, Roboto, Helvetica, sans-serif`;

export const fonts = {
  FUTURA: getFontFamily('FuturaPT')
};
