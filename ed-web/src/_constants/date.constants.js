export const ISO_DATE_FORMAT = 'YYYY-MM-DD'; // e.g. "2019-01-01"
export const READABLE_DATETIME_FORMAT = 'dddd, MMMM Do, YYYY @ h:mm A z'; // e.g. "Monday, July 1st, 2019 @ 1:10 PM ET"
export const CONCISE_READABLE_DATETIME_FORMAT = 'MMM DD, YYYY hh:mm a z'; // e.g. "Jun 12, 2019 12:00 pm ET"
export const READABLE_DATE_FORMAT = 'MMM DD, YYYY'; // e.g. "Jun 01, 2019"
