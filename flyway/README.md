# Flyway Migrations

> Database migrations for the Event Dynamic platform.

## Setup

1. Install Flyway

    `brew install flyway`

2. See Flyway commands

    `flyway --help`

_Flyway commands should be run from the flyway directory so Flyway can find the configuration file._

## Configuration

Flyway will check for configuration in the `flyway.conf` file by default. `flyway.conf` is set up for local development.

## Scripts

__Make__ -- `./flyway-make.sh`

Helper script to create a new migration inside of the `sql` directory with the correct file format.

```shell
> ./flyway-make.sh
Description (PascalCase): MyMigration
Creating V2019.06.14.10.51.30__MyMigration.sql
```

## Resources

* [Flyway](https://flywaydb.org/)
