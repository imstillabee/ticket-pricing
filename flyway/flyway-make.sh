#!/bin/bash

read -p 'Description (PascalCase): ' fdesc

file_name="V$(date +%Y.%m.%d.%H.%M.%S)__$fdesc.sql"

echo Creating $file_name

touch ./sql/$file_name
