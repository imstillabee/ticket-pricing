CREATE TABLE "Users" (
    "clientId" INTEGER REFERENCES "Clients" ("id"),
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "email" TEXT NOT NULL UNIQUE,
    "firstName" TEXT NOT NULL,
    "id" SERIAL NOT NULL PRIMARY KEY,
    "isAdmin" BOOLEAN NOT NULL DEFAULT FALSE,
    "lastName" TEXT NOT NULL,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "password" TEXT NOT NULL,
    "phoneNumber" TEXT
);
