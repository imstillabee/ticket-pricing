CREATE TABLE "Venues" (
    "capacity" INTEGER NOT NULL,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "id" SERIAL NOT NULL PRIMARY KEY,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "name" TEXT NOT NULL,
    "zipcode" TEXT NOT NULL
);
