CREATE TABLE "Events" (
    "clientId" INTEGER NOT NULL REFERENCES "Clients" ("id"),
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "id" SERIAL NOT NULL PRIMARY KEY,
    "integrationId" INTEGER UNIQUE,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "name" TEXT,
    "seasonId" INTEGER REFERENCES "Seasons" ("id") ON DELETE SET NULL,
    "timestamp" TIMESTAMP WITH TIME ZONE,
    "venueId" INTEGER REFERENCES "Venues" ("id")
);
