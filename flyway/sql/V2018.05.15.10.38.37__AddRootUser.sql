INSERT INTO "Users"
	("createdAt", "modifiedAt", "email", "firstName", "lastName", "password", "isAdmin") VALUES
	(NOW(), NOW(), 'root@eventdynamic.com', 'John', 'Smith', '$2a$10$gCXbPpMjjKMnMInDtkJ0jei8dyqF.s7mLOOKgze9Zf5RKnE/A5b2W', TRUE);

-- Email: root@eventdynamic.com
-- password: 3v3ntDynamicRoot!
