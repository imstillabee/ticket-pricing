CREATE TABLE "Integrations" (
    "appId" TEXT,
    "appKey" TEXT,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "credentials" TEXT,
    "host" TEXT,
    "id" SERIAL NOT NULL PRIMARY KEY,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "name" TEXT NOT NULL UNIQUE,
    "version" TEXT,
    "logoUrl" TEXT
);
