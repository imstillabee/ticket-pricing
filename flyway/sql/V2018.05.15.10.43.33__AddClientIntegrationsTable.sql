CREATE TABLE "ClientIntegrations" (
    "clientId" INTEGER NOT NULL REFERENCES "Clients" ("id"),
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "id" SERIAL NOT NULL PRIMARY KEY,
    "integrationId" INTEGER NOT NULL REFERENCES "Integrations" ("id"),
    "isActive" BOOLEAN NOT NULL DEFAULT FALSE,
    "isPrimary" BOOLEAN NOT NULL DEFAULT FALSE,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL
);
