ALTER TABLE "Users" ADD COLUMN "tempPass" TEXT;
ALTER TABLE "Users" ADD COLUMN "tempExpire" TIMESTAMP;
ALTER TABLE "Users" ALTER COLUMN "password" DROP NOT NULL;
