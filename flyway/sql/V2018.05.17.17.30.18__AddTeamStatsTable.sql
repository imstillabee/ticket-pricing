CREATE TABLE "TeamStats" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "clientId" INTEGER REFERENCES "Clients" ("id"),
    "wins" INTEGER NOT NULL,
    "losses" INTEGER NOT NULL,
    "gamesTotal" INTEGER NOT NULL
);
