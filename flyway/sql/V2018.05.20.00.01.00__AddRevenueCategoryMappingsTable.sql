CREATE TABLE "RevenueCategoryMappings" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "clientId" INTEGER NOT NULL REFERENCES "Clients" ("id"),
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "revenueCategoryId" INTEGER NOT NULL REFERENCES "RevenueCategories" ("id"),
  "regex" TEXT NOT NULL,
  "order" INT NOT NULL,
  UNIQUE ("order", "clientId")
);
