CREATE TABLE "Seats" (
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "id" SERIAL NOT NULL PRIMARY KEY,
  "integrationId" INTEGER UNIQUE NOT NULL,
  "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "row" TEXT NOT NULL,
  "seat" TEXT NOT NULL,
  "section" TEXT NOT NULL
);