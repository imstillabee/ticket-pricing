CREATE TABLE "EventSeats" (
  "eventId" INTEGER NOT NULL REFERENCES "Events" ("id"),
  "id" SERIAL NOT NULL PRIMARY KEY,
  "listedPrice" DECIMAL,
  "priceScaleId" INTEGER NOT NULL, -- TDC price scale
  "seatId" INTEGER NOT NULL REFERENCES "Seats" ("id"),

  UNIQUE("seatId", "eventId")
);

CREATE INDEX ON "EventSeats" ("priceScaleId");
