CREATE TABLE "Transactions" (
  "buyerTypeCode" TEXT NOT NULL,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "eventSeatId" INTEGER NOT NULL REFERENCES "EventSeats" ("id"),
  "id" SERIAL NOT NULL PRIMARY KEY,
  "integrationId" INTEGER NOT NULL REFERENCES "Integrations" ("id"),
  "price" DECIMAL NOT NULL,
  "revenueCategoryId" INTEGER REFERENCES "RevenueCategories" ("id"),
  "timestamp" TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX ON "Transactions" ("eventSeatId");
CREATE INDEX ON "Transactions" ("revenueCategoryId");
CREATE INDEX ON "Transactions" ("integrationId");
