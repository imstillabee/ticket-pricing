CREATE TABLE "WeatherAverages" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "venueId" INTEGER REFERENCES "Venues" ("id"),
  "month" INTEGER NOT NULL,
  "day" INTEGER NOT NULL,
  "hour" INTEGER NOT NULL,
  "temp" FLOAT NOT NULL,

  UNIQUE("venueId", "month", "day", "hour")
)
