INSERT INTO "Integrations" ("createdAt", "modifiedAt", "name", "logoUrl") VALUES (NOW(), NOW(), 'Tickets.com', 'https://s3.amazonaws.com/eventdynamic.com/assets/tickets-com-logo.png');
INSERT INTO "Integrations" ("createdAt", "modifiedAt", "name", "logoUrl") VALUES (NOW(), NOW(), 'SeatGeek', 'https://s3.amazonaws.com/eventdynamic.com/assets/seatgeek-logo.png');
