CREATE TABLE "MySportsFeedsTeamAbbreviations" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "clientId" INTEGER NOT NULL REFERENCES "Clients" ("id"),
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "abbreviation" TEXT NOT NULL,
  UNIQUE ("abbreviation", "clientId")
);
