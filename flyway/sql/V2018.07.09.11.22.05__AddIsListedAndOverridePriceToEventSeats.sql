ALTER TABLE "EventSeats" ADD COLUMN "overridePrice" DECIMAL;
ALTER TABLE "EventSeats" ADD COLUMN "isListed" BOOLEAN NOT NULL DEFAULT TRUE;
CREATE INDEX ON "EventSeats" ("eventId");
