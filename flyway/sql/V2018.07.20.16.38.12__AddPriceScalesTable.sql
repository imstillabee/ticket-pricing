CREATE TABLE "PriceScales" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "venueId" INTEGER REFERENCES "Venues" ("id"),
    "integrationId" INTEGER NOT NULL,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX ON "PriceScales" ("venueId")
