CREATE TABLE "ClientIntegrationEvents" (
    "clientIntegrationId" INTEGER NOT NULL REFERENCES "ClientIntegrations" ("id"),
    "id" SERIAL NOT NULL PRIMARY KEY,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "eventId" INTEGER NOT NULL REFERENCES "Events" ("id"),
    "integrationEventId" INTEGER NOT NULL
);

CREATE INDEX ON "ClientIntegrationEvents" ("eventId");
CREATE INDEX ON "ClientIntegrationEvents" ("clientIntegrationId");
