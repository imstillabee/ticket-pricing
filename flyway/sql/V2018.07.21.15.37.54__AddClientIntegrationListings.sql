CREATE TABLE "ClientIntegrationListings" (
    "clientIntegrationEventsId" INTEGER NOT NULL REFERENCES "ClientIntegrationEvents" ("id"),
    "id" SERIAL NOT NULL PRIMARY KEY,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "listingId" INTEGER NOT NULL
);


CREATE INDEX ON "ClientIntegrationListings" ("clientIntegrationEventsId");
