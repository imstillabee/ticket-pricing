CREATE TABLE "ClientIntegrationEventSeats" (
    "clientIntegrationListingId" INTEGER NOT NULL REFERENCES "ClientIntegrationListings" ("id"),
    "id" SERIAL NOT NULL PRIMARY KEY,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "eventSeatId" INTEGER NOT NULL REFERENCES "EventSeats" ("id")
);

CREATE INDEX ON "ClientIntegrationEventSeats" ("clientIntegrationListingId");
CREATE INDEX ON "ClientIntegrationEventSeats" ("eventSeatId");
