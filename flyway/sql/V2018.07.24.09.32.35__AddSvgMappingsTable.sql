CREATE TABLE "SvgMappings" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "sectionRef" TEXT NOT NULL,
    "venueId" INTEGER REFERENCES "Venues" ("id"),
    "priceScaleId" INTEGER REFERENCES "PriceScales" ("id"),
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX ON "SvgMappings" ("venueId")
