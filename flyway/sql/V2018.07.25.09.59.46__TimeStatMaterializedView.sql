CREATE MATERIALIZED VIEW "EventTimeGroups" AS 
  SELECT
  	es."eventId" as "eventId",
    date_trunc('hour', t.timestamp::timestamp + (60 * interval '1 minute')) as "hourCeiledTimestamp",
    COALESCE(SUM(t.price), 0) as "revenue",
    COALESCE(SUM(SIGN(t.price) * -1), 0) as "inventory"
  FROM "Transactions" t
    LEFT JOIN "EventSeats" es ON es.id = t."eventSeatId"
    INNER JOIN "RevenueCategories" rc ON t."revenueCategoryId" = rc.id
  WHERE
    rc."name" = 'Single Game'
  GROUP BY es."eventId", "hourCeiledTimestamp";
  
CREATE INDEX ON "EventTimeGroups" ("eventId");

CREATE UNIQUE INDEX ON "EventTimeGroups" ("eventId", "hourCeiledTimestamp")

-- Rollback

-- DROP MATERIALIZED VIEW "EventTimeGroups"