CREATE MATERIALIZED VIEW "EventRevenueGroups" AS
SELECT
	es."eventId" AS "eventId",
	t."revenueCategoryId" AS "revenueCategoryId",
	SUM(COALESCE(t."price", 0)) AS "revenue",
	SUM(COALESCE(SIGN(t."price"), 0)) AS "ticketsSold"
FROM
	"EventSeats" es
	INNER JOIN "Transactions" t ON es.id = t."eventSeatId"
GROUP BY
	es."eventId",
	t."revenueCategoryId";
	
CREATE INDEX ON "EventRevenueGroups" ("eventId");

CREATE UNIQUE INDEX ON "EventRevenueGroups" ("eventId", "revenueCategoryId");

-- Rollback

-- DROP MATERIALIZED VIEW "EventRevenueGroups";