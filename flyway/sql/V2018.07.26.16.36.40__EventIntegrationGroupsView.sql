CREATE MATERIALIZED VIEW "EventIntegrationGroups" AS
SELECT
	es."eventId" AS "eventId",
	t."integrationId" AS "integrationId",
	SUM(COALESCE(t."price", 0)) AS "revenue",
	SUM(COALESCE(SIGN(t."price"), 0)) AS "ticketsSold"
FROM
	"EventSeats" es
	INNER JOIN "Transactions" t ON es.id = t."eventSeatId"
GROUP BY
	es."eventId",
	t."integrationId";
	
CREATE INDEX ON "EventIntegrationGroups" ("eventId");

CREATE UNIQUE INDEX ON "EventIntegrationGroups" ("eventId", "integrationId");

-- Rollback

--DROP MATERIALIZED VIEW "EventIntegrationGroups";