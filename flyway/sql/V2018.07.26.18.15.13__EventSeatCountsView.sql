CREATE MATERIALIZED VIEW "EventSeatCounts" AS
SELECT
	es."eventId" as "eventId",
	COUNT(DISTINCT(es.id)) as "total",
	SUM(SIGN(COALESCE(t."price", 0))) as "sold"
FROM
	"EventSeats" es
	LEFT JOIN "Transactions" t on es.id = t."eventSeatId"
GROUP BY es."eventId";
    
CREATE UNIQUE INDEX ON "EventSeatCounts" ("eventId");

-- Rollback

--DROP MATERIALIZED VIEW "EventSeatCounts"
