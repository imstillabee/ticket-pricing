ALTER TABLE "Transactions" ADD "externalTransactionId" INT;
ALTER TABLE "Transactions" ADD "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL default now();

CREATE UNIQUE INDEX ON "Transactions" ("externalTransactionId", "eventSeatId");
