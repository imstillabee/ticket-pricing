ALTER TABLE "Seats" DROP CONSTRAINT "Seats_integrationId_key";
ALTER TABLE "Seats" RENAME COLUMN "integrationId" TO "primarySeatId";
ALTER TABLE "Events" DROP CONSTRAINT "Events_integrationId_key";
ALTER TABLE "Events" RENAME COLUMN "integrationId" TO "primaryEventId";
