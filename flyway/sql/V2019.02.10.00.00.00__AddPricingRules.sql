CREATE TABLE "ProVenuePricingRules" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "clientId" INTEGER NOT NULL REFERENCES "Clients" ("id"),
  "name" TEXT,
  "isActive" BOOLEAN NOT NULL DEFAULT FALSE,
  "mirrorPriceScaleId" INTEGER REFERENCES "PriceScales" ("id"),
  "percent" INTEGER,
  "constant" DECIMAL,
  "round" TEXT NOT NULL DEFAULT 'up' -- Expected values: up, down, natural, none
);

CREATE TABLE "ProVenuePricingRuleEvents" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "eventId" INTEGER REFERENCES "Events" ("id"),
  "proVenuePricingRuleId" INTEGER REFERENCES "ProVenuePricingRules" ("id"),

  UNIQUE("eventId", "proVenuePricingRuleId")
);

CREATE TABLE "ProVenuePricingRulePriceScales" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "priceScaleId" INTEGER REFERENCES "PriceScales" ("id"),
  "proVenuePricingRuleId" INTEGER REFERENCES "ProVenuePricingRules" ("id"),

  UNIQUE("priceScaleId", "proVenuePricingRuleId")
);

CREATE TABLE "ProVenuePricingRuleBuyerTypes" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "externalBuyerTypeId" TEXT NOT NULL,
  "proVenuePricingRuleId" INTEGER REFERENCES "ProVenuePricingRules" ("id"),

  UNIQUE("externalBuyerTypeId", "proVenuePricingRuleId")
);
