CREATE TABLE "EventCategories" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "venueId" INTEGER REFERENCES "Venues" ("id") NULL,
    "name" TEXT NOT NULL,

    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,

    UNIQUE("venueId", "name")
);

CREATE INDEX ON "EventCategories" ("venueId");
