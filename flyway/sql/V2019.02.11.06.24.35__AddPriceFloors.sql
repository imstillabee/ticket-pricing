CREATE TABLE "PriceFloors" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "eventCategoryId" INTEGER REFERENCES "EventCategories" ("id") NOT NULL,
    "priceScaleId" INTEGER REFERENCES "PriceScales" ("id") NULL,
    "row" TEXT NULL,
    "minimumPrice" DECIMAL NOT NULL,

    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,

    UNIQUE("eventCategoryId", "priceScaleId"),
    UNIQUE("eventCategoryId", "row")
);

CREATE INDEX ON "PriceFloors" ("eventCategoryId");
CREATE INDEX ON "PriceFloors" ("priceScaleId");
