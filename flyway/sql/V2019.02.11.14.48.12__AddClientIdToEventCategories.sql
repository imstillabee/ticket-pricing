ALTER TABLE "EventCategories" DROP CONSTRAINT "EventCategories_venueId_name_key";
ALTER TABLE "EventCategories" DROP COLUMN "venueId";

ALTER TABLE "EventCategories" ADD COLUMN "clientId" INTEGER REFERENCES "Clients" ("id") NULL;
CREATE INDEX ON "EventCategories" ("clientId");

ALTER TABLE "EventCategories" ADD UNIQUE("clientId", "name");
