ALTER TABLE "Events" ADD COLUMN "eventCategoryId" INTEGER REFERENCES "EventCategories" ("id") NULL;
CREATE INDEX ON "Events" ("eventCategoryId");
