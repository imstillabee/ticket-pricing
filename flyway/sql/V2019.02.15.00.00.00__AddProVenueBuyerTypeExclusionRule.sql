CREATE TABLE "ProVenueBuyerTypeExclusionRules" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "clientId" INTEGER REFERENCES "Clients" ("id") NOT NULL,
  "externalBuyerTypeId" TEXT NOT NULL,
  "disabled" BOOLEAN NOT NULL,

  UNIQUE("clientId", "externalBuyerTypeId")
);
