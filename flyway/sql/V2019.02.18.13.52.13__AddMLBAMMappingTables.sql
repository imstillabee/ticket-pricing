CREATE TABLE "MLBAMSectionToPriceScale" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "priceScaleId" INTEGER NOT NULL REFERENCES "PriceScales" ("id"),
  "sectionId" INTEGER NOT NULL
);

CREATE TABLE "MLBAMScheduleToEvent" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "eventId" INTEGER NOT NULL REFERENCES "Events" ("id"),
  "scheduleId" INTEGER NOT NULL
);

CREATE TABLE "MLBAMTeamToClient" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "clientId" INTEGER NOT NULL REFERENCES "Clients" ("id"),
  "teamId" INTEGER NOT NULL
);
