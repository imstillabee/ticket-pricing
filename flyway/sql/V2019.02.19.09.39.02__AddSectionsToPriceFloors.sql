ALTER TABLE "PriceFloors" ADD COLUMN "section" TEXT;
ALTER TABLE "PriceFloors" DROP CONSTRAINT "PriceFloors_eventCategoryId_row_key";
CREATE UNIQUE INDEX ON "PriceFloors" ("eventCategoryId", "row", "section");
