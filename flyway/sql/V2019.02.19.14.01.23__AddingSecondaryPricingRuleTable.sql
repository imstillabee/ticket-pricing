CREATE TABLE "SecondaryPricingRules" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "clientIntegrationId" INTEGER UNIQUE NOT NULL REFERENCES "ClientIntegrations" ("id"),
  "percent" INTEGER,
  "constant" DECIMAL
);
