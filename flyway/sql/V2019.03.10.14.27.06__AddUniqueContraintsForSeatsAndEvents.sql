CREATE UNIQUE INDEX ON "Events" ("clientId", "primaryEventId");
CREATE UNIQUE INDEX ON "Seats" ("venueId", "primarySeatId");
