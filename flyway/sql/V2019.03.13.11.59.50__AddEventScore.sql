ALTER TABLE "Events"
  ADD COLUMN "eventScore" decimal,
  ADD COLUMN "eventScoreOverride" decimal;
