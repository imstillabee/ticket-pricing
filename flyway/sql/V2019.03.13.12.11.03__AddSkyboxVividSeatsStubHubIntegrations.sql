INSERT INTO "Integrations" ("createdAt", "modifiedAt", "name", "logoUrl") VALUES (NOW(), NOW(), 'Skybox - Stubhub', 'https://s3.amazonaws.com/eventdynamic.com/assets/stubhub-logo.png');
INSERT INTO "Integrations" ("createdAt", "modifiedAt", "name", "logoUrl") VALUES (NOW(), NOW(), 'Skybox - Vivid Seats', 'https://s3.amazonaws.com/eventdynamic.com/assets/vivid-seats-logo.png');

ALTER TABLE "ClientIntegrations" ADD COLUMN "json" TEXT NULL;
