ALTER TABLE "Events"
  ADD COLUMN "spring" decimal,
  ADD COLUMN "springModifier" decimal;
