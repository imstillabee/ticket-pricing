ALTER TABLE "PriceFloors" RENAME TO "PriceGuards";
ALTER TABLE "PriceGuards" ADD COLUMN "maximumPrice" INTEGER;
ALTER TABLE "PriceGuards" ALTER COLUMN "minimumPrice" SET DATA TYPE INTEGER;
