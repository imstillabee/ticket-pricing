DROP INDEX IF EXISTS "Transactions_externalTransactionId_eventSeatId_idx";
CREATE UNIQUE INDEX ON "Transactions" ("externalTransactionId", "eventSeatId", "integrationId");
