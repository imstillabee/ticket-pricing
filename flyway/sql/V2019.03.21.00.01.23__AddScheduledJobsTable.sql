CREATE TABLE "ScheduledJobs" (
    "id" SERIAL NOT NULL PRIMARY KEY,

    "eventId" INTEGER REFERENCES "Events" ("id"),
    "jobConfigId" INTEGER REFERENCES "JobConfigs" ("id"),

    "status" VARCHAR(255) NOT NULL,

    "priority" INTEGER NOT NULL DEFAULT 0,

    "startTime" TIMESTAMP WITH TIME ZONE NULL,
    "endTime" TIMESTAMP WITH TIME ZONE NULL,

    "error" TEXT NULL,

    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX ON "ScheduledJobs" ("status");
