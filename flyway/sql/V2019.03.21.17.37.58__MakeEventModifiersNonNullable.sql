UPDATE "Events" SET "eventScoreModifier" = 0;
ALTER TABLE "Events" ALTER "eventScoreModifier" SET NOT NULL;

UPDATE "Events" SET "springModifier" = 0;
ALTER TABLE "Events" ALTER "springModifier" SET NOT NULL;
