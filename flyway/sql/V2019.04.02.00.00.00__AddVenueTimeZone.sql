ALTER TABLE "Venues" ADD COLUMN "timeZone" TEXT NULL;
UPDATE "Venues" SET "timeZone" = 'America/New_York';
ALTER TABLE "Venues" ALTER "timeZone" SET NOT NULL;
