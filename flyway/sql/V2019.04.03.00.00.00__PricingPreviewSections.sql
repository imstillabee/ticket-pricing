CREATE TABLE "PricingPreviewSections" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
  "clientId" INTEGER REFERENCES "Clients" ("id"),
  "section" TEXT NOT NULL,
  UNIQUE("clientId", "section")
)
