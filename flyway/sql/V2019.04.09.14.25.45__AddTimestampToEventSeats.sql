ALTER TABLE "EventSeats" ADD COLUMN "createdAt" TIMESTAMP WITH TIME ZONE;
ALTER TABLE "EventSeats" ADD COLUMN "modifiedAt" TIMESTAMP WITH TIME ZONE;

UPDATE "EventSeats" SET "createdAt" = now();
UPDATE "EventSeats" SET "modifiedAt" = now();

ALTER TABLE "EventSeats" ALTER COLUMN "createdAt" SET NOT NULL;
ALTER TABLE "EventSeats" ALTER COLUMN "modifiedAt" SET NOT NULL;