ALTER TABLE "ProVenuePricingRuleBuyerTypes" DROP CONSTRAINT "ProVenuePricingRuleBuyerTypes_proVenuePricingRuleId_fkey";
ALTER TABLE "ProVenuePricingRuleBuyerTypes" ADD CONSTRAINT "ProVenuePricingRuleBuyerTypes_proVenuePricingRuleId_fkey" FOREIGN KEY("proVenuePricingRuleId") REFERENCES "ProVenuePricingRules"("id") ON DELETE CASCADE;

ALTER TABLE "ProVenuePricingRuleEvents" DROP CONSTRAINT "ProVenuePricingRuleEvents_proVenuePricingRuleId_fkey";
ALTER TABLE "ProVenuePricingRuleEvents" ADD CONSTRAINT "ProVenuePricingRuleEvents_proVenuePricingRuleId_fkey" FOREIGN KEY("proVenuePricingRuleId") REFERENCES "ProVenuePricingRules"("id") ON DELETE CASCADE;

ALTER TABLE "ProVenuePricingRulePriceScales" DROP CONSTRAINT "ProVenuePricingRulePriceScales_proVenuePricingRuleId_fkey";
ALTER TABLE "ProVenuePricingRulePriceScales" ADD CONSTRAINT "ProVenuePricingRulePriceScales_proVenuePricingRuleId_fkey" FOREIGN KEY("proVenuePricingRuleId") REFERENCES "ProVenuePricingRules"("id") ON DELETE CASCADE;
