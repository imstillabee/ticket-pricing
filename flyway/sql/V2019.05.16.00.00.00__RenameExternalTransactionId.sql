-- Rename externalTransactionId to primaryTransactionId
DROP INDEX IF EXISTS "Transactions_externalTransactionId_eventSeatId_integrationI_idx";
ALTER TABLE "Transactions" RENAME COLUMN "externalTransactionId" TO "primaryTransactionId";
ALTER TABLE "Transactions" ALTER COLUMN "primaryTransactionId" DROP NOT NULL;
CREATE UNIQUE INDEX ON "Transactions" ("integrationId", "eventSeatId", "primaryTransactionId");

-- Add secondaryTransactionId
ALTER TABLE "Transactions" ADD COLUMN "secondaryTransactionId" INTEGER NULL;
CREATE UNIQUE INDEX ON "Transactions" ("integrationId", "eventSeatId", "secondaryTransactionId");

-- Make sure either primaryTransactionId or secondaryTransactionId always exists
ALTER TABLE "Transactions"
  ADD CONSTRAINT "Transactions_primaryTransactionId_secondaryTransactionId_not_null"
  CHECK (("primaryTransactionId" IS NOT NULL) OR ("secondaryTransactionId" IS NOT NULL));
