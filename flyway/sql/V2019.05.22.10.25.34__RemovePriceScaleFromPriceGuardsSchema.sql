ALTER TABLE "PriceGuards" RENAME TO "OldPriceGuards";

CREATE TABLE "PriceGuards" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "eventId" INTEGER REFERENCES "Events" ("id") NOT NULL,
    "section" TEXT NOT NULL,
    "row" TEXT NOT NULL,
    "minimumPrice" INTEGER NOT NULL,
    "maximumPrice" INTEGER NULL,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,

    UNIQUE("eventId", "row", "section")
);

CREATE INDEX ON "PriceGuards" ("eventId");
CREATE INDEX ON "PriceGuards" ("section");
CREATE INDEX ON "PriceGuards" ("row");

INSERT INTO "PriceGuards" ("eventId", "section", "row", "minimumPrice", "maximumPrice", "modifiedAt", "createdAt")
    SELECT
    e.id AS "eventId",
    pg."section" AS "section",
    pg."row" AS "row",
    pg."minimumPrice" AS "minimumPrice",
    pg."maximumPrice" AS "maximumPrice",
    pg."modifiedAt" AS "modifiedAt",
    pg."createdAt" AS "createdAt"
    FROM "OldPriceGuards" pg 
    JOIN "Events" e ON e."eventCategoryId" = pg."eventCategoryId"
    WHERE pg."section" IS NOT NULL
    AND pg."row" IS NOT NULL;

DROP TABLE "OldPriceGuards";