ALTER TABLE "Transactions" ADD COLUMN "type" TEXT;

UPDATE "Transactions"
SET "type" = 'Purchase'
WHERE "price" >= 0;

UPDATE "Transactions"
SET "type" = 'Return'
where "price" < 0;


ALTER TABLE "Transactions" ALTER COLUMN "type" SET NOT NULL;
