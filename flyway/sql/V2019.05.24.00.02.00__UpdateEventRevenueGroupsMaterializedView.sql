DROP MATERIALIZED VIEW "EventRevenueGroups";

CREATE MATERIALIZED VIEW "EventRevenueGroups" AS
SELECT
	es."eventId" AS "eventId",
	t."revenueCategoryId" AS "revenueCategoryId",
	SUM(COALESCE(t."price", 0)) AS "revenue",
	SUM(
		case
			when t."type" = 'Purchase' then 1
			when t."type" = 'Return' then -1
			else 0
		end
	) as "ticketsSold"
FROM
	"EventSeats" es
	INNER JOIN "Transactions" t ON es.id = t."eventSeatId"
GROUP BY
	es."eventId",
	t."revenueCategoryId";

CREATE INDEX ON "EventRevenueGroups" ("eventId");

CREATE UNIQUE INDEX ON "EventRevenueGroups" ("eventId", "revenueCategoryId");
