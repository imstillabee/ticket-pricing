DROP MATERIALIZED VIEW "EventIntegrationGroups";

CREATE MATERIALIZED VIEW "EventIntegrationGroups" AS
SELECT
    es."eventId" AS "eventId",
    t."integrationId" AS "integrationId",
    SUM(COALESCE(t."price", 0)) AS "revenue",
    SUM(
        case
            when t."type" = 'Purchase' then 1
            when t."type" = 'Return' then -1
            else 0
        end
    ) as "ticketsSold"
FROM
    "EventSeats" es
    INNER JOIN "Transactions" t ON es.id = t."eventSeatId"
GROUP BY
    es."eventId",
    t."integrationId";

CREATE INDEX ON "EventIntegrationGroups" ("eventId");

CREATE UNIQUE INDEX ON "EventIntegrationGroups" ("eventId", "integrationId");
