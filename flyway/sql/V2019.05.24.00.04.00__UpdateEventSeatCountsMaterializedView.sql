DROP MATERIALIZED VIEW "EventSeatCounts";

CREATE MATERIALIZED VIEW "EventSeatCounts" AS
SELECT
    es."eventId" as "eventId",
    COUNT(DISTINCT(es.id)) as "total",
    SUM(
        case
            when t."type" = 'Purchase' then 1
            when t."type" = 'Return' then -1
            else 0
        end
    ) as "sold"
FROM
    "EventSeats" es
    LEFT JOIN "Transactions" t on es.id = t."eventSeatId"
GROUP BY es."eventId";

CREATE UNIQUE INDEX ON "EventSeatCounts" ("eventId");
