CREATE TABLE "UserClients" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "userId" INT NOT NULL REFERENCES "Users" ("id"),
    "clientId" INT NOT NULL REFERENCES "Clients" ("id"),
    "isDefault" BOOLEAN NOT NULL DEFAULT false,

    UNIQUE("userId", "clientId")
);

INSERT INTO "UserClients" ("userId", "clientId", "isDefault")
    SELECT
        u.id AS "userId",
        u."clientId" AS "clientId",
        true AS "isDefault"
    FROM "Users" u
    WHERE u."clientId" IS NOT NULL