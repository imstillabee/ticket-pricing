CREATE EXTENSION IF NOT EXISTS citext;

ALTER TABLE "Users" ALTER COLUMN email TYPE citext;
