-- Remove root user that doesn't have an associated Client
DELETE FROM "Users" WHERE email = 'root@eventdynamic.com';

-- Rename and require the activeClientId for a User
ALTER TABLE "Users" RENAME COLUMN "clientId" TO "activeClientId";
ALTER TABLE "Users" ALTER COLUMN "activeClientId" SET NOT NULL;
