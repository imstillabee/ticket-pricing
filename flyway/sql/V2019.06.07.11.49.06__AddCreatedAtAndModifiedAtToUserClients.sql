ALTER TABLE "UserClients" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL default now();
ALTER TABLE "UserClients" ADD "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL default now();
