ALTER TABLE "Transactions" ALTER COLUMN "primaryTransactionId" type text;
ALTER TABLE "Transactions" ALTER COLUMN "secondaryTransactionId" type text;
