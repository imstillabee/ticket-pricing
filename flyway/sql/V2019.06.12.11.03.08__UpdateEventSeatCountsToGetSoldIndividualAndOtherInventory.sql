DROP MATERIALIZED VIEW "EventSeatCounts";

CREATE MATERIALIZED VIEW "EventSeatCounts" AS
SELECT es."eventId",
    COUNT(DISTINCT es.id) AS total,
    SUM(
        CASE
            WHEN (rc."name" <> 'Single Game' AND rc."name" <> 'Group') THEN 0
            WHEN (t.type = 'Purchase'::text) THEN 1
            WHEN (t.type = 'Return'::text) THEN '-1'::integer
            ELSE 0
        END) AS "soldIndividual",
    SUM(
        CASE
            WHEN (rc."name" = 'Single Game' OR rc."name" = 'Group') THEN 0
            WHEN (t.type = 'Purchase'::text) THEN 1
            WHEN (t.type = 'Return'::text) THEN '-1'::integer
            ELSE 0
        END) AS "soldOther"
FROM ("EventSeats" es
    LEFT JOIN "Transactions" t ON ((es.id = t."eventSeatId")))
    LEFT JOIN "RevenueCategories" rc ON t."revenueCategoryId" = rc.id
GROUP BY es."eventId";

CREATE UNIQUE INDEX ON "EventSeatCounts" ("eventId");
