DROP MATERIALIZED VIEW "EventTimeGroups";

CREATE MATERIALIZED VIEW "EventTimeGroups" AS
SELECT
    es."eventId" as "eventId",
    date_trunc('hour', t.timestamp::timestamp + interval '1 hour') as "hourCeiledTimestamp",
    SUM(COALESCE(t.price, 0)) as "revenue",
    SUM(
        case
            when t."type" = 'Purchase' then -1
            when t."type" = 'Return' then 1
            else 0
        end
    ) as "inventory"
FROM "Transactions" t
    LEFT JOIN "EventSeats" es ON es.id = t."eventSeatId"
    INNER JOIN "RevenueCategories" rc ON t."revenueCategoryId" = rc.id
WHERE
    rc."name" = 'Single Game' OR rc."name" = 'Group'
GROUP BY es."eventId", "hourCeiledTimestamp";

CREATE INDEX ON "EventTimeGroups" ("eventId");

CREATE UNIQUE INDEX ON "EventTimeGroups" ("eventId", "hourCeiledTimestamp");
