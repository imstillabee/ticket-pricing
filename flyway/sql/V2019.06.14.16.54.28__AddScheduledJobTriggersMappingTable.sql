CREATE TABLE "ScheduledJobTriggers" (
    "id"  SERIAL NOT NULL PRIMARY KEY,
    "scheduledJobId" INTEGER REFERENCES "ScheduledJobs" ("id"),
    "jobTriggerId" INTEGER REFERENCES "JobTriggers" ("id"),
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);
