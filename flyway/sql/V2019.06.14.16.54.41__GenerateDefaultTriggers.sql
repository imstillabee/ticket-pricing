INSERT INTO "JobTriggers" ("type") VALUES ('Manual');

INSERT INTO "ScheduledJobTriggers" ("scheduledJobId", "jobTriggerId")
(SELECT sj.id AS "scheduledJobId", jt.id AS "jobTriggerId" FROM "ScheduledJobs" sj CROSS JOIN "JobTriggers" jt);
