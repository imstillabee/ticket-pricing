ALTER TABLE "ScheduledJobTriggers"
ADD CONSTRAINT "ScheduledJobTriggers_scheduledJobId_jobTriggerId_key" UNIQUE ("scheduledJobId", "jobTriggerId");
