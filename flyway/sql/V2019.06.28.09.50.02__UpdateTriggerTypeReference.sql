ALTER TABLE "ScheduledJobTriggers" DROP CONSTRAINT "ScheduledJobTriggers_scheduledJobId_fkey";
ALTER TABLE "ScheduledJobTriggers" ADD CONSTRAINT "ScheduledJobTriggers_scheduledJobId_fkey" FOREIGN KEY ("scheduledJobId") REFERENCES "ScheduledJobs" ("id") ON DELETE CASCADE;
ALTER TABLE "ScheduledJobTriggers" DROP CONSTRAINT "ScheduledJobTriggers_jobTriggerId_fkey";
ALTER TABLE "ScheduledJobTriggers" ADD CONSTRAINT "ScheduledJobTriggers_jobTriggerId_fkey" FOREIGN KEY ("jobTriggerId") REFERENCES "JobTriggers" ("id") ON DELETE CASCADE;
