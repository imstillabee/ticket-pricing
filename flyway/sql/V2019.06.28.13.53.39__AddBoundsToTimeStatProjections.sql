ALTER TABLE "TimeStatProjections" ADD COLUMN "inventoryLowerBound" INTEGER NOT NULL;
ALTER TABLE "TimeStatProjections" ADD COLUMN "inventoryUpperBound" INTEGER NOT NULL;
ALTER TABLE "TimeStatProjections" ADD COLUMN "revenueLowerBound" NUMERIC NOT NULL;
ALTER TABLE "TimeStatProjections" ADD COLUMN "revenueUpperBound" NUMERIC NOT NULL;
