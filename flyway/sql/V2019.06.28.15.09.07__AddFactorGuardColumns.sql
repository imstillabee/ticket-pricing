ALTER TABLE "Clients"
ADD COLUMN "eventScoreFloor" decimal NOT NULL DEFAULT 2,
ADD COLUMN "eventScoreCeiling" decimal,
ADD COLUMN "springFloor" decimal NOT NULL DEFAULT 1.125,
ADD COLUMN "springCeiling" decimal;
