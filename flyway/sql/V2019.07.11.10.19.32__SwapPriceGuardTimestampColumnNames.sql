ALTER TABLE "PriceGuards" RENAME COLUMN "createdAt" TO "oldCreatedAt";
ALTER TABLE "PriceGuards" RENAME COLUMN "modifiedAt" TO "createdAt";
ALTER TABLE "PriceGuards" RENAME COLUMN "oldCreatedAt" TO "modifiedAt";
