ALTER TABLE "Clients" ADD COLUMN "performanceType" TEXT NOT NULL DEFAULT 'MLB';
ALTER TABLE "Clients" ALTER COLUMN "performanceType" DROP DEFAULT;
UPDATE "Clients" SET "performanceType" = 'NFL' WHERE "name" LIKE '%Falcons%';
