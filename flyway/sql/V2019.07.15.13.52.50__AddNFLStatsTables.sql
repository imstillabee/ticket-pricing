CREATE TABLE "NFLSeasonStats" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "clientId" INTEGER REFERENCES "Clients" ("id"),
    "seasonId" INTEGER REFERENCES "Seasons" ("id"),
    "wins" INTEGER NOT NULL,
    "losses" INTEGER NOT NULL,
    "ties" INTEGER NOT NULL,
    "gamesTotal" INTEGER NOT NULL,

    UNIQUE("seasonId")
);

CREATE TABLE "NFLEventStats" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "clientId" INTEGER REFERENCES "Clients" ("id"),
    "eventId" INTEGER REFERENCES "Events" ("id"),
    "opponent" TEXT NOT NULL,
    "isHomeOpener" BOOLEAN NOT NULL,
    "isPreSeason" BOOLEAN NOT NULL,
    
    UNIQUE("eventId")
);
