
INSERT INTO "JobPriorities" ("name", "priority") VALUES ('Default', 1);
INSERT INTO "JobPriorities" ("name", "priority") VALUES ('TimeDecayWeekly', 10);
INSERT INTO "JobPriorities" ("name", "priority") VALUES ('TimeDecayDaily', 20);
INSERT INTO "JobPriorities" ("name", "priority") VALUES ('TimeDecayHourly', 30);
INSERT INTO "JobPriorities" ("name", "priority") VALUES ('WeatherTemperature', 40);
INSERT INTO "JobPriorities" ("name", "priority") VALUES ('WeatherCondition', 50);
INSERT INTO "JobPriorities" ("name", "priority") VALUES ('EventStatsChange', 60);
INSERT INTO "JobPriorities" ("name", "priority") VALUES ('VelocityLow', 70);
INSERT INTO "JobPriorities" ("name", "priority") VALUES ('VelocityHigh', 80);
INSERT INTO "JobPriorities" ("name", "priority") VALUES ('ManualPricingModifierChange', 90);
INSERT INTO "JobPriorities" ("name", "priority") VALUES ('ManualOverride', 100);
