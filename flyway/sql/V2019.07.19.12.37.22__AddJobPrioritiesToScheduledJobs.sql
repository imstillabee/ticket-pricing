ALTER TABLE "ScheduledJobs" ADD COLUMN "jobPriorityId" INTEGER REFERENCES "JobPriorities" ("id") NOT NULL DEFAULT 10;
ALTER TABLE "ScheduledJobs" DROP COLUMN "priority";
