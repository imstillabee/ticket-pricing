CREATE TABLE "MLSSeasonStats" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "seasonId" INTEGER REFERENCES "Seasons" ("id"),
    "wins" INTEGER NOT NULL,
    "losses" INTEGER NOT NULL,
    "ties" INTEGER NOT NULL,
    "gamesTotal" INTEGER NOT NULL,

    UNIQUE("seasonId")
);

CREATE TABLE "MLSEventStats" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL,
    "eventId" INTEGER REFERENCES "Events" ("id"),
    "opponent" TEXT NOT NULL,
    "gameNumber" INTEGER NOT NULL,
    
    UNIQUE("eventId")
);
