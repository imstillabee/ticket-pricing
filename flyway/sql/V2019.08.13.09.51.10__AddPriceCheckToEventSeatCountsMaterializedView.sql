DROP MATERIALIZED VIEW "EventSeatCounts";

CREATE MATERIALIZED VIEW "EventSeatCounts" AS

 SELECT es."eventId",
    count(DISTINCT es.id) AS total,
    sum(
        CASE
            WHEN ((rc.name <> 'Single Game'::text) AND (rc.name <> 'Group'::text)) THEN 0
            WHEN (t.type = 'Purchase'::text) AND (t.price > 0) THEN 1
            WHEN (t.type = 'Return'::text) THEN '-1'::integer
            ELSE 0
        END) AS "soldIndividual",
    sum(
        CASE
            WHEN ((rc.name = 'Single Game'::text) OR (rc.name = 'Group'::text) OR (rc.name = 'Exclusions'::text)) THEN 0
            WHEN (t.type = 'Purchase'::text) AND (t.price > 0) THEN 1
            WHEN (t.type = 'Return'::text) THEN '-1'::integer
            ELSE 0
        END) AS "soldOther"
   FROM (("EventSeats" es
     LEFT JOIN "Transactions" t ON ((es.id = t."eventSeatId")))
     LEFT JOIN "RevenueCategories" rc ON ((t."revenueCategoryId" = rc.id)))
  GROUP BY es."eventId";
