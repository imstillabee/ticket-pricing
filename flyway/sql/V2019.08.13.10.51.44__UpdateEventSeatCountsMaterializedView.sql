DROP MATERIALIZED VIEW "EventSeatCounts";

CREATE MATERIALIZED VIEW "EventSeatCounts" AS
SELECT es."eventId",
    count(DISTINCT es.id) as "total",
    SUM(
        CASE
            WHEN (rc."name" <> 'Single Game' AND rc."name" <> 'Group') THEN 0
            WHEN (t.type = 'Purchase'::text) AND (t.price > 0) THEN 1
            WHEN (t.type = 'Return'::text) THEN '-1'::integer
            ELSE 0
        END) AS "soldIndividual",
    SUM(
        CASE
            WHEN (rc."name" = 'Single Game' OR rc."name" = 'Group' OR rc."name" = 'Exclusions') THEN 0
            WHEN (t.type = 'Purchase'::text) AND (t.price > 0) THEN 1
            WHEN (t.type = 'Return'::text) THEN '-1'::integer
            ELSE 0
        END) AS "soldOther",
    count(DISTINCT es.id) FILTER (WHERE es."isHeld" = false) as "unsold"
FROM ("EventSeats" es
    LEFT JOIN "Transactions" t ON ((es.id = t."eventSeatId")))
    LEFT JOIN "RevenueCategories" rc ON t."revenueCategoryId" = rc.id
GROUP BY es."eventId";

CREATE UNIQUE INDEX ON "EventSeatCounts" ("eventId");
