DROP MATERIALIZED VIEW "EventIntegrationGroups";

CREATE MATERIALIZED VIEW "EventIntegrationGroups" AS SELECT es."eventId",
    t."integrationId",
    sum(COALESCE(t.price, 0::numeric)) AS revenue,
    sum(
        CASE
            WHEN t.type = 'Purchase'::text AND t.price != 0 THEN 1
            WHEN t.type = 'Return'::text THEN '-1'::integer
            ELSE 0
        END) AS "ticketsSold"
   FROM "EventSeats" es
     JOIN "Transactions" t ON es.id = t."eventSeatId"
  GROUP BY es."eventId", t."integrationId";

CREATE INDEX ON "EventIntegrationGroups" ("eventId");
CREATE UNIQUE INDEX ON "EventIntegrationGroups" ("eventId", "integrationId");
