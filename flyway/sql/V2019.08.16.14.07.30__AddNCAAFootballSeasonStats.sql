CREATE TABLE "NCAAFootballSeasonStats"
(
    "id" SERIAL PRIMARY KEY,
    "seasonId" integer REFERENCES "Seasons"("id"),
    "wins" integer NOT NULL,
    "losses" integer NOT NULL,
    "gamesTotal" integer NOT NULL,
    "createdAt" timestamp NOT NULL,
    "modifiedAt" timestamp NOT NULL,

    UNIQUE("seasonId")
);
