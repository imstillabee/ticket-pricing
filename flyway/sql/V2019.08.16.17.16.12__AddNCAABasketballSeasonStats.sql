CREATE TABLE "NCAABasketballSeasonStats"
(
    "id" SERIAL PRIMARY KEY,
    "seasonId" integer REFERENCES "Seasons"("id"),
    "wins" integer NOT NULL DEFAULT 0,
    "losses" integer NOT NULL DEFAULT 0,
    "gamesTotal" integer NOT NULL DEFAULT 0,
    "createdAt" timestamp NOT NULL,
    "modifiedAt" timestamp NOT NULL,

    UNIQUE("seasonId")
);
