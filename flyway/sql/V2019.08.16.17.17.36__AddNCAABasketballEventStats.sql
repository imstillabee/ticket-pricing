CREATE TABLE "NCAABasketballEventStats" (

    "id" SERIAL PRIMARY KEY,
    "eventId" integer REFERENCES "Events"("id"),
    "opponent" text NOT NULL,
    "isHomeOpener" boolean NOT NULL,
    "isPreSeason" boolean NOT NULL,
    "createdAt" timestamp NOT NULL,
    "modifiedAt" timestamp NOT NULL,

    UNIQUE("eventId")
);

