CREATE TABLE "Jobs" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "type" citext NOT NULL UNIQUE,
    "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    "modifiedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

INSERT INTO "Jobs" ("type") VALUES ('Pricing'), ('PrimarySync'), ('TicketFulfillment');
