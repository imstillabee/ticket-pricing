ALTER TABLE "JobConfigs" ADD COLUMN "jobId" INTEGER;
ALTER TABLE "JobConfigs" ADD FOREIGN KEY ("jobId") REFERENCES "Jobs"("id") ON DELETE CASCADE;

-- Rename current job configs to match new naming convention
UPDATE "JobConfigs"
SET "name" = 'Pricing'
WHERE "JobConfigs"."name" = 'pricing';

UPDATE "JobConfigs"
SET "name" = 'PrimarySync'
WHERE "JobConfigs"."name" = 'primary-sync';

UPDATE "JobConfigs"
SET "name" = 'TicketFulfillment'
WHERE "JobConfigs"."name" = 'ticket-fulfillment';

-- Switch from explicitly storing the job name to storing a reference to the job in the jobId
UPDATE "JobConfigs" 
SET "jobId" = subquery."jobId"
FROM (
    SELECT
        jc.id AS "configId",
        jc."name" AS "jobType",
        j.id AS "jobId"
    FROM "JobConfigs" jc
    JOIN "Jobs" j
    ON jc."name" = j."type"
) AS subquery
WHERE "JobConfigs".id = subquery."configId";

-- Reset unique constraint
ALTER TABLE "JobConfigs" ALTER COLUMN "jobId" SET NOT NULL;
ALTER TABLE "JobConfigs" ADD CONSTRAINT "JobConfigs_clientId_jobId_key" UNIQUE ("clientId", "jobId");

-- Remove unused columns
ALTER TABLE "JobConfigs" DROP COLUMN "name";
