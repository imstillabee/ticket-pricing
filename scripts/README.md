# Scripts

## Docker Registry

Docker images are hosted on an AWS [ECR](https://aws.amazon.com/ecr/) repository available at `515200254182.dkr.ecr.us-east-1.amazonaws.com`

### Pushing / Pulling

Prerequisites:
- [Homebrew](https://brew.sh/)
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
- AWS User with ECR push / pull permissions

AWS ECR provides token based docker authentication that expires every 12 hours. To make pushing and pulling simpler, an [ECR docker credential helper](https://github.com/awslabs/amazon-ecr-credential-helper) should be used:

1. Install `docker-credential-helper-ecr`:

   ```
   $ brew install docker-credential-helper-ecr
   ```

2. Add the following key to your `~/.docker/config.json` file. 
   ```
   "credHelpers": {
     "515200254182.dkr.ecr.us-east-1.amazonaws.com": "ecr-login"
   }
	 ```

3. Validate the credential helper installation
   ```
   $ AWS_REGION=us-east-1 docker-credential-ecr-login list
	 ```
	 _Should return_
	 ```
	 {"https://515200254182.dkr.ecr.us-east-1.amazonaws.com":"AWS"}
	 ```
