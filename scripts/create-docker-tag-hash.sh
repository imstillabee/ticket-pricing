#!/bin/bash

set -e

GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)

DOCKER_TAG_HASH="${GIT_BRANCH#*/}-$(git rev-parse --short HEAD)" # Remove forward slash if exists for Docker tagging

printf $DOCKER_TAG_HASH
