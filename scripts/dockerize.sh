#!/bin/bash

set -e

BUILD_DIR=$1
DOCKER_REPO=$2
SHOULD_PUSH=$3

SCRIPT_DIR=$PWD/scripts
DOCKER_ORG="515200254182.dkr.ecr.us-east-1.amazonaws.com"

if [[ -z "$DOCKER_TAG_HASH" ]]; then
  printf "> DOCKER_TAG_HASH NOT set. Setting DOCKER_TAG based on current branch \n"
  DOCKER_TAG_HASH=$(${SCRIPT_DIR}/create-docker-tag-hash.sh)
fi

printf "> Setting DOCKER_TAG=$DOCKER_TAG_HASH \n"
DOCKER_TAG=$DOCKER_TAG_HASH

# Build docker image
printf "\n> Building Docker Images: $DOCKER_ORG/$DOCKER_REPO\n\n"
docker build --no-cache --rm --compress \
  --tag $DOCKER_ORG/$DOCKER_REPO:$DOCKER_TAG \
  $BUILD_DIR

# Push docker images
if [[ "$SHOULD_PUSH" != "" && "$SHOULD_PUSH" == true ]]; then
	printf "\n> Pushing Docker Images to ECR: $DOCKER_ORG/$DOCKER_REPO\n\n"
	docker push $DOCKER_ORG/$DOCKER_REPO:$DOCKER_TAG
else
	printf "\n> NOT pushing Docker Images to ECR\n\n"
fi
